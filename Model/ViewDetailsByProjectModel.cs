﻿namespace Model
{
    public class ViewDetailsByProjectModel
    {
        private int _ProjectDetailMasterID;

        public int ProjectDetailMasterID
        {
            get { return _ProjectDetailMasterID; }
            set { _ProjectDetailMasterID = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name == null ? "" : _Name.Trim(); }
            set { _Name = (value == null ? "" : value.Trim()); }
        }

        private string _Attachment;

        public string Attachment
        {
            get { return _Attachment == null ? "" : _Attachment.Trim(); }
            set { _Attachment = (value == null ? "" : value.Trim()); }
        }
    }
}