﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
  public  class AndroidFileModel
    {
        private string _Attachment;

        public string Attachment
        {
            get { return _Attachment == null ? "" : _Attachment.Trim(); }
            set { _Attachment = (value == null ? "" : value.Trim()); }
        }
        private string _OriginalFileName;

        public string OriginalFileName
        {
            get { return _OriginalFileName == null ? "" : _OriginalFileName.Trim(); }
            set { _OriginalFileName = (value == null ? "" : value.Trim()); }
        }

        private string _DocType;

        public string DocType
        {
            get { return _DocType == null ? "" : _DocType.Trim(); }
            set { _DocType = (value == null ? "" : value.Trim()); }
        }
    }
}
