﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
   public class ProviderConfigurationViewModel
    {
        public int ProviderID { get; set; }
        public int OperatorID { get; set; }
        public int ServiceID { get; set; }

        private string _OpCode;

        public string OpCode
        {
            get { return _OpCode == null ? "" : _OpCode.Trim(); }
            set { _OpCode = value == null ? "" : value.Trim(); }
        }

        private string _OperatorName;

        public string OperatorName
        {
            get { return _OperatorName == null ? "" : _OperatorName.Trim(); }
            set { _OperatorName = value == null ? "" : value.Trim(); }
        }

        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName.Trim(); }
            set { _ServiceName = value == null ? "" : value.Trim(); }
        }
        private string _Provider;

        public string Provider
        {
            get { return _Provider == null ? "" : _Provider.Trim(); }
            set { _Provider = value == null ? "" : value.Trim(); }
        }

        
        private string _Type;

        public string Type
        {
            get { return _Type == null ? "" : _Type.Trim(); }
            set { _Type = value == null ? "" : value.Trim(); }
        }
    }
}
