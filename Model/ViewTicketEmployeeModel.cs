﻿namespace Model
{
    public class ViewTicketEmployeeModel
    {
        private int _TicketID;

        public int TicketID
        {
            get { return _TicketID; }
            set { _TicketID = value; }
        }

        private string _TicketNo;

        public string TicketNo
        {
            get { return _TicketNo == null ? "" : _TicketNo.Trim(); }
            set { _TicketNo = (value == null ? "" : value.Trim()); }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName == null ? "" : _SystemName.Trim(); }
            set { _SystemName = (value == null ? "" : value.Trim()); }
        }

        private System.DateTime _DOC;

        public System.DateTime DOC
        {
            get { return _DOC; }
            set { _DOC = value; }
        }

        public string DOCInString
        {
            get
            {
                return _DOC.ToString("dd/MMMM/yyyy").Trim();
            }
        }

        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName.Trim(); }
            set { _ServiceName = (value == null ? "" : value.Trim()); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.Trim(); }
            set { _Description = (value == null ? "" : value.Trim()); }
        }

        private string _Attachment;

        public string Attachment
        {
            get { return _Attachment == null ? "" : _Attachment.Trim(); }
            set { _Attachment = (value == null ? "" : value.Trim()); }
        }

        private string _TicketStatus;

        public string TicketStatus
        {
            get { return _TicketStatus == null ? "" : _TicketStatus.Trim(); }
            set { _TicketStatus = (value == null ? "" : value.Trim()); }
        }

        private string _EmployeeRemark;

        public string EmployeeRemark
        {
            get { return _EmployeeRemark == null ? "" : _EmployeeRemark.Trim(); }
            set { _EmployeeRemark = (value == null ? "" : value.Trim()); }
        }

        private string _AdminRemark;

        public string AdminRemark
        {
            get { return _AdminRemark == null ? "" : _AdminRemark.Trim(); }
            set { _AdminRemark = (value == null ? "" : value.Trim()); }
        }

        private int _FileID;

        public int FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }

        private string _OriginalFileName;

        public string OriginalFileName
        {
            get { return _OriginalFileName == null ? "" : _OriginalFileName.Trim(); }
            set { _OriginalFileName = (value == null ? "" : value.Trim()); }
        }
    }
}