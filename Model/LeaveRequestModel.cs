﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
   public class LeaveRequestModel
    {
        private int _LeaveID;
        private string eMailID;

        public string EMailID
        {
            get { return eMailID; }
            set { eMailID = value; }
        }
        public int LeaveID
        {
            get { return _LeaveID; }
            set { _LeaveID = value; }
        }
        private DateTime _FromDate;

        public DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }

        private DateTime _ToDate;

        public DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }
        private string _Description;

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        private int _IsApprove;

        public int IsApprove
        {
            get { return _IsApprove; }
            set { _IsApprove = value; }
        }
        private int _LeaveRequestID;

        public int LeaveRequestID
        {
            get { return _LeaveRequestID; }
            set { _LeaveRequestID = value; }
        }
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private int _NoOfDays;

        public int NoOfDays
        {
            get { return _NoOfDays; }
            set { _NoOfDays = value; }
        }
        private int _ApprovedBy;

        public int ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
       
        public string FromDateString
        {
            get
            {
                return _FromDate.ToString("dd/MMMM/yyyy").Trim();
            }
        }
        public string ToDateString
        {
            get
            {
                return _ToDate.ToString("dd/MMMM/yyyy").Trim();
            }
        }
        private string _ApprovedByName;
    

       public string ApprovedByName
      {
        get { return _ApprovedByName; }
        set { _ApprovedByName = value; }
      }
   }
}
