﻿namespace Model
{
    public class SystemNameModel
    {
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name == null ? "" : _Name; }
            set { _Name = (value == null ? "" : value.Trim()); }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName == null ? "" : _SystemName.Trim(); }
            set { _SystemName = (value == null ? "" : value.Trim()); }
        }

        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }

        private string _Username;

        public string Username
        {
            get { return _Username == null ? "" : _Username.Trim(); }
            set { _Username = (value == null ? "" : value.Trim()); }
        }

        private string _EmailID;

        public string EmailID
        {
            get { return _EmailID == null ? "" : _EmailID.Trim(); }
            set { _EmailID = (value == null ? "" : value.Trim()); }
        }

        private string _MobileNumber;

        public string MobileNumber
        {
            get { return _MobileNumber == null ? "" : _MobileNumber.Trim(); }
            set { _MobileNumber = (value == null ? "" : value.Trim()); }
        }

        private int _ServiceID;

        public int ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName.Trim(); }
            set { _ServiceName = (value == null ? "" : value.Trim()); }
        }

        private int _ProductID;

        public int ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }

        private string _ProductName;

        public string ProductName
        {
            get { return _ProductName == null ? "" : _ProductName.Trim(); }
            set { _ProductName = (value == null ? "" : value.Trim()); }
        }

        private string _URL;

        public string URL
        {
            get { return _URL; }
            set { _URL = (value == null ? "" : value.Trim()); }
        }
    }
}