﻿namespace Model
{
    public class TimeSheetModel
    {
        private int _TimeSheetMasterID;

        public int TimeSheetMasterID
        {
            get { return _TimeSheetMasterID; }
            set { _TimeSheetMasterID = value; }
        }

        private int _TimeSheetDetailID;

        public int TimeSheetDetailID
        {
            get { return _TimeSheetDetailID; }
            set { _TimeSheetDetailID = value; }
        }

        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName == null ? "" : _SystemName.Trim(); }
            set { _SystemName = (value == null ? "" : value.Trim()); }
        }

        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _UserName;

        public string UserName
        {
            get { return _UserName == null ? "" : _UserName.Trim(); }
            set { _UserName = (value == null ? "" : value.Trim()); }
        }

        private int _Time;

        public int Time
        {
            get { return _Time; }
            set { _Time = value; }
        }

        private int _TotalTime;

        public int TotalTime
        {
            get { return _TotalTime; }
            set { _TotalTime = value; }
        }

        private string _Task;

        public string Task
        {
            get { return _Task == null ? "" : _Task.Trim(); }
            set { _Task = (value == null ? "" : value.Trim()); }
        }

        private string _Date;

        public string Date
        {
            get { return _Date == null ? "" : _Date.Trim(); }
            set { _Date = (value == null ? "" : value.Trim()); }
        }

        private string _DOC;

        public string DOC
        {
            get { return _DOC == null ? "" : _DOC.Trim(); }
            set { _DOC = (value == null ? "" : value.Trim()); }
        }

        private string _Reason;

        public string Reason
        {
            get { return _Reason == null ? "" : _Reason.Trim(); }
            set { _Reason = (value == null ? "" : value.Trim()); }
        }

        private string _FormDate;

        public string FormDate
        {
            get { return _FormDate == null ? "" : _FormDate.Trim(); }
            set { _FormDate = (value == null ? "" : value.Trim()); }
        }

        private string _Todate;

        public string Todate
        {
            get { return _Todate == null ? "" : _Todate.Trim(); }
            set { _Todate = (value == null ? "" : value.Trim()); }
        }

        int _Status;

        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
    }
}