﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class HoliDayModel
    {

        int _DayID;

        public int DayID
        {
            get { return _DayID == null ? 0 : _DayID; }
            set { _DayID = value == null ? 0 : value; }
        }

        DateTime _Date;

        public DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }


        public string DOCInString
        {
            get
            {
                return _Date.ToString("dd/MMMM/yyyy").Trim();
            }
        }

        string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.Trim().ToString(); }
            set { _Description = value == null ? "" : value; }
        }
    }
}
