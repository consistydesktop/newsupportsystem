﻿using log4net;
using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Model
{
    public class Logger
    {
        public void LogInfo(string Pagename, string FunctionName, string Message)
        {
            ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            Message = string.Format("{0} - {1} : {2}", Pagename, FunctionName, Message);
            log.Info(Message);
        }

        public void write(Exception ex, string info = "", [CallerMemberName] string callerName = "",

            [CallerFilePath] string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0)
        {
            var st = new StackTrace(ex, true); // create the stack trace
            var query = st.GetFrames()         // get the frames
                            .Select(frame => new
                            {
                                // get the info
                                FileName = frame.GetFileName(),
                                LineNumber = frame.GetFileLineNumber(),
                                Method = frame.GetMethod().Name,
                                Class = frame.GetMethod().DeclaringType,
                            }).First();
            ILog log = LogManager.GetLogger(query.Class);

            string innerMessage = (ex.InnerException != null)
                      ? ex.InnerException.Message
                      : "";

            string ErrorMessage = string.Format("ErrorMessage : {0}, Class :{1}, Method : {2}, Line Number:{3}, Info:{4},  Inner Message:{5}",
                ex.Message, callerFilePath, callerName, callerLineNumber, info, innerMessage);

            log.Error(string.Format("{0}", ErrorMessage));
        }

        public void register()
        {
            log4net.Config.XmlConfigurator.Configure();
        }
    }
}