﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class APIResponse
    {
        private string _status;
        public string Status
        {
            get { return _status != null ? _status.Trim() : "PENDING"; }
            set { _status = value != null ? value.Trim() : "PENDING"; }
        }
        private string _Response;
        public string Response
        {
            get { return _Response != null ? _Response.Trim() : ""; }
            set { _Response = value != null ? value.Trim() : ""; }
        }
      
    }
}
