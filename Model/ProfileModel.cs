﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
   public class ProfileModel
    {

        int _UserID;


        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        string _UserName;

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value == null ? "" : value.Trim(); }
        }
        string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value == null ? "" : value.Trim(); }
        }

        string _EmailID;

        public string EmailID
        {
            get { return _EmailID; }
            set { _EmailID = value == null ? "" : value.Trim(); }
        }

        string _NewEmailID;

        public string NewEmailID
        {
            get { return _NewEmailID; }
            set { _NewEmailID = value == null ? "" : value.Trim(); }
        }

        string _Password;

        public string Password
        {
            get { return _Password; }
            set { _Password = value == null ? "" : value.Trim(); }
        }

        string _NewPassword;

        public string NewPassword
        {
            get { return _NewPassword; }
            set { _NewPassword = value == null ? "" : value.Trim(); }
        }
        
       
    }
}
