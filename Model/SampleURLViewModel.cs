﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class SampleURLViewModel
    {
        public int SampleURLID { get; set; }
        private string _DeveloperName;

        public string DeveloperName
        {
            get { return _DeveloperName == null ? "" : _DeveloperName.Trim(); }
            set { _DeveloperName = value == null ? "" : value.Trim(); }
        }

        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName.Trim(); }
            set { _ServiceName = value == null ? "" : value.Trim(); }
        }

        private string _URL;

        public string URL
        {
            get { return _URL == null ? "" : _URL.Trim(); }
            set { _URL = value == null ? "" : value.Trim(); }
        }


        private string _URLType;

        public string URLType
        {
            get { return _URLType == null ? "" : _URLType.Trim(); }
            set { _URLType = value == null ? "" : value.Trim(); }
        }
        
        private string _MethodType;

        public string MethodType
        {
            get { return _MethodType == null ? "" : _MethodType.Trim(); }
            set { _MethodType = value == null ? "" : value.Trim(); }
        }

        private string _HeaderJSON;

        public string HeaderJSON
        {
            get { return _HeaderJSON == null ? "" : _HeaderJSON.Trim(); }
            set { _HeaderJSON = value == null ? "" : value.Trim(); }
        }

        private string _DateFormat;

        public string DateFormat
        {
            get { return _DateFormat == null ? "" : _DateFormat.Trim(); }
            set { _DateFormat = value == null ? "" : value.Trim(); }
        }

        public int ProviderID { get; set; }
        public int ServiceID { get; set; }
    }
}
