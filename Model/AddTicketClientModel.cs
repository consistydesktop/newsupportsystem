﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class AddTicketClientModel
    {
        public int UserID { get; set; }

        public int ServiceID { get; set; }

        public string Description { get; set; }

        public string TeamAny { get; set; }

        public string Priority { get; set; }

        public string TicketNo { get; set; }

        public string RequestType { get; set; }

        public int FileID { get; set; }

        public int TicketID { get; set; }

        public string UploadType { get; set; }
        public string Attachment { get; set; }

        private DateTime _DOC;
     

        public DateTime DOC
        {
            get { return _DOC; }
            set { _DOC = value; }
        }
        public string ServiceName { get; set; }

        public string TicketStatus { get; set; }

        public string EmployeeRemark { get; set; }

        public string AdminRemark { get; set; }


        public string DOCInString
        {
            get
            {
                return _DOC.ToString("dddd, dd MMMM yyyy HH:mm:ss").Trim();
            }
        }

        public string RemoteAccessType { get; set; }

        public string Title { get; set; }
        public string OriginalFileName { get; set; }
    }
}
