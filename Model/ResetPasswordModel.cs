﻿namespace Model
{
    public class ResetPasswordModel
    {
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _NewPassword;

        public string NewPassword
        {
            get { return _NewPassword == null ? "" : _NewPassword.Trim(); }
            set { _NewPassword = (value == null ? "" : value.Trim()); }
        }

        private string _OldPassword;

        public string OldPassword
        {
            get { return _OldPassword == null ? "" : _OldPassword.Trim(); }
            set { _OldPassword = (value == null ? "" : value.Trim()); }
        }
    }
}