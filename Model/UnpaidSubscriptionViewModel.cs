﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class UnpaidSubscriptionViewModel
    {
        private string _Name;

        public string Name
        {
            get { return _Name == null ? "" : _Name.ToString().Trim(); }
            set { _Name = value == null ? "" : value.Trim(); }
        }
        private string _LastDOC;

        public string LastDOC
        {
            get { return _LastDOC == null ? "" : _LastDOC.ToString().Trim(); }
            set { _LastDOC = value == null ? "" : value.Trim(); }
        }
        private string _WebSiteURL;

        public string WebSiteURL
        {
            get { return _WebSiteURL == null ? "" : _WebSiteURL.ToString().Trim(); }
            set { _WebSiteURL = value == null ? "" : value.Trim(); }
        }

        
        private string _Month;

        public string Month
        {
            get { return _Month == null ? "" : _Month.ToString().Trim(); }
            set { _Month = value == null ? "" : value.Trim(); }
        }

        private string _ProductName;

        public string ProductName
        {
            get { return _ProductName == null ? "" : _ProductName.ToString().Trim(); }
            set { _ProductName = value == null ? "" : value.Trim(); }
        }
        
        public decimal LastAmount { get; set; }
        public int SystemID { get; set; }
        public int UserID { get; set; }
    }
}
