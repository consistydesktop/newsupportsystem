﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class AndroidSupportModel
    {
        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }
        private string _PlayStoreURL;

        public string PlayStoreURL
        {
            get { return _PlayStoreURL; }
            set { _PlayStoreURL = value; }
        }
        private string _GoogleConsoleUserName;

        public string GoogleConsoleUserName
        {
            get { return _GoogleConsoleUserName; }
            set { _GoogleConsoleUserName = value; }
        }
        private string _GoogleConsolePassword;

        public string GoogleConsolePassword
        {
            get { return _GoogleConsolePassword; }
            set { _GoogleConsolePassword = value; }
        }
        private string _CodeRepository;

        public string CodeRepository
        {
            get { return _CodeRepository; }
            set { _CodeRepository = value; }
        }
       
        private string _Description;

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName; }
            set { _SystemName = value; }
        }
        private int _FileID;

        public int FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }
        private string _Attachment;

        public string Attachment
        {
            get { return _Attachment; }
            set { _Attachment = value; }
        }
        private string _OriginalFileName;

        public string OriginalFileName
        {
            get { return _OriginalFileName; }
            set { _OriginalFileName = value; }
        }
        private string _UploadType;

        public string UploadType
        {
            get { return _UploadType; }
            set { _UploadType = value; }
        }
        private string _DocType;
        public string DocType
        {
            get { return _DocType != null ? _DocType.Trim() : ""; }
            set { _DocType = value != null ? value.Trim() : ""; }
        }
        public int AndroidPlayStoreID { get; set; }
    }
}
