﻿namespace Model
{
    public class MessageWithIcon
    {

        private string _Icon;

        public string Icon
        {
            get { return _Icon == null ? "" : _Icon.ToString(); }
            set { _Icon = (value == null ? "" : value.Trim()); }
        }

      

        private string _Message;

        public string Message
        {
            get { return _Message == null ? "" : _Message.Trim(); }
            set { _Message = (value == null ? "" : value.Trim()); }
        }

      
    }
}