﻿namespace Model
{
    public class DashboardModel
    {
        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName == null ? "" : _SystemName.Trim(); }
            set { _SystemName = (value == null ? "" : value.Trim()); }
        }

        private string _TicketNo;

        public string TicketNo
        {
            get { return _TicketNo == null ? "" : _TicketNo.Trim(); }
            set { _TicketNo = (value == null ? "" : value.Trim()); }
        }

        private string _PendingTickets;

        public string PendingTickets
        {
            get { return _PendingTickets == null ? "" : _PendingTickets.Trim(); }
            set { _PendingTickets = (value == null ? "" : value.Trim()); }
        }

        private string _ResolvedTickets;

        public string ResolvedTickets
        {
            get { return _ResolvedTickets == null ? "" : _ResolvedTickets.Trim(); }
            set { _ResolvedTickets = (value == null ? "" : value.Trim()); }
        }

        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }

        private int _ServiceID;

        public int ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName.Trim(); }
            set { _ServiceName = (value == null ? "" : value.Trim()); }
        }

        private int _TicketID;

        public int TicketID
        {
            get { return _TicketID; }
            set { _TicketID = value; }
        }
    }
}