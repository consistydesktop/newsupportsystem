﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class SystemReportModel
    {
        private int _TimeSheetMasterID;

        public int TimeSheetMasterID
        {
            get { return _TimeSheetMasterID; }
            set { _TimeSheetMasterID = value; }
        }

        private int _TimeSheetDetailID;

        public int TimeSheetDetailID
        {
            get { return _TimeSheetDetailID; }
            set { _TimeSheetDetailID = value; }
        }

        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName == null ? "" : _SystemName.Trim(); }
            set { _SystemName = (value == null ? "" : value.Trim()); }
        }

        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _UserName;

        public string UserName
        {
            get { return _UserName == null ? "" : _UserName.Trim(); }
            set { _UserName = (value == null ? "" : value.Trim()); }
        }

        private int _Time;

        public int Time
        {
            get { return _Time; }
            set { _Time = value; }
        }

        private int _TotalTime;

        public int TotalTime
        {
            get { return _TotalTime; }
            set { _TotalTime = value; }
        }

        

        private string _Date;

        public string Date
        {
            get { return _Date == null ? "" : _Date.Trim(); }
            set { _Date = (value == null ? "" : value.Trim()); }
        }

        private string _DOC;

        public string DOC
        {
            get { return _DOC == null ? "" : _DOC.Trim(); }
            set { _DOC = (value == null ? "" : value.Trim()); }
        }

        private string _Reason;

        public string Reason
        {
            get { return _Reason == null ? "" : _Reason.Trim(); }
            set { _Reason = (value == null ? "" : value.Trim()); }
        }

        private string _FormDate;

        public string FormDate
        {
            get { return _FormDate == null ? "" : _FormDate.Trim(); }
            set { _FormDate = (value == null ? "" : value.Trim()); }
        }

        private string _Todate;

        public string Todate
        {
            get { return _Todate == null ? "" : _Todate.Trim(); }
            set { _Todate = (value == null ? "" : value.Trim()); }
        }

        int _Status;

        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }


        string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private int _TaskID;

        public int TaskID
        {
            get { return _TaskID; }
            set { _TaskID = value; }
        }

        private string _Task;

        public string Task
        {
            get { return _Task == null ? "" : _Task.ToString().Trim(); }
            set { _Task = value == null ? "" : value.Trim(); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.ToString().Trim(); }
            set { _Description = value == null ? "" : value.Trim(); }
        }

        private string _Deadline;

        public string Deadline
        {
            get { return _Deadline == null ? "" : _Deadline.ToString().Trim(); }
            set { _Deadline = value == null ? "" : value.Trim(); }
        }

  

        private int _Point;

        public int Point
        {
            get { return _Point; }
            set { _Point = value; }
        }

      

        private string _Attachment;

        public string Attachment
        {
            get { return _Attachment == null ? "" : _Attachment.ToString().Trim(); }
            set { _Attachment = value == null ? "" : value.Trim(); }
        }

        private string _OriginalFileName;

        public string OriginalFileName
        {
            get { return _OriginalFileName == null ? "" : _OriginalFileName.ToString().Trim(); }
            set { _OriginalFileName = value == null ? "" : value.Trim(); }
        }

        private int _AssignedUserID;

        public int AssignedUserID
        {
            get { return _AssignedUserID; }
            set { _AssignedUserID = value; }
        }

        private string _TaskStatus;

        public string TaskStatus
        {
            get { return _TaskStatus; }
            set { _TaskStatus = value; }
        }

        private int _TaskDetailMasterID;

        public int TaskDetailMasterID
        {
            get { return _TaskDetailMasterID; }
            set { _TaskDetailMasterID = value; }
        }

        private string _SubTask;

        public string SubTask
        {
            get { return _SubTask == null ? "" : _SubTask.ToString().Trim(); }
            set { _SubTask = value == null ? "" : value.Trim(); }
        }

        private int _SubTaskStatus;

        public int SubTaskStatus
        {
            get { return _SubTaskStatus; }
            set { _SubTaskStatus = value; }
        }

        private string _fileIDs;

        public string FileIDs
        {
            get { return _fileIDs == null ? "" : _fileIDs.ToString().Trim(); }
            set { _fileIDs = value == null ? "" : value.Trim(); }
        }

        private int _FileID;

        public int FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }

        private string _UploadType;

        public string UploadType
        {
            get { return _UploadType == null ? "" : _UploadType.ToString().Trim(); }
            set { _UploadType = value == null ? "" : value.Trim(); }
        }

        private string _Comment;

        public string Comment
        {
            get { return _Comment == null ? "" : _Comment.ToString().Trim(); }
            set { _Comment = value == null ? "" : value.Trim(); }
        }

        private int _CommentID;

        public int CommentID
        {
            get { return _CommentID; }
            set { _CommentID = value; }
        }

        private int _SubTaskID;

        public int SubTaskID
        {
            get { return _SubTaskID; }
            set { _SubTaskID = value; }
        }

        private string _CommentType;

        public string CommentType
        {
            get { return _CommentType == null ? "" : _CommentType.ToString().Trim(); }
            set { _CommentType = value == null ? "" : value.Trim(); }
        }

        private string _EmployeeNames;

        public string EmployeeNames
        {
            get { return _EmployeeNames == null ? "" : _EmployeeNames.ToString().Trim(); }
            set { _EmployeeNames = value == null ? "" : value.Trim(); }
        }



        private int _TicketID;

        public int TicketID
        {
            get { return _TicketID; }
            set { _TicketID = value; }
        }

        private string _TicketNo;

        public string TicketNo
        {
            get { return _TicketNo == null ? "" : _TicketNo; }
            set { _TicketNo = (value == null ? "" : value.Trim()); }
        }

      

        //public string DOCInString
        //{
        //    get
        //    {
        //        return _DOC.ToString("dd/MMMM/yyyy").Trim();
        //    }
        //}

        private int _ServiceID;

        public int ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName.Trim(); }
            set { _ServiceName = (value == null ? "" : value.Trim()); }
        }

    


        private string _TicketStatus;

        public string TicketStatus
        {
            get { return _TicketStatus == null ? "" : _TicketStatus.Trim(); }
            set { _TicketStatus = (value == null ? "" : value.Trim()); }
        }

        private string _EmployeeRemark;

        public string EmployeeRemark
        {
            get { return _EmployeeRemark == null ? "" : _EmployeeRemark.Trim(); }
            set { _EmployeeRemark = (value == null ? "" : value.Trim()); }
        }

        private string _AdminRemark;

        public string AdminRemark
        {
            get { return _AdminRemark == null ? "" : _AdminRemark.Trim(); }
            set { _AdminRemark = (value == null ? "" : value.Trim()); }
        }


        private string _TicketGeneratedDate;

        public string TicketGeneratedDate
        {
            get { return _TicketGeneratedDate; }
            set { _TicketGeneratedDate = value; }
        }


        private string _AssignedDate;

        public string AssignedDate
        {
            get { return _AssignedDate; }
            set { _AssignedDate = value == null ? "" : value.Trim(); }
        }
    }
}
