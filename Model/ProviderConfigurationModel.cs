﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ProviderConfigurationModel
    {
        public int ProviderID { get; set; }
        public int OperatorID { get; set; }
        public int ServiceID { get; set; }

        private string _Opcode;

        public string Opcode
        {
            get { return _Opcode == null ? "" : _Opcode.Trim(); }
            set { _Opcode = value == null ? "" : value.Trim(); }
        }

        private string _OperatorName;

        public string OperatorName
        {
            get { return _OperatorName == null ? "" : _OperatorName.Trim(); }
            set { _OperatorName = value == null ? "" : value.Trim(); }
        }

        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName.Trim(); }
            set { _ServiceName = value == null ? "" : value.Trim(); }
        }
         private string _Param1;

        public string Param1
        {
            get { return _Param1 == null ? "" : _Param1.Trim(); }
            set { _Param1 = value == null ? "" : value.Trim(); }
        }
    }
}
