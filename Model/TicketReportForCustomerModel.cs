﻿namespace Model
{
    public class TicketReportForCustomerModel
    {
        private int _TicketID;

        public int TicketID
        {
            get { return _TicketID; }
            set { _TicketID = value; }
        }

        private string _Priority;

        public string Priority
        {
            get { return _Priority == null ? "" : _Priority.Trim(); }
            set { _Priority = (value == null ? "" : value.Trim()); }
        }

        private string _DOC;

        public string DOC
        {
            get { return _DOC == null ? "" : _DOC.Trim(); }
            set { _DOC = (value == null ? "" : value.Trim()); }
        }

        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName.Trim(); }
            set { _ServiceName = (value == null ? "" : value.Trim()); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.Trim(); }
            set { _Description = (value == null ? "" : value.Trim()); }
        }

        private string _Attachment;

        public string Attachment
        {
            get { return _Attachment == null ? "" : _Attachment.Trim(); }
            set { _Attachment = (value == null ? "" : value.Trim()); }
        }

        private string _TicketStatus;

        public string TicketStatus
        {
            get { return _TicketStatus == null ? "" : _TicketStatus.Trim(); }
            set { _TicketStatus = (value == null ? "" : value.Trim()); }
        }

        private string _EmployeeRemark;

        public string EmployeeRemark
        {
            get { return _EmployeeRemark == null ? "" : _EmployeeRemark.Trim(); }
            set { _EmployeeRemark = (value == null ? "" : value.Trim()); }
        }

        private string _AdminRemark;

        public string AdminRemark
        {
            get { return _AdminRemark == null ? "" : _AdminRemark.Trim(); }
            set { _AdminRemark = (value == null ? "" : value.Trim()); }
        }

        private string _GeneratedDate;

        public string GeneratedDate
        {
            get { return _GeneratedDate == null ? "" : _GeneratedDate.Trim(); }
            set { _GeneratedDate = (value == null ? "" : value.Trim()); }
        }

        private string _ResolvedDate;

        public string ResolvedDate
        {
            get { return _ResolvedDate == null ? "" : _ResolvedDate.Trim(); }
            set { _ResolvedDate = (value == null ? "" : value.Trim()); }
        }

        private string _TicketNo;

        public string TicketNo
        {
            get { return _TicketNo == null ? "" : _TicketNo.Trim(); }
            set { _TicketNo = (value == null ? "" : value.Trim()); }
        }

        private string _RequestType;

        public string RequestType
        {
            get { return _RequestType == null ? "" : _RequestType.Trim(); }
            set { _RequestType = (value == null ? "" : value.Trim()); }
        }

        public int UserID { get; set; }
    }
}