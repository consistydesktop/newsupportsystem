﻿namespace Model
{
    public class InvoiceModel
    {

        public int UserID { get; set; }
        public int UserTypeID { get; set; }
        public int SystemID { get; set; }

        public int SuccessCount { get; set; }

        public decimal Amount;
        public decimal CGST;
        public decimal SGST;
        public decimal IGST;
        public decimal Total;

        public decimal Discount { get; set; }

   


        private string _FromDate;

        public string FromDate
        {
            get { return _FromDate != null ? _FromDate.Trim() : ""; }
            set { _FromDate = value != null ? value.Trim() : ""; }
        }


        private string _ToDate;

        public string ToDate
        {
            get { return _ToDate != null ? _ToDate.Trim() : ""; }
            set { _ToDate = value != null ? value.Trim() : ""; }
        }

        private string _UserName;

        public string UserName
        {
            get { return _UserName != null ? _UserName.Trim() : ""; }
            set { _UserName = value != null ? value.Trim() : ""; }
        }

        private string _Receiver;

        public string Receiver
        {
            get { return _Receiver != null ? _Receiver.Trim() : ""; }
            set { _Receiver = value != null ? value.Trim() : ""; }
        }


        
        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName != null ? _SystemName.Trim() : ""; }
            set { _SystemName = value != null ? value.Trim() : ""; }
        }
        private string _State;

        public string State
        {
            get { return _State != null ? _State.Trim() : ""; }
            set { _State = value != null ? value.Trim() : ""; }
        }

        private string _GSTNumber;

        public string GSTNumber
        {
            get { return _GSTNumber != null ? _GSTNumber.Trim() : ""; }
            set { _GSTNumber = value != null ? value.Trim() : ""; }
        }


        private string _Address;

        public string Address
        {
            get { return _Address != null ? _Address.Trim() : ""; }
            set { _Address = value != null ? value.Trim() : ""; }
        }

        private string _PAN;

        public string PAN
        {
            get { return _PAN != null ? _PAN.Trim() : ""; }
            set { _PAN = value != null ? value.Trim() : ""; }
        }


        private string _DOC;

        public string DOC
        {
            get { return _DOC != null ? _DOC.Trim() : ""; }
            set { _DOC = value != null ? value.Trim() : ""; }
        }

        private string _InvoiceNumber;

        public string InvoiceNumber
        {
            get { return _InvoiceNumber != null ? _InvoiceNumber.Trim() : ""; }
            set { _InvoiceNumber = value != null ? value.Trim() : ""; }
        }

        public int ReceiverID { get; set; }
        public decimal Charge { get; set; }
  
    }
}
