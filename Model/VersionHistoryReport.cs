﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
   public class VersionHistoryReport
    {
        

        private string _Title;

        public string Title
        {
            get { return _Title != null ? _Title.Trim() : ""; }
            set { _Title = value != null ? value.Trim() : ""; }
        }
        private string _Description;

        public string Description
        {
            get { return _Description != null ? _Description.Trim() : ""; }
            set { _Description = value != null ? value.Trim() : ""; }
        }

        private string _Version;

        public string Version
        {
            get { return _Version != null ? _Version.Trim() : ""; }
            set { _Version = value != null ? value.Trim() : ""; }
        }
        private string _DOC;

        public string DOC
        {
            get { return _DOC != null ? _DOC.Trim() : ""; }
            set { _DOC = value != null ? value.Trim() : ""; }
        }

        public int FinnovativeVersionHistoryID { get; set; }
    }
}
