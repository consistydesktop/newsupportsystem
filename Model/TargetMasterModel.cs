﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class TargetMasterModel
    {
        private string _Month;

        public string Month
        {
            get { return _Month; }
            set { _Month = value; }
        }
        private string _Year;

        public string Year
        {
            get { return _Year; }
            set { _Year = value; }
        }
        private decimal _TargetAmount;

        public decimal TargetAmount
        {
            get { return _TargetAmount; }
            set { _TargetAmount = value; }
        }
        private int _TransactionMasterID;

        public int TransactionMasterID
        {
            get { return _TransactionMasterID; }
            set { _TransactionMasterID = value; }
        }
    }
}
