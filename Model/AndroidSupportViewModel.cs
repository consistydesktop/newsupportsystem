﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class AndroidSupportViewModel
    {
        private string _FormDate;

        public string FormDate
        {
            get { return _FormDate == null ? "" : _FormDate.Trim(); }
            set { _FormDate = (value == null ? "" : value.Trim()); }
        }

        private string _Todate;


        public string Todate
        {
            get { return _Todate == null ? "" : _Todate.Trim(); }
            set { _Todate = (value == null ? "" : value.Trim()); }
        }

        private string _DOC;


        public string DOC
        {
            get { return _DOC == null ? "" : _DOC.Trim(); }
            set { _DOC = (value == null ? "" : value.Trim()); }
        }
        

        private string _KeyStoreFileName;


        public string KeyStoreFileName
        {
            get { return _KeyStoreFileName == null ? "" : _KeyStoreFileName.Trim(); }
            set { _KeyStoreFileName = (value == null ? "" : value.Trim()); }
        }
        private string _KeyStoreFilePath;


        public string KeyStoreFilePath
        {
            get { return _KeyStoreFilePath == null ? "" : _KeyStoreFilePath.Trim(); }
            set { _KeyStoreFilePath = (value == null ? "" : value.Trim()); }
        }
        private string _JKSFileName;
        public string JKSFileName
        {
            get { return _JKSFileName == null ? "" : _JKSFileName.Trim(); }
            set { _JKSFileName = (value == null ? "" : value.Trim()); }
        }
        private string _JKSFileFilePath;


        public string JKSFileFilePath
        {
            get { return _JKSFileFilePath == null ? "" : _JKSFileFilePath.Trim(); }
            set { _JKSFileFilePath = (value == null ? "" : value.Trim()); }
        }



        public int SystemID { get; set; }

        private string _PlayStoreURL;

        public string PlayStoreURL
        {
            get { return _PlayStoreURL; }
            set { _PlayStoreURL = value; }
        }
        private string _GoogleConsoleUserName;

        public string GoogleConsoleUserName
        {
            get { return _GoogleConsoleUserName; }
            set { _GoogleConsoleUserName = value; }
        }
        private string _GoogleConsolePassword;

        public string GoogleConsolePassword
        {
            get { return _GoogleConsolePassword; }
            set { _GoogleConsolePassword = value; }
        }
        private string _CodeRepository;

        public string CodeRepository
        {
            get { return _CodeRepository; }
            set { _CodeRepository = value; }
        }

        private string _Description;

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName; }
            set { _SystemName = value; }
        }

        public int AndroidPlayStoreID { get; set; }
    }
}
