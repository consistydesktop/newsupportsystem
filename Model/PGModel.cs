﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class PGModel
    {
        public int SystemID { get; set; }
        public string IPAddress { get; set; }
        public string OrderId { get; set; }
        public decimal Amount { get; set; }
        public decimal CommisionAmount { get; set; }

        public int UserID { get; set; }
        public string gatewayID { get; set; }
        public string gatewayName { get; set; }
        public string status { get; set; }
        public int isUpdate { get; set; }
        public string response { get; set; }

        public int PaymentGatewayID { get; set; }
        public string PayMode { get; set; }
        public string Month { get; set; }

        public string PaymentId { get; set; }

        public string Signature { get; set; }
        public string TransactionID { get; set; }

        public string Date { get; set; }
        public string ToDate { get; set; }

        public string DisplayDate { get; set; }



        public string UserName { get; set; }
        public string SystemName { get; set; }
   
        public string MobileNumber { get; set; }
        public string DaysMonthType { get; set; }
        public string DaysMonth { get; set; }
        public int ProductID { get; set; }
    }
}
