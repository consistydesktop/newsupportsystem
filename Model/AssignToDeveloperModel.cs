﻿namespace Model
{
    public class AssignToDeveloperModel
    {
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name == null ? "" : _Name.Trim(); }
            set { _Name = (value == null ? "" : value.Trim()); }
        }

        private int _ProjectDetailMasterID;

        public int ProjectDetailMasterID
        {
            get { return _ProjectDetailMasterID; }
            set { _ProjectDetailMasterID = value; }
        }

        private string _TeamAny;

        public string TeamAny
        {
            get { return _TeamAny == null ? "" : _TeamAny.Trim(); }
            set { _TeamAny = (value == null ? "" : value.Trim()); }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName == null ? "" : _SystemName.Trim(); }
            set { _SystemName = (value == null ? "" : value.Trim()); }
        }

        private string _AssignDate;

        public string AssignDate
        {
            get { return _AssignDate == null ? "" : _AssignDate.Trim(); }
            set { _AssignDate = (value == null ? "" : value.Trim()); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.Trim(); }
            set { _Description = (value == null ? "" : value.Trim()); }
        }

        private int _AssignEmployeeID;

        public int AssignEmployeeID
        {
            get { return _AssignEmployeeID; }
            set { _AssignEmployeeID = value; }
        }

        private string _AdminRemark;

        public string AdminRemark
        {
            get { return _AdminRemark == null ? "" : _AdminRemark.Trim(); }
            set { _AdminRemark = (value == null ? "" : value.Trim()); }
        }

        private int _TicketID;

        public int TicketID
        {
            get { return _TicketID; }
            set { _TicketID = value; }
        }

        private int _DeveloperID;

        public int DeveloperID
        {
            get { return _DeveloperID; }
            set { _DeveloperID = value; }
        }

        public int SystemID { get; set; }

        public string TicketNo { get; set; }

        public string Title { get; set; }
    }
}