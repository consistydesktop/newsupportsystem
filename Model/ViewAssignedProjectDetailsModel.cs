﻿namespace Model
{
    public class ViewAssignedProjectDetailsModel
    {
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private int _ProjectDetailMasterID;

        public int ProjectDetailMasterID
        {
            get { return _ProjectDetailMasterID; }
            set { _ProjectDetailMasterID = value; }
        }

        private int _FileID;

        public int FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }

        private int _ProductID;

        public int ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name == null ? "" : _Name.Trim(); }
            set { _Name = (value == null ? "" : value.Trim()); }
        }

        private string _DOC;

        public string DOC
        {
            get { return _DOC == null ? "" : _DOC.Trim(); }
            set { _DOC = (value == null ? "" : value.Trim()); }
        }

        private string _ProjectName;

        public string ProjectName
        {
            get { return _ProjectName == null ? "" : _ProjectName.Trim(); }
            set { _ProjectName = (value == null ? "" : value.Trim()); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.Trim(); }
            set { _Description = (value == null ? "" : value.Trim()); }
        }

        private string _Attachment;

        public string Attachment
        {
            get { return _Attachment == null ? "" : _Attachment.Trim(); }
            set { _Attachment = (value == null ? "" : value.Trim()); }
        }

        private string _FromDate;

        public string FromDate
        {
            get { return _FromDate == null ? "" : _FromDate.Trim(); }
            set { _FromDate = (value == null ? "" : value.Trim()); }
        }

        private string _ToDate;

        public string ToDate
        {
            get { return _ToDate == null ? "" : _ToDate.Trim(); }
            set { _ToDate = (value == null ? "" : value.Trim()); }
        }

        private string _ExpectedDelivery;

        public string ExpectedDelivery
        {
            get { return _ExpectedDelivery == null ? "" : _ExpectedDelivery.Trim(); }
            set { _ExpectedDelivery = (value == null ? "" : value.Trim()); }
        }

        private string _ProductName;

        public string ProductName
        {
            get { return _ProductName == null ? "" : _ProductName.Trim(); }
            set { _ProductName = (value == null ? "" : value.Trim()); }
        }

        private string _ClientName;

        public string ClientName
        {
            get { return _ClientName == null ? "" : _ClientName.Trim(); }
            set { _ClientName = (value == null ? "" : value.Trim()); }
        }

        private string _EmailID;

        public string EmailID
        {
            get { return _EmailID == null ? "" : _EmailID.Trim(); }
            set { _EmailID = (value == null ? "" : value.Trim()); }
        }

        private string _MobileNumber;

        public string MobileNumber
        {
            get { return _MobileNumber == null ? "" : _MobileNumber.Trim(); }
            set { _MobileNumber = (value == null ? "" : value.Trim()); }
        }

        private string _FileIDs;

        public string FileIDs
        {
            get { return _FileIDs == null ? "" : _FileIDs.Trim(); }
            set { _FileIDs = (value == null ? "" : value.Trim()); }
        }

        private string _Status;

        public string Status
        {
            get { return _Status == null ? "" : _Status.Trim(); }
            set { _Status = (value == null ? "" : value.Trim()); }
        }

        private string _OriginalFileName;

        public string OriginalFileName
        {
            get { return _OriginalFileName == null ? "" : _OriginalFileName.Trim(); }
            set { _OriginalFileName = (value == null ? "" : value.Trim()); }
        }

        private string _UploadType;

        public string UploadType
        {
            get { return _UploadType == null ? "" : _UploadType.Trim(); }
            set { _UploadType = (value == null ? "" : value.Trim()); }
        }
    }
}