﻿namespace Model
{
    public class ForgotModel
    {
        private string _MobileNumber;

        public string MobileNumber
        {
            get { return _MobileNumber == null ? "" : _MobileNumber.Trim(); }
            set { _MobileNumber = (value == null ? "" : value.Trim()); }
        }

        private string _Password;

        public string Password
        {
            get { return _Password == null ? "" : _Password.Trim(); }
            set { _Password = (value == null ? "" : value.Trim()); }
        }

        private string _EmailID;

        public string EmailID
        {
            get { return _EmailID == null ? "" : _EmailID.Trim(); }
            set { _EmailID = (value == null ? "" : value.Trim()); }
        }
    }
}