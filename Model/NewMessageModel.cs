﻿namespace Model
{
    public class NewMessageModel
    {
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private int _UserTypeID;

        public int UserTypeID
        {
            get { return _UserTypeID; }
            set { _UserTypeID = value; }
        }

        private string _UserType;

        public string UserType
        {
            get { return _UserType == null ? "" : _UserType.Trim(); }
            set { _UserType = (value == null ? "" : value.Trim()); }
        }

        private int _MessageID;

        public int MessageID
        {
            get { return _MessageID; }
            set { _MessageID = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name == null ? "" : _Name.Trim(); }
            set { _Name = (value == null ? "" : value.Trim()); }
        }

        private string _Message;

        public string Message
        {
            get { return _Message == null ? "" : _Message.Trim(); }
            set { _Message = (value == null ? "" : value.Trim()); }
        }

        private string _DOC;

        public string DOC
        {
            get { return _DOC == null ? "" : _DOC.Trim(); }
            set { _DOC = (value == null ? "" : value.Trim()); }
        }

        private string _Usertype;

        public string Usertype
        {
            get { return _Usertype == null ? "" : _UserType.Trim(); }
            set { _Usertype = (value == null ? "" : value.Trim()); }
        }

        private string _MobileNumber;

        public string MobileNumber
        {
            get { return _MobileNumber == null ? "" : _MobileNumber.Trim(); }
            set { _MobileNumber = (value == null ? "" : value.Trim()); }
        }
    }
}