﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class TransactionMasterModel
    {
        private int _TransactionMasterID;

        public int TransactionMasterID
        {
            get { return _TransactionMasterID; }
            set { _TransactionMasterID = value; }
        }

        private string _Date;

        public string Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private string _ReferenceNo;

        public string ReferenceNo
        {
            get { return _ReferenceNo; }
            set { _ReferenceNo = value; }
        } 
        private decimal _Amount;
    
        public decimal Amount
        {
            get { return _Amount; }
        set { _Amount = value; }
        }
        private string _TransactionType;

        public string TransactionType
        {
          get { return _TransactionType; }
          set { _TransactionType = value; }
        }
        private string _PaymentType;

        public string PaymentType
        {
          get { return _PaymentType; }
          set { _PaymentType = value; }
        }
        private string _Remark;
    
        public string Remark
        {
          get { return _Remark; }
          set { _Remark = value; }
        }
        private string _BankName;

        public string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }



        public object BankMasterID { get; set; }

        public object IsGST { get; set; }
    }
}
