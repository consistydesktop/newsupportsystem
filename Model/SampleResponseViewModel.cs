﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class SampleResponseViewModel
    {
        public int SampleResponseID { get; set; }
        private string _DeveloperName;

        public string DeveloperName
        {
            get { return _DeveloperName == null ? "" : _DeveloperName.Trim(); }
            set { _DeveloperName = value == null ? "" : value.Trim(); }
        }
        private string _Type;

        public string Type
        {
            get { return _Type == null ? "" : _Type.Trim(); }
            set { _Type = value == null ? "" : value.Trim(); }
        }
        private string _TextMustExit;

        public string TextMustExit
        {
            get { return _TextMustExit == null ? "" : _TextMustExit.Trim(); }
            set { _TextMustExit = value == null ? "" : value.Trim(); }
        }
        private string _Response;

        public string Response
        {
            get { return _Response == null ? "" : _Response.Trim(); }
            set { _Response = value == null ? "" : value.Trim(); }
        }
        private string _Status;

        public string Status
        {
            get { return _Status == null ? "" : _Status.Trim(); }
            set { _Status = value == null ? "" : value.Trim(); }
        }

        public int ProviderID { get; set; }
    }
}
