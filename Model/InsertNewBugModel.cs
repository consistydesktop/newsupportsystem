﻿namespace Model
{
    public class InsertNewBugModel
    {
        private int _BugID;

        public int BugID
        {
            get { return _BugID; }
            set { _BugID = value; }
        }

        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _Status;

        public string Status
        {
            get { return _Status == null ? "" : _Status.Trim(); }
            set { _Status = (value == null ? "" : value.Trim()); }
        }

        private string _UpdateDOC;

        public string UpdateDOC
        {
            get { return _UpdateDOC == null ? "" : _UpdateDOC.Trim(); }
            set { _UpdateDOC = (value == null ? "" : value.Trim()); }
        }

        
        private int _ProjectBugMasterID;

        public int ProjectBugMasterID
        {
            get { return _ProjectBugMasterID; }
            set { _ProjectBugMasterID = value; }
        }

        private string _FormDate;

        public string FormDate
        {
            get { return _FormDate == null ? "" : _FormDate.Trim(); }
            set { _FormDate = (value == null ? "" : value.Trim()); }
        }

        private string _Todate;

        public string Todate
        {
            get { return _Todate == null ? "" : _Todate.Trim(); }
            set { _Todate = (value == null ? "" : value.Trim()); }
        }


        private string _TestingIssueID;

        public string TestingIssueID
        {
            get { return _TestingIssueID; }
            set { _TestingIssueID = value; }
        }

        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }

        private int _DeveloperID;

        public int DeveloperID
        {
            get { return _DeveloperID; }
            set { _DeveloperID = value; }
        }

        private string _BugTitle;

        public string BugTitle
        {
            get { return _BugTitle == null ? "" : _BugTitle.Trim(); }
            set { _BugTitle = (value == null ? "" : value.Trim()); }
        }

        private string _PageURL;

        public string PageURL
        {
            get { return _PageURL == null ? "" : _PageURL.Trim(); }
            set { _PageURL = (value == null ? "" : value.Trim()); }
        }

        private string _Priority;

        public string Priority
        {
            get { return _Priority == null ? "" : _Priority.Trim(); }
            set { _Priority = (value == null ? "" : value.Trim()); }
        }

        private string _Attachment;

        public string Attachment
        {
            get { return _Attachment == null ? "" : _Attachment.Trim(); }
            set { _Attachment = (value == null ? "" : value.Trim()); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.Trim(); }
            set { _Description = (value == null ? "" : value.Trim()); }
        }

        private string _ExpectedResult;

        public string ExpectedResult
        {
            get { return _ExpectedResult == null ? "" : _ExpectedResult.Trim(); }
            set { _ExpectedResult = (value == null ? "" : value.Trim()); }
        }

        private System.DateTime _DOC;

        public System.DateTime DOC
        {
            get { return _DOC; }
            set { _DOC = value; }
        }

        public string DOCInString
        {
            get
            {
                return _DOC.ToString("dd/MMMM/yyyy").Trim();
            }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName; }
            set { _SystemName = (value == null ? "" : value.Trim()); }
        }

        private string _Name;

        public string Name
        {
            get { return _Name == null ? "" : _Name.Trim(); }
            set { _Name = (value == null ? "" : value.Trim()); }
        }

        private string _IssueName;

        public string IssueName
        {
            get { return _IssueName == null ? "" : _IssueName.Trim(); }
            set { _IssueName = (value == null ? "" : value.Trim()); }
        }

        private string _Comment;

        public string Comment
        {
            get { return _Comment == null ? "" : _Comment.Trim(); }
            set { _Comment = (value == null ? "" : value.Trim()); }
        }

        private int _FileID;

        public int FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }

        private string _FileIDs;

        public string FileIDs
        {
            get { return _FileIDs == null ? "" : _FileIDs.Trim(); }
            set { _FileIDs = (value == null ? "" : value.Trim()); }
        }

        private string _OriginalFileName;

        public string OriginalFileName
        {
            get { return _OriginalFileName == null ? "" : _OriginalFileName.Trim(); }
            set { _OriginalFileName = (value == null ? "" : value.Trim()); }
        }

        private string _UploadType;

        public string UploadType
        {
            get { return _UploadType == null ? "" : _UploadType.Trim(); }
            set { _UploadType = (value == null ? "" : value.Trim()); }
        }
        private string _Developer;

        public string Developer
        {
            get { return _Developer == null ? "" : _Developer.Trim(); }
            set { _Developer = (value == null ? "" : value.Trim()); }
        }


        
    }
}