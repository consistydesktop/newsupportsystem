﻿namespace Model
{
    public class MemberDetailsModel
    {
        private string _DateAndTime;

        public string DateAndTime
        {
            get { return _DateAndTime == null ? "" : _DateAndTime.Trim(); }
            set { _DateAndTime = (value == null ? "" : value.Trim()); }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName == null ? "" : SystemName.Trim(); }
            set { _SystemName = (value == null ? "" : value.Trim()); }
        }

        private string _MobileNumber;

        public string MobileNumber
        {
            get { return _MobileNumber == null ? "" : _MobileNumber.Trim(); }
            set { _MobileNumber = (value == null ? "" : value.Trim()); }
        }

        private string _EmailID;

        public string EmailID
        {
            get { return _EmailID == null ? "" : _EmailID.Trim(); }
            set { _EmailID = (value == null ? "" : value.Trim()); }
        }

        private int _IsApproved;

        public int IsApproved
        {
            get { return _IsApproved; }
            set { _IsApproved = value; }
        }

        private string _CustomerName;

        public string CustomerName
        {
            get { return _CustomerName == null ? "" : _CustomerName.Trim(); }
            set { _CustomerName = (value == null ? "" : value.Trim()); }
        }

        private int _UserTypeID;

        public int UserTypeID
        {
            get { return _UserTypeID; }
            set { _UserTypeID = value; }
        }

        private string _UserName;

        public string UserName
        {
            get { return _UserName == null ? "" : _UserName.Trim(); }
            set { _UserName = (value == null ? "" : value.Trim()); }
        }

        private int _RegistrationID;

        public int RegistrationID
        {
            get { return _RegistrationID; }
            set { _RegistrationID = value; }
        }

        public string GSTNo { get; set; }

        public string PANNo { get; set; }
    }
}