﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class AttendanceModel
    {
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }



        private int _CountOfLessThan5Days;

        public int CountOfLessThan5Days
        {
            get { return _CountOfLessThan5Days; }
            set { _CountOfLessThan5Days = value; }
        }
        private int _TimeSheetDetailID;

        public int TimeSheetDetailID
        {
            get { return _TimeSheetDetailID; }
            set { _TimeSheetDetailID = value; }
        }

        private string _Task;

        public string Task
        {
            get { return _Task; }
            set { _Task = value; }
        }

        int _Time;

        public int Time
        {
            get { return _Time; }
            set { _Time = value; }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName; }
            set { _SystemName = value; }
        }
        private string _Name;

        public string Name
        {
            get { return _Name == null ? "" : _Name.Trim(); }
            set { _Name = (value == null ? "" : value.Trim()); }
        }

        private string _UserName;

        public string UserName
        {
            get { return _UserName == null ? "" : _UserName.Trim(); }
            set { _UserName = (value == null ? "" : value.Trim()); }
        }

        private string _Date;

        public string Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        private int _TotalTime;

        public int TotalTime
        {
            get { return _TotalTime; }
            set { _TotalTime = value; }
        }


        private string _FromDate;

        public string FromDate
        {
            get { return _FromDate; }
            set { _FromDate = (value == null ? "" : value.Trim()); }
        }

        private string _ToDate;

        public string ToDate
        {
            get { return _ToDate; }
            set { _ToDate = (value == null ? "" : value.Trim()); }
        }



        private int _Status;

        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
    }
}
