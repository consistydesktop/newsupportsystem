﻿namespace Model
{
    public class CustomerCallScreenModel
    {
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _IssueName;

        public string IssueName
        {
            get { return _IssueName == null ? "" : _IssueName.Trim(); }
            set { _IssueName = (value == null ? "" : value.Trim()); }
        }

        private int _CallIssueID;

        public int CallIssueID
        {
            get { return _CallIssueID; }
            set { _CallIssueID = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name == null ? "" : _Name.Trim(); }
            set { _Name = (value == null ? "" : value.Trim()); }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName == null ? "" : _SystemName.Trim(); }
            set { _SystemName = (value == null ? "" : value.Trim()); }
        }

        private int _CallID;

        public int CallID
        {
            get { return _CallID; }
            set { _CallID = value; }
        }

        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }

        private string _MobileNumber;

        public string MobileNumber
        {
            get { return _MobileNumber == null ? "" : _MobileNumber.Trim(); }
            set { _MobileNumber = (value == null ? "" : value.Trim()); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.Trim(); }
            set { _Description = (value == null ? "" : value.Trim()); }
        }

        private System.DateTime _DOC;

        public System.DateTime DOC
        {
            get { return _DOC; }
            set { _DOC = value; }
        }

     

        public string DOCInString
        {
            get
            {
                return _DOC.ToString("dd/MMM/yyyy").Trim();
            }
        }

        private string _TeamAny;

        public string TeamAny
        {
            get { return _TeamAny == null ? "" : _TeamAny.Trim(); }
            set { _TeamAny = (value == null ? "" : value.Trim()); }
        }
    }
}