﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ServiceModel
    {
        int _ServiceID;

        public int ServiceID
        {
            get { return _ServiceID == null ? 0 : _ServiceID; }
            set { _ServiceID = value == null ? 0 : value; }
        }

        string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName; }
            set { _ServiceName = value == null ? "" : value; }
        }
    }
}
