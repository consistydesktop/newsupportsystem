﻿namespace Model
{
    public class LoginModel
    {
        private string _MobileNumber;

        public string MobileNumber
        {
            get { return _MobileNumber == null ? "" : _MobileNumber.Trim(); }
            set { _MobileNumber = (value == null ? "" : value.Trim()); }
        }

        private string _Password;

        public string Password
        {
            get { return _Password == null ? "" : _Password.Trim(); }
            set { _Password = (value == null ? "" : value.Trim()); }
        }

        private string _Captcha;

        public string Captcha
        {
            get { return _Captcha == null ? "" : _Captcha.Trim(); }
            set { _Captcha = (value == null ? "" : value.Trim()); }
        }

        private string _Token;

        public string Token
        {
            get { return _Token == null ? "" : _Token.Trim(); }
            set { _Token = (value == null ? "" : value.Trim()); }
        }

        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _OTP;

        public string OTP
        {
            get { return _OTP == null ? "" : _OTP.Trim(); }
            set { _OTP = (value == null ? "" : value.Trim()); }
        }

        private int _UserTypeID;

        public int UserTypeID
        {
            get { return _UserTypeID; }
            set { _UserTypeID = value; }
        }
    }
}