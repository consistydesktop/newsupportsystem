﻿namespace Model
{
    public class ActionModel
    {
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName == null ? "" : _SystemName.Trim(); }
            set { _SystemName = value == null ? "" : value.Trim(); }
        }

        private string _ResolvedDate;

        public string ResolvedDate
        {
            get { return _ResolvedDate == null ? "" : _ResolvedDate.Trim(); }
            set { _ResolvedDate = value == null ? "" : value.Trim(); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.Trim(); }
            set { _Description = value == null ? "" : value.Trim(); }
        }

        private string _EmployeeRemark;

        public string EmployeeRemark
        {
            get { return _EmployeeRemark == null ? "" : _EmployeeRemark.Trim(); }
            set { _EmployeeRemark = value == null ? "" : value.Trim(); }
        }

        private int _TicketID;

        public int TicketID
        {
            get { return _TicketID; }
            set { _TicketID = value; }
        }

        private string _TicketStatus;

        public string TicketStatus
        {
            get { return _TicketStatus == null ? "" : _TicketStatus.Trim(); }
            set { _TicketStatus = value == null ? "" : value.Trim(); }
        }

        private string _Priority;

        public string Priority
        {
            get { return _Priority == null ? "" : _Priority.Trim(); }
            set { _Priority = value == null ? "" : value.Trim(); }
        }

        public string TicketNo { get; set; }
    }
}