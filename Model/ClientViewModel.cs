﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ClientViewModel
    {
        private string _ClientName;

        public string ClientName
        {
            get { return _ClientName == null ? "" : _ClientName.Trim(); }
            set { _ClientName = (value == null ? "" : value.Trim()); }
        }
    }
}
