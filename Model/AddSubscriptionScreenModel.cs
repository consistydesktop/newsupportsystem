﻿namespace Model
{
    public class AddSubscriptionScreenModel
    {
        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName == null ? "" : _SystemName.Trim(); }
            set { _SystemName = value == null ? "" : value.Trim(); }
        }

        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }

        private int _SubscriptionID;

        public int SubscriptionID
        {
            get { return _SubscriptionID; }
            set { _SubscriptionID = value; }
        }

        private int _DesktopID;

        public int DesktopID
        {
            get { return _DesktopID; }
            set { _DesktopID = value; }
        }

        private string _Desktop;

        public string Desktop
        {
            get { return _Desktop == null ? "" : _Desktop.Trim(); }
            set { _Desktop = value == null ? "" : value.Trim(); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.Trim(); }
            set { _Description = value == null ? "" : value.Trim(); }
        }

        private string _DemoYear;

        public string DemoYear
        {
            get { return _DemoYear == null ? "" : _DemoYear.Trim(); }
            set { _DemoYear = value == null ? "" : value.Trim(); }
        }

        private string _PaymentStatus;

        public string PaymentStatus
        {
            get { return _PaymentStatus == null ? "" : _PaymentStatus.Trim(); }
            set { _PaymentStatus = value == null ? "" : value.Trim(); }
        }

        private System.DateTime _DOC;

        public System.DateTime DOC
        {
            get { return _DOC; }
            set { _DOC = value; }
        }

        public string DOCInString
        {
            get
            {
                return _DOC.ToString("dd/MMM/yyyy").Trim();
            }
        }
    }
}