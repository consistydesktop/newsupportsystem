﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class FinoPerformanceModel
    {

        private string _ClientName;

        public string ClientName
        {
            get { return _ClientName == null ? "" : _ClientName.Trim(); }
            set { _ClientName = value == null ? "" : value.Trim(); }
        }


        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName.Trim(); }
            set { _ServiceName = value == null ? "" : value.Trim(); }
        }



        private decimal _SuccessAmount;

        public decimal SuccessAmount
        {
            get { return _SuccessAmount; }
            set { _SuccessAmount = value; }
        }
        private string _TxnDoc;

        public string TxnDoc
        {
            get { return _TxnDoc == null ? "" : _TxnDoc.Trim(); }
            set { _TxnDoc = value == null ? "" : value.Trim(); }
        }

        private decimal _FailAmount;

        public decimal FailAmount
        {
            get { return _FailAmount; }
            set { _FailAmount = value; }
        }


        private int _FailCount;

        public int FailCount
        {
            get { return _FailCount; }
            set { _FailCount = value; }

        }
        private int _SuccessCount;

        public int SuccessCount
        {
            get { return _SuccessCount; }
            set { _SuccessCount = value; }

        }

    }
}
