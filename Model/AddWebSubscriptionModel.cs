﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class AddWebSubscriptionModel
    {
        public int SystemID { get; set; }

        public string SystemName { get; set; }

        public string WebsiteURL { get; set; }

        public DateTime EndDate { get; set; }

        public string DemoYear { get; set; }

        public string PaymentStatus { get; set; }

        public string Description { get; set; }

        public string HostingIP { get; set; }

        public int SubscriptionID { get; set; }

        public DateTime DOC { get; set; }


        private string _EndDateInString;

        public string EndDateInString
        {
            get { return EndDate.ToString(" dd MMMM yyyy"); }
            set { _EndDateInString = value; }
        }
                  
        public string DOCInString
        {
            get { return DOC.ToString("dddd, dd MMMM yyyy"); }

        }

        private string _ProductName;

        public string ProductName
        {
            get { return _ProductName == null ? "" : _ProductName.Trim(); }
            set { _ProductName = value == null ? "" : value.Trim(); }
        }

        public object Token { get; set; }
    }
}
