﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Employee
    {
        int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        int _CountOfLessThan5Hours;

        public int CountOfLessThan5Hours
        {
            get { return _CountOfLessThan5Hours; }
            set { _CountOfLessThan5Hours = value; }
        }

        int _CountOfAbsentDays;

        public int CountOfAbsentDays
        {
            get { return _CountOfAbsentDays; }
            set { _CountOfAbsentDays = value; }
        }


        public List<string> DatesOfAbsentDays = new List<string>();

        int _Time;

        public int Time
        {
            get { return _Time; }
            set { _Time = value; }
        }

        string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Date;

        public string Date
        {
            get { return _Date; }
            set { _Date = value; }
        }


        public List<string> DatesOfLessThan5Hour = new List<string>();

    }
}
