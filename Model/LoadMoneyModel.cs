﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class LoadMoneyModel
    {
        private decimal _Amount;

        public decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }
        private int _ParentID;

        public int ParentID
        {
            get { return _ParentID; }
            set { _ParentID = value; }
        }

        private string _PaytmTransactionID;

        public string PaytmTransactionID
        {
            get { return _PaytmTransactionID; }
            set { _PaytmTransactionID = value; }
        }
        private string _Month;

        public string Month
        {
            get { return _Month; }
            set { _Month = value; }
        }
        private decimal _OpeningBalance;


        private decimal _NetAmount;

        public decimal NetAmount
        {
            get { return _NetAmount; }
            set { _NetAmount = value; }
        }


        private string _ProviderRequestID;

        public string ProviderRequestID
        {
            get { return _ProviderRequestID != null ? _ProviderRequestID.Trim() : ""; }
            set { _ProviderRequestID = value != null ? value.Trim() : ""; }
        }
        private string _MobileNumber;

        public string MobileNumber
        {
            get { return _MobileNumber != null ? _MobileNumber.Trim() : ""; }
            set { _MobileNumber = value != null ? value.Trim() : ""; }
        }

        private string _IpAddress;

        public string IpAddress
        {
            get { return _IpAddress != null ? _IpAddress.Trim() : ""; }
            set { _IpAddress = value != null ? value.Trim() : ""; }
        }

        private string _Through;

        public string Through
        {
            get { return _Through != null ? _Through.Trim() : ""; }
            set { _Through = value != null ? value.Trim() : ""; }
        }


        private string _Remark;

        public string Remark
        {
            get { return _Remark != null ? _Remark.Trim() : ""; }
            set { _Remark = value != null ? value.Trim() : ""; }
        }
        private string _TransferType;

        public string TransferType
        {
            get { return _TransferType != null ? _TransferType.Trim() : ""; }
            set { _TransferType = value != null ? value.Trim() : ""; }
        }
        private string _RequestType;

        public string RequestType
        {
            get { return _RequestType != null ? _RequestType.Trim() : ""; }
            set { _RequestType = value != null ? value.Trim() : ""; }
        }


        private string _DOC;

        public string DOC
        {
            get { return _DOC != null ? _DOC.Trim() : ""; }
            set { _DOC = value != null ? value.Trim() : ""; }
        }
        private string _Type;

        public string Type
        {
            get { return _Type != null ? _Type.Trim() : ""; }
            set { _Type = value != null ? value.Trim() : ""; }
        }



        private int _RequestID;

        public int RequestID
        {
            get { return _RequestID; }
            set { _RequestID = value; }
        }



        public decimal GST { get; set; }

        public decimal Charge { get; set; }

        public string Paymode { get; set; }


        private int _ProductID;

        public int ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }
     

        private string _DaysMonthType;

        public string DaysMonthType
        {
            get { return _DaysMonthType != null ? _DaysMonthType.Trim() : ""; }
            set { _DaysMonthType = value != null ? value.Trim() : ""; }
        }

    }
}
