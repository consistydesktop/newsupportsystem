﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class KnowledgeBaseModel
    {
        private int _KnowledgeBaseID;

        public int KnowledgeBaseID
        {
            get { return _KnowledgeBaseID; }
            set { _KnowledgeBaseID = value; }
        }
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }
        private string _Issue;

        public string Issue
        {
            get { return _Issue; }
            set { _Issue = value; }
        }
        private string _Resolution;

        public string Resolution
        {
            get { return _Resolution; }
            set { _Resolution = value; }
        }
        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }
        private int _IsActive;

        public int IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }
        private int _SyetemID;

        public int SyetemID
        {
            get { return _SyetemID; }
            set { _SyetemID = value; }
        }
        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName; }
            set { _SystemName = value; }
        }
        private string _FromDate;

        public string FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }
        private string _ToDate;

        public string ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
    }
}
