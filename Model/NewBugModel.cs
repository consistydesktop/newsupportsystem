﻿namespace Model
{
    public class NewBugModel
    {
        private string _ProjectName;

        public string ProjectName
        {
            get { return _ProjectName == null ? "" : _ProjectName.Trim(); }
            set { _ProjectName = (value == null ? "" : value.Trim()); }
        }

        private int _TotalIssue;

        public int TotalIssue
        {
            get { return _TotalIssue; }
            set { _TotalIssue = value; }
        }

        private int _ProjectBugMasterID;

        public int ProjectBugMasterID
        {
            get { return _ProjectBugMasterID; }
            set { _ProjectBugMasterID = value; }
        }

        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _UserName;

        public string UserName
        {
            get { return _UserName == null ? "" : _UserName.Trim(); }
            set { _UserName = (value == null ? "" : value.Trim()); }
        }


        private int _RESOLVED;

        public int RESOLVED
        {
            get { return _RESOLVED; }
            set { _RESOLVED = value; }
        }


        private int _INPROGRESS;

        public int INPROGRESS
        {
            get { return _INPROGRESS; }
            set { _INPROGRESS = value; }
        }

        private int _NEW;

        public int NEW
        {
            get { return _NEW; }
            set { _NEW = value; }
        }

        private int _CONFIRMED;

        public int CONFIRMED
        {
            get { return _CONFIRMED; }
            set { _CONFIRMED = value; }
        }

        private int _CLOSED;

        public int CLOSED
        {
            get { return _CLOSED; }
            set { _CLOSED = value; }
        }

        private int _FEEDBACK;

        public int FEEDBACK
        {
            get { return _FEEDBACK; }
            set { _FEEDBACK = value; }
        }

       
    }
}