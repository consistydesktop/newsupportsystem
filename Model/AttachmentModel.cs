﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class AttachmentModel
    {
        int _FileID;
        public int FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }

        string _FileName;

        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }

        string _FilePath;

        public string FilePath
        {
            get { return _FilePath; }
            set { _FilePath = value; }
        }
    }
}
