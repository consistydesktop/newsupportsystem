﻿namespace Model
{
    public class ViewProjectDetailsForEmployeeModel
    {
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private int _FileID;

        public int FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }

        private int _ProjectDetailMasterID;

        public int ProjectDetailMasterID
        {
            get { return _ProjectDetailMasterID; }
            set { _ProjectDetailMasterID = value; }
        }

        private System.DateTime _DOC;

        public System.DateTime DOC
        {
            get { return _DOC; }
            set { _DOC = value; }
        }

        public string DOCInString
        {
            get
            {
                return _DOC.ToString("dd/MMMM/yyyy").Trim();
            }
        }

        private string _ProjectName;

        public string ProjectName
        {
            get { return _ProjectName == null ? "" : _ProjectName.Trim(); }
            set { _ProjectName = (value == null ? "" : value.Trim()); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.Trim(); }
            set { _Description = (value == null ? "" : value.Trim()); }
        }

        private string _Attachment;

        public string Attachment
        {
            get { return _Attachment == null ? "" : _Attachment.Trim(); }
            set { _Attachment = (value == null ? "" : value.Trim()); }
        }

        private string _UploadType;

        public string UploadType
        {
            get { return _UploadType == null ? "" : _UploadType.Trim(); }
            set { _UploadType = (value == null ? "" : value.Trim()); }
        }

        private string _OriginalFileName;

        public string OriginalFileName
        {
            get { return _OriginalFileName == null ? "" : _OriginalFileName.Trim(); }
            set { _OriginalFileName = (value == null ? "" : value.Trim()); }
        }
    }
}