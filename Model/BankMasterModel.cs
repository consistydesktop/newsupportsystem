﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class BankMasterModel
    {
        private int _BankMasterID;

        public int BankMasterID
        {
            get { return _BankMasterID; }
            set { _BankMasterID = value; }
        }
        private string _BankName;

        public string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }
        private string _AccountNumber;

        public string AccountNumber
        {
            get { return _AccountNumber; }
            set { _AccountNumber = value; }
        }
        private string _BranchName;

        public string BranchName
        {
            get { return _BranchName; }
            set { _BranchName = value; }
        }
        private string _IFSCCode;

        public string IFSCCode
        {
            get { return _IFSCCode; }
            set { _IFSCCode = value; }
        }
    }
}
