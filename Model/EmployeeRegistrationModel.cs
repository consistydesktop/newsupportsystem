﻿using System;
namespace Model
{
    public class EmployeeRegistrationModel
    {
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _FirstName;

        public string FirstName
        {
            get { return _FirstName == null ? "" : _FirstName.Trim(); }
            set { _FirstName = (value == null ? "" : value.Trim()); }
        }

        private string _LastName;

        public string LastName
        {
            get { return _LastName == null ? "" : _LastName.Trim(); }
            set { _LastName = (value == null ? "" : value.Trim()); }
        }

        private string _Username;

        public string Username
        {
            get { return _Username == null ? "" : _Username.Trim(); }
            set { _Username = (value == null ? "" : value.Trim()); }
        }

        private string _EmailID;

        public string EmailID
        {
            get { return _EmailID == null ? "" : _EmailID.Trim(); }
            set { _EmailID = (value == null ? "" : value.Trim()); }
        }

        private string _Password;

        public string Password
        {
            get { return _Password == null ? "" : _Password.Trim(); }
            set { _Password = (value == null ? "" : value.Trim()); }
        }

        private string _Token;

        public string Token
        {
            get { return _Token == null ? "" : _Token.Trim(); }
            set { _Token = (value == null ? "" : value.Trim()); }
        }

        private int _UserTypeID;

        public int UserTypeID
        {
            get { return _UserTypeID; }
            set { _UserTypeID = value; }
        }

        private string _Register;

        public string Register
        {
            get { return _Register == null ? "" : _Register.Trim(); }
            set { _Register = (value == null ? "" : value.Trim()); }
        }

        private string _MobileNumber;

        public string MobileNumber
        {
            get { return _MobileNumber == null ? "" : _MobileNumber.Trim(); }
            set { _MobileNumber = (value == null ? "" : value.Trim()); }
        }


        private DateTime _DateOfJoining;

        public DateTime DateOfJoining
        {
            get { return _DateOfJoining; }
            set { _DateOfJoining = value; }
        }

        public string DateOfJoiningInString
        {
            get { return _DateOfJoining.ToString("dd/MMMM/yyyy"); }
            
        }
        public string DateOfRelivingInString
        {
            get { return _DateOfReliving.ToString("dd/MMMM/yyyy"); }
        }
        private string _JoiningDate;

        public string JoiningDate
        {
            get { return _JoiningDate; }
            set { _JoiningDate = value; }
        }
        
        private string _EmployeeMentStatus;

        public string EmployeeMentStatus
        {
            get { return _EmployeeMentStatus; }
            set { _EmployeeMentStatus = value; }
        }
        private DateTime _DateOfReliving;

        public DateTime DateOfReliving
        {
            get { return _DateOfReliving; }
            set { _DateOfReliving = value; }
        }
        private string _PANNo;

        public string PANNo
        {
            get { return _PANNo; }
            set { _PANNo = value; }
        }
        private string _GSTNo;

        public string GSTNo
        {
            get { return _GSTNo; }
            set { _GSTNo = value; }
        }
        private string _State;

        public string State
        {
            get { return _State; }
            set { _State = value; }
        }
    }
}