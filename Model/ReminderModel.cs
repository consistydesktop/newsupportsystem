﻿namespace Model
{
    public class ReminderModel
    {
        private int _ReminderMasterID;

        public int ReminderMasterID
        {
            get { return _ReminderMasterID; }
            set { _ReminderMasterID = value; }
        }

        private string _CustomerName;

        public string CustomerName
        {
            get { return _CustomerName == null ? "" : _CustomerName.Trim(); }
            set { _CustomerName = (value == null ? "" : value.Trim()); }
        }

        private string _ReminderType;

        public string ReminderType
        {
            get { return _ReminderType == null ? "" : _ReminderType.Trim(); }
            set { _ReminderType = (value == null ? "" : value.Trim()); }
        }

        private string _ReminderDate;

        public string ReminderDate
        {
            get { return _ReminderDate == null ? "" : _ReminderDate.Trim(); }
            set { _ReminderDate = (value == null ? "" : value.Trim()); }
        }

        private double _Amount;

        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.Trim(); }
            set { _Description = (value == null ? "" : value.Trim()); }
        }

        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _UserName;

        public string UserName
        {
            get { return _UserName == null ? "" : _UserName.Trim(); }
            set { _UserName = (value == null ? "" : value.Trim()); }
        }

        private string _FormDate;

        public string FormDate
        {
            get { return _FormDate == null ? "" : _FormDate.Trim(); }
            set { _FormDate = (value == null ? "" : value.Trim()); }
        }

        private string _Todate;

        public string Todate
        {
            get { return _Todate == null ? "" : _Todate.Trim(); }
            set { _Todate = (value == null ? "" : value.Trim()); }
        }

        private string _Status;

        public string Status
        {
            get { return _Status == null ? "" : _Status.Trim(); }
            set { _Status = (value == null ? "" : value.Trim()); }
        }

        private int _ReminderTypeID;

        public int ReminderTypeID
        {
            get { return _ReminderTypeID; }
            set { _ReminderTypeID = value; }
        }

        private string _TypeName;

        public string TypeName
        {
            get { return _TypeName == null ? "" : _TypeName.Trim(); }
            set { _TypeName = (value == null ? "" : value.Trim()); }
        }
    }
}