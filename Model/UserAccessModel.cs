﻿namespace Model
{
    public class UserAccessModel
    {
        private string _Pagename;

        public string Pagename
        {
            get { return _Pagename == null ? "" : _Pagename.Trim(); }
            set { _Pagename = (value == null ? "" : value.Trim()); }
        }

        private int _UserTypeID;

        public int UserTypeID
        {
            get { return _UserTypeID; }
            set { _UserTypeID = value; }
        }
    }
}