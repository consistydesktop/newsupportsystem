﻿namespace Model
{
    public class AddTicketForCustomerModel
    {
        private int _ServiceID;

        public int ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        private int _DeveloperID;

        public int DeveloperID
        {
            get { return _DeveloperID; }
            set { _DeveloperID = value; }
        }

        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }

        private int _TicketID;

        public int TicketID
        {
            get { return _TicketID; }
            set { _TicketID = value; }
        }

        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName == null ? "" : _SystemName.Trim(); }
            set { _SystemName = (value == null ? "" : value.Trim()); }
        }

        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName.Trim(); }
            set { _ServiceName = (value == null ? "" : value.Trim()); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.Trim(); }
            set { _Description = (value == null ? "" : value.Trim()); }
        }

        private string _TeamAny;

        public string TeamAny
        {
            get { return _TeamAny == null ? "" : _TeamAny.Trim(); }
            set { _TeamAny = (value == null ? "" : value.Trim()); }
        }

        private string _Attachment;

        public string Attachment
        {
            get { return _Attachment == null ? "" : _Attachment.Trim(); }
            set { _Attachment = (value == null ? "" : value.Trim()); }
        }

        private string _Priority;

        public string Priority
        {
            get { return _Priority == null ? "" : _Priority.Trim(); }
            set { _Priority = (value == null ? "" : value.Trim()); }
        }

        private int _UserId;

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        private string _TicketNo;

        public string TicketNo
        {
            get { return _TicketNo == null ? "" : _TicketNo.Trim(); }
            set { _TicketNo = (value == null ? "" : value.Trim()); }
        }

        private string _RequestType;

        public string RequestType
        {
            get { return _RequestType == null ? "" : _RequestType.Trim(); }
            set { _RequestType = (value == null ? "" : value.Trim()); }
        }

        private int _FileID;

        public int FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }

        private string _UploadType;

        public string UploadType
        {
            get { return _UploadType == null ? "" : _UploadType.Trim(); }
            set { _UploadType = (value == null ? "" : value.Trim()); }
        }
    }
}