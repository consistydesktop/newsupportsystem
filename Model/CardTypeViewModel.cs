﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class CardTypeViewModel
    {
        public int CardTypeID { get; set; }

        private string _CardType;

        public string CardType
        {
            get { return _CardType == null ? "" : _CardType.Trim(); }
            set { _CardType = (value == null ? "" : value); }
        }

        
    }
}
