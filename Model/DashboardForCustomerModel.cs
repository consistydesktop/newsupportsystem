﻿namespace Model
{
    public class DashboardForCustomerModel
    {
        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName.Trim(); }
            set { _ServiceName = (value == null ? "" : value.Trim()); }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName; }
            set { _SystemName = (value == null ? "" : value.Trim()); }
        }

        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }
    }
}