﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class FinovativeVersionHistoryModel
    {

        private int _FinovativeVersionId;

        public int FinovativeVersionId
        {
            get { return FinovativeVersionId; }
            set { FinovativeVersionId = value; }
        }
        private string _Title;

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        private string _Description;

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

         private string _Version;

        public string Version
        {
            get { return _Version != null ? _Version.Trim() : ""; }
            set { _Version = value != null ? value.Trim() : ""; }
        }

        public DateTime Date { get; set; }
        public int FinnovativeVersionHistoryID { get; set; }
    }
}
