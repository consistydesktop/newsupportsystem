﻿namespace Model
{
    public class ViewTimeSheetForEmployeeModel
    {
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name == null ? "" : _Name.Trim(); }
            set { _Name = (value == null ? "" : value.Trim()); }
        }

        private int _TimeSheetMasterID;

        public int TimeSheetMasterID
        {
            get { return _TimeSheetMasterID; }
            set { _TimeSheetMasterID = value; }
        }

        private int _SubTimeSheetMasterID;

        public int SubTimeSheetMasterID
        {
            get { return _SubTimeSheetMasterID; }
            set { _SubTimeSheetMasterID = value; }
        }

        private int _Time;

        public int Time
        {
            get { return _Time; }
            set { _Time = value; }
        }

        private string _Date;

        public string Date
        {
            get { return _Date == null ? "" : _Date.Trim(); }
            set { _Date = (value == null ? "" : value.Trim()); }
        }

        private string _Task;

        public string Task
        {
            get { return _Task == null ? "" : _Task.Trim(); }
            set { _Task = (value == null ? "" : value.Trim()); }
        }

        private string _DOC;

        public string DOC
        {
            get { return (_DOC == null ? "" : _DOC.Trim()); }
            set { _DOC = (value == null ? "" : value.Trim()); }
        }

        private string _FormDate;

        public string FormDate
        {
            get { return (_FormDate == null ? "" : _FormDate.Trim()); }
            set { _FormDate = (value == null ? "" : value.Trim()); }
        }

        private string _Todate;

        public string Todate
        {
            get { return (_Todate == null ? "" : _Todate.Trim()); }
            set { _Todate = (value == null ? "" : value.Trim()); }
        }

        private string _DeveloperName;

        public string DeveloperName
        {
            get { return (_DeveloperName == null ? "" : _DeveloperName.Trim()); }
            set { _DeveloperName = (value == null ? "" : value.Trim()); }
        }
    }
}