﻿namespace Model
{
    public class NotificationModel
    {
        private int _NotificationID;

        public int NotificationID
        {
            get { return _NotificationID; }
            set { _NotificationID = value; }
        }

        private string _Notification;

        public string Notification
        {
            get { return _Notification == null ? "" : _Notification.Trim(); }
            set { _Notification = (value == null ? "" : value.Trim()); }
        }

        private System.DateTime _DOC;

        public System.DateTime DOC
        {
            get { return _DOC; }
            set { _DOC = value; }
        }

        public string DOCInString
        {
            get
            {
                return _DOC.ToString("dd/MMMM/yyyy").Trim();
            }
        }
    }
}