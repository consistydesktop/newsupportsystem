﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
   public  class WebSubscriptionResponseModel
    {
        string _Status;

        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        DateTime _EndDate;

        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _Message;

        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        int _RemainingDays;

        public int RemainingDays
        {
            get { return _RemainingDays; }
            set { _RemainingDays = value; }
        }
    }
}
