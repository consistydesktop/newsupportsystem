﻿namespace Model
{
    public class UserInformationModel
    {
        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private int _UserTypeID;

        public int UserTypeID
        {
            get { return _UserTypeID; }
            set { _UserTypeID = value; }
        }
        


            private string _LoginToken;

        public string LoginToken
        {
            get { return _LoginToken == null ? "" : _LoginToken.Trim(); }
            set { _LoginToken = (value == null ? "" : value.Trim()); }
        }
        private string _Name;

        public string Name
        {
            get { return _Name == null ? "" : _Name.Trim(); }
            set { _Name = (value == null ? "" : value.Trim()); }
        }

        private string _Username;

        public string Username
        {
            get { return _Username == null ? "" : _Username.Trim(); }
            set { _Username = (value == null ? "" : value.Trim()); }
        }

        private string _EmailID;

        public string EmailID
        {
            get { return _EmailID == null ? "" : _EmailID.Trim(); }
            set { _EmailID = (value == null ? "" : value.Trim()); }
        }

        private string _MobileNumber;

        public string MobileNumber
        {
            get { return _MobileNumber == null ? "" : _MobileNumber.Trim(); }
            set { _MobileNumber = (value == null ? "" : value.Trim()); }
        }

        private string _UserType;

        public string UserType
        {
            get { return _UserType == null ? "" : _UserType.Trim(); }
            set { _UserType = (value == null ? "" : value.Trim()); }
        }

        private string _URL;

        public string URL
        {
            get { return _URL == null ? "" : _URL.Trim(); }
            set { _URL = value == null ? "" : value.Trim(); }
        }

        public string GSTNo { get; set; }

        public string PANNo { get; set; }

        public string Address { get; set; }

        public string State { get; set; }

        public int IsActive { get; set; }
    }
}