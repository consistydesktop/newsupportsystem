﻿namespace Model
{
    public class DashboardForEmployeeModel
    {
        private string _TicketNo;

        public string TicketNo
        {
            get { return _TicketNo; }
            set { _TicketNo = (value == null ? "" : value.Trim()); }
        }

        private string _ProjectName;

        public string ProjectName
        {
            get { return _ProjectName; }
            set { _ProjectName = (value == null ? "" : value.Trim()); }
        }

        private int _TicketID;

        public int TicketID
        {
            get { return _TicketID; }
            set { _TicketID = value; }
        }

        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value; }
        }

        private int _CallID;

        public int CallID
        {
            get { return _CallID; }
            set { _CallID = value; }
        }

        private int _ProjectDetailMasterID;

        public int ProjectDetailMasterID
        {
            get { return _ProjectDetailMasterID; }
            set { _ProjectDetailMasterID = value; }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName; }
            set { _SystemName = (value == null ? "" : value.Trim()); }
        }

        private string _PendingTickets;

        public string PendingTickets
        {
            get { return _PendingTickets; }
            set { _PendingTickets = (value == null ? "" : value.Trim()); }
        }

        private string _ResolvedTickets;

        public string ResolvedTickets
        {
            get { return _ResolvedTickets; }
            set { _ResolvedTickets = (value == null ? "" : value.Trim()); }
        }

        private System.DateTime _DOC;

        public System.DateTime DOC
        {
            get { return _DOC; }
            set { _DOC = value; }
        }

        public string DOCInString
        {
            get
            {
                return _DOC.ToString("dd/MMMM/yyyy").Trim();
            }
        }

        private string _MobileNumber;

        public string MobileNumber
        {
            get { return _MobileNumber; }
            set { _MobileNumber = (value == null ? "" : value.Trim()); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description; }
            set { _Description = (value == null ? "" : value.Trim()); }
        }

        private string _TeamAny;

        public string TeamAny
        {
            get { return _TeamAny; }
            set { _TeamAny = (value == null ? "" : value.Trim()); }
        }

        private string _IssueName;

        public string IssueName
        {
            get { return _IssueName; }
            set { _IssueName = (value == null ? "" : value.Trim()); }
        }

        private int _CallIssueID;

        public int CallIssueID
        {
            get { return _CallIssueID; }
            set { _CallIssueID = value; }
        }

        private int _TestingIssueID;

        public int TestingIssueID
        {
            get { return _TestingIssueID; }
            set { _TestingIssueID = value; }
        }

        private string _BugTitle;

        public string BugTitle
        {
            get { return _BugTitle; }
            set { _BugTitle = (value == null ? "" : value.Trim()); }
        }

        private string _ExpectedResult;

        public string ExpectedResult
        {
            get { return _ExpectedResult; }
            set { _ExpectedResult = (value == null ? "" : value.Trim()); }
        }

        private string _PageURL;

        public string PageURL
        {
            get { return _PageURL; }
            set { _PageURL = (value == null ? "" : value.Trim()); }
        }

        private string _OriginalFileName;

        public string OriginalFileName
        {
            get { return _OriginalFileName; }
            set { _OriginalFileName = value; }
        }

        private string _Priority;

        public string Priority
        {
            get { return _Priority; }
            set { _Priority = (value == null ? "" : value.Trim()); }
        }

        private string _Attachment;

        public string Attachment
        {
            get { return _Attachment; }
            set { _Attachment = (value == null ? "" : value.Trim()); }
        }

        private int _BugID;

        public int BugID
        {
            get { return _BugID; }
            set { _BugID = value; }
        }

        private int _TaskID;

        public int TaskID
        {
            get { return _TaskID; }
            set { _TaskID = value; }
        }

        private int _FileID;

        public int FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }

        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _Task;

        public string Task
        {
            get { return _Task; }
            set { _Task = (value == null ? "" : value.Trim()); }
        }

        private string _Status;

        public string Status
        {
            get { return _Status == null ? "" : _Status.Trim(); }
            set { _Status = (value == null ? "" : value.Trim()); }
        }

        private string _Deadline;

        public string Deadline
        {
            get { return _Deadline == null ? "" : _Deadline.Trim(); }
            set { _Deadline = (value == null ? "" : value.Trim()); }
        }

        private string _UploadType;

        public string UploadType
        {
            get { return _UploadType; }
            set { _UploadType = (value == null ? "" : value.Trim()); }
        }


        int _TaskStatus;


        public int TaskStatus
        {
            get { return _TaskStatus; }
            set { _TaskStatus = value; }
        }
    }
}