﻿namespace Model
{
    public class AssignTaskModel
    {
        private int _TaskID;

        public int TaskID
        {
            get { return _TaskID; }
            set { _TaskID = value; }
        }

        private string _Task;

        public string Task
        {
            get { return _Task == null ? "" : _Task.ToString().Trim(); }
            set { _Task = value == null ? "" : value.Trim(); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description == null ? "" : _Description.ToString().Trim(); }
            set { _Description = value == null ? "" : value.Trim(); }
        }

        private string _Deadline;

        public string Deadline
        {
            get { return _Deadline == null ? "" : _Deadline.ToString().Trim(); }
            set { _Deadline = value == null ? "" : value.Trim(); }
        }

        private string _DOC;

        public string DOC
        {
            get { return _DOC == null ? "" : _DOC.ToString().Trim(); }
            set { _DOC = value == null ? "" : value.Trim(); }
        }

        private int _Point;

        public int Point
        {
            get { return _Point; }
            set { _Point = value; }
        }

        private int _UserID;

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name == null ? "" : _Name.ToString().Trim(); }
            set { _Name = value == null ? "" : value.Trim(); }
        }

        private string _Attachment;

        public string Attachment
        {
            get { return _Attachment == null ? "" : _Attachment.ToString().Trim(); }
            set { _Attachment = value == null ? "" : value.Trim(); }
        }

        private string _OriginalFileName;

        public string OriginalFileName
        {
            get { return _OriginalFileName == null ? "" : _OriginalFileName.ToString().Trim(); }
            set { _OriginalFileName = value == null ? "" : value.Trim(); }
        }

        private int _AssignedUserID;

        public int AssignedUserID
        {
            get { return _AssignedUserID; }
            set { _AssignedUserID = value; }
        }

        private string _TaskStatus;

        public string TaskStatus
        {
            get { return _TaskStatus; }
            set { _TaskStatus = value; }
        }

        private int _TaskDetailMasterID;

        public int TaskDetailMasterID
        {
            get { return _TaskDetailMasterID; }
            set { _TaskDetailMasterID = value; }
        }

        private string _SubTask;

        public string SubTask
        {
            get { return _SubTask == null ? "" : _SubTask.ToString().Trim(); }
            set { _SubTask = value == null ? "" : value.Trim(); }
        }

        private int _SubTaskStatus;

        public int SubTaskStatus
        {
            get { return _SubTaskStatus; }
            set { _SubTaskStatus = value; }
        }

        private string _fileIDs;

        public string FileIDs
        {
            get { return _fileIDs == null ? "" : _fileIDs.ToString().Trim(); }
            set { _fileIDs = value == null ? "" : value.Trim(); }
        }

        private int _FileID;

        public int FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }

        private string _UploadType;

        public string UploadType
        {
            get { return _UploadType == null ? "" : _UploadType.ToString().Trim(); }
            set { _UploadType = value == null ? "" : value.Trim(); }
        }

        private string _Comment;

        public string Comment
        {
            get { return _Comment == null ? "" : _Comment.ToString().Trim(); }
            set { _Comment = value == null ? "" : value.Trim(); }
        }

        private int _CommentID;

        public int CommentID
        {
            get { return _CommentID; }
            set { _CommentID = value; }
        }

        private int _SubTaskID;

        public int SubTaskID
        {
            get { return _SubTaskID; }
            set { _SubTaskID = value; }
        }

        private string _CommentType;

        public string CommentType
        {
            get { return _CommentType == null ? "" : _CommentType.ToString().Trim(); }
            set { _CommentType = value == null ? "" : value.Trim(); }
        }

        private string _EmployeeNames;

        public string EmployeeNames
        {
            get { return _EmployeeNames == null ? "" : _EmployeeNames.ToString().Trim(); }
            set { _EmployeeNames = value == null ? "" : value.Trim(); }
        }

        private int _SystemID;

        public int SystemID
        {
            get { return _SystemID; }
            set { _SystemID = value == null ? 0 : value; }
        }

        private string _SystemName;

        public string SystemName
        {
            get { return _SystemName; }
            set { _SystemName = value ; }
        }
       
    }
}