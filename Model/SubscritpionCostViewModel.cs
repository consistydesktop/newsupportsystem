﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class SubscritpionCostViewModel
    {
        public decimal Amount { get; set; }


        private string _Month;

        public string Month
        {
            get { return _Month == null ? "" : _Month.Trim(); }
            set { _Month = (value == null ? "" : value.Trim()); }
        }
        private string _DaysMonthType;

        public string DaysMonthType
        {
            get { return _DaysMonthType == null ? "" : _DaysMonthType.Trim(); }
            set { _DaysMonthType = (value == null ? "" : value.Trim()); }
        }
  
        public int DaysMonth { get; set; }
    }
}
