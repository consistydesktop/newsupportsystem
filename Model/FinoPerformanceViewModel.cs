﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
   public class FinoPerformanceViewModel
    {
        public int FinnoPerformanceID { get; set; }
        private string _ClientName;

        public string ClientName
        {
            get { return _ClientName == null ? "" : _ClientName.Trim(); }
            set { _ClientName = value == null ? "" : value.Trim(); }
        }


        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName == null ? "" : _ServiceName.Trim(); }
            set { _ServiceName = value == null ? "" : value.Trim(); }
        }

     

        private string _DOC;

        public string DOC
        {
            get { return _DOC == null ? "" : _DOC.Trim(); }
            set { _DOC = value == null ? "" : value.Trim(); }
        }
        private string _FormDate;

        public string FormDate
        {
            get { return _FormDate == null ? "" : _FormDate.Trim(); }
            set { _FormDate = (value == null ? "" : value.Trim()); }
        }

        private string _Todate;


        public string Todate
        {
            get { return _Todate == null ? "" : _Todate.Trim(); }
            set { _Todate = (value == null ? "" : value.Trim()); }
        }

        private string _SuccessAmount;


        public string SuccessAmount
        {
            get { return _SuccessAmount == null ? "" : _SuccessAmount.Trim(); }
            set { _SuccessAmount = (value == null ? "" : value.Trim()); }
        }
        private string _FailAmount;


        public string FailAmount
        {
            get { return _FailAmount == null ? "" : _FailAmount.Trim(); }
            set { _FailAmount = (value == null ? "" : value.Trim()); }
        }
        private string _SalesType;


        public string SalesType
        {
            get { return _SalesType == null ? "" : _SalesType.Trim(); }
            set { _SalesType = (value == null ? "" : value.Trim()); }
        }
        

        public int SuccessCount { get; set; }
        public int FailCount { get; set; }
    }
}
