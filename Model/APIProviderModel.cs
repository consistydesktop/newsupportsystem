﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
   public class APIProviderModel
    {
        public int ProviderID { get; set; }
        private string _URL;

        public string URL
        {
            get { return _URL == null ? "" : _URL.Trim(); }
            set { _URL = value == null ? "" : value.Trim(); }
        }
        private string _ProviderName;

        public string ProviderName
        {
            get { return _ProviderName == null ? "" : _ProviderName.Trim(); }
            set { _ProviderName = value == null ? "" : value.Trim(); }
        }
        private string _DOC;

        public string DOC
        {
            get { return _DOC == null ? "" : _DOC.Trim(); }
            set { _DOC = value == null ? "" : value.Trim(); }
        }
    }
}
