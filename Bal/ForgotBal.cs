﻿using Dal;
using Model;
using System.Data;

namespace Bal
{
    public class ForgotBal
    {
        private ForgotDal objDAL = new ForgotDal();

        public DataTable ForgotPassword(ForgotModel objModel)
        {
            return objDAL.ForgotPassword(objModel);
        }
    }
}