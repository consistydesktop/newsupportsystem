﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
   
    public class KnowledgeBaseBal
    {
        private KnowledgeBaseDal ObjDAL = new KnowledgeBaseDal();

        public List<KnowledgeBaseModel> SelectSystemName()
        {
            return ObjDAL.SelectSystemName();
        }
        public int  Insert(KnowledgeBaseModel ObjModel)
        {
            return ObjDAL.Insert(ObjModel);
        }
        public List<List<KnowledgeBaseModel>> SelectByID(int KnowledgeBaseID)
        {
            return ObjDAL.SelectByID(KnowledgeBaseID);
        }
        public int UpdateByID(KnowledgeBaseModel ObjModel)
        {
            return ObjDAL.UpdateByID(ObjModel);
        }
    
    }
}
