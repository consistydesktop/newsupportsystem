﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{

    public class FinovativeVersionHistoryBal
    {
        private FinovativeVersionHistoryDal ObjDAL = new FinovativeVersionHistoryDal();
        public int InsertFinovativeVersionHistory(FinovativeVersionHistoryModel ObjModel)
        {
            return ObjDAL.InsertFinovativeVersionHistory(ObjModel);
        }

        public DataTable selectVersionHistoryReport()
        {
            return ObjDAL.selectVersionHistoryReport();
        }

        public int Delete(int FinnovativeVersionHistoryID)
        {
            return ObjDAL.Delete(FinnovativeVersionHistoryID);
        }

        public VersionHistoryReport SelectByID(int FinnovativeVersionHistoryID)
        {
            return ObjDAL.SelectByID(FinnovativeVersionHistoryID);
        }

        public int Update(FinovativeVersionHistoryModel objModel)
        {
            return ObjDAL.Update(objModel);
        }
    }
}
