﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class AddSubscriptionScreenBal
    {
        private AddSubscriptionScreenDal objDal = new AddSubscriptionScreenDal();

        public List<AddSubscriptionScreenModel> SelectSystemName()
        {
            return objDal.SelectSystemName();
        }

        public List<AddSubscriptionScreenModel> SelectDesktop()
        {
            return objDal.SelectDesktop();
        }

        public int Insert(AddSubscriptionScreenModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public List<AddSubscriptionScreenModel> Select()
        {
            return objDal.Select();
        }

        public AddSubscriptionScreenModel SelectByID(int SubscriptionID)
        {
            return objDal.SelectByID(SubscriptionID);
        }

        public int Update(AddSubscriptionScreenModel ObjModel)
        {
            return objDal.Update(ObjModel);
        }

        public int Delete(int SubscriptionID)
        {
            return objDal.Delete(SubscriptionID);
        }
    }
}