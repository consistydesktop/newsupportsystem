﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class APIProviderBal
    {
        private APIProviderDal ObjDAL = new APIProviderDal();
        public DataTable SelectAPIProviderReport()
        {

            return ObjDAL.SelectAPIProviderReport();
        }
        public int Insert(APIProviderModel ObjModel)
        {
            return ObjDAL.Insert(ObjModel);
        }

        public int Delete(int ProviderID)
        {
            return ObjDAL.Delete(ProviderID);
        }

        public APIProviderModel SelectByID(int ProviderID)
        {
            return ObjDAL.SelectByID(ProviderID);
        }

        public int Update(APIProviderModel objModel)
        {
            return ObjDAL.Update(objModel);
        }
      
    }
}
