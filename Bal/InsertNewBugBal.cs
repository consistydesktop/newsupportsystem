﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;

namespace Bal
{
    public class InsertNewBugBal
    {
        private InsertNewBugDal objDal = new InsertNewBugDal();

        public List<InsertNewBugModel> SelectSystemName()
        {
            return objDal.SelectSystemName();
        }

        public List<InsertNewBugModel> SelectEmployeeName()
        {
            return objDal.SelectEmployeeName();
        }

        public List<InsertNewBugModel> SelectIssue()
        {
            return objDal.SelectIssue();
        }

        public int Insert(InsertNewBugModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public List<InsertNewBugModel> Select(int ProjectBugMasterID,string FromDate, string ToDate, string Status,string searchtype)
        {
            return objDal.Select(ProjectBugMasterID ,FromDate, ToDate, Status, searchtype);
        }

        public InsertNewBugModel SelectByID(int BugID)
        {
            return objDal.SelectByID(BugID);
        }

        public int Update(InsertNewBugModel ObjModel)
        {
            return objDal.Update(ObjModel);
        }

        public int Delete(int BugID)
        {
            return objDal.Delete(BugID);
        }

        public DataTable FileInsert(InsertNewBugModel objModel)
        {
            return objDal.FileInsert(objModel);
        }

        public int FileMappingInsert(InsertNewBugModel objModel)
        {
            return objDal.FileMappingInsert(objModel);
        }

        public List<InsertNewBugModel> SelectAttachment(InsertNewBugModel ObjModel)
        {
            return objDal.SelectAttachment(ObjModel);
        }

        public int Exist(string File, int ProjectBugMasterID)
        {
            return objDal.Exist(File, ProjectBugMasterID);
        }



        public string SelectProjectName(int ProjectBugMasterID)
        {
            return objDal.SelectProjectName(ProjectBugMasterID);
        }

        public List<List<InsertNewBugModel>> Search(string fromDate, string toDate)
        {
            throw new NotImplementedException();
        }
    }
}