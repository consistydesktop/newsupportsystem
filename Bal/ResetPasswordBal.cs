﻿using Dal;
using Model;

namespace Bal
{
    public class ResetPasswordBal
    {
        private ResetPasswordDal objDal = new ResetPasswordDal();

        public int Update(ResetPasswordModel ObjModel)
        {
            return objDal.Update(ObjModel);
        }
    }
}