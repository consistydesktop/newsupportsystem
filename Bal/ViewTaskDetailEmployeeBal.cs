﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class ViewTaskDetailEmployeeBal
    {
        private ViewTaskDetailEmployeeDal ObjDAL = new ViewTaskDetailEmployeeDal();

        public List<AssignTaskModel> SelectEmployeeName()
        {
            return ObjDAL.SelectEmployeeName();
        }

        public List<List<AssignTaskModel>> SelectByID(int TaskID)
        {
            return ObjDAL.SelectByID(TaskID);
        }

        public List<AssignTaskModel> SelectAttachment(int TaskID)
        {
            return ObjDAL.SelectAttachment(TaskID);
        }

        public int InsertComment(AssignTaskModel ObjModel)
        {
            return ObjDAL.InsertComment(ObjModel);
        }

        public int UpdateSubTaskStatus(AssignTaskModel ObjModel)
        {
            return ObjDAL.UpdateSubTaskStatus(ObjModel);
        }

        public int UpdateTaskStatus(AssignTaskModel ObjModel)
        {
            return ObjDAL.UpdateTaskStatus(ObjModel);
        }
    }
}