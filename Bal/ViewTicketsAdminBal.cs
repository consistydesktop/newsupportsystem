﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class ViewTicketsAdminBal
    {
        private ViewTicketsAdminDal objDal = new ViewTicketsAdminDal();

        public List<ViewTicketsAdminModel> Select()
        {
            return objDal.Select();
        }

        public List<ViewTicketsAdminModel> Search(string FromDate, string ToDate)
        {
            return objDal.Search(FromDate, ToDate);
        }

        public List<ViewTicketsAdminModel> SelectByTicketID(int TicketID)
        {
            return objDal.SelectByTicketID(TicketID);
        }

        public List<ViewTicketsAdminModel> SelectEmployeeName()
        {
            return objDal.SelectEmployeeName();
        }

        public List<ViewTicketsAdminModel> SelectServiceName()
        {
            return objDal.SelectServiceName();
        }

        public List<ViewTicketsAdminModel> SelectSystemName()
        {
            return objDal.SelectSystemName();
        }

      
        public List<ViewTicketsAdminModel> SearchByFilter(string FromDate, string ToDate, int DeveloperID, int SystemID, int ServiceID, string TicketNo)
        {
            return objDal.SearchByFilter(FromDate, ToDate, DeveloperID, SystemID, ServiceID, TicketNo);
        }
    }
}