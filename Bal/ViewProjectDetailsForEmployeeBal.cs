﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class ViewProjectDetailsForEmployeeBal
    {
        private ViewProjectDetailsForEmployeeDal objDal = new ViewProjectDetailsForEmployeeDal();

        public List<ViewProjectDetailsForEmployeeModel> Select(int UserID)
        {
            return objDal.Select(UserID);
        }

        public List<ViewProjectDetailsForEmployeeModel> SelectByID(int UserID, int ProjectDetailMasterID)
        {
            return objDal.SelectByID(UserID, ProjectDetailMasterID);
        }

        public List<ViewProjectDetailsForEmployeeModel> Search(string FromDate, string ToDate, int UserID)
        {
            return objDal.Search(FromDate, ToDate, UserID);
        }

        public List<ViewProjectDetailsForEmployeeModel> SelectAttachment(ViewProjectDetailsForEmployeeModel ObjModel)
        {
            return objDal.SelectAttachment(ObjModel);
        }
    }
}