﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
   public  class LeaveRequestReportbal
    {
       private LeaveRequestReportDal ObjDAL = new LeaveRequestReportDal();
       


        public List<LeaveRequestModel> SelectByDate(string FromDate, string ToDate,int UserID)
        {
            return ObjDAL.SelectByDate(FromDate, ToDate, UserID);
        }

        public int LeaveRequestApprove(int LeaveID, int IsApprove,int ApprovedBy)
        {
            return ObjDAL.LeaveRequestApprove(LeaveID, IsApprove, ApprovedBy);
        }



        public LeaveRequestModel SelectByID(int LeaveID)
        {
            return ObjDAL.SelectByID(LeaveID);
        }
    }
}
