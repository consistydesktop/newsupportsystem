﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
  public  class FinnoPerformanceBal
    {
        private FinnoPerformanceDal ObjDAL = new FinnoPerformanceDal();
       
        public int Insert(FinoPerformanceModel ObjModel)
        {
            return ObjDAL.Insert(ObjModel);
        }

        public DataTable Search(FinoPerformanceViewModel objModel)
        {
            return ObjDAL.Search(objModel);
        }

        public List<ClientViewModel> ClientSelect()
        {
            return ObjDAL.ClientSelect();
        }
    }
}
