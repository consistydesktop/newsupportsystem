﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class ViewAssignedProjectDetailsBal
    {
        private ViewAssignedProjectDetailsDal objDal = new ViewAssignedProjectDetailsDal();

        public List<ViewAssignedProjectDetailsModel> Select()
        {
            return objDal.Select();
        }

        public List<ViewAssignedProjectDetailsModel> Search(ViewAssignedProjectDetailsModel ObjModel)
        {
            return objDal.Search(ObjModel);
        }

        public List<ViewAssignedProjectDetailsModel> SelectEmployeeName()
        {
            return objDal.SelectEmployeeName();
        }

        public List<ViewAssignedProjectDetailsModel> SelectEmployeeByUserID(int UserID)
        {
            return objDal.SelectEmployeeByUserID(UserID);
        }

        public List<ViewAssignedProjectDetailsModel> SelectByDate(string FromDate, string ToDate)
        {
            return objDal.SelectByDate(FromDate, ToDate);
        }

        public int Delete(int ProjectDetailMasterID)
        {
            return objDal.Delete(ProjectDetailMasterID);
        }

        public int Update(ViewAssignedProjectDetailsModel ObjModel)
        {
            return objDal.Update(ObjModel);
        }

        public List<ViewAssignedProjectDetailsModel> SelectEmployee(int ProjectDetailMasterID)
        {
            return objDal.SelectEmployee(ProjectDetailMasterID);
        }

        public List<ViewAssignedProjectDetailsModel> SelectByProjectDetailMasterID(ViewAssignedProjectDetailsModel ObjModel)
        {
            return objDal.SelectByProjectDetailMasterID(ObjModel);
        }

        public int DeleteFile(int FileID)
        {
            return objDal.DeleteFile(FileID);
        }
    }
}