﻿using Dal;
using Model;
using System.Collections.Generic;
using System.Data;

namespace Bal
{
    public class DashboardForEmployeeBal
    {
        private DashboardForEmployeeDal objDal = new DashboardForEmployeeDal();

        public List<DashboardForEmployeeModel> SelectTicketDetails(int UserID)
        {
            return objDal.SelectTicketDetails(UserID);
        }

        public List<DashboardForEmployeeModel> SelectProjectDetails(int UserID)
        {
            return objDal.SelectProjectDetails(UserID);
        }

        public int SelectPendingProjects(int UserID)
        {
            return objDal.SelectPendingProjects(UserID);
        }

        public int SelectPendingCalls(int UserID)
        {
            return objDal.SelectPendingCalls(UserID);
        }

        public int SelectPendingTickets(int UserID)
        {
            return objDal.SelectPendingTickets(UserID);
        }

        public int SelectPendingTasks(int UserID)
        {
            return objDal.SelectPendingTasks(UserID);
        }

        public int SelectPendingBugs(int UserID)
        {
            return objDal.SelectPendingBugs(UserID);
        }

        public List<DashboardForEmployeeModel> SelectCallDetails(int UserID)
        {
            return objDal.SelectCallDetails(UserID);
        }

        public List<DashboardForEmployeeModel> SelectBugDetails(int UserID)
        {
            return objDal.SelectBugDetails(UserID);
        }

        public List<DashboardForEmployeeModel> SelectTaskDetails(int UserID)
        {
            return objDal.SelectTaskDetails(UserID);
        }

        public int UpdateStatus(DashboardForEmployeeModel ObjModel)
        {
            return objDal.UpdateStatus(ObjModel);
        }

        public List<DashboardForEmployeeModel> SelectByTaskID(DashboardForEmployeeModel ObjModel)
        {
            return objDal.SelectByTaskID(ObjModel);
        }

        public DataTable FileMappingInsert(DashboardForEmployeeModel objModel)
        {
            return objDal.FileMappingInsert(objModel);
        }

        public string SelectDescriptionByTaskID(DashboardForEmployeeModel ObjModel)
        {
            return objDal.SelectDescriptionByTaskID(ObjModel);
        }
        
    }
}