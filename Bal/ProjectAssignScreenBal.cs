﻿using Dal;
using Model;
using System.Collections.Generic;
using System.Data;

namespace Bal
{
    public class ProjectAssignScreenBal
    {
        private ProjectAssignScreenDal objDal = new ProjectAssignScreenDal();

        public List<ProjectAssignScreenModel> SelectServiceName()
        {
            return objDal.SelectServiceName();
        }

        public List<ProjectAssignScreenModel> SelectEmployeeName()
        {
            return objDal.SelectEmployeeName();
        }

        public List<ProjectAssignScreenModel> SelectProductName()
        {
            return objDal.SelectProductName();
        }

        public int Insert(List<ProjectAssignScreenModel> objModel)
        {
            return objDal.Insert(objModel);
        }

        public DataTable FileInsert(ProjectAssignScreenModel objModel)
        {
            return objDal.FileInsert(objModel);
        }

        public DataTable FileMappingInsert(ProjectAssignScreenModel objModel)
        {
            return objDal.FileMappingInsert(objModel);
        }

        public List<ProjectAssignScreenModel> SelectByID(int ProjectDetailMasterID)
        {
            return objDal.SelectByID(ProjectDetailMasterID);
        }

        public int Update(List<ProjectAssignScreenModel> objModel)
        {
            return objDal.Update(objModel);
        }

        public List<ProjectAssignScreenModel> SelectAttachment(int ProjectDetailMasterID)
        {
            return objDal.SelectAttachment(ProjectDetailMasterID);
        }
    }
}