﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class KnowledgeBaseReportbal
    {
        private KnowledgeBaseReportDal ObjDAL = new KnowledgeBaseReportDal();

        public List<KnowledgeBaseModel> SelectByDate(string FromDate, string ToDate)
        {
            return ObjDAL.SelectByDate(FromDate, ToDate);
        }
        
    }
}
