﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class EmployeeRegistrationbal
    {
        private EmployeeRegistrationDAL objDal = new EmployeeRegistrationDAL();

        public int Insert(EmployeeRegistrationModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public List<EmployeeRegistrationModel> Select()
        {
            return objDal.Select();
        }

        public EmployeeRegistrationModel SelectByID(int UserID)
        {
            return objDal.SelectByID(UserID);
        }

        public int Update(EmployeeRegistrationModel ObjModel)
        {
            return objDal.Update(ObjModel);
        }

        public int Delete(int UserID)
        {
            return objDal.Delete(UserID);
        }
    }
}