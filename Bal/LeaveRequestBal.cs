﻿using Dal;
using Model;

namespace Bal
{
   public class LeaveRequestBal
    {
       private LeaveRequestDal ObjDAL = new LeaveRequestDal();
  
       public int Insert(LeaveRequestModel ObjModel)
       {
           return ObjDAL.Insert(ObjModel);
       }
   
    }
}
