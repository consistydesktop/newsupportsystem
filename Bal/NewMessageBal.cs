﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class NewMessageBal
    {
        private NewMessageDal objDal = new NewMessageDal();

        public List<NewMessageModel> SelectName(string Usertype)
        {
            return objDal.SelectName(Usertype);
        }

        public int Insert(NewMessageModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public List<NewMessageModel> Select()
        {
            return objDal.Select();
        }
    }
}