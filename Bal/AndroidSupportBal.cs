﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class AndroidSupportBal
    {
        private AndroidSupportDal ObjDAL = new AndroidSupportDal();

        public List<AndroidSupportModel> SelectSystemName()
        {
            return ObjDAL.SelectSystemName();
        }
        public int Insert(AndroidSupportModel ObjModel)
        {
            return ObjDAL.Insert(ObjModel);
        }
        
        


        public System.Data.DataTable FileInsert(AndroidSupportModel objModel)
        {
            return ObjDAL.FileInsert(objModel);
        }

        public int FileMappingInsert(AndroidSupportModel objModel)
        {
            return ObjDAL.FileMappingInsert(objModel);
        }

        public List<AndroidSupportViewModel> Search(AndroidSupportViewModel objModel)
        {
            return ObjDAL.Search(objModel);
        }
    }
}
