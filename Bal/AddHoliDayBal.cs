﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Bal
{
    

    public class AddHoliDayBal
    {
        private AddHoliDayDal objDal = new AddHoliDayDal();

        public int Insert(Model.HoliDayModel objmodel)
        {
            return objDal.Insert(objmodel);
        }

        public int Update(HoliDayModel objmodel)
        {
            return objDal.Update(objmodel);
        }

        public int Delete(int DayID)
        {
            return objDal.Delete(DayID);
        }
        
        public DataTable Select ()
        {
            return objDal.Select();
        }

        public DataTable SelectByID(int DayID)
        {
            return objDal.SelectByID(DayID);
        }
    }
}
