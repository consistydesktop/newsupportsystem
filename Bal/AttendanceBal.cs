﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{

    public class AttendanceBal
    {
        AttendanceDal ObjDal = new AttendanceDal();

        public List<Employee>  Select(int Month, int Year)
        {
            return ObjDal.Select(Month,Year);
        }
    }
}
