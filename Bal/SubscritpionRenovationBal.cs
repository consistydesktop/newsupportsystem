﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;

namespace Bal
{
    public class SubscritpionRenovationBal
    {
        private SubscritpionRenovationDal objDal = new SubscritpionRenovationDal();
        public DataTable SelectCost(string daysType,int ProductID)
        {
            return objDal.SelectCost(daysType, ProductID);
    
        }

        public DataTable CardTypeSelect()
        {
            return objDal.CardTypeSelect();
        }
        public DataTable SelectSlab(string PayMode)
        {
            return objDal.SelectSlab(PayMode);

        }
        public int Insert(PGModel objPGModel)
        {
            return objDal.Insert(objPGModel);
        }
        public int Update(PGModel objPGModel)
        {
            return objDal.Update(objPGModel);
        }


        public DataTable Select(string OrderId)
        {
            return objDal.Select(OrderId);
        }

        public List<AddWebSubscriptionModel> SelectSystemName(int userID)
        {
            return objDal.SelectSystemName(userID);
        }

        public DataTable SelectBySystemID(int SystemID, int UserID)
        {
            return objDal.SelectBySystemID(SystemID, UserID);
        }

        public DataTable SelectUser(int userID)
        {
            return objDal.SelectUser(userID);
        }

        public int ExtendSubscription(int ProductID, string DaysMonthType, int userID,int SystemID)
        {
            return objDal.ExtendSubscription(ProductID, DaysMonthType, userID, SystemID);
        }

        public int SelectSystemID(string webURL)
        {
            return objDal.SelectSystemID(webURL);
        }

        public DataTable SelectSearch(InvoiceModel request)
        {
            return objDal.SelectSearch(request);
        }

        public DataTable PGReportDateWiseSelect(PGModel objPGModel)
        {
            return objDal.PGReportDateWiseSelectDal(objPGModel);
        }
        public bool CheckDuplicateOrderID(string orderId)
        {
            return objDal.CheckDuplicateOrderID(orderId);
        }

        public DataTable SelectCallBackURL(int UserID, int ProductID, int SystemID)
        {
            return objDal.SelectCallBackURL(UserID,  ProductID,  SystemID);
        }
    }
}
