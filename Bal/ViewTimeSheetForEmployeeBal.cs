﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class ViewTimeSheetForEmployeeBal
    {
        private ViewTimeSheetForEmployeeDal ObjDAL = new ViewTimeSheetForEmployeeDal();

        public List<ViewTimeSheetForEmployeeModel> Select()
        {
            return ObjDAL.Select();
        }

        public List<ViewTimeSheetForEmployeeModel> SelectEmployeeName()
        {
            return ObjDAL.SelectEmployeeName();
        }

        public List<ViewTimeSheetForEmployeeModel> Search(ViewTimeSheetForEmployeeModel ObjModel)
        {
            return ObjDAL.Search(ObjModel);
        }
    }
}