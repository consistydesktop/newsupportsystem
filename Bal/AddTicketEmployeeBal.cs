﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class AddTicketEmployeeBal
    {
        private AddTicketEmployeeDal objDal = new AddTicketEmployeeDal();

        public List<AddTicketEmployeeModel> SelectServiceName()
        {
            return objDal.SelectServiceName();
        }

        public int Insert(AddTicketEmployeeModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public List<AddTicketEmployeeModel> SelectSystemName()
        {
            return objDal.SelectSystemName();
        }

        public int FileMappingInsert(AddTicketEmployeeModel objModel)
        {
            return objDal.FileMappingInsert(objModel);
        }
    }
}