﻿using Dal;
using Model;
using System.Collections.Generic;
using System.Data;

namespace Bal
{
    public class AddTicketAdminBal
    {
        private AddTicketAdminDal objDal = new AddTicketAdminDal();

        public List<AddTicketAdminModel> SelectServiceName()
        {
            return objDal.SelectServiceName();
        }

        public int Insert(AddTicketAdminModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public List<AddTicketAdminModel> SelectSystemName()
        {
            return objDal.SelectSystemName();
        }

        public DataTable FileInsert(AddTicketAdminModel objModel)
        {
            return objDal.FileInsert(objModel);
        }

        public int FileMappingInsert(AddTicketAdminModel objModel)
        {
            return objDal.FileMappingInsert(objModel);
        }

        public List<AddTicketAdminModel> SelectEmployeeName()
        {
            return objDal.SelectEmployeeName();
        }

       
    }
}