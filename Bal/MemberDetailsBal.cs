﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class MemberDetailsBal
    {
        private MemberDetailsDal objDal = new MemberDetailsDal();

        public List<MemberDetailsModel> Select()
        {
            return objDal.Select();
        }

        public int AcceptRejectCustomer(int RegistrationID, int IsAprroved, string Password)
        {
            return objDal.AcceptRejectCustomer(RegistrationID, IsAprroved, Password);
        }

        public List<MemberDetailsModel> SelectCustomerName()
        {
            return objDal.SelectCustomerName();
        }

        public MemberDetailsModel SelectByRegistrationID(int RegistrationID)
        {
            return objDal.SelectByRegistrationID(RegistrationID);
        }

        
    }
}