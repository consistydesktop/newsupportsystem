﻿using Dal;
using Model;
using System;
using System.Collections.Generic;

namespace Bal
{
    public class TimeSheetBal
    {
        private TimeSheetDal ObjDAL = new TimeSheetDal();

        public List<TimeSheetModel> SelectSystemName()
        {
            return ObjDAL.SelectSystemName();
        }

        //public int Insert(List<TimeSheetModel> ObjModel, int UserID)
        //{
        //    return ObjDAL.Insert(ObjModel, UserID);
        //}

        public int Update(TimeSheetModel ObjModel)
        {
            return ObjDAL.Update(ObjModel);
        }

        public int UpdateByID(TimeSheetModel ObjModel)
        {
            return ObjDAL.UpdateByID(ObjModel);
        }

        public List<TimeSheetModel> Select(int UserID)
        {
            return ObjDAL.Select(UserID);
        }

        public List<TimeSheetModel> SelectByID(string Date, int UserID)
        {
            return ObjDAL.SelectByID(Date, UserID);
        }

        public int Delete(String Date, int UserID)
        {
            return ObjDAL.Delete(Date, UserID);
        }



        public List<TimeSheetModel> SelectTimeSheetByDate(int UserID, string Date)
        {
            return ObjDAL.SelectTimeSheetByDate(UserID, Date);
        }

        public int Insert(int UserID, string Date, int SystemID, string Task, int Time, int Status)
        {
            return ObjDAL.Insert(UserID, Date, SystemID, Task, Time, Status);
        }

        public int Update(int UserID, int TimeSheetDetailID, int SystemID, string Task, int Time, int Status)
        {
            return ObjDAL.Update(UserID, TimeSheetDetailID, SystemID, Task, Time, Status);
        }

        public int Delete(int UserID, int TimeSheetDetailID)
        {
            return ObjDAL.Delete(UserID, TimeSheetDetailID);
        }

        public DateTime getDateByID(int TimeSheetDetailID)
        {
            return ObjDAL.getDateByID(TimeSheetDetailID);
        }

        public int SelectTotalTimeByUserID(int UserID, string Date)
        {
            return ObjDAL.SelectTotalTimeByUserID(UserID, Date);
        }
    }
}