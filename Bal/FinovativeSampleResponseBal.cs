﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class FinovativeSampleResponseBal
    {
        private FinovativeSampleResponseDal ObjDAL = new FinovativeSampleResponseDal();
        public DataTable selectSampleResponseReport()
        {
            return ObjDAL.selectSampleResponseReport();
        }
        public DataTable SelectBYProviderID(int ProviderID)
        {
            return ObjDAL.SelectBYProviderID(ProviderID);
        }
        
        public int Insert(SampleResponseViewModel ObjModel)
        {
            return ObjDAL.Insert(ObjModel);
        }

        public int Delete(int SampleResponseID)
        {
            return ObjDAL.Delete(SampleResponseID);
        }

        public SampleResponseViewModel SelectByID(int SampleResponseID)
        {
            return ObjDAL.SelectByID(SampleResponseID);
        }

        public int Update(SampleResponseViewModel objModel)
        {
            return ObjDAL.Update(objModel);
        }



    }
}
