﻿using Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class ViewTicketClientBal
    {

        private ViewTicketClientDal objDal = new ViewTicketClientDal();

        public System.Data.DataTable Select(int UserID)
        {
            return objDal.Select(UserID);
        }

        public System.Data.DataTable SelectByDate(int UserID, string FromDate, string ToDate)
        {
            return objDal.SelectByDate(UserID, FromDate, ToDate);
        }

      
    }
}
