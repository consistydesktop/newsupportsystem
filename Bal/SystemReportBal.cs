﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class SystemReportBal
    {
        SystemReportDal ObjDal = new SystemReportDal();

        public List<SystemReportModel> SelectSystemName()
        {
            return ObjDal.SelectSystemName();
        }

        public List<List<SystemReportModel>> Search(string FromDate, string ToDate, int SystemID)
        {
            return ObjDal.Search(FromDate, ToDate, SystemID);
        }
    }
}
