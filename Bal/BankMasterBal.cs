﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class BankMasterBal
    {
        private BankMasterDal ObjDAL = new BankMasterDal();
        public int InsertBankMaster(BankMasterModel ObjModel)
        {
            return ObjDAL.InsertBankMaster(ObjModel);
        }
        public List<BankMasterModel> SelectBankMaster()
        {
            return ObjDAL.SelectBankMaster();
        }
    }
}
