﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class AssignToDeveloperBal
    {
        private AssignToDeveloperDal objDal = new AssignToDeveloperDal();

        public List<AssignToDeveloperModel> SelectEmployeeName()
        {
            return objDal.SelectEmployeeName();
        }

        public int Insert(AssignToDeveloperModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public AssignToDeveloperModel SelectByTicketID(int TicketID)
        {
            return objDal.SelectByTicketID(TicketID);
        }

        public List<AssignToDeveloperModel> SelectSystem()
        {
            return objDal.SelectSystem();
        }

        public int getUserIDByTicketID(int TicketID)
        {
            return objDal.getUserIDByTicketID(TicketID);
        }

        public string GetMobileNoByUserID(int AssignEmployeeID)
        {
            return objDal.GetMobileNoByUserID(AssignEmployeeID);
        }
    }
}