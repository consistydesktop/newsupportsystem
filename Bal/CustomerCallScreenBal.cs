﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class CustomerCallScreenBal
    {
        private CustomerCallScreenDal objDal = new CustomerCallScreenDal();

        public List<CustomerCallScreenModel> SelectSystemName()
        {
            return objDal.SelectSystemName();
        }

        public List<CustomerCallScreenModel> SelectEmployeeName()
        {
            return objDal.SelectEmployeeName();
        }

        public List<CustomerCallScreenModel> SelectIssue()
        {
            return objDal.SelectIssue();
        }

        public int Insert(CustomerCallScreenModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public List<CustomerCallScreenModel> Select()
        {
            return objDal.Select();
        }

        public CustomerCallScreenModel SelectByID(int UserID)
        {
            return objDal.SelectByID(UserID);
        }

        public int Update(CustomerCallScreenModel ObjModel)
        {
            return objDal.Update(ObjModel);
        }

        public int Delete(int UserID)
        {
            return objDal.Delete(UserID);
        }
    }
}