﻿using Dal;
using Model;
using System.Collections.Generic;
using System.Data;

namespace Bal
{
    public class UserInformationBal
    {
        private UserInformationDal objDal = new UserInformationDal();

        public List<UserInformationModel> Select(int UserTypeID,int LoginUserID)
        {
            return objDal.Select(UserTypeID, LoginUserID);
        }

        public UserInformationModel SelectByID(int UserID)
        {
            return objDal.SelectByID(UserID);
        }

        public int Update(UserInformationModel ObjModel)
        {
            return objDal.Update(ObjModel);
        }

        public int Delete(int UserID)
        {
            return objDal.Delete(UserID);
        }

        public int UpdateAciveUser(int UserID, int IsActive)
        {
            return objDal.UpdateAciveUser(UserID, IsActive);
        }

        public DataTable GetEmailByUserID(int UserID)
        {
            return objDal.GetEmailByUserID(UserID);
        }

        public DataTable UserinformationSelectForIncompleteTimeSheet(string date)
        {
           return objDal.UserinformationSelectForIncompleteTimeSheet(date);
        }
    }
}