﻿using Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
   public class ClientInformationBal
    {
       private ClientInformationDal objDal = new ClientInformationDal();




       public System.Data.DataTable SelectClient()
       {
           return objDal.SelectClient();
       }

       public int UpdateAciveUser(int UserID, int IsActive)
       {
           return objDal.UpdateAciveUser(UserID, IsActive);
       }

       public System.Data.DataTable UserInformationSelectClientByID(int UserID)
       {
           return objDal.UserInformationSelectClientByID(UserID);
       }
    }
}
