﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class DashboardForCustomerBal
    {
        private DashboardForCustomerDal objDal = new DashboardForCustomerDal();



        public int SelectTicketCount(int UserID)
        {
            return objDal.SelectTicketCount(UserID);
        }
    }
}