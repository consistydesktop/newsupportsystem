﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class SystemNameBal
    {
        private SystemNameDal objDal = new SystemNameDal();

        public List<SystemNameModel> SelectCustomerName()
        {
            return objDal.SelectCustomerName();
        }

        public int Insert(SystemNameModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public List<SystemNameModel> Select()
        {
            return objDal.Select();
        }

        public List<SystemNameModel> SelectService()
        {
            return objDal.SelectService();
        }

        public List<SystemNameModel> SelectProduct()
        {
            return objDal.SelectProduct();
        }

        public SystemNameModel SelectByID(int UserID)
        {
            return objDal.SelectByID(UserID);
        }

        public int Update(SystemNameModel ObjModel)
        {
            return objDal.Update(ObjModel);
        }

        public int Delete(int UserID)
        {
            return objDal.Delete(UserID);
        }
    }
}