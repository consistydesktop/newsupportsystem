﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class TicketReportForCustomerBal
    {
        private TicketReportForCustomerDal objDal = new TicketReportForCustomerDal();

        public List<TicketReportForCustomerModel> Select(int UserID)
        {
            return objDal.Select(UserID);
        }

        public List<TicketReportForCustomerModel> Search(string FromDate, string ToDate, int UserID)
        {
            return objDal.Search(FromDate, ToDate, UserID);
        }

        public List<TicketReportForCustomerModel> SelectBySystemID(int SystemID, int UserID)
        {
            return objDal.SelectBySystemID(SystemID, UserID);
        }
    }
}