﻿using Dal;
using Model;

namespace Bal
{
    public class ActionBal
    {
        private ActionDal objDal = new ActionDal();

        public int Insert(ActionModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public ActionModel SelectByTicketID(int TicketID, int UserID)
        {
            return objDal.SelectByTicketID(TicketID, UserID);
        }
    }
}