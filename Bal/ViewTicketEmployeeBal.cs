﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class ViewTicketEmployeeBal
    {
        private ViewTicketEmployeeDal objDal = new ViewTicketEmployeeDal();

        public List<ViewTicketEmployeeModel> Select(int UserID)
        {
            return objDal.Select(UserID);
        }

        public List<ViewTicketEmployeeModel> Search(string FromDate, string ToDate, int UserID)
        {
            return objDal.Search(FromDate, ToDate, UserID);
        }

        public List<ViewTicketEmployeeModel> SelectByTicketID(int TicketID, int UserID)
        {
            return objDal.SelectByTicketID(TicketID, UserID);
        }

        public List<ViewTicketEmployeeModel> SelectAttachment(ViewTicketEmployeeModel ObjModel)
        {
            return objDal.SelectAttachment(ObjModel);
        }
    }
}