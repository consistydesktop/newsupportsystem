﻿using Dal;
using Model;
using System.Collections.Generic;
using System.Data;

namespace Bal
{
    public class AssignTaskBal
    {
        private AssignTaskDal ObjDAL = new AssignTaskDal();

        public List<AssignTaskModel> SelectEmployeeName()
        {
            return ObjDAL.SelectEmployeeName();
        }

        public List<List<AssignTaskModel>> SelectByID(int TaskID)
        {
            return ObjDAL.SelectByID(TaskID);
        }

        public DataTable FileInsert(AssignTaskModel ObjModel)
        {
            return ObjDAL.FileInsert(ObjModel);
        }

        public DataTable FileMappingInsert(AssignTaskModel objModel)
        {
            return ObjDAL.FileMappingInsert(objModel);
        }

        public int Insert(int EmployeeID, AssignTaskModel ObjModel)
        {
            return ObjDAL.Insert(EmployeeID, ObjModel);
        }

        public int Update(int EmployeeID, AssignTaskModel ObjModel)
        {
            return ObjDAL.Update(EmployeeID, ObjModel);
        }

        public int SubTaskInsert(AssignTaskModel ObjModel)
        {
            return ObjDAL.SubTaskInsert(ObjModel);
        }

        public int InsertComment(AssignTaskModel ObjModel)
        {
            return ObjDAL.InsertComment(ObjModel);
        }



        public List<AssignTaskModel> SelectAttachment(int TaskID)
        {
            return ObjDAL.SelectAttachment(TaskID);
        }

        public List<AssignTaskModel> SelectSystemName()
        {
            return ObjDAL.SelectSystemName();
        }

        public int Delete(int TaskID)
        {
            return ObjDAL.Delete(TaskID);
        }
    }
}