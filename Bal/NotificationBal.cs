﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class NotificationBal
    {
        private NotificationDal objDal = new NotificationDal();

        public int Insert(NotificationModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public List<NotificationModel> Select()
        {
            return objDal.Select();
        }

        public List<NotificationModel> SelectNotification()
        {
            return objDal.SelectNotification();
        }

        public NotificationModel SelectByID(int NotificationID)
        {
            return objDal.SelectByID(NotificationID);
        }

        public int Update(NotificationModel ObjModel)
        {
            return objDal.Update(ObjModel);
        }

        public int Delete(int NotificationID)
        {
            return objDal.Delete(NotificationID);
        }
    }
}