﻿using Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class ProfileBal
    {
        ProfileDal ObjDal = new ProfileDal();

        public Model.ProfileModel SelectProfile(int UserID)
        {
            return ObjDal.SelectProfile(UserID);
        }

        public int UpdateEmail(Model.ProfileModel ObjModel)
        {
            return ObjDal.UpdateEmail(ObjModel);
        }

        public int UpdatePassword(Model.ProfileModel ObjModel)
        {
            return ObjDal.UpdatePassword(ObjModel);
        }
    }
}
