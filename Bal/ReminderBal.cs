﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class ReminderBal
    {
        private ReminderDal ObjDAL = new ReminderDal();

        public int Insert(ReminderModel ObjModel)
        {
            return ObjDAL.Insert(ObjModel);
        }

        public List<ReminderModel> Select()
        {
            return ObjDAL.Select();
        }

        public List<ReminderModel> SelectByID(int ReminderMasterID, int UserID)
        {
            return ObjDAL.SelectByID(ReminderMasterID, UserID);
        }

        public int Delete(int ReminderMasterID)
        {
            return ObjDAL.Delete(ReminderMasterID);
        }

        public int Update(ReminderModel ObjModel)
        {
            return ObjDAL.Update(ObjModel);
        }

        public List<ReminderModel> SelectCustomerName()
        {
            return ObjDAL.SelectCustomerName();
        }

        public List<ReminderModel> SelectReminderType()
        {
            return ObjDAL.SelectReminderType();
        }

        public List<ReminderModel> Search(ReminderModel ObjModel)
        {
            return ObjDAL.Search(ObjModel);
        }

        public List<ReminderModel> SelectSystemName()
        {
            return ObjDAL.SelectSystemName();
        }
    }
}