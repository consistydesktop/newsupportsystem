﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class ViewTaskEmployeeBal
    {
        private ViewTaskEmployeeDal ObjDal = new ViewTaskEmployeeDal();

        public List<AssignTaskModel> Select(int UserID)
        {
            return ObjDal.Select(UserID);
        }

        public List<AssignTaskModel> Search(string FromDate, string ToDate)
        {
            return ObjDal.Search(FromDate, ToDate);
        }
    }
}