﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;

namespace Bal
{
    public class DashboardBal
    {
        private DashboardDal objDal = new DashboardDal();

        public List<DashboardModel> Select()
        {
            return objDal.Select();
        }

    
        public DataTable SelectTaskCount(int UserID)
        {
            return objDal.SelectTaskCount(UserID);
        }
        public DataTable SelectUnpaidSubscription()
        {
            return objDal.SelectUnpaidSubscription();
        }
        public DataTable SelectpaidSubscriptionDetails()
        {
            return objDal.SelectpaidSubscriptionDetails();
        }
        
        public System.Data.DataTable SelectTicketCounts(int UserID)
        {
            return objDal.SelectTicketCounts(UserID);
        }

        public DataTable SelectLastSubscriptionDetails(int SystemID, int UserID)
        {
            return objDal.SelectLastSubscriptionDetails(SystemID,UserID);
        }
    }
}