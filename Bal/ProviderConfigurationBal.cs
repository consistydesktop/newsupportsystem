﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class ProviderConfigurationBal
    {
        private ProviderConfigurationDal objDAL = new ProviderConfigurationDal();
        public DataTable SelectOpertaorSetUp(ProviderConfigurationModel objModel)
        {
            return objDAL.SelectOpertaorSetUp(objModel);
        }

        public int UpdateOperatorSetUp(Model.ProviderConfigurationModel objModel)
        {
            return objDAL.UpdateOperatorSetUp(objModel);
        }

        public DataTable SelectConfiguration( )
        {
            return objDAL.SelectOpertaorSetUp();
        }
        public DataTable ServiceSelect()
        {
            return objDAL.ServiceSelect();
        }

        
    }
}
