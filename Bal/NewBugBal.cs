﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class NewBugBal
    {
        private NewBugDal ObjDal = new NewBugDal();

        public int Insert(NewBugModel ObjModel, int UserID)
        {
            return (ObjDal.Insert(ObjModel, UserID));
        }

        public List<NewBugModel> Select()
        {
            return ObjDal.Select();
        }

        public int Delete(int ProjectBugMasterID)
        {
            return ObjDal.Delete(ProjectBugMasterID);
        }

        public bool ExistProjectName(string ProjectName)
        {
            return ObjDal.ExistProjectName(ProjectName);
        }
    }
}