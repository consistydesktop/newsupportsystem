﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class ViewDetailsByProjectBal
    {
        private ViewDetailsByProjectDal objDal = new ViewDetailsByProjectDal();

        public List<ViewDetailsByProjectModel> Select(int ProjectDetailMasterID)
        {
            return objDal.Select(ProjectDetailMasterID);
        }

        public List<ViewDetailsByProjectModel> SelectByProjectDetailMasterID(int ProjectDetailMasterID)
        {
            return objDal.SelectByProjectDetailMasterID(ProjectDetailMasterID);
        }
    }
}