﻿using Dal;
using Model;

namespace Bal
{
    public class CustomerRegistrationBal
    {
        private CustomerRegistrationDal objDal = new CustomerRegistrationDal();

        public int Insert(CustomerRegistrationModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public CustomerRegistrationModel SelectByID(int UserID)
        {
            return objDal.SelectByID(UserID);
        }

        public int Update(CustomerRegistrationModel ObjModel)
        {
            return objDal.Update(ObjModel);
        }

        public CustomerRegistrationModel SelectByRegistrationID(int RegistrationID)
        {
            return objDal.SelectByRegistrationID(RegistrationID);
        }
    }
}