﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
   public class TaxInvoiceBal
    {
        private TaxInvoiceDal objDal = new TaxInvoiceDal();
        public DataTable SelectSearch(InvoiceModel request)
        {
            return objDal.SelectSearch(request);
        }
        public DataTable SelectInvoiceReportForPrint(InvoiceModel request)
        {
            return objDal.SelectInvoiceReportForPrint(request);
        }
        
    }
}
