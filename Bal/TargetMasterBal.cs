﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class TargetMasterBal
    {
        private TargetMasterDal ObjDAL = new TargetMasterDal();
        public int InsertTargetMaster(TargetMasterModel ObjModel)
        {
            return ObjDAL.InsertTargetMaster(ObjModel);
        }
        public List<TargetMasterModel> SelectTargetMaster()
        {
            return ObjDAL.SelectTargetMaster();
        }
    }
}
