﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class TimeSheetReportBal
    {
        private TimeSheetReportDal ObjDAL = new TimeSheetReportDal();

        public List<TimeSheetModel> Select(int UserID)
        {
            return ObjDAL.Select(UserID);
        }

        public List<TimeSheetModel> Search(TimeSheetModel ObjModel)
        {
            return ObjDAL.Search(ObjModel);
        }

        public List<TimeSheetModel> SelectByID(string Date, int UserID)
        {
            return ObjDAL.SelectByID(Date, UserID);
        }

      
    }
}