﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class SendMessageBal
    {
        private SendMessageDal objDal = new SendMessageDal();
        public int insertMessage(MessageMasterModel ObjModel)
        {
            return objDal.insertMessage(ObjModel);
        }

        public DataTable getMobileNumberfromUserID(int UserID)
        {
            return objDal.getMobileNumberfromUserID(UserID);
        }

        
    }
}
