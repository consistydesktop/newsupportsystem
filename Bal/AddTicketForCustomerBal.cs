﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class AddTicketForCustomerBal
    {
        private AddTicketForCustomerDal objDal = new AddTicketForCustomerDal();

        public List<AddTicketForCustomerModel> SelectServiceName()
        {
            return objDal.SelectServiceName();
        }

        public int Insert(AddTicketForCustomerModel objModel)
        {
            return objDal.Insert(objModel);
        }

        public List<AddTicketForCustomerModel> SelectSystemName(int UserID)
        {
            return objDal.SelectSystemName(UserID);
        }

        public AddTicketForCustomerModel SelectByTicketID(int TicketID, int UserID)
        {
            return objDal.SelectByTicketID(TicketID, UserID);
        }

        public int FileMappingInsert(AddTicketForCustomerModel objModel)
        {
            return objDal.FileMappingInsert(objModel);
        }
    }
}