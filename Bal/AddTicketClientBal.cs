﻿using System;
using Dal;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{

    public class AddTicketClientBal
    {
        private AddTicketClientDal ObjDal = new AddTicketClientDal();


        public List<Model.ServiceModel> SelectServiceName()
        {
            return ObjDal.SelectServiceName();
         }

        public int Insert(Model.AddTicketClientModel Insert)
        {
            return ObjDal.Insert(Insert);
         }

        public int FileMappingInsert(Model.AddTicketClientModel objModel)
        {
            return ObjDal.FileMappingInsert(objModel);
        }

        public int UpdateStatusByClient(string TicketNo, string Status)
        {
            return ObjDal.UpdateStatusByClient(TicketNo, Status);
        }

        public int Delete(int TicketID)
        {
            return ObjDal.Delete(TicketID);
        }

        public System.Data.DataTable SelectByID(int TicketID)
        {
            return ObjDal.SelectByID(TicketID);
        }

        public System.Data.DataTable SelectTicketAttachmentByID(int TicketID, string UploadType)
        {
            return ObjDal.SelectTicketAttachmentByID(TicketID, UploadType);
        }

        public int Update(Model.AddTicketClientModel objModel)
        {
            return ObjDal.Update(objModel);
        }
    }
}
