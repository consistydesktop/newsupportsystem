﻿using Dal;
using Model;
using System.Data;

namespace Bal
{
    public class LoginBal
    {
        private LoginDAL objDAL = new LoginDAL();

        public DataTable Select(LoginModel objModel)
        {
            return objDAL.Select(objModel);
        }
        public DataTable SelectByToken(string Token)
        {
            return objDAL.SelectByToken(Token);
        }

        public DataTable SelectToken(LoginModel objModel)
        {
            return objDAL.SelectToken(objModel);
        }

        public int UpdateToken(LoginModel objModel)
        {
            return objDAL.UpdateToken(objModel);
        }

        public int UpdateOTP(LoginModel objModel)
        {
            return objDAL.UpdateOTP(objModel);
        }

        public DataTable SelectOTP(LoginModel objModel)
        {
            return objDAL.SelectOTP(objModel);
        }
        public DataTable SelectTokenForGateway(Model.LoginModel objModel)
        {
            return objDAL.SelectTokenForGateway(objModel);
        }
    }
}