﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class DashboardPendingStatusBal
    {
        private DashboardPendingStatusDal objDal = new DashboardPendingStatusDal();

        public List<DashboardModel> Select(int SystemID)
        {
            return objDal.Select(SystemID);
        }

        public List<DashboardModel> SelectForEmployee(int SystemID, int UserID)
        {
            return objDal.SelectForEmployee(SystemID, UserID);
        }
    }
}