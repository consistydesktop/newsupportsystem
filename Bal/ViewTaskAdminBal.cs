﻿using Dal;
using Model;
using System.Collections.Generic;

namespace Bal
{
    public class ViewTaskAdminBal
    {
        private ViewTaskAdminDal ObjDal = new ViewTaskAdminDal();

        public List<AssignTaskModel> Select()
        {
            return ObjDal.Select();
        }

        public List<AssignTaskModel> Search(string FromDate, string ToDate,int UserID,string Status)
        {
            return ObjDal.Search(FromDate, ToDate,UserID,Status);
        }


        public List<AssignTaskModel> SearchByUserID(string FromDate, string ToDate,int UserID)
        {
            return ObjDal.SearchByUserID(FromDate, ToDate, UserID);
        }
        public List<AssignTaskModel> SelectEmployeeName()
        {
            return ObjDal.SelectEmployee();
        }
    }
}