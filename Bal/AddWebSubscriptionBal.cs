﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class AddWebSubscriptionBal
    {
        AddWebSubscriptionDal ObjDal = new AddWebSubscriptionDal();

        public List<Model.AddWebSubscriptionModel> SelectSystemName()
        {
            return ObjDal.SelectSystemName();
        }

        public int Insert(Model.AddWebSubscriptionModel ObjModel)
        {
            return ObjDal.Insert(ObjModel);
        }

        public List<Model.AddWebSubscriptionModel> Select()
        {
            return ObjDal.Select();
        }

        public AddWebSubscriptionModel SelectByID(int WebSubscriptionID)
        {
            return ObjDal.SelectByID(WebSubscriptionID);
        }

        public int Delete(int WebSubscriptionID)
        {
            return ObjDal.Delete(WebSubscriptionID);
        }

        public int Update(AddWebSubscriptionModel objModel)
        {
            return ObjDal.Update(objModel);
        }

        public DataTable getEndDate(string URL, string HostedIP, string Token)
        {
            return ObjDal.getEndDate(URL, HostedIP, Token);
        }
        public DataTable getEndDateForFinovative(string URL, string HostedIP, string LoginToken)
        {
            return ObjDal.getEndDateForFinovative(URL, HostedIP, LoginToken);
        }

        public DataTable getEndDateForFinovativeBySystemID(int systemID)
        {
            return ObjDal.getEndDateForFinovativeBySystemID(systemID);
        }

        public int UpdateCallBackURL(string callBackURL,int UserID, int ProductID, string WebURL)
        {
            return ObjDal.UpdateCallBackURL(callBackURL, UserID, ProductID, WebURL);
        }
    }

}
