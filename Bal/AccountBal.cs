﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
    public class AccountBal
    {
        private AccountDal ObjDAL = new AccountDal();
        public List<TransactionMasterModel> SelectUser()
        {
            return ObjDAL.SelectUser();
        }
        public int InsertTransactionMaster(TransactionMasterModel ObjModel)
        {
            return ObjDAL.InsertTransactionMaster(ObjModel);
        }

        public List<TransactionMasterModel> SelectByDate(string FromDate, string ToDate, string PaymentType, string TransactionType,int BankMasterID,int IsGST)
        {
           return ObjDAL.SelectByDate(FromDate, ToDate, PaymentType, TransactionType, BankMasterID,IsGST);
        }


        public List<TransactionMasterModel> Select(string FromDate, string ToDate)
        {
            return ObjDAL.Select(FromDate, ToDate);
        }

        public List<List<TransactionMasterModel>> SelectByID(int TransactionMasterID)
        {
           return ObjDAL.SelectByID(TransactionMasterID);
        }

        public int UpdateByID(TransactionMasterModel ObjModel)
        {
            return ObjDAL.UpdateByID(ObjModel);
        }
    }
}
 