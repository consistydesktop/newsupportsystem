﻿using Dal;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
   public class FinovativeSampleURLBal
    {
        private FinovativeSampleURLDal ObjDAL = new FinovativeSampleURLDal();
        public DataTable selectSampleURLReport()
        {

            return ObjDAL.selectSampleURLReport();
        }
        public int Insert(SampleURLViewModel ObjModel)
        {
            return ObjDAL.Insert(ObjModel);
        }

        public int Delete(int SampleURLID)
        {
            return ObjDAL.Delete(SampleURLID);
        }

        public SampleURLViewModel SelectByID(int SampleURLID)
        {
            return ObjDAL.SelectByID(SampleURLID);
        }

        public int Update(SampleURLViewModel objModel)
        {
            return ObjDAL.Update(objModel);
        }

        public DataTable SelectBYProviderID(int ProviderID)
        {
            return ObjDAL.SelectBYProviderID(ProviderID);
        }
    }
}
