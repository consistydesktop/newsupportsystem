﻿using Dal;
using Model;

namespace Bal
{
    public class UserAccessBAL
    {
        private UserAccessDAL objDAL = new UserAccessDAL();

        public bool SelectValidAccess(UserAccessModel objModel)
        {
            return objDAL.SelectValidAccess(objModel);
        }
    }
}