﻿using Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bal
{
   public  class AllTimeSheetReportBal
    {
       AllTimeSheetReportDal ObjDal = new AllTimeSheetReportDal();

        public List<Model.AllTimeSheetReportModel> SelectEmployeeName()
        {
            return ObjDal.SelectEmployeeName();
        }

        public List<Model.AllTimeSheetReportModel> Select()
        {
            return ObjDal.Select();
        }

        public List<Model.AllTimeSheetReportModel> Search(Model.AllTimeSheetReportModel ObjModel)
        {
            return ObjDal.Search(ObjModel);
        }

        public List<Model.AllTimeSheetReportModel> SelectTimeSheetReportByEmployeeName(string Date, int UserID)
        {
            return ObjDal.SelectTimeSheetReportByEmployeeName(Date, UserID);
        }
    }
}
