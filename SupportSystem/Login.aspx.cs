﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using SupportSystem.Controller;
using SupportSystem.PG;
using System;
using System.Data;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //DateTime dt = DateTime.Now;
            //if (dt.Hour == 0 && dt.Minute <= 15)
            //{

            //    TimeSheetIncomplteSendMeassage incompleteTimesheetSendMessage = new TimeSheetIncomplteSendMeassage();
            //    incompleteTimesheetSendMessage.SendMessage();
            //}
        }

        #region LoginValidation

        public static bool ValidUserInformation(string MobileNumber, string Password, string Captcha)
        {
            Validation validation = new Validation();
            if (!validation.IsValidString(MobileNumber))
                return false;
            if (!validation.IsValidString(Password))
                return false;
            if (!validation.IsValidString(Captcha))
                return false;

            return true;
        }

        [WebMethod]
        public static MessageWithIcon LoginValidation(string MobileNumber, string Password, string Captcha)
        {
            AccessController ac = new AccessController();
            MessageWithIcon msgIcon = new MessageWithIcon();

            LoginModel objModel = new LoginModel();
            LoginBal objBAL = new LoginBal();

            try
            {
                string sessionCaptcha = HttpContext.Current.Session["Captcha"].ToString();

                if (!Captcha.Equals(sessionCaptcha))
                {
                    msgIcon.Icon = "Error";
                    msgIcon.Message = "Please enter a valid captcha";
                    return msgIcon;
                }

                if (!ValidUserInformation(MobileNumber, Password, Captcha))
                {
                    msgIcon.Icon = "Error";
                    msgIcon.Message = "Missing paramter";
                    return msgIcon;
                }


                DataTable dt = new DataTable();
                objModel.MobileNumber = MobileNumber;
                objModel.Password = Password;

                dt = objBAL.Select(objModel);

                int count = dt.Rows.Count;

                if (count <= 0)
                {
                    msgIcon.Icon = "Error";
                    msgIcon.Message = "Please enter a valid username and password";
                    return msgIcon;
                }

                HttpContext.Current.Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                HttpContext.Current.Session["MobileNumber"] = dt.Rows[0]["MobileNumber"].ToString();
                HttpContext.Current.Session["Username"] = dt.Rows[0]["Username"].ToString();
                HttpContext.Current.Session["UserTypeID"] = dt.Rows[0]["UserTypeID"].ToString();
                HttpContext.Current.Session["UserType"] = dt.Rows[0]["UserType"].ToString();
                HttpContext.Current.Session["EmailID"] = dt.Rows[0]["EmailID"].ToString();
                if (RoleMaster.Employee != Convert.ToInt32(dt.Rows[0]["UserTypeID"]) && RoleMaster.Customer != Convert.ToInt32(dt.Rows[0]["UserTypeID"]) && RoleMaster.Support != Convert.ToInt32(dt.Rows[0]["UserTypeID"]))
                {
                    string otp = ac.getOTP();
                    objModel.OTP = otp;

                    int result = objBAL.UpdateOTP(objModel);

                    string otpMessage = string.Format("Your OTP is {0}", otp);
                    //   MessageProcessing.SendSMS(MobileNumber, otp);
                }

                int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                int UserTypeID = Convert.ToInt32(HttpContext.Current.Session["UserTypeID"]);

                if (UserID == Convert.ToInt32(dt.Rows[0]["UserID"]))
                {
                    AccessController a = new AccessController();
                    string Token = a.getRandomToken();
                    objModel.Token = Token;

                    objBAL.UpdateToken(objModel);
                    HttpContext.Current.Session["Token"] = Token;
                    msgIcon.Message = UserTypeID.ToString();
                }

                //sendSMS();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return msgIcon;
        }

        #endregion LoginValidation

        //private static void sendSMS()
        //{

        //     IncompleteTimesheetSendMessageBAL objBAL = new IncompleteTimesheetSendMessageBAL();
        //    bool isTodaySent = objBAL.isTodaySMSSent();
        //    if (!isTodaySent)
        //    {
        //        //Task.Run(() =>
        //        //{

        //        //});

        //        TimeSheetIncomplteSendMeassage incompleteTimesheetSendMessage = new TimeSheetIncomplteSendMeassage();
        //        incompleteTimesheetSendMessage.SendMessage();

        //        objBAL.Insert();
        //    }


    }



}

