﻿using Bal;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class DashboardForCustomer : System.Web.UI.Page
    {
        private static string pageName = "DashboardForCustomer.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static int SelectTicketCount()
        {
            AccessController.checkAccess(pageName);
            int UserID = -1;
            int TicketCount = -1;
            UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());

            DashboardForCustomerBal objBal = new DashboardForCustomerBal();

            try
            {
                TicketCount = objBal.SelectTicketCount(UserID);
            }
            catch (Exception ex)
            {

            }
            return TicketCount;
        }
    }
}