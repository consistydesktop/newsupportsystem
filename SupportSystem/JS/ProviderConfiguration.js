﻿function ProviderMasterSelect() {

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/APIProvider.aspx/Select",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataProviderMaster(data);
        },
        error: function (result) {
        }
    });
}

function BindDataProviderMaster(data) {
    $("#txtProviderName").html("");
    $("#txtProviderName").append("<option value=0>Select Provider </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].ProviderID, data.d[i].ProviderName));
        $("#txtProviderName").append(strRowDropdown);
    }

    $("#txtProviderName").trigger("chosen:updated");
    $("#txtProviderName").chosen({ width: "100%" });

}

function ServiceSelect() {

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ProviderConfiguration.aspx/ServiceSelect",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataServices(data);
        },
        error: function (result) {
        }
    });
}

function BindDataServices(data) {
    $("#txtServiceName").html("");
    $("#txtServiceName").append("<option value=0>Select Service </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].ServiceID, data.d[i].ServiceName));
        $("#txtServiceName").append(strRowDropdown);
    }
    $("#txtServiceName").trigger("chosen:updated");
    $("#txtServiceName").chosen({ width: "100%" });

}

function Select() {


    var ProviderID = $("#txtProviderName").val();
    var ServiceID = $("#txtServiceName").val();

    if (ServiceID == null || ServiceID == undefined) {
        ServiceID = 0;

    }

    $("#lblProviderID").val(ProviderID);

    var jsonObj = {
        ProviderID: ProviderID,
        ServiceID: ServiceID
    };
    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ProviderConfiguration.aspx/Select",
        data: jData,
        dataType: "json",
        success: function (data) {
            $("#SampleConfigurationDetails").html("<thead><tr><th>#</th><th>Service</th><th>Operator</th><th>Code</th><th>Type</th><th>Update</th></tr></thead>");

            for (var i = 0; i < data.d.length; i++) {
                var strRow = [];
                var color = 'black';

                strRow.push(String.format("<tr style='color:{0};'>", color));
                strRow.push(String.format("<td> {0} </td>", (i + 1)));
                strRow.push(String.format("<td> <input type='hidden' id='ServiceID{0}'  value='{1}'/>{1}</td>", i, data.d[i].ServiceName));
                strRow.push(String.format("<td> <input type='hidden' id='OperatorID{0}'  value='{2}'/>{1}</td>", i, data.d[i].OperatorName.toUpperCase(), data.d[i].OperatorID));
                strRow.push(String.format("<td> <input style='width:100px;' type='text' id='Opcode{0}' value='{1}'/></td>", i, data.d[i].Opcode));
                 strRow.push(String.format("<td> <input style='width:100px;' type='text' id='Param1{0}' value='{1}'/></td>", i, data.d[i].Param1));

                strRow.push(String.format("<td class='text-center'><input type='image' onclick=\"updateRow('{0}','{1}','{2}','{3}')\" src='/Images/update-3.png' alt='Update' height='30'></td>", i, data.d[i].ProviderID, data.d[i].OperatorID));
                strRow.push('</tr>');

                $("#SampleConfigurationDetails").append(strRow.join(""));
            }
            $('#SampleConfigurationDetails').DataTable({
                "bDestroy": true,
                "paging": false,
                "ordering": false,
                dom: 'Bfrtip',
                buttons: [

                    'excelHtml5',

                    'pdfHtml5'
                ]
            });
        },
        error: function (result) {

        }
    });

}

function UpdateProviderSetUp() {
    $.alert.open({
        type: 'confirm',
        content: "Are you sure to update operator details ?",
        icon: 'warning',
        title: ClientName,
        buttons: {
            Ok: 'Ok',
            Cancel: 'Cancel'
        },
        callback: function (button) {
            if (button == 'Ok') {
                var ProviderID = $("#lblProviderID").val();
                var table = $('#SampleConfigurationDetails').DataTable();
                var indexx = 0;

                var deferreds = [];
                var successCount = 0;
                var failCount = 0;

                table.rows().eq(0).each(function (index) {
                    var row = table.row(index);
                    var data = row.data();
                    var OperatorID = $("#OperatorID" + index).val();
                    var Opcode = $("#Opcode" + index).val();

                    var Param1 = $("#Param1" + index).val();

                    if (Param1 == null || Param1 == undefined) {
                        Param1 = "0";
                    }


                    var jsonObj = {
                        OperatorID: OperatorID,
                        Opcode: Opcode,
                        ProviderID: ProviderID,
                        index: indexx,
                        Param1: Param1
                    };

                    var jData = JSON.stringify(jsonObj)

                    indexx = indexx + 1;

                    deferreds.push(
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "ProviderConfiguration.aspx/UpdateOperator",
                            data: jData,
                            dataType: "json",

                            success: function (data) {
                                successCount = successCount + 1;
                                showProgress(successCount, indexx);
                            },
                            error: function (xhr, status, error) {
                                failCount = failCount + 1;
                                console.log("failCount : " + xhr.responseText);
                            }
                        }));
                });

                showLoaderwithCount(indexx);

                $.when.apply($, deferreds).then(function () {
                    //pass
                }).fail(function () {
                    //pass
                }).always(function () {
                    console.log("All done");
                    hideLoaderwithCount();

                    if (failCount >= 1) {
                        $.alert.open({
                            content: String.format("Provider operator details not updated" ), icon: 'confirm', title: ClientName,
                            callback: function () {
                                // select(ProviderID);
                            }
                        });
                    }
                    else {
                        $.alert.open({
                            content: String.format("Provider operator successfully updated"), icon: 'confirm', title: ClientName,
                            callback: function () {
                                //   select(ProviderID);
                            }
                        });
                    }
                });
            }
        }
    });
}

function updateRow(i, Pid, Oid) {
    var ProviderID = $("#lblProviderID").val();
    var OperatorID = $("#OperatorID" + i).val();
    var Opcode = $("#Opcode" + i).val();

    var Param1 = $("#Param1" + i).val();

    if (Opcode == "") {
        $.alert.open({
            content: String.format(pleaseEnter, "operator code"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#Opcode" + i).focus();
            }
        });
        return false;
    }


    if (Param1 == null || Param1 == undefined) {
        Param1 = "0";
    }

    var jsonObj = {
        OperatorID: OperatorID,
        Opcode: Opcode,
        ProviderID: ProviderID,
        index: i,
        Param1: Param1
    };
    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ProviderConfiguration.aspx/UpdateOperator",
        data: jData,
        dataType: "json",
        success: function (data) {

            if (data.d >= 1) {
                $.alert.open({
                    content: "Provider Operator detail updated successfully", icon: 'confirm', title: ClientName,
                    callback: function () {
                        //select(RechargeProviderID);
                    }
                });
            }
            else {
                $.alert.open({
                    content: String.format( "Something went wrong during updating operator"), icon: 'confirm', title: ClientName,
                    callback: function () {
                        //select(RechargeProviderID);
                    }
                });
            }
        },
        error: function (data) {
        }
    });

}