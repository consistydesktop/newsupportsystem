﻿function ServiceMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ProjectAssignScreen.aspx/SelectServiceName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataServiceMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}
function BindDataServiceMaster(data) {
    $("#inputService").html("");
    $("#inputService").append("<option value=0>Select Service </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].ServiceID, data.d[i].ServiceName));
        $("#inputService").append(strRowDropdown);
    }
    $("#inputService").chosen();
}

function UserInformationSelectEmployeeName() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ProjectAssignScreen.aspx/SelectEmployeeName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindEmployeeNames(data);
        },
        error: function (result) {
            $.alert.open({ content: "Developers  not found", icon: 'error', title: ClientName, });
        }
    });
}
function BindEmployeeNames(data) {
    $("#inputEmployee").html("");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
        $("#inputEmployee").append(strRowDropdown);
    }
    $("#inputEmployee").chosen();
}

function ProductMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ProjectAssignScreen.aspx/SelectProductName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataProductMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Products not found", icon: 'error', title: ClientName, });
        }
    });
}
function BindDataProductMaster(data) {
    $("#inputProductType").html("");
    $("#inputProductType").append("<option value='0'>Product type </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{1}'> {1} </option> ", data.d[i].ProductID, data.d[i].ProductName));
        $("#inputProductType").append(strRowDropdown);
    }
    $("#inputProductType").chosen();
}

function OtherProduct() {
    var ProductName = $("#inputProductType").val();
    if (ProductName == "Other") {
        $("#labelinputOtherProductType").show();
        $("#inputOtherProductType").show();
        $("#inputOtherProductType").focus();
    }
    else {
        $("#labelinputOtherProductType").hide();
        $("#inputOtherProductType").hide();
        $("#inputProductType").focus();
    }
}

function ProjectAssignScreenValidation() {
    var ProjectName = $("#inputProjectName").val();
    var selected = $("#inputEmployee ").val();
    var EmailID = $("#inputEmail").val();
    var ServiceID = $("#inputService").val();
    var MobileNo = $("#inputMobileNumber").val();

    var Date = $("#inputExpectedDeliveryDate").val();
    if (ProjectName == "") {
        $.alert.open({
            content: String.format("Please Enter Project Name"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputProjectName").focus();
            }
        });
        return false;
    }

    if (ServiceID == 0) {
        $.alert.open({
            content: String.format("Please Select Service"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputService").focus();
            }
        });
        return false;
    }

    if (selected == "") {
        $.alert.open({
            content: String.format("Please select developer"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputEmployee").focus();
            }
        });

        return false;
    }

    if (EmailID != "") {
        //   if (!EmailValidation(EmailID)) {
        $.alert.open({
            content: String.format("Please enter a valid email address"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtEmail").focus();
            }
        });
        return false;
        //  }
    }
    if (MobileNo.length != 0) {
        if (MobileNo.length != 10) {
            $.alert.open({
                content: String.format("Please enter a valid Mobile No"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#txtMobileNumber").focus();
                }
            });
            return false;
        }
    }

    if (Date == "") {
        $.alert.open({
            content: String.format("Please select a delivery date"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputExpectedDeliveryDate").focus();
            }
        });
        return false;
    }
}

$("#SubmitButton").click(function (evt) {
    if (ProjectAssignScreenValidation() == false) {
        return false;
    }
    var File = $("#inputAttachment").val();

    if (File == "") {
        var result = 0;
        ProjectDetailMasterInsert(result);
    }
    else {
        var data = new FormData();
        var fileUpload = $("#inputAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandler.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else { ProjectDetailMasterInsert(result); }

            },
            error: function (err) {
                alert('Error:' + err)
            }
        });
    }
    evt.preventDefault();
});


function ShowResponseFile(result) {

    $.alert.open({
        content: result, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}



function ProjectDetailMasterInsert(result) {
    var ProjectName = $("#inputProjectName").val();
    var FileIDs = result;
    var ProductName = $("#inputProductType ").val();

    if (ProductName == "Other") {
        ProductName = $("#inputOtherProductType").val();
    }

    var ServiceID = $("#inputService").val();
    var selected = $("#inputEmployee ").val();
    var Description = $("#inputDescription").val();
    var Client = $("#inputClientName").val();
    var EmailID = $("#inputEmail").val();
    var MobileNumber = $("#inputMobileNumber").val();
    var ExpectedDelivery = $("#inputExpectedDeliveryDate").val();

    for (Name in selected) {
        var UserID;
        UserID = selected[Name];

        var jSonObj = {
            ProjectName: ProjectName,
            ProductName: ProductName,
            FileIDs: FileIDs,
            UserID: UserID,
            ServiceID: ServiceID,
            Description: Description,
            ClientName: Client,
            EmailID: EmailID,
            MobileNumber: MobileNumber,
            ExpectedDelivery: ExpectedDelivery
        };
        JSonObjects.push(jSonObj);
    }

    var JData = JSON.stringify({ ObjModel: JSonObjects });
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ProjectAssignScreen.aspx/Insert",
        data: JData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            ShowResponse(data);
            ResetAll();
        }
    });

    $("#labelinputOtherProductType").css('display', 'none');
    $("#inputOtherProductType").css('display', 'none');
    ResetAll();
}

function ProjectDetailMasterSelectByID(ProjectDetailMasterID) {
    var jsonObj = {
        ProjectDetailMasterID: ProjectDetailMasterID,
    };
    var jData = JSON.stringify(jsonObj);
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ProjectAssignScreen.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#loader").hide();
            BindForEdit(data);
        },
        error: function (result) {
            $.alert.open({ content: "Not Found", icon: 'error', title: ClientName, });
        }
    });
}

function BindForEdit(data) {
    var UserIDs = [];

    for (var i = 0; i < data.d.length; i++)
        UserIDs[i] = data.d[i].UserID;

    $("#inputProjectName").val(data.d[0].ProjectName);

    $("#inputService ").val(data.d[0].ServiceID).trigger('chosen:updated');
    var Service = $("#inputService ").val();

    if (Service == null || Service == undefined) {
        ServiceMasterSelect();
        $("#inputService ").val(data.d[0].ServiceID).trigger('chosen:updated');
    }


    $("#inputProductType ").val(data.d[0].ProductName).trigger('chosen:updated');

    var ProductType = $("#inputProductType ").val();
    if (ProductType == null || ProductType == undefined) {
        ProductMasterSelect();
        $("#inputProductType ").val(data.d[0].ProductName).trigger('chosen:updated');
    }


    var ProductName = $("#inputProductType ").val();

    if (ProductName == "Other") {
        $("#labelinputOtherProductType").show();
        $("#inputOtherProductType").show();
        $("#inputOtherProductType").val(data.d[0].ProductName);
        $("#inputOtherProductType").focus();
    }

    $("#inputClientName").val(data.d[0].ClientName);
    $("#inputEmail").val(data.d[0].EmailID);
    $("#inputMobileNumber").val(data.d[0].MobileNumber);
    $("#ProjectDetailMasterID").html(ProjectDetailMasterID);
    $("#inputDescription").val(data.d[0].Description);
    $("#inputExpectedDeliveryDate").val(data.d[0].ExpectedDelivery);


    $("#inputEmployee").val(UserIDs).trigger('chosen:updated');
    var Employess = $("#inputEmployee").val();

    if ((Employess == null || Employess == undefined)) {
        UserInformationSelectEmployeeName();
        $("#inputEmployee").val(UserIDs).trigger('chosen:updated');
    }


    $("#DownloadAttachmentButton").show();
}

function ProjectDetailMasterUpdate(result) {
    var ProjectDetailMasterID = $("#ProjectDetailMasterID").text();
    var ProjectName = $("#inputProjectName").val();
    var FileIDs = result;

    var ProductName = $("#inputProductType ").val();
    if (ProductName == "Other") {
        var ProductName = $("#inputOtherProductType").val();
    }
    if (ProductName == 0) {
        ProductName = "";
    }

    var ServiceID = $("#ddlServiceName").val();
    var selected = $("#inputEmployee ").val();
    var Description = $("#inputDescription").val();
    var Client = $("#inputClientName").val();
    var EmailID = $("#inputEmail").val();
    var MobileNumber = $("#inputMobileNumber").val();
    var ExpectedDelivery = $("#inputExpectedDeliveryDate").val();


    for (Name in selected) {
        var UserID;
        UserID = selected[Name];

        var jSonObj = {
            ProjectDetailMasterID: ProjectDetailMasterID,
            ProjectName: ProjectName,
            ProductName: ProductName,
            FileIDs: FileIDs,
            UserID: UserID,
            ServiceID: ServiceID,
            Description: Description,
            ClientName: Client,
            EmailID: EmailID,
            MobileNumber: MobileNumber,
            ExpectedDelivery: ExpectedDelivery
        };
        JSonObjects.push(jSonObj);
    }

    var JData = JSON.stringify({ ObjModel: JSonObjects });

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to update',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ProjectAssignScreen.aspx/Update",
                data: JData,
                dataType: "json",
                success: function (data) {
                    ShowResponse(data);
                    $("#lblProduct").css('display', 'none');
                    $("#txtProductName").css('display', 'none');
                    location.href = "ViewAssignedProjectDetails.aspx";
                },
                error: function (result) {
                    ShowResponse(data);
                }
            });
        },
    });
}

function ShowResponse(data) {

    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {

        }
    });
}

function ResetAll() {
    $("#inputProjectName").val("");
    $("#inputService").val('0').trigger('chosen:updated');
    $("#inputProductType").val('0').trigger('chosen:updated');
    $("#inputOtherProductType").val("");
    $("#inputEmployee").val('0').trigger('chosen:updated');
    $("#inputAttachment").val("");
    $("#inputClientName").val("");
    $("#inputEmail").val("");
    $("#inputMobileNumber").val("");
    $("#inputExpectedDeliveryDate").val("");
    $("#inputDescription").val("");
    $("#ProjectDetailMasterID").text("");
    $("#inputProjectName").focus();
}

function ProjectUpdate() {
    if (ProjectAssignScreenValidation() == false) {
        return false;
    }
    var File = $("#inputAttachment").val();
    if (File == "") {
        var result = 0;
        ProjectDetailMasterUpdate(result);
    }

    else {
        var data = new FormData();
        var fileUpload = $("#inputAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandler.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {
                ProjectDetailMasterUpdate(result);
            },

            error: function (result) {
                $.alert.open({ content: "Something went wrong", icon: 'error', title: ClientName, });
            }
        });
    }
}

function DownloadAttachment() {

    var ProjectDetailMasterID = $("#ProjectDetailMasterID");
    if (window.location.href.indexOf("ProjectDetailMasterID") > -1) {
        var url = window.location.href;
        var id = url.split('=')
        ProjectDetailMasterID = id[1];
    }




    var jsonObj = {
        ProjectDetailMasterID: ProjectDetailMasterID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ProjectAssignScreen.aspx/SelectAttachment",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BindDataAttachment(data);
            $("#modal_attachment").modal('show');
        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function BindDataAttachment(data) {
    $("#Attachments").html("");
    $("#Attachments").html("<thead><tr><th>Sr.No</th><th>Attachment</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format("<td class='text-center'> <a href='{0}' target='_blank'>{1}</a> </td>", data.d[i].Attachment, data.d[i].OriginalFileName));
        strRow.push('</tr>');
        $("#Attachments").append(strRow.join(""));
    }
}