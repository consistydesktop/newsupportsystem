﻿function SelectName() {
    var Usertype = $("#ddlUserType").val();

    var jsonObj = {
        Usertype: Usertype,
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "NewMessage.aspx/SelectName",
        data: jData,
        dataType: "JSON",
        success: function (data) {
            $("#ddlName").html("");

            for (var i = 0; i < data.d.length; i++) {
                var strRowDropdown = [];
                strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].UserType + '-' + data.d[i].Name));
                $("#ddlName").append(strRowDropdown);
            }
            $('#ddlName').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 220,
                enableCaseInsensitiveFiltering: true,
                enableFiltering: true
            });

            $('#ddlName').multiselect('updateButtonText');
            $("#ddlName").trigger("multiselect:updated");
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function MessageMasterInsert() {
    var Usertype = $("#ddlUserType").val();
    var selected = $("#ddlName").val();
    var Message = $("#txtEditor ").val();

    if (Usertype == '0') {
        $.alert.open({
            content: String.format("Please select user type"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#ddlUserType").focus();
            }
        });

        return false;
    }

    if (selected.length == 0) {
        $.alert.open({
            content: String.format("Please select name"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#ddlName").focus();
            }
        });
        return false;
    }

    if (Message == "") {
        $.alert.open({
            content: String.format("Please enter message"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtEditor").focus();
            }
        });
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Are you want to send message',
        icon: 'confirm',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }
            if (button.toUpperCase() != 'Yes'.toUpperCase()) {
                return false;
            }

            for (Name in selected) {
                var UserID;
                UserID = selected[Name];

                var jsonObj = {
                    Usertype: Usertype,
                    UserID: UserID,
                    Message: Message,
                };

                var jData = JSON.stringify(jsonObj)
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "NewMessage.aspx/Insert",
                    data: jData,
                    dataType: "json",
                    contentType: "application/json",

                    success: function (data) {
                        location.reload();
                        //  ShowRespose(data);
                    },
                    //error: function (result) {
                    //    $.alert.open({
                    //        content: String.format("Somthing worng"), icon: 'error', title: ClientName,
                    //    });
                    //}
                });
            }
            $.alert.open({
                content: String.format("Message sent"), icon: 'warning', title: ClientName,
            });
        }
    });
}

function BindDataMessageMaster(data) {
    $("#MessageMasterDetails").html("");
    $("#MessageMasterDetails").html("<thead><tr><th>Sr.No</th><th>Date</th><th>UserType</th><th>Message</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td> {0} </td>', data.d[i].DOC));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Usertype));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Message));
        strRow.push('</tr>');
        $("#MessageMasterDetails").append(strRow.join(""));
    }

    $('#MessageMasterDetails').dataTable({
        "scrollX": true,
        "bDestroy": true,
        dom: 'Blfrtip',
        buttons: ['excelHtml5', 'pdfHtml5']
    });
}

function MessageMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "NewMessage.aspx/Select",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindDataMessageMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not Found", icon: 'error', title: ClientName, });
        }
    });
}

function ShowRespose(data) {
    Icon = data.d.Icon;
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            MessageMasterSelect();
            Reset();
        }
    });
}

function Reset() {
    $("#ddlUserType").val('0');
    $('#ddlName').multiselect('refresh');
    var htmlEditorExtender = $('.ajax__html_editor_extender_texteditor');
    htmlEditorExtender.html("");
}

function CheckUserSelected() {
    var Downline = $("#ddlName").val();

    if (Downline == "0") {
        $('#ddlName').empty();
        $('#ddlName').trigger("chosen:updated");
        $('#ddlName').append('<option value="0">--AllUsers--</option>');
        $('#ddlName').trigger("chosen:updated");
    } else if (Downline != 0) {
        $("#ddlName option[value='0']").remove();
        $('#ddlName').trigger("chosen:updated");
    } else if (Downline == "") {
        getDownline();
    }
}