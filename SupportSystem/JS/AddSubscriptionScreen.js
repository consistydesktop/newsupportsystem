﻿function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AddSubscriptionScreen.aspx/SelectSystemName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataSystemMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSystemMaster(data) {
    $("#inputSystemName").html("");
    $("#inputSystemName").append("<option value=0>Select System </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#inputSystemName").append(strRowDropdown);
    }
    $("#inputSystemName").chosen();
}

function DesktopMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AddSubscriptionScreen.aspx/SelectDesktop",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataDesktopMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataDesktopMaster(data) {
    $("#inputDesktopType").html("");
    $("#inputDesktopType").append("<option value=0>Select Desktop </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].DesktopID, data.d[i].Desktop));
        $("#inputDesktopType").append(strRowDropdown);
    }
    $("#inputDesktopType").chosen();
}

function EmailValidation(EmailID) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(EmailID)) {
        return false;
    } else {
        return true;
    }
}

function AddSubscriptionScreenValidation() {
    var SystemID = $("#inputSystemName").val();
    var DesktopID = $("#inputDesktopType").val();
    var Description = $("#inputDescription").val();
    var DemoYear = $('input[name=inputDemoYear]:checked').val();
    var PaymentStatus = $('input[name=inputPaymentStatus]:checked').val();

    if (SystemID == 0) {
        $.alert.open({
            content: String.format("Please select system  name"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });

        return false;
    }

    if (DesktopID == 0) {
        $.alert.open({
            content: String.format("Please select desktop type"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputDesktopType").focus();
            }
        });
        return false;
    }

    if ($('input[name=inputDemoYear]:checked').length <= 0) {
        $.alert.open({
            content: String.format("Please select demo or year "), icon: 'warning', title: ClientName,
            callback: function () {
                $("#RbDemo").focus();
            }
        });
        return false;
    }

    if ($('input[name=inputPaymentStatus]:checked').length <= 0) {
        $.alert.open({
            content: String.format("Please select payment status "), icon: 'warning', title: ClientName,
            callback: function () {
                $("#RbPending").focus();
            }
        });
        return false;
    }

    if (Description == "") {
        $.alert.open({
            content: "Please Enter Description", title: ClientName,
            callback: function () {
                $("#inputDescription").focus();
            }
        });
        return false;
    }



    return true;
}

function SubscriptionMasterInsert() {
    if (!AddSubscriptionScreenValidation())
        return false;

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure ? To add a subscription',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }
            if (button.toUpperCase() != 'Yes'.toUpperCase()) {
                return false;
            }

            var SystemID = $("#inputSystemName").val();
            var DesktopID = $("#inputDesktopType").val();
            var Description = $("#inputDescription").val();
            var DemoYear = $('input[name=inputDemoYear]:checked').val();
            var PaymentStatus = $('input[name=inputPaymentStatus]:checked').val();

            var jsonObj = {
                SystemID: SystemID,
                DesktopID: DesktopID,
                Description: Description,
                DemoYear: DemoYear,
                PaymentStatus: PaymentStatus
            };
            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AddSubscriptionScreen.aspx/Insert",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowResponse(data);
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing worng"), icon: 'error', title: ClientName,
                    });
                }
            });
        }
    });
    return false;
}

function BindDataSubscriptionMaster(data) {
    if (data.d.length > 0) {
        $("#SubscriptionDetails").html("");
        $("#SubscriptionDetails").html("<thead><tr><th>#</th><th>Date</th><th>SystemName</th><th>Desktop</th><th>Description</th><th>Demo/Year</th><th>PaymentStatus</th><th>Edit</th><th>Delete</th></tr></thead><tbody></tbody>");
        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));

            strRow.push(String.format('<td> {0} </td>', data.d[i].DOCInString));
            strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
            strRow.push(String.format('<td> {0} </td>', data.d[i].Desktop));
            if (data.d[i].Description == "") {
                strRow.push(String.format('<td> {0} </td>', ""));
            }
            else {
                strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Description' style='width:50%; background-color: white; border-color: #fff'' onclick=\"ViewDescription('{0}')\" ><i class='fa fa-file-text' style='font-size:20px;color:blue'></i></button> </td>", (data.d[i].Description).replace(/\n/ig, '</br>')));
            }
            strRow.push(String.format('<td> {0} </td>', data.d[i].DemoYear));
            strRow.push(String.format('<td> {0} </td>', data.d[i].PaymentStatus));

            strRow.push(String.format("<td class='text-center'> <button type='button'class='btn'  title='Edit' style='width:50%; background-color: white; border-color: #fff'' onclick='return SubscriptionMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].SubscriptionID));
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50% background-color: white; border-color: #fff'' onclick='return SubscriptionMasterDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].SubscriptionID));

            strRow.push('</tr>');
            $("#SubscriptionDetails").append(strRow.join(""));
        }
    }
}

function ViewDescription(Description) {
    $("#modal_Sender").modal('show');
    $("#Description").html(Description);
}

function SubscriptionMasterSelect() {
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AddSubscriptionScreen.aspx/Select",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            $("#loader").hide();
            BindDataSubscriptionMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
    Reset();
}

function SubscriptionMasterDelete(SubscriptionID) {
    var SubscriptionID = SubscriptionID;
    var jsonObj = {
        SubscriptionID: SubscriptionID,
    };

    var jData = JSON.stringify(jsonObj)
    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AddSubscriptionScreen.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowResponse(data);
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('not updated'), icon: 'error', title: ClientName, });
        }
    });
}

function SubscriptionMasterSelectByID(SubscriptionID) {
    var SubscriptionID = SubscriptionID;
    $("#inputSystemName ").val("Please Wait...")
    $("#inputDesktopType ").val("Please Wait...")
    $("#inputDescription").val("Please Wait...");

    var jsonObj = {
        SubscriptionID: SubscriptionID,
    };
    var jData = JSON.stringify(jsonObj);
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AddSubscriptionScreen.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#loader").hide();
            $("#inputSystemName").val(data.d.SystemID).trigger('chosen:updated');
            var name = $("#inputSystemName option:selected").text();
           
     
            $("#inputSystemName").prop('disabled', 'disabled');
          
            $("#inputDesktopType ").val(data.d.DesktopID).trigger('chosen:updated');
            if (data.d.DemoYear == "Demo") {
                $("#inputDemo").prop("checked", true);
            }
            else {
                $("#inputYear").prop("checked", true);
            }

            if (data.d.PaymentStatus == "Pending") {
                $("#inputPending").prop("checked", true);
            }
            else {
                $("#inputReceived").prop("checked", true);
            }

            $("#inputDescription ").val(data.d.Description);

            $("#SubscriptionID").html(SubscriptionID);
            $("#SubmitButton").css('display', 'none');
            $("#UpdateButton").show();
        },
        error: function (result) {
            $.alert.open({ content: "not Updated", icon: 'error', title: ClientName, });
        }
    });
}

function SubscriptionMasterUpdate() {
    if (AddSubscriptionScreenValidation() == false) {
        return false;
    }

    var SystemID = $("#inputSystemName").val();
    var DesktopID = $("#inputDesktopType").val();
    var Description = $("#inputDescription").val();
    var DemoYear = $('input[name=inputDemoYear]:checked').val();
    var PaymentStatus = $('input[name=inputPaymentStatus]:checked').val();
    var SubscriptionID = $("#SubscriptionID").text();

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure ? To update a subscription',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                SystemID: SystemID,
                DesktopID: DesktopID,
                Description: Description,
                DemoYear: DemoYear,
                PaymentStatus: PaymentStatus,
                SubscriptionID: SubscriptionID
            };

            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AddSubscriptionScreen.aspx/Update",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowResponse(data);
                    $("#UpdateButton").css('display', 'none');
                    $("#SubmitButton").show();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not inserted'), icon: 'error', title: ClientName, });
        }
    });
    Reset();
}

function Reset() {
    $("#inputSystemName").val('0').trigger('chosen:updated');
    $("#inputDesktopType").val('0').trigger('chosen:updated');
    $("#inputDescription").val("");
    $("#SubscriptionID").val("");
    $('input[name=inputDemoYear]:checked').prop('checked', false);
    $('input[name=inputPaymentStatus]:checked').prop('checked', false);
    $("#inputDescription").val("");
    $("#SubscriptionID").html();
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
            SubscriptionMasterSelect();
        }
    });
}