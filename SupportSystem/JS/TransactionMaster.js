﻿function SelectUser() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Account.aspx/SelectUser",
        data: {},
        async: false,
        dataType: "JSON",
        success: function (data) {
            BindUser(data);
        },
        error: function (result) {
            ShowResponse(result);
        }
    });
}

function BindUser(data) {
    for (i = 0; i < data.d.length; i++)
        Users.push(data.d[i].Name);
}

function autocomplete(inp, arr) {
    var currentFocus;

    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;

        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;

        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");

        this.parentNode.appendChild(a);

        for (i = 0; i < arr.length; i++) {
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                b = document.createElement("DIV");

                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);

                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";

                b.addEventListener("click", function (e) {
                    inp.value = this.getElementsByTagName("input")[0].value;

                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });

    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            currentFocus++;

            addActive(x);
        } else if (e.keyCode == 38) { //up
            currentFocus--;

            addActive(x);
        } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        if (!x) return false;

        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);

        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

function TransactionMasterInsert()
{
    var Date = $("#inputDate").val();
    var Name = $("#inputName").val();
    var ReferenceNo = $("#inputReferenceNo").val();
    var Amount = $("#inputAmount").val();
    var TransactionType = $("#inputCRDR").val();
    var PaymentType = $("#inputPayment").val();
    var Remark = $("#inputRemark").val();
    var BankMasterID = $("#inputBank").val();
    var IsGST = 0;
  
    //if (document.getElementById("inputIsGST").checked) {
    //    IsGST = "1";
    //}

    if ($("#inputIsGST").is(":checked")) {
        IsGST = "1";
    }


    if (Date == "") {
        $.alert.open({
            content: "Please Select Date", title: ClientName,
            callback: function () {
                $("#inputDate").focus();
            }
        });
        return false;
    }

    if (Name == "" ) {
        $.alert.open({
            content: "Please Enter Name", title: ClientName,
            callback: function () {
                $("#inputName").focus();
            }
        });
        return false;
    }
    if (ReferenceNo == "") {
        $.alert.open({
            content: "Please Enter Reference Number", title: ClientName,
            callback: function () {
                $("#inputReferenceNo").focus();
            }
        });
        return false;
    }
    if (Amount == "") {
        $.alert.open({
            content: "Please Enter Amount", title: ClientName,
            callback: function () {
                $("#InputAmount").focus();
            }
        });
        return false;
    }
    if (TransactionType == "0") {
        $.alert.open({
            content: "Please Select Transaction Type", title: ClientName,
            callback: function () {
                $("#inputCRDR").focus();
            }
        });
        return false;
    }
    if (PaymentType == "0") {
        $.alert.open({
            content: "Please Select Payment Type", title: ClientName,
            callback: function () {
                $("#inputPayment").focus();
            }
        });
        return false;
    }
    
    if (BankMasterID == "0") {
        $.alert.open({
            content: "Please Select Bank Name", title: ClientName,
            callback: function () {
                $("#inputBankName").focus();
            }
        });
        return false;
    }
    if (Remark == "") {
        $.alert.open({
            content: "Please Enter Remark", title: ClientName,
            callback: function () {
                $("#inputRemark").focus();
            }
        });
        return false;
    }
    var JsonObj =
   {
       Date: Date,
       Name: Name,
       ReferenceNo: ReferenceNo,
       Amount: Amount,
       TransactionType: TransactionType,
       PaymentType: PaymentType,
       Remark: Remark,
       BankMasterID: BankMasterID,
       IsGST: IsGST

   };

    var JData = JSON.stringify(JsonObj);


    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Account.aspx/InsertTransactionMaster",
        data: JData,
        dataType: "json",
        success: function (data) {
            $.alert.open({
                content: data.d.Message, icon: 'warning', title: ClientName,
                callback: function () {
                    TransactionMasterResetAll()
                    
                }
            });

        },
        error: function (data) {
            
        }
    });
}
function TransactionMasterResetAll() {
    $("#inputDate").show();
    $("#inputName").val('');
    $("#inputReferenceNo").val('');
    $("#inputAmount").val('');
    $("#inputCRDR").val('');
    $("#inputPayment").val('');
    $("#inputRemark").val('');
    
}

function TransactionMasterSelect()
{
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/TransactionDetailReport.aspx/SelectTransactionMaster",
        data: {},
        dataType: "JSON",
        success: function (data) {
            $("#loader").hide();
            BindTransactionMaster(data);
        },
        error: function (result) {

        }
    });
}

function BindTransactionMaster(data)
{
    var mainData = [];
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];

        var color = 'black';
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', (i + 1), color));

        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].Date, color));
        strRow.push(String.format('<td> <span style="color:{1}">{0}</span>  </td>', data.d[i].Name, color));

        strRow.push(String.format('<td> <span style="color:{1}"> {0}</span>  </td>', data.d[i].ReferenceNo, color));

        strRow.push(String.format('<td> <span style="color:{1}; float : right"> {0} </span> </td>', data.d[i].Amount.toFixed(2), color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].TransactionType, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].PaymentType, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].Remark, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].BankName, color));
      //  strRow.push(String.format('<td><button type="button" id="UpdateButton{0}" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="TransactionMasterForEdit({0})" >Edit</button>', data.d[i].TransactionMasterID));
        strRow.push(String.format("<td class='text-center'> <button type='button' id='UpdateButton{0}' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return TransactionMasterForEdit({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TransactionMasterID));

        mainData.push(strRow);
    }



    $("#TransactionDetails").html("");
    $("#TransactionDetails").html("<thead> <tr> <th>#</th> <th>Date</th> <th>Name</th> <th>ReferenceNo</th>  <th>Amount</th> <th>TransactionType</th> <th>PaymentType</th> <th>Remark</th> <th>BanKName</th>    <th>Action</th> </tr>  </thead>")
    $("#TransactionDetails").append("<tfoot> <tr> <th></th>   <th></th>       <th></th>         <th></th>      <th></th>       <th></th>               <th></th>           <th></th>          <th></th>                   <th></th>        </tr> </tfoot>")
    createDataTable('TransactionDetails', mainData, [4]);
}

function TransactionMasterForEdit(TransactionMasterID) {
    console.log(TransactionMasterID);
    location.href = "/Account.aspx?TransactionMasterID=" + TransactionMasterID;
}

function TransactionMasterSelectByDate() {

    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    var TransactionType = $("#inputCRDR").val();
    var PaymentType = $("#inputPayment").val();
    var BankMasterID = $("#inputBank").val();
    var IsGST = 0
    if ($("#inputIsGST").is(":checked")) {
        IsGST = "1";
    }
    if (FromDate == "") {
        $.alert.open({
            content: "Select From date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: "Select To date", title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });

        return false;
    }
    if (TransactionType == "")
    {
        $.alert.open({
            content: "Select Transaction Type ", title: ClientName,
            callback: function () {
                $("#inputCRDR").focus();
            }
        });

        return false;
    }
    if (PaymentType == "") {
        $.alert.open({
            content: "Select Payment Type ", title: ClientName,
            callback: function () {
                $("#inputPayment").focus();
            }
        });

        return false;
    }
    var JsonObj =
    {
        FromDate: FromDate,
        ToDate: ToDate,
        TransactionType: TransactionType,
        PaymentType: PaymentType,
        BankMasterID: BankMasterID,
        IsGST : IsGST


    };
    var JData = JSON.stringify(JsonObj);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/TransactionDetailReport.aspx/SelectByDate",
        data: JData,
        dataType: "json",

        success: function (data) {
            BindTransactionMaster(data);
        },

        error: function (data) {
            ShowResponse(data.d);
        }
    });


}

function SelectBankName()
{
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Account.aspx/SelectBankMaster",
        data: {},
        dataType: "JSON",
        success: function (data) {
            BindBankName(data);
           
        },
        error: function (result) {

        }
    });
}

function BindBankName(data) {
    $("#inputBank").html("");
    $("#inputBank").append("<option  value='0'>Select Bank </option>");

    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].BankMasterID, data.d[i].BankName));
        $("#inputBank").append(strRowDropdown);
    }
    $("#inputBank").chosen();

}

function TransactionMasterSelectByID(TransactionMasterID) {
    var jsonObj = {
        TransactionMasterID: TransactionMasterID,

    };

    var jData = JSON.stringify(jsonObj);

    $("#loader").show();

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Account.aspx/SelectByID",
        data: jData,
        dataType: "json",
        success: function (data) {

            $("#loader").hide();
            BindTransactionMasterForEdit(data);

        }
    });
}
function BindTransactionMasterForEdit(data) {

    $("#inputBank").val(data.d[0][0].BankMasterID).trigger("chosen:updated");
    $("#inputName").val(data.d[0][0].Name);
    $("#inputReferenceNo").val(data.d[0][0].ReferenceNo);
    $("#inputAmount").val(data.d[0][0].Amount);
    $("#inputCRDR").val(data.d[0][0].TransactionType);
    $("#inputPayment").val(data.d[0][0].PaymentType);
    $("#inputRemark").val(data.d[0][0].Remark);

}

function TransactionMasterUpdate() {

    var Name = $("#inputName").val();
    var ReferenceNo = $("#inputReferenceNo").val();
    var Amount = $("#inputAmount").val();
    var TransactionType = $("#inputCRDR").val();
    var PaymentType = $("#inputPayment").val();
    var Remark = $("#inputRemark").val();
    var BankMasterID = $("#inputBank").val();
    var TransactionMasterID = $("#hdnTransactionMasterID").val();

    var jSonObj = {
        TransactionMasterID: TransactionMasterID,
        Name: Name,
        ReferenceNo: ReferenceNo,
        Amount: Amount,
        TransactionType: TransactionType,
        PaymentType: PaymentType,
        Remark: Remark,
        BankMasterID: BankMasterID

    };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Account.aspx/UpdateByID",
        data: JSON.stringify(jSonObj),
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
          //  ShowResponse(data);

            $.alert.open({
                content: data.d.Message, icon: 'warning', title: ClientName,
                callback: function () {
                    TransactionMasterResetAll();
                    location.href = '/Account.aspx';
                }
            });


           
           // 
           // location.reload();
        }
    });
}
function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            location.reload();
        }
    });
}


