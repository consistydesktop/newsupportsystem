﻿function DateValidation() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    var TodaysDate = $("#TodaysDate").val();

    if (FromDate == "") {
        $.alert.open({
            content: String.format("Please select fromdate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtFromDate").focus();
            }
        });

        return false;
    }
    if (FromDate > TodaysDate) {
        $.alert.open({
            content: String.format("Please select valid fromdate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtFromDate").focus();
            }
        });
    }

    if (ToDate == "") {
        $.alert.open({
            content: String.format("Please select todate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtToDate").focus();
            }
        });

        return false;
    }
    if (ToDate > TodaysDate) {
        $.alert.open({
            content: String.format("Please select valid todate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtToDate").focus();
            }
        });
    }
    if (ToDate > TodaysDate && FromDate > TodaysDate) {
        $.alert.open({
            content: String.format("Please select valid fromdate and todate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtFromDate").focus();
            }
        });
    }
}

function ProjectDetailMasterSelectByDate() {
    var FromDate = $("#txtFromDate").val();
    var ToDate = $("#txtToDate").val();
    var TodaysDate = $("#TodaysDate").val();

    if (DateValidation() == false) {
        return false;
    }
    var jsonObj = {
        FromDate: FromDate,
        ToDate: ToDate,
    };
    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewAssignedProjectDetails.aspx/SelectByDate",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BinddataProjectDetailMaster(data);
        },
        error: function (result) {
            $.alert.open({
                content: String.format("No data available between given date"), icon: 'error', title: ClientName,
            });
        }
    });
    return false;
}

function ProjectDetailMasterSelect() {
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewAssignedProjectDetails.aspx/Select",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            $("#loader").hide();
            BinddataProjectDetailMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BinddataProjectDetailMaster(data) {
    if (data.d.length < 1) {
    }
    else {
        $("#ViewAssignedEmployeeDetails").html("");
        $("#ViewAssignedEmployeeDetails").html("<thead><tr><th>Sr.No</th><th>Delivery Date</th><th>Project</th><th>Description</th><th>Developers</th><th>Status</th><th>Change Status</th><th>Documents</th><th>Edit</th><th>Delete</th></thead><tbody></tbody>");

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));

            strRow.push(String.format('<td>{0}</td>', data.d[i].ExpectedDelivery));
            strRow.push(String.format('<td>{0}</td>', data.d[i].ProjectName));
            if (data.d[i].Description != "") {
                strRow.push(String.format("<td class='text-center'> <button type='button' class='btn'  title='Description' style='width:50%; background-color: white; border-color: #fff' onclick=\"ViewDescription('{0}')\" ><i class='fa fa-file-text' style='font-size:20px;color:blue'></i></button> </td>", (data.d[i].Description).replace(/\n/ig, '</br>')));
            }
            else {
                strRow.push(String.format('<td>{0}</td>', "No Description"));
            }
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Developers' style='width:50%;  background-color: white; border-color: #fff' onclick=' ViewDevelopers({0})' ><i class='fa fa-user-plus' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].ProjectDetailMasterID));
            strRow.push(String.format('<td>{0} </td>', data.d[i].Status));
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Status' style='width:50%; background-color: white; border-color: #fff' onclick=' ProjectDetailMasterUpdate({0})' ><i class='fa fa-pencil' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].ProjectDetailMasterID));
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='download' style='width:50%; background-color: white; border-color: #fff' onclick='return DownloadAttachment({0})' ><i class='fa fa-download' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].ProjectDetailMasterID));
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return ProjectDetailMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].ProjectDetailMasterID));
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%  background-color: white; border-color: #fff' onclick='return ProjectDetailMasterDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].ProjectDetailMasterID));

            strRow.push('</tr>');
            $("#ViewAssignedEmployeeDetails").append(strRow.join(""));


          

        }
        addProperties(ViewAssignedEmployeeDetails);

    }
}

function DownloadAttachment(ProjectDetailMasterID) {
    var jsonObj = {
        ProjectDetailMasterID: ProjectDetailMasterID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewAssignedProjectDetails.aspx/SelectByProjectDetailMasterID",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BindDataAttachment(data);

            $("#modal_attachment").modal('show');
        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function BindDataAttachment(data) {
    $("#Attachments").html("");
    $("#Attachments").html("<thead><tr><th>Sr.No</th><th>Attachment</th><th>Delete</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format("<td class='text-center'> <a href='{0}'>{1}</a> </td>", data.d[i].Attachment, data.d[i].OriginalFileName));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%  background-color: white; border-color: #fff' onclick='return FileMasterDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].FileID));

        strRow.push('</tr>');
        $("#Attachments").append(strRow.join(""));
    }
}

function FileMasterDelete(FileID) {
    var jsonObj = {
        FileID: FileID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewAssignedProjectDetails.aspx/DeleteFile",
        data: jData,
        dataType: "JSON",
        success: function (data) {
            $("#modal_attachment").modal('hide');

            ShowRespose(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function ViewDevelopers(ProjectDetailMasterID) {
    var ProjectDetailMasterID = ProjectDetailMasterID;
    $("#ProjectDetailMasterID").html(ProjectDetailMasterID);
    var jsonObj = {
        ProjectDetailMasterID: ProjectDetailMasterID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewAssignedProjectDetails.aspx/SelectEmployee",
        data: jData,
        dataType: "JSON",
        success: function (data) {
            BindDataAssignedDevelopers(data);
            $("#modal_developers").modal('show');
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function ProjectDetailMasterUpdate(ProjectDetailMasterID) {
    $("#ProjectDetailMasterID").html(ProjectDetailMasterID);
    $("#modal_status").modal('show');
}

function ProjectDetailMasterUpdateStatus() {
    var Status = $("#ddlStatus").val();
    if (Status != "0") {
        Status = Status;
    }
    else {
        if (Status == "0") {
            $("#modal_status").modal('show');
            $("#ddlStatus").focus();
        }
    }

    var ProjectDetailMasterID = $("#ProjectDetailMasterID").text();
    Status = $("#ddlStatus").val();
    var jsonObj = {
        Status: Status,
        ProjectDetailMasterID: ProjectDetailMasterID,
    };
    var jData = JSON.stringify(jsonObj)

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to update status ',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ViewAssignedProjectDetails.aspx/Update",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                    location.reload();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not inserted'), icon: 'error', title: ClientName, });
        }
    });
}

function ProjectDetailMasterSelectByID(ProjectDetailMasterID) {
    location.href = "ProjectAssignScreen.aspx?ProjectDetailMasterID =" + ProjectDetailMasterID;
}

function BindDataAssignedDevelopers(data) {
    $("#Developers").html("");
    $("#Developers").html("<thead><tr><th>Sr.No</th><th>Developer</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format('<td>{0}</td>', data.d[i].Name));

        strRow.push('</tr>');
        $("#Developers").append(strRow.join(""));
    }
}

function ViewDescription(Description) {
    $("#modal_Sender").modal('show');
    $("#txtDescription").html(Description);
}

function ProjectDetailMasterDelete(ProjectDetailMasterID) {
    var ProjectDetailMasterID = ProjectDetailMasterID;
    var jsonObj = {
        ProjectDetailMasterID: ProjectDetailMasterID,
    };

    var jData = JSON.stringify(jsonObj)
    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ViewAssignedProjectDetails.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                    location.reload();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not updated'), icon: 'error', title: ClientName, });
        }
    });
}

function ShowRespose(data) {
    Icon = data.d.Icon;
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            ProjectDetailMasterSelect();
        }
    });
}

function Search() {
    if (!Validation())
        return false;

    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();

    var JsonObj =
   {
       FromDate: FromDate,
       ToDate: ToDate,
   };

    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewAssignedProjectDetails.aspx/Search",
        data: JData,
        dataType: "json",

        success: function (data) {
            if (data.d.length < 1) {
                $.alert.open({
                    content: "Record Not found", icon: 'error', title: ClientName,
                    callback: function () {
                    }
                });
            }
            else {
                BinddataProjectDetailMaster(data);
            }
        },

        error: function (data) {
        }
    });
}

function Validation() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();

    if (FromDate == "") {
        $.alert.open({
            content: "Select From date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: "Select To date", title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });
        return false;
    }

    return true;
}