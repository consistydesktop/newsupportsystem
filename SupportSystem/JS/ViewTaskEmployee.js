﻿function DateValidation() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    if (FromDate == "") {
        $.alert.open({
            content: "Select From date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: "Select To date", title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });

        return false;
    }

    if (new Date(FromDate) > new Date(ToDate)) {
        $.alert.open({
            content: "Invallid date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });

        return false;
    }

    return true;
}

function TaskMasterSelectForEmployee() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewTaskEmployee.aspx/Select",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BinddataTaskDetails(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BinddataTaskDetails(data) {
    if (data.d.length < 1) {
        $.alert.open({
            content: 'There is no task', icon: 'error', title: ClientName,
            callback: function () {
            }
        });
        return false;
    }
    $("#TaskDetails").html("");
    $("#TaskDetails").html("<thead><tr><th>#</th><th>System</th><th>Task</th><th>Due date</th><th>View</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        if (data.d[i].TaskStatus == '4') {
            color = 'red';
        }

        if (data.d[i].TaskStatus == '5') {
            color = 'orange';
        }

        if (data.d[i].TaskStatus == '3') {
            color = 'green';
        }

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Task));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Deadline));
        //    strRow.push(String.format('<td> <select id="inputStatus{0}" onchange="ChangeStatus({0})"><option value="Inprogress">Inprogress</option><option value="Completed">Resolved</option></select> </td>', data.d[i].TaskID));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='View' style='width:50%; background-color: white; border-color: #fff' onclick='ViewTaskDetail({0})' ><i class='fa fa-eye' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TaskID));

        strRow.push('</tr>');
        $("#TaskDetails").append(strRow.join(""));
    }
    var id = document.getElementById("TaskDetails");
    addProperties(id);
}

function ChangeStatus(TaskID) {
    $("#inputStatus" + TaskID).val();

}

function ViewTaskDetail(TaskID) {
    location.href = "ViewTaskDetailEmployee.aspx?TaskID=" + TaskID;
}

function ViewDescription(Description) {
    $("#modal_Sender").modal('show');
    $("#txtDescription").html(Description);
}

function DownloadAttachment(TaskID) {
    var jsonObj = {
        TaskID: TaskID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTaskEmployee.aspx/SelectAttachment",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BindDataAttachment(data);
            $("#modal_attachment").modal('show');
        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function BindDataAttachment(data) {
    $("#Attachments").html("");
    $("#Attachments").html("<thead><tr><th>Sr.No</th><th>Attachment</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format("<td class='text-center'> <a href='{0}'>{1}</a> </td>", data.d[i].Attachment, data.d[i].OriginalFileName));
        strRow.push('</tr>');
        $("#Attachments").append(strRow.join(""));
    }
}

function TaskMasterUpdate(TaskID) {
    $("#TaskID").html(TaskID);
    $("#modal_status").modal('show');
}

function TaskMasterMasterUpdateStatus() {
    var Status = $("#ddlStatus").val();
    var File = $("#inputTaskAttachment").val();
    if (Status != "0") {
        Status = Status;
    }
    else {
        if (Status == "0") {
            $.alert.open({
                content: String.format("Please select status"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#modal_status").modal('show');
                    $("#ddlStatus").focus();
                }
            });

            return false;
        }
    }

    if (File == 0) {
        TaskMasterUpdateForStatus(0);
    }
    else {
        var data = new FormData();
        var fileUpload = $("#inputTaskAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForTask.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else {
                    TaskMasterUpdateForStatus(result);
                }


            },
            error: function (err) {
                alert('Error:' + err)
            }
        });
    }
    Reset();
}
function ShowResponseFile(result) {

    $.alert.open({
        content: result, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}
function TaskMasterUpdateForStatus(result) {
    var Status = $("#ddlStatus").val();
    var TaskID = $("#TaskID").text();
    var FileIDs = result;
    var jsonObj = {
        Status: Status,
        TaskID: TaskID,
        FileIDs: FileIDs,
    };
    var jData = JSON.stringify(jsonObj)

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to update status ',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ViewTaskEmployee.aspx/UpdateStatus",
                data: jData,
                dataType: "json",
                success: function (data) {
                    location.reload();
                    TaskMasterSelectForEmployee();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not inserted'), icon: 'error', title: ClientName, });
        }
    });
}

function TaskMasterSelectByDate() {

    if (!DateValidation()) { return false; }
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();

    if (FromDate != "" && ToDate != "") {
        var jsonObj = {
            FromDate: FromDate,
            ToDate: ToDate,
        };
        var jData = JSON.stringify(jsonObj);

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "ViewTaskEmployee.aspx/Search",
            data: jData,
            dataType: "json",
            success: function (data) {
                BinddataTaskDetails(data);
            }
        });
    }
}