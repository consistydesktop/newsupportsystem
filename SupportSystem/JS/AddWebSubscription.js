﻿function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AddWebSubscription.aspx/SelectSystemName",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataSystemMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSystemMaster(data) {
    $("#inputSystemName").html("");
    $("#inputSystemName").append("<option value=0>Select System </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#inputSystemName").append(strRowDropdown);
    }
    $("#inputSystemName").chosen();
}

function AddWebSubscriptionScreenValidation() {
    var SystemID = $("#inputSystemName").val();
    var WebsiteURL = $("#inputWebsiteURL").val();
    var Description = $("#inputDescription").val();
    var EndDate = $('#inputEndDate').val();
    var PaymentStatus = $('input[name=inputPaymentStatus]:checked').val();
    var HostingIP = $('#inputHostingIP').val();


    if (SystemID == 0) {
        $.alert.open({
            content: String.format("Please select system  name"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });

        return false;
    }

    if (WebsiteURL == "") {
        $.alert.open({
            content: String.format("Please enter website URL"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputWebsiteURL").focus();
            }
        });
        return false;
    }

    if (EndDate=="") {
        $.alert.open({
            content: String.format("Please enter end date "), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputEndDate").focus();
            }
        });
        return false;
    }


    var d = new Date(EndDate);
    if (d == "Invalid Date") {
        $.alert.open({
            content: "Please enter valid date.", title: ClientName,
            callback: function () {
                $("#inputEndDate").focus();
            }
        });
        return false;
    }


    if ($('input[name=inputPaymentStatus]:checked').length <= 0) {
        $.alert.open({
            content: String.format("Please select payment status "), icon: 'warning', title: ClientName,
            callback: function () {
                $("#RbPending").focus();
            }
        });
        return false;
    }
    
    if (HostingIP == "") {
        $.alert.open({
            content: String.format("Please enter IP address"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputHostingIP").focus();
            }
        });
        return false;
    }




    return true;
}

function WebSubscriptionMasterInsert() {
    if (!AddWebSubscriptionScreenValidation())
        return false;

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure ? To add a subscription',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }
            if (button.toUpperCase() != 'Yes'.toUpperCase()) {
                return false;
            }

            var SystemID = $("#inputSystemName").val();
            var WebsiteURL = $("#inputWebsiteURL").val();

            var EndDate = $('#inputEndDate').val();
            var PaymentStatus = $('input[name=inputPaymentStatus]:checked').val();
            var Description = $("#inputDescription").val();
            var HostingIP = $('#inputHostingIP').val();

            var jsonObj = {
                SystemID: SystemID,
                WebsiteURL: WebsiteURL,
                HostingIP: HostingIP,
                EndDate: EndDate,
                PaymentStatus: PaymentStatus,

                Description: Description,

            };
            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AddWebSubscription.aspx/Insert",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowResponse(data);
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing went worng"), icon: 'error', title: ClientName,
                    });
                }
            });
        }
    });
    return false;
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
            WebSubscriptionMasterSelect();
        }
    });
}

function WebSubscriptionMasterSelect() {
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AddWebSubscription.aspx/Select",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            $("#loader").hide();
            BindDataSubscriptionMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
    Reset();
}

function BindDataSubscriptionMaster(data) {
    if (data.d.length > 0) {
        $("#WebSubscriptionDetails").html("");
        $("#WebSubscriptionDetails").html("<thead><tr><th>#</th><th>Product </th><th>System </th><th>URL</th><th>End Date</th><th>Payment Status</th><th>Edit</th><th>Delete</th></tr></thead><tbody></tbody>");
        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));
            strRow.push(String.format('<td> {0} </td>', data.d[i].ProductName));
            strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
            strRow.push(String.format('<td> {0} </td>', data.d[i].WebsiteURL));


            strRow.push(String.format('<td> {0} </td>', data.d[i].EndDateInString));

            strRow.push(String.format('<td> {0} </td>', data.d[i].PaymentStatus));

            strRow.push(String.format("<td class='text-center'> <button type='button'class='btn'  title='Edit' style='width:50%; background-color: white; border-color: #fff'' onclick='return WebSubscriptionMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].SubscriptionID));
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50% background-color: white; border-color: #fff'' onclick='return WebSubscriptionMasterDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].SubscriptionID));

            strRow.push('</tr>');
            $("#WebSubscriptionDetails").append(strRow.join(""));
        }

        addProperties(WebSubscriptionDetails);
    }
}

function Reset() {
    $("#inputSystemName").val('0').trigger('chosen:updated');

    $("#inputDescription").val("");
    $("#WebSubscriptionID").val("");
    $('input[name=inputPaymentStatus]:checked').prop('checked', false);
    $("#inputWebsiteURL").val("");
    $("#inputEndDate").val("");

    $("#inputHostingIP").val("");



}

function WebSubscriptionMasterSelectByID(WebSubscriptionID) {
    $("#inputWebsiteURL").val("Please wait");
    $("#inputEndDate").val("Please wait");
    $("#inputHostingIP").val("Please wait");
    $("#inputDescription ").val("Please wait");

    var jsonObj = {
        WebSubscriptionID: WebSubscriptionID,
    };
    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AddWebSubscription.aspx/SelectByID",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BindDataForEdit(data);
        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing went worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function BindDataForEdit(data) {

    $("#inputSystemName").val(data.d.SystemID).trigger('chosen:updated');


    if (data.d.PaymentStatus == "Pending") {
        $("#inputPending").prop("checked", true);
    }
    else {
        $("#inputReceived").prop("checked", true);
    }
    $("#inputWebsiteURL").focus();

    $("#inputWebsiteURL").val(data.d.WebsiteURL);
    $("#inputEndDate").val(data.d.EndDateInString);

    $("#inputHostingIP").val(data.d.HostingIP);
    $("#inputDescription ").val(data.d.Description);

    $("#WebSubscriptionID").html(data.d.SubscriptionID);
    $("#SubmitButton").css('display', 'none');
    $("#UpdateButton").show();

}

function WebSubscriptionMasterDelete(WebSubscriptionID) {
    var jsonObj = {
        WebSubscriptionID: WebSubscriptionID,


    };
    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AddWebSubscription.aspx/Delete",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            ShowResponse(data);
        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing went worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function WebSubscriptionMasterUpdate() {
    if (!AddWebSubscriptionScreenValidation())
        return false;

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure ? To update a subscription',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }
            if (button.toUpperCase() != 'Yes'.toUpperCase()) {
                return false;
            }
            var WebSubscriptionID = $("#WebSubscriptionID").html();
            var SystemID = $("#inputSystemName").val();
            var WebsiteURL = $("#inputWebsiteURL").val();

            var EndDate = $('#inputEndDate').val();
            var PaymentStatus = $('input[name=inputPaymentStatus]:checked').val();
            var Description = $("#inputDescription").val();
            var HostingIP = $('#inputHostingIP').val();

            var jsonObj = {
                WebSubscriptionID:WebSubscriptionID,
                SystemID: SystemID,
                WebsiteURL: WebsiteURL,
                HostingIP: HostingIP,
                EndDate: EndDate,
                PaymentStatus: PaymentStatus,
                Description: Description,
            };
            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AddWebSubscription.aspx/Update",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowResponse(data);
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing went worng"), icon: 'error', title: ClientName,
                    });
                }
            });
        }
    });
    return false;
}
