﻿function TargetMasterInsert() {
    var Month = $("#inputMonth").val();
    var Year = $("#inputYear").val();
    var TargetAmount = $("#inputTargetAmount").val();



    if (Month == "") {
        $.alert.open({
            content: "Please Select Month", title: ClientName,
            callback: function () {
                $("#inputMonth").focus();
            }
        });
        return false;
    }

    if (Year == "") {
        $.alert.open({
            content: "Please Select Year", title: ClientName,
            callback: function () {
                $("#inputYear").focus();
            }
        });
        return false;
    }
    if (TargetAmount == "") {
        $.alert.open({
            content: "Please Enter Target Amount", title: ClientName,
            callback: function () {
                $("#inputTargetAmount").focus();
            }
        });
        return false;
    }


    var JsonObj =
   {
       Month: Month,
       Year: Year,
       TargetAmount: TargetAmount,


   };

    var JData = JSON.stringify(JsonObj);


    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Account.aspx/InsertTargetMaster",
        data: JData,
        dataType: "json",
        success: function (data) {
           // ShowResponse(data);

            $.alert.open({
                content: data.d.Message, icon: 'warning', title: ClientName,
                callback: function () {
                   
                }
            });
        },
        error: function (data) {
            ShowResponse(data);
        }
    });
}
function TargetMasterSelect() {
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Account.aspx/SelectTargetMaster",
        data: {},
        async: false,
        dataType: "JSON",
        success: function (data) {
            $("#loader").hide();
            BindTargetMaster(data);
        },
        error: function (result) {
            $("#loader").hide();

        }
    });
    }
function BindTargetMaster(data) {
    var mainData = [];
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];

        var color = 'black';
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', (i + 1), color));

        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].Month, color));
        strRow.push(String.format('<td> <span style="color:{1}">{0}</span>  </td>', data.d[i].Year, color));
        strRow.push(String.format('<td> <span style="color:{1}">{0}</span>  </td>', data.d[i].TargetAmount, color));

      
       // strRow.push(String.format('<td><button type="button" id="UpdateButton{0}" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="TargetMasterSelectByID({0})" >Edit</button>', data.d[i].KnowledgeBaseID));
        strRow.push(String.format("<td class='text-center'> <button type='button' id='UpdateButton{0}' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return TargetMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].KnowledgeBaseID));

        mainData.push(strRow);
    }



    $("#TargetMasterDetails").html("");

    $("#TargetMasterDetails").html("  <thead> <tr>  <th>#</th> <th>Month</th> <th>Year</th> <th>TargetAmount</th> <th>Action</th>    </tr></thead>")
    $("#TargetMasterDetails").append("<tfoot> <tr>  <th></th>  <th></th>         <th></th>       <th></th>           <th></th>         </tr> </tfoot>")
    createDataTable('TargetMasterDetails', mainData, []);
}
