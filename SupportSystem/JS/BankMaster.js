﻿function BankMasterInsert() {
    var BankName = $("#inputBankName").val();
    var AccountNumber = $("#inputAccountNumber").val();
    var BranchName = $("#inputBranchName").val();
    var IFSCCode = $("#inputIFSC").val();


    if (BankName == "") {
        $.alert.open({
            content: "Please Enter Bank Name", title: ClientName,
            callback: function () {
                $("#inputBankName").focus();
            }
        });
        return false;
    }

    if (AccountNumber == "") {
        $.alert.open({
            content: "Please Enter Acount Number", title: ClientName,
            callback: function () {
                $("#inputAccountNumber").focus();
            }
        });
        return false;
    }
    if (BranchName == "") {
        $.alert.open({
            content: "Please Enter Branch Name", title: ClientName,
            callback: function () {
                $("#inputBranchName").focus();
            }
        });
        return false;
    }
    if (IFSCCode == "") {
        $.alert.open({
            content: "Please Enter IFSC code", title: ClientName,
            callback: function () {
                $("#inputIFSC").focus();
            }
        });
        return false;
    }
    var JsonObj =
   {
       BankName: BankName,
       AccountNumber: AccountNumber,
       BranchName: BranchName,
       IFSCCode: IFSCCode

   };

    var JData = JSON.stringify(JsonObj);


    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Account.aspx/InsertBankMaster",
        data: JData,
        dataType: "json",
        success: function (data) {
            ShowResponse(data);
        },
        error: function (data) {
            ShowResponse(data);
        }
    });
}

function BankMasterSelect()
{
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Account.aspx/SelectBankMaster",
        data: {},
        async: false,
        dataType: "JSON",
        success: function (data) {
            $("#loader").hide();
            BindBankMaster(data);
        },
        error: function (result) {
            $("#loader").hide();
        }
    });
}
function BindBankMaster(data) {
    var mainData = [];
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];

        var color = 'black';
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', (i + 1), color));

        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].BankName, color));
        strRow.push(String.format('<td> <span style="color:{1}">{0}</span>  </td>', data.d[i].AccountNumber, color));
        strRow.push(String.format('<td> <span style="color:{1}">{0}</span>  </td>', data.d[i].BranchName, color));

        strRow.push(String.format('<td> <span style="color:{1}"> {0}</span>  </td>', data.d[i].IFSCCode, color));


        //strRow.push(String.format('<td><button type="button" id="UpdateButton{0}" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="BankMasterSelectByID({0})" >Edit</button>', data.d[i].KnowledgeBaseID));
        strRow.push(String.format("<td class='text-center'> <button type='button' id='UpdateButton{0}' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return BankMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].KnowledgeBaseID));

        mainData.push(strRow);
    }



    $("#BankDetails").html("");

    $("#BankDetails").html("  <thead> <tr>  <th>#</th> <th>Name</th> <th>AccountNumber</th> <th>BranchName</th>  <th>IFSCCode</th> <th>Action</th> </tr></thead>")
    $("#BankDetails").append("<tfoot> <tr>  <th></th>  <th></th>         <th></th>              <th></th>            <th></th>         <th></th>       </tr> </tfoot>")
    createDataTable('BankDetails', mainData, []);
}
function ResetAll() {
    $("#inputBankName").show();
    $("#inputAccountNumber").val('');
    $("#inputBranchName").val('');
    $("#inputIFSC").val('');
   

}
