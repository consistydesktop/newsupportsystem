﻿function CreateNewProject() {
    var ProjectName = $("#inputProjectName").val();

    if (ProjectName == "") {
        $.alert.open({
            content: "Enter Project Name ", title: ClientName,
        });
    }
    else {
        var btn = document.createElement("DIV");
        btn.setAttribute("id", "Div_" + ProjetBugID);
        btn.setAttribute("class", "btn btn-info w-100 mt-4 logbutton buttons");
        btn.setAttribute("onclick", "AccessProject('{0}')", ProjetBugID);
        var t = document.createTextNode(ProjectName);
        btn.appendChild(t);

        NewProjects.appendChild(btn);
        $("#NewProject").val("");

        var jsonObj =
            {
                ProjectName: ProjectName,
            };

        var jData = JSON.stringify(jsonObj);
        $.ajax({
            type: "POST",
            contentType: "Application/json;charset:UTF-8",
            url: "NewTestProject.aspx/Insert",
            data: jData,
            dataType: "JSON",
            title: ClientName,
            success: function (data) {
                ShowResponse(data);
            },
            error: function (result) {
                $.alert.open({ content: "Not Submitted", icon: 'error', title: ClientName, });
            }
        });
    }
}

function AccessProject(ProjectBugMasterID) {
    console.log("AccessProject called " + ProjectBugMasterID);
    location.href = "/InsertNewBug.aspx?ProjectBugMasterID=" + ProjectBugMasterID;
}

function ProjectBugMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "NewTestProject.aspx/Select",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataProjectBugMaster(data);
            BindDataProjectBugMasterForSelect(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataProjectBugMasterForSelect(data) {
    $("#inputSearchProjectName").html("");
    $("#inputSearchProjectName").append("<option  value='0'>Select Your Project </option>");

    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{1}'> {0} </option> ", data.d[i].ProjectName, data.d[i].ProjectBugMasterID));
        $("#inputSearchProjectName").append(strRowDropdown);
    }
    $("#inputSearchProjectName").chosen();
}

//function BindDataProjectBugMaster(data) {
//    if (data.d.length > 0) {
//        $("#NewProjects").html("");
//        $("#NewProjects").html("<thead><tr><th>#</th><th>Project Name</th><th>Delete</th></tr></thead><tbody></tbody>")
//        for (var i = 0; i < data.d.length; i++) {
//            var strRow = [];
//            var color = 'black';

//            strRow.push(String.format("<tr style='color:{0};'>", color));
//            strRow.push(String.format("<td>{0}</td>", (i + 1)));
//            strRow.push(String.format('<td>  <div class="card row" ><div class="card-header" style="background-color: darkgray"><a class="ui-button" onclick="AccessProject({1})" data-toggle="collapse" href="#ThisBody"   aria-expanded="false"><i class="fa fa-arrow-down" aria-hidden="true"></i> </a>  {0} <div style="float:right"> <label >Total Bugs : {2}</label></div></div> </div> </td> ', data.d[i].ProjectName, data.d[i].ProjectBugMasterID, data.d[i].TotalIssue));
//            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return ProjectBugMasterDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].ProjectBugMasterID));
//            strRow.push('</tr>');
//            $("#NewProjects").append(strRow.join(""));
//        }
//    }
//}

function ProjectBugMasterDelete(ProjectBugMasterID) {
    var jsonObj = {
        ProjectBugMasterID: ProjectBugMasterID
    };

    var jData = JSON.stringify(jsonObj)

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "Application/json;charset:UTF-8",
                url: "NewTestProject.aspx/Delete",
                data: jData,
                dataType: "JSON",
                success: function (data) {
                    ShowResponse(data)
                },
                error: function (result) {
                    $.alert.open({ content: "Project not deleted", icon: 'error', title: ClientName, });
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not deleted'), icon: 'error', title: ClientName, });
        }
    });
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            ProjectBugMasterSelect();
            $("#inputProjectName").val("");
        }
    });
}

function OpenProject() {
    var ProjectBugMasterID = $("#inputSearchProjectName").val();
    AccessProject(ProjectBugMasterID);
}

function BindDataProjectBugMaster(data)
{
    if (data.d.length > 0) {
             $("#NewProjects").html("");
                $("#NewProjects").html("<thead><tr><th>#</th><th>Project Name</th><th>New</th><th>Resolved</th><th>Inprogress</th><th>Total Issue</th></tr></thead><tbody></tbody>")
                for (var i = 0; i < data.d.length; i++) {
                    var strRow = [];
                    var color = 'black';

                    strRow.push(String.format("<tr style='color:{0};'>", color));
                    strRow.push(String.format("<td>{0}</td>", (i + 1)));
                    strRow.push(String.format('<td> <span style="color:{1}" onclick="AccessProject({2})"> {0} </span> </td>', data.d[i].ProjectName, color, data.d[i].ProjectBugMasterID));
                    strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].NEW, color));
                    strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].RESOLVED, color));
                    strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].INPROGRESS, color));
                    strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].TotalIssue, color));
                 
                    strRow.push('</tr>');
                    $("#NewProjects").append(strRow.join(""));
                }
    }
    addProperties(NewProjects);
}