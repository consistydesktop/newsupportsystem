﻿function Vallidation() {
    var Date = $("#inputDate").val();
    var Description = $("#inputDescription").val();

    if (Date == "") {
        $.alert.open({
            content: "Select date", title: ClientName,
            callback: function () {
                $("#inputDate").focus();
            }
        });
        return false;
    }

    if (Description == "") {
        $.alert.open({
            content: "Enter description", title: ClientName,
            callback: function () {
                $("#inputDate").focus();
            }
        });
        return false;
    }

    return true;
}

function HolidayMasterInsert() {
    var Date = $("#inputDate").val();
    var Description = $("#inputDescription").val();

    if (!Vallidation())
        return false;

    var jsonObj = {
        Date: Date,
        Description: Description,
    };

    var JData = JSON.stringify(jsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AddHoliday.aspx/Insert",
        data: JData,
        dataType: "json",

        success: function (data) {
           // $("#ResponseMsgLbl").text(data.d.Message);
            ShowResponse(data);
            HolidayMasterSelect();
        },

        error: function (data) {
            ShowResponse(data);
        }
    });

}

function HolidayMasterUpdate() {
    var DayID = $("#HolidayID").html();
    var Date = $("#inputDate").val();
    var Description = $("#inputDescription").val();

    if (!Vallidation())
        return false;

    var jsonObj = {
        DayID: DayID,
        Date: Date,
        Description: Description,
    };

    var JData = JSON.stringify(jsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AddHoliday.aspx/Update",
        data: JData,
        dataType: "json",

        success: function (data) {
        //    $("#ResponseMsgLbl").text(data.d.Message);
            ShowResponse(data);
            HolidayMasterSelect();
        },

        error: function (data) {
            ShowResponse(data);
        }
    });

}

function HolidayMasterDelete(DayID) {
   
    var jsonObj = {
        DayID: DayID,
    };

    var JData = JSON.stringify(jsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AddHoliday.aspx/Delete",
        data: JData,
        dataType: "json",

        success: function (data) {
          //  $("#ResponseMsgLbl").text(data.d.Message);
            ShowResponse(data);
            HolidayMasterSelect();
        },

        error: function (data) {
            ShowResponse(data);
        }
    });

}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}

function HolidayMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AddHoliday.aspx/Select",
        data: {},
        dataType: "JSON",
        success: function (data) {
            BindDataHolidayMaster(data);
        },
        error: function (result) {
            alert("Not Found");
        }
    });
}

function BindDataHolidayMaster(data) {
    $("#HolidayDetails").html("");
    $("#HolidayDetails").html("<thead><tr><th>Sr.No</th><th>Date</th><th>Description</th><th>Delete</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td class="Grcenter"> {0} </td>', (i + 1)));
        strRow.push(String.format('<td> {0} </td>', data.d[i].DOCInString));

        strRow.push(String.format('<td> {0} </td>', data.d[i].Description));


    //    strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return HolydayMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].DayID));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return HolidayMasterDelete({0})' ><i class='fa fa-trash-o' style='font-size:20px;color:red'></i></button> </td>", data.d[i].DayID));

        strRow.push('</tr>');
        $("#HolidayDetails").append(strRow.join(""));
    }

    $('#HolidayDetails').dataTable({
        destroy: true,
        dom: 'Blfrtip',
        buttons: ['excelHtml5', 'pdfHtml5']
    });
}

function HolydayMasterSelectByID(DayID) {

    $("#inputDate").val("Please Wait");
    $("#inputDescription").val("Please Wait");


   
    var jsonObj = {
        DayID: DayID,
    };

    var JData = JSON.stringify(jsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AddHoliday.aspx/SelectByID",
        data: JData,
        dataType: "JSON",
        success: function (data) {
            $("#inputDate").val(data.d[0].DOCInString);
            $("#inputDescription").val(data.d[0].Description);  
            $("#HolidayID").html(data.d[0].DayID);
            $("#SubmitButton").hide();
            $("#UpdateButton").show();
        },
        error: function (result) {
            alert("Not Found");
        }
    });
}