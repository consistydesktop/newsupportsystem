﻿var ClientName = 'Consisty';
function EmailValidation(EmailID) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(EmailID)) {
        return false;
    } else {
        return true;
    }
}
function UserInformationSelectForForgotPassword() {
    var MobileNumber = $("#inputMobileNumber").val();
    var EmailID = $("#inputEmail").val();

    if ((MobileNumber == "" && EmailID == "") || (MobileNumber == undefined && EmailID == undefined)) {
        $.alert.open({
            content: String.format("Please enter mobile number or password"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputMobileNumber").focus();
            }
        });
        return false;
    }

    if (EmailID != "") {
        if (!EmailValidation(EmailID)) {
            $.alert.open({
                content: "Please enter a valid email address", icon: 'warning', title: ClientName,
                callback: function () {
                    $("#inputEmail").focus();
                }
            });
            return false;
        }
    }

    var jsonObj = {
        MobileNumber: MobileNumber,
        EmailID: EmailID
    };
    var jData = JSON.stringify(jsonObj)

    $.alert.open({
        type: 'confirm',
        content: 'Are you want to submit',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'Yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Forgot.aspx/ForgotPassword",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowRespose(data);
                    Reset();
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing worng"), icon: 'error', title: ClientName,
                    });
                    Reset();
                }
            });
            return false;
        }
    });
  
}

function Reset() {
    $("#inputMobileNumber").val("");
    $("#inputMobileNumber").focus();
    $("#inputEmail").val("");
}

function ShowRespose(data) {
    Icon = data.d.Icon;
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function (data) {
            Reset();
        }
    });
}

function Redirect() {
    window.location.href = "Login.aspx";
}