﻿$("#SubmitButton").click(function (evt) {
    if (InsertNewBugValidation() == false) {
        return false;
    }
    var File = $("#inputAttachment").val();
    if (File == "") {
        var result = 0;
        BugMasterInsert(result);
    }
    else {
        var data = new FormData();
        var fileUpload = $("#inputAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForBug.ashx",
            type: "POST",
            data: data,
            async:false,
            contentType: false,
            processData: false,
            success: function (result) {

                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    $("#modal_ReportNewBug").modal('hide');
                    $("#OutputMsg").html(result);
                }
                else { BugMasterInsert(result); }


            },
            error: function (err) {
                alert('Error:' + err)
            }
        });
    }
    evt.preventDefault();
});

function InsertNewBugValidation() {
    var BugTitle = $("#inputBugTitle").val();
    var TestingIssueID = $("#inputIssueType").val();
    var Priority = $("#inputPriority").val();
    var Description = $("#inputDescription").val();
    var ExpectedResult = $("#inputExpectedResult").val();

    if (BugTitle == "" && ExpectedResult == "" && Priority == "0") {
        $("#lblinputBugTitle").show();
        $("#lblinputPriority").show();
        $("#lblinputExpectedResult").show();
        return false;
    }

    if (BugTitle == "") {
        $("#lblinputBugTitle").show();
        return false;
    }

    if (Priority == '0') {
        $("#lblinputPriority").show();
        return false;
    }

    if (ExpectedResult == "") {
        $("#lblinputExpectedResult").show();
        return false;
    }
}

function BugMasterInsert(result) {

    //var ProjectBugMasterID = $("#ProjectBugMasterID").text();

    var ProjectBugMasterID = 0;
    if (window.location.href.indexOf("ProjectBugMasterID") > -1) {
        var url = window.location.href;
        var ID = url.split('=')
        ProjectBugMasterID = ID[1];
        $("#ProjectBugMasterID").html(ID[1]);
        $("#ProjectBugMasterID").css('display', 'none');

    }

    var BugTitle = $("#inputBugTitle").val();
    var PageURL = $("#inputURL").val();
    var TestingIssueID = $("#inputIssueType").val();
    var Priority = $("#inputPriority").val();
    var FileIDs = result;
    var Description = $("#inputDescription").val();
    var ExpectedResult = $("#inputExpectedResult").val();

    var jsonObj = {
        ProjectBugMasterID: ProjectBugMasterID,
        TestingIssueID: TestingIssueID,
        BugTitle: BugTitle,
        FileIDs: FileIDs,
        PageURL: PageURL,
        Priority: Priority,
        Description: Description,
        ExpectedResult: ExpectedResult,
    };

    var jData = JSON.stringify(jsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "InsertNewBug.aspx/Insert",
        data: jData,
        async:false,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            $("#modal_ReportNewBug").modal('hide');
            $("#OutputMsg").html(data.d.Message);
            var updateBug = location.reload();
            ResetAll();
            BugMasterSelect();

        },
        error: function (result) {
            $.alert.open({
                content: String.format("Something worng"), icon: 'error', title: ClientName,
            });
        }
    });

    return false;
}

function ProjectBugMasterSelect() {
    var ProjectBugMasterID = 0;
    if (window.location.href.indexOf("ProjectBugMasterID") > -1) {
        var url = window.location.href;
        var ID = url.split('=')
        ProjectBugMasterID = ID[1];
        $("#ProjectBugMasterID").html(ID[1]);
        $("#ProjectBugMasterID").css('display', 'none');

    }

    var jsonObj = {
        ProjectBugMasterID: ProjectBugMasterID,
    };

    var jData = JSON.stringify(jsonObj)


    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "InsertNewBug.aspx/SelectProjectName",
        data: jData,
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            $("#ProjectName").html(data.d);
        },
        error: function (result) {

        }
    });

}

function BugMasterSelect(searchtype) {
    var ProjectBugMasterID = 0;
    if (window.location.href.indexOf("ProjectBugMasterID") > -1) {
        var url = window.location.href;
        var ID = url.split('=')
        ProjectBugMasterID = ID[1];
        $("#ProjectBugMasterID").html(ID[1]);
        $("#ProjectBugMasterID").css('display', 'none');

    }

    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    var Status = $("#txtStatus").val();


    if (FromDate == "") {
        $.alert.open({
            content: "Please select from date", icon: 'error', title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }

    if (ToDate == "") {
        $.alert.open({
            content: "Please select to date", icon: 'error', title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });
        return false;
    }

    if (Status == "") {
        $.alert.open({
            content: "Please select system", icon: 'error', title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });
        return false;
    }
    $("#BugDetails").html("");
    var jsonObj = {
        ProjectBugMasterID: ProjectBugMasterID,
        FromDate: FromDate,
        ToDate: ToDate,
        Status: Status,
        searchtype: searchtype
    };



    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "InsertNewBug.aspx/Select",
        data: jData,
        title: ClientName,
        dataType: "JSON",
        success: function (data) {

            BindDataBugMaster(data);
        },
        //error: function (result) {
        //   $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        // }
    });
}



function BindDataBugMaster(data) {
    $("#BugDetails").html("");
    if (data.d.length > 0) {
        $("#BugDetails").html("");
        $("#BugDetails").html("<thead><tr><th>#</th><th>ID</th><th>Bug Title</th><th>UpdatedDOC</th><th>Type</th><th>Priority</th><th>Developer</th><th>Edit</th><th>Delete</th></tr></thead><tbody></tbody>");

        var len = data.d.length;
        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';


            if (data.d[i].Status.toUpperCase() == "NEW".toUpperCase()) {
                //  color = '#f8f9fa';
                color = 'red';
                //  baclgroundColor = '#dc3545';
            }
            else if (data.d[i].Status.toUpperCase() == "CONFIRMED".toUpperCase())
                color = 'DARKBLUE';

            else if (data.d[i].Status.toUpperCase() == "RESOLVED".toUpperCase())
                color = 'GREEN';

            else if (data.d[i].Status.toUpperCase() == "INPROGRESS".toUpperCase())
                color = 'BLUE';

            else if (data.d[i].Status.toUpperCase() == "FEEDBACK".toUpperCase())
                color = 'GRAY';

            else if (data.d[i].Status.toUpperCase() == "CLOSED".toUpperCase())
                color = 'BLACK';

            else if (data.d[i].Status.toUpperCase() == "ACKNOWLEDGED".toUpperCase())
                color = '#630396';


            strRow.push(String.format('<tr  style="color:{0}; background-color:{1};"   onclick="return BugMasterSelectByBugID({2})"  ">', color, data.d[i].BugID));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));

            strRow.push(String.format('<td> {0} </td>', data.d[i].BugID));

            strRow.push(String.format('<td> {0} </td>', data.d[i].BugTitle));
            strRow.push(String.format('<td> {0} </td>', data.d[i].UpdateDOC));



            strRow.push(String.format('<td> {0} </td>', data.d[i].TestingIssueID));
            strRow.push(String.format('<td> {0} </td>', data.d[i].Priority));
            strRow.push(String.format('<td> {0} </td>', data.d[i].Developer));

            // strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='download' style='width:50%; background-color: white; border-color: #fff' onclick='return DownloadAttachment({0})' ><i class='fa fa-download' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].BugID));

            strRow.push(String.format("<td class='text-center'> <button type='button'class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return BugMasterSelectByBugID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].BugID));

            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return BugMasterDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].BugID));

            strRow.push('</tr>');
            $("#BugDetails").append(strRow.join(""));
        }
        addProperties(BugDetails);
    }

}

function BugMasterSelectByBugID(BugID) {
    var BugID = BugID;

    var ProjectBugMasterID = $("#ProjectBugMasterID").text();
    var jsonObj = {
        BugID: BugID,
    };

    var jData = JSON.stringify(jsonObj);

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "InsertNewBug.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {

            ResetAll();
            $("#SubmitButton").hide();
            $("#UpdateButton").show();

            $("#StatusDiv").show();
            $("#inputPriority").val(data.d.Priority.toUpperCase()).trigger('chosen:updated');
            $("#inputIssueType").val(data.d.TestingIssueID).trigger('chosen:updated');
            $("#inputStatus").val(data.d.Status.toUpperCase()).trigger('chosen:updated');

            if (data.d.Status.toUpperCase() === 'FEEDBACK') {
                $("#CommentSection").show();

                $("#inputComment").val(data.d.Comment);
            }



            $("#inputBugTitle").val(data.d.BugTitle);
            $("#inputExpectedResult").val(data.d.ExpectedResult);
            $("#inputDescription").val(data.d.Description);
            $("#BugID").html(BugID);
            $("#inputURL").val(data.d.PageURL);
            $("#ProjectBugMasterID").html(ProjectBugMasterID);
            DownloadAttachment(BugID);
            $("#modal_ReportNewBug").modal('show');
            $("#InsertHeader").hide();
            $("#UpdateHeader").show();

        },
        error: function (result) {

        }
    });
}

function ReportNewBug() {
    ResetAll();
    $("#UpdateHeader").hide();
    $("#InsertHeader").show();
};

function DownloadAttachment(BugID) {
    var jsonObj = {
        BugID: BugID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "InsertNewBug.aspx/SelectAttachment",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            if (data.d.length > 0) {
                BindDataAttachment(data);


            }

        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function BindDataAttachment(data) {
    $("#Attachments").html("");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';
        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td>{0} </th>', (i + 1)));
        strRow.push(String.format("<td class='text-left'> <a href='{0}' target='_blank'>{1}</a> </td>", data.d[i].Attachment, data.d[i].OriginalFileName));
        strRow.push('</tr>');
        $("#Attachments").append(strRow.join(""));
    }
}

$("#UpdateButton").click(function (evt) {
    if (InsertNewBugValidation() == false) {
        return false;
    }
   
    var File = $("#inputAttachment").val();
    if (File == "" || File == undefined) {
        var result = 0;
        BugMasterUpdate(result);
    }

    else {
        var data = new FormData();
        var fileUpload = $("#inputAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForBug.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {
                BugMasterUpdate(result);
            },
            error: function (err) {
                alert('Error:' + err)
            }
        });
    }
    evt.preventDefault();
});

function BugMasterUpdate(result) {
    var ProjectBugMasterID = $("#ProjectBugMasterID").text();
    var BugID = $("#BugID").text();

    var BugTitle = $("#inputBugTitle").val();
    var PageURL = $("#inputURL").val();
    var TestingIssueID = $("#inputIssueType").val();
    var Priority = $("#inputPriority").val();
    var FileIDs = result;
    var Description = $("#inputDescription").val();
    var ExpectedResult = $("#inputExpectedResult").val();
    var Comment = $("#inputComment").val();

    var Status = $("#inputStatus").val();


    var jsonObj = {
        BugTitle: BugTitle,
        PageURL: PageURL,
        TestingIssueID: TestingIssueID,
        Priority: Priority,
        FileIDs: FileIDs,
        Description: Description,
        Comment: Comment,
        Status: Status,
        ExpectedResult: ExpectedResult,
        BugID: BugID,

    };

    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "InsertNewBug.aspx/Update",
        data: jData,
        async:false,
        dataType: "json",
        success: function (data) {
            $("#modal_ReportNewBug").modal('hide');
            $("#BugID").html('');
            $("#OutputMsg").html(data.d.Message);
            var updateBug = location.reload();
            ResetAll();
            BugMasterSelect();
        }
    });

}

function ResetAll() {
    $("#SubmitButton").show();
    $("#UpdateButton").hide();

    $("#inputPriority").val(0).trigger('chosen:updated');
    $("#inputIssueType").val(0).trigger('chosen:updated');

    $("#inputBugTitle").val('');
    $("#inputExpectedResult").val('');
    $("#inputDescription").val('');
    $("#BugID").html('');
    $("#inputURL").val('');
    // $("#ProjectBugMasterID").html(ProjectBugMasterID);
    $("#inputAttachment").val('');
    $("#Attachments").html("");
    $("#inputComment").val('');
    $("#InsertHeader").show();
    $("#UpdateHeader").hide();
    $("#StatusDiv").hide();
}

function BugMasterDelete(BugID) {

    var jsonObj = {
        BugID: BugID,
    };

    var jData = JSON.stringify(jsonObj)



    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "InsertNewBug.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    $("#OutputMsg").html(data.d.Message);
                    ResetAll();
                    BugMasterSelect();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not deleted'), icon: 'error', title: ClientName, });
        }
    });







}

