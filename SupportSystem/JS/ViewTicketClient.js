﻿function TicketInformationSelectByClient() {
    $("#loader").show();

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewTicketClient.aspx/Select",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            $("#loader").hide();
            BindDataTicketInformation(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });

}

function BindDataTicketInformation(data) {
    $("#TicketDetails").html("");
    $("#TicketDetails").html("<thead><tr><th>#</th><th>Ticket No</th><th>Date</th><th>Title</th><th>Service</th><th>Status</th><th>Edit</th><th>Delete</th></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format('<td>{0}</td>', data.d[i].TicketNo));

        strRow.push(String.format('<td>{0}</td>', data.d[i].DOCInString));
        strRow.push(String.format('<td>{0}</td>', data.d[i].Title));
        strRow.push(String.format('<td>{0}</td>', data.d[i].ServiceName));
        strRow.push(String.format('<td><select class="form-control" id="inputStatus{1}" onchange="UpdateTicketStatus(\'{0}\',this.value)"><option value="NEW">NEW</option><option value="INCOMPLETE">IN COMPLETE</option> <option value="RESOLVED">RESOLVED</option></select></td>', data.d[i].TicketNo, i));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return TicketInformationSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TicketID));

        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return TicketInformationDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].TicketID));
        strRow.push('</tr>');
        $("#TicketDetails").append(strRow.join(""));

        $('#inputStatus' + i).val(data.d[i].TicketStatus);
        $("#inputStatus" + i).trigger("chosen:updated");
    }
    addProperties(TicketDetails);
}

function UpdateTicketStatus(TicketNo, Status) {
    $("#loader").show();


    var jsonObj = {
        Status: Status,
        TicketNo: TicketNo,
    };

    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTicketClient.aspx/UpdateStatusByClient",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            $("#loader").hide();
            $.alert.open({
                content: data.d, icon: 'success', title: ClientName,
            });

        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function DateValidation() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    var TodaysDate = $("#TodaysDate").val();

    if (FromDate == "") {
        $.alert.open({
            content: String.format("Please select fromdate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtFromDate").focus();
            }
        });

        return false;
    }
    if (FromDate > TodaysDate) {
        $.alert.open({
            content: String.format("Please select valid fromdate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtFromDate").focus();
            }
        });
    }

    if (ToDate == "") {
        $.alert.open({
            content: String.format("Please select todate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtToDate").focus();
            }
        });

        return false;
    }
    if (ToDate > TodaysDate) {
        $.alert.open({
            content: String.format("Please select valid todate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtToDate").focus();
            }
        });
    }
    if (ToDate > TodaysDate && FromDate > TodaysDate) {
        $.alert.open({
            content: String.format("Please select valid fromdate and todate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtFromDate").focus();
            }
        });
    }
}

function TicketInformationSelectByDateForClient() {


    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();

    if (DateValidation() == false) {
        return false;
    }
    var jsonObj = {
        FromDate: FromDate,
        ToDate: ToDate,
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewTicketClient.aspx/SelectByDate",
        data: jData,
        dataType: "JSON",
        success: function (data) {

            BindDataTicketInformation(data);
            if (data.d.length <= 0) {
                $.alert.open({
                    content: String.format("There is not any ticket raised in given dates."), icon: 'warning', title: ClientName,
                    callback: function () {
                        $("#txtFromDate").focus();
                    }
                });

                return false;

            }
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}


function ViewDescription(Description) {
    $("#modal_Sender").modal('show');
    $("#txtDescription").html(Description);
}

function TicketInformationDelete(TicketID) {

    var jsonObj = {
        TicketID: TicketID,
    };

    var jData = JSON.stringify(jsonObj)

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }
            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ViewTicketClient.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    $.alert.open({ content: data.d, icon: 'error', title: ClientName, });
                    TicketInformationSelectByClient();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not deleted'), icon: 'error', title: ClientName, });
        }
    });
}

function TicketInformationSelectByID(TicketID) {
    location.href = "AddTicketClient.aspx?TicketID=" + TicketID;
}