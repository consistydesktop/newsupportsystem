﻿function HolidayMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/AddHoliday.aspx/Select",
        data: {},
        dataType: "JSON",
        success: function (data) {
            BindDataHolidayMaster(data);
        },
        error: function (result) {
            alert("Not Found");
        }
    });
}

function BindDataHolidayMaster(data) {
    $("#HolidayDetails").html("");
    $("#HolidayDetails").html("<thead><tr><th>#</th><th>Date</th><th>Description</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td class="Grcenter"> {0} </td>', (i + 1)));
        strRow.push(String.format('<td> {0} </td>', data.d[i].DOCInString));

        strRow.push(String.format('<td> {0} </td>', data.d[i].Description));

        strRow.push('</tr>');
        $("#HolidayDetails").append(strRow.join(""));
    }

    $('#HolidayDetails').dataTable({
        destroy: true,
        dom: 'Blfrtip',
        buttons: ['excelHtml5', 'pdfHtml5']
    });
}