﻿function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "TimeSheet.aspx/SelectSystemName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataSystemMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSystemMaster(data) {
    $("#ddlSystemName").html("");
    $("#ddlSystemName").append("<option value='0'>Select System </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#ddlSystemName").append(strRowDropdown);

        var SystemObject = {
            SystemID: data.d[i].SystemID,
            SystemName: data.d[i].SystemName
        }

        SystemObjects.push(SystemObject);
    }
    $("#ddlSystemName").chosen();
}

function InsertRecord() {
    var table = document.getElementById("EditTimeSheet");

    if (true) {
        if (table.rows.length > 1) {
            var e = "";
            var SystemID = "";

            var SystemName = "";

            var Task = "";
            var Time = "";

            var Date = $('#txtDate').val();
            var Reason = $('#txtReason').val();

            for (var i = 1; i < table.rows.length; i++) {
                var objCells = table.rows.item(i).cells;

                SystemID = $("#ddlSystemName_" + i + "").val();

                Task = $("#editTask_" + i + "").val();
                Time = $("#editTime_" + i + "").val();

                if (SystemID == 0) {
                    $.alert.open({
                        content: String.format("Please Select System"), icon: 'warning', title: ClientName,
                        callback: function () {
                            $(String.format("#ddlSystemName_{0}", i)).focus();
                        }
                    });
                    return false;
                }

                if (Task == " ") {
                    $.alert.open({
                        content: String.format("Please enter task"), icon: 'warning', title: ClientName,
                        callback: function () {
                            $("#editTask_" + i + "").focus();
                        }
                    });
                    return false;
                }

                if (Time == " ") {
                    $.alert.open({
                        content: String.format("Please enter time"), icon: 'warning', title: ClientName,
                        callback: function () {
                            $("#editTime_" + i + "").focus();
                        }
                    });
                    return false;
                }
            }
        }

        var table = document.getElementById("EditTimeSheet");

        var row = table.insertRow(-1);
        var cell1 = row.insertCell(0);

        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);

        ++count;
        List.push(count);
        cell1.innerHTML = (String.format('<select id="ddlSystemName_{0}"  title="Select System" ><option></option></select>', count, count));
        cell2.innerHTML = (String.format('<textarea id="editTask_{0}" title="Enter Task" class="form-control loginput"> </textarea>', count));
        cell3.innerHTML = (String.format('<textarea id="editTime_{0}" title="Enter Time" class="form-control loginput" onkeypress="return isNumberKey(event)" maxlength="4"> </textarea>', count));
        cell4.innerHTML = (String.format("<button type='button' class='btn btn-info buttons' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='deleteRow(this,{0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button>", count));

        $(String.format("#ddlSystemName_{0}", count)).html("");
        $(String.format("#ddlSystemName_{0}", count)).append("<option value='0'>Select System </option>");
        for (var i = 0; i < SystemObjects.length; i++) {
            var strRowDropdown = [];
            strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", SystemObjects[i].SystemID, SystemObjects[i].SystemName));
            $(String.format("#ddlSystemName_{0}", count)).append(strRowDropdown);
        }

        $(String.format("#ddlSystemName_{0}", count)).chosen();

        $('#txtTask').val("");
        $('#txtTime').val("");

        $('#txtTask').val("");
        $('#txtTime').val("");
    }
}

function deleteRow(r, c) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("EditTimeSheet").deleteRow(i);
    List.splice(List.indexOf(c), 1);
    //count = count - 1;
}

function ShowResponse(data) {
    Icon = data.d.Icon;
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}

function Reset() {
    location.reload();
}

function TimeSheetMasterInsert() {
    var e = "";
    var SystemID = "";

    var SystemName = "";

    var Task = "";
    var Time = "";

    var Date = $('#inputDate').val();
    var Reason = $('#inputRemark').val();

    List;
    var table = document.getElementById("EditTimeSheet");

    for (j = 0, i = 1; i < table.rows.length; i++, j++) {
        var objCells = table.rows.item(i).cells;

        SystemID = $("#ddlSystemName_" + List[j] + "").val();

        Task = $("#editTask_" + List[j] + "").val();
        Time = $("#editTime_" + List[j] + "").val();

        if (SystemID == 0) {
            $.alert.open({
                content: String.format("Please Select System"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#ddlSystemName_" + i + "").focus();
                }
            });
            return false;
        }

        if (Task == " ") {
            $.alert.open({
                content: String.format("Please enter task"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#editTask_" + i + "").focus();
                }
            });
            return false;
        }

        if (Time == " ") {
            $.alert.open({
                content: String.format("Please enter time"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#editTime_" + i + "").focus();
                }
            });
            return false;
        }

        if ((typeof SystemID != 'undefined') && (typeof Task != 'undefined') && (typeof Time != 'undefined')) {
            var jSonObj = {
                Date: Date,
                SystemID: SystemID,
                Task: Task,
                Time: Time,
                Reason: Reason,
            }

            JSonObjects.push(jSonObj);
        }
    }

    if (JSonObjects.length == 0) {
        $.alert.open({
            content: String.format("Please Fill TimeSheet"), icon: 'warning', title: ClientName,
            callback: function () {
              
                InsertRecord();
            }
        });
        return false;
    }

    var JData = JSON.stringify({ ObjModel: JSonObjects });

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "TimeSheet.aspx/Insert",
        data: JData,
        dataType: "json",
        success: function (data) {
            ShowResponse(data);
            location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function TimeSheetMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "TimeSheet.aspx/Select",
        data: {},
        dataType: "json",

        success: function (data) {
            BindDataTimeSheetMaster(data);
        }
    });
}

function BindDataTimeSheetMaster(data) {
    $("#TimeSheetDetails").html("<thead><tr><th>Sr.No</th><th>Date</th><th>Total Time(In minutes)</th><th>Edit</th><th>Delete</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td class="Grcenter"> {0} </td>', (i + 1)));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Date));
        strRow.push(String.format('<td> {0} minutes</td>', data.d[i].TotalTime));

        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return TimeSheetMasterSelectByID(\"{0}\")' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", (data.d[i].Date)));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return TimeSheetMasterDelete(\"{0}\")'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", (data.d[i].Date)));

        strRow.push('</tr>');
        $("#TimeSheetDetails").append(strRow.join(""))
    }
}

function TimeSheetMasterSelectByID(Date) {
    var jsonObj =
        {
            Date: Date,
        };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "TimeSheet.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            BindTimeSheetData(data);
        },
        error: function (result) {
            $.alert.open({ content: "not Updated", icon: 'error', title: ClientName, });
        }
    });
}

function BindTimeSheetData(data) {
    $("#EditTimeSheet").html("<thead><tr><th>System Name</th><th>Task</th><th>Time</th><th>Update</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format("<tr style='color:{0};'>", color));

        strRow.push(String.format('<td><select id="ddlSystemName_{1}"  title="Select"><option value={1}></option></select> </td>', data.d[i].SystemID, data.d[i].TimeSheetDetailID));
        strRow.push(String.format('<td> <textarea id="editTask_{1}" class="form-control loginput">{0}</textarea> </td>', data.d[i].Task, data.d[i].TimeSheetDetailID));
        strRow.push(String.format('<td> <input id="editTime_{1}" type="text" value="{0}" class="form-control loginput"/> </td>', data.d[i].Time, data.d[i].TimeSheetDetailID));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Update' style='width:50%; background-color: white; border-color: #fff' onclick='return TimeSheetDetailUpdate({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TimeSheetDetailID));

        strRow.push('</tr>');
        $("#EditTimeSheet").append(strRow.join(""));
        $('#inputDate').val(data.d[i].Date);

        $(String.format("#ddlSystemName_{0}", data.d[i].TimeSheetDetailID)).html("");
        $(String.format("#ddlSystemName_{0}", data.d[i].TimeSheetDetailID)).append("<option value='0'>Select System </option>");
        for (var j = 0; j < SystemObjects.length; j++) {
            var strRowDropdown = [];
            strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", SystemObjects[j].SystemID, SystemObjects[j].SystemName));
            $(String.format("#ddlSystemName_{0}", data.d[i].TimeSheetDetailID)).append(strRowDropdown);
        }
        $(String.format("#ddlSystemName_{0}", data.d[i].TimeSheetDetailID)).chosen();

        $(String.format("#ddlSystemName_{0}", data.d[i].TimeSheetDetailID)).val(data.d[i].SystemID).trigger('chosen:updated');

        if (data.d[i].Reason != "") {
            $('#inputRemark').val(data.d[i].Reason);
        }
        $("#TimeSheetMasterID").text(data.d[i].TimeSheetMasterID);
        $("#TimeSheetDetailID").text(data.d[i].TimeSheetDetailID);
        $("#SubmitButton").hide();
        $("#UpdateButton").show();
    }
}

function TimeSheetMasterDelete(Date) {
    var jsonObj =
        {
            Date: Date,
        };
    var jData = JSON.stringify(jsonObj);

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "Application/json;charset:UTF-8",
                url: "TimeSheet.aspx/Delete",
                data: jData,
                dataType: "JSON",
                title: ClientName,
                success: function (data) {
                    ShowResponse(data);
                    location.reload();
                },
                error: function (result) {
                    $.alert.open({ content: "Not Deleted", icon: 'error', title: ClientName, });
                }
            });
        }
    });
}

function TimeSheetDetailUpdate(TimeSheetDetailID) {
    var SystemID = $("#ddlSystemName_" + TimeSheetDetailID + "").val();
    var Task = $("#editTask_" + TimeSheetDetailID + "").val();
    var Time = $("#editTime_" + TimeSheetDetailID + "").val();

    var jSonObj = {
        TimeSheetDetailID: TimeSheetDetailID,
        SystemID: SystemID,

        Task: Task,
        Time: Time,
    }

    var JData = JSON.stringify(jSonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "TimeSheet.aspx/UpdateByID",
        data: JData,
        dataType: "json",
        success: function (data) {
            ShowResponse(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function TimeSheetMasterUpdate() {
    var TimeSheetMasterID = $("#TimeSheetMasterID").text();
    TimeSheetMasterID = parseInt(TimeSheetMasterID, 10);

    var Date = $('#txtDate').val();
    var Reason = $('#txtReason').val();

    var jSonObj = {
        TimeSheetMasterID: TimeSheetMasterID,
        Date: Date,
        Reason: Reason,
    }

    var JData = JSON.stringify(jSonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "TimeSheet.aspx/Update",
        data: JData,
        dataType: "json",
        success: function (data) {
            ShowResponse(data);
            location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function isNumberKey(e) {
    $("#lblUsername").css('display', 'none');
    $("#lblPassword").css('display', 'none');
    $("#lblCaptcha").css('display', 'none');
    var r = e.which ? e.which : event.keyCode;
    return r > 31 && (48 > r || r > 57) && (e.which != 46 || $(this).val().indexOf('.') != -1) ? !1 : void 0
}