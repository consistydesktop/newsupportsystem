﻿function UserInformationSelectByEmployeeID() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewTicketsAdmin.aspx/SelectEmployeeName",
        data: "{}",
        async:false,
        dataType: "JSON",
        success: function (data) {
            BindDataSelectEmployeeName(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSelectEmployeeName(data) {
    $("#inputEmployeeName").html("");
    $("#inputEmployeeName").append("<option value=0> Select Developer </option> ");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
        $("#inputEmployeeName").append(strRowDropdown);
    }
    $("#inputEmployeeName").chosen();
}

function ServiceMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewTicketsAdmin.aspx/SelectServiceName",
        data: "{}",
        async: false,
        dataType: "JSON",
        success: function (data) {
            BindDataServiceMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataServiceMaster(data) {
    $("#inputService").html("");
    $("#inputService").append("<option value=0>Select Service </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].ServiceID, data.d[i].ServiceName));
        $("#inputService").append(strRowDropdown);
    }
    $("#inputService").chosen();
}

function TicketInformationSelectByTicketID(TicketID) {
    var TicketID = TicketID;
    var jsonObj = {
        TicketID: TicketID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewTicketsAdmin.aspx/SelectByTicketID",
        data: jData,
        dataType: "JSON",
        success: function (data) {
            BinddataTicketInformation(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function TicketsInformationSelect() {
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewTicketsAdmin.aspx/Select",
        data: "{}",
        dataType: "JSON",
        title: ClientName,
        success: function (data) {

            BinddataTicketInformation(data);
            $("#loader").hide();
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BinddataTicketInformation(data) {
    $("#ViewTicketsDetailsForAdmin").html("");
    $("#ViewTicketsDetailsForAdmin").html("<thead><tr><th>#</th><th>Ticket No</th><th>System Name </th><th>Date & Time</th><th>Service</th><th>Description</th><th>Ticket Status</th><th>Raised By</th><th>Assigned To</th><th>Assign</th></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td>{0}</td>', data.d[i].TicketNo));
        strRow.push(String.format('<td>{0}</td>', data.d[i].SystemName));
        strRow.push(String.format('<td>{0}</td>', data.d[i].DOCInString));
        strRow.push(String.format('<td>{0}</td>', data.d[i].ServiceName));
        if (data.d[i].Description != "") {
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn'  title='Description' style='width:50%; background-color: white; border-color: #fff' onclick=\"ViewDescription('{0}')\" ><i class='fa fa-file-text' style='font-size:20px;color:blue'></i></button> </td>", (data.d[i].Description).replace(/\n/ig, '</br>')));
        }
        else {
            strRow.push(String.format('<td>{0}</td>', ""));
        }
        strRow.push(String.format('<td>{0}</td>', data.d[i].TicketStatus));
        strRow.push(String.format('<td>{0}</td>', data.d[i].RaisedBy));
        strRow.push(String.format('<td>{0}</td>', data.d[i].AssignedTo));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Assign To Developer' style='width:50%; background-color: white; border-color: #fff'' onclick=\"AssignToDeveloper({0},'{1}')\" ><i class='fa fa-edit' style='font-size:20px;color:green'></i></button> </td>", data.d[i].TicketID, data.d[i].SystemName));

        strRow.push('</tr>');
        $("#ViewTicketsDetailsForAdmin").append(strRow.join(""));
    }

    addProperties(ViewTicketsDetailsForAdmin);
}

function ViewDescription(Description) {
    $("#modal_Sender").modal('show');
    $("#txtDescription").html(Description);
}

function AssignToDeveloper(TicketID) {
    location.href = "AssignToDeveloper.aspx?TicketID=" + TicketID;
}

function DateValidation() {
    var FromDate = $("#txtFromDate").val();
    var ToDate = $("#txtToDate").val();
    var TodaysDate = $("#TodaysDate").val();

    if (FromDate == "") {
        $.alert.open({
            content: String.format("Please select fromdate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtFromDate").focus();
            }
        });

        return false;
    }
    if (FromDate > TodaysDate) {
        $.alert.open({
            content: String.format("Please select valid fromdate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtFromDate").focus();
            }
        });
    }

    if (ToDate == "") {
        $.alert.open({
            content: String.format("Please select todate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtToDate").focus();
            }
        });

        return false;
    }
    if (ToDate > TodaysDate) {
        $.alert.open({
            content: String.format("Please select valid todate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtToDate").focus();
            }
        });
    }
    if (ToDate > TodaysDate && FromDate > TodaysDate) {
        $.alert.open({
            content: String.format("Please select valid fromdate and todate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtFromDate").focus();
            }
        });
    }
}

function TicketInformationSelectByDate() {
    var FromDate = $("#txtFromDate").val();
    var ToDate = $("#txtToDate").val();

    if (DateValidation() == false) {
        return false;
    }
    var jsonObj = {
        FromDate: FromDate,
        ToDate: ToDate,
    };
    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTicketsAdmin.aspx/Search",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BinddataTicketInformation(data);
        },
        error: function (result) {
            $.alert.open({
                content: String.format("No data available between given date"), icon: 'error', title: ClientName,
            });
        }
    });
    return false;
}

function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewTicketsAdmin.aspx/SelectSystemName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataSystemMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data Not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSystemMaster(data) {
    $("#inputSystemName").html("");
    $("#inputSystemName").append("<option value=0>Select System </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#inputSystemName").append(strRowDropdown);
    }
    $("#ddlSystemName").chosen();
}

function TicketInformationSelectByFilter() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    var Name = $('#inputEmployeeName option:selected').html();
    var ServiceName = $('#inputService option:selected').html();
    var SystemName = $('#inputSystemName option:selected').html();

    var DeveloperID = $('#inputEmployeeName').val();
    var ServiceID = $('#inputService').val();
    var SystemID = $('#inputSystemName').val();

    var TicketNo = $("#inputTicketNo").val();

    var jsonObj = {
        FromDate: FromDate,
        ToDate: ToDate,
        DeveloperID: DeveloperID,
        ServiceID: ServiceID,
        SystemID: SystemID,
        ServiceName: ServiceName,
        TicketNo: TicketNo,
    };
    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTicketsAdmin.aspx/SearchByFilter",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BinddataTicketInformation(data);
        },
        error: function (result) {
            $.alert.open({
                content: String.format("No data available between given date"), icon: 'error', title: ClientName,
            });
        }
    });
    return false;
}