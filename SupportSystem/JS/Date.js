function SetDate() {
    var lastDate = new Date();
    lastDate.setDate(lastDate.getDate() - 2);//any date you want
    $("#txtFromDate").val($.datepicker.formatDate("dd/mm/yy", lastDate));

    $("#txtToDate").val($.datepicker.formatDate("dd/mm/yy", new Date()));
}

function SetTodaysDate() {
    $("#txtFromDate").val($.datepicker.formatDate("dd/mm/yy", new Date()));

    $("#txtToDate").val($.datepicker.formatDate("dd/mm/yy", new Date()));
}