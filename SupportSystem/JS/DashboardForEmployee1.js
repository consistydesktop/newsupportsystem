﻿function GetPendingProjects() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectPendingProjects",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            document.getElementById('CountOfPendingProjetcs').innerHTML = " " + data.d;
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function GetPendingCalls() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectPendingCalls",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            document.getElementById('CountOfPendingCalls').innerHTML = " " + data.d;
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function GetPendingTickets() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectPendingTickets",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            document.getElementById('CountOfPendingTickets').innerHTML = " " + data.d;
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function GetPendingBugs() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectPendingBugs",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            document.getElementById('CountOfPendingBugs').innerHTML = " " + data.d;
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function GetPendingTasks() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectPendingTasks",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            document.getElementById('CountOfPendingTasks').innerHTML = " " + data.d;
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function TicketInformationSelectForEmployee() {
    $("#cards").hide();

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectTicketDetails",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            $("#tasks").css('display', 'none');
            $("#calls").css('display', 'none');
            $("#tickets").css('display', 'block');
            $("#projects").css('display', 'none');
            BindDataTicketInformation(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataTicketInformation(data) {
    $("#TicketDetails").html("");
    $("#TicketDetails").html("<thead><tr><th>#</th><th>System</th><th>Pending Issues</th><th>Resolved Issues</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
        strRow.push(String.format('<td> <a href="#" onclick="DashPending({0})" >{1}</a> </td>', data.d[i].SystemID, data.d[i].PendingTickets));
        strRow.push(String.format('<td> <a href="#" onclick="DashResolved({0})" >{1}</a> </td>', data.d[i].SystemID, data.d[i].ResolvedTickets));

        strRow.push('</tr>');
        $("#TicketDetails").append(strRow.join(""));
    }
}

function DashPending(ID) {
    location.href = "DashboardPendingStatus.aspx?ID=" + ID;
}

function DashResolved(ID) {
    location.href = "DashboardResolvedStatus.aspx?ID=" + ID;
}

function ProjectDetailMasterSelectForEmployee() {
    $("#cards").hide();
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectProjectDetails",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            $("#projects").css('display', 'block');
            $("#calls").css('display', 'none');
            $("#tickets").css('display', 'none');
            $("#tasks").css('display', 'none');
            BindDataProjectDetailMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataProjectDetailMaster(data) {
    $("#ProjectDetails").html("");
    $("#ProjectDetails").html("<thead><tr><th>#</th><th>Project Name</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td> <a href="#" onclick="ViewProjectDetails({0})" >{1}</a> </td>', data.d[i].ProjectDetailMasterID, data.d[i].ProjectName));

        strRow.push('</tr>');
        $("#ProjectDetails").append(strRow.join(""));
    }
    addProperties(ProjectDetails);
}

function ViewProjectDetails(ProjectDetailMasterID) {
    location.href = "ViewProjectDetailsForEmployee.aspx?ProjectDetailMasterID=" + ProjectDetailMasterID;
}

function CallMasterSelectForEmployee() {
    $("#cards").hide();
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectCallDetails",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            $("#tasks").css('display', 'none');
            $("#calls").css('display', 'block');
            $("#tickets").css('display', 'none');
            $("#projects").css('display', 'none');
            BindDataCallMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataCallMaster(data) {
    $("#TicketDetails").html("");
    $("#TicketDetails").html("<thead><tr><th>#</th><th>Date</th><th>System</th><th>Description</th><th>Issue Type</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td> {0} </td>', data.d[i].DOC));
        strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
        strRow.push(String.format('<td> {0} </td>', data.d[i].MobileNumber));
        if (data.d[i].Description == "") {
            strRow.push(String.format('<td> {0} </td>', " "));
        }
        else {
            strRow.push(String.format('<td> {0} </td>', data.d[i].Description));
        }

        if (data.d[i].IssueName == "") {
            strRow.push(String.format('<td> {0} </td>', " "));
        } else {
            strRow.push(String.format('<td> {0} </td>', data.d[i].IssueName));
        }
        if (data.d[i].MobileNumber == "") {
            strRow.push(String.format('<td> {0} </td>', " "));
        }
        else {
            strRow.push(String.format('<td> {0} </td>', data.d[i].MobileNumber));
        }
        strRow.push('</tr>');
        $("#TicketDetails").append(strRow.join(""));
    }

    $('#TicketDetails').dataTable({
        "scrollX": true,
        "bDestroy": true,
        dom: 'Blfrtip',
        buttons: ['excelHtml5', 'pdfHtml5'
        ]
    });
}

function BugMasterSelectForEmployee() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectBugDetails",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            $("#Bug").css('display', 'block');
            $("#Call").css('display', 'none');
            $("#Ticket").css('display', 'none');
            $("#Project").css('display', 'none');
            $("#Task").css('display', 'none');
            BindDataBugMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataBugMaster(data) {
    $("#BugDetails").html("");
    $("#BugDetails").html("<thead><tr><th>#</th><th>Date</th><th>BugTitle</th><th>System</th><th>Priority</th><th>Description</th><th>IssueName</th><th>ExpectedResult</th><th>PageURL</th><th>Attachment</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td> {0} </td>', data.d[i].DOC));
        strRow.push(String.format('<td> {0} </td>', data.d[i].BugTitle));
        strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Priority));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Description));
        if (data.d[i].IssueName == "") {
            strRow.push(String.format('<td> {0} </td>', " "));
        }
        else {
            strRow.push(String.format('<td> {0} </td>', data.d[i].IssueName));
        }
        if (data.d[i].ExpectedResult == "") {
            strRow.push(String.format('<td> {0} </td>', " "));
        }
        else {
            strRow.push(String.format('<td> {0} </td>', data.d[i].ExpectedResult));
        }
        if (data.d[i].PageURL == "") {
            strRow.push(String.format('<td> {0} </td>', " "));
        } else {
            strRow.push(String.format('<td> {0} </td>', data.d[i].PageURL));
        }
        if (data.d[i].Attachment == "") {
            strRow.push(String.format('<td> {0} </td>', " "));
        } else {
            strRow.push(String.format('<td> {0} </td>', data.d[i].Attachment));
        }

        strRow.push('</tr>');
        $("#BugDetails").append(strRow.join(""));
    }
}

function TaskMasterSelectForEmployee() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectTaskDetails",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            $("#tasks").css('display', 'block');
            $("#calls").css('display', 'none');
            $("#tickets").css('display', 'none');
            $("#projects").css('display', 'none');

            BindDataTaskMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataTaskMaster(data) {
    $("#cards").hide();

    if (data.d.length < 1) {
        $.alert.open({
            content: 'There is no task', icon: 'error', title: ClientName,
            callback: function () {
            }
        });
        return false;
    }
    $("#TaskDetails").html("");
    $("#TaskDetails").html("<thead><tr><th>#</th><th>Task</th><th>Due date</th><th>Description</th><th>Status</th><th>Attachment</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        if (data.d[i].Status == "Completed")
            color = 'green';

        if (data.d[i].Status == 'incomplete') {
            color = 'red';
        }

        if (data.d[i].Status == 'required') {
            color = 'orange';
        }

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Task));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Deadline));
        if (data.d[i].Description != "") {
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn'  title='Description' style='width:50%; background-color: white; border-color: #fff' onclick=\"ViewDescription('{0}')\" ><i class='fa fa-file-text' style='font-size:20px;color:blue'></i></button> </td>", ((data.d[i].Description).replace(/\n/ig, '</br>')).replace("'", "")));
        }
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Status' style='width:50%; background-color: white; border-color: #fff' onclick=' TaskMasterUpdate({0})' ><i class='fa fa-pencil' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TaskID));

        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='download' style='width:50%; background-color: white; border-color: #fff' onclick='return DownloadAttachment({0})' ><i class='fa fa-download' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TaskID));

        strRow.push('</tr>');
        $("#TaskDetails").append(strRow.join(""));
    }
    var id = document.getElementById("TaskDetails");
    addProperties(id);
}

function ViewDescription(Description) {
    $("#modal_Sender").modal('show');
    $("#txtDescription").html(Description);
}

function TaskMasterUpdate(TaskID) {
    $("#TaskID").html(TaskID);
    $("#modal_status").modal('show');
}

function TaskMasterMasterUpdateStatus() {
    var Status = $("#ddlStatus").val();
    var File = $("#inputTaskAttachment").val();
    if (Status != "0") {
        Status = Status;
    }
    else {
        if (Status == "0") {
            $.alert.open({
                content: String.format("Please select status"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#modal_status").modal('show');
                    $("#ddlStatus").focus();
                }
            });

            return false;
        }
    }

    if (File == 0) {
        TaskMasterUpdateForStatus(0);
    }
    else {
        var data = new FormData();
        var fileUpload = $("#inputTaskAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForTask.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {

                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else {


                    TaskMasterUpdateForStatus(result);
                }




            },
            error: function (err) {
                alert('Error:' + err)
            }
        });
    }
    Reset();
}
function ShowResponseFile(result) {

    $.alert.open({
        content: result, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}
function TaskMasterUpdateForStatus(result) {
    var Status = $("#ddlStatus").val();
    var TaskID = $("#TaskID").text();
    var FileIDs = result;
    var jsonObj = {
        Status: Status,
        TaskID: TaskID,
        FileIDs: FileIDs,
    };
    var jData = JSON.stringify(jsonObj)

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to update status ',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "DashboardForEmployee.aspx/UpdateStatus",
                data: jData,
                dataType: "json",
                success: function (data) {
                    location.reload();
                    TaskMasterSelectForEmployee();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not inserted'), icon: 'error', title: ClientName, });
        }
    });
}

function DownloadAttachment(TaskID) {
    var jsonObj = {
        TaskID: TaskID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "DashboardForEmployee.aspx/SelectByTaskID",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BindDataAttachment(data);
            $("#modal_attachment").modal('show');
        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function BindDataAttachment(data) {
    if (data.d.length < 0) {
    }
    else {
        $("#Attachments").html("");
        $("#Attachments").html("<thead><tr><th>#</th><th>Attachment</th></tr></thead><tbody></tbody>");

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));
            strRow.push(String.format("<td class='text-center'> <a href='{0}'target='_blank'>{1}</a> </td>", data.d[i].Attachment, data.d[i].OriginalFileName));
            strRow.push('</tr>');
            $("#Attachments").append(strRow.join(""));
        }
    }
}