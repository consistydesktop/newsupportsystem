﻿
function createDataTable(divName, mainData, footerSumIndex) {
    var mainDivname = divName;

    if (divName.startsWith('#') === false) {
        mainDivname = '#' + divName;
    }


    $(mainDivname).dataTable({
        data: mainData,
        "bDeferRender": true,
        "bDestroy": true,
        dom: 'Blfrtip',
        buttons: [
           'excelHtml5',
           'pdfHtml5'
        ],

        "footerCallback": function (row, data, start, end, display) {
            var api = this.api();

            var arrayLength = footerSumIndex.length;
            for (var i = 0; i < arrayLength; i++) {
                displaySum(api, data, footerSumIndex[i]);
            }
        }
    });
}

function displaySum(api, data, index) {
    // Remove the formatting to get integer data for summation
    var intval = function (i) {
        if (typeof i === 'string') {
            i = i.replace(/<\/?[^>]+(>|$)/g, "");
            i = i.replace(/[\$,]/g, '') * 1;
        }

        if (Number.isNaN(i)) {
            return 0;
        }

        return i;
    };

    // Total over all pages
    total = api
        .column(index)
        .data()
        .reduce(function (a, b) {
            return intval(a) + intval(b);
        }, 0).toFixed(2);

    // Total over this page
    pageTotal = api
        .column(index, { page: 'current' })
        .data()
        .reduce(function (a, b) {
            return intval(a) + intval(b);
        }, 0).toFixed(2);

    var searchTotalData = api.column(index, { 'filter': 'applied' }).data();
    var searchTotal = searchTotalData.reduce(function (a, b) { return intval(a) + intval(b); }, 0).toFixed(2);

    $(api.column(index).footer()).html(
      pageTotal + ' total (' + searchTotal + ')'
    );

    // $(api.column(index).footer()).html(
    //  pageTotal + ', search total [' + searchTotal + '], total (' + total + ')'
    //);
}




//function displaySum(api, data, index) {

//    // Remove the formatting to get integer data for summation
//    var intVal = function (i) {
//        return typeof i === 'string' ?
//            i.replace(/[\$,]/g, '') * 1 :
//            typeof i === 'number' ?
//            i : 0;
//    };

//    // Total over all pages
//    total = api
//        .column(index)
//        .data()
//        .reduce(function (a, b) {
//            return intVal(a) + intVal(b);
//        }, 0);

//    // Total over this page
//    pageTotal = api
//        .column(index, { page: 'current' })
//        .data()
//        .reduce(function (a, b) {
//            return intVal(a) + intVal(b);
//        }, 0);

//    // Update footer
//    $(api.column(index).footer()).html(
//         pageTotal.toFixed(2) + '(' + total.toFixed(2) + ')'
//    );
//}


function createDataTablesum(divName, mainData, footerSumIndex) {
    var mainDivname = divName;

    if (divName.startsWith('#') === false) {
        mainDivname = '#' + divName;
    }


    $(mainDivname).dataTable({
        data: mainData,
        "bDeferRender": true,
        "bDestroy": true,
        dom: 'Blfrtip',
        buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],

        "footerCallback": function (row, data, start, end, display) {
            var api = this.api();

            var arrayLength = footerSumIndex
                .length;
            for (var i = 0; i < arrayLength; i++) {
                displaySum1(api, data, footerSumIndex[i]);
            }
        }
    });
}
function displaySum1(api, data, index) {
    // Remove the formatting to get integer data for summation
    var intval = function (i) {
        if (typeof i === 'string') {
            i = i.replace(/<\/?[^>]+(>|$)/g, "");
            i = i.replace(/[\$,]/g, '') * 1;
        }

        if (Number.isNaN(i)) {
            return 0;
        }

        return i;
    };

    // Total over all pages
    total = api
        .column(index)
        .data()
        .reduce(function (a, b) {
            return intval(a) + intval(b);
        }, 0).toFixed(2);

    // Total over this page
    pageTotal = api
        .column(index, { page: 'current' })
        .data()
        .reduce(function (a, b) {
            return intval(a) + intval(b);
        }, 0).toFixed(2);

    var searchTotalData = api.column(index, { 'filter': 'applied' }).data();
    var searchTotal = searchTotalData.reduce(function (a, b) { return intval(a) + intval(b); }, 0).toFixed(2);

    $(api.column(index).footer()).html('<span class="text-right d-block"> ' +
        inIndianCurruncy(pageTotal)   + ' Total (' + inIndianCurruncy(searchTotal) + ')  </span>'
    );

    // $(api.column(index).footer()).html(
    //  pageTotal + ', search total [' + searchTotal + '], total (' + total + ')'
    //);
}