﻿function Validation() {
    var CustomerName = $("#inputCustomerName").val();
    var Type = $("#inputType").val();
    //var Type = $('#inputType').find(":selected").text();
    var ReminderDate = $("#inputDate").val();
    var Amount = $("#inputAmount").val();

    if (CustomerName == "") {
        $.alert.open({
            content: "Enter Customer Name", title: ClientName,
            callback: function () {
                $("#inputCustomerName").focus();
            }
        });
        return false;
    }
    if (Type == "0") {
        $.alert.open({
            content: "Please Select Type", title: ClientName,
            callback: function () {
                $("#inputType").focus();
            }
        });
        return false;
    }

    if (ReminderDate == "") {
        $.alert.open({
            content: "Please Select Date", title: ClientName,
            callback: function () {
                $("#inputDate").focus();
            }
        });
        return false;
    }


    var d = new Date(ReminderDate);
    if (d == "Invalid Date") {
        $.alert.open({
            content: "Please enter valid date.", title: ClientName,
            callback: function () {
                $("#inputDate").focus();
            }
        });
        return false;
    }

    if (Amount <= 0) {
        $.alert.open({
            content: "Amount shoild be greater than zero.", title: ClientName,
            callback: function () {
                $("#inputAmount").focus();
            }
        });
        return false;
    }

    return true;
}

function ReminderMasterInsert() {
    if (!Validation())
        return false;

    var CustomerName = $("#inputCustomerName").val();
    var Type = $("#inputType").val();
    var ReminderDate = $("#inputDate").val();
    var Amount = $("#inputAmount").val();
    var Description = $("#inputDescription").val();

    if (Amount == "")
        Amount = 0;

    var JsonObj =
    {
        CustomerName: CustomerName,
        ReminderType: Type,
        ReminderDate: ReminderDate,
        Amount: Amount,
        Description: Description,
        Status: Status,
    };

    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Reminder.aspx/Insert",
        data: JData,
        dataType: "json",

        success: function (data) {
            $("#ResponseMsgLbl").text("Reminder Submited Successfully");
            ShowResponse(data);
        },

        error: function (data) {
            ShowResponse(data);
        }
    });
    location.reload();
}

function ReminderMasterSelectByID(ReminderMasterID) {
    $("#inputCustomerName").val("Please wait");
    $("#inputType").val("Please wait");
    $("#inputDate").val("Please wait");
    $("#inputAmount").val("Please wait");
    $("#inputDescription").val("Please wait");

    var JsonObj =
    {
        ReminderMasterID: ReminderMasterID,
    };

    var JData = JSON.stringify(JsonObj);
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Reminder.aspx/SelectByID",
        data: JData,
        dataType: "json",

        success: function (data) {

            $("#inputCustomerName").val(data.d[0].CustomerName);

            var ReminderType = data.d[0].ReminderType;

            var Type = $("#inputType").val();

            console.log(Type);



            if ($("#inputType").val() == null || $("#inputType").val() == undefined || $("#inputType").val() == "0") {

                $("#inputType").val(ReminderType).trigger('chosen:updated');

            }



            $("#inputDate").val(data.d[0].ReminderDate);
            $("#inputAmount").val(data.d[0].Amount);
            $("#inputDescription").val(data.d[0].Description);
            $("#ReminderID").html(data.d[0].ReminderMasterID);
            $('input[type="checkbox"]').prop("checked", true);
            $("#SubmitButton").hide();
            $("#UpdateButton").show();
            $("#loader").hide();
        },

        error: function (data) {
            $.alert.open({
                content: 'Data is not found', title: 'Consisty',
            });
        }
    });
}



function ReminderMasterUpdate() {
    if (!Validation())
        return false;

    if (CheckStatus())
        Status = "activated";
    else
        Status = "deactivated";

    var ReminderMasterID = $("#ReminderID").html();
    var CustomerName = $("#inputCustomerName").val();
    var Type = $("#inputType").val();
    var ReminderDate = $("#inputDate").val();
    var Amount = $("#inputAmount").val();
    var Description = $("#inputDescription").val();

    var JsonObj =
    {
        ReminderMasterID: ReminderMasterID,
        CustomerName: CustomerName,
        ReminderType: Type,
        ReminderDate: ReminderDate,
        Amount: Amount,
        Description: Description,
        Status: Status,
    };

    var JData = JSON.stringify(JsonObj);

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Update',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Reminder.aspx/Update",
                data: JData,
                dataType: "json",

                success: function (data) {
                    location.href = "ReminderReport.aspx";
                },

                error: function (data) {
                    ShowResponse(data);
                }
            });
        }
    });
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}

function SelectCustomerName() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Reminder.aspx/SelectCustomerName",
        data: {},
        async: false,
        dataType: "JSON",
        success: function (data) {
            BindCustomerName(data);
        },
        error: function (result) {
            ShowResponse(result);
        }
    });
}

function BindCustomerName(data) {
    for (i = 0; i < data.d.length; i++)
        CustomerNames.push(data.d[i].CustomerName);
}

function SelectReminderType() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Reminder.aspx/SelectReminderType",
        data: {},
        async: false,
        dataType: "JSON",
        success: function (data) {
            $("#inputType").html("");
            $("#inputType").append("<option value='0'>Select Type</option> ");
            for (var i = 0; i < data.d.length; i++) {
                var strRowDropdown = [];
                strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].ReminderTypeID, data.d[i].TypeName));
                $("#inputType").append(strRowDropdown);
            }
            $("#inputType").chosen();
        },
        error: function (result) {
            alert("Not FOund");
        }
    });
}

function autocomplete(inp, arr) {
    var currentFocus;

    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;

        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;

        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");

        this.parentNode.appendChild(a);

        for (i = 0; i < arr.length; i++) {
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                b = document.createElement("DIV");

                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);

                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";

                b.addEventListener("click", function (e) {
                    inp.value = this.getElementsByTagName("input")[0].value;

                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });

    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            currentFocus++;

            addActive(x);
        } else if (e.keyCode == 38) { //up
            currentFocus--;

            addActive(x);
        } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        if (!x) return false;

        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);

        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

function CheckStatus() {
    if ($('input[type = "checkbox"]').prop("checked") == true)
        return true;
    else if ($('input[type = "checkbox"]').prop("checked") == false)
        return false;
}

function Reset() {
    $("#inputCustomerName").val("");
    $("#inputType").val(0).trigger('chosen:updated');
    $("#inputDate").val("");
    $("#inputAmount").val("");
    $("#inputDescription").val("");
    $("#ReminderID").html("");
    $('input[type="checkbox"]').prop("checked", true);
    $("#SubmitButton").show();
    $("#UpdateButton").hide();
    $("#inputCustomerName").focus();
}

