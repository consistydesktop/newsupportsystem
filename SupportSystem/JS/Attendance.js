﻿function BindYear() {
    $("#inputYear").html("");
    var Year = new Date().getFullYear();
    var strRowDropdown = [];
    strRowDropdown.push(String.format("<option value='0'>Select Year</option> ", Year));
    for (var i = 0; i < 10; i++) {
        strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {0} </option> ", Year));
        Year--;
        $("#inputYear").append(strRowDropdown);
    }
    $("#inputYear").chosen();
}

function Search() {
    var Month = $("#inputMonth").val();
    var Year = $("#inputYear").val();
    
    if (Month == "0") {
        $.alert.open({
            content: "Select Month", title: ClientName,
            callback: function () {
                $("#inputMonth").focus();
            }
        });
        return false;
    }

    var JsonObj =
   {
       Month: Month,
       Year:Year,
   };

    var JData = JSON.stringify(JsonObj);

    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Attendance.aspx/Select",
        data: JData,
        dataType: "json",

        success: function (data) {
            $("#loader").hide();
            if (data.d.length < 1) {
                $.alert.open({
                    content: "Record Not found. Please select valid month.", icon: 'error', title: ClientName,
                    callback: function () {
                    }
                });
            }
            else {
                BindData(data);
            }
        },

        error: function (data) {
        }
    });
}

function BindData(data) {

    $("#AttendanceDetails").html("");
    $("#AttendanceDetails").html("<thead><tr><th>#</th><th>Name</th><th>Days < 5 Hours</th><th>Days</th><th>Absent Days</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';


        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td class="Grcenter"> {0} </td>', (i + 1)));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Name));
        strRow.push(String.format('<td> {0} </td>', data.d[i].CountOfLessThan5Hours));
        strRow.push(String.format('<td> {0} </td>', data.d[i].DatesOfLessThan5Hour));
        strRow.push(String.format('<td> {0} </td>', data.d[i].CountOfAbsentDays));


        strRow.push('</tr>');
        $("#AttendanceDetails").append(strRow.join(""));
    }

    $('#AttendanceDetails').dataTable({
        destroy: true,
        dom: 'Blfrtip',
        buttons: ['excelHtml5', 'pdfHtml5'],
        "order": [[2, "desc"]]
    });

}