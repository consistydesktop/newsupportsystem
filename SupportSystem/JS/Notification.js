﻿function ShowRespose(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
            NotificationMasterSelect();
        }
    });
}

function BindDataNotificationMaster(data) {
    if (data.d.length>0) {
    $("#NotificationMasterDetails").html("");
    $("#NotificationMasterDetails").html("<thead><tr><th>#</th><th>Date</th><th>Notification</th><th>Edit</th><th>Delete</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td> {0} </td>', data.d[i].DOCInString));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Notification));

        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return NotificationMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].NotificationID));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return  NotificationMasterDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].NotificationID));

        strRow.push('</tr>');
        $("#NotificationMasterDetails").append(strRow.join(""));
    }
}
}

function NotificationMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/Notification.aspx/Select",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindDataNotificationMaster(data);
        },
        error: function (result) {
            //$.alert.open({ content: "Data not Found", icon: 'error', title: ClientName, });
        }
    });
}

function NotificationMasterSelectNotification() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/Notification.aspx/SelectNotification",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            if (data.d.length > 0) {
                $(".spanNotification").html(data.d[0].Notification);
            }
        },
        rror: function (result) {
            $.alert.open({ content: "Something wrong", icon: 'error', title: ClientName, });
        }
    });
}

function NotificationMasterInsert() {
    var Notification = $("#inputNotification").val();

    if (Notification == "") {
        $.alert.open({
            content: String.format("Please enter message"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputNotification").focus();
            }
        });
        return false;
    }

    var jsonObj = {
        Notification: Notification,
    };

    var jData = JSON.stringify(jsonObj)
    $.alert.open({
        type: 'confirm',
        content: 'Are you want to add notification',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }
            if (button.toUpperCase() != 'Yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Notification.aspx/Insert",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowRespose(data);
                    location.reload();
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing worng"), icon: 'error', title: ClientName,
                    });
                }
            });
        }
    });
    return false;
}

function NotificationMasterDelete(NotificationID) {
    var jsonObj = {
        NotificationID: NotificationID,
    };

    var jData = JSON.stringify(jsonObj)
    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Notification.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                    location.reload();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('not updated'), icon: 'error', title: ClientName, });
        }
    });
}

function NotificationMasterSelectByID(NotificationID) {
    $("#inputNotification").val("Please Wait...");

    var jsonObj = {
        NotificationID: NotificationID,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/Notification.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#inputNotification").val(data.d.Notification);
            $("#NotificationID").html(NotificationID);
            $("#SubmitButton").css('display', 'none');
            $("#UpdateButton").show();
        },
        error: function (result) {
            $.alert.open({ content: "not Updated", icon: 'error', title: ClientName, });
        }
    });
}

function NotificationMasterUpdate() {
    var Notification = $("#inputNotification").val();
    var NotificationID = $("#NotificationID").text();

    var jsonObj = {
        NotificationID: NotificationID,
        Notification: Notification
    };
    var jData = JSON.stringify(jsonObj)

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Update',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Notification.aspx/Update",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                    location.reload();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not inserted'), icon: 'error', title: ClientName, });
        }
    });
    Reset();
}

