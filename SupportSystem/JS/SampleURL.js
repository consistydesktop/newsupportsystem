﻿function SampleURLValidation() {
    var ProviderID = $("#txtProviderName").val();
    var ServiceID = $("#txtServiceName").val();
    var URL = $("#txtURL").val();
    var DateFormat = $("#txtDateFormat").val();
    var URLType = $("#ddlURLType").val();
    var MethodType = $("#ddlMethodType").val();
    var HeaderJSON = $("#txtHeaderJSON").val();




    if (ProviderID == '0' || ProviderID == "" || ProviderID == undefined) {
        $.alert.open({
            content: String.format("Please select provider "), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtProviderName").focus();
            }
        });
        return false;
    }



    if (ServiceID == '0' || ServiceID == "" || ServiceID == undefined) {
        $.alert.open({
            content: String.format("Please select service "), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtServiceName").focus();
            }
        });
        return false;
    }
    if (URL == "") {
        $.alert.open({
            content: String.format("Please enter URL"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtURL").focus();
            }
        });
        return false;
    }




    if (MethodType == '0' || MethodType == "" || MethodType == undefined) {
        $.alert.open({
            content: String.format("Please select method type"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#ddlMethodType").focus();
            }
        });

        return false;
    }
    if (MethodType == "POST") {
        if (HeaderJSON == "") {
            $.alert.open({
                content: String.format("Please enter Header JSON"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#txtHeaderJSON").focus();
                }
            });
            return false;
        }
    }


    if (URLType == '0' || URLType == "" || URLType == undefined) {
        $.alert.open({
            content: String.format("Please select URL type"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#ddlURLType").focus();
            }
        });

        return false;
    }
}

function SampleURLInsert() {
    if (SampleURLValidation() == false) {
        return false;
    }
    var ProviderID = $("#txtProviderName").val();
    var ServiceID = $("#txtServiceName").val();
    var ServiceName = $("#txtServiceName option:selected").text();

    

    var URL = $("#txtURL").val();
    var DateFormat = $("#txtDateFormat").val();
    var URLType = $("#ddlURLType").val();
    var MethodType = $("#ddlMethodType").val();
    var HeaderJSON = $("#txtHeaderJSON").val();


    var jsonObj = {
        ProviderID: ProviderID,
        ServiceID: ServiceID,
        ServiceName: ServiceName,
        URL: URL,
        DateFormat: DateFormat,
        URLType: URLType,
        MethodType: MethodType,
        HeaderJSON: HeaderJSON
    };

    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "SampleURL.aspx/Insert",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            ShowRespose(data);
            Select();
        },
        error: function (result) {

        }
    });

    return false;
}

function Select() {
    var ProviderID = $("#txtProviderName").val();

    var jsonObj = {
        ProviderID: ProviderID,
    };
    var jData = JSON.stringify(jsonObj);
    $("#SampleURLDetails").html("");
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/SampleURL.aspx/SelectBYProviderID",
        data: jData,
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindData(data);
        },
        error: function (result) {

        }
    });

}

function BindData(data) {
    if (data.d.length > 0) {
        $("#SampleURLDetails").html("");
        $("#SampleURLDetails").html("<thead><tr><th>#</th><th>Name</th><th>Service</th><th>URL</th><th>URLType</th><th>MethodType</th><th>HeaderJSON</th><th>DateFormat</th><th>Edit</th><th>Delete</th></tr></thead><tbody></tbody>");

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));
            strRow.push(String.format('<td> {0} </td>', data.d[i].DeveloperName));
            strRow.push(String.format('<td> {0} </td>', data.d[i].ServiceName));
            strRow.push(String.format('<td> {0} </td>', data.d[i].URL));
            strRow.push(String.format('<td> {0} </td>', data.d[i].URLType));
            strRow.push(String.format('<td> {0} </td>', data.d[i].MethodType));
            strRow.push(String.format('<td> {0} </td>', data.d[i].HeaderJSON));
            strRow.push(String.format('<td> {0} </td>', data.d[i].DateFormat));
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return SampleURLSelectBySampleURLID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].SampleURLID));
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return  SampleURLDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].SampleURLID));


            strRow.push('</tr>');
            $("#SampleURLDetails").append(strRow.join(""));
        }
    }
}




function SampleURLSelectBySampleURLID(SampleURLID) {
    location.href = "SampleURL.aspx?SampleURLID =" + SampleURLID;
}

function SampleURLSelectByID(SampleURLID) {

    var jsonObj = {
        SampleURLID: SampleURLID,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/SampleURL.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
           // $("#txtProviderName").val(data.d.ProviderID);

            $("#txtProviderName").val(data.d.ProviderID).trigger('chosen:updated');
            $("#txtServiceName").val(data.d.ServiceID);
            $("#txtURL").val(data.d.URL);
            $("#txtDateFormat").val(data.d.DateFormat);
            $("#ddlURLType").val(data.d.URLType);
            $("#ddlMethodType").val(data.d.MethodType);
            $("#txtHeaderJSON").val(data.d.HeaderJSON);

            if (data.d.MethodType == 'POST') {
                $("#ddlMethodType").val('POST');
                $("#divHeaderJson").css('display', 'block');
            } else {
                $("#ddlMethodType").val('GET');
            }


            $("#txtSampleURLID").html(data.d.SampleURLID);
            $("#SubmitButton").css('display', 'none');
            $("#UpdateButton").show();
        },
        error: function (result) {
            $.alert.open({ content: "not Updated", icon: 'error', title: ClientName, });
        }
    });
}

function SampleURLUpdate() {
    var SampleURLID = $("#txtSampleURLID").text();

    var ProviderID = $("#txtProviderName").val();
    var ServiceID = $("#txtServiceName").val();
    var ServiceName = $("#txtServiceName option:selected").text();
 
    var URL = $("#txtURL").val();
    var DateFormat = $("#txtDateFormat").val();
    var URLType = $("#ddlURLType").val();
    var MethodType = $("#ddlMethodType").val();
    var HeaderJSON = $("#txtHeaderJSON").val();

    if (SampleURLValidation() == false) {
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Update',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                SampleURLID: SampleURLID,
                ProviderID: ProviderID,
                ServiceID: ServiceID,
                ServiceName: ServiceName,
                URL: URL,
                DateFormat: DateFormat,
                URLType: URLType,
                MethodType: MethodType,
                HeaderJSON: HeaderJSON,

            };
            var jData = JSON.stringify(jsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "SampleURL.aspx/Update",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not inserted'), icon: 'error', title: ClientName, });
        }
    });
}

function SampleURLDelete(SampleURLID) {
    var SampleURLID = SampleURLID;

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                SampleURLID: SampleURLID,
            };

            var jData = JSON.stringify(jsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "SampleURL.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('not updated'), icon: 'error', title: ClientName, });
        }
    });
}

function ResetAll() {
    $("#ddlMethodType").val(0).trigger('chosen:updated');
    $("#ddlURLType").val(0).trigger('chosen:updated');
    $("#inputIssueType").val(0).trigger('chosen:updated');
    $("#txtProviderName").val("");
    $("#txtServiceName").val("");
    $("#txtDateFormat").val("");
    $("#txtHeaderJSON").val("");
    $("#txtURL").val("");
}

function ShowRespose(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Select();
            ResetAll();
        }
    });
}


function ProviderMasterSelect() {

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/APIProvider.aspx/Select",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataProviderMaster(data);
        },
        error: function (result) {
        }
    });
}
function BindDataProviderMaster(data) {
    $("#txtProviderName").html("");
    $("#txtProviderName").append("<option value=0>Select Provider </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].ProviderID, data.d[i].ProviderName));
        $("#txtProviderName").append(strRowDropdown);
    }

    $("#txtProviderName").trigger("chosen:updated");
    $("#txtProviderName").chosen({ width: "100%" });

}