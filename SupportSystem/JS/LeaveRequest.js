﻿
function LeaveRequestInsert() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    var Description = $("#inputDescription").val();
   

    if (FromDate == "") {
        $.alert.open({
            content: "Select From date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: "Select To date", title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });

        return false;
    }
    if (Description == "") {
        $.alert.open({
            content: "Enter The Desrciption", title: ClientName,
            callback: function () {
                $("#inputDescription").focus();
            }
        });
        return false;
    }
    if (Date.parse(FromDate)> Date.parse(ToDate)) {
        $.alert.open({
            content: "Select Valid Date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();

            }
        });
        return false;
    }

    var JsonObj =
    {
        FromDate: FromDate,
        ToDate: ToDate,
        Description: Description,
    };

    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/LeaveRequest.aspx/Insert",
        data: JData,
        dataType: "json",
        success: function (data) {
            ShowResponse(data);
        },
        error: function (data) {
            ShowResponse(data);
        }
    });
}
function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            location.reload();
        }
    });
}
