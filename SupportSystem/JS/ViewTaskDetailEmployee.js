﻿function UserInformationSelectEmployeeName() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTaskDetailEmployee.aspx/SelectEmployeeName",
        data: {},
        dataType: "json",

        success: function (data) {
            BindEmployees(data);
        }
    });
}

function BindEmployees(data) {
    $("#inputEmployee").html("");
    $("#inputEmployee").append("<option value=0>Select Employee</option> ");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
        $("#inputEmployee").append(strRowDropdown);
    }
    $("#inputEmployee").chosen();
}

function TaskMasterSelectByID(TaskID) {
    var jsonObj = {
        TaskID: TaskID,
    };

    var jData = JSON.stringify(jsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTaskDetailEmployee.aspx/SelectByID",
        data: jData,
        dataType: "json",

        success: function (data) {
           
            ViewAllTaskDetails(data);
        }
    });
}

function ViewAllTaskDetails(data) {
   
    $("#inputTask").val(data.d[0][0].Task);
    $("#inputDueDate").val(data.d[0][0].Deadline);
    $("#inputStatus").val(data.d[0][0].TaskStatus).trigger('chosen:updated');
    $("#inputDescription").val(data.d[0][0].Description);
    $("#inputNumberOfPoints").val(data.d[0][0].Point);

    $("#inputTask").attr("disabled", "disabled");
    $("#inputDueDate").attr("disabled", "disabled");
    $("#inputDescription").attr("disabled", "disabled");
    $("#inputNumberOfPoints").attr("disabled", "disabled");

    // For SubTask
    BindSubTaskMaster(data.d[1]);

    for (var i = 0; i < data.d[2].length; i++) {
        $("#CommentPanel").append('<div>' + data.d[2][i].Comment + '</div>');
    }

    $("#AddCommentButton").show();

    var UserID = [];
    for (var i = 0; i < data.d[3].length; i++) {
        UserID[i] = data.d[3][i].UserID;
    }
    $("#inputEmployee").val(UserID).trigger('chosen:updated');
    $("#inputEmployee").prop("disabled", "disabled");
}

function BindSubTaskMaster(data) {
    $("#TaskDetails").html("");

    
    {
        for (var i = 0; i < data.length; i++) {
            var strRow = [];

            var color = 'black';
            strRow.push(String.format("<tr style='color:{0};'>", color));

            // strRow.push(String.format('<td> <input type="checkbox" id /></td>', data[i].SubTaskID));
            strRow.push(String.format('<td> <select id="inputSubTaskStatus_{0}" class="form-control loginput" > <option value="0">Select Status</option> <option value="1">New</option> <option value="2">InProgress</option> <option value="3">Resolved</option> </select></td>', data[i].SubTaskID));
            $("#inputSubTaskStatus_" + data[i].SubTaskID).val(data[i].Status).trigger('chosen:updated');
            strRow.push(String.format('<td><input type="text" disabled id="inputSubTask{0}" class="form-control loginput" value="{1}" />', data[i].SubTaskID, data[i].SubTask));
            strRow.push(String.format('<td><button type="button" id="UpdateButton{0}" class="btn btn-info " onclick="SubTaskMasterUpdateStatus({0})" >Update</button>', data[i].SubTaskID));

            strRow.push('</tr>');
            $("#TaskDetails").append(strRow.join(""));
        }
    }
}

function SubTaskMasterUpdateStatus(SubTaskID) {
    var SubTaskStatus = $("#inputSubTaskStatus_" + SubTaskID).val();

    if (SubTaskStatus == "0") {
        $.alert.open({
            content: "Please update Status", icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputSubTaskStatus_" + SubTaskID).focus();
            }
        });
    }

    var jsonObj = {
        SubTaskID: SubTaskID,
        SubTaskStatus: SubTaskStatus,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTaskDetailEmployee.aspx/UpdateSubTaskStatus",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            ShowResponse(data);
        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function DownloadAttachment() {
    var TaskID = $("#TaskID").html();
    var jsonObj = {
        TaskID: TaskID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTaskDetailEmployee.aspx/SelectAttachment",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BindDataAttachment(data);
            $("#modal_attachment").modal('show');
        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function BindDataAttachment(data) {
    $("#Attachments").html("");
    $("#Attachments").html("<thead><tr><th>Sr.No</th><th>Attachment</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format("<td class='text-center'> <a href='{0}' target='_blank'>{1}</a> </td>", data.d[i].Attachment, data.d[i].OriginalFileName));
        strRow.push('</tr>');
        $("#Attachments").append(strRow.join(""));
    }
}

function CommentMasterInsert() {


    var Comment = $("#inputComment").val();
    var TaskID = $("#TaskID").html();

    if (Comment == "") {
        $.alert.open({
            content: "Enter Comment here ", title: ClientName,
            callback: function () {
                $("#inputComment").focus();
            }
        });
        return false;
    }

    var jSonObj = {
        TaskID: TaskID,
        Comment: Comment,
    };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTaskDetailEmployee.aspx/InsertComment",
        data: JSON.stringify(jSonObj),
        dataType: "json",

        success: function (data) {
            //     BindEmployees(data);
            ShowResponse(data);
            location.reload();
        }
    });
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            location.reload();
        }
    });
}

function GetTaskID() {
    if (window.location.href.indexOf("TaskID") > -1) {
        var url = window.location.href;
        var id = url.split('=');
    }
    return id[1];
}

function TaskMasterUpdateStatus() {
    var Status = $("#inputStatus").val();
    var TaskID = GetTaskID();

    var jsonObj = {
        TaskID: TaskID,
        Status: Status,
    };

    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTaskDetailEmployee.aspx/UpdateTaskStatus",
        data: jData,
        dataType: "json",

        success: function (data) {
            ShowResponse(data);
        }
    });
}