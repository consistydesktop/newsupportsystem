﻿function APIProviderValidation() {
    var ProviderName = $("#txtProviderName").val();

    var URL = $("#txtURL").val();
    if (ProviderName == "") {
        $.alert.open({
            content: String.format("Please enter Provider Name"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtProviderName").focus();
            }
        });
        return false;
    }


    if (URL == "") {
        $.alert.open({
            content: String.format("Please enter URL"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtURL").focus();
            }
        });
        return false;
    }
}

function APIProviderInsert() {
    if (APIProviderValidation() == false) {
        return false;
    }
    var ProviderName = $("#txtProviderName").val();
        var URL = $("#txtURL").val();
    
    var jsonObj = {
        ProviderName: ProviderName,
               URL: URL,
        };

    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "APIProvider.aspx/Insert",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            ShowRespose(data);
            Select();
        },
        error: function (result) {
                    }
    });

    return false;
}

function Select() {

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/APIProvider.aspx/Select",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindData(data);
        },
        error: function (result) {

        }
    });

}

function BindData(data) {
    if (data.d.length > 0) {
        $("#APIProviderDetails").html("");
        $("#APIProviderDetails").html("<thead><tr><th>#</th><th>Name</th><th>URL</th><th>Edit</th><th>Delete</th></tr></thead><tbody></tbody>");

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));
            strRow.push(String.format('<td> {0} </td>', data.d[i].ProviderName));

            strRow.push(String.format('<td> {0} </td>', data.d[i].URL));
      
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return APIProviderSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].ProviderID));
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return  APIProviderDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].ProviderID));


            strRow.push('</tr>');
            $("#APIProviderDetails").append(strRow.join(""));
        }
    }
}

function APIProviderSelectByID(ProviderID) {

    var jsonObj = {
        ProviderID: ProviderID,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/APIProvider.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#txtProviderName").val(data.d.ProviderName);
      
            $("#txtURL").val(data.d.URL);
       

            $("#txtProviderID").html(data.d.ProviderID);
            $("#SubmitButton").css('display', 'none');
            $("#UpdateButton").show();
        },
        error: function (result) {
            $.alert.open({ content: "not Updated", icon: 'error', title: ClientName, });
        }
    });
}

function APIProviderUpdate() {
    var ProviderID = $("#txtProviderID").text();
   
    var ProviderName = $("#txtProviderName").val();
  
    var URL = $("#txtURL").val();
 

    if (APIProviderValidation() == false) {
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Update',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                ProviderID: ProviderID,
                ProviderName: ProviderName,
             
                URL: URL,
               

            };
            var jData = JSON.stringify(jsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "APIProvider.aspx/Update",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data); Select();
                    ResetAll();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not inserted'), icon: 'error', title: ClientName, });
        }
    });
}

function APIProviderDelete(ProviderID) {
    var ProviderID = ProviderID;

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                ProviderID: ProviderID,
            };

            var jData = JSON.stringify(jsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "APIProvider.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data); Select();
                    ResetAll();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('not updated'), icon: 'error', title: ClientName, });
        }
    });
}

function ResetAll() {

    $("#txtProviderName").val("");
    $("#txtURL").val("");
    
}

function ShowRespose(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Select();
            ResetAll();
        }
    });
}