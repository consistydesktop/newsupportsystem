﻿function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AndroidSupport.aspx/SelectSystemName",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataSystemMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}
function BindDataSystemMaster(data) {
    $("#inputSystemName").html("");
    $("#inputSystemName").append("<option  value='0'>Select System </option>");

    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#inputSystemName").append(strRowDropdown);
    }
    $("#inputSystemName").chosen();
}
function AndroidSupportValidation() {
    var SystemID = $("#inputSystemName").val();
    var PlayStoreURL = $("#inputPlayStoreURLID").val();
    var GoogleConsoleUserName = $("#inputGCUserName").val();
    var GoogleConsolePassword = $("#inputGCPassword").val();
    var Description = $("#inputDescription").val();
    var CodeRepository = $("#inputCodeRepository").val();


    if (SystemID == "") {
        $.alert.open({
            content: "Select System  Name", title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });
        return false;
    }
    if (PlayStoreURL == "") {
        $.alert.open({
            content: "please enter the play store url", title: ClientName,
            callback: function () {
                $("#inputPlayStoreURLID").focus();
            }
        });

        return false;
    }
    if (GoogleConsoleUserName == "") {
        $.alert.open({
            content: "Enter the username", title: ClientName,
            callback: function () {
                $("#inputGCUserName").focus();
            }
        });
        return false;
    }
    if (GoogleConsolePassword == "") {
        $.alert.open({
            content: "Enter the Password", title: ClientName,
            callback: function () {
                $("#inputGCPassword").focus();
            }
        });
        return false;
    }
    if (Description == "") {
        $.alert.open({
            content: "Enter the Description", title: ClientName,
            callback: function () {
                $("#inputDescription").focus();
            }
        });
        return false;
    }
    if (CodeRepository == "") {
        $.alert.open({
            content: "Enter the Code Repository", title: ClientName,
            callback: function () {
                $("#inutCodeRepository").focus();
            }
        });
        return false;

    }
    return true;
}
function AndroidSupportInsert(result) {
    if (!AndroidSupportValidation()) {
        return;
    }

    var SystemID = $("#inputSystemName").val();
    var PlayStoreURL = $("#inputPlayStoreURLID").val();
    var GoogleConsoleUserName = $("#inputGCUserName").val();
    var GoogleConsolePassword = $("#inputGCPassword").val();
    var Description = $("#inputDescription").val();
    var CodeRepository = $("#inputCodeRepository").val();
    var FileIDs = result;

    var JsonObj =
    {
        SystemID: SystemID,
        PlayStoreURL: PlayStoreURL,
        GoogleConsoleUserName: GoogleConsoleUserName,
        GoogleConsolePassword: GoogleConsolePassword,
        Description: Description,
        CodeRepository: CodeRepository,
        FileIDs: FileIDs,
    };

    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/AndroidSupport.aspx/Insert",
        data: JData,
        dataType: "json",
        success: function (data) {
            ShowResponse(data);
        },
        error: function (data) {
            ShowResponse(data);
        }
    });
}

$("#AddButton").click(function (evt) {
    if (AndroidSupportValidation() == false) {
        return false;
    }
    var File = $("#inputAttachment").val();
    if (File == "") {
        var result = 0;
        AndroidSupportInsert(result);
    }

    else {
        var data = new FormData();
        var fileUpload = $("#inputAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('JKSDoc', files[i]);
        }


        var KeystorefileUpload = $("#inputKeyStoreAttachment").get(0);
        var Keystorefiles = KeystorefileUpload.files;

        for (var i = 0; i < Keystorefiles.length; i++) {
            data.append('KeyStoreDoc', Keystorefiles[i]);
        }




        $.ajax({
            url: "FileUploadHandlerForAndroidData.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else {
                    AndroidSupportInsert(result);
                }


            },

            error: function (err) {
                alert('Error:' + err)
            }
        });
    }

    evt.preventDefault();
});
function ShowResponseFile(result) {

    $.alert.open({
        content: result, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}


function BindAndroidData(data) {

    var mainData = [];

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];

        var color = 'black';


        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', (i + 1), color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].DOC, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].SystemName, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].PlayStoreURL, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].GoogleConsolePassword, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].GoogleConsoleUserName, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].Description, color));
        strRow.push(String.format("<td class='text-center'> <a href='{0}'>{1}</a> </td>", data.d[i].JKSFileFilePath, data.d[i].JKSFileName));
        strRow.push(String.format("<td class='text-center'> <a href='{0}'>{1}</a> </td>", data.d[i].KeyStoreFilePath, data.d[i].KeyStoreFileName));

        mainData.push(strRow);
    }
    $("#AndroidDetails").html("");
    $("#AndroidDetails").html("<thead> <tr> <th>#</th> <th>Date</th>  <th>System</th>  <th>URL</th>  <th>Pass</th>  <th>GC UserName</th>     <th>Description</th>       <th>JKS File</th> <th>KeyStore File</th>  </tr> </thead>")
    $("#AndroidDetails").append("<tfoot> <tr> <th></th>   <th></th>     <th></th>      <th></th>       <th></th>          <th></th>                 <th></th>     <th></th>       </tr> </tfoot>")

    createDataTable('AndroidDetails', mainData, []);
}

function Validation() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();

    if (FromDate == "") {
        $.alert.open({
            content: "Select From date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: "Select To date", title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });
        return false;
    }

    return true;
}

function AndroidDataSelect() {
    if (!Validation())
        return false;

    var FromDate = "";
    var ToDate = "";
    var SystemID = $("#inputSystemName").val();

    FromDate = $("#inputFromDate").val();
    ToDate = $("#inputToDate").val();


    var JsonObj =
    {
        FromDate: FromDate,
        ToDate: ToDate,
        SystemID: SystemID,
    };

    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AndroidSupportReport.aspx/AndroidDataSelect",
        data: JData,
        dataType: "json",

        success: function (data) {
            if (data.d.length < 1) {
                $.alert.open({
                    content: "Data Not found", icon: 'error', title: ClientName,
                    callback: function () {
                    }
                });
            }
            else {
                BindAndroidData(data);
            }
        },

        error: function (data) {
        }
    });
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
        }
    });
}