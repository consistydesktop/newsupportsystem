﻿function UserInformationSelectEmployeeName() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AssignTask.aspx/SelectEmployeeName",
        data: {},
        async: false,
        dataType: "json",

        success: function (data) {
            BindEmployees(data);
        }
    });
}

function BindEmployees(data) {
    $("#inputEmployee").html("");
    //  $("#inputEmployee").append("<option  value='0'>Select Employee </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
        $("#inputEmployee").append(strRowDropdown);
    }
    $("#inputEmployee").chosen();
}

function InsertSubTask() {
    if (count > 0) {
        var SubTaskStatus = $("#inputStatus" + count).val();
        var SubTask = $("#inputSubTask" + count).val();

        if (SubTaskStatus == 0) {
            $.alert.open({
                content: String.format("Please select Status"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#inputStatus" + count).focus();
                }
            });
            return;
        }
        if (SubTask == "") {
            $.alert.open({
                content: String.format("Please Enter SubTask"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#inputSubTask" + count).focus();
                }
            });
            return;
        }

        var strRow = [];
        count++;
        var color = 'black';
        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td><input type="text" id="inputSubTask{0}" class="form-control loginput" />', count/* data.d[i].SubTaskID*/));
        strRow.push(String.format('<td><select id="inputStatus{0}" class="form-control loginput" ><option value="0">Select Status</option><option value="1" selected="selected">NEW</option><option value="2">INPROGRESS</option><option value="3">RESOLVED</option></select></td>', count/*, data.d[i].SubTaskID*/));
        strRow.push(String.format('<td><button type="button" id="AddButton{0}" class="btn btn-info w-100 logbutton buttons" onclick="InsertSubTask()" >Add</button>', count /* ,data.d[i].SubTaskID */));

        strRow.push('</tr>');
        $("#TaskDetails").append(strRow.join(""));
    }
    else {
        var strRow = [];
        count++;
        var color = 'black';
        strRow.push(String.format("<tr style='color:{0};'>", color));


        strRow.push(String.format('<td width=50% ><input type="text" id="inputSubTask{0}" class="form-control loginput" />', count/* data.d[i].SubTaskID*/));
        strRow.push(String.format('<td width=35% ><select id="inputStatus{0}" class="form-control loginput" ><option value="0">Select Status</option><option value="1" selected="selected">NEW</option><option value="2">INPROGRESS</option><option value="3">RESOLVED</option></select></td>', count/*, data.d[i].SubTaskID*/));
        strRow.push(String.format('<td width=15% ><button type="button" id="AddButton{0}" class="btn btn-info w-100 logbutton buttons" onclick="InsertSubTask()" >Add</button>', count /* ,data.d[i].SubTaskID */));

        strRow.push('</tr>');
        $("#TaskDetails").append(strRow.join(""));
    }
}

function BindSubTaskMaster(data) {
    $("#TaskDetails").html("");

    //if (data == undefined) {
    //    var strRow = [];

    //    var color = 'black';
    //    strRow.push(String.format("<tr style='color:{0};'>", color));

    //    strRow.push(String.format('<td><select id="inputStatus{0}" class="form-control loginput" ><option value="0">Select Status</option><option value="1">New</option><option value="2">InProgress</option><option value="3">Resolved</option></select></td>', 1/*, data.d[i].SubTaskID*/));
    //    strRow.push(String.format('<td><input type="text" id="inputSubTask{0}" class="form-control loginput" />', 1/* data.d[i].SubTaskID*/));
    //    strRow.push(String.format('<td><button type="button" id="AddButton{0}" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="InsertTaskDetailMaster(0)" >Add</button>', 1  /* ,data.d[i].SubTaskID */));

    //    strRow.push('</tr>');
    //    $("#TaskDetails").append(strRow.join(""));
    //}
    //else
    {
        for (var i = 0; i < data.length; i++) {
            var strRow = [];

            var color = 'black';
            strRow.push(String.format("<tr style='color:{0};'>", color));


            strRow.push(String.format('<td><input type="text" disabled id="inputSubTask{0}" class="form-control loginput" value="{1}" />', data[i].SubTaskID, data[i].SubTask));
            strRow.push(String.format('<td><select id="inputStatus{0}" class="form-control loginput" ><option value="0">Select Status</option><option value="1" selected="selected">NEW</option><option value="2">INPROGRESS</option><option value="3">RESOLVED</option></select></td>', data[i].SubTaskID));
            $("#inputStatus" + data[i].SubTaskID).val(data[i].Status).trigger('chosen:updated');
            strRow.push(String.format('<td><button type="button" id="UpdateButton{0}" class="btn btn-info w-100 mt-4 logbutton buttons" oncklick="SubTaskMasterUpdate({0})" >Update</button>', data[i].SubTaskID));

            strRow.push('</tr>');
            $("#TaskDetails").append(strRow.join(""));
        }

        var strRow = [];

        var color = 'black';
        strRow.push(String.format("<tr style='color:{0};'>", color));

        strRow.push(String.format('<td><select id="inputStatus{0}" class="form-control loginput" ><option value="0">Select Status</option><option value="1">NEW</option><option value="2">INPROGRESS</option><option value="3">RESOLVED</option></select></td>', count));
        strRow.push(String.format('<td><input type="text" id="inputSubTask{0}" class="form-control loginput" />', count));
        strRow.push(String.format('<td><button type="button" id="AddButton{0}" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="InsertSubTask(0)" >Add</button>', count));

        strRow.push('</tr>');
        $("#TaskDetails").append(strRow.join(""));
    }
}

function InsertTaskDetailMaster(TaskID) {
    TaskID = 1;
    var SubTask = $("#inputSubTask" + TaskID).val();
    var Status = $("#inputStatus" + TaskID).val();
}

function TaskMasterInsert(result) {
    var EmployeeID = $("#inputEmployee").val();
    var Task = $("#inputTask").val();
    var DueDate = $("#inputDueDate").val();
    var Status = $("#inputStatus").val();
    var Description = $("#inputDescription").val();

    var NumberOfPoints = $("#inputNumberOfPoints").val();
    var Comment = $("#inputComment").val();
    var SystemID = $("#inputSystemName").val();


    if (!TaskMasterValidation())
        return false;


    if (NumberOfPoints == "")
        NumberOfPoints = 0;

    var FileIDs = result;

    var table = document.getElementById("TaskDetails");

    for (i = 1; i <= table.rows.length; i++) {
        var SubTaskStatus = $("#inputStatus" + i).val();
        var SubTask = $("#inputSubTask" + i).val();

        if (SubTaskStatus != 0 && SubTask != "") {
            var jSonObj = {
                SubTaskStatus: SubTaskStatus,
                SubTask: SubTask,
            }
            SubTaskObjects.push(jSonObj);
        }
    }

    var jSonObj = {
        EmployeeID: EmployeeID,
        SystemID: SystemID,
        Task: Task,
        FileIDs: FileIDs,
        DueDate: DueDate,
        Status: Status,
        Description: Description,
        NumberOfPoints: NumberOfPoints,
        Comment: Comment,
        SubTaskObjects: SubTaskObjects,
    };

   // $("#loader").show();

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AssignTask.aspx/Insert",
        data: JSON.stringify(jSonObj),
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            $("#loader").hide();
            if (data.d > -1) {
                $("#TaskID").val(data.d);

                $.alert.open({
                    content: "Task assigned successfully", icon: 'warning', title: ClientName,
                    callback: function () {
                        ResetAll();
                        location.reload();
                    }
                });
            }
            else {
                $.alert.open({
                    content: "Task not assigned ", icon: 'warning', title: ClientName,
                    callback: function () {
                        ResetAll();
                        location.reload();
                    }
                });
            }
        }
    });
}

function TaskMasterUpdate(result) {

    var TaskID = $("#TaskID").html();
    var EmployeeID = $("#inputEmployee").val();
    var Task = $("#inputTask").val();
    var DueDate = $("#inputDueDate").val();
    var Status = $("#inputStatus").val();
    var Description = $("#inputDescription").val();
    var NumberOfPoints = $("#inputNumberOfPoints").val();
    var Comment = $("#inputComment").val();
    var SystemID = $("#inputSystemName").val();
    if (!TaskMasterValidation())
        return false;


    var FileIDs = result;

    var table = document.getElementById("TaskDetails");

    var jSonObj = {
        EmployeeID: EmployeeID,
        SystemID: SystemID,
        TaskID: TaskID,
        Task: Task,
        FileIDs: FileIDs,
        DueDate: DueDate,
        Status: Status,
        Description: Description,
        NumberOfPoints: NumberOfPoints,
        Comment: Comment,
    };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AssignTask.aspx/Update",
        data: JSON.stringify(jSonObj),
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            ShowResponse(data);
            ResetAll();
            location.href = '/AssignTask.aspx';
        }
    });
}

function TaskMasterValidation() {
    var EmployeeID = $("#inputEmployee").val();
    var Task = $("#inputTask").val();
    var Description = $("#inputDescription").val();
    var SystemID = $("#inputSystemName").val();

    if (EmployeeID == "" || EmployeeID == undefined) {
        $.alert.open({
            content: String.format("Please select employee"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputEmployee").focus();
            }
        });
        return false;
    }
    if (SystemID == 0) {
        $.alert.open({
            content: String.format("Please select system"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });
        return false;
    }

    if (Task == "" || Task == undefined) {
        $.alert.open({
            content: String.format("Please enter task"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputTask").focus();
            }
        });
        return false;
    }

    if (Description == "" || Description == undefined) {
        $.alert.open({
            content: String.format("Please enter some description"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputDescription").focus();
            }
        });
        return false;
    }


    return true;
}

$("#SubmitButton").click(function (evt) {

    var File = $("#inputAttachment").val();
    var File = $("#inputAttachmentSubTask").val();

    if (File == "") {
        var result = "";
        TaskMasterInsert(result);
    }
    else {
        var data = new FormData();
        var fileUpload = $("#inputAttachment").get(0);
        var fileUpload = $("#inputAttachmentSubTask").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForTask.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {


                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else {
                    TaskMasterInsert(result);
                }



            },
            error: function (err) {
                alert('Error:' + err)
            }
        });
    }
    evt.preventDefault();
});



function ShowResponseFile(result) {

    $.alert.open({
        content: result, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}



$("#UpdateButton").click(function (evt) {
    //if (TaskMasterValidation() == false) {
    //    return false;
    //}
    var File = $("#inputAttachment").val();
    var File = $("#inputAttachmentSubTask").val();

    if (File == "") {
        var result = 0;
        TaskMasterUpdate(result);
    }
    else {
        var data = new FormData();
        var fileUpload = $("#inputAttachment").get(0);
        var fileUpload = $("#inputAttachmentSubTask").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForTask.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {


                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else {

                    TaskMasterUpdate(result);
                }
            },
            error: function (err) {
                alert('Error:' + err)
            }
        });
    }
    evt.preventDefault();
});

function ResetAll() {
    $("#inputEmployee").val("").trigger('chosen:updated');
    $("#inputSystemName").val("").trigger('chosen:updated');

    $("#inputTask").val("");
    $("#inputDueDate").val("");
    $("#inputStatus").val(1).trigger('chosen:updated');
    $("#inputDescription").val("");
    $("#inputNumberOfPoints").val("");
    $("#inputComment").val("");
    $("#TaskDetails").html("");
    $("#CommentPanel").html("");
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
        }
    });
}

function TaskMasterSelectByID(TaskID) {
    var jsonObj = {
        TaskID: TaskID,
    };

    var jData = JSON.stringify(jsonObj);

    $("#loader").show();

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AssignTask.aspx/SelectByID",
        data: jData,
        dataType: "json",
        success: function (data) {
            $("#loader").hide();
            BindTaskForEdit(data);
        }
    });
}

function BindTaskForEdit(data) {

    $("#inputTask").val(data.d[0][0].Task);
    $("#inputDueDate").val(data.d[0][0].Deadline);

    $("#inputStatus").val(data.d[0][0].TaskStatus);
    $("#inputStatus").trigger('chosen:updated');


    $("#inputDescription").val(data.d[0][0].Description);
    $("#inputNumberOfPoints").val(data.d[0][0].Point);




    $("#inputSystemName").val(data.d[0][0].SystemID);
    $("#inputSystemName").trigger('chosen:updated');


    $("#inputSystemName").val(data.d[0][0].SystemID);
    $("#inputSystemName").trigger('chosen:updated');

    // For SubTask

    BindSubTaskMaster(data.d[1]);

    for (var i = 0; i < data.d[2].length; i++) {
        $("#CommentPanel").append('<div>' + data.d[2][i].Comment + '</div>');
    }



    var UserID = [];
    for (var i = 0; i < data.d[3].length; i++) {
        UserID[i] = data.d[3][i].UserID;
    }

    $("#inputEmployee").val(UserID).trigger('chosen:updated');
    $("#AddCommentButton").show();
    $("#DownloadAttachmentButton").show();
    $("#inputEmployee").val(UserID).trigger('chosen:updated');
}

function ViewAssignedTask() {
    
    var UserID = $("#ViewTaskButton").text();
       
}
function CommentMasterInsert() {
    var Comment = $("#inputComment").val();
    var TaskID = $("#TaskID").html();

    var jSonObj = {
        TaskID: TaskID,
        Comment: Comment,
    };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AssignTask.aspx/InsertComment",
        data: JSON.stringify(jSonObj),
        dataType: "json",

        success: function (data) {
            //     BindEmployees(data);
            ShowResponse(data);
            location.reload();
        }
    });
}

function SubTaskMasterUpdate(SubTaskID) {
    var s = SubTaskID;
}

function DownloadAttachment() {
    var TaskID = $("#TaskID").html();
    var jsonObj = {
        TaskID: TaskID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AssignTask.aspx/SelectAttachment",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BindDataAttachment(data);
            $("#modal_attachment").modal('show');
        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function BindDataAttachment(data) {
    $("#Attachments").html("");
    $("#Attachments").html("<thead><tr><th>Sr.No</th><th>Attachment</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format("<td class='text-center'> <a href='{0}' target='_blank'>{1}</a> </td>", data.d[i].Attachment, data.d[i].OriginalFileName));
        strRow.push('</tr>');
        $("#Attachments").append(strRow.join(""));
    }
}

function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AssignTask.aspx/SelectSystemName",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataSystemMaster(data);
        },
        //error: function (result) {
       //     $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
      //  }
    });
}

function BindDataSystemMaster(data) {
    $("#inputSystemName").html("");
    $("#inputSystemName").append("<option  value='0'>Select System </option>");

    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#inputSystemName").append(strRowDropdown);
    }
    $("#inputSystemName").chosen();
}

function ViewAssignedTaskUserwise() {

    var UserTypeID = $("#UserTypeID").text();
    if (UserTypeID == 1)
        window.location.href = "ViewTaskAdmin.aspx";

    if (UserTypeID == 2)
        window.location.href = "ViewTaskEmployee.aspx";
}