﻿function ServiceMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AddTicketClient.aspx/SelectServiceName",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataServiceMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}


function readURLTicketImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageTicket')
                .attr('src', e.target.result)
                .width(250)
                .height(250);
        };

        reader.readAsDataURL(input.files[0]);
    }

}
function BindDataServiceMaster(data) {
    $("#inputService").html("");
    $("#inputService").append("<option value='0'>Select Service </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].ServiceID, data.d[i].ServiceName));
        $("#inputService").append(strRowDropdown);
    }
    $("#inputService").chosen();
    $("#inputService").focus();
}

function AddTicketValidation() {

    var Title = $("#inputTitle").val();
    var ServiceID = $("#inputService").val();
    var RequestType = $("#inputRequestType").val();
    var Priority = $("#inputPriority").val();


    if (Title == "") {
        $.alert.open({
            content: String.format("Please enter title"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputTitle").focus();
            }
        });
        return false;
    }

    if (ServiceID == '0') {
        $.alert.open({
            content: String.format("Please select service"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputService").focus();
            }
        });
        return false;
    }

    if (RequestType == '0') {
        $.alert.open({
            content: String.format("Please select request type"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputRequestType").focus();
            }
        });
        return false;
    }

    if (Priority == '0') {
        $.alert.open({
            content: String.format("Please select priority"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputPriority").focus();
            }
        });

        return false;
    }
    return true;
}

$("#SubmitButton").click(function (evt) {
    if (AddTicketValidation() == false) {
        return false;
    }
    var File = $("#inputAttachment").val();
    if (File == "") {
        var result = 0;
        TicketInformationInsertTicket(result);
    }

    else {
        var data = new FormData();
        var fileUpload = $("#inputAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForTicket.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {

                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else {
                    TicketInformationInsertTicket(result);
                }

               
            },

            error: function (err) {
                alert('Error:' + err)
            }
        });
    }

    evt.preventDefault();
});


function ShowResponseFile(result) {

    $.alert.open({
        content: result, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}

function TicketInformationInsertTicket(result) {
    if (AddTicketValidation() == false) {
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Do you want to add ticket',
        icon: 'confirm',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var Title = $("#inputTitle").val();
            var ServiceID = $("#inputService").val();
            var RequestType = $("#inputRequestType").val();
            var Description = $.trim($("#inputDescription").val());
            var TeamAny = $("#inputAnyDeskID ").val();
            var FileIDs = result;
            var Priority = $("#inputPriority").val();
            var TicketGenerationDate = $("#TicketGenerationDate").val();

            var RemoteAccessType = $("#inputRemoteAccess").val();

            var jsonObj = {
                Title: Title,
                ServiceID: ServiceID,
                Description: Description,
                RequestType: RequestType,
                TeamAny: TeamAny,
                FileIDs: FileIDs,
                Priority: Priority,
                TicketGenerationDate: TicketGenerationDate,
                RemoteAccessType: RemoteAccessType
            };

            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AddTicketClient.aspx/Insert",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowResponse(data);

                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing worng"), icon: 'error', title: ClientName,
                    });
                }
            });
        }
    });
}

function Reset() {
    $("#inputTitle").val("");
    $("#inputService").val(0).trigger('chosen:updated');;
    $("#inputAnyDeskID").val("");
    $("#inputRequestType").val(0).trigger('chosen:updated');
    $("#inputDescription").val("");
    $("#inputAttachment").val("");
    $("#inputPriority").val("");
    $("#TicketGenerationDate").val("");

    $("#inputService").focus();
    $("#AttachmentTab").html("");
}

function SelectRemoteAccessOption() {
    $("#RemoteAccessGroup").show();

    var RemoteAccess = $("#inputRemoteAccess").val();

    if (RemoteAccess == "0") {
        $("#RemoteAccessGroup").hide();
        return;
    }
    if (RemoteAccess == "AnyDesk") {
        $("#LabelinputAnyDeskID").html("Any Desk ID");
        return;
    }
    if (RemoteAccess == "TeamViewer") {
        $("#LabelinputAnyDeskID").html("Team Viewer ID");
        return;
    }
    if (RemoteAccess == "UltraViewer") {
        $("#LabelinputAnyDeskID").html("Ultra Viewer ID");
        return;
    }


}

function TicketInformationSelectByTicketID(TicketID) {
    var jsonObj = {
        TicketID: TicketID,
    };

    var jData = JSON.stringify(jsonObj);

    $("#loader").show();

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AddTicketClient.aspx/SelectByID",
        data: jData,
        dataType: "json",
        success: function (data) {
            $("#loader").hide();
            BindDataForEdit(data);
        }
    });
}

function TicketInformationSelectAttachments(TicketID) {
    var jsonObj = {
        TicketID: TicketID,
    };

    var jData = JSON.stringify(jsonObj);

    $("#loader").show();

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AddTicketClient.aspx/SelectTicketAttachmentByID",
        data: jData,
        dataType: "json",
        success: function (data) {
            $("#loader").hide();
            BindAttachments(data);
        }
    });
}

function BindDataForEdit(data) {
    $("#inputTitle").val(data.d.Title);
    $('#inputService').val(data.d.ServiceID);
    $("#inputService").trigger("chosen:updated");
    $("#inputPriority").val(data.d.Priority);
    $("#inputRequestType").val(data.d.RequestType);
    $("#inputDescription").val(data.d.Description);
    $("#TicketID").html(data.d.TicketID);
    $("#SubmitButton").hide();
    $("#UpdateButton").show();
    $("#inputRequestType").trigger("chosen:updated");
    $("#inputPriority").trigger("chosen:updated");
    $("#inputRemoteAccess").trigger("chosen:updated");
    $("#imageTicket").attr("src", data.d.Attachment);
    $("#inputAttachment").val(data.d.OriginalFileName);
}

function BindAttachments(data) {



    $("#AttachmentTab").html("");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';
        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td>{0} </th>', (i + 1)));
        strRow.push(String.format("<td class='text-left'> <a href='{0}' target='_blank'>{1}</a> </td>", data.d[i].FilePath, data.d[i].FileName));
        strRow.push('</tr>');
        $("#AttachmentTab").append(strRow.join(""));
    }

}


$("#UpdateButton").click(function (evt) {
    if (AddTicketValidation() == false) {
        return false;
    }
    var File = $("#inputAttachment").val();
    if (File == "") {
        var result = 0;
        TicketInformationUpdateTicket(result);
    }

    else {
        var data = new FormData();
        var fileUpload = $("#inputAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForTicket.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {


                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else {
                    TicketInformationUpdateTicket(result);
                }

               
            },

            error: function (err) {
                alert('Error:' + err)
            }
        });
    }

    evt.preventDefault();
});

function TicketInformationUpdateTicket(result) {
    if (AddTicketValidation() == false) {
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Do you want to update ticket',
        icon: 'confirm',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var TicketID = $("#TicketID").text();
            var Title = $("#inputTitle").val();
            var ServiceID = $("#inputService").val();
            var RequestType = $("#inputRequestType").val();
            var Description = $.trim($("#inputDescription").val());
            var TeamAny = $("#inputAnyDeskID ").val();
            var FileIDs = result;
            var Priority = $("#inputPriority").val();
            var TicketGenerationDate = $("#TicketGenerationDate").val();

            var RemoteAccessType = $("#inputRemoteAccess").val();

            var jsonObj = {
                TicketID: TicketID,
                Title: Title,
                ServiceID: ServiceID,
                Description: Description,
                RequestType: RequestType,
                TeamAny: TeamAny,
                FileIDs: FileIDs,
                Priority: Priority,
                TicketGenerationDate: TicketGenerationDate,
                RemoteAccessType: RemoteAccessType
            };

            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AddTicketClient.aspx/Update",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowResponse(data);

                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing worng"), icon: 'error', title: ClientName,
                    });
                }
            });
        }
    });
}