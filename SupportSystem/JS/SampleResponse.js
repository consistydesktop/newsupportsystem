﻿function SampleResponseValidation() {
    var ProviderID = $("#txtProviderName").val();
    var TextMustExit = $("#txtTextMustExit").val();
    var Type = $("#ddlType").val();
    var Status = $("#ddlStatus").val();
    var Response = $("#txtResponse").val();





    if (ProviderID == '0' || ProviderID == "" || ProviderID == undefined) {
        $.alert.open({
            content: String.format("Please select provider "), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtProviderName").focus();
            }
        });
        return false;
    }



    if (TextMustExit == "") {
        $.alert.open({
            content: String.format("Please enter TextMustExit"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtTextMustExit").focus();
            }
        });
        return false;
    }
    if (Response == "") {
        $.alert.open({
            content: String.format("Please enter response"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtResponse").focus();
            }
        });
        return false;
    }

    if (Type == '0' || Type == "" || Type == undefined) {
        $.alert.open({
            content: String.format("Please select type"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#ddlType").focus();
            }
        });

        return false;
    }

    if (Status == '0' || Status == "" || Status == undefined) {
        $.alert.open({
            content: String.format("Please select status"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#ddlStatus").focus();
            }
        });

        return false;
    }
}

function SampleResponseInsert() {
    if (SampleResponseValidation() == false) {
        return false;
    }
    var ProviderID = $("#txtProviderName").val();
    var TextMustExit = $("#txtTextMustExit").val();
    var Type = $("#ddlType").val();
    var Status = $("#ddlStatus").val();
    var Response = $("#txtResponse").val();



    var jsonObj = {
        ProviderID: ProviderID,
        TextMustExit: TextMustExit,
        Type: Type,
        Status: Status,
        Response: Response,

    };

    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "SampleResponse.aspx/Insert",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {


            var msg = data.d.Message;

                     toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            Command: toastr["info"](msg, "Response")
            ResetAll(); Select();
            //ShowRespose(data);
       
        },
        error: function (result) {

        }
    });

    return false;
}

function Select() {
    var ProviderID = $("#txtProviderName").val();
    
    var jsonObj = {
        ProviderID: ProviderID,
    };
    var jData = JSON.stringify(jsonObj);
    $("#SampleResponseDetails").html("");
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/SampleResponse.aspx/SelectBYProviderID",
        data: jData,
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindData(data);
        },
        error: function (result) {

        }
    });

}

function BindData(data) {
    if (data.d.length > 0) {
        $("#SampleResponseDetails").html("");
        $("#SampleResponseDetails").html("<thead><tr><th>#</th><th>Name</th><th>Type</th><th>TextMustExit</th><th>Status</th><th>Response</th><th>Edit</th><th>Delete</th></tr></thead><tbody></tbody>");

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));
            strRow.push(String.format('<td> {0} </td>', data.d[i].DeveloperName));
            strRow.push(String.format('<td> {0} </td>', data.d[i].Type));
            strRow.push(String.format('<td> {0} </td>', data.d[i].TextMustExit));
            strRow.push(String.format('<td> {0} </td>', data.d[i].Status));
        //    strRow.push(String.format('<td> {0} </td>', data.d[i].Response));
            strRow.push(String.format("<td>  <textarea cols='70' rows='5' class='form-control'  style='font-size:14px;'  id='txtExpression{0}' value='{1}' maxlength=50000 >{1}</textarea></td>", i, data.d[i].Response));

            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return SampleResponseSelectBySampleResponseID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].SampleResponseID));
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return  SampleResponseDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].SampleResponseID));


            strRow.push('</tr>');
            $("#SampleResponseDetails").append(strRow.join(""));
        }
    }
}




function SampleResponseSelectBySampleResponseID(SampleResponseID) {
    location.href = "SampleResponse.aspx?SampleResponseID =" + SampleResponseID;
}
function SampleResponseSelectByID(SampleResponseID) {

    var jsonObj = {
        SampleResponseID: SampleResponseID,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/SampleResponse.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#txtProviderName").val(data.d.ProviderID).trigger('chosen:updated');
            $("#txtTextMustExit").val(data.d.TextMustExit);
            $("#txtResponse").val(data.d.Response);
            $("#ddlType").val(data.d.Type);
            $("#ddlStatus").val(data.d.Status);
            $("#txtSampleResponseID").html(data.d.SampleResponseID);
            $("#SubmitButton").css('display', 'none');
            $("#UpdateButton").show();
        },
        error: function (result) {
         
        }
    });
}

function SampleURLUpdate() {
    var SampleResponseID = $("#txtSampleResponseID").text();
    var ProviderID = $("#txtProviderName").val();
    var TextMustExit = $("#txtTextMustExit").val();
    var Type = $("#ddlType").val();
    var Status = $("#ddlStatus").val();
    var Response = $("#txtResponse").val();
    if (SampleResponseValidation() == false) {
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Update',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                SampleResponseID: SampleResponseID,
                ProviderID: ProviderID,
                TextMustExit: TextMustExit,
                Type: Type,
                Status: Status,
                Response: Response,
            };
            var jData = JSON.stringify(jsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "SampleResponse.aspx/Update",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data); ResetAll();
                }
            });
        },
        error: function (data) {

        }
    });
}

function SampleResponseDelete(SampleResponseID) {
    var SampleResponseID = SampleResponseID;

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                SampleResponseID: SampleResponseID,
            };

            var jData = JSON.stringify(jsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "SampleResponse.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data); Select();
                }
            });
        },
        error: function (data) {

        }
    });
}

function ResetAll() {
    $("#ddlType").val(0).trigger('chosen:updated');
    $("#ddlStatus").val(0).trigger('chosen:updated');
    $("#txtProviderName").val(0).trigger('chosen:updated');
    $("#txtTextMustExit").val("");
    $("#txtResponse").val("");
    $("#txtHeaderJSON").val("");
}

function ShowRespose(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Select();
            ResetAll();
        }
    });
}



function ProviderMasterSelect() {

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/APIProvider.aspx/Select",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataProviderMaster(data);
        },
        error: function (result) {
        }
    });
}


function BindDataProviderMaster(data) {
    $("#txtProviderName").html("");
    $("#txtProviderName").append("<option value=0>Select Provider </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].ProviderID, data.d[i].ProviderName));
        $("#txtProviderName").append(strRowDropdown);
    }

    $("#txtProviderName").trigger("chosen:updated");
    $("#txtProviderName").chosen({ width: "100%" });

}