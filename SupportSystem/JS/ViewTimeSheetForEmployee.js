﻿function UserInformationSelectEmployeeName() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTimeSheetForEmployee.aspx/SelectEmployeeName",
        data: {},
        dataType: "json",

        success: function (data) {
            $("#DeveloperNames").html("");
            $("#DeveloperNames").append("<option value='{0}'>Select Developer</option> ");
            for (var i = 0; i < data.d.length; i++) {
                var strRowDropdown = [];
                strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
                $("#DeveloperNames").append(strRowDropdown);
            }
            $("#DeveloperNames").chosen();
        }
    });
}

function TimeSheetMasterSelectAll() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTimeSheetForEmployee.aspx/Select",
        data: {},
        dataType: "json",

        success: function (data) {
            BindDataTimeSheetMaster(data);
        }
    });
}

function BindDataTimeSheetMaster(data) {
    if (data.d.length < 1) {
        $.alert.open({
            content: String.format("Record Not found"), icon: 'warning', title: ClientName,
        });
    }

    $("#TimeSheetDetails").html("");
    $("#TimeSheetDetails").html("<thead><tr><th>Sr.No</th><th>Employee Name</th><th>Date</th><th>Time (In Minutes)</th><th>Validate</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td class="Grcenter"> {0} </td>', (i + 1)));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Name));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Date));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Time));

        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return TimeSheetMasterDelete({0})'><i class='fa fa-check' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].TimeSheetMasterID));
        strRow.push('</tr>');
        $("#TimeSheetDetails").append(strRow.join(""));
    }

    $('#TimeSheetDetails').dataTable({
        "scrollX": true,
        "bDestroy": true,
        dom: 'Blfrtip',
        buttons: ['excelHtml5', 'pdfHtml5']
    });
}

function TimeSheetMasterDelete(TimeSheetMasterID) {
    var JsonObj = { TimeSheetMasterID: TimeSheetMasterID };

    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTimeSheetForEmployee.aspx/Delete",
        data: JData,
        dataType: "json",

        success: function (data) {
            ShowResponse(data);
            Reset();
        },

        error: function (data) {
            $.alert.open({
                content: 'Record not found', title: ClientName,
            });
        }
    });
}

function ShowRespose(data) {
    Icon = data.d.Icon;
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}

function Search() {
    var FromDate = "";
    var ToDate = "";
    var DeveloperName = "";

    FromDate = $("#FromDate").val();
    ToDate = $("#ToDate").val();

    DeveloperName = $('#DeveloperNames option:selected').html();

    if (FromDate == "") {
        $.alert.open({
            content: String.format("Please select From Date"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#FromDate").focus();
            }
        });
        return false;
    }

    if (ToDate == "") {
        $.alert.open({
            content: String.format("Please select To Date"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#ToDate").focus();
            }
        });
        return false;
    }
    var JsonObj =
    {
        FromDate: FromDate,
        ToDate: ToDate,
        DeveloperName: DeveloperName,
    };

    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTimeSheetForEmployee.aspx/Search",
        data: JData,
        dataType: "json",

        success: function (data) {
            BindDataTimeSheetMaster(data);
        },

        error: function (data) {
            $.alert.open({
                content: 'Data is not Found', title: 'Consisty',
            });
        }
    });
}