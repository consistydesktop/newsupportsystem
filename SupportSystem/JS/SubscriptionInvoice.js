﻿function UserInformationSelectCustomerName() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "SystemName.aspx/SelectCustomerName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            $("#inputClientName").html("");
            $("#inputClientName").append("<option value='0'>Select Customer </option>");

            for (var i = 0; i < data.d.length; i++) {
                var strRowDropdown = [];
                strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
                $("#inputClientName").append(strRowDropdown);
            }
            $("#inputClientName").trigger("chosen:updated");
            $("#inputClientName").chosen({ width: "100%" });

          
        },
        error: function (result) {

        }
    });
}

function SystemMasterSelect() {
    $("#inputSystemName").html("");
    var ID = $("#ID").text();
    var uID = $("#uid").text();

    var UserID = 0;
    if (ID == 1) {
        UserID = $("#inputClientName").val();
        if (UserID == null || UserID == undefined) {
            UserID = 1;
        }
    }
    else {
        UserID = uID;
    }
    var jsonObj = {
        UserID: UserID
    };
    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "SubscriptionInvoice.aspx/SelectSystemName",
        data: jData,
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataSystemMaster(data);
        },
        error: function (result) {
        }
    });
}

function BindDataSystemMaster(data) {
    $("#inputSystemName").html("");
    $("#inputSystemName").append("<option value=0>Select System </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#inputSystemName").append(strRowDropdown);
    }

    $("#inputSystemName").trigger("chosen:updated");
    $("#inputSystemName").chosen({ width: "100%" });

}





function SelectPurchaseReport() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();

    var SystemID = $("#inputSystemName").val();
    var ID = $("#ID").text();
    var uID = $("#uid").text();


    var UserID = 0;
    if (ID == 1) {
        var UserID = $("#inputClientName").val();
    }
    else {
        UserID = uID;
    }

    if (FromDate == "") {
        $.alert.open({
            content: String.format(pleaseEnter, "from date"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });

        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: String.format(pleaseEnter, "to date"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });

        return false;
    }

    if (UserID == "" || UserID == undefined || UserID == "0") {
        $.alert.open({
            content: String.format("Please select customer"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputClientName").focus();
            }
        });

        return false;
    }

    if (SystemID == "" || SystemID == undefined || SystemID == "0" ) {
        $.alert.open({
            content: String.format( "Please select  system"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });

        return false;
    }



    $('#Payment-POS').css('display', 'none');
    $("#loader").show();

    var jsonObj = {
        FromDate: FromDate,
        ToDate: ToDate,
        UserID: UserID,
        SystemID: SystemID

    };

    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "SubscriptionInvoice.aspx/selectPurchaseReport",
        data: jData,

        dataType: "json",
        success: function (data) {
            $("#loader").hide();


            var dateObj = new Date();
            var month = dateObj.getUTCMonth() + 1;
            var day = dateObj.getUTCDate();
            var year = dateObj.getUTCFullYear();

            newdate = year + "/" + month + "/" + day;


            $('#Payment-POS').css('display', '');
            $('#Payment-POS-btn').css('display', '');
            $("#lblDebit").text(inIndianCurruncy(data.d.Amount));
            $("#lblDebit1").text(inIndianCurruncy(data.d.Amount));
            $("#lblCredit").text(inIndianCurruncy(data.d.Amount));
            $("#lblCredit1").text(inIndianCurruncy(data.d.Amount));
            $("#lblSubTotal").text(inIndianCurruncy(data.d.Amount));
            $("#lblSubTotalOwner").text(inIndianCurruncy(data.d.Amount));
            $("#lblPurchaseDate").text(FromDate + " to " + ToDate);
            $("#lblVoucherNo").text("P" + "/" + UserID + "/" + newdate);
            $("#lblParty").text(data.d.Receiver);

            $("#lblAmountInWords").text(inWords(data.d.Amount).toUpperCase());

        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
}




function printDiv(divName) {
    Popup($('<div/>').append($(divName).clone()).html(), divName);
}

function Popup(data, divName) {
    var toPrint = document.getElementById(divName);
    var popupWin = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
    popupWin.document.open();
    popupWin.document.write('<html> <head><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> <style type="text/css"> p{ margin-bottom: 0; } .innertable{ margin-bottom: 0px; } .table > thead > tr > th{ vertical-align: middle; } table.tablelist tr td{ vertical-align: middle; } table.tablelist tr td h4{ margin:0px; font-weight: bold; font-size: 16px;} table.tablelist tr th h4{ margin:0px; font-weight: bold; font-size: 16px;} </style> </head> <body onload="window.print()">')
    popupWin.document.write($("#" + divName).html());
    console.log($("#" + divName).html());
    popupWin.document.write('</html>');
    popupWin.document.close();
}