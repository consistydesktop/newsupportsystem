﻿function TimeSheetMasterSelectByDate(TimesheetDate) {

    if (TimesheetDate == '')
        TimesheetDate = $("#inputDate").val();

    var jSonObj = {
        TimesheetDate: TimesheetDate,
    }

    var JData = JSON.stringify(jSonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "TimeSheet.aspx/SelectTimeSheetByDate",
        data: JData,
        dataType: "JSON",
        contentType: "application/json",
        success: function (data) {
            OpenTimeSheet(data);
        }
    });

}

function OpenTimeSheet(data) {
    var totalTime = 0;
    $("#TimeSheetDetails").html("");
    $("#TimeSheetDetails").html("<thead><tr><th>System Name</th><th>Task</th><th>Time(min)</th><th>Status</th><th>Action</th><th>Delete</th></tr></thead>");

    if (data != "") {
        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format("<tr>  <td> <select id='inputSystem_{0}' class='form-control loginput' value='{1}' >{1}</select></td>", data.d[i].TimeSheetDetailID));
            strRow.push(String.format("<td>  <textarea  class='form-control loginput'    id='inputTask_{0}' value='{1}' >{1}</textarea> </td>", data.d[i].TimeSheetDetailID, data.d[i].Task));
            strRow.push(String.format("<td>  <input type='text'  class='form-control loginput'  id='inputTime_{0}' value='{1}' style='width:80px' onkeypress='return isNumberKey(event)'  maxlength='3' /></td>", data.d[i].TimeSheetDetailID, data.d[i].Time));
            strRow.push(String.format("<td>  <select  class='form-control loginput'  id='inputStatus_{0}'><option value=1>Inprogress</option><option value=2>Completed</option></select></td>", data.d[i].TimeSheetDetailID));


            strRow.push(String.format("<td class='text-center'><button type='button' class='btn'  style='width:50%; background-color: white; border-color: #fff' title='Update' value='Update' onclick=\"TimesheetMasterUpdate({0})\"><i class='fa fa-refresh' style='font-size:20px;color:blue'></i></button>  </td>", data.d[i].TimeSheetDetailID));
            strRow.push(String.format("<td class='text-center'><button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick=\"TimesheetMasterDelete({0})\"><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button>  </td>", data.d[i].TimeSheetDetailID));


            strRow.push('</tr>');
            $("#TimeSheetDetails").append(strRow.join(""));


            $("#inputSystem_" + data.d[i].TimeSheetDetailID).html("");
            $("#inputSystem_" + data.d[i].TimeSheetDetailID).append("<option value=0>Select System</option>");
            for (var j = 0; j < SystemObjects.length; j++) {
                var strRowDropdown = [];
                strRowDropdown.push(String.format("<option value={0}> {1} </option> ", SystemObjects[j].SystemID, SystemObjects[j].SystemName));
                $("#inputSystem_" + data.d[i].TimeSheetDetailID).append(strRowDropdown);
            }
            $("#inputSystem_" + data.d[i].TimeSheetDetailID).chosen();

            $("#inputSystem_" + data.d[i].TimeSheetDetailID).val(data.d[i].SystemID).trigger('chosen:updated');

            $("#inputStatus_" + data.d[i].TimeSheetDetailID).val(data.d[i].Status).trigger('chosen:updated');

            $("#inputDate").val(data.d[i].Date);

            var time = data.d[i].Time;
            totalTime = totalTime + (time);


        }

    }

    var strRow = [];
    var color = 'black';

    strRow.push(String.format("<tr>  <td><select id='inputSystem' class='form-control loginput' value='{1}' >{1}</select></td>", i, ""));
    strRow.push(String.format("<td>  <textarea  class='form-control loginput'    id='inputTask' value='{1}'  ></textarea></td>", i, ""));
    strRow.push(String.format("<td>  <input type='text'  class='form-control loginput'  id='inputTime' value='{1}' onkeypress='return isNumberKey(event)'  maxlength='3' /></td>", i, ""));
    strRow.push(String.format("<td>  <select  class='form-control loginput'  id='inputStatus'><option value=1>Inprogress</option><option value=2>Completed</option></select></td>", i));
    //  strRow.push(String.format("<td>  <input type='button' class='btn btn-info'  id='AddButton' value='Add' onclick='TimesheetMasterInsert()' /> <input type='button' class='btn btn-danger'  id='DeleteButton{0}' value='Delete' onclick=\"TimesheetMasterDelete({0})\"  /></td>", i, ""));
    strRow.push(String.format("<td class='text-center'><button type='button' class='btn'  style='width:50%; background-color: white; border-color: #fff' title='Add' value='Add' onclick='TimesheetMasterInsert()'><i class='fa fa-plus' style='font-size:20px;color:blue'></i></button>  </td>", i));
    strRow.push(String.format("<td class='text-center'><button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick=\"TimesheetMasterDelete({0})\"><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button>  </td>", i));


    strRow.push('</tr>');
    var time = $("#inputTime").val();
    totalTime = totalTime + (time == undefined ? 0 : time);


    strRow.push(String.format('<tr><td></td><td></td><td><label id="AddLabel"><b> {0}:{1} Hours</b></label></td><td></td></tr>', Math.floor(totalTime / 60), (totalTime % 60)));

    $("#TimeSheetDetails").append(strRow.join(""));
    $("#inputSystem").html("");
    $("#inputSystem").append("<option value=0>Select System</option>");
    for (var j = 0; j < SystemObjects.length; j++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value={0}> {1} </option> ", SystemObjects[j].SystemID, SystemObjects[j].SystemName));
        $("#inputSystem").append(strRowDropdown);
    }
    $("#inputSystem").chosen();




}

function isNumberKey(e) {
    var r = e.which ? e.which : event.keyCode;
    return r > 31 && (48 > r || r > 57) && (e.which != 46 || $(this).val().indexOf('.') != -1) ? !1 : void 0
}
function AddRow() {
    if (count > -1) {
        var SystemID = $("#inputSystem_" + count).val();
        var Task = $("#inputTask" + count).val();
        var Time = $("#inputTime" + count).val();

        if (SystemID == 0) {
            $.alert.open({
                content: String.format("Please select system"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#inputSystem_" + count).focus();
                }
            });
            return;
        }

        if (Task == "") {
            $.alert.open({
                content: String.format("Please enter task"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#inputTask" + count).focus();
                }
            });
            return;
        }

        if (Time == "") {
            $.alert.open({
                content: String.format("Please enter time"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#inputTime" + count).focus();
                }
            });
            return;
        }

        count++;
        var strRow = [];
        var color = 'black';
        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td> <select id="inputSystem_{0}"  class="form-control loginput" ></select></td>', (count)));

        strRow.push(String.format('<td><input type="text" id="inputTask{0}" class="form-control loginput" /></td>', count/*, data.d[i].SubTaskID*/));
        strRow.push(String.format('<td><input type="text" id="inputTime{0}" onkeypress="return isNumberKey(event)" class="form-control loginput" />', count/* data.d[i].SubTaskID*/));
        strRow.push(String.format('<td><button type="button" id="AddButton{0}" class="btn btn-info" onclick="AddRow()" >Add</button> <button type="button" id="RemoveButton{0}" class="btn btn-danger " onclick="deleteRow(this,{0})" >Remove</button>', count  /* ,data.d[i].SubTaskID */));

        strRow.push('</tr>');
        $("#TimeSheetDetails").append(strRow.join(""));
        BindDataSystem("inputSystem_" + count, SystemObjects);

        $("#inputSystem_" + count).html("");
        $("#inputSystem_" + count).append("<option value=0>Select System</option> ");
        for (var i = 0; i < SystemObjects.length; i++) {
            var strRowDropdown = [];
            strRowDropdown.push(String.format("<option value={0}> {1} </option> ", SystemObjects[i].SystemID, SystemObjects[i].SystemName));
            $("#inputSystem_" + count).append(strRowDropdown);
        }
        $("#inputSystem_" + count).chosen();
    }
}

function deleteRow(r, c) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("TimeSheetDetails").deleteRow(i);
    List.splice(List.indexOf(c), 1);
    //count = count - 1;
}

function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "TimeSheet.aspx/SelectSystemName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataSystemMaster(data);
            OpenTimeSheet("");
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSystemMaster(data) {
    for (var i = 0; i < data.d.length; i++) {
        var SystemObject = {
            SystemID: data.d[i].SystemID,
            SystemName: data.d[i].SystemName
        }
        SystemObjects.push(SystemObject);
    }
}

function BindDataSystem(inputSystem, data) {
    $(inputSystem).html("");
    $(inputSystem).append("<option value=0>Select System </option>");
    for (var i = 0; i < data.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format('<option value="' + data[i].SystemID + '">' + data[i].SystemName + '</option>'));
        $(inputSystem).append(strRowDropdown);
    }
    $(inputSystem).chosen();
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
        }
    });
}

function TimesheetMasterUpdate(TimeSheetDetailID) {

    var SystemID = $("#inputSystem_" + TimeSheetDetailID).val();
    var Task = $("#inputTask_" + TimeSheetDetailID).val();
    var Time = $("#inputTime_" + TimeSheetDetailID).val();
    var Status = $("#inputStatus_" + TimeSheetDetailID).val();


    if (SystemID == "0" || SystemID == undefined) {
        $.alert.open({
            content: 'select system ', icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputSystem").focus();
            }
        });
        return false;

    }
    if (Task == "") {
        $.alert.open({
            content: 'enter task ', icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputTask").focus();
            }
        });
        return false;

    }
    if (Time == "") {
        $.alert.open({
            content: 'Enter time ', icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputTime").focus();
            }
        });
        return false;

    }


    var jSonObj = {
        TimeSheetDetailID: TimeSheetDetailID,
        SystemID: SystemID,
        Task: Task,
        Time: Time,
        Status: Status,
    }

    var JData = JSON.stringify(jSonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "TimeSheet.aspx/Update",
        data: JData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            ShowResponse(data);
            TimeSheetMasterSelectByDate('');
        }
    });

}

function TimesheetMasterDelete(TimeSheetDetailID) {




    var jSonObj = {
        TimeSheetDetailID: TimeSheetDetailID,
    }

    var JData = JSON.stringify(jSonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "TimeSheet.aspx/Delete",
        data: JData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            ShowResponse(data);
            TimeSheetMasterSelectByDate('');
        }
    });

}

function TimesheetMasterInsert() {
    var TimeSheetDate = $("#inputDate").val();
    var SystemID = $("#inputSystem").val();
    var Task = $("#inputTask").val();
    var Time = $("#inputTime").val();
    var Status = $("#inputStatus").val();


    if (TimeSheetDate == "" || TimeSheetDate == undefined) {
        $.alert.open({
            content: 'Select Date ', icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputDate").focus();
            }
        });
        return false;

    }
    if (SystemID == "0" || SystemID == undefined) {
        $.alert.open({
            content: 'Select system ', icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputSystem").focus();
            }
        });
        return false;

    }
    if (Task == "") {
        $.alert.open({
            content: 'enter task ', icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputTask").focus();
            }
        });
        return false;

    }
    if (Time == "") {
        $.alert.open({
            content: 'Enter time ', icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputTime").focus();
            }
        });
        return false;

    }




    var jSonObj = {
        Date: TimeSheetDate,
        SystemID: SystemID,
        Task: Task,
        Time: Time,
        Status: Status,
    }

    var JData = JSON.stringify(jSonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "TimeSheet.aspx/Insert",
        data: JData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            ShowResponse(data);
            TimeSheetMasterSelectByDate('');
        }
    });


}
