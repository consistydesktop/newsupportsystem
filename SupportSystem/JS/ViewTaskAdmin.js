﻿function UserInformationSelectEmployeeName() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTaskAdmin.aspx/SelectEmployeeName",
        data: {},
        dataType: "json",

        success: function (data) {
            BindEmployees(data);
        }
    });
}

function BindEmployees(data) {
    $("#inputEmployee").html("");
    $("#inputEmployee").append("<option value=0>Select Employee</option> ");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
        $("#inputEmployee").append(strRowDropdown);
    }
    $("#inputEmployee").chosen();
}

function TaskMasterSelect() {

    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTaskAdmin.aspx/Select",
        data: {},
        dataType: "json",
        success: function (data) {
            $("#loader").hide();
            BindTaskDetails(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function BindTaskDetails(data) {

    var mainData = [];
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';


        if ((data.d[i].TaskStatus) == 'INCOMPLETE'.toUpperCase()) {
            color = 'red';
        }
        if (data.d[i].TaskStatus == 'COMPLETE'.toUpperCase()) {
            color = 'green';
        }
        if (data.d[i].TaskStatus == 'ALL'.toUpperCase()) {
            color = 'black';
            color = 'red';
            color = 'green';
        }

        strRow.push(String.format('<td><span style="color:{1}"> {0}</span> </td>', (i + 1), color));
        strRow.push(String.format('<td><span style="color:{1}"> {0} </span></td>', data.d[i].SystemName, color));
        strRow.push(String.format('<td><span style="color:{1}"> {0} </span></td>', data.d[i].Task, color));
        strRow.push(String.format('<td><span style="color:{1}"> {0} </span></td>', data.d[i].EmployeeNames, color));
        strRow.push(String.format('<td><span style="color:{1}"> {0} </span></td>', data.d[i].Deadline, color));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return TaskMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TaskID));
       // strRow.push(String.format("<td><span  class='red' title='Delete'  onclick='return TaskMasterDelete({0})'  >Delete</span></td>", data.d[i].TaskID));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff'  onclick='return TaskMasterDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].TaskID));

        mainData.push(strRow);

    }

    $("#TaskDetails").html();
    $("#TaskDetails").html("<thead >  <tr> <th>#</th>  <th>System Name</th> <th>Task</th> <th>Employees</th><th>Deadline</th> <th>Edit</th> <th>Delete</th> </tr></thead><tbody></tbody>");
    $("#TaskDetails").append("<tfoot> <tr> <th></th>   <th></th>            <th></th>     <th></th>         <th></th>         <th></th>     <th></th>        </tr></tfoot>");
    createDataTable('TaskDetails', mainData, []);



    //for (var i = 0; i < data.d.length; i++) {
    //    var strRow = [];
    //    var color = 'black';


    //    if ((data.d[i].TaskStatus) == 'INCOMPLETE'.toUpperCase()) {
    //        color = 'red';
    //    }
    //    if (data.d[i].TaskStatus == 'COMPLETE'.toUpperCase()) {
    //        color = 'green';
    //    }
    //    strRow.push(String.format('<tr style="color:{0};">', color));

    //    strRow.push(String.format('<td> {0} </td>', (i + 1)));
    //    strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
    //    strRow.push(String.format('<td> {0} </td>', data.d[i].Task));
    //    strRow.push(String.format('<td> {0} </td>', data.d[i].EmployeeNames));
    //    strRow.push(String.format('<td> {0} </td>', data.d[i].Deadline));
    //    strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return TaskMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TaskID));
    //    strRow.push(String.format("<td><span  class='red' title='Delete'  onclick='return TaskMasterDelete({0})'  >Delete</span></td>", data.d[i].TaskID));
    //    strRow.push('</tr>');
    //    $("#TaskDetails").append(strRow.join(""));
    //}

    //addProperties(TaskDetails);
}

function TaskMasterSelectByID(TaskID) {
    location.href = "/Assigntask.aspx?TaskID=" + TaskID;
}

function TaskMasterSelectByDate() {

    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    var UserID = $("#inputEmployee").val();
    var Status = $("#inputStatus").val();

    if (FromDate == "" || FromDate == undefined) {
        $.alert.open({
            content: String.format("Please select from date"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }

    if (ToDate == "" || ToDate == undefined) {
        $.alert.open({
            content: String.format("Please select to date"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });
        return false;
    }

    var jsonObj = {
        FromDate: FromDate,
        ToDate: ToDate,
        UserID: UserID,
        Status: Status,
    };
    var jData = JSON.stringify(jsonObj)
  
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewTaskAdmin.aspx/Search",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BindTaskDetails(data);
        },
        error: function (result) {
            $.alert.open({
                content: String.format("No data available between given date"), icon: 'error', title: ClientName,
            });
        }
    });
    return false;


}


function TaskMasterDelete(TaskID) {



    var jsonObj = {
        TaskID: TaskID,
    };

    var jData = JSON.stringify(jsonObj)
    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to delete this task',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AssignTask.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    $.alert.open({ content: data.d, icon: 'error', title: ClientName, });
                    location.reload();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not updated'), icon: 'error', title: ClientName, });
        }
    });
}