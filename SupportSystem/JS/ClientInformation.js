﻿function UserInformationSelectForClient() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ClientInformation.aspx/SelectClient",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataClientInformation(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataClientInformation(data) {
    $("#ClientInformation").html("");
    $("#ClientInformation").html("<thead><tr><th>#</th><th>Name</th><th>Mobile No</th><th>GST</th><th>PAN</th><th>State</th><th>Edit</th><th>IsActive</th></thead><tbody></tbody>");




    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';
        var checked = 'checked';
        if (data.d[i].IsActive != 1) {
            color = "red";
            checked = '';
        }


        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td >{0}</td>', data.d[i].Name));
        //   strRow.push(String.format('<td>{0}</td>', data.d[i].Username));
        strRow.push(String.format('<td>{0}</td>', data.d[i].MobileNumber));
        strRow.push(String.format('<td> {0}</td>', data.d[i].GSTNo));
        strRow.push(String.format('<td>{0}</td>', data.d[i].PANNo));
        strRow.push(String.format('<td>{0}</td>', data.d[i].State));

        strRow.push(String.format("<td><span  class='green' title='View'  onclick='return UserInformationSelectClientByID({0})'  >View</span></td>", data.d[i].UserID));
        if (data.d[i].IsActive == 1) {

            strRow.push(String.format('<td> <input type="checkbox" class="form-control" name="{0}" id="checks{0}"  onchange=\'UserInformationUpdateForActive({0},{1},{2})\'  CHECKED /> </td>', i, data.d[i].UserID, checked));
        }
        else {
            strRow.push(String.format('<td> <input type="checkbox" class="form-control" name="{0}" id="checks{0}"  onchange=\'UserInformationUpdateForActive({0},{1},{2})\'   /> </td>', i, data.d[i].UserID, checked));
        }

        strRow.push('</tr>');
        $("#ClientInformation").append(strRow.join(""));
    }

    addProperties(ClientInformation);




}

function UserInformationUpdateForActive(i, UserID) {


    var IsActive = 0;

    if ($("#checks" + i).is(":checked")) {
        IsActive = 1;
    }


    var jsonObj = {
        UserID: UserID,
        IsActive: IsActive,

    };
    var jsonData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ClientInformation.aspx/UpdateAciveUser",
        data: jsonData,
        dataType: "json",
        success: function (data) {
            $.alert.open({
                content: data.d, icon: 'warning', title: ClientName,
            });
            UserInformationSelectForClient();
        },
        error: function (data) {
            $.alert.open({ content: String.format('not update'), icon: 'error', title: ClientName });
        }
    });


}


function UserInformationSelectClientByID(UserID) {
    
    var jsonObj = {
        UserID: UserID,
    };
    var jsonData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ClientInformation.aspx/UserInformationSelectClientByID",
        data: jsonData,
        dataType: "json",
        success: function (data) {
           
        },
        error: function (data) {
            $.alert.open({ content: String.format('not update'), icon: 'error', title: ClientName });
        }
    });

}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
    });
}