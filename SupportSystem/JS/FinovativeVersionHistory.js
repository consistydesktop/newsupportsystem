﻿function FinovativeVersionInsert() {
    var Title = $("#inputTitle").val();
    var Description = $("#inputDescription").val();
    var Version = $("#inputVersion").val();
    var Date = $('#inputDate').val();

    if (Version == "") {
        $.alert.open({
            content: "Please Enter Version", title: ClientName,
            callback: function () {
                $("#inputVersion").focus();
            }
        });
        return false;
    }

    if (Title == "") {
        $.alert.open({
            content: "Please Enter Title", title: ClientName,
            callback: function () {
                $("#inputTitle").focus();
            }
        });
        return false;
    }

    if (Description == "") {
        $.alert.open({
            content: "Please Enter Description", title: ClientName,
            callback: function () {
                $("#inputDescription").focus();
            }
        });
        return false;
    }

    if (Date == "") {
        $.alert.open({
            content: "Please select date", title: ClientName,
            callback: function () {
                $("#inputDate").focus();
            }
        });
        return false;
    }



    var JsonObj =
    {
        Title: Title,
        Description: Description,
        Version: Version,
        Date: Date

    };

    var JData = JSON.stringify(JsonObj);


    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/FinovativeVersionHistory.aspx/InsertFinovativeVersionHistory",
        data: JData,
        dataType: "json",
        success: function (data) {
            ShowResponse(data);
        },
        error: function (data) {

        }
    });
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            ResetAll(); Select();
        }
    });
}

function ResetAll() {
    $("#inputTitle").val('');
    $("#inputVersion").val('');
    $("#inputDescription").val('');
    $("#inputDate").val('');

}

function Select() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/FinovativeVersionHistory.aspx/Select",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindData(data);
        },
        error: function (result) {

        }
    });
}

function BindData(data) {
    if (data.d.length > 0) {
        $("#tblVersionHistory").html("");
        $("#tblVersionHistory").html("<thead><tr><th>#</th><th>Date</th><th>Version</th><th>Title</th><th>Edit</th><th>Delete</th><th>Description</th></tr></thead><tbody></tbody>");

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));
            strRow.push(String.format('<td> {0} </td>', data.d[i].DOC));
            strRow.push(String.format('<td> {0} </td>', data.d[i].Version));
            strRow.push(String.format('<td> {0} </td>', data.d[i].Title));

            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return VersionHistorySelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].FinnovativeVersionHistoryID));
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return  VersionHistoryDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].FinnovativeVersionHistoryID));

            strRow.push(String.format('<td> {0} </td>', data.d[i].Description));

            strRow.push('</tr>');
            $("#tblVersionHistory").append(strRow.join(""));
        }
    }
}

function VersionHistorySelectByID(FinnovativeVersionHistoryID) {

    var jsonObj = {
        FinnovativeVersionHistoryID: FinnovativeVersionHistoryID,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "/FinovativeVersionHistory.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {

            $("#inputVersion").val(data.d.Version);
            $("#inputTitle").val(data.d.Title);
            $("#inputDate").val(data.d.DOC);
            $("#inputDescription").val(data.d.Description);

            $("#txtFinovativeVersionHistoryID").html(data.d.FinnovativeVersionHistoryID);
            $("#SubmitButton").css('display', 'none');
            $("#UpdateButton").show();
        },
        error: function (result) {

        }
    });
}

function VersionHistoryUpdate() {
    var FinnovativeVersionHistoryID = $("#txtFinovativeVersionHistoryID").text();

    var Title = $("#inputTitle").val();
    var Description = $("#inputDescription").val();
    var Version = $("#inputVersion").val();
    var Date = $('#inputDate').val();

    if (Version == "") {
        $.alert.open({
            content: "Please Enter Version", title: ClientName,
            callback: function () {
                $("#inputVersion").focus();
            }
        });
        return false;
    }

    if (Title == "") {
        $.alert.open({
            content: "Please Enter Title", title: ClientName,
            callback: function () {
                $("#inputTitle").focus();
            }
        });
        return false;
    }

    if (Description == "") {
        $.alert.open({
            content: "Please Enter Description", title: ClientName,
            callback: function () {
                $("#inputDescription").focus();
            }
        });
        return false;
    }

    if (Date == "") {
        $.alert.open({
            content: "Please select date", title: ClientName,
            callback: function () {
                $("#inputDate").focus();
            }
        });
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Update',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                FinnovativeVersionHistoryID: FinnovativeVersionHistoryID,

                Title: Title,
                Description: Description,
                Version: Version,
                Date: Date

            };
            var jData = JSON.stringify(jsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "FinovativeVersionHistory.aspx/Update",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowResponse(data);
                }
            });
        },
        error: function (data) {

        }
    });
}

function VersionHistoryDelete(FinnovativeVersionHistoryID) {


    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                FinnovativeVersionHistoryID: FinnovativeVersionHistoryID,
            };

            var jData = JSON.stringify(jsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "FinovativeVersionHistory.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowResponse(data);
                }
            });
        },
        error: function (data) {

        }
    });
}