﻿function UserInformationSelectEmployeeName() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AssignTask.aspx/SelectEmployeeName",
        data: {},
        dataType: "json",

        success: function (data) {
            $("#inputEmployee").html("");
            $("#inputEmployee").append("<option value='0'>Select Employee</option> ");
            for (var i = 0; i < data.d.length; i++) {
                var strRowDropdown = [];
                strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
                $("#inputEmployee").append(strRowDropdown);
            }
            $("#inputEmployee").chosen();
        }
    });
}

function TaskMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AssignTask.aspx/Select",
        data: {},
        dataType: "JSON",
        success: function (data) {
            if (data.d.length > 0) {
                BindDataTaskMaster(data);
            }
        },
        error: function (result) {
        }
    });
}

function BindDataTaskMaster(data) {
    $("#TaskDetails").html("<thead><tr><th>#</th><th>Employee Name</th><th>Task</th><th>Deadline</th><th>Attachment</th><th>Edit</th><th>Delete</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        if (data.d[i].Status == "Completed")
            color = 'green';

        if (data.d[i].Status == 'incomplete') {
            color = 'red';
        }

        if (data.d[i].Status == 'required') {
            color = 'orange';
        }
        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td class="Grcenter"> {0} </td>', (i + 1)));

        strRow.push(String.format('<td> {0} </td>', data.d[i].Name));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Task));

        //strRow.push(String.format('<td> {0} </td>', data.d[i].Status));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Deadline));

        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='download' style='width:50%; background-color: white; border-color: #fff' onclick='return ViewAttachment({0})' ><i class='fa fa-download' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TaskID));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return TaskMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TaskID));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%  background-color: white; border-color: #fff' onclick='return TaskMasterDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].TaskID));
        strRow.push('</tr>');
        $("#TaskDetails").append(strRow.join(""));
    }
    $('#TaskDetails').dataTable({
        "scrollX": false
    });
}

function ViewDescription(Description) {
    $("#modal_Description").modal('show');
    $("#txtDescription").text(Description);
}

function AssignTaskValidation() {
    var DeveloperID = $("#inputEmployee").val();
    var Task = $("#inputTask").val();
    var Description = $("#inputDescription").val();

    if (DeveloperID == '0') {
        $.alert.open({
            content: String.format("Please select developer"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputEmployee").focus();
            }
        });
        return false;
    }

    if (Task == "") {
        $.alert.open({
            content: String.format("Please enter task"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputTask").focus();
            }
        });
        return false;
    }

    if (Description == "") {
        $.alert.open({
            content: String.format("Please write some description"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputDescription").focus();
            }
        });
        return false;
    }
}
$("#ViewTaskButton").click(function (evt) {
    if (UserID == 161) { location.href = '/ViewTaskAdmin.aspx' }
    else {
        location.href = '/ViewTaskEmployee.aspx'
    }
}


$("#SubmitButton").click(function (evt) {
    if (AssignTaskValidation() == false)
    { return false; }

    var File = $("#inputAttachment").val();

    if (File == "") {
        var result = 0;
        TaskMasterInsert(result);
    }

    else {
        var data = new FormData();
        var fileUpload = $("#inputAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForTask.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {


                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else {

                    TaskMasterInsert(result);
                }

            },

            error: function (err) {
                alert('Error:' + err)
            }
        });
    }
    evt.preventDefault();
});

function TaskMasterInsert(Result) {
    var UserID = $("#inputEmployee").val();
    var Task = $("#inputTask").val();
    var Description = $("#inputDescription").val();
    var FileIDs = Result;
    var Deadline = $("#inputDueDate").val();

    var JsonObj =
    {
        UserID: UserID,
        Task: Task,
        Description: Description,
        FileIDs: FileIDs,
        Deadline: Deadline,
    };

    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AssignTask.aspx/Insert",
        data: JData,
        dataType: "json",

        success: function (data) {
            ShowResponse(data);
        },

        error: function (data) {
        }
    });
    location.reload();
}

function TaskMasterSelectByID(TaskID) {
    var JsonObj = {
        TaskID: TaskID
    };
    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AssignTask.aspx/SelectByID",
        data: JData,
        dataType: "json",

        success: function (data) {
            $("#inputEmployee").val(data.d.UserID).trigger('chosen:updated');
            $("#inputTask").val(data.d.Task);
            $("#inputDescription").val(data.d.Description);
            $("#inputDueDate").val(data.d.Deadline);
            $("#TaskID").html(TaskID);
            $("#SubmitButton").css('display', 'none');
            $("#UpdateButton").show();
            $("#TaskStatus").html(data.d.Status);
            $("#inputTask").focus();
        },
        error: function (data) {
            $.alert.open({
                content: 'Record not found', title: ClientName,
            });
        }
    });
}

function TaskMasterDelete(TaskID) {
    $.alert.open({
        type: 'confirm',
        content: 'Do you want to Delete task',
        icon: 'confirm',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var JsonObj = {
                TaskID: TaskID
            };
            var JData = JSON.stringify(JsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AssignTask.aspx/Delete",
                data: JData,
                dataType: "json",

                success: function (data) {
                    ShowResponse(data);
                },

                error: function (data) {
                    alert("Data is not Deleted...");
                }
            });
            location.reload();
        }
    });
}

function ViewAttachment(TaskID) {
    var JsonObj = {
        TaskID: TaskID
    };
    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AssignTask.aspx/SelectFileByID",
        data: JData,
        dataType: "json",

        success: function (data) {
            BindDataAttachment(data);
            $("#modal_attachment").modal('show');
        },

        error: function (data) {
            $.alert.open({
                content: 'Attachment Not Found', title: 'Consisty',
            });
        }
    });
}

function BindDataAttachment(data) {
    $("#Attachments").html("");
    $("#Attachments").html("<thead><tr><th>Sr.No</th><th>Attachment</th></tr></thead><tbody></tbody>");

    var l = data.d.length;
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format("<td class='text-center'> <a href='{0}' target='_blank'>{1}</a> </td>", data.d[i].Attachment, data.d[i].OriginalFileName));

        strRow.push('</tr>');
        $("#Attachments").append(strRow.join(""));
    }

    $('#Attachments').dataTable({
        "bDestroy": true,
    });
}

$("#UpdateButton").click(function (evt) {
    if (AssignTaskValidation() == false)
    { return false; }

    var File = $("#inputAttachment").val();

    if (File == "") {
        var result = 0;
        TaskMasterUpdate(result.toString());
    }

    else {
        var data = new FormData();
        var fileUpload = $("#inputAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForTask.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {

                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else {

                    TaskMasterUpdate(result);
                }


            },

            error: function (err) {
                alert('Error:' + err)
            }
        });
    }
    evt.preventDefault();
});

function TaskMasterUpdate(Result) {
    var TaskID = $("#TaskID").text();
    var DeveloperID = $("#inputEmployee").val();
    var Task = $("#inputTask").val();
    var Description = $("#inputDescription").val();
    var FileIDs = Result;
    var Deadline = $("#inputDueDate").val();
    var Status = $("#TaskStatus").html();

    $.alert.open({
        type: 'confirm',
        content: 'Do you want to Update Task',
        icon: 'confirm',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var JsonObj =
             {
                 TaskID: TaskID,
                 DeveloperID: DeveloperID,
                 Task: Task,
                 Description: Description,
                 FileIDs: FileIDs,
                 Deadline: Deadline,
                 Status: Status,
             };

            var JData = JSON.stringify(JsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AssignTask.aspx/Update",
                data: JData,
                dataType: "json",

                success: function (data) {
                    ShowResponse(data);
                },

                error: function (data) {
                    ShowResponse(data);
                }
            });
            location.reload();
        }
    });
}

function ShowResponse(data) {
    Icon = data.d.Icon;
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
            TaskMasterSelect();
        }
    });
}

function Reset() {
    $("#inputEmployee").val("0").trigger('chosen:updated');
    $("#inputTask").val("");
    $("#inputDescription").val("");
    $("#inputDueDate").val("");
    $("#inputAttachment").val("");
    $("#TaskID").html("");
}


function ShowResponseFile(result) {

    $.alert.open({
        content: result, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}

