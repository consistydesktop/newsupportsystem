﻿function ProjectEmployeeMappingSelect(ProjectDetailMasterID) {
    var ProjectDetailMasterID = ProjectDetailMasterID;
    $("#ProjectDetailMasterID").html(ProjectDetailMasterID);
    var jsonObj = {
        ProjectDetailMasterID: ProjectDetailMasterID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewDetailsByProject.aspx/Select",
        data: jData,
        dataType: "JSON",
        success: function (data) {
            BindDataAssignedDevelopers(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataAssignedDevelopers(data) {
    $("#ProjectDetails").html("");
    $("#ProjectDetails").html("<thead><tr><th>Sr.No</th><th>Developer</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format('<td>{0}</td>', data.d[i].Name));
        //if (data.d[i].Attachment != "") {
        //    strRow.push(String.format("<td class='text-center'> <a href='{0}'><i class='fa fa-download' style='font-size:20px;color:blue'></a> </td>", data.d[i].Attachment));
        //         }

        strRow.push('</tr>');
        $("#ProjectDetails").append(strRow.join(""));
    }

    $('#ProjectDetails').dataTable({
        "scrollX": true,
        "bDestroy": true,
        dom: 'Blfrtip',
        buttons: ['excelHtml5', 'pdfHtml5']
    });
}

function DownloadAttachment() {
    var ProjectDetailMasterID = $("#ProjectDetailMasterID").text();
    var jsonObj = {
        ProjectDetailMasterID: ProjectDetailMasterID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewDetailsByProject.aspx/SelectByProjectDetailMasterID",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });
}