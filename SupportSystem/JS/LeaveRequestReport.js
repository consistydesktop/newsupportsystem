﻿function LeaveRequestSelect() {
    $("#loader").show();

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/LeaveRequestReport.aspx/Select",
        data: {},
        dataType: "JSON",
        success: function (data) {
            $("#loader").hide();
            BindLeaveRequest(data);
        },
        error: function (result) {
           
        }
    });
}

function BindLeaveRequest(data) {
    var mainData = [];
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];

        var color = 'black';
        var checked = 'checked';

        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', (i + 1), color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].Name, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].FromDateString, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].ToDateString, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].NoOfDays, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].Description, color));

        if (data.d[i].IsApprove == 1)
            strRow.push(String.format('<td> <input type="checkbox" name="{0}" id="checks{0}" onchange="LeaveRequestApprove({0},{1})"  {2} /> </td>', i, data.d[i].LeaveID, checked));
        else
            strRow.push(String.format('<td> <input type="checkbox" name="{0}" id="checks{0}" onchange="LeaveRequestApprove({0},{1})"  {2}/> </td>', i, data.d[i].LeaveID));

        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].ApprovedByName, color));
        mainData.push(strRow);
    }
    $("#LeaveRequestDetails").html("");
    $("#LeaveRequestDetails").html("<thead> <tr> <th>#</th>  <th>Name</th> <th>FromDate</th> <th>ToDate</th> <th>No Of Days</th> <th>Description</th> <th>IsApprove</th> <th>ApprovedBy</th> </tr> </thead>")
    $("#LeaveRequestDetails").append("<tfoot> <tr> <th></th>   <th></th>     <th></th>         <th></th>       <th></th>         <th></th>               <th></th>           </tr> </tfoot>")

    createDataTable('LeaveRequestDetails', mainData,[-1]);

}

function LeaveRequestSelectByDate() {

    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    if (FromDate == "") {
        $.alert.open({
            content: "Select From date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: "Select To date", title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });

        return false;
    }
    var JsonObj =
    {
        FromDate: FromDate,
        ToDate: ToDate,
    };
    var JData = JSON.stringify(JsonObj);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/LeaveRequestReport.aspx/SelectByDate",
        data: JData,
        dataType: "json",

        success: function (data) {
            BindLeaveRequest(data)
        },

        error: function (data) {
            ShowResponse(data.d);
        }
    });


}

function LeaveRequestApprove(i, LeaveID) {
    var IsApprove = 0;
    if ($("#checks" + i).is(":checked")) {
        IsApprove = "1";
    }

    var jsonObj = {
        LeaveID: LeaveID,
        IsApprove: IsApprove,
    };

    var jsonData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "LeaveRequestReport.aspx/LeaveRequestApprove",
        data: jsonData,
        dataType: "json",
        success: function (data) {
            ShowResponse(data);
           
        },
        error: function (data) {
            $.alert.open({
                content: String.format('not update'), icon: 'error', title: ClientName,

                callback: function () {
                    
                }

            });

        }

    });
    LeaveRequestSelect();
}


function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            location.reload();
        }
    });
}

