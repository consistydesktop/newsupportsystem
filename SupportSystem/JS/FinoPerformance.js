﻿


function BindData(data) {
    var mainData = [];
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];

        var color = 'black';

        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', (i + 1), color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].DOC, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].ClientName, color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].ServiceName, color));
        strRow.push(String.format('<td > <span class="text-right d-block"> {1}</span> </td>', i, data.d[i].SuccessAmount));
        strRow.push(String.format('<td > <span class="text-right d-block"> {1}</span> </td>', i, inIndianCurruncy(data.d[i].SuccessCount)));
        strRow.push(String.format('<td > <span class="text-right d-block"> {1}</span> </td>', i, data.d[i].FailAmount));
        strRow.push(String.format('<td > <span class="text-right d-block"> {1}</span> </td>', i, inIndianCurruncy(data.d[i].FailCount)));
        mainData.push(strRow);
    }
    $("#FinoPerformanceDetails").html("<thead> <tr> <th>#</th>  <th>DOC</th> <th>Client</th> <th>Service</th> <th>Success Amt</th>  <th>Success Cnt</th> <th>Fail Amt</th>  <th>Fail Cnt</th></tr> </thead>")
    $("#FinoPerformanceDetails").append("<tfoot> <tr> <th></th>   <th></th>     <th></th>         <th></th>       <th></th>          <th></th>          <th></th>           <th></th>                </tr> </tfoot>")
    createDataTablesum('FinoPerformanceDetails', mainData, [4, 5, 6, 7]);

}

function FinoPerformanceSelect() {

    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    var ClientName = $("#inputClientName").val();
    var SalesType = $("#ddlSalesType").val();
    
    if (FromDate == "") {
        $.alert.open({
            content: "Select From date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: "Select To date", title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });

        return false;
    }
    var JsonObj =
    {
        FromDate: FromDate,
        ToDate: ToDate,
        ClientName: ClientName,
        SalesType: SalesType
    };
    var JData = JSON.stringify(JsonObj);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/FinoPerformance.aspx/Select",
        data: JData,
        dataType: "json",

        success: function (data) {
            BindData(data)
        },

        error: function (data) {

        }
    });


}


function ClientSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "FinoPerformance.aspx/ClientSelect",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            $("#inputClientName").html("");
            $("#inputClientName").append("<option value='All'>Select Customer </option>");

            for (var i = 0; i < data.d.length; i++) {
                var strRowDropdown = [];
                strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].ClientName, data.d[i].ClientName));
                $("#inputClientName").append(strRowDropdown);
            }
            $("#inputClientName").trigger("chosen:updated");
            $("#inputClientName").chosen({ width: "100%" });


        },
        error: function (result) {

        }
    });
}



function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            location.reload();
        }
    });
}
