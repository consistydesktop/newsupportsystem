﻿function UserInformationSelectForAll() {
    var UserTypeID = document.getElementById("inputUser").value;

    if (UserTypeID == "0") {
        $.alert.open({
            content: String.format("Please Select User"), icon: 'warning', title: ClientName,
            callback: function () {
                location.reload();
            }
        });
        return false;
    }


    var JsonObj =
   {
       UserTypeID: UserTypeID,
   };

    var JData = JSON.stringify(JsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "UserInformation.aspx/Select",
        data: JData,
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindData(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}
function BindData(data) {
    $("#inputName").css('display', 'block');
    $("#inputName").empty();
    $("#inputName").multiselect("destroy");

    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].MobileNumber + '-' + data.d[i].Name));
        $("#inputName").append(strRowDropdown);
    }
    $('#inputName').multiselect({
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true
    });
    $("#inputName").multiselect('selectAll', false);
    $('#inputName').multiselect('updateButtonText');
    $("#inputName").trigger("multiselect:updated");


}
function CheckUserSelected() {
    var Downline = $("#ddlmsgUsers").val();

    if (Downline == "0") {
        $('#inputName').empty();
        $('#inputName').trigger("chosen:updated");
        $('#inputName').append('<option value="0">--AllUsers--</option>');
        $('#inputName').trigger("chosen:updated");
    } else if (Downline != 0) {
        $("#inputName option[value='0']").remove();
        $('#inputName').trigger("chosen:updated");
    } else if (Downline == "") {
        getDownline();
    }
}


function SendMessage() {
    var Message = $("#inputMessage").val().trim();
    var selected = $("#inputName option:selected");
    var Usertype = $("#inputUser option:selected").text();

    var UserID = "";
    selected.each(function () {
        UserID += "," + $(this).val();
    });

    if (UserID == "") {

        $.alert.open({
            content: String.format(pleaseSelect, "user"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputName").focus();
            }
        });
        return false;
    }

    if (Message == '') {
        $.alert.open({
            content: String.format(pleaseEnter, "some text"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputMessage").focus();
            }
        });
        return false;
    }

    var jsonObj = {
        UserID: UserID,
        Message: Message,
        Usertype: Usertype,
    };

    var jData = JSON.stringify(jsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "SendMessage.aspx/InsertMessage",
        data: jData,
        dataType: "json",
        success: function (data) {
           
           
            $.alert.open({
                content: data.d.Message, icon: 'confirm', title: ClientName,
                callback: function () {
                    location.reload();
                }
            });
        },
        error: function (error) {
           
        }
    });

}