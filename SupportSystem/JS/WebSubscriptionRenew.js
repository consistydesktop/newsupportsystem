﻿function SystemMasterSelect() {
    $("#inputSystemName").html("");
    var ID = $("#ID").text();
    var uID = $("#uid").text();

    var UserID = 0;
    if (ID == 1) {
        UserID = $("#inputClientName").val();
        if (UserID == null || UserID == undefined) {
            UserID = 1;
        }
    }
    else {
        UserID = uID;
    }
    var jsonObj = {
        UserID: UserID
    };
    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "SubscriptionInvoice.aspx/SelectSystemName",
        data: jData,
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataSystemMaster(data);
        },
        error: function (result) {
        }
    });
}


function BindDataSystemMaster(data) {
    $("#inputSystemName").html("");
    $("#inputSystemName").append("<option value=0>Select System </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#inputSystemName").append(strRowDropdown);
    }

    $("#inputSystemName").trigger("chosen:updated");
    $("#inputSystemName").chosen({ width: "100%" });

}



function SelectSubscriptionDetails() {
    var SystemID = $("#inputSystemName").val();

    if (SystemID == 0 || SystemID == null || SystemID == undefined) {
        $.alert.open({
            content: String.format("Please select system "), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });

        return false;
    }


    var jsonObj = {
        SystemID: SystemID
    };
    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "WebSubscriptionRenew.aspx/SelectSubscriptionDetails",
        data: jData,
        dataType: "JSON",
        async: false,
        success: function (data) {

            if (data.d != "OK") {
                ShowRespose(data.d);
            }
            else { window.location.href = "SubscritpionRenovation.aspx";}


        },
        error: function (result) {
        }
    });
}


function ShowRespose(Message) {

    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
    });
}
