﻿

function SubscriptionHistory() {
    $(".loader").fadeIn('slow');
    $("#tblSubscriptionDetails").html("");
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    var Status = $("#ddlStatus").val();
    if (Status == undefined) {
        Status = 'All';

    }

    if (FromDate == "") {
        $.alert.open({
            content: String.format(pleaseEnter, "from date"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtFromDate").focus();
            }
        });

        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: String.format(pleaseEnter, "to date"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtToDate").focus();
            }
        });

        return false;
    }

    var jsonObj = {
        FromDate: FromDate,
        ToDate: ToDate,
        Status: Status,

    };
    var jData = JSON.stringify(jsonObj)

    $("#tblSubscriptionDetails").html("");
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "SubscriptionRenovationReport.aspx/PGSelectDatewise",
        data: jData,
        dataType: "json",
        success: function (data) {

            bindData(data);
        },
        error: function (result) {

        }
    });

}

function bindData(data) {
    $(".loader").fadeIn('slow');
    var ID = $("#ID").text();

    if (ID == 1) {
        var mainData = [];

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];

            var color = '';
            if (data.d[i].status.toUpperCase() == 'FAILURE'.toUpperCase()) {
                color = 'red';
            }
            if (data.d[i].status.toUpperCase() == 'SUCCESS'.toUpperCase()) {
                color = 'green';
            }
            if (data.d[i].status.toUpperCase() == 'PENDING'.toUpperCase()) {
               color = 'orange'; 
              
            }

            strRow.push(String.format('<td>{0}</td>', (i + 1)));
            strRow.push(String.format('<td class="GrLeft" > {0} </td>', data.d[i].DisplayDate));
            strRow.push(String.format('<td class="GrLeft"  > {0} </td>', data.d[i].UserName));
            strRow.push(String.format('<td class="GrLeft"  > {0} </td>', data.d[i].SystemName));

            strRow.push(String.format('<td class="text-right"> {1} </td>', i, data.d[i].Amount.toFixed(2)));
            strRow.push(String.format('<td class="text-center"><span class="{0}">{1}</span> </td>', color, data.d[i].status.toUpperCase()));
            if (data.d[i].status.toUpperCase() == "PENDING".toUpperCase()) {
                strRow.push(String.format("<td> <button type='button'  onclick =\"StatusCheck('{0}')\" class='btn btn-small btn-success datatablebtn'>Status Check</button> </td>", data.d[i].OrderId));
            }
            else {
                strRow.push(String.format("<td>-</td>"));
            }

            strRow.push(String.format('<td class="text-left" > {0} </td>', data.d[i].PayMode));
            strRow.push(String.format('<td class="GrLeft"  > {0} </td>', data.d[i].OrderId));
            strRow.push(String.format('<td class="GrLeft"  > {0} </td>', data.d[i].TransactionID));



           

            strRow.push(String.format('<td <span class="messageResponse"> {0} </span> </td>', data.d[i].response));


            mainData.push(strRow);
        }
        $(".loader").fadeOut('slow');
        $("#tblSubscriptionDetails").html("<thead><tr><th>#</th><th>Date</th><th>Name</th>  <th> System</th><th>Amount</th><th>Status</th><th>Check</th><th>Mode</th><th>OrderID</th><th>TransactionID</th><th>Response</th></thead><tbody></tbody>");
        $("#tblSubscriptionDetails").append('<tfoot><tr><th></th><th></th><th></th><th></th><th class="text-right"></th><th></th><th></th><th></th><th></th><th></th><th></th></tr></tfoot>');
        createDataTable('tblSubscriptionDetails', mainData, [4]);
    }
    if (ID == 3) {
        var mainData = [];

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];

            var color = '';
            if (data.d[i].status.toUpperCase() == 'FAILURE'.toUpperCase()) {
                color = 'red';
            }
            if (data.d[i].status.toUpperCase() == 'SUCCESS'.toUpperCase()) {
                color = 'green';
            }
            if (data.d[i].status.toUpperCase() == 'PENDING'.toUpperCase()) {
                color = 'orange';
            }

            strRow.push(String.format('<td>{0}</td>', (i + 1)));
            strRow.push(String.format('<td class="GrLeft" > {0} </td>', data.d[i].DisplayDate));
            strRow.push(String.format('<td class="GrLeft"  > {0} </td>', data.d[i].SystemName));
            strRow.push(String.format('<td class="text-right"> {1} </td>', i, data.d[i].Amount.toFixed(2)));
            strRow.push(String.format('<td class="text-center"><span class="{0}">{1}</span> </td>', color, data.d[i].status.toUpperCase()));
            if (data.d[i].status.toUpperCase() == "PENDING".toUpperCase()) {
                strRow.push(String.format("<td> <button type='button'  onclick =\"StatusCheck('{0}')\" class='btn btn-small btn-success datatablebtn'>Status Check</button> </td>", data.d[i].OrderId));
            }
            else {
                strRow.push(String.format("<td>-</td>"));
            }

            strRow.push(String.format('<td class="text-left" > {0} </td>', data.d[i].PayMode));
            strRow.push(String.format('<td class="GrLeft"  > {0} </td>', data.d[i].OrderId));
           
            strRow.push(String.format('<td class="GrLeft"  > {0} </td>', data.d[i].TransactionID));


        
            mainData.push(strRow);
        }
        $(".loader").fadeOut('slow');
        $("#tblSubscriptionDetails").html("<thead><tr><th>#</th><th>Date</th>  <th> System</th><th>Amount</th><th>Status</th><th> Check</th><th>Mode</th><th>OrderID</th><th>TransactionID</th></thead><tbody></tbody>");
        $("#tblSubscriptionDetails").append('<tfoot><tr><th></th><th></th><th></th><th></th><th class="text-right"></th><th class="text-right"></th><th></th><th></th></tr></tfoot>');
        createDataTable('tblSubscriptionDetails', mainData, [3]);
    }
}

function StatusCheck(OrderID) {

    var jsonObj = {
        OrderID: OrderID
    };
    var jsonData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "SubscriptionRenovationReport.aspx/StatusCheck",
        data: jsonData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            if (data.d != "") {
                $.alert.open({
                    content: String.format(data.d), icon: 'warning', title: ClientName,
                    callback: function () {
                        SubscriptionHistory();
                    }
                });
            }
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
    $(".loader").fadeOut("slow");
}