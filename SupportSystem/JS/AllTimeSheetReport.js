﻿function UserInformationSelectEmployeeName() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AllTimeSheetReport.aspx/SelectEmployeeName",
        data: {},
        dataType: "json",

        success: function (data) {
            BindEmployees(data);
        }
    });
}

function BindEmployees(data) {
    $("#inputEmployee").html("");
    $("#inputEmployee").append("<option value=0>Select Employee</option> ");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
        $("#inputEmployee").append(strRowDropdown);
    }
    $("#inputEmployee").chosen();
}

function TimeSheetMasterSelectForAllReport() {
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AllTimeSheetReport.aspx/Select",
        data: {},
        dataType: "json",
        success: function (data) {
            $("#loader").hide();
            BindDataTimeSheetMaster(data);
        }

    });

}

function BindDataTimeSheetMaster(data) {
    $("#TimeSheetDetails").html("<thead><tr><th>Sr.No</th><th>Name</th><th>Date</th><th>Total Time</th><th>View</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        var TotalTime = Math.floor(data.d[i].TotalTime / 60);
        if (TotalTime < 8)
            color = 'red';

        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td class="Grcenter"> {0} </td>', (i + 1)));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Name));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Date));
        strRow.push(String.format('<td><b> {0}:{1} Hours</b></td>', Math.floor(data.d[i].TotalTime / 60), (data.d[i].TotalTime % 60)));
        strRow.push(String.format("<td class='text-center'> <span  class='green' title='View'  onclick='return TimeSheetMasterSelectByIDForReport(\"{0}\",\"{1}\")' data-toggle='modal' data-target='#exampleModalCenter'>View</span> </td>", (data.d[i].Date), (data.d[i].UserID)));

        strRow.push('</tr>');
        $("#TimeSheetDetails").append(strRow.join(""))
    }
    addProperties(TimeSheetDetails);
}


function TimeSheetMasterSelectByIDForPreviousDayReport() {
    var UserID = $("#UserID").text();
    var Date = $("#LabelTimeSheetPreviousDate").text();
    TimeSheetMasterSelectByIDForReport(Date, UserID);
}

function TimeSheetMasterSelectByIDForNextDayReport() {
    var UserID = $("#UserID").text();
    var Date = $("#LabelTimeSheetNextDate").text();
    TimeSheetMasterSelectByIDForReport(Date, UserID);
}

function TimeSheetMasterSelectByIDForReport(Date, UserID) {
   
    $("#UserID").html(UserID);
    var JsonObj =
   {
       Date:Date,
       UserID: UserID,
   };
    var JData = JSON.stringify(JsonObj);
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AllTimeSheetReport.aspx/SelectTimeSheetReportByEmployeeName",
        data: JData,
        dataType: "json",

        success: function (data) {
            $("#loader").hide();
            BindDataTimeSheetMasterReport(data);
            $("#modal_TimeSheetReport").modal('show');
        }
    });

}

function BindDataTimeSheetMasterReport(data) {
    var totalTime = 0;
    $("#TimeSheetReport").html("<thead><tr><th>System Name</th><th>Task</th><th> Time(In minutes)</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'RED';
        var Status = '';
        color = (data.d[i].Status == 1) ? 'RED' : 'GREEN';

        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Task));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Time));
        strRow.push('</tr>');

        var Time = data.d[i].Time;
        totalTime = totalTime + Time;
        $("#TimeSheetReport").append(strRow.join(""));

    }

    if (data.d.length > 0) {
        addProperties(TimeSheetReport);


        var day = new Date(data.d[0].Date);

        var nextDay = new Date();
        nextDay.setDate(day.getDate() + 1);

        $("#LabelTimeSheetNextDate").html(nextDay);

        var previousDay = new Date();

        previousDay.setDate(day.getDate() - 1);
        $("#LabelTimeSheetPreviousDate").html(previousDay);

        $("#LabelTimeSheetDate").html(data.d[0].Date + "'");
        $("#LabelTimeSheetEmployeeName").html(data.d[0].Name);
        $("#LabelTotalTime").html((String.format('{0}:{1} Hours', Math.floor(totalTime / 60), (totalTime % 60))));

    }

}

function DownloadReport() {
}

function Validation() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();

    if (FromDate == "") {
        $.alert.open({
            content: "Select From date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: "Select To date", title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });
        return false;
    }
    return true;
}

function Search() {
    if (!Validation())
        return false;

    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    var UserID = $("#inputEmployee").val();
  
    var JsonObj =
    {
        UserID: UserID,
        FromDate: FromDate,
        ToDate: ToDate,
    };

    var JData = JSON.stringify(JsonObj);
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AllTimeSheetReport.aspx/Search",
        data: JData,
        dataType: "json",
        success: function (data) {
            $("#loader").hide();
            if (data.d.length < 1) {
                $.alert.open({
                    content: "Record Not found", icon: 'error', title: ClientName,
                    callback: function () {
                    }
                });
            }
            else {
                BindDataTimeSheetMaster(data);
            }
        },
        error: function (data) {
        }
    });
}




