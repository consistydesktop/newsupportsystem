﻿function TicketInformationSelectResolved(SystemID) {
    var SystemID = SystemID;

    var jsonObj = {
        SystemID: SystemID
    };

    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardResolvedStatus.aspx/Select",
        data: jData,
        dataType: "JSON",
        success: function (data) {
            BindDataResolvedService(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function TicketInformationSelectResolvedForEmployee(ID) {
    var jsonObj = {
        SystemID: ID
    };

    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardResolvedStatus.aspx/SelectForEmployee",
        data: jData,
        dataType: "JSON",
        success: function (data) {
            BindDataResolvedService(data);
        },
       // error: function (result) {
       //     $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
       // }
    });
}
function BindDataResolvedService(data) {
    $("#ResolvedServiceDetails").html("");
    $("#ResolvedServiceDetails").html("<thead><tr><th>Sr.No</th><th>System</th><th>Service</th><th>Resolved Tickets</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format('<td> <a href="#" onclick="ViewTicketsAdmin({0})" >{1}</a> </td>', data.d[i].TicketID, data.d[i].SystemName));
        strRow.push(String.format('<td>{0}</td>', data.d[i].ServiceName));
        strRow.push(String.format('<td> {0} </td>', data.d[i].ResolvedTickets));

        strRow.push('</tr>');
        $("#ResolvedServiceDetails").append(strRow.join(""));
    }

    $('#ResolvedServiceDetails').dataTable({
        "scrollX": true,
        "bDestroy": true,
        dom: 'Blfrtip',
        buttons: ['excelHtml5', 'pdfHtml5']
    });
}

function ViewTicketsAdmin(TicketID) {
    location.href = "ViewTicketsAdmin.aspx?TicketID=" + TicketID;
}