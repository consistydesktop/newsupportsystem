﻿function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "KnowledgeBase.aspx/SelectSystemName",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataSystemMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSystemMaster(data) {
    $("#inputSystemName").html("");
    $("#inputSystemName").append("<option  value='0'>Select System </option>");

    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#inputSystemName").append(strRowDropdown);
    }
    $("#inputSystemName").chosen();
}

function KnowledgeBaseInsert() {
    var Issue = $("#inputIssue").val();
    var Resolution = $("#inputResolution").val();
    var SystemID = $("#inputSystemName").val();
   
    if (Issue == "") {
        $.alert.open({
            content: "Please Enter Issue ", title: ClientName,
            callback: function () {
                $("#inputIssue").focus();
            }
        });
        return false;
    }
    if (Resolution == "") {
        $.alert.open({
            content: "Please Enter resolution", title: ClientName,
            callback: function () {
                $("#inputResolution").focus();
            }
        });
        return false;
    }
    if (SystemID == 0)
    {
        $.alert.open({
            content: "Select System Name", title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });
        return false;
    }

    
    var JsonObj =
    {
        Issue: Issue,
        Resolution: Resolution,
        SystemID: SystemID
    };

    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/KnowledgeBase.aspx/Insert",
        data: JData,
        dataType: "json",
        success: function (data) {
            ShowResponse(data);
        },
        error: function (data) {
            ShowResponse(data);
        }
    });
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            location.reload();
        }
    });
}

function KnowledgeBaseSelectByID(KnowledgeBaseID) {
    var jsonObj = {
        KnowledgeBaseID: KnowledgeBaseID,
      
    };

    var jData = JSON.stringify(jsonObj);

    $("#loader").show();

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "KnowledgeBase.aspx/SelectByID",
        data: jData,
        dataType: "json",
        success: function (data) {

            $("#loader").hide();
            BindKnowledgeBaseForEdit(data);
           
        }
    });
}



function BindKnowledgeBaseForEdit(data) {

    $("#inputSystemName").val(data.d[0][0].SystemID).trigger("chosen:updated");
    $("#inputIssue").val(data.d[0][0].Issue);
    $("#inputResolution").val(data.d[0][0].Resolution);



}

function KnowledgeBaseUpdate() {

    var SystemID = $("#inputSystemName").val();
    var Issue = $("#inputIssue").val();
    var Resolution = $("#inputResolution").val();
    var KnowledgeBaseID = $("#hdnKnowledgeBaseID").val();
   
    var jSonObj = {
        KnowledgeBaseID: KnowledgeBaseID,
        Issue: Issue,
        Resolution: Resolution,
        SystemID: SystemID
    };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "KnowledgeBase.aspx/UpdateByID",
        data: JSON.stringify(jSonObj),
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            ShowResponse(data);
            ResetAll();
            location.href = '/KnowledgeBase.aspx';
            location.reload();
        }
    });
}

function CheckLength(startfrom,charend)
{
    var len = document.getElementById(startfrom);
    document.getElementById(charend).innerHTML = len.textLength;
    if (len.textLength >= 3900)
    {
        alert("maximun characters rechead");
    }
    else
    {
        return true;
    }
}
function KnowledgeBaseResetAll() {
    $("#inputIssue").val();
    $("#inputIssue").val('');
    $("#inputResolution").val('');
   

}


