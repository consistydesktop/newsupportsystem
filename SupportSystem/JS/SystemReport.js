﻿function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AssignTask.aspx/SelectSystemName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataSystemMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSystemMaster(data) {
    $("#inputSystemName").html("");
    $("#inputSystemName").append("<option  value='0'>Select System </option>");

    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#inputSystemName").append(strRowDropdown);
    }
    $("#inputSystemName").chosen();
}

function Search() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    var SystemID = $("#inputSystemName").val();


    if (FromDate == "") {
        $.alert.open({
            content: "Please select from date", icon: 'error', title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }

    if (ToDate == "") {
        $.alert.open({
            content: "Please select to date", icon: 'error', title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });
        return false;
    }

    if (SystemID == 0) {
        $.alert.open({
            content: "Please select system", icon: 'error', title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });
        return false;
    }
    var JsonObj =
    {
        FromDate: FromDate,
        ToDate: ToDate,
        SystemID: SystemID,
    };

    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "SystemReport.aspx/Search",
        data: JData,
        dataType: "json",
        success: function (data) {
            if (data.d.length < 1) {
                $.alert.open({
                    content: "Record Not found", icon: 'error', title: ClientName,
                    callback: function () {
                    }
                });
            }
            else {
                BindDataAllReport(data);

            }
        },
        error: function (data) {
        }
    });
}

function BindDataAllReport(data) {
    BindDataTimeSheetReport(data.d[0]);
    BindDataTaskReport(data.d[1]);
    BindDataTicketReport(data.d[2]);
}

function BindDataTimeSheetReport(data) {
    var totalTime = 0;
    $("#TimeSheetReport").html("<thead><tr><th>#</th><th>Date</th><th>Name</th><th>Task</th><th> Time(In min)</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.length; i++) {
        var strRow = [];
        var color = 'black';

        var Status = '';
        Status = (data[i].Status == 1) ? 'InProgress' : 'Completed';

        if (Status == "Completed")
            color = 'green';
        if (Status == "InProgress")
            color = 'red';


        strRow.push(String.format("<tr style='color:{0};'>", color));

        strRow.push(String.format('<td> {0} </td>', i + 1));
        strRow.push(String.format('<td> {0} </td>', data[i].Date));
        strRow.push(String.format('<td> {0} </td>', data[i].Name));
        strRow.push(String.format('<td> {0} </td>', data[i].Task));

        strRow.push(String.format('<td> {0} </td>', data[i].Time));


        strRow.push('</tr>');

        var Time = data[i].Time;
        totalTime = totalTime + Time;
        $("#TimeSheetReport").append(strRow.join(""));

    }
    var strRow = [];
    strRow.push(String.format('<tr><td></td><th></th><td></td><td></td><td><b><time>{0}:{1}</time> Hours </b></td><td></td></tr>', Math.floor(totalTime / 60), (totalTime % 60)));
    $("#TimeSheetReport").append(strRow.join(""));
    addProperties(TimeSheetReport);


}

function BindDataTaskReport(data) {
    $("#TaskReport").html("<thead><tr><th>Task</th><th>Deadline</th><th>Employees</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.length; i++) {
        var strRow = [];
        var color = 'black';
        var Status = getStatus(data[i].TaskStatus);

        if (data[i].TaskStatus.toUpperCase() == "COMPLETED".toUpperCase())
            color = 'green';
        if (data[i].TaskStatus.toUpperCase() == "NEW".toUpperCase())
            color = 'red';


        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td> {0} </td>', data[i].Task));
        strRow.push(String.format('<td> {0} </td>', data[i].Deadline));
       
        strRow.push(String.format('<td> {0} </td>', data[i].EmployeeNames));

        strRow.push('</tr>');
        $("#TaskReport").append(strRow.join(""));

    }

   
    addProperties(TaskReport);
}

function BindDataTicketReport(data) {
    $("#TicketReport").html("<thead><tr><th>Generated Date</th><th>Ticket No</th><th>Assigned Date</th><th>Status</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.length; i++) {
        var strRow = [];
        var color = 'black';
        //  var Status = getStatus(data[i].TicketStatus);

        if (data[i].TicketStatus == "Completed")
            color = 'green';
        if (data[i].TicketStatus == "New")
            color = 'red';


        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td> {0} </td>', data[i].TicketGeneratedDate));
        strRow.push(String.format('<td> {0} </td>', data[i].TicketNo));
        strRow.push(String.format('<td> {0} </td>', data[i].AssignedDate));
        strRow.push(String.format('<td> {0} </td>', data[i].TicketStatus));


        strRow.push('</tr>');
        $("#TicketReport").append(strRow.join(""));

    }

    $("#TicketReport").append(strRow.join(""));
    addProperties(TaskReport);
}

function getStatus(data) {
    if (data == 1)
        return "New";
    if (data == 2)
        return "InProgress";
    if (data == 3)
        return "Resolved";
    if (data == 4)
        return "Completed";

}