﻿function EmailValidation(EmailID) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(EmailID)) {
        return false;
    } else {
        return true;
    }
}
function EmployeeRegistrationValidation() {
    var FirstName = $("#inputFirstName").val();
    var LastName = $("#inputLastName").val();
    var Username = $("#inputUserName").val();
    var EmailID = $("#inputEmailID").val();
    var MobileNumber = $("#inputMobileNumber").val();
    var DateOfJoining = $("#inputDateOfJoining").val();
    var EmployeeMentStatus = $("inputEmployeeMentStatus")

    if (FirstName == "") {
        $.alert.open({
            content: String.format("Please enter employee firstname"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputFirstName").focus();
            }
        });
        return false;
    }
    if (LastName == "") {
        $.alert.open({
            content: String.format("Please enter employee lastname"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputLastName").focus();
            }
        });
        return false;
    }
    if (Username == "") {
        $.alert.open({
            content: String.format("Please enter user name"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputUserName").focus();
            }
        });
        return false;
    }
    if (EmailID == "") {
        $.alert.open({
            content: String.format("Please enter a email address"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputEmailID").focus();
            }
        });
        return false;
    }
    if (EmailID != "") {
        if (!EmailValidation(EmailID)) {
            $.alert.open({
                content: "Please enter a valid email address", icon: 'warning', title: ClientName,
                callback: function () {
                    $("#inputEmailID").focus();
                }
            });
            return false;
        }
    }
    if (DateOfJoining == "") {
        $.alert.open({
            content: String.format("Please select date of joining"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputDateOfJoining").focus();
            }
        });
        return false;
    }
    if (EmployeeMentStatus == "") {
        $.alert.open({
            content: String.format("Please select status"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputEmployeeMentStatus").focus();
            }
        });
        return false;
    }

    if (MobileNumber == "") {
        $.alert.open({
            content: String.format("Please enter mobile number"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputMobileNumber").focus();
            }
        });
        return false;
    }
    if (MobileNumber.length != 10) {
        $.alert.open({
            content: String.format("Please enter valid mobile number"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputMobileNumber").focus();
            }
        });
        return false;
    }

    return true;
}

function ShowRespose(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            UserInformationSelect();
        }
    });
}

function BindData(data) {
    $("#UserInformationDetails").html("");
    $("#UserInformationDetails").html("<thead><tr><th>UserID</th><th>Name</th><th>Username</th><th>EmailID</th><th>Mobile Number</th><th>Edit</th><th>Delete</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td> {0} </td>', data.d[i].Name));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Username));
        strRow.push(String.format('<td> {0} </td>', data.d[i].EmailID));
        strRow.push(String.format('<td> {0} </td>', data.d[i].MobileNumber));

        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return UserInformationSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].UserID));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return UserInformationDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].UserID));
        strRow.push('</tr>');
        $("#UserInformationDetails").append(strRow.join(""));
    }

    $('#UserInformationDetails').dataTable({
        "scrollX": true,
        "bDestroy": true,
        dom: 'Blfrtip',
        buttons: ['excelHtml5', 'pdfHtml5']
    });
}

function UserInformationInsert() {
    var FirstName = $("#inputFirstName").val();
    var LastName = $("#inputLastName").val();
    var Username = $("#inputUserName").val();
    var EmailID = $("#inputEmailID").val();
    var MobileNumber = $("#inputMobileNumber").val();
    var DateOfJoining = $("#inputDateOfJoining").val();
    var EmployeeMentStatus = $("#inputEmployeeMentStatus").val();

    if (EmployeeRegistrationValidation() == false) {
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Are you want to register new employee',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'Yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                FirstName: FirstName,
                LastName: LastName,
                Username: Username,
                EmailID: EmailID,
                MobileNumber: MobileNumber,
                DateOfJoining: DateOfJoining,
                EmployeeMentStatus: EmployeeMentStatus,
            };
            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "EmployeeRegistration.aspx/Insert",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowRespose(data);
                    location.reload();
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing worng"), icon: 'error', title: ClientName,
                    });
                }
            });
        }
    });
}

function UserInformationSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "EmployeeRegistration.aspx/Select",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindData(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function UserInformationDelete(ID) {
    var ID = ID;
    var jsonObj = {
        UserID: ID,
    };

    var jData = JSON.stringify(jsonObj)
    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "EmployeeRegistration.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                    location.reload();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not updated'), icon: 'error', title: ClientName, });
        }
    });
}

function UserInformationSelectByID(ID) {
    var ID = ID;
    $("#inputFirstName").val("Please Wait...");
    $("#inputLastName").val("Please Wait...");
    $("#inputUserName").val("Please Wait...");
    $("#inputEmailID").val("Please Wait...");
    $("#inputMobileNumber").val("Please Wait...");
    $("#inputDateOfJoining").val("Please Wait...");
    $("#inputEmployeeMentStatus").val("Please Wait...");
    $("#inputDateOfReliving").val("Please Wait...")


    $("#UserID").html("");
    var jsonObj = {
        UserID: ID,
    };

    var jData = JSON.stringify(jsonObj);

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "EmployeeRegistration.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#inputFirstName").val(data.d.FirstName);
            $("#inputLastName").val(data.d.LastName);
            $("#inputUserName").val(data.d.Username);
            $("#inputEmailID").val(data.d.EmailID);
            $("#inputMobileNumber").val(data.d.MobileNumber);
            $("#userID").html(ID);
            $("#inputDateOfJoining").val(data.d.DateOfJoiningInString);
            $("#inputEmployeeMentStatus").val(data.d.EmployeeMentStatus);
            $("#inputDateOfReliving").val(data.d.DateOfRelivingInString);
            $("#SubmitButton").css('display', 'none');
            $("#UpdateButton").show();
        },
        error: function (result) {
            $.alert.open({ content: "Not updated", icon: 'error', title: ClientName, });
        }
    });
}

function UserInformationUpdate() {
    var FirstName = $("#inputFirstName").val();
    var LastName = $("#inputLastName").val();
    var Username = $("#inputUserName").val();
    var EmailID = $("#inputEmailID").val();
    var MobileNumber = $("#inputMobileNumber").val();
    var UserID = $("#userID").html();
    var DateOfReliving = $("#inputDateOfReliving").val();
    var DateOfJoining = $("#inputDateOfJoining").val();
    var EmployeeMentStatus = $("#inputEmployeeMentStatus").val();
    var PANNo = $("#inputPANNo").val();
    var GSTNo = $("#inputGSTNo").val();
    var State = $("#inputState").val();


    if (EmployeeRegistrationValidation() == false) {
        return false;
    }
    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to update',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                FirstName: FirstName,
                LastName: LastName,
                Username: Username,
                EmailID: EmailID,
                MobileNumber: MobileNumber,
                UserID: UserID,
                DateOfReliving: DateOfReliving,
                EmployeeMentStatus: EmployeeMentStatus,
                PANNo: PANNo,
                GSTNo: GSTNo,
                State: State,
                DateOfJoining: DateOfJoining,

            };
            var jData = JSON.stringify(jsonObj)
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "EmployeeRegistration.aspx/Update",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                    // location.href = '/EmployeeRegistration.aspx';
                    window.location.href.indexOf("EmpRegID");
                    var url = window.location.href;
                    var id = url.split('=');
                    UserInformationSelectByID(id[1]);
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not updated'), icon: 'error', title: ClientName, });
        }
    });
}

function ResetAll() {
    $("#inputFirstName").val("");
    $("#inputLastName").val("");
    $("#inputUserName").val("");
    $("#inputEmailID").val("");
    $("#inputMobileNumber").val("");
    $("#userID").html("");
    $("#inputFirstName").focus();
    $("#inputLastName").focus();
}