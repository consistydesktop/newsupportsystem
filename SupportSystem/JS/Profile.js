﻿function UserInformationSelectForProfile() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Profile.aspx/SelectProfile",
        data: {},
        dataType: "json",

        success: function (data) {
            BindProfileDetails(data)

        }
    });
}

function BindProfileDetails(data) {
    $("#UserID").html(data.d.UserID);
    $("#inputName").val(data.d.Name);
    $("#inputUserName").val(data.d.UserName);
    $("#inputOldEmail").val(data.d.EmailID);
}

function UserInformationUpdateEmailID() {
    var UserID = $("#UserID").html();
    var OldEmailID = $("#inputOldEmail").val();
    var NewEmailID= $("#inputNewEmail").val();

    var jSonObj = {
        UserID:UserID,
        OldEmailID: OldEmailID,
        NewEmailID: NewEmailID,
    };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Profile.aspx/UpdateEmail",
        data: JSON.stringify(jSonObj),
        dataType: "json",

        success: function (data) {
            ShowResponse(data);

        }
    });
}

function UserInformationUpdatePassword() {
    var UserID = $("#UserID").html();
    var OldPassword = $("#inputOldPassword").val();
    var NewPassword = $("#inputNewPassword").val();
    var ConfirmPassword = $("#inputConfirmPassword").val();

    if (NewPassword != ConfirmPassword) {
        $.alert.open({
            content: 'password does not matched', icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputConfirmPassword").focus();
            }
        });
        return;
    }

    var jSonObj = {
        UserID: UserID,
        OldPassword: OldPassword,
        NewPassword: NewPassword,
    };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Profile.aspx/UpdatePassword",
        data: JSON.stringify(jSonObj),
        dataType: "json",

        success: function (data) {
            ShowResponse(data);

        }
    });



}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
            UserInformationSelectForProfile();
        }
    });
}

function Reset() {
    $("#UserID").text("");
    $("#inputName").val("");
    $("#inputUserName").val("");
    $("#inputOldEmail").val("");
    $("#inputNewEmail").val("");
    $("#inputOldPassword").val();
    $("#inputNewPassword").val();
    $("#inputConfirmPassword").val();
}