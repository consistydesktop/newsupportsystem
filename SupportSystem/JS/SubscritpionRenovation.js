﻿
function SelectCost() {
    var DaysType = $("#DaysType").val();

    var jsonObj = {
        DaysType: DaysType,
    };
    var jData = JSON.stringify(jsonObj)


    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "SubscritpionRenovation.aspx/SelectCost",
        data: jData,
        dataType: "json",
        success: function (data) {
            $("#lblCost").text(data.d.Amount);
            $("#lblAmountInWords").text("");
            var AmtInWords = capitalizeFirstLetter(inWords(data.d.Amount));
            $("#lblAmountInWords").text(AmtInWords);
        }
    });


}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function SelectSlab() {
    var PayMode = $("#ddlPayMode").val();

    var jsonObj = {
        PayMode: PayMode,
    };
    var jData = JSON.stringify(jsonObj)


    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "SubscritpionRenovation.aspx/SelectSlab",
        data: jData,
        dataType: "json",
        success: function (data) {
            var amount = $("#lblCost").text();
            var charge = data.d.Charge;
            var TotalAmount = charge + amount;
        }
    });


}

function CardTypeSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "SubscritpionRenovation.aspx/CardTypeSelect",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataCardType(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataCardType(data) {
    $("#ddlPayMode").html("");
    $("#ddlPayMode").append("<option value='0'>Select Paymode </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].CardTypeID, data.d[i].CardType));
        $("#ddlPayMode").append(strRowDropdown);
    }
    $("#ddlPayMode").chosen();
    $("#ddlPayMode").focus();
}


