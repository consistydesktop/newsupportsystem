﻿function KnowledgeBaseSelect()
{
    $("#loader").show();
    $.ajax({
        type:"POST",
        contentType: "application/json; charset=utf-8",
        url: "/KnowledgeBaseReport.aspx/Select",
        data: {},
        dataType: "JSON",
        success: function (data) {
            $("#loader").hide();
            BindKnowledgeBase(data);
        },
        error: function (result) {

        }
    });
}

function BindKnowledgeBase(data)
{
    var mainData = [];
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];

        var color = 'black';
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', (i + 1), color));

        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].SystemName, color));
        strRow.push(String.format('<td> <span style="color:{1}"><textarea rows="4" cols="50"> {0}</textarea> </span>  </td>', data.d[i].Issue, color));

        strRow.push(String.format('<td> <span style="color:{1}"><textarea rows="4" cols="50" maxlength="3900"> {0}</textarea> </span>  </td>', data.d[i].Resolution, color));

        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span> </td>', data.d[i].Name, color));
       // strRow.push(String.format('<td><button type="button" id="UpdateButton{0}" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="KnowledgeBaseSelectByID({0})" >Edit</button>', data.d[i].KnowledgeBaseID));
        strRow.push(String.format("<td class='text-center'> <button type='button' id='UpdateButton{0}' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return KnowledgeBaseSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].KnowledgeBaseID));

        mainData.push(strRow);
    }

       

        $("#KnowledgeBaseDetails").html("");
        $("#KnowledgeBaseDetails").html("<thead> <tr> <th>#</th> <th>System</th> <th>Issue</th> <th>Resolution</th>  <th>Name</th>     <th>Action</th></thead>")
        $("#KnowledgeBaseDetails").append("<tfoot> <tr> <th></th>   <th></th>       <th></th>         <th></th>      <th></th>              <th></th>        </tr> </tfoot>")
        createDataTable('KnowledgeBaseDetails', mainData, []);
   
}
function KnowledgeBaseSelectByDate() {

    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    if (FromDate == "") {
        $.alert.open({
            content: "Select From date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: "Select To date", title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });

        return false;
    }
    var JsonObj =
    {
        FromDate: FromDate,
        ToDate: ToDate,
    };
    var JData = JSON.stringify(JsonObj);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/KnowledgeBaseReport.aspx/SelectByDate",
        data: JData,
        dataType: "json",

        success: function (data) {
            BindKnowledgeBase(data);
        },

        error: function (data) {
            ShowResponse(data.d);
        }
    });


}
function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            location.reload();
        }
    });
}

function KnowledgeBaseReportUpdateByID(data)
{
    var KnowledgeBaseID = $("#KnowledgeBaseID").html();
    var Issue = $("#inputIssue").html();
    var Resolution = $("#inputResolution").html();

    var jSonObj = {
        KnowledgeBaseID: KnowledgeBaseID
        
        
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "KnowledgeBaseReport.aspx/UpdateByID",
        data: JSON.stringify(jSonObj),
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            ShowResponse(data);
            ResetAll();
            location.href = '/KnowledgeBase.aspx';
        }
    });
}


function KnowledgeBaseSelectByID(KnowledgeBaseID) {
    console.log(KnowledgeBaseID);
    location.href = "/KnowledgeBase.aspx?KnowledgeBaseID=" + KnowledgeBaseID;
}




