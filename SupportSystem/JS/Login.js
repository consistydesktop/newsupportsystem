﻿var ClientName = 'Consisty';

var OTPCount = 0;

function LoginValidation() {
    var MobileNumber = $("#inputUserName").val();
    var Password = $("#inputPassword").val();
    var Captcha = $("#inputCaptcha").val();

    if (MobileNumber == "" && Password == "" && Captcha == "") {
        $("#lblUsername").show();
        $("#lblPassword").show();
        $("#lblCaptcha").show();
        $("#inputUserName").focus();
        return false;
    }
    if (MobileNumber == "") {
        $.alert.open({
            content: String.format("Please enter a mobile number"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputUserName").focus();
            }
        });

        return false;
    }
    if (MobileNumber.length != 10) {
        $.alert.open({
            content: String.format("Please enter valid mobile number"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputUserName").focus();
            }
        });
        return false;
    }
    if (Password == "") {
        $.alert.open({
            content: String.format("Please enter  password"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputPassword").focus();
            }
        });

        return false;
    }
    if (Captcha == "") {
        $.alert.open({
            content: String.format("Please enter  captcha"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputCaptcha").focus();
            }
        });

        return false;
    }
}

function SignIn() {
    var MobileNumber = $("#inputUserName").val();
    var Password = $("#inputPassword").val();
    var Captcha = $("#inputCaptcha").val();

    if (LoginValidation() == false) {
        return false;
    }

    var jsonObj = {
        MobileNumber: MobileNumber,
        Password: Password,
        Captcha: Captcha,
    };

    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Login.aspx/LoginValidation",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            if (data.d.Icon == "Error") {
                ShowResponse(data);
                reloadCaptcha();
            }
            else {
                if (data.d.Message == "1")
                    window.location.href = "Dashboard.aspx";

                if (data.d.Message == "2")
                    window.location.href = "DashboardForEmployee.aspx";

                if (data.d.Message == "3")
                    window.location.href = "DashboardForCustomer.aspx";

                if (data.d.Message == "4")
                    window.location.href = "Dashboard.aspx";
            }
        },
        error: function (result) {
            $.alert.open({ content: String.format("Something worng"), icon: 'error', title: ClientName });
        }
    });
}

function Reset() {
    $("#inputUserName").val("");
    $("#inputUserName").focus();
    $("#inputPassword").val("");
    $("#inputCaptcha").val("");
    $("#txtotpnew").val("");
    document.getElementById('captcha').src = document.getElementById('captcha').src + '?' + new Date();
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function (data) {
            if (Message != "Please enter a valid captcha")
                Reset();
            else {
                $("#inputCaptcha").val("");
                $("#inputCaptcha").focus();
            }
        }
    });
}

function logout() {
    $.alert.open({
        type: 'warning',
        content: 'Are you sure to logout',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }
            if (button.toUpperCase() == 'ok'.toUpperCase()) {
                window.location.href = "Logout.aspx";
            }
        }
    });
}

function Redirect() {
    window.location.href = "CustomerRegistration.aspx";
}