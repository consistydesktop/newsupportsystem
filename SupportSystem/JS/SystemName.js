﻿function UserInformationSelectCustomerName() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "SystemName.aspx/SelectCustomerName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            $("#inputClientName").html("");
            $("#inputClientName").append("<option value='0'>Select Customer </option>");

            for (var i = 0; i < data.d.length; i++) {
                var strRowDropdown = [];
                strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
                $("#inputClientName").append(strRowDropdown);
            }
            $("#inputClientName").chosen();
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            ResetAll();
            SystemMasterSelect();
        }
    });
}

function SystemNameValidation() {
    var SystemName = $("#inputSystemName").val();
    var UserID = $("#inputClientName").val();
    var URL = $("#inputURL").val();
    var HostingIP = $('#inputHostingIP').val();
    if (UserID == 0) {
        $.alert.open({
            content: String.format("Please select customer"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputClientName").focus();
            }
        });
        return false;
    }
    if (SystemName == "") {
        $.alert.open({
            content: String.format("Please enter system  name"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });

        return false;
    }
    if (URL == "") {
        $.alert.open({
            content: String.format("Please enter URL"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputURL").focus();
            }
        });

        return false;
    }

    if (HostingIP == "") {
        $.alert.open({
            content: String.format("Please enter IP"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputHostingIP").focus();
            }
        });

        return false;
    }

}

function SystemMasterInsert() {
    var SystemName = $("#inputSystemName").val();
    var UserID = $("#inputClientName").val();
    var ServiceID = $("#inputService").val();
    var ProductID = $("#inputProductType").val();
    var URL = $("#inputURL").val();
    var HostingIP = $('#inputHostingIP').val();
    if (SystemNameValidation() == false) {
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Are you want to assign system name to customer',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }
            if (button.toUpperCase() != 'Yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                SystemName: SystemName,
                UserID: UserID,
                ServiceID: ServiceID,
                ProductID: ProductID,
                URL: URL,
                HostingIP: HostingIP,
            };

            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "SystemName.aspx/Insert",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
               
                    ShowRespnose(data); SystemMasterSelect();
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing worng"), icon: 'error', title: ClientName,
                    });
                }
            });
        }
    });
    return false;
}

function ResetAll() {
    $("#inputClientName").val(0).trigger('chosen:updated');
    $("#inputSystemName").val("");
    $("#inputService").val(0).trigger('chosen:updated');
    $("#inputProductType").val(0).trigger('chosen:updated');
    $("#inputURL").val("");
}

function BindDataSystemMaster(data) {
    var mainData = [];
   
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

       
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td> {0} </td>', data.d[i].Name));
        strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
        strRow.push(String.format('<td> {0} </td>', data.d[i].ProductName));

        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return SystemMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].SystemID));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return SystemMasterDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].SystemID));
     
        mainData.push(strRow);
       
    }
    $("#SystemMasterDetails").html("");
    $("#SystemMasterDetails").html("<thead><tr>   <th>Sr.No</th>  <th>Client</th>  <th>System</th>  <th>Product</th> <th>Edit</th> <th>Delete</th> </tr></thead><tbody></tbody>");
    $("#SystemMasterDetails").append("<tfoot> <tr>   <th></th>       <th></th>        <th></th>       <th></th>       <th></th>      <th></th>          </tr> </tfoot>")
    createDataTable('SystemMasterDetails', mainData, []);
   
}

function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "SystemName.aspx/Select",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindDataSystemMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not Found", icon: 'error', title: ClientName, });
        }
    });
}

function ServiceMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "SystemName.aspx/SelectService",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindDataServiceMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not Found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataServiceMaster(data) {
    $("#inputService").html("");
    $("#inputService").append("<option value=0>Select Service</option> ");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value={0}> {1} </option> ", data.d[i].ServiceID, data.d[i].ServiceName));
        $("#inputService").append(strRowDropdown);
    }
    $("#ddlProduct").chosen();
}

function ProductMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "SystemName.aspx/SelectProduct",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindDataProductMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not Found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataProductMaster(data) {
    $("#inputProductType").html("");
    $("#inputProductType").append("<option value=0>Select Product</option> ");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value={0}> {1} </option> ", data.d[i].ProductID, data.d[i].ProductName));
        $("#inputProductType").append(strRowDropdown);
    }
    $("#inputProductType").chosen();
}

function SystemMasterDelete(SystemID) {
    var SystemID = SystemID;

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                SystemID: SystemID,
            };

            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "SystemName.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowResponse(data);
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('not updated'), icon: 'error', title: ClientName, });
        }
    });
}

function SystemMasterSelectByID(SystemID) {
    var SystemID = SystemID;

    $("#inputClientName ").val("Please Wait...").trigger('chosen:updated');
    $("#inputSystemName").val("Please Wait...");
    $("#inputService").val("Please Wait...").trigger('chosen:updated');
    $("#inputProductType").val("Please Wait...").trigger('chosen:updated');
    $("#inputURL").val("Please Wait...");

    var jsonObj = {
        SystemID: SystemID,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "SystemName.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#inputClientName ").val(data.d.UserID).trigger('chosen:updated');
            $("#inputSystemName").val(data.d.SystemName);
            $("#SystemID").html(SystemID);
            $("#inputService").val(data.d.ServiceID).trigger('chosen:updated');
            $("#inputProductType").val(data.d.ProductID).trigger('chosen:updated');
            $("#inputURL").val(data.d.URL);
            $("#SubmitButton").css('display', 'none');
            $("#UpdateButton").show();
            $("#inputClientName ").focus();
        },
        error: function (result) {
            $.alert.open({ content: "not Updated", icon: 'error', title: ClientName, });
        }
    });
}

function SystemMasterUpdate() {
    var UserID = $("#inputClientName").val();
    var SystemName = $("#inputSystemName").val();
    var SystemID = $("#SystemID").text();
    var ServiceID = $("#inputService").val();
    var ProductID = $("#inputProductType").val();
    var URL = $("#inputURL").val();

    if (SystemNameValidation() == false) {
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Update',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                UserID: UserID,
                SystemID: SystemID,
                SystemName: SystemName,
                ServiceID: ServiceID,
                ProductID: ProductID,
                URL: URL,
            };
            var jData = JSON.stringify(jsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "SystemName.aspx/Update",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowResponse(data);
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not inserted'), icon: 'error', title: ClientName, });
        }
    });
}


function ShowRespnose(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            ResetAll();
            
        }
    });
}