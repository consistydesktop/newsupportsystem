﻿function EmailValidation(EmailID) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(EmailID)) {
        return false;
    } else {
        return true;
    }
}

function CustomerRegistrationValidation() {

    var Name = $("#txtName").val();
    var Username = $("#txtUserName").val();
    var EmailID = $("#txtEmail").val();
    var MobileNumber = $("#txtMobileNumber").val();

    var PANNo = $("#txtPANNo").val();
    var GSTNo = $("#txtGSTNo").val();
    var Address = $("#txtAddress").val();
    var State = $("#txtState").val();

    if (Name == "" && Username == "" && EmailID == "" && MobileNumber == "" && Address == "" && State == "0") {
        $("#lblname").show();
        $("#lblUsername").show();
        $("#lblEmailID").show();
        $("#lblMobileNumber").show();
        $("#lblAddress").show();
        $("#lblState").show();
        $("#txtName").focus();

        return false;
    }

    if (Name == "") {
        $("#lblname").show();

        return false;
    }

    if (EmailID == "") {
        $("#lblEmailID").show();
        return false;
    }
    if (EmailID != "") {
        if (!EmailValidation(EmailID)) {
            $("#lblEmailID").html("Please enter valid email");
            $("#lblEmailID").show();
            return false;
        }
    }

    if (MobileNumber == "") {
        $("#lblMobileNumber").show();
        return false;
    }

    if (MobileNumber.length != 10) {
        $("#lblMobileNumber").html("Please enter valid mobile number");
        $("#lblMobileNumber").show();
        return false;
    }


    if (PANNo == "" && GSTNo == "") {
        $("#lblPANNo").html("Please enter PAN no or GST no.");
        $("#lblPANNo").show();

        return false;
    }

    if (PANNo != "") {
        var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;

        if (!regpan.test(PANNo)) {
            $("#lblPANNo").html("Please enter valid PAN card no");
            $("#lblPANNo").show();

            $("#lblPANNoFormat").html("XXXXX1234X");
            $("#lblPANNoFormat").show();
            $("#txtPANNo").focus();
            return false;
        }
        if (PANNo.length != 10) {
            $("#lblPANNo").html("Please enter valid PAN card no");
            $("#txtPANNo").focus();
            return false;
        }
    }


    if (GSTNo != "") {
        var regGst = /^([0-9]){2}([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([0-9]){1}([a-zA-Z]){1}([a-zA-Z0-9]){1}?$/;
        if (!regGst.test(GSTNo)) {
            $("#lblGSTNo").html("Please enter valid GST no");
            $("#lblGSTNo").show();

            $("#lblGSTNoFormat").html("00XXXXX0000X0X0");
            $("#lblGSTNoFormat").show();
            $("#txtGSTNo").focus();
            return false;

        }

    }




    if (State == "0") {

        $("#lblState").show();
        return false;
    }

    return true;
}

function ShowRespose(data) {
    Icon = data.d.Icon;
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}

function CustomerRegistrationInsert() {
    var Name = $("#txtName").val();
    var Username = $("#txtUserName").val();
    var EmailID = $("#txtEmail").val();
    var MobileNumber = $("#txtMobileNumber").val();
    Username = MobileNumber;

    var PANNo = $("#txtPANNo").val();
    var GSTNo = $("#txtGSTNo").val();
    var Address = $("#txtAddress").val();
    var State = $("#txtState").val();

    if (CustomerRegistrationValidation() == false) {
        return false;
    }

    var jsonObj = {
        Name: Name,
        Username: Username,
        EmailID: EmailID,
        MobileNumber: MobileNumber,
        PANNo: PANNo,
        GSTNo: GSTNo,
        Address: Address,
        State: State,
    };
    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "SignUp.aspx/Insert",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            Icon = data.d.Icon;
            Message = data.d.Message;
            $.alert.open({
                content: Message, icon: 'warning', title: ClientName,
                callback: function () {
                }
            });
        },
        error: function (result) {
            $.alert.open({
                content: "Somthing Worng", icon: 'error', title: ClientName,
            });
        }
    });
    return false;


}

function Reset() {
    $("#txtName").val("");
    $("#txtUserName").val("");
    $("#txtEmail").val("");
    $("#txtMobileNumber").val("");
    $("#txtName").focus();

    $("#txtPANNo").val("");
    $("#txtGSTNo").val("");
    $("#txtAddress").val("");
    $("#txtState").val("");

}

function UserInformationSelectByID(ID) {
    var ID = ID;
    $("#txtName").val("Please Wait...");
    $("#txtUserName").val("Please Wait...");
    $("#txtEmail").val("Please Wait...");
    $("#txtMobileNumber").val("Please Wait...");

    var jsonObj = {
        UserID: ID,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "SignUp.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#txtName").val(data.d.Name);
            $("#txtUserName").val(data.d.Username);
            $("#txtEmail").val(data.d.EmailID);
            $("#txtMobileNumber").val(data.d.MobileNumber);

            $("#UserID").html(ID);
        },
        error: function (result) {
            $.alert.open({ content: "Not updated", icon: 'error', title: ClientName, });
        }
    });
}

function UserInformationUpdate() {
    var Name = $("#txtName").val();
    var Username = $("#txtUserName").val();
    var EmailID = $("#txtEmail").val();
    var MobileNumber = $("#txtMobileNumber").val();
    var UserID = $("#UserID").text();

    var jsonObj = {
        Name: Name,
        Username: Username,
        EmailID: EmailID,
        MobileNumber: MobileNumber,
        UserID: UserID,
    };
    var jData = JSON.stringify(jsonObj)

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to update',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "SignUp.aspx/Update",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                    window.location.href = "UserInformation.aspx";
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: 'Not updated', icon: 'error', title: ClientName, });
        }
    });
    Reset();
}

function Redirect() {
    window.location.href = "Login.aspx";
}

function isPANKey(e, controlName) {
    $("#lblPANNo").hide();
    var r = e.which ? e.which : event.keyCode;
    var txtLength = $("#" + controlName).val().length;

    var txt = document.getElementById(controlName);
    var selectedTextLen = txt.value.substr(txt.selectionStart, (txt.selectionEnd - txt.selectionStart)).length;
    txtLength = txtLength - selectedTextLen;

    if (txtLength < 5 || txtLength > 8) {
        return (64 > r || r > 91) && (96 > r || r > 122) && (e.which != 8) ? !1 : void 0
    }
    else {
        return r > 31 && (48 > r || r > 57) && (e.which != 46) && (e.which != 8) ? !1 : void 0
    }
}

function isGSTKey(e, controlName) {
    $("#lblGSTNo").hide();
    var r = e.which ? e.which : event.keyCode;
    //console.log("length:" + $("#" + controlName).val().length);

    var txt = document.getElementById(controlName);
    var selectedTextLen = txt.value.substr(txt.selectionStart, (txt.selectionEnd - txt.selectionStart)).length;
    var contentLength = $("#" + controlName).val().length;

    contentLength = contentLength - selectedTextLen;

    if (contentLength <= 1) {
        //number
        return r > 31 && (48 > r || r > 57) && (e.which != 46) && (e.which != 8) ? !1 : void 0
    }
    else if (contentLength >= 2 && contentLength <= 6) {
        //alphanumberic
        return (64 > r || r > 91) && (96 > r || r > 122) && (e.which != 8) ? !1 : void 0
    }
    else if (contentLength >= 7 && contentLength <= 10) {
        return r > 31 && (48 > r || r > 57) && (e.which != 46) && (e.which != 8) ? !1 : void 0
    }
    else if (contentLength == 11) {
        return (64 > r || r > 91) && (96 > r || r > 122) && (e.which != 8) ? !1 : void 0
    }
    else if (contentLength == 12) {
        return r > 31 && (48 > r || r > 57) && (e.which != 46) && (e.which != 8) ? !1 : void 0
    }
    else if (contentLength == 13) {
        return (64 > r || r > 91) && (96 > r || r > 122) && (e.which != 8) ? !1 : void 0
    }
    else if (contentLength == 14) {
        return (48 > r || r > 57) && (64 > r || r > 91) && (96 > r || r > 122) && (e.which != 8) ? !1 : void 0
    }
    else {
        return (64 > r || r > 91) && (96 > r || r > 122) && (e.which != 8) ? !1 : void 0
    }
}



$("#txtPANNo").focusout(function () {
    var PANNo = $("#txtPANNo").val();
    if (PANNo != "") {
        var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;

        if (!regpan.test(PANNo)) {
            $("#lblPANNo").html("Please enter valid PAN card no");
            $("#lblPANNo").show();

            $("#lblPANNoFormat").html("XXXXX1234X");
            $("#lblPANNoFormat").show();
            $("#txtPANNo").focus();
            return false;
        }
        if (PANNo.length != 10) {
            $("#lblPANNo").html("Please enter valid PAN card no");
            $("#txtPANNo").focus();
            return false;
        }
    }
    $("#lblPANNoFormat").hide();
});

$("#txtGSTNo").focusout(function () {

    var PANNo = $("#txtPANNo").val();
    if (PANNo != "") {
        return true;
    }

    var GSTNo = $("#txtGSTNo").val();

    var regGst = /^([0-9]){2}([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([0-9]){1}([a-zA-Z]){1}([a-zA-Z0-9]){1}?$/;
    if (!regGst.test(GSTNo)) {
        $("#lblGSTNo").html("Please enter valid GST no");
        $("#lblGSTNo").show();

        $("#lblGSTNoFormat").html("00XXXXX0000X0X0");
        $("#lblGSTNoFormat").show();
        $("#txtGSTNo").focus();
        return false;

    }
    $("#lblGSTNoFormat").hide();
});


$("#txtEmail").focusout(function () {

    var EmailID = $("#txtEmail").val();
    if (!EmailValidation(EmailID)) {
        $("#lblEmailID").html("Please enter valid email");
        $("#lblEmailIDFormat").html("email@mail.com");
        $("#lblEmailIDFormat").show();
        $("#lblEmailID").show();
        return false;
    }


    $("#lblEmailIDFormat").hide();
});


$("#txtMobileNumber").focusout(function () {

    var MobileNumber = $("#txtMobileNumber").val();

    if (MobileNumber == "") {
        $("#lblMobileNumber").show();
        return false;
    }

    if (MobileNumber.length != 10) {
        $("#lblMobileNumber").html("Please enter valid mobile number");
        $("#lblMobileNumber").show();
        return false;
    }


});