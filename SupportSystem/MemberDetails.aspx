﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MemberDetails.aspx.cs" Inherits="SupportSystem.MemberDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">

        <section>

            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Approve Customer
                                <button class="btn btn-info buttons float-right" type="button" onclick=' location.href="\SystemName.aspx"'>Assign System Name</button></h6>
                        </div>
                    </div>
                </div>
            </section>

            <div class="table-responsive">
                <table id="MemberDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                    <thead></thead>
                    <tbody></tbody>
                </table>
            </div>
        </section>


        <div class="modal fade" id="modal_CustomerInformation" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" style="min-width: 30%" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle"><b>
                            <label id="#">Customer Information</label>
                        </b>

                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-6">
                                    <h5><b>Customer Name:   </b></h5>
                                </div>
                                <div class="col-6">
                                    <h5>
                                        <label id="LabelCustomerName"></label>
                                    </h5>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-6">
                                    <h5><b>User Name:   </b></h5>
                                </div>
                                <div class="col-6">
                                    <h5>
                                        <label id="LabelUserName"></label>
                                    </h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h5><b>GST No:   </b></h5>
                                </div>
                                <div class="col-6">
                                    <h5>
                                        <label id="LabelGSTNo"></label>
                                    </h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h5><b>PAN No:   </b></h5>
                                </div>
                                <div class="col-6">
                                    <h5>
                                        <label id="LabelPANNo"></label>
                                    </h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h5><b>Applied Date:   </b></h5>
                                </div>
                                <div class="col-6">
                                    <h5>
                                        <label id="LabelAppliedDate"></label>
                                    </h5>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-6">
                                    <h5><b>EmailID :   </b></h5>
                                </div>
                                <div class="col-6">
                                    <h5>
                                        <label id="LabelEmailID"></label>
                                    </h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h5><b>MobileNo :   </b></h5>
                                </div>
                                <div class="col-6">
                                    <h5>
                                        <label id="LabelMobileNo"></label>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="/JS/MemberDetails.js"></script>

    <script>
        $(document).ready(function () {

            CustomerRegistrationSelect();

        });
    </script>
</asp:Content>
