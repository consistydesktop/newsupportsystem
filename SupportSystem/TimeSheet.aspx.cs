﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class TimeSheet : System.Web.UI.Page
    {
        private static string pageName = "TimeSheet.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static TimeSheetModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);

            List<TimeSheetModel> details = new List<TimeSheetModel>();
            TimeSheetBal objBAL = new TimeSheetBal();
            try
            {
                details = objBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Insert(string Date, int SystemID, string Task, int Time, int Status)
        {


            int Result = -1;
            TimeSheetBal ObjBAL = new TimeSheetBal();
            MessageWithIcon msgIcon = new MessageWithIcon();

            DateTime ToDay = DateTime.Today;
            DateTime TimeSheetDate =Convert.ToDateTime(Date);

            double i = (ToDay - TimeSheetDate).TotalDays;

            if (i > 5.0)
            {
                msgIcon.Message = "You cannot insert timesheet.";
                return msgIcon;
            }


            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            int TotalTime = 0;
            try
            {
                TotalTime = ObjBAL.SelectTotalTimeByUserID(UserID,Date);
               
            }
            catch (Exception ex) { new Logger().write(ex); }
            try
            {
                Result = ObjBAL.Insert(UserID, Date, SystemID, Task, Time, Status);
            }
            catch (Exception ex) { new Logger().write(ex); }

            if (Result > 0)
                msgIcon.Message = "Timesheet submitted successufully";
            else
                msgIcon.Message = "Timesheet Not submitted";
            return msgIcon;
        }

        [WebMethod]
        public static MessageWithIcon Update(int TimeSheetDetailID, int SystemID, string Task, int Time, int Status)
        {

            TimeSheetBal ObjBAL = new TimeSheetBal();
            MessageWithIcon msgIcon = new MessageWithIcon();

            DateTime ToDay = DateTime.Today;
            DateTime TimeSheetDate = ObjBAL.getDateByID(TimeSheetDetailID);

            double i = (ToDay - TimeSheetDate).TotalDays;

            if (i > 2.0)
            {
                msgIcon.Message = "You cannot update timesheet.";
                return msgIcon;
            }

            int Result = -1;


            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            try
            {
                Result = ObjBAL.Update(UserID, TimeSheetDetailID, SystemID, Task, Time, Status);
            }
            catch (Exception ex) { new Logger().write(ex); }

            if (Result > 0)
                msgIcon.Message = "Timesheet updated successufully.";
            else
                msgIcon.Message = "Timesheet not updated";
            return msgIcon;
        }

        [WebMethod]
        public static TimeSheetModel[] SelectTimeSheetByDate(string TimesheetDate) //
        {

            AccessController.checkAccess(pageName);

            List<TimeSheetModel> details = new List<TimeSheetModel>();
            TimeSheetBal ObjBAL = new TimeSheetBal();
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());

            try
            {
                details = ObjBAL.SelectTimeSheetByDate(UserID, TimesheetDate);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Delete(int TimeSheetDetailID)
        {
            int Result = -1;
            TimeSheetBal ObjBAL = new TimeSheetBal();
            MessageWithIcon msgIcon = new MessageWithIcon();
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            try
            {
                Result = ObjBAL.Delete(UserID, TimeSheetDetailID);
            }
            catch (Exception ex) { new Logger().write(ex); }

            if (Result > 0)
                msgIcon.Message = "Record deleted successfully.";
            else
                msgIcon.Message = "Record not deleted.";
            return msgIcon;
        }


    }
}