﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reminder.aspx.cs" Inherits="SupportSystem.Reminder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Reminder
                                <button class="btn btn-info buttons float-right" type="button" onclick='location.href="/ReminderReport.aspx"' title="View Report">
                                    View Report
                                </button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputCustomerName">Name</label>
                        <input type="text" class="form-control loginput" id="inputCustomerName" title="Customer Name" aria-describedby="emailHelp" placeholder="Enter Name" onchange='$("#inputType").focus()' />
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputType">Type</label><br />
                        <select class="form-control loginput" id="inputType" title="Select Type" onchange='$("#inputDate").focus()'>
                            <option value="0">Select Type</option>
                            <option value="1">Payment</option>
                            <option value="2">Website Demo</option>
                            <option value="3">App Demo</option>
                            <option value="4">GST Bill</option>
                            <option value="5">Office Bill</option>
                            <option value="6">Server Payment</option>
                        </select>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputDate">Reminder Date</label>
                        <input type="text" autocomplete="off" class="form-control loginput" id="inputDate" title="Select Date" aria-describedby="emailHelp" placeholder="Reminder Date" onchange='$("#inputAmount").focus()' />
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputAmount">Amount (Optional)</label>
                        <input type="text" class="form-control loginput" id="inputAmount" title="Enter Amount" style="text-align: right; width: auto" onkeypress="return isNumberKey(event)" maxlength="8" aria-describedby="emailHelp" placeholder="Enter Amount" onchange='$("#inputIsActiveReminder").focus()' />
                    </div>
                </div>

                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputIsActiveReminder">Is Active </label>
                        <input type="checkbox" checked="checked" class="form-control loginput" id="inputIsActiveReminder" title="Is active" aria-describedby="emailHelp" />
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputDescription">Description (Optional)</label>
                        <textarea class="form-control loginput" id="inputDescription" title="Enter Description" aria-describedby="emailHelp" placeholder="Enter Description" onchange='$("#SubmitButton").focus()'></textarea>
                    </div>
                    <label id="ReminderID" style="display: none"></label>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return ReminderMasterInsert();" id="SubmitButton" title="Add Reminder">ADD</button>
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return ReminderMasterUpdate();" id="UpdateButton" title="Update Reminder" style="display: none;">Update</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick=" Reset();" title="Cancel Reminder">Cancel</button>
                            </div>
                        </div>
                        <label id="ResponseMsgLbl"></label>
                    </div>
                </div>
            </form>
            <div class="loader" id="loader" style="display: none"></div>
        </section>
    </div>
    <script src="/JS/Reminder.js"></script>

    <script>
        $("#inputDate").datepicker({
            dateFormat: 'd MM, yy',
            minDate: "0",
            changeMonth: true,
            changeYear: true
        });

        var CustomerNames = [];
        var Status = "activated";

        $(document).ready(function () {
            $("#inputCustomerName").focus();
            SelectCustomerName();
         //   SelectReminderType();
            autocomplete(document.getElementById("inputCustomerName"), CustomerNames);
        });



        function isNumberKey(e) {
            var r = e.which ? e.which : event.keyCode;
            return r > 31 && (48 > r || r > 57) && (e.which != 46 || $(this).val().indexOf('.') != -1) ? !1 : void 0
        }
        if (window.location.href.indexOf("ReminderMasterID") > -1) {
            var url = window.location.href;
            var id = url.split('=')
         //   SelectReminderType();
            ReminderMasterSelectByID(id[1]);
        }
    </script>
</asp:Content>
