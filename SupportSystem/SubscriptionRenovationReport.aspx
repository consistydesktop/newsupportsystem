﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SubscriptionRenovationReport.aspx.cs" Inherits="SupportSystem.SubscriptionRenovationReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Subscritpion Report
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <div class="form-group row mt-3">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputFromDate">From Date </label>
                    <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                </div>

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputToDate">To Date</label>
                    <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()' />
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputRequestType">Status</label>
                    <select id="ddlStatus" class="form-control loginput" >

                        <option value="All">All</option>
                        <option value="Success">Success</option>
                        <option value="Failure">Fail</option>
                        <option value="PENDING">Pending</option>
                    </select>

                </div>

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-6">
                            <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="SubscriptionHistory();" id="SearchButton" title="Search">Search</button>
                        </div>
                    </div>
                    <label id="ResponseMsgLbl"></label>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="Div1">
                            <div class="tab-pane fade show active" id="Div2" role="tabpanel" aria-labelledby="projects-tab">
                                <div class="table-responsive">
                                    <table id="tblSubscriptionDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="loader" id="loader" style="display: none"></div>
               <input type="hidden" id="ID" />


    </div>


    <label id="SubscriptionID" style="display: none"></label>

  
    <script src="JS/SubscriptionRenovationReport.js"></script>

    <script src="JS/Validation.js"></script>
    <script>
        $(document).ready(function () {
            var id = ('<%= Session["UserTypeID"] %>');
            $("#ID").text(id);

            $("#inputFromDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });
            $("#inputToDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true
            });

            var d = new Date();
            var currMonth = d.getMonth();
            var currYear = d.getFullYear();

            var startDate = new Date(currYear, currMonth, 1);
            $("#inputFromDate").datepicker("setDate", startDate);

            $("#inputToDate").datepicker('option', 'minDate', startDate);
            $("#inputToDate").datepicker("setDate", d);


            //     SubscriptionHistory();
        });
    </script>
</asp:Content>
