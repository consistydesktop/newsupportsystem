﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FinovativeVersionHistory.aspx.cs" Inherits="SupportSystem.FinovativeVersionHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .bs-example {
            margin: 20px;
        }
    </style>
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Finovative Version History
                            </h6>
                        </div>
                    </div>
                </div>
            </section>

            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputVersion">Version</label>
                        <input type="text" class="form-control loginput" id="inputVersion" title="Version" placeholder="Version"></input>
                    </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputEndDate">Date</label>
                        <input type="text" class="form-control loginput" name="inputDate" id="inputDate" title=" date" />
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputTitle">Title</label>
                        <input type="text" class="form-control loginput" id="inputTitle" title="Title" aria-describedby="emailHelp" placeholder="Title"></input>
                    </div>
                  
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                        <label for="inputDescription">Description</label>
                        <textarea class="form-control loginput p-0" rows="8" id="inputDescription" aria-describedby="emailHelp" placeholder="Enter Description"></textarea>
                    </div>


                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" title="Add" onclick="FinovativeVersionInsert()">Add</button>

                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return VersionHistoryUpdate();" id="UpdateButton" title="Update URL Details" style="display: none;">Update</button>


                            </div>
                        </div>

                    </div>
                </div>
            </form>
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="projects" role="tabpanel" aria-labelledby="projects-tab">
                                <div class="table-responsive">
                                    <table id="tblVersionHistory" class="table table-bordered nowrap table-hover" style="width: 100%">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="hdnFinovativeVersionHistoryID" />
            <label id="txtFinovativeVersionHistoryID" style="display: none"></label>
        </section>
    </div>
    <script src="JS/FinovativeVersionHistory.js"></script>
    <script>
        $(document).ready(function () {

            Select();
            $("#inputDate").datepicker({
                dateFormat: 'd MM, yy',
                // minDate: "0",
                changeMonth: true,
                changeYear: true
            });
        });


    </script>
</asp:Content>
