﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;

namespace SupportSystem
{
    /// <summary>
    /// Summary description for FileUploadHandlerForTicket
    /// </summary>
    public class FileUploadHandlerForTicket : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            List<AddTicketAdminModel> details = new List<AddTicketAdminModel>();
            string serverFileName = string.Empty;
            string fileName = string.Empty;
            HttpPostedFile file;

            serverFileName = "NoImage";
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;

                for (int i = 0; i < files.Count; i++)
                {
                    file = files[i];
                    string OriginalFileName = file.FileName;

                    string Extension = Path.GetExtension(file.FileName);
                    if (!Validation.IsValidImageType(Extension))
                    {

                        context.Response.ContentType = "text/plain";
                        context.Response.Write("Only jpg / jpeg /pdf and png files are allowed!");
                        return;
                    }
                    if (!Validation.IsValidImageSize(file.ContentLength))
                    {

                        context.Response.ContentType = "text/plain";
                        context.Response.Write("File size must under 2MB!");
                        return;
                    }
                    string extension = ".png";
                    fileName = Guid.NewGuid().ToString() + extension;
                    string key = files.AllKeys[i];

                    serverFileName = context.Server.MapPath("./TicketAttachment/" + fileName);
                    file.SaveAs(serverFileName);

                    DataTable dt = new DataTable();
                    dt = FileInsert(fileName, OriginalFileName);
                    foreach (DataRow dtrow in dt.Rows)
                    {
                        AddTicketAdminModel report = new AddTicketAdminModel();
                        report.FileID = Convert.ToInt32(dtrow["FileID"].ToString());
                        details.Add(report);
                    }
                }
                context.Response.ContentType = "text/plain";
                string fileIDs = "";

                foreach (AddTicketAdminModel data in details)
                {
                    fileIDs = fileIDs + "#" + data.FileID;
                }

                context.Response.Write(fileIDs);
            }
        }

        private DataTable FileInsert(string path, string OriginalFileName)
        {
            AddTicketAdminModel objModel = new AddTicketAdminModel();
            AddTicketAdminBal objBal = new AddTicketAdminBal();

            DataTable dt = new DataTable();

            objModel.Attachment = "/TicketAttachment/" + path;
            objModel.OriginalFileName = OriginalFileName;
            dt = objBal.FileInsert(objModel);

            return dt;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}