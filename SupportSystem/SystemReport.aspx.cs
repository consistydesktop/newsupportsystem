﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class SystemReport : System.Web.UI.Page
    {
        private static string pageName = "SystemReport.aspx";


        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }


        [WebMethod]
        public static SystemReportModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);

            List<SystemReportModel> details = new List<SystemReportModel>();
            SystemReportBal objBAL = new SystemReportBal();
            try
            {
                details = objBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }


        [WebMethod]
        public static List<List<SystemReportModel>> Search(string FromDate, string ToDate, int SystemID)
        {
            List<List<SystemReportModel>> details = new List<List<SystemReportModel>>();
            SystemReportBal objBAL = new SystemReportBal();
            try
            {
                details = objBAL.Search(FromDate, ToDate, SystemID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;

        }
    }
}