﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportSystem.StaticClass
{
    public class AllMessages
    {
        public static string TicketAssignEmailToClient = "Hi [Username], <br/><br/> Ticket:[TicketNo] is accepted and assigned to employee. <br/>  Title: \" [Title] \" <br/><br/> ​Thanks & Regards, <br/><br/><br/> Consisty System <br/> Office Contact : 9067 486948 |  9067 397680  <br/> Office Time- 10:00 Am to 07:00 PM - Monday - Saturday <br/> www.consisty.com ";

        public static string TicketAddedByClientEmail = " Hi [Username], <br/><br/> Ticket for created successfully. <br/> Title: \" [Title] \" <br/> Your ticket no is [TicketNo]. <br/><br/> ​Thanks & Regards, <br/><br/><br/> Consisty System <br/> Office Contact : 9067 486948 |  9067 397680 <br/> Office Time- 10:00 Am to 07:00 PM - Monday - Saturday <br/> www.consisty.com ";

        public static string TicketResolvedByEmployeeEmail = " Hi [Username], <br/><br/> Ticket:[TicketNo] status is [Status] updated by developer. If still you find any issue please revert back.<br/> Title: \" [Title] \" <br/> Your ticket no is [TicketNo]. <br/><br/> ​Thanks & Regards, <br/><br/><br/> Consisty System <br/> Office Contact : 9067 486948 |  9067 397680 <br/> Office Time- 10:00 Am to 07:00 PM - Monday - Saturday <br/> www.consisty.com ";

        public static string TicketAcceptedAckForClient = "Consisty System || Ticket Accepted";

        public static string TicketAssignedSMSToDeveloper = "Hi, Ticket:[TicketNo] is accepted and assigned to you. Please check.";

        public static string Failure = "FAILURE";

        public static string NotFound = "[Parameter] not found.";


        public static string DataNotFound = "Data not found.";
        public static string Success = "SUCCESS";
        public static string Blank = "[Parameter] cannot be blank.";
        public static string Invalid = "Invalid [Parameter]";
        public static string LeaveRequestAccept = "Hi [Username], <br/><br/> Your leave from  :[FromDate] to [ToDate] is approved. <br/>  ​Thanks & Regards, <br/><br/><br/> Consisty System <br/> Office Contact : 9067 486948 |  9067 397680  <br/> Office Time- 10:00 Am to 07:00 PM - Monday - Saturday <br/> www.consisty.com ";
        public static string LeaveRequestReject = "Hi [Username], <br/><br/> Your leave from  :[FromDate] to [ToDate] is not approved. <br/>  ​Thanks & Regards, <br/><br/><br/> Consisty System <br/> Office Contact : 9067 486948 |  9067 397680  <br/> Office Time- 10:00 Am to 07:00 PM - Monday - Saturday <br/> www.consisty.com ";
        public static int FinovativeProjectMasterID = 1143;
        public static string invalidUserInput = "Invalid user inputs";
        public static readonly string PGSubscriptionMessage = "Thank you for online Payment! Your subscription is extended.";
    }
}