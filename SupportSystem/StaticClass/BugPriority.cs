﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportSystem.StaticClass
{
    public class BugPriority
    {
        public static string LOW = "1";
        public static string MEDIUM = "2";
        public static string HIGH = "3";
        public static string CRITICAL = "4";
    }
}