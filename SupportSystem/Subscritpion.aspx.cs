﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using SupportSystem.StaticClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class Subscritpion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Logger logger = new Logger();

            if (Request.Params.Count <= 0)
            {

                logger.LogInfo("Subscritpion.aspx", "Original", "No response");
                return;
            }

            try
            {
                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                url = HttpUtility.UrlDecode(url);

                if (!url.Contains("?")) return;


                url = url.Replace("\n", " ");
                logger.LogInfo("SubscritpionRenovation.aspx", "url", url);
                var URl = url.Split('?')[0];
                var Parameter = url.Split('?')[1];

                var LoginToken = Parameter.Split('#')[0];
                var WebURL = Parameter.Split('#')[1];
                var ProductID = Products.Finovative.ToString();
                var CallBackURL = "";



                try
                {
                    ProductID = Parameter.Split('#')[2];
                }
                catch (Exception ex)
                {
                    ProductID = Products.Finovative.ToString();
                }

                try
                {
                    CallBackURL = Parameter.Split('#')[3];
                }
                catch (Exception ex)
                {
                    ProductID = Products.Finovative.ToString();
                }
                DataTable dt = new System.Data.DataTable();
                LoginBal objBAL = new LoginBal();
                dt = objBAL.SelectByToken(LoginToken);


                if (dt.Rows.Count <= 0)
                {
                    HttpContext.Current.Response.Redirect("Login.aspx", true);
                    return;
                }

                HttpContext.Current.Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                HttpContext.Current.Session["MobileNumber"] = dt.Rows[0]["MobileNumber"].ToString();
                HttpContext.Current.Session["Username"] = dt.Rows[0]["Username"].ToString();
                HttpContext.Current.Session["EmailID"] = dt.Rows[0]["EmailID"].ToString();
                HttpContext.Current.Session["UserTypeID"] = dt.Rows[0]["UserTypeID"].ToString();
                HttpContext.Current.Session["UserType"] = dt.Rows[0]["UserType"].ToString();


                LoginModel objModel = new LoginModel();
                AccessController a = new AccessController();
                string Token = a.getRandomToken();
                objModel.Token = Token;
                objModel.MobileNumber = dt.Rows[0]["MobileNumber"].ToString(); ;
                objBAL.UpdateToken(objModel);
                HttpContext.Current.Session["Token"] = Token.ToString();
                HttpContext.Current.Session["WebURL"] = WebURL.ToString();
                HttpContext.Current.Session["ProductID"] = ProductID.ToString();
                Task.Run(() =>
                {
                    int UserID = Convert.ToInt32(dt.Rows[0]["UserID"].ToString());
                    AddWebSubscriptionBal ObjBal = new AddWebSubscriptionBal();
                    ObjBal.UpdateCallBackURL(CallBackURL, UserID, Convert.ToInt32(ProductID), WebURL.ToString());
                });


                string SubscriptionEndDate = "";
                int remainingDays = 0;
                string HostedIP = GetIPAddress();
                DateTime EndDate = getRemainingDays(WebURL.ToString(), HostedIP, LoginToken);
                if (DateTime.Compare(EndDate, default(DateTime)) == 0)
                {
                    SubscriptionEndDate = "";
                    remainingDays = 0;
                }
                else
                {
                    DateTime Today = DateTime.Now;
                    remainingDays = (EndDate - Today).Days;
                    SubscriptionEndDate = (EndDate.ToString("dd/MM/yyyy"));

                }

                HttpContext.Current.Session["SubscriptionEndDate"] = SubscriptionEndDate.ToString();
                HttpContext.Current.Session["RemainingDays"] = remainingDays.ToString();


                HttpContext.Current.Response.Redirect("SubscritpionRenovation.aspx", true);



            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                // HttpContext.Current.Response.Redirect("Login.aspx", true);
            }

        }
        private DateTime getRemainingDays(string URL, string HostedIP, string LoginToken)
        {
            DataTable dt = new DataTable();
            AddWebSubscriptionBal ObjBal = new AddWebSubscriptionBal();
            dt = ObjBal.getEndDateForFinovative(URL, HostedIP, LoginToken);

            if (dt.Rows.Count < 1)
            {
                return default(DateTime);
            }

            DateTime EndDate = Convert.ToDateTime(dt.Rows[0]["EndDate"]);
            return EndDate;
        }
        private string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}