﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Web.Services;

namespace SupportSystem
{
    public partial class ViewTicketsAdmin : System.Web.UI.Page
    {
        private static string pageName = "ViewTicketsAdmin.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static ViewTicketsAdminModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();
            ViewTicketsAdminBal objBAL = new ViewTicketsAdminBal();
            try
            {
                details = objBAL.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static ViewTicketsAdminModel[] Search(string FromDate, string ToDate)
        {
            AccessController.checkAccess(pageName);
            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();
            ViewTicketsAdminBal objBal = new ViewTicketsAdminBal();
            try
            {
                details = objBal.Search(FromDate, ToDate);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static ViewTicketsAdminModel[] SelectByTicketID(int TicketID)
        {
            AccessController.checkAccess(pageName);

            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();
            ViewTicketsAdminBal objBAL = new ViewTicketsAdminBal();
            try
            {
                details = objBAL.SelectByTicketID(TicketID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static ViewTicketsAdminModel[] SelectEmployeeName()
        {
            AccessController.checkAccess(pageName);
            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();
            ViewTicketsAdminBal objBAL = new ViewTicketsAdminBal();
            try
            {
                details = objBAL.SelectEmployeeName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static ViewTicketsAdminModel[] SelectServiceName()
        {
            AccessController.checkAccess(pageName);
            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();
            ViewTicketsAdminBal objBAL = new ViewTicketsAdminBal();
            try
            {
                details = objBAL.SelectServiceName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static ViewTicketsAdminModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);

            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();
            ViewTicketsAdminBal objBAL = new ViewTicketsAdminBal();
            try
            {
                details = objBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
       
        public static ViewTicketsAdminModel[] SearchByFilter(string FromDate, string ToDate, int DeveloperID, int SystemID, int ServiceID, string TicketNo)
        {
            AccessController.checkAccess(pageName);
            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();
            ViewTicketsAdminBal objBal = new ViewTicketsAdminBal();

            try
            {
                //details = objBal.SearchByFilter(FromDate, ToDate, Name, SystemName, ServiceName, TicketNo);
                details = objBal.SearchByFilter(FromDate.Trim(), ToDate.Trim(), DeveloperID, SystemID, ServiceID, TicketNo.Trim());
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }
    }
}