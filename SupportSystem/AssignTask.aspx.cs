﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using SupportSystem.Controller;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class AssignTask : System.Web.UI.Page
    {
        private static string pageName = "AssingTask.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static AssignTaskModel[] SelectEmployeeName()
        {
            AccessController.checkAccess(pageName);

            List<AssignTaskModel> details = new List<AssignTaskModel>();
            AssignTaskBal ObjBAL = new AssignTaskBal();
            try
            {
                details = ObjBAL.SelectEmployeeName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static int Insert(int EmployeeID, int SystemID, string Task, string FileIDs, string DueDate, string Status, string Description, int NumberOfPoints, string Comment, List<AssignTaskModel> SubTaskObjects)
        {
            MessageWithIcon msgIcon = new MessageWithIcon();
            AssignTaskModel ObjModel = new AssignTaskModel();

            ObjModel.AssignedUserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);

            ObjModel.Task = Task;
            ObjModel.SystemID = SystemID;
            ObjModel.Deadline = DueDate;
            ObjModel.TaskStatus = "NEW";
            ObjModel.Description = Description;
            ObjModel.Point = NumberOfPoints;

            int i = -1;
            AssignTaskBal ObjBal = new AssignTaskBal();
            try
            {
                i = ObjBal.Insert(EmployeeID, ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (i < 0)
                msgIcon.Message = "Task not assigned .";
            else
            {
                msgIcon.Message = "Task assigned successfully";
                ObjModel.TaskID = i;

                int j = 0;
                string[] fileID = FileIDs.Split('#');

                if (fileID.Length > 1)
                {
                   
                        for (j = 1; j < fileID.Length; j++)
                        {
                            var currentFileID = fileID[j];
                            ObjModel.FileID = Convert.ToInt32(currentFileID);
                            ObjModel.TaskID = i;
                            ObjModel.UploadType = UploadTypeMaster.UploadTask;
                            try
                            {
                                ObjBal.FileMappingInsert(ObjModel);
                            }
                            catch (Exception Ex) { new Logger().write(Ex); }
                        }
                    
                }

                foreach (var item in SubTaskObjects)
                {
                    item.TaskID = ObjModel.TaskID;
                    try
                    {
                        i = ObjBal.SubTaskInsert(item);
                    }
                    catch (Exception ex) { new Logger().write(ex); }

                    if (i < 0)
                        msgIcon.Message += " Sub task not added successfully";
                }


                if (Comment != "")
                {
                    ObjModel.Comment = Comment;
                    ObjModel.CommentType = CommentTypeMaster.TaskCommentType;
                    i = -1;
                    try
                    {
                        i = ObjBal.InsertComment(ObjModel);
                    }
                    catch (Exception ex)
                    {
                        new Logger().write(ex);
                    }
                }
            }

            return ObjModel.TaskID;
        }

        [WebMethod]
        public static List<List<AssignTaskModel>> SelectByID(int TaskID)
        {
            AccessController.checkAccess(pageName);

            List<List<AssignTaskModel>> details = new List<List<AssignTaskModel>>();
            AssignTaskBal ObjBAL = new AssignTaskBal();

            try
            {
                details = ObjBAL.SelectByID(TaskID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }

        [WebMethod]
        public static MessageWithIcon InsertComment(int TaskID, string Comment)
        {
            MessageWithIcon msgIcon = new MessageWithIcon();
            int Result = -1;
            AssignTaskBal ObjBal = new AssignTaskBal();

            AssignTaskModel ObjModel = new AssignTaskModel();
            ObjModel.TaskID = TaskID;
            ObjModel.AssignedUserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            ObjModel.Comment = Comment + "    @" + DateTime.Now;

            ObjModel.CommentType = CommentTypeMaster.TaskCommentType;
            try
            {
                Result = ObjBal.InsertComment(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (Result > -1)
                msgIcon.Message = "Comment added successfully";
            else
                msgIcon.Message = "Comment not added";

            return msgIcon;
        }

        [WebMethod]
        public static MessageWithIcon Update(int EmployeeID, int SystemID, int TaskID, string Task, string FileIDs, string DueDate, string Status, string Description, int NumberOfPoints, string Comment)
        {
            MessageWithIcon msgIcon = new MessageWithIcon();
            AssignTaskModel ObjModel = new AssignTaskModel();

            ObjModel.AssignedUserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);

            ObjModel.TaskID = TaskID;
            ObjModel.SystemID = SystemID;
            ObjModel.Task = Task;
            ObjModel.Deadline = DueDate;
            ObjModel.TaskStatus = "NEW";
            ObjModel.Description = Description;
            ObjModel.Point = NumberOfPoints;

            int i = -1;
            AssignTaskBal ObjBal = new AssignTaskBal();
            try
            {
                i = ObjBal.Update(EmployeeID, ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (i < 0)
                msgIcon.Message = "Task not updated .";
            else
            {
                msgIcon.Message = "Task updated successfully";
                ObjModel.TaskID = i;

                int j = 0;
                string[] fileID = FileIDs.Split('#');

                if (fileID.Length > 1)
                {
                   
                        for (j = 1; j < fileID.Length; j++)
                        {
                            var currentFileID = fileID[j];
                            ObjModel.FileID = Convert.ToInt32(currentFileID);
                            ObjModel.TaskID = i;
                            ObjModel.UploadType = UploadTypeMaster.UploadTask;
                            try
                            {
                                ObjBal.FileMappingInsert(ObjModel);
                            }
                            catch (Exception Ex) { new Logger().write(Ex); }
                        }
                    
                }

                ObjModel.Comment = Comment;
            }

            return msgIcon;
        }

        [WebMethod]
        public static AssignTaskModel[] SelectAttachment(int TaskID)
        {
            AccessController.checkAccess(pageName);

            List<AssignTaskModel> details = new List<AssignTaskModel>();
            AssignTaskBal ObjBAL = new AssignTaskBal();
            try
            {
                details = ObjBAL.SelectAttachment(TaskID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static AssignTaskModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);

            List<AssignTaskModel> details = new List<AssignTaskModel>();

            AssignTaskBal objBAL = new AssignTaskBal();
            try
            {
                details = objBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }


        [WebMethod]
        public static MessageWithIcon Delete(int TaskID)
        {
            MessageWithIcon msgIcon = new MessageWithIcon();
            int Result = -1;
            AssignTaskBal ObjBal = new AssignTaskBal();
            try
            {
                Result = ObjBal.Delete(TaskID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (Result < 1)
            {
                msgIcon.Message = "Task not deleted.";
                return msgIcon;
            }

            msgIcon.Message = "Task deleted successfully.";
            return msgIcon;
        }

    }
}