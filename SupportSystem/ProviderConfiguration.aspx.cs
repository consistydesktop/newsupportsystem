﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class ProviderConfiguration : System.Web.UI.Page
    {
        private static string pageName = "ProviderConfiguration.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }





        public static bool ValidSelect(int ProviderID)
        {
            Validation validation = new Validation();
            if (validation.IsValidInt(ProviderID))
                return true;
            return true;
        }

        [WebMethod]
        public static ProviderConfigurationModel[] Select(int ProviderID, int ServiceID)
        {
            AccessController.checkAccess(pageName);
            List<ProviderConfigurationModel> details = new List<ProviderConfigurationModel>();

            try
            {
                if (!ValidSelect(ProviderID))
                    return details.ToArray();

                ProviderConfigurationModel objModel = new ProviderConfigurationModel();
                objModel.ProviderID = ProviderID;
                objModel.ServiceID = ServiceID;


                ProviderConfigurationBal objBAL = new ProviderConfigurationBal();
                DataTable dt = objBAL.SelectOpertaorSetUp(objModel);

                foreach (DataRow dtrow in dt.Rows)
                {
                    ProviderConfigurationModel report = new ProviderConfigurationModel();
                    report.ProviderID = Convert.ToInt32(dtrow["ProviderID"]);

                    report.Opcode = dtrow["OperatorCode"].ToString();
                    report.OperatorID = Convert.ToInt32(dtrow["OperatorID"].ToString());
                    report.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString());
                    report.ServiceName = dtrow["ServiceName"].ToString();
                    report.OperatorName = dtrow["OperatorName"].ToString();
                    report.Param1 = dtrow["Param1"].ToString();

                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        public static bool ValidUpdate(int OperatorID, string Opcode, int ProviderID, int index)
        {
            Validation validation = new Validation();
            if (validation.IsValidInt(OperatorID))
                return true;
            if (validation.IsValidString(Opcode))
                return true;


            if (validation.IsValidInt(ProviderID))
                return true;
            if (validation.IsValidInt(index))
                return true;
            return true;
        }
        [WebMethod]
        public static int UpdateOperator(int OperatorID, string Opcode, int ProviderID, int index, string Param1)
        {
            AccessController.checkAccess(pageName);
            ProviderConfigurationModel objModel = new ProviderConfigurationModel();
            ProviderConfigurationBal objBAL = new ProviderConfigurationBal();
            int i = 0;

            try
            {
                if (!ValidUpdate(OperatorID, Opcode, ProviderID, index))
                    return i;

                objModel.OperatorID = OperatorID;
                objModel.Opcode = Opcode;
                objModel.ProviderID = ProviderID;
                objModel.Param1 = Param1;

                i = objBAL.UpdateOperatorSetUp(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return i;
        }

        [WebMethod]
        public static ServiceModel[] ServiceSelect()
        {
            AccessController.checkAccess(pageName);
            List<ServiceModel> details = new List<ServiceModel>();
            DataTable dt = new DataTable();

            try
            {

                ProviderConfigurationBal objBAL = new ProviderConfigurationBal();

                dt = objBAL.ServiceSelect();
                if (dt.Rows.Count <= 0)
                {
                    return details.ToArray();
                }
                foreach (DataRow dtrow in dt.Rows)
                {

                    Model.ServiceModel report = new Model.ServiceModel();
                    report.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString());
                    report.ServiceName = (dtrow["Name"].ToString());

                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Model.Logger().write(ex);
            }
            return details.ToArray();
        }

    }
}