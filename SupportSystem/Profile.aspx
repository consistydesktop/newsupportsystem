﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="SupportSystem.Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Profile</h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputName">Name</label>
                        <input type="text" id="inputName" title="Name" class="form-control loginput" placeholder="Name" autofocus="autofocus" />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputUserName">UserName</label>
                        <input type="text" id="inputUserName" title="User Name" placeholder="User name" class="form-control loginput" onchange='$("#inputEmailID").focus();' />
                    </div>
                </div>
            </form>
            <label id="UserID" style="display:none"></label>
            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputOldEmail">Old Email</label>
                        <input type="text" id="inputOldEmail" title="Old email" placeholder="Old email" class="form-control loginput" onchange='$("#inputEmailID").focus();' />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputNewEmail">New Email</label>
                        <input type="text" id="inputNewEmail" title="New Email" placeholder="New email" class="form-control loginput" onchange='$("#inputEmailID").focus();' />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="UpdateEmailButton" onclick="UserInformationUpdateEmailID()" title="Update email">Update Email</button>
                    </div>
                </div>
            </form>
            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputOldPassword">Old Password</label>
                        <input type="password" id="inputOldPassword" title="Old password" placeholder="Old password" class="form-control loginput" onchange='$("#inputEmailID").focus();' />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputNewPassword">New Password</label>
                        <input type="password" id="inputNewPassword" title="New password" placeholder="New password" class="form-control loginput" onchange='$("#inputEmailID").focus();' />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputConfirmPassword">Confirm Password</label>
                        <input type="password" id="inputConfirmPassword" title="Confirm password" placeholder="Confirm password" class="form-control loginput" onchange='$("#inputEmailID").focus();' />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="UpdatePasswordButton" onclick="UserInformationUpdatePassword()" title="Update password">Update Password</button>
                    </div>
                </div>
            </form>
        </section>
    </div>

    <script src="/JS/Profile.js"></script>
    <script>
        UserInformationSelectForProfile();
    </script>
</asp:Content>
