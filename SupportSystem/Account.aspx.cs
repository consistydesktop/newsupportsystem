﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class Account : System.Web.UI.Page
    {
        private static string pageName = "Account.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }
        static bool IsValidInsert(string BankName, string AccountNumber, string BranchName, string IFSCCode)
        {

            if (string.IsNullOrEmpty(BankName))
            {
                return false;
            }
            if (string.IsNullOrEmpty(AccountNumber))
            {
                return false;
            }

            if (string.IsNullOrEmpty(BranchName))
            {
                return false;
            }

            if (string.IsNullOrEmpty(IFSCCode))
            {
                return false;
            }

            return true;
        }

        [WebMethod]
        public static MessageWithIcon InsertBankMaster(string BankName, string AccountNumber, string BranchName, string IFSCCode)
        {
            AccessController.checkAccess(pageName);



            MessageWithIcon msgIcon = new MessageWithIcon();
            if (!IsValidInsert(BankName, AccountNumber, BranchName, IFSCCode))
            {
                msgIcon.Message = Message.InvalidData;
                return msgIcon;
            }

            msgIcon.Message = "Bank not Added";
            int result = -1;

            try
            {

                BankMasterModel ObjModel = new BankMasterModel();
                ObjModel.BankName = BankName;
                ObjModel.AccountNumber = AccountNumber;
                ObjModel.BranchName = BranchName;
                ObjModel.IFSCCode = IFSCCode;

                BankMasterBal ObjBAL = new BankMasterBal();
                result = ObjBAL.InsertBankMaster(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result < 1)
                return msgIcon;


            msgIcon.Message = "Bank Add successfully";
            return msgIcon;
        }

        static bool IsValidInsert(string Month, string Year, decimal TargetAmount)
        {


            if (string.IsNullOrEmpty(Month))
            {
                return false;
            }
            if (string.IsNullOrEmpty(Year))
            {
                return false;
            }
            if (TargetAmount == 0)
            {
                return false;
            }

            return true;
        }
        [WebMethod]
        public static MessageWithIcon InsertTargetMaster(string Month, string Year, decimal TargetAmount)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            if (!IsValidInsert(Month, Year, TargetAmount))
            {
                msgIcon.Message = Message.InvalidData;
                return msgIcon;
            }
            msgIcon.Message = "Target Not Set";
            int result = -1;

            try
            {

                TargetMasterModel ObjModel = new TargetMasterModel();
                ObjModel.Month = Month;
                ObjModel.Year = Year;
                ObjModel.TargetAmount = TargetAmount;


                TargetMasterBal ObjBAL = new TargetMasterBal();
                result = ObjBAL.InsertTargetMaster(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result > 0)
            {
                msgIcon.Message = "Target Set successfully";
            }
            return msgIcon;
        }

        [WebMethod]
        public static TransactionMasterModel[] SelectUser()
        {
            AccessController.checkAccess(pageName);

            List<TransactionMasterModel> details = new List<TransactionMasterModel>();
            AccountBal ObjBAL = new AccountBal();

            try
            {
                details = ObjBAL.SelectUser();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }
        static bool IsValidInsert(string Date, string Name, string ReferenceNo, decimal Amount, string TransactionType, string PaymentType, string Remark, int BankMasterID,int IsGST)
        {


            if (string.IsNullOrEmpty(Name))
            {
                return false;
            }
            if (string.IsNullOrEmpty(ReferenceNo))
            {
                return false;
            }

            if (Amount == 0)
            {
                return false;
            }

            if (string.IsNullOrEmpty(TransactionType))
            {
                return false;
            }
            if (string.IsNullOrEmpty(PaymentType))
            {
                return false;
            } if (string.IsNullOrEmpty(Remark))
            {
                return false;
            }
            if (BankMasterID==0)
            {
                return false;

            }

            return true;
        }

        [WebMethod]
        public static MessageWithIcon InsertTransactionMaster(string Date, string Name, string ReferenceNo, decimal Amount, string TransactionType, string PaymentType, string Remark, int BankMasterID,int IsGST)
        {
            AccessController.checkAccess(pageName);



            MessageWithIcon msgIcon = new MessageWithIcon();
            if (!IsValidInsert(Date, Name, ReferenceNo, Amount, TransactionType, PaymentType, Remark, BankMasterID,IsGST))
            {
                msgIcon.Message = Message.InvalidData;
                return msgIcon;
            }

            msgIcon.Message = "Transaction not Added";
            int result = -1;

            try
            {

                TransactionMasterModel ObjModel = new TransactionMasterModel();
                ObjModel.Date = Date;
                ObjModel.Name = Name;
                ObjModel.ReferenceNo = ReferenceNo;
                ObjModel.Amount = Amount;
                ObjModel.TransactionType = TransactionType;
                ObjModel.PaymentType = PaymentType;
                ObjModel.Remark = Remark;
                ObjModel.BankMasterID = BankMasterID;
                ObjModel.IsGST = IsGST;


                AccountBal ObjBAL = new AccountBal();
                result = ObjBAL.InsertTransactionMaster(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result < 1)
                return msgIcon;


            msgIcon.Message = "Transaction Added Successfully With TransID : "+result;
            return msgIcon;
        }

        [WebMethod]
        public static BankMasterModel[] SelectBankMaster()
        {
            AccessController.checkAccess(pageName);
            List<BankMasterModel> details = new List<BankMasterModel>();
            BankMasterBal ObjBAL = new BankMasterBal();
            try
            {

                details = ObjBAL.SelectBankMaster();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static TargetMasterModel[] SelectTargetMaster()
        {
            AccessController.checkAccess(pageName);
            List<TargetMasterModel> details = new List<TargetMasterModel>();
            TargetMasterBal ObjBAL = new TargetMasterBal();
            try
            {

                details = ObjBAL.SelectTargetMaster();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static List<List<TransactionMasterModel>> SelectByID(int TransactionMasterID)
        {
            AccessController.checkAccess(pageName);

            List<List<TransactionMasterModel>> details = new List<List<TransactionMasterModel>>();
            AccountBal ObjBAL = new AccountBal();

            try
            {
                details = ObjBAL.SelectByID(TransactionMasterID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }

        [WebMethod]
        public static MessageWithIcon UpdateByID(int TransactionMasterID, string Name, string ReferenceNo, decimal Amount, string TransactionType, string PaymentType, string Remark, int BankMasterID)
        {
            AccountBal ObjBAL = new AccountBal();
            MessageWithIcon msgIcon = new MessageWithIcon();

            int Result = -1;
            try
            {
                TransactionMasterModel ObjModel = new TransactionMasterModel();
                ObjModel.TransactionMasterID = TransactionMasterID;
                ObjModel.Name = Name;
                ObjModel.ReferenceNo = ReferenceNo;
                ObjModel.Amount = Amount;
                ObjModel.TransactionType = TransactionType;
                ObjModel.PaymentType = PaymentType;
                ObjModel.Remark = Remark;
                ObjModel.BankMasterID = BankMasterID;

                Result = ObjBAL.UpdateByID(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (Result > 0)
                msgIcon.Message = " Transaction Updated successufully.";
            else
                msgIcon.Message = "Transaction Not updated";
            return msgIcon;

        }
    }
}