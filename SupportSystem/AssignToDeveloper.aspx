﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AssignToDeveloper.aspx.cs" Inherits="SupportSystem.AssignToDeveloper" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Assign To Developer
                                
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-3">
                <div class="form-group row mt-4">

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputTitle">Title</label>
                        <input type="text" id="inputTitle" class="form-control" />
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputEmployeeName">Employee Name</label>
                        <select id="inputEmployeeName" class="form-control loginput"></select>
                    </div>
                </div>
                <div class="form-group row mt-4">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputSystemName">System Name</label>

                        <select id="inputSystemName" class="form-control loginput"></select>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputAnyDeskID">TeamViewer/AnyDesk ID (Optional)</label>
                        <input type="text" id="inputAnyDeskID" class="form-control" placeholder="TeamViewer/AnyDesk ID" onkeypress="return IsAlphaNumeric(event)" />
                    </div>

                </div>

                <div class="form-group row mt-3">


                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputAdminRemark">Admin Remark  (Optional)</label>
                        <textarea class="form-control " style="height: 265px" id="inputAdminRemark" placeholder="Admin Remark"></textarea>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputDescription">Description</label>
                        <textarea id="inputDescription" style="height: 265px" class="form-control" placeholder="Description"></textarea>
                    </div>


                    <input type="text" class="form-control" id="AssignDatepicker" style="display: none" data-toggle="tooltip" data-placement="top" title="Assign Date" placeholder="Select Date" name="Date" />
                    <input type="text" class="form-control" id="TicketGenerationDate" style="display: none" name="TicketGenerationDate" />

                    <label id="TicketID" style="display: none"></label>
                    <label id="userID" style="display: none"></label>
                    <label id="TicketNo" style="display: none"></label>
                </div>

                <div class="form-group row mt-3">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" onclick="TicketInformationInsert()" title="Assign">Assign</button>
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton buttons" id="UpdateButton" onclick="" style="display: none" title="Update Ticket">Update</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" id="CancelButton" onclick="Reset()" title="Clear All">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>

    <script src="/JS/AssignToDeveloper.js"></script>
    <script>
        $(function () {
            $("#AssignDatepicker").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true
            });
            $("#AssignDatepicker").datepicker().datepicker("setDate", new Date());
        });

        $(document).ready(function () {

            UserInformationSelectByEmployeeID();
            SelectSystemMaster();
            if (window.location.href.indexOf("TicketID") > -1) {
                var url = window.location.href;
                var TicketID = url.split('=')
                TicketInformationSelectByTicketID(TicketID[1]);
            }
        });
    </script>
</asp:Content>
