﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class WebSubscriptionRenew : System.Web.UI.Page
    {
        private static string pageName = "WebSubscriptionRenew.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }


        [WebMethod]
        public static string SelectSubscriptionDetails(int SystemID)
        {
            AccessController.checkAccess(pageName);

               SubscritpionRenovationBal objBAL = new SubscritpionRenovationBal();
            try
            {
                int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                DataTable dt1 = new DataTable();
                AddWebSubscriptionBal ObjBal = new AddWebSubscriptionBal();
                dt1 = ObjBal.getEndDateForFinovativeBySystemID(SystemID);

                string SubscriptionEndDate = "";
                int remainingDays = 0;
                if (dt1.Rows.Count < 1)
                {
                    return "System Details not added for subscription ,Please contact to admin ";
                }
                DateTime EndDate = Convert.ToDateTime(dt1.Rows[0]["EndDate"]); 
                DateTime Today = DateTime.Now;
                remainingDays = (EndDate - Today).Days;
                SubscriptionEndDate = (EndDate.ToString("dd/MM/yyyy"));


                HttpContext.Current.Session["SubscriptionEndDate"] = SubscriptionEndDate.ToString();
                HttpContext.Current.Session["RemainingDays"] = remainingDays.ToString();
                DataTable dt = objBAL.SelectBySystemID(SystemID, UserID);
                if (dt.Rows.Count < 1)
                {
                    return "System Details not added for subscription ,Please contact to admin ";
                }
                HttpContext.Current.Session["WebURL"] = dt.Rows[0]["WebURL"].ToString();
             
                HttpContext.Current.Session["ProductID"] = dt1.Rows[0]["ProductID"].ToString();

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return "OK";
        }




    }
}