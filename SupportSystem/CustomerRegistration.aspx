﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CustomerRegistration.aspx.cs" Inherits="SupportSystem.CustomerRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Registration
                                <button class="btn btn-info buttons float-right" type="button" title="View TimeSheet" onclick="location.href='/UserInformation.aspx?UserTypeID=3'">Back to UserInformation</button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>

            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputName">Name</label>
                        <input type="text" id="inputName" title="Name" class="form-control loginput" onchange='$("#inputUserName").focus();' onmousedown=' $("#ErrorInputName").hide();' />
                        <label for="inputName" id="ErrorInputName" style="display: none; color: red">Name is required</label>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputUserName">UserName</label>
                        <input type="text" id="inputUserName" title="User Name" class="form-control loginput" onchange='$("#inputEmailID").focus();' onmousedown=' $("#ErrorInputUserName").hide();' />
                        <label for="inputUserName" id="ErrorInputUserName" style="display: none; color: red">Username is required</label>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputEmailID">Email ID</label>
                        <input type="text" id="inputEmailID" title="Email ID" class="form-control loginput" onchange='$("#inputMobileNumber").focus();' onmousedown=' $("#ErrorInputEmailID").hide();' />
                        <label for="inputEmailID" id="ErrorInputEmailID" style="display: none; color: red">Email id is required</label>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputMobileNumber">Mobile No</label>
                        <input type="text" id="inputMobileNumber" title="Mobile No" onmousedown=' $("#ErrorInputMobileNumber").hide();' onkeypress="return isNumberKey(event)" maxlength="10" class="form-control loginput" onchange='$("#inputPANNo").focus();' />
                        <label for="inputMobileNumber" id="ErrorInputMobileNumber" style="display: none; color: red">Mobile number is required</label>
                    </div>

                </div>
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputPANNo">PAN No</label>
                        <input type="text" class="form-control loginput" maxlength="10" onmousedown="$('#ErrorInputPANNo').hide()" id="inputPANNo" placeholder="PAN No"  title="PAN No" onkeypress="return isPANKey(event,'inputPANNo')" onchange='$("#inputGSTNo").focus();' />
                        <label for="inputPANNo" id="ErrorInputPANNo" style="display: none; color: red">PAN No is required</label>
                    </div>


                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputGSTNo">GST No</label>

                        <input type="text" class="form-control loginput" id="inputGSTNo" maxlength="15" placeholder="GST No" title="GST No" onkeypress="return isGSTKey(event,'inputGSTNo')" onchange='$("#inputState").focus();' />
                        <label for="inputGSTNo" id="ErrorInputGSTNo" style="display: none; color: red">PAN No is required</label>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputState">Select State</label>
                        <select id="inputState" class="form-control loginput" onchange="$('#ErrorInputState').hide()">
                            <option value="0">Select State</option>
                            <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                            <option value="Andhra Pradesh">Andhra Pradesh</option>
                            <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                            <option value="Assam">Assam</option>
                            <option value="Bihar">Bihar</option>
                            <option value="Chandigarh">Chandigarh</option>
                            <option value="Chhattisgarh">Chhattisgarh</option>
                            <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                            <option value="Daman and Diu">Daman and Diu</option>
                            <option value="Delhi">Delhi</option>
                            <option value="Goa">Goa</option>
                            <option value="Gujarat">Gujarat</option>
                            <option value="Haryana">Haryana</option>
                            <option value="Himachal Pradesh">Himachal Pradesh</option>
                            <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                            <option value="Jharkhand">Jharkhand</option>
                            <option value="Karnataka">Karnataka</option>
                            <option value="Kerala">Kerala</option>
                            <option value="Lakshadweep">Lakshadweep</option>
                            <option value="Madhya Pradesh">Madhya Pradesh</option>
                            <option value="Maharashtra">Maharashtra</option>
                            <option value="Manipur">Manipur</option>
                            <option value="Meghalaya">Meghalaya</option>
                            <option value="Mizoram">Mizoram</option>
                            <option value="Nagaland">Nagaland</option>
                            <option value="Orissa">Orissa</option>
                            <option value="Pondicherry">Pondicherry</option>
                            <option value="Punjab">Punjab</option>
                            <option value="Rajasthan">Rajasthan</option>
                            <option value="Sikkim">Sikkim</option>
                            <option value="Tamil Nadu">Tamil Nadu</option>
                            <option value="Tripura">Tripura</option>
                            <option value="Uttaranchal">Uttaranchal</option>
                            <option value="Uttar Pradesh">Uttar Pradesh</option>
                            <option value="West Bengal">West Bengal</option>
                        </select>
                        <label for="inputState" id="ErrorInputState" style="display: none; color: red">State is required</label>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12" style="display: none">
                        <label for="inputURL">URL</label>
                        <input type="text" id="inputURL" title="URL" class="form-control loginput" onchange='$("#SubmitButton").focus();' />
                    </div>
                </div>
                <label id="userID" style="display: none"></label>
                <div class="form-group row mt-3">

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" onclick="return UserInformationInsert();" title="Submit">Submit</button>
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return UserInformationUpdate();" id="UpdateButton" title="Update" style="display: none;">Update</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick="ResetAll();" title="Clear All">Cancel</button>
                            </div>
                        </div>
                    </div>


                </div>
            </form>
        </section>
    </div>
    <script src="JS/CustomerRegistration.js"></script>
    <script>

        $(document).ready(function () {
            UserInformationSelectByID();
        });

        function isNumberKey(e) {
            var r = e.which ? e.which : event.keyCode;
            return r > 31 && (48 > r || r > 57) && (e.which != 46 || $(this).val().indexOf('.') != -1) ? !1 : void 0
        }



        $(function () {
            $('#inputName').keydown(function (e) {
                {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 9) || (key == 11) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {

                        e.preventDefault();
                    }
                }
            });
        })
    </script>
</asp:Content>
