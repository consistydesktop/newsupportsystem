﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class ProjectAssignScreen : System.Web.UI.Page
    {
        private static string pageName = "ProjectAssignScreen.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
              
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static ProjectAssignScreenModel[] SelectServiceName()
        {
              
            AccessController.checkAccess(pageName);
            List<ProjectAssignScreenModel> details = new List<ProjectAssignScreenModel>();
            ProjectAssignScreenBal objBAL = new ProjectAssignScreenBal();
            try
            {
                details = objBAL.SelectServiceName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static ProjectAssignScreenModel[] SelectEmployeeName()
        {
              
            AccessController.checkAccess(pageName);
            List<ProjectAssignScreenModel> details = new List<ProjectAssignScreenModel>();
            ProjectAssignScreenBal objBAL = new ProjectAssignScreenBal();
            try
            {
               details = objBAL.SelectEmployeeName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static ProjectAssignScreenModel[] SelectProductName()
        {
              
            AccessController.checkAccess(pageName);
            List<ProjectAssignScreenModel> details = new List<ProjectAssignScreenModel>();
            ProjectAssignScreenBal objBAL = new ProjectAssignScreenBal();
            try
            {
                details = objBAL.SelectProductName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Insert(List<ProjectAssignScreenModel> ObjModel)
        {
              
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = -1;

            ProjectAssignScreenBal objBal = new ProjectAssignScreenBal();
            try
            {
                i = objBal.Insert(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 1)
                msgIcon.Message = "Project not Assigned";
            else
            {
                foreach (var item in ObjModel)
                {
                    int j = 0;
                    String Files = item.FileIDs;
                    string[] fileID = Files.Split('#');

                    if (fileID.Length < 1)
                        break;
                    for (j = 1; j < fileID.Length; j++)
                    {
                        var currentFileID = fileID[j];
                        item.FileID = Convert.ToInt32(currentFileID);
                        item.ProjectDetailMasterID = i;
                        item.UploadType = UploadTypeMaster.UploadProject;
                        ProjectAssignScreenBal ObjBal = new ProjectAssignScreenBal();
                        try
                        {
                            ObjBal.FileMappingInsert(item);
                        }
                        catch (Exception Ex) { new Logger().write(Ex); }
                    }
                }
                msgIcon.Message = "Project Assigned successfully";
           
            }
            return msgIcon;
        }

        [WebMethod]
        public static List<ProjectAssignScreenModel> SelectByID(int ProjectDetailMasterID)
        {
              
            AccessController.checkAccess(pageName);
            List<ProjectAssignScreenModel> Register = new List<ProjectAssignScreenModel>();
            ProjectAssignScreenBal objBal = new ProjectAssignScreenBal();
            try
            {
                Register = objBal.SelectByID(ProjectDetailMasterID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Register;
        }

        [WebMethod]
        public static MessageWithIcon Update(List<ProjectAssignScreenModel> ObjModel)
        {
              
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            ProjectAssignScreenBal objBAL = new ProjectAssignScreenBal();
            try
            {
                i = objBAL.Update(ObjModel);
            }
            catch (Exception Ex) { new Logger().write(Ex); }

            if (i < 1)
                msgIcon.Message = "Project not updated";
            else
            {
                foreach (var item in ObjModel)
                {
                    int j = 0;
                    String Files = item.FileIDs;
                    string[] fileID = Files.Split('#');
                    if (fileID.Length < 1)
                        break;
                    for (j = 1; j < fileID.Length; j++)
                    {
                        var currentFileID = fileID[j];
                        item.FileID = Convert.ToInt32(currentFileID);
                        item.ProjectDetailMasterID = i;
                        item.UploadType = UploadTypeMaster.UploadProject;
                        ProjectAssignScreenBal ObjBal = new ProjectAssignScreenBal();
                        try
                        {
                            ObjBal.FileMappingInsert(item);
                        }
                        catch (Exception Ex) { new Logger().write(Ex); }
                    }
                }
                msgIcon.Message = "Project updated successfully";
              
            }
            return msgIcon;
        }


        [WebMethod]
        public static ProjectAssignScreenModel[] SelectAttachment(int ProjectDetailMasterID)
        {
            AccessController.checkAccess(pageName);

            List<ProjectAssignScreenModel> details = new List<ProjectAssignScreenModel>();
            ProjectAssignScreenBal ObjBAL = new ProjectAssignScreenBal();
            try
            {
                details = ObjBAL.SelectAttachment(ProjectDetailMasterID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }
   
    }
}