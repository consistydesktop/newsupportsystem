﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class CustomerRegistration : System.Web.UI.Page
    {
        private static string pageName = "CustomerRegistration.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #region Insert

        public static bool ValidInsert(string Name, string Username, string EmailID, string MobileNumber)
        {
            Validation validation = new Validation();
            if (!validation.IsValidString(Name))
                return false;
            if (!validation.IsValidString(Username))
                return false;
            if (!validation.IsValidString(EmailID))
                return false;
            if (!validation.IsEmailValid(EmailID))
                return false;
            if (!validation.IsValidString(MobileNumber))
                return false;
            return true;
        }

        [WebMethod]
        public static MessageWithIcon Insert(string Name, string Username, string EmailID, string MobileNumber)
        {
            MessageWithIcon msgIcon = new MessageWithIcon();
            CustomerRegistrationModel objModel = new CustomerRegistrationModel();

            int i = 0;

            if (!ValidInsert(Name, Username, EmailID, MobileNumber))
                msgIcon.Message = "Missing paramter";

            else
            {
                objModel.Name = Name;
                objModel.Username = Username;
                objModel.EmailID = EmailID;
                objModel.UserTypeID = RoleMaster.Customer;
                objModel.MobileNumber = MobileNumber;

                CustomerRegistrationBal objBAL = new CustomerRegistrationBal();
                try
                {
                    i = objBAL.Insert(objModel);
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }
                if (i > 0)
                    msgIcon.Message = "Customer Added Successfully";
                else
                    msgIcon.Message = "Duplicate Customer";
            }
            return msgIcon;
        }

        #endregion Insert

        [WebMethod]
        public static CustomerRegistrationModel SelectByID(int UserID)
        {

            AccessController.checkAccess(pageName);
            CustomerRegistrationModel Register = new CustomerRegistrationModel();

            CustomerRegistrationBal objBal = new CustomerRegistrationBal();
            try
            {
                Register = objBal.SelectByID(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Register;
        }

        [WebMethod]
        public static MessageWithIcon Update(
            string Name,
            string Username,
            string EmailID,
            string MobileNumber,
            int UserID,
            string PANNo,
            string GSTNo,
            string State)
        {

            AccessController.checkAccess(pageName);
            CustomerRegistrationModel objModel = new CustomerRegistrationModel();
            MessageWithIcon msgIcon = new MessageWithIcon();

            int i = 0;

            objModel.Name = Name;
            objModel.Username = Username;
            objModel.EmailID = EmailID;
            objModel.UserID = UserID;
            objModel.MobileNumber = MobileNumber;
            objModel.PANNo = PANNo;
            objModel.GSTNo = GSTNo;
            objModel.State = State;

            CustomerRegistrationBal objBAL = new CustomerRegistrationBal();
            try
            {
                i = objBAL.Update(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "Duplicate user";
            else
                msgIcon.Message = "User data updated successfully";

            return msgIcon;
        }

    }
}