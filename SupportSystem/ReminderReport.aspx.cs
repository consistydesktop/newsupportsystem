﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class ReminderReport : System.Web.UI.Page
    {
       static  string pageName = "ReminderReport.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
              
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static ReminderModel[] Search(string FromDate, string ToDate, string Status)
        {
              
            AccessController.checkAccess(pageName);

            List<ReminderModel> details = new List<ReminderModel>();
            ReminderBal ObjBAL = new ReminderBal();

            ReminderModel ObjModel = new ReminderModel();

            ObjModel.FormDate = FromDate;
            ObjModel.Todate = ToDate;
            ObjModel.Status = Status;
            try
            {
                details = ObjBAL.Search(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static ReminderModel[] Select()
        {
              
            AccessController.checkAccess(pageName);

            List<ReminderModel> details = new List<ReminderModel>();
            ReminderBal ObjBAL = new ReminderBal();

            try
            {
                details = ObjBAL.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Delete(int ReminderMasterID)
        {
            AccessController.checkAccess(pageName);
            int result = -1;
            MessageWithIcon msgIcon = new MessageWithIcon();
            msgIcon.Message = "Reminder not Deleted";
            ReminderBal ObjBAL = new ReminderBal();

            try
            {
                result = ObjBAL.Delete(ReminderMasterID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (result > 0)
                msgIcon.Message = "Reminder Deleted successfully";



            return msgIcon;
        }
    }
}