﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class AddWebSubscription : System.Web.UI.Page
    {
        private static string pageName = "AddWebSubscription.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static AddWebSubscriptionModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);

            List<AddWebSubscriptionModel> details = new List<AddWebSubscriptionModel>();
            AddWebSubscriptionBal objBAL = new AddWebSubscriptionBal();
            try
            {
                details = objBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Insert(
            int SystemID,
            string WebsiteURL,
            string HostingIP,
            string EndDate,
            string PaymentStatus,
            string Description)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            AddWebSubscriptionModel objModel = new AddWebSubscriptionModel();

            int i = -1;
             string host="";


             if (WebsiteURL == "")
             {
                 msgIcon.Message = "URL cannot be blank.";
                 return msgIcon;
             }

            //try
            //{
            //    Uri myUri = new Uri(WebsiteURL);
            //    host = myUri.Host;
            //}
            //catch (Exception ex) { new Logger().write(ex); }

            objModel.SystemID = SystemID;
            objModel.WebsiteURL = WebsiteURL;
            objModel.HostingIP = HostingIP;
            objModel.EndDate = Convert.ToDateTime(EndDate);
            objModel.PaymentStatus = PaymentStatus;
            objModel.Description = Description;
            AccessController ac = new AccessController();
            objModel.Token = ac.getRandomToken();
            AddWebSubscriptionBal objBAL = new AddWebSubscriptionBal();
            try
            {
                i = objBAL.Insert(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "Already exist system .";
            else
                msgIcon.Message = "Subscription added successfully.";
            return msgIcon;
        }


        [WebMethod]
        public static AddWebSubscriptionModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<AddWebSubscriptionModel> details = new List<AddWebSubscriptionModel>();

            AddWebSubscriptionBal objBAL = new AddWebSubscriptionBal();
            try
            {
                details = objBAL.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }



        [WebMethod]
        public static AddWebSubscriptionModel SelectByID(int WebSubscriptionID)
        {
            AccessController.checkAccess(pageName);
            AddWebSubscriptionModel details = new AddWebSubscriptionModel();

            AddWebSubscriptionBal objBAL = new AddWebSubscriptionBal();
            try
            {
                details = objBAL.SelectByID(WebSubscriptionID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }



        [WebMethod]
        public static MessageWithIcon Delete(int WebSubscriptionID)
        {
            AccessController.checkAccess(pageName);
            int Result = -1;
            MessageWithIcon msgIcon = new MessageWithIcon();
            AddWebSubscriptionBal objBAL = new AddWebSubscriptionBal();
            try
            {
                Result = objBAL.Delete(WebSubscriptionID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (Result > 0)
            {
                msgIcon.Message = "Subscription data deleted successfully.";
            }
            else
            {
                msgIcon.Message = "Subscription data not deleted.";
            }

            return msgIcon;
        }


        [WebMethod]
        public static MessageWithIcon Update(
            int WebSubscriptionID,
            int SystemID,
            string WebsiteURL,
            string HostingIP,
            string EndDate,
            string PaymentStatus,
            string Description)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();

            if (WebsiteURL == "")
            {
                msgIcon.Message = "URL cannot be blanck.";
                return msgIcon;
            }

            string host = "";
            try
            {
                Uri myUri = new Uri(WebsiteURL);
                host = myUri.Host;
            }
            catch (Exception ex) { new Logger().write(ex); }

            
            AddWebSubscriptionModel objModel = new AddWebSubscriptionModel();

            int i = -1;

            objModel.SubscriptionID = WebSubscriptionID;
            objModel.SystemID = SystemID;
            objModel.WebsiteURL = WebsiteURL;
            objModel.HostingIP = HostingIP;
            objModel.EndDate = Convert.ToDateTime(EndDate);
            objModel.PaymentStatus = PaymentStatus;
            objModel.Description = Description;

            AddWebSubscriptionBal objBAL = new AddWebSubscriptionBal();
            try
            {
                i = objBAL.Update(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "Subscription not updated.";
            else
                msgIcon.Message = "Subscription updated successfully.";
            return msgIcon;
        }

    }
}