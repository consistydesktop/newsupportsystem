﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Text;
using System.Web;

namespace SupportSystem
{
    public partial class Captcha : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Clear();
            captcha();
        }

        public void captcha()
        {
            int iHeight = 80;
            int iWidth = 190;
            Bitmap oOutputBitmap = new Bitmap(iWidth, iHeight, PixelFormat.Format24bppRgb);
            //    Bitmap objBitmap = new Bitmap(iWidth, iHeight);
            Graphics objGraphics = Graphics.FromImage(oOutputBitmap);

            int[] aBackgroundNoiseColor = new int[] { 150, 150, 150 };
            int[] aTextColor = new int[] { 0, 0, 0 };
            int[] aFontEmSizes = new int[] { 20, 22, 25, 27 };

            string[] aFontNames = new string[]
            {
             "Comic Sans MS",
             "Arial",
             "Times New Roman",
             "Georgia",
             "Verdana",
             "Geneva"
            };
            FontStyle[] aFontStyles = new FontStyle[]
            {
             FontStyle.Bold,
             FontStyle.Italic,
             FontStyle.Regular,
             FontStyle.Strikeout,
             FontStyle.Underline
            };

            objGraphics.Clear(Color.White);
            Random objRandom = new Random();
            objGraphics.DrawLine(Pens.Black, objRandom.Next(0, 50), objRandom.Next(10, 30), objRandom.Next(0, 200), objRandom.Next(0, 50));
            objGraphics.DrawRectangle(Pens.Blue, objRandom.Next(0, 20), objRandom.Next(0, 20), objRandom.Next(50, 80), objRandom.Next(0, 20));
            objGraphics.DrawLine(Pens.Blue, objRandom.Next(0, 20), objRandom.Next(10, 50), objRandom.Next(100, 200), objRandom.Next(0, 80));
            Brush objBrush =
                default(Brush);
            //create background style
            HatchStyle[] aHatchStyles = new HatchStyle[]
            {
                HatchStyle.BackwardDiagonal, HatchStyle.Cross, HatchStyle.DashedDownwardDiagonal, HatchStyle.DashedHorizontal, HatchStyle.DashedUpwardDiagonal, HatchStyle.DashedVertical,
                    HatchStyle.DiagonalBrick, HatchStyle.DiagonalCross, HatchStyle.Divot, HatchStyle.DottedDiamond, HatchStyle.DottedGrid, HatchStyle.ForwardDiagonal, HatchStyle.Horizontal,
                    HatchStyle.HorizontalBrick, HatchStyle.LargeCheckerBoard, HatchStyle.LargeConfetti, HatchStyle.LargeGrid, HatchStyle.LightDownwardDiagonal, HatchStyle.LightHorizontal
            };
            //create rectangular area
            RectangleF oRectangleF = new RectangleF(0, 0, 300, 300);
            objBrush = new HatchBrush(aHatchStyles[objRandom.Next(aHatchStyles.Length - 3)], Color.FromArgb((objRandom.Next(100, 255)), (objRandom.Next(100, 255)), (objRandom.Next(100, 255))), Color.White);
            objGraphics.FillRectangle(objBrush, oRectangleF);

            string combination = "0123456789";
            Random random = new Random();
            StringBuilder captcha = new StringBuilder();

            for (int i = 0; i < 4; i++)
                captcha.Append(combination[random.Next(combination.Length)]);

            //   HttpContext.Current.Session["captcha"] = captcha.ToString();

            Session["captcha"] = captcha.ToString();
            string captchaText = captcha.ToString();

            //Generate the image for captcha
            //   string captchaText = string.Format("{0:X}", objRandom.Next(1000000, 9999999));
            //add the captcha value in session
            Font objFont = new Font("Courier New", 20, FontStyle.Bold);
            //Draw the image for captcha
            System.Drawing.Drawing2D.Matrix oMatrix = new System.Drawing.Drawing2D.Matrix();
            Random oRandom = new Random();
            for (int i = 0; i <= captchaText.Length - 1; i++)
            {
                oMatrix.Reset();
                int iChars = captchaText.Length;
                int x = iWidth / (iChars + 1) * i;
                int y = iHeight / 2;

                oMatrix.RotateAt(oRandom.Next(-40, 40), new PointF(x, y));
                objGraphics.Transform = oMatrix;

                objGraphics.DrawString
                (
                    // Text
                captchaText.Substring(i, 1),
                    //Random Font Name and Style
                new Font(aFontNames[oRandom.Next(aFontNames.Length - 1)],
                   aFontEmSizes[oRandom.Next(aFontEmSizes.Length - 1)],
                   aFontStyles[oRandom.Next(aFontStyles.Length - 1)]),
                    //Random Color (Darker colors RGB 0 to 100)
                new SolidBrush(Color.FromArgb(oRandom.Next(0, 100),
                   oRandom.Next(0, 100), oRandom.Next(0, 100))),
                x,
                oRandom.Next(10, 40)
                );
                objGraphics.ResetTransform();
            }
            //    objGraphics.DrawString(captchaText, objFont, Brushes.Black, 20, 20);
            oOutputBitmap.Save(HttpContext.Current.Response.OutputStream, ImageFormat.Gif);
        }
    }
}