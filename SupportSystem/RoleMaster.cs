﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportSystem
{
    public class RoleMaster
    {
       public const int Admin = 1;
       public const int Employee = 2;
       public const int Customer = 3;
       public const int Support = 4;
    }
}