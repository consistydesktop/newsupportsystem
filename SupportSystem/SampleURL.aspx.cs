﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

namespace SupportSystem
{
    public partial class SampleURL : System.Web.UI.Page
    {
        private static string pageName = "SampleURL.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }



        static bool IsValidInsert( string ServiceName, string URL, string DateFormat, string URLType, string MethodType)
        {

          
            if (string.IsNullOrEmpty(ServiceName))
            {
                return false;
            }
            if (string.IsNullOrEmpty(URL))
            {
                return false;
            }
            if (string.IsNullOrEmpty(DateFormat))
            {
                return false;
            }
            if (string.IsNullOrEmpty(URLType))
            {
                return false;
            }
            if (string.IsNullOrEmpty(MethodType))
            {
                return false;
            }
            
            return true;
        }

        [WebMethod]
        public static MessageWithIcon Insert(int ProviderID, int ServiceID, string ServiceName, string URL,   string DateFormat, string URLType, string MethodType, string HeaderJSON)
        {
            AccessController.checkAccess(pageName);



            MessageWithIcon msgIcon = new MessageWithIcon();
            if (!IsValidInsert( ServiceName, URL, DateFormat, URLType, MethodType))
            {
                msgIcon.Message = Message.InvalidData;
                return msgIcon;
            }

            msgIcon.Message = "URL details not added";
            int result = -1;

            try
            {

                SampleURLViewModel ObjModel = new SampleURLViewModel();
                ObjModel.ProviderID = ProviderID;
                ObjModel.ServiceID = ServiceID;
                ObjModel.ServiceName = ServiceName;
                ObjModel.URL = URL;
                ObjModel.DateFormat = DateFormat;
                ObjModel.URLType = URLType;
                ObjModel.MethodType = MethodType;
                ObjModel.HeaderJSON = HeaderJSON;


                FinovativeSampleURLBal ObjBAL = new FinovativeSampleURLBal();
                result = ObjBAL.Insert(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result < 1)
                return msgIcon;


            msgIcon.Message = " Sample URL details added successfully";
            return msgIcon;
        }


        [WebMethod]
        public static SampleURLViewModel[] SelectBYProviderID(int ProviderID)
        {
            AccessController.checkAccess(pageName);
            List<SampleURLViewModel> details = new List<SampleURLViewModel>();
            DataTable dt = new DataTable();

            try
            {
                Model.VersionHistoryReport objModel = new Model.VersionHistoryReport();
                FinovativeSampleURLBal objBAL = new FinovativeSampleURLBal();

                dt = objBAL.SelectBYProviderID(ProviderID);
                if (dt.Rows.Count <= 0)
                {
                    return details.ToArray();
                }
                foreach (DataRow dtrow in dt.Rows)
                {

                    Model.SampleURLViewModel report = new Model.SampleURLViewModel();
                    report.SampleURLID = Convert.ToInt32(dtrow["SampleURLID"].ToString());
                    report.DeveloperName = (dtrow["DeveloperName"].ToString());
                    report.ServiceName = (dtrow["ServiceName"].ToString());
                    report.URL = (dtrow["URL"].ToString());
                    report.URLType = (dtrow["URLType"].ToString());
                    report.MethodType = (dtrow["MethodType"].ToString());
                    report.HeaderJSON = (dtrow["HeaderJSON"].ToString());
                    report.DateFormat = (dtrow["DateFormat"].ToString());
                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Model.Logger().write(ex);
            }
            return details.ToArray();
        }



        [WebMethod]
        public static SampleURLViewModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<SampleURLViewModel> details = new List<SampleURLViewModel>();
            DataTable dt = new DataTable();

            try
            {
                Model.VersionHistoryReport objModel = new Model.VersionHistoryReport();
                FinovativeSampleURLBal objBAL = new FinovativeSampleURLBal();

                dt = objBAL.selectSampleURLReport();
                if (dt.Rows.Count <= 0)
                {
                    return details.ToArray();
                }
                foreach (DataRow dtrow in dt.Rows)
                {

                    Model.SampleURLViewModel report = new Model.SampleURLViewModel();
                    report.SampleURLID = Convert.ToInt32(dtrow["SampleURLID"].ToString());
                    report.DeveloperName = (dtrow["DeveloperName"].ToString());
                    report.ServiceName = (dtrow["ServiceName"].ToString());
                    report.URL = (dtrow["URL"].ToString());
                    report.URLType = (dtrow["URLType"].ToString());
                    report.MethodType = (dtrow["MethodType"].ToString());
                    report.HeaderJSON = (dtrow["HeaderJSON"].ToString());
                    report.DateFormat = (dtrow["DateFormat"].ToString());
                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Model.Logger().write(ex);
            }
            return details.ToArray();
        }


        [WebMethod]
        public static SampleURLViewModel SelectByID(int SampleURLID)
        {
            AccessController.checkAccess(pageName);
            SampleURLViewModel report = new SampleURLViewModel();

            FinovativeSampleURLBal objBal = new FinovativeSampleURLBal();
            try
            {
                report = objBal.SelectByID(SampleURLID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return report;
        }

        [WebMethod]
        public static MessageWithIcon Update(int SampleURLID, int ProviderID, int ServiceID  ,string ServiceName, string URL, string DateFormat, string URLType, string MethodType, string HeaderJSON)
        {
            AccessController.checkAccess(pageName);



            MessageWithIcon msgIcon = new MessageWithIcon();
            if (!IsValidInsert( ServiceName, URL, DateFormat, URLType, MethodType))
            {
                msgIcon.Message = Message.InvalidData;
                return msgIcon;
            }

            msgIcon.Message = "URL details not updated";
            int result = -1;

            try
            {

                SampleURLViewModel ObjModel = new SampleURLViewModel();
                ObjModel.SampleURLID = SampleURLID;
                ObjModel.ProviderID = ProviderID;
                ObjModel.ServiceID = ServiceID;

                ObjModel.ServiceName = ServiceName;
                ObjModel.URL = URL;
                ObjModel.DateFormat = DateFormat;
                ObjModel.URLType = URLType;
                ObjModel.MethodType = MethodType;
                ObjModel.HeaderJSON = HeaderJSON;


                FinovativeSampleURLBal ObjBAL = new FinovativeSampleURLBal();
                result = ObjBAL.Update(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result < 1)
                return msgIcon;


            msgIcon.Message = " Sample URL details updated successfully";
            return msgIcon;
        }



        [WebMethod]
        public static MessageWithIcon Delete(int SampleURLID)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            FinovativeSampleURLBal objBal = new FinovativeSampleURLBal();
            try
            {
                i = objBal.Delete(SampleURLID);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i > 0)
                msgIcon.Message = "SampleURL not deleted";
            else
                msgIcon.Message = "SampleURL deleted Successfully";
            return msgIcon;
        }

    }
}