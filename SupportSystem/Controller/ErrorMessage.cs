﻿namespace SupportProjectEmployee.Controller
{
    public class ErrorMessage
    {
        public int Missing = -1;
        public int Duplicate = -3;
        public int NotValid = -2;
        public int NotCorrect = -4;
        public int success = 2;
        public int failed = -5;
    }
}