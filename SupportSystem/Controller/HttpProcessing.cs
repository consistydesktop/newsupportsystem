﻿
using Model;
using RestSharp;
using SupportSystem.PG;
using System;

using System.IO;
using System.Net;
using System.Text;


namespace SupportSystem.Controller
{
    public class HttpProcessing
    {
        public string postMethod(string requestUrl)
        {
            string result = string.Empty;
            try
            {
                //------22/05/2019  added for issues in secure and unsecure site callback
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                // ---------------------------------------------------------------

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
                request.Timeout = 200 * 1000;
                request.Credentials = CredentialCache.DefaultCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                result = readStream.ReadToEnd();
                response.Close();
                readStream.Close();
            }
            catch (Exception ex)
            {
                Logger log = new Logger();
                log.write(ex);
                new Logger().LogInfo("HttpProcessing", "postMethod", requestUrl);
            }
            return result;
        }


        public string PostMethodForPayOut(string body)
        {
            string result = string.Empty;

            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var client = new RestClient("https://api.razorpay.com/v1/orders");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                String encoded = System.Convert.ToBase64String(
                                      System.Text.Encoding.GetEncoding("UTF-8").GetBytes(RazorpayInfo.RazorpayKey + ":" + RazorpayInfo.RazorpaySecret));
                request.AddHeader("content-type", "application/json");
                request.AddHeader("Authorization", "Basic " + encoded);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                result = response.Content;
            }
            catch (Exception ex)
            {
                Logger log = new Logger();
                log.write(ex);
                          }
            return result;
        }


        internal string GetMethodForPayment(string body)
        {
            string result = string.Empty;

            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string URL = "https://api.razorpay.com/v1/payments/" + body;
                var client = new RestClient(URL);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                String encoded = System.Convert.ToBase64String(
                                      System.Text.Encoding.GetEncoding("UTF-8").GetBytes(RazorpayInfo.RazorpayKey + ":" + RazorpayInfo.RazorpaySecret));
                request.AddHeader("content-type", "application/json");
                request.AddHeader("Authorization", "Basic " + encoded);
                IRestResponse response = client.Execute(request);
                result = response.Content;
            }
            catch (Exception ex)
            {
                Logger log = new Logger();
                log.write(ex);
                      }
            return result;
        }

        public string GetMethodForPayOut(string body)
        {
            string result = string.Empty;

            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string URL = "https://api.razorpay.com/v1/orders/" + body + "/payments";
                var client = new RestClient(URL);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                String encoded = System.Convert.ToBase64String(
                                      System.Text.Encoding.GetEncoding("UTF-8").GetBytes(RazorpayInfo.RazorpayKey + ":" + RazorpayInfo.RazorpaySecret));
                request.AddHeader("content-type", "application/json");
                request.AddHeader("Authorization", "Basic " + encoded);
                IRestResponse response = client.Execute(request);
                result = response.Content;
            }
            catch (Exception ex)
            {
                Logger log = new Logger();
                log.write(ex);
                //Logger.LogInfo("HttpProcessing", "EpocketAPIPostMethod", URL + ", body:" + body + ", Header:" + Header);
            }
            return result;
        }

    }
}