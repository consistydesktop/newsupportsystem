﻿using Model;
using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace SupportProjectEmployee.Controller
{
    public class Validation
    {
        public bool IsValidString(string inStr)
        {
            if (string.IsNullOrEmpty(inStr) || string.IsNullOrWhiteSpace(inStr))
                return false;

            if (inStr.Trim().Length <= 0)
                return false;

            if (inStr == "")
                return false;
            return true;
        }

        public bool IsValidInt(int inInt)
        {
            if (inInt == null)
                return false;

            if (inInt <= 0)
                return false;

            return true;
        }

        public bool IsValidDecimal(decimal inDec)
        {
            if (inDec == null)
                return false;

            if (inDec <= 0)
                return false;

            return true;
        }

        public bool IsEmailValid(string email)
        {
            string expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, string.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool IsNullOrEmptyOrWhiteSpace(string str)
        {
            if (string.IsNullOrEmpty(str) ||
                string.IsNullOrWhiteSpace(str))
            {
                return true;
            }
            return false;
        }

        public static bool MobileLength(string str)
        {
            if (str.Length != 10)
            {
                return true;
            }
            return false;
        }


        public static bool IsValidImageType(string extension)
        {

            if (extension.ToLower().EndsWith(".png"))
            {
                return true;
            }
            if (extension.ToLower().EndsWith(".jpg"))
            {
                return true;
            }
            if (extension.ToLower().EndsWith(".jpeg"))
            {
                return true;
            }
            if (extension.ToLower().EndsWith(".pdf"))
            {
                return true;
            }
            return false;
        }



        public static bool IsValidAndroidSize(int FileSize)
        {
            if (FileSize > 1048576) //1mb
                return false;

            return true;

        }

        public static bool IsValidAndroidFileType(string extension)
        {

            if (extension.ToLower().EndsWith(".keystore"))
            {
                return true;
            }
            if (extension.ToLower().EndsWith(".jks"))
            {
                return true;
            }
           


            return false;

        }


        public static bool IsValidImageSize(int FileSize)
        {
            if (FileSize > 2097152) //2mb
                return false;

            return true;

        }

        public static bool IsValidExcelSize(int FileSize)
        {
            if (FileSize > 5242880) //5mb
                return false;

            return true;

        }
        public static bool IsValidExcelType(string extension)
        {
            if (extension.ToLower().Contains("xls".ToLower()))
                return true;


            if (extension.ToLower().Contains("xlsx".ToLower()))
                return true;


            return false;

        }
        public  string AmountInIndianCurrency(decimal Balance)
        {
            string Bal = "0";
            System.Globalization.NumberFormatInfo format = new System.Globalization.NumberFormatInfo();
            int[] indSizes = { 3, 2, 2 };
            format.CurrencyDecimalDigits = 2;
            format.CurrencyDecimalSeparator = ".";
            format.CurrencyGroupSeparator = ",";
            format.CurrencySymbol = "";
            format.CurrencyGroupSizes = indSizes;
            Bal = Balance.ToString("C", format);
            return Bal;
        }

        public DateTime ParseRequestDate(string StrDate)
        {
            try
            {
                CultureInfo enUS = new CultureInfo("en-US");
                var dt = StrDate;
                //30/03/2019
                //15-04-2019
                //23/04/2019 12:38:35 PM

                DateTime dateValue;

                // Scenario #1
                if (DateTime.TryParseExact(dt, "dd/MM/yyyy", enUS, DateTimeStyles.None, out dateValue))
                    return dateValue;

                // Scenario #2
                if (DateTime.TryParseExact(dt, "dd/MM/yyyy hh:mm:ss tt", enUS, DateTimeStyles.None, out dateValue))
                    return dateValue;

                // Scenario #3
                if (DateTime.TryParseExact(dt, "dd-MM-yyyy", enUS, DateTimeStyles.None, out dateValue))
                    return dateValue;

                // Scenario #4
                if (DateTime.TryParseExact(dt, "dd-MM-yyyy hh:mm:ss tt", enUS, DateTimeStyles.None, out dateValue))
                    return dateValue;

                // Scenario #5
                if (DateTime.TryParseExact(dt, "dd-MM-yyyy hh:mm:ss tt", enUS, DateTimeStyles.AdjustToUniversal, out dateValue))
                    return dateValue;

                // Scenario #6
                if (DateTime.TryParseExact(dt, "MM/dd/yyyy HH:mm:ss", enUS, DateTimeStyles.None, out dateValue))
                    return dateValue;

                // Scenario #7
                if (DateTime.TryParseExact(dt, "M/dd/yyyy hh:mm", enUS, DateTimeStyles.None, out dateValue))
                    return dateValue;

                // Scenario #8
                if (DateTime.TryParseExact(dt, "MM/dd/yyyy hh:mm", enUS, DateTimeStyles.None, out dateValue))
                    return dateValue;

                // Scenario #9
                if (DateTime.TryParseExact(dt, "MM/dd/yyyy hh:mm:ss tt zzz", enUS, DateTimeStyles.None, out dateValue))
                    return dateValue;

                // Scenario #10
                if (DateTime.TryParseExact(dt, "MM/dd/yyyy hh:mm:ss tt zzz", enUS, DateTimeStyles.AdjustToUniversal, out dateValue))
                    return dateValue;

                // Scenario #11
                if (DateTime.TryParseExact(dt, "MM/dd/yyyy hh:mm", enUS, DateTimeStyles.None, out dateValue))
                    return dateValue;

                // Scenario #12
                if (DateTime.TryParseExact(dt, "MM/dd/yyyy hh:mm", enUS, DateTimeStyles.None, out dateValue))
                    return dateValue;
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
           
            }
            return Convert.ToDateTime(StrDate);
        }
    }
}