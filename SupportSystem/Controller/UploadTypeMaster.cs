﻿namespace SupportProjectEmployee.Controller
{
    public class UploadTypeMaster
    {
        public static string UploadProject = "Project";
        public static string UploadTicketAdmin = "TicketByAdmin";
        public static string UploadTicketDeveloper = "TicketByDeveloper";
        public static string UploadTicketClient = "TicketByCustomer";

        public static string UploadBug = "Bug";

        public static string UploadTask = "Task";
        public static string UploadTaskResolved = "Resolved";

        public static string UploadAndroidData = "AndroidData";
    }
}