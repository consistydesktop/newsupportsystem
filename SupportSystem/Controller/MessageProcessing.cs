﻿using Model;
using SupportSystem.StaticClass;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace SupportProjectEmployee.Controller
{
    public class MessageProcessing
    {
        #region SMS

        public static string SendSMS(string MobileNo, string Message)//Create in Common Function
        {
            string strUrl = string.Empty;
            string dataString = string.Empty;
           
            try
            {
                MessageProcessing cc = new MessageProcessing();
                strUrl = cc.getSmsURL(MobileNo, Message);

                WebRequest request = HttpWebRequest.Create(strUrl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                dataString = readStream.ReadToEnd();
                response.Close();
                s.Close();
                readStream.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dataString;
        }

        public string getSmsURL(string mobileNo, string Message)
        {
            //string smsurl = "http://49.50.67.32/smsapi/httpapi.jsp?username=Consisty&password=Consisty&from=CONSTY&to={0}&text={1}&coding=0";

            string smsurl = System.Configuration.ConfigurationSettings.AppSettings["SMSURL"].ToString();
            string url = String.Format(smsurl, mobileNo, Message);
            return url;

        }

        #endregion SMS

        #region Email

        public static int SendEmail(string EmailID, string Message, string Subject)
        {
            int i = 0;
            string fromAddress = SecureParam.EmailID;
            string fromPassword = SecureParam.Password;

            using (System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage())
            {
                try
                {
                    mailMessage.From = new MailAddress(fromAddress, "Consisty Support System");
                    mailMessage.Subject = Subject;
                    mailMessage.Body = Message;
                    mailMessage.IsBodyHtml = true;

                    mailMessage.To.Add(EmailID);
                    SmtpClient smtp = new SmtpClient();
                    smtp.EnableSsl = true;
                    smtp.Host = "smtp.gmail.com";
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = fromAddress;
                    NetworkCred.Password = fromPassword;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = int.Parse("587");
                    smtp.Send(mailMessage);
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }
                return i;
            }
        }

        #endregion Email

        public static string SendPassword(string MobileNo, string Password)
        {
            string strUrl = string.Empty;
            string dataString = string.Empty;
            Password = "Welcome to consisty system! Your password for Consisty support system is " + Password;
            try
            {
                MessageProcessing cc = new MessageProcessing();
                strUrl = cc.getSmsURLForPassword(MobileNo, Password);

                WebRequest request = HttpWebRequest.Create(strUrl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                dataString = readStream.ReadToEnd();
                response.Close();
                s.Close();
                readStream.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dataString;
        }

        public string getSmsURLForPassword(string mobileNo, string Password)
        {
            string smsurl = "http://49.50.67.32/smsapi/httpapi.jsp?username=Consisty&password=Consisty&from=CONSTY&to={0}&text={1}&coding=0";

            string url = String.Format(smsurl, mobileNo, Password);

            return url;
        }
    }
}