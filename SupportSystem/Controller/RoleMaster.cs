﻿namespace SupportProjectEmployee.Controller
{
    public class RoleMaster
    {
        public static int Admin = 1;
        public static int Employee = 2;
        public static int Customer = 3;
        public static int Support = 4;
    }
}