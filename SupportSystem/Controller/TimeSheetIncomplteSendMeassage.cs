﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SupportSystem.Controller
{
    public class TimeSheetIncomplteSendMeassage
    {
        public void SendMessage()
        {
            string date = DateTime.Now.Date.AddDays(-1).ToString();
            UserInformationBal ObjBal = new UserInformationBal();
            DataTable dt = new DataTable();
            dt = ObjBal.UserinformationSelectForIncompleteTimeSheet(date);

            if (dt.Rows.Count <= 0)
                return;


            MessageProcessing processing = new MessageProcessing();
            string Message = "Hi [NAME], Please fill yesterdays timesheet.";
            foreach (DataRow dtrow in dt.Rows)
            {

                    string MobileNo = dtrow["MobileNumber"].ToString().Trim();
                    string newMessage = Message;
                    string Name = dtrow["Name"].ToString().Trim();
                    newMessage = newMessage.Replace("[NAME]", Name);
                    MessageProcessing.SendSMS(MobileNo, newMessage);
                
            }
        }
    }

}



