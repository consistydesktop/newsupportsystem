﻿namespace SupportProjectEmployee.Controller
{
    public class Message
    {

        public string Refund = "REFUND";

        public string pending = "PENDING";
        public string Process = "PROCESS";


        public string FAILURE = "FAILURE";
        public string SUCCESS = "SUCCESS";

        public static string InvalidData = "Invalid data.";
        public string invalid = "Invalid user input";
        public string failure = "Failure";
        public string activation = "Dear user your account activated successfully with Name:{0},UserName:{1},Email:{2},Password:{3}.Thank you for using OM recharge service.";
        public string duplicateMobileNo = "This number already has registration, please try with another number";
        public string error = "error";
        public string activate = "User activated successfully";
        public string success = "Success";
        public string dataNot = "Data not available";
        public string duplicateRecharge = "Recharge fail due to duplicate recharge";
        public string lowBalance = "Low Balance";
        public string rechargeBillNotAccept = "Recharge bill not accepted";
        public string wrongCredential = "Please enter correct credentials";
        public string Exception = "Exception error occur";
        public string TicketAdd = "Ticket added successfully";
        public string NotAdd = "Ticket not added successfully";
        public string missing = "missing parameter";
        public string ErrorInSenderRegistration = "something wrong during registration";
        public string ErrorInMoneyTransfer = "something wrong during MoneyTransfer";
        public string ErrorInSenderValidation = "Sender ID not registerd, Enter valid sender number";
        public string ErrorInBenRegistration = "Invalid benificiary details";
        public string ExceptionOccurred = "Exception occurred";
        public string somethingWentWrong = "Something went wrong";
    }
}