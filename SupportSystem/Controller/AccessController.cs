﻿using Bal;
using Model;
using System;
using System.Data;
using System.Text;
using System.Web;

namespace SupportProjectEmployee.Controller
{
    public class AccessController
    {
        public static void Redirect(string pageName)
        {
            HttpContext.Current.Response.Redirect(pageName, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            return;
        }

        public string getOTP()
        {
            int lengthOfRechargePin = 6;
            string valid = "1234567890";
            StringBuilder strOTP = new StringBuilder(100);
            Random random = new Random();
            while (lengthOfRechargePin-- > 0)
            {
                strOTP.Append(valid[random.Next(valid.Length)]);
            }
            return strOTP.ToString();
        }

        public string autoGeneratePass()
        {
            int lengthOfPass = 8;
            string valid = "1234567890";
            StringBuilder strPass = new StringBuilder(100);
            Random random = new Random();
            while (lengthOfPass-- > 0)
            {
                strPass.Append(valid[random.Next(valid.Length)]);
            }
            return strPass.ToString();
        }

        public string getRandomToken()
        {
            int lengthOfToken = 20;
            string valid = "abcdefghijklmnozABCDEFGHIJKLMNOZ1234567890";
            StringBuilder strB = new StringBuilder(100);
            Random random = new Random();
            while (lengthOfToken-- > 0)
            {
                strB.Append(valid[random.Next(valid.Length)]);
            }
            return strB.ToString();
        }

        private static void checkToken()
        {
            LoginBal objBAL = new LoginBal();
            LoginModel objModel = new LoginModel();
            DataTable dt = new DataTable();

            try
            {
                objModel.UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                objModel.Token = HttpContext.Current.Session["Token"].ToString();
                //objModel.ApplicationType = "Web";
                dt = objBAL.SelectToken(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                HttpContext.Current.Response.Redirect("Login.aspx", true);
                return;
            }
            if (dt.Rows.Count <= 0)
            {
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.RemoveAll();
                HttpContext.Current.Session.Abandon();

                HttpContext.Current.Response.Redirect("Login.aspx", true);
                return;
            }
        }

        public void UpdateToken(string MobileNumber)
        {
            LoginModel objModel = new LoginModel();
            LoginBal objBAL = new LoginBal();
            string Token = getRandomToken();
            objModel.MobileNumber = MobileNumber;

            objModel.Token = Token;
            objBAL.UpdateToken(objModel);
        }

        public static void checkAccess(string pageName)
        {

            if (HttpContext.Current.Session["MobileNumber"] == null)
            {
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.RemoveAll();
                HttpContext.Current.Session.Abandon();
                HttpContext.Current.Response.Redirect("Login.aspx", true);
                return;
            }

            UserAccessModel objModel = new UserAccessModel();
            objModel.Pagename = pageName;
            objModel.UserTypeID = Convert.ToInt32(HttpContext.Current.Session["UserTypeID"]);
            UserAccessBAL objBal = new UserAccessBAL();

            if (!objBal.SelectValidAccess(objModel))
            {
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.RemoveAll();
                HttpContext.Current.Session.Abandon();
                HttpContext.Current.Response.Redirect("Login.aspx", true);
                return;
            }

            checkToken();

            
        }
    }
}