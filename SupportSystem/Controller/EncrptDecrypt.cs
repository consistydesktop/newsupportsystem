﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace SupportProjectEmployee.Controller
{
    public class EncrptDecrypt
    {
        //  public int softwareCounter = 999;//Main

        private const string passPhrase = "96TYUE%^@#)Srt hYH";
        private const string initVector = "j&^%sNGD#23IUerw";

        private byte[] Keys = Encoding.ASCII.GetBytes(passPhrase);
        private SymmetricAlgorithm _algorithm = new RijndaelManaged();

        private byte[] KEY = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
        private byte[] IV = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

        // This constant is used to determine the keysize of the encryption algorithm.
        private const int keysize = 256;

        public string DecryptString(string cipherText)
        {
            if (cipherText.Length > 0)
            {
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
                PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
                byte[] keyBytes = password.GetBytes(keysize / 8);
                RijndaelManaged symmetricKey = new RijndaelManaged();
                symmetricKey.Mode = CipherMode.CBC;
                ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
                MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
                CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                memoryStream.Close();
                cryptoStream.Close();
                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
            }
            else
            {
                return "";
            }
        }

        public byte[] DecryptData(byte[] cryptoData)
        {
            //Decrypt
            RijndaelManaged rm = new RijndaelManaged();

            MemoryStream ms = new MemoryStream(cryptoData);
            byte[] Key = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
            byte[] IV = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

            CryptoStream cs = new CryptoStream(
                ms, rm.CreateDecryptor(Key, IV), CryptoStreamMode.Read);
            StreamReader sr = new StreamReader(cs);

            return ms.ToArray();
        }

        public byte[] DecryptBytes(IEnumerable<byte> bytes)
        {
            //The ICryptoTransform is created for each call to this method as the MSDN documentation indicates
            //that the public methods may not be thread-safe and so we cannot hold a static reference to an instance
            using (var r = Rijndael.Create())
            {
                using (var decryptor = r.CreateDecryptor(KEY, IV))
                {
                    return Transform(bytes, decryptor);
                }
            }
        }

        private static byte[] Transform(IEnumerable<byte> bytes, ICryptoTransform transform)
        {
            using (var stream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write))
                {
                    foreach (var b in bytes)
                        cryptoStream.WriteByte(b);
                }

                return stream.ToArray();
            }
        }

        public string EncryptString(string plainText)
        {
            byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(cipherTextBytes);
        }
    }
}