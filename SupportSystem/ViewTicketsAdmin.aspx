﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewTicketsAdmin.aspx.cs" Inherits="SupportSystem.ViewTicketsAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Ticket Report
                                <button class="btn btn-info buttons float-right" type="button" onclick="location.href='/AddTicketAdmin.aspx'" title="Add Ticket">Add Ticket</button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputFromDate">From Date </label>
                        <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputToDate">To Date</label>
                        <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()'></input>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputEmployeeName">Select Developer (Optional)</label>
                        <select class="form-control loginput" id="inputEmployeeName"></select>
                    </div>
                </div>
                <div class="form-group row mt-3">

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputService">Select Service (Optional)</label>
                        <select class="form-control loginput" id="inputService" title="Select service" aria-describedby="emailHelp" onchange='$("#inputStatus").focus()'></select>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputTicketNo">Ticket No (Optional) </label>
                        <input type="text" class="form-control loginput" id="inputTicketNo" title="Ticket No" aria-describedby="emailHelp" />
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputSystemName">Select System (Optional)</label>
                        <select class="form-control loginput" id="inputSystemName" title="Select System" aria-describedby="emailHelp" onchange='$("#inputStatus").focus()'></select>
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick=" TicketInformationSelectByFilter();" id="SearchButton" title="Search">Search</button>
                            </div>
                        </div>
                        <label id="ResponseMsgLbl"></label>
                    </div>
                </div>
            </form>
            <div class="loader" id="loader" style="display: none"></div>
            <div class="table-responsive">
                <table id="ViewTicketsDetailsForAdmin" class="table table-bordered nowrap table-hover">
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
    <div class="modal fade" id="modal_Sender" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Tickets Description</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <span id="txtDescription"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="/JS/ViewTicketsAdmin.js"></script>

    <script>

        UserInformationSelectByEmployeeID();
        ServiceMasterSelect();
        SystemMasterSelect();
        $(document).ready(function () {
            $("#inputFromDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });
            $("#inputToDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true
            });

            var d = new Date();
            var currMonth = d.getMonth();
            var currYear = d.getFullYear();

            var startDate = new Date(currYear, currMonth, 1);
            $("#inputFromDate").datepicker("setDate", startDate);

            $("#inputToDate").datepicker('option', 'minDate', startDate);
            $("#inputToDate").datepicker("setDate", d);
            if (window.location.href.indexOf("TicketID") > -1) {
                var url = window.location.href;
                var id = url.split('=')
                TicketInformationSelectByTicketID(id[1]);
            }
            else {
                TicketsInformationSelect();
            }
        });
    </script>
</asp:Content>
