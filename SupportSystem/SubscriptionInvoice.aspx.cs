﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class SubscriptionInvoice : System.Web.UI.Page
    {
        private static string pageName = "SubscriptionInvoice.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }


        [WebMethod]
        public static AddWebSubscriptionModel[] SelectSystemName(int UserID)
        {
            AccessController.checkAccess(pageName);

            List<AddWebSubscriptionModel> details = new List<AddWebSubscriptionModel>();
            SubscritpionRenovationBal objBAL = new SubscritpionRenovationBal();
            try
            {
                details = objBAL.SelectSystemName(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }


       


        [WebMethod]
        public static InvoiceModel selectPurchaseReport(string FromDate, string ToDate, int UserID,int SystemID)
        {
            InvoiceModel details = new InvoiceModel();
            try
            {
                AccessController.checkAccess(pageName);

                
                InvoiceModel request = new InvoiceModel();
                request.UserID = UserID;
                request.FromDate = FromDate;
                request.ToDate = ToDate;
                request.SystemID = SystemID;

                
                if (UserID != 0)
                {
                    request.ReceiverID = UserID;
                }
                else
                {
                    request.ReceiverID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                }

                SubscritpionRenovationBal objBAL = new SubscritpionRenovationBal();
                DataTable dt = objBAL.SelectSearch(request);

                if (dt.Rows.Count <= 0)
                {
                    return details;
                }

                details.Amount = Convert.ToDecimal(dt.Rows[0]["PurchaseAmount"]);
                details.Charge = Convert.ToDecimal(dt.Rows[0]["Charge"]);

                details.Address = dt.Rows[0]["Address"].ToString();
                details.UserName = dt.Rows[0]["UserName"].ToString();
                details.Receiver = dt.Rows[0]["Receiver"].ToString();
                details.GSTNumber = dt.Rows[0]["GSTNo"].ToString();
                details.SystemName = dt.Rows[0]["SystemName"].ToString();

                details.Amount = details.Amount - details.Charge;

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }

    }
}