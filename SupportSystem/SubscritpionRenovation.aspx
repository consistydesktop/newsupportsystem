﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SubscritpionRenovation.aspx.cs" Inherits="SupportSystem.SubscritpionRenovation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="/Images/favicon.ico" />
    <title>Welcome to Consisty Support System</title>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" />
    <!-- Our Custom CSS -->
    <link href="/PublicSite/css/css.css" rel="stylesheet" />
    <link href="/PublicSite/css/font-awesome.css" rel="stylesheet" />
    <link href="/PublicSite/css/vertical-responsive-menu.css" rel="stylesheet" />
    <link href="/PublicSite/css/demo.css" rel="stylesheet" />
    <link href="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="/Chosen/chosen.min.css" rel="stylesheet" />
    <link href="/Chosen/chosen.css" rel="stylesheet" />

    <link href="/Styles/alert/css/alert.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/alert/themes/default/theme.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/PublicSite/js/jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".account").click(function () {
                var X = $(this).attr('id');

                if (X == 1) {
                    $(".submenu").hide();
                    $(this).attr('id', '0');
                }
                else {

                    $(".submenu").show();
                    $(this).attr('id', '1');
                }

            });
            //Mouseup textarea false
            $(".submenu").mouseup(function () {
                return false
            });
            $(".account").mouseup(function () {
                return false
            });
            //Textarea without editing.
            $(document).mouseup(function () {
                $(".submenu").hide();
                $(".account").attr('id', '');
            });
        });

    </script>

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <%--  <script src="https://code.jquery.com/jquery-1.9.1.js"></script>--%>
    <script src="PublicSite/js/vertical-responsive-menu.js"></script>

    <%--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>--%>
    <script src="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>


    <script src="/Scripts/String.js"></script>
    <script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>


    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" type="text/javascript"></script>

    <script src="/Chosen/chosen.jquery.js"></script>
    <script src="/Chosen/chosen.jquery.min.js"></script>
    <script src="/Chosen/chosen.order.jquery.min.js"></script>

    <script src="/Styles/alert/js/alert.js" type="text/javascript"></script>
    <script src="/Styles/alert/js/alert.min.js"></script>

    <script src="/JS/Clientname.js"></script>
    <script src="/JS/DataTableProperties.js"></script>
    <script src="/JS/DataTableSum.js"></script>
    <script src="/JS/Login.js"></script>

    <link rel="stylesheet" href="/PublicSite/css/style.css" />



    <div class="wrapper">
        <section id="divcustomer">
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Subscritpion Renovation 
                            </h6>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="row">
                    <div class="col-3"></div>
                    <div class="col-6">

                        <div class="form-group row mt-3">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" style="display: none;">
                                <label for="inputRequestType" style="font-size: medium; font-weight: bold;">Value</label>

                                <asp:DropDownList ID="DaysValue" class="form-control loginput" runat="server" AutoPostBack="true">
                                    <asp:ListItem Text="Select value" Value="0" />
                                    <asp:ListItem Text="1" Value="1" />
                                    <asp:ListItem Text="2" Value="2" />
                                    <asp:ListItem Text="3" Value="3" />
                                    <asp:ListItem Text="6" Value="6" />


                                </asp:DropDownList>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                                <label for="inputRequestType" style="font-size: medium; font-weight: bold;"> Time peroid</label>

                                <asp:DropDownList ID="DaysType" class="form-control loginput" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnSelectedIndexChanged">
                                    <asp:ListItem Text="Select Time peroid" Value="0" />
                                    <asp:ListItem Text="2  Days" Value="2Days" />
                                    <asp:ListItem Text="1  Month" Value="1Month" />
                                    <asp:ListItem Text="3  Months" Value="3Months" />
                                    <asp:ListItem Text="6  Months" Value="6Months" />
                                    <asp:ListItem Text="1  Year" Value="1Years" />


                                </asp:DropDownList>

                                <label for="inputRequestType" style="display: none; color: red;" id="ErrorInputRequestType"></label>
                            </div>

                        </div>
                        <div class="form-group row mt-3">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <label for="inputRequestType" style="font-size: medium; font-weight: bold;">PayMode</label>

                                <asp:DropDownList ID="ddlPayMode" runat="server" class="form-control loginput"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group row mt-3">

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="margin-top: 3%;">
                                <label style="font-weight: bold;">End Date : </label>
                                <b>
                                    <asp:Label ID="lbl_SubscriptionEndDate" runat="server" Style="font-size: medium; font-weight: bold; margin-top: 10%;"> </asp:Label>

                                </b>
                            </div>

                        </div>
                        <div class="form-group row mt-3">

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="margin-top: 3%;">
                                <label style="font-weight: bold;">Remaining Days : </label>
                                <b>
                                    <asp:Label ID="lbl_RemainingDays" runat="server" Style="font-size: medium; font-weight: bold; margin-top: 10%;"> </asp:Label>

                                </b>
                            </div>

                        </div>

                        <div class="form-group row mt-3">

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="margin-top: 3%;">
                                <label style="font-weight: bold;">Amount : </label>
                                <b>
                                    <asp:Label ID="lbl_Cost" runat="server" Style="font-size: medium; font-weight: bold; margin-top: 10%;"> </asp:Label>

                                </b>
                            </div>

                        </div>
                        <div class="form-group row mt-3">

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="margin-top: 3%;">
                                <label style="font-weight: bold;">
                                    Amount In Words : 
                                </label>
                                <b>
                                    <asp:Label ID="lblAmountInWords" runat="server" Style="font-size: medium; font-weight: bold; margin-top: 10%; color: #2da02d"> </asp:Label>

                                </b>

                            </div>

                        </div>


                        <div class="form-group row mt-3">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="row">
                                    <div class="col-12">

                                        <b>
                                            <label id="lbl_Message" style="color: red;" runat="server" />
                                        </b>

                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <asp:Button ID="btnSubmit" Style="background: #3db8db none repeat scroll 0 0 !important; border: medium none; color: #ffffff !important; font-size: 16px; margin: 2px; text-transform: uppercase; border-radius: 0px; font-family: 'sf_pro_displayregular';"
                                            class="btn btn-info w-100 mt-4 logbutton buttons" runat="server" Text="Pay Now" OnClick="btnSubmit_Click" CssClass="form-control btn btn-primary allinputbtn" />
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </section>
            <input type="hidden" id="ID" />
        </section>


        <div class="loader" id="loader" style="display: none"></div>


    </div>





    <label id="SubscriptionID" style="display: none"></label>
    <script src="JS/SubscritpionRenovation.js"></script>
    <script src="JS/Validation.js"></script>

    <script type="text/javascript">
        $(function () {
            var pgurl = window.location.href.substr(window.location.href
                .lastIndexOf("/") + 1);
            $(".menu .navbar-nav li a").each(function () {
                if ($(this).attr("href") == pgurl || $(this).attr("href") == '')
                    $(this).addClass("active");
            })
        });
    </script>
    <script>
        $(document).ready(function () {
            var id = ('<%= Session["UserTypeID"] %>');
            var WebURL = ('<%= Session["WebURL"] %>');

            $("#ID").text(id);
            //    CardTypeSelect();
        });
    </script>
</asp:Content>
