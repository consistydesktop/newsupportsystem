﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewTaskDetailEmployee.aspx.cs" Inherits="SupportSystem.ViewTaskDetailEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <form class="formbox mt-4" id="NewTaskForm">

            <div class="form-group row mt-3">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputEmployee">Employee Name(s)</label>

                    <select class="form-control loginput" multiple="multiple" id="inputEmployee" title="Select Employess" >
                    </select>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputTask">Task</label>
                    <input type="text" class="form-control loginput" id="inputTask" aria-describedby="emailHelp" title="Enter Task" placeholder="Enter Task" />
                </div>

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputDueDate">Due Date</label>
                    <input type="text" class="form-control loginput" id="inputDueDate" aria-describedby="emailHelp" title="Due Date" placeholder="Due Date" />
                </div>

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputStatus">Status</label>
                    <select class="form-control loginput" id="inputStatus" onchange="TaskMasterUpdateStatus()" title="Status">
                        <option value="0">Select Status</option>
                        <option value="1">New</option>
                        <option value="2">InProgress</option>
                        <option value="3">Resolved</option>
                        <option value="4">Completed</option>
                    </select>
                </div>
            </div>

            <div class="form-group row mt-3">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <label for="inputDescription">Description</label>
                    <textarea class="form-control loginput p-0" id="inputDescription" aria-describedby="emailHelp" placeholder="Enter Description"></textarea>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputAttachment">Attachment</label>
                    <button type='button' class='btn' title='download' style='width: 50%; background-color: white; border-color: #fff;' onclick='return DownloadAttachment()'><i class='fa fa-download' style='font-size: 20px; color: blue'></i></button>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputNumberOfPoints">Points</label>
                    <input type="text" id="inputNumberOfPoints" class="form-control loginput" placeholder="Enter number of points" />
                </div>
            </div>

            <div class="form-group row mt-3">

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-6">
                           
                        </div>
                        <div class="col-6">
                        </div>
                    </div>
                </div>
            </div>
            <label id="TaskID" style="display: none"></label>
            <label id="SubTaskID" style="display: none"></label>
            <div class="form-group row mt-3">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="table-responsive">
                        <table id="TaskDetails" class="table" style="width: 100%">
                            <thead></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <label for="inputComment">Comment</label>
                    <div id="CommentPanel"></div>
                    <input type="text" id="inputComment" class="form-control loginput" />
                    <button type="button" id="AddCommentButton" style="display: none" onclick="CommentMasterInsert()"><i class="fa fa-comments" aria-hidden="true"> Add Comment</i></button>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="modal_attachment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="H3">Attachment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid alltrans">
                        <div class="table-responsive">
                            <table id="Attachments" class="table table-bordered nowrap" cellspacing="0" width="100%">
                                <thead>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_status" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="H2">Update Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <select name="Status" id="ddlStatus" class="form-control" data-toggle="tooltip" data-placement="top" title="Ticket Status" style="height: 36px;">
                        <option value="0">Select Status</option>
                        <option value="Inprogress">Inprogress</option>
                        <option value="Completed">Resolved</option>
                    </select>
                    <br />
                    <input type="file" id="inputTaskAttachment" title="Attach file" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="return TaskMasterMasterUpdateStatus();" title="Update status" data-dismiss="modal">Update Status</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_Sender" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="h1">Description</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span id="txtDescription" style="height: 100%; width: 100%;"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <label id="Label1" style="display: none"></label>

    <div class="modal fade" id="Div3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="H4">Update Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <select name="Status" id="Select1" class="form-control" data-toggle="tooltip" data-placement="top" title="Ticket Status" style="height: 36px;">
                        <option value="0">Select Status</option>
                        <option value="Inprogress">Inprogress</option>
                        <option value="Completed">Resolved</option>
                    </select>
                    <br />
                    <input type="file" id="File1" title="Attach file" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="return TaskMasterMasterUpdateStatus();" title="Update status" data-dismiss="modal">Update Status</button>
                </div>
            </div>
        </div>
    </div>

    <script src="/JS/ViewTaskDetailEmployee.js"></script>

    <script>
        UserInformationSelectEmployeeName();
        $(document).ready(function () {
            if (window.location.href.indexOf("TaskID") > -1) {
                var url = window.location.href;
                var id = url.split('=');
                $("#TaskID").html(id[1]);
                TaskMasterSelectByID(id[1]);

            }
        });
    </script>
</asp:Content>