﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SupportSystem
{
    public class TicketGenerator
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public string GenerateTicketNo()
        {
            string TicketNo = "";
            int TicketID = 0;
            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectMaxTicketID");
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                    TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());

                if (TicketID == 0)
                    TicketNo = "con1";

                else
                {
                    TicketNo = "con " + (TicketID++);
                }

            }
            catch (Exception Ex) { new Logger().write(Ex); }


            return TicketNo;
        }
    }
}