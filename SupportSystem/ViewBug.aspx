﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewBug.aspx.cs" Inherits="SupportSystem.ViewBug" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>View Bug
                                <button class="btn btn-info buttons float-right" type="button" onclick="#" title="Insert new bug">New Bug</button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>

</asp:Content>
