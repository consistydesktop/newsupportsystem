﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class AndroidSupport : System.Web.UI.Page
    {
        private static string pageName = "AndroidSupport.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }
        [WebMethod]
        public static AndroidSupportModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);
            List<AndroidSupportModel> details = new List<AndroidSupportModel>();
            AndroidSupportBal objBAL = new AndroidSupportBal();
            try
            {
                details = objBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();

        }

        static bool IsValidInsert(int SystemID, string PlayStoreURL, string GoogleConsoleUserName, string GoogleConsolePassword, string Description, string CodeRepository)
        {

            if (SystemID == null)
            {
                return false;
            }
            if (PlayStoreURL == null)
            {
                return false;
            }
            if (GoogleConsoleUserName == null)
            {
                return false;
            }
            if (GoogleConsolePassword == null)
            {
                return false;
            }
            
            if (Description == null)
            {
                return false;
            }
            if (CodeRepository == null)
            {
                return false;
            }

            return true;
        }

        [WebMethod]
        public static MessageWithIcon Insert(int SystemID, string PlayStoreURL, string GoogleConsoleUserName, string GoogleConsolePassword, string Description, string CodeRepository, string FileIDs)
        {
           
            AccessController.checkAccess(pageName);
            bool IsVallid = IsValidInsert(SystemID, PlayStoreURL, GoogleConsoleUserName, GoogleConsolePassword, Description, CodeRepository);
            MessageWithIcon msgIcon = new MessageWithIcon();
            if (IsVallid == false)
            {
                msgIcon.Message = "Input parameter is not in correct format.";
            }
            AndroidSupportModel ObjModel = new AndroidSupportModel();
            msgIcon.Message = "Data not submitted";
            int k = -1;
            int result = -1;
            int UserID = -1;
            try
            {
                UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());

               
                ObjModel.SystemID = SystemID;
                ObjModel.PlayStoreURL = PlayStoreURL;
                ObjModel.GoogleConsoleUserName = GoogleConsoleUserName;
                ObjModel.GoogleConsolePassword = GoogleConsolePassword;
                ObjModel.Description = Description;
                ObjModel.CodeRepository = CodeRepository;


                AndroidSupportBal ObjBAL = new AndroidSupportBal();
                result = ObjBAL.Insert(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result > 0)
            {
                msgIcon.Message = "Data submitted successfully";
            }
            if (FileIDs != "0")
            {
                int j = 0;
                String Files = FileIDs;
                string[] fileID = Files.Split('#');

                for (j = 1; j < fileID.Length; j++)
                {
                    var currentFileID = fileID[j];
                    ObjModel.FileID = Convert.ToInt32(currentFileID);
                    ObjModel.AndroidPlayStoreID = result;
                    ObjModel.UploadType = UploadTypeMaster.UploadAndroidData;
                    AndroidSupportBal ObjBal = new AndroidSupportBal();
                    try
                    {
                        k = ObjBal.FileMappingInsert(ObjModel);
                    }
                    catch (Exception ex) { new Logger().write(ex); }
                    if (k < 0)
                        msgIcon.Message += " File not added";
                }
            }

            return msgIcon;
        }


        


    }
}