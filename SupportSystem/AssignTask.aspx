﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AssignTask.aspx.cs" Inherits="SupportSystem.AssignTask" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Assign Task
                                <input type="hidden" id="ViewTaskButton" />
                                <button class="btn btn-info buttons float-right" type="button" onclick="ViewAssignedTaskUserwise()">View Assigned Tasks</button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>

            <form class="formbox mt-4" id="NewTaskForm">
                <div class="form-group row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputEmployee">Employee Name</label>
                        <select class="form-control" data-live-search="true" id="inputEmployee" title="Select Employess">
                        </select>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <labelstyle="margin-top:20px" for="inputSystemName">System Name</labelstyle="margin-top:20px">
                        <select class="form-control loginput" id="inputSystemName">
                        </select>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label style="margin-top:20px" for="inputTask">Task</label>
                        <input type="text" class="form-control loginput" id="inputTask" aria-describedby="emailHelp" title="Enter Task" placeholder="Enter Task" />
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label style="margin-top:20px" for="inputDueDate">Due Date (Optional)</label>
                        <input type="text" class="form-control loginput" id="inputDueDate" aria-describedby="emailHelp" title="Due Date" placeholder="Due Date" />
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label style="margin-top:20px" for="inputStatus">Status</label>
                        <select class="form-control loginput" id="inputStatus" title="Status">
                            <option value="0">Select Status</option>
                            <option value="NEW" selected="selected">NEW</option>
                            <option value="INPROGRESS">INPROGRESS</option>
                            <option value="COMPLETE">COMPLETE</option>
                          
                        </select>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label style="margin-top:20px" for="inputDescription">Description</label>
                        <textarea class="form-control loginput p-0" id="inputDescription" aria-describedby="emailHelp" placeholder="Enter Description"></textarea>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="file-upload-wrapper">
                            <label style="margin-top:20px" for="inputAttachment">Attachment (Optional)</label>
                            <input type="file" id="inputAttachment" class="file-upload " multiple='true'/>
                            <button type='button' id="DownloadAttachmentButton" class='btn' title='download' style='display: none; background-color: white; border-color: #fff;' onclick='return DownloadAttachment()'><i class='fa fa-download' style='font-size: 20px; color: blue' ></i></button>
                        </div>
                    </div>

                </div>
                <div class="form-group row mt-3">



                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label style="margin-top:20px" for="inputNumberOfPoints">Points (Optional)</label>
                        <input type="text" id="inputNumberOfPoints" class="form-control loginput" placeholder="Enter number of points" />
                    </div>


                </div>


                <div class="form-group row mt-3" style="border: initial">

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h5><b>Sub Task</b></h5>
                        <div class="table-responsive" style="border: initial">
                            <table id="TaskDetails" class="table" style="width: 100%" >
                                <thead></thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="file-upload-wrapper">
                            <label style="margin-top:20px" for="inputAttachment">Attachment (Optional)</label>
                            <input type="file" id="inputAttachmentSubTask" class="file-upload " multiple='true'/>
                            <button type='button' id="DownloadAttachmentButtonSubTask" class='btn' title='download' style='display: none; background-color: white; border-color: #fff;' onclick='return DownloadAttachment()'><i class='fa fa-download' style='font-size: 20px; color: blue' ></i></button>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label style="margin-top:20px" for="inputComment">Comment (Optional)</label>
                        <div id="CommentPanel">
                            <input type="text" id="inputComment" class="form-control loginput" />
                            <button type="button" id="AddCommentButton" style="display: none" onclick="CommentMasterInsert()">Add Comment</button>
                        </div>
                    </div>
                </div>

                <div class="form-group row mt-3">

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" title="Assign Task">Assign</button>
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="UpdateButton" title="Update Task" style="display: none">Update</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" id="CancelButton" onclick="ResetAll()" title="Clear All ">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <label id="TaskID" style="display: none"></label>
                <label id="SubTaskID" style="display: none"></label>
                <label id="UserTypeID" style="display: none"></label>


            </form>
            <div class="loader" id="loader" style="display: none"></div>
            <div class="modal fade" id="modal_attachment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="H3">Attachment</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid alltrans">
                                <div class="table-responsive">
                                    <table id="Attachments" class="table table-bordered nowrap" cellspacing="0" width="100%">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src="/JS/AssignTask.js"></script>
    <script>
        var count = 0;

        var SubTaskObjects = [];

        $("#inputDueDate").datepicker({
            dateFormat: 'd MM, yy',
            minDate: "0",
            changeMonth: true,
            changeYear: true

        });

        InsertSubTask();


        $(document).ready(function () {

            $("#inputEmployee").focus();
            $("#inputStatus").val('NEW').trigger('chosen:updated');


            var UserTypeID = '<%= Session["UserTypeID"] %>';
            $("#UserTypeID").html(UserTypeID);
            UserInformationSelectEmployeeName();
            SystemMasterSelect();

            if (window.location.href.indexOf("TaskID") > -1) {
                var url = window.location.href;
                var id = url.split('=');
                var UserID = '<%= Session["UserID"] %>';
            
                $("#TaskID").html(id[1]);
                TaskMasterSelectByID(id[1]);
                $("#SubmitButton").css('display', 'none');
                $("#UpdateButton").show();
            }
        });
    </script>
</asp:Content>
