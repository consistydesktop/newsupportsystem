﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SystemName.aspx.cs" Inherits="SupportSystem.SystemName" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Assign System

                                    <button class="btn btn-info buttons float-right" type="button" onclick=' location.href="\AddWebSubscription.aspx"'>Add Subscription</button>


                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputClientName">Name</label>
                        <select class="form-control loginput" id="inputClientName" title="Client Name"></select>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputSystemName">System Name</label>
                        <input type="text" class="form-control loginput" id="inputSystemName" title="System Name" aria-describedby="emailHelp" placeholder="Enter System Name" />
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputService">Service (Optionl)</label>
                        <select class="form-control loginput" id="inputService" title="Select Service"></select>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputURL">URL (Optionl)</label>
                        <input type="text" class="form-control loginput" id="inputURL" title="URL" aria-describedby="emailHelp" placeholder="Enter URL" />
                    </div>

                </div>

                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputProductType">Product Type (Optionl) </label>
                        <select class="form-control loginput" id="inputProductType" title="Product Type">
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputHostingIP">Hosting IP </label>
                        <input type="text" class="form-control loginput" name="inputHostingIP" id="inputHostingIP" title="Enter IP" />
                    </div>
                    <label id="SystemID" style="display: none"></label>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" onclick="SystemMasterInsert();" title="Assign ">Assign</button>
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="UpdateButton" onclick="SystemMasterUpdate()" title="Update" style="display: none;">Update</button>
                            </div>


                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick="ResetAll();" title="Clear All">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table id="SystemMasterDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                    <thead></thead>
                    <tbody></tbody>
                </table>
            </div>
        </section>
    </div>
    <script src="/JS/SystemName.js"></script>
    <script>
        $(document).ready(function () {
            $("#inputClientName").focus();
            UserInformationSelectCustomerName();
            SystemMasterSelect();
            ServiceMasterSelect();
            ProductMasterSelect();
        });

    </script>
</asp:Content>
