﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using SupportSystem.StaticClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

namespace SupportSystem
{
    public partial class AssignToDeveloper : System.Web.UI.Page
    {
        private static string pageName = "AssignToDeveloper.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static AssignToDeveloperModel[] SelectEmployeeName()
        {
            AccessController.checkAccess(pageName);
            List<AssignToDeveloperModel> details = new List<AssignToDeveloperModel>();

            AssignToDeveloperBal objBAL = new AssignToDeveloperBal();
            try
            {
                details = objBAL.SelectEmployeeName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static AssignToDeveloperModel[] SelectSystem()
        {
            AccessController.checkAccess(pageName);
            List<AssignToDeveloperModel> details = new List<AssignToDeveloperModel>();

            AssignToDeveloperBal objBAL = new AssignToDeveloperBal();
            try
            {
                details = objBAL.SelectSystem();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static AssignToDeveloperModel SelectByTicketID(int TicketID)
        {
            AccessController.checkAccess(pageName);
            AssignToDeveloperModel details = new AssignToDeveloperModel();

            AssignToDeveloperBal objBAL = new AssignToDeveloperBal();
            try
            {
                details = objBAL.SelectByTicketID(TicketID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }

        [WebMethod]
        public static MessageWithIcon Insert(int AssignEmployeeID, string AssignDate, string AdminRemark, int TicketID, string Description, int SystemID, string TicketNo)
        {
            AccessController.checkAccess(pageName);
            AssignToDeveloperModel objModel = new AssignToDeveloperModel();

            objModel.DeveloperID = AssignEmployeeID;
            objModel.AssignDate = AssignDate;
            objModel.AdminRemark = AdminRemark;
            objModel.TicketID = TicketID;
            objModel.Description = Description;
            objModel.SystemID = SystemID;
            int i = -1;
            AssignToDeveloperBal objBal = new AssignToDeveloperBal();

            try
            {
                i = objBal.Insert(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            MessageWithIcon msgIcon = new MessageWithIcon();

            string returnMessage = AllMessages.TicketAssignEmailToClient;
            returnMessage = returnMessage.Replace("[TicketNo]", TicketNo);
            msgIcon.Message = returnMessage;
            if (i < 0)
            {
                msgIcon.Message = "Not assigned";
                return msgIcon;
            }

            int UserID = objBal.getUserIDByTicketID(TicketID);

            string returnEmail = AllMessages.TicketAssignEmailToClient;
            returnEmail = returnEmail.Replace("[TicketNo]", TicketNo);

            UserInformationBal Objbal = new UserInformationBal();
            DataTable dt = Objbal.GetEmailByUserID(AssignEmployeeID);
            string EmailID = dt.Rows[0]["EmailID"].ToString();      
            string MobileNo = dt.Rows[0]["MobileNumber"].ToString();
       

            string Message = AllMessages.TicketAssignedSMSToDeveloper;
            Message = Message.Replace("[TicketNo]", TicketNo);

            MessageProcessing.SendSMS(MobileNo, Message);

            SendEmail(returnEmail, AllMessages.TicketAcceptedAckForClient, EmailID);
            return msgIcon;
        }

        public static void SendEmail(string Message, string Subject, string EmailID)
        {
            UserInformationModel ObjModel = new UserInformationModel();
            UserInformationBal ObjBal = new UserInformationBal();

            try
            {
                MessageProcessing.SendEmail(EmailID, Message, Subject);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
        }
    }
}