﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using SupportSystem.StaticClass;
using System;
using System.Data;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class Action : System.Web.UI.Page
    {
        private static string pageName = "Action.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static MessageWithIcon Insert(string ResolvedDate, string TicketStatus, string EmployeeRemark, int TicketID, int UserID, string TicketNo)
        {
            AccessController.checkAccess(pageName);
            ActionModel objModel = new ActionModel();
            MessageWithIcon msgIcon = new MessageWithIcon();
            objModel.ResolvedDate = ResolvedDate;
            objModel.TicketStatus = TicketStatus;
            objModel.EmployeeRemark = EmployeeRemark;
            objModel.TicketID = TicketID;

            ActionBal objBal = new ActionBal();
            int i = -1;
            try
            {
                i = objBal.Insert(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (i < 0)
            {
                msgIcon.Message = "Not taken any action";
                return msgIcon;
            }
           
            UserInformationBal Objbal = new UserInformationBal();
            DataTable dt = Objbal.GetEmailByUserID(UserID);
            string EmailID = dt.Rows[0]["EmailID"].ToString();
            string MobileNo = dt.Rows[0]["MobileNumber"].ToString();
            string UserName = dt.Rows[0]["Name"].ToString();

            string returnEmail = AllMessages.TicketResolvedByEmployeeEmail;
            returnEmail = returnEmail.Replace("[Username]", UserName);
            returnEmail = returnEmail.Replace("[TicketNo]", TicketNo);
            returnEmail = returnEmail.Replace("[Status]", TicketStatus);

            SendEmail(returnEmail, AllMessages.TicketAcceptedAckForClient, EmailID);
            msgIcon.Message = "Ticket status updated successful";
            return msgIcon;
        }

        public static void SendEmail(string Message, string Subject, string EmailID)
        {
            UserInformationModel ObjModel = new UserInformationModel();
            UserInformationBal ObjBal = new UserInformationBal();
          
            try
            {            
                MessageProcessing.SendEmail(EmailID, Message, Subject);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
        }

        [WebMethod]
        public static ActionModel SelectByTicketID(int TicketID)
        {
            AccessController.checkAccess(pageName);
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            ActionModel details = new ActionModel();
            ActionBal objBAL = new ActionBal();
            try
            {
                details = objBAL.SelectByTicketID(TicketID, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }
    }
}