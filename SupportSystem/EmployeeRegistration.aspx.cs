﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Web.Services;

namespace SupportSystem
{
    public partial class EmployeeRegistration : System.Web.UI.Page
    {
        private static string pageName = "EmployeeRegistration.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        #region Insert

        public static bool ValidInsert(string FirstName, string LastName, string Username, string EmailID, string MobileNumber, string DateOfJoining, string EmplyoyeeMentStatus)
        {
            Validation validation = new Validation();
            if (validation.IsValidString(FirstName))
                return true;
            if (validation.IsValidString(LastName))
                return true;
            if (validation.IsValidString(Username))
                return true;
            if (validation.IsValidString(EmailID))
                return true;
            if (validation.IsEmailValid(EmailID))
                return true;
            if (validation.IsValidString(MobileNumber))
                return true;
            if (validation.IsValidString(DateOfJoining))
                return true;
            if (validation.IsValidString(EmplyoyeeMentStatus))
                return true;
            return true;
        }

        [WebMethod]
        public static MessageWithIcon Insert(string FirstName, string LastName, string Username, string EmailID, string MobileNumber, string DateOfJoining, string EmployeeMentStatus)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();

            if (!ValidInsert(FirstName, LastName, Username, EmailID, MobileNumber, DateOfJoining, EmployeeMentStatus))
            {

                msgIcon.Message = "Missing paramter";
                return msgIcon;
            }
            int i = -1;
            EmployeeRegistrationModel objModel = new EmployeeRegistrationModel();
            AccessController objAC = new AccessController();
            string Password = objAC.autoGeneratePass();

            objModel.FirstName = FirstName;
            objModel.LastName = LastName;
            objModel.Username = Username;
            objModel.EmailID = EmailID;
            objModel.Password = Password;
            objModel.UserTypeID = RoleMaster.Employee;
            objModel.MobileNumber = MobileNumber;
            objModel.DateOfJoining = Convert.ToDateTime(DateOfJoining);
            objModel.EmployeeMentStatus = EmployeeMentStatus;

            EmployeeRegistrationbal objBAL = new EmployeeRegistrationbal();
            try
            {
                i = objBAL.Insert(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i <= 0)
            {
                msgIcon.Message = "Duplicate employee";
                return msgIcon;
            }

            string PasswordMessage = string.Format("Your Password is {0}", Password);
            MessageProcessing.SendPassword(MobileNumber, Password);
            msgIcon.Message = "Employee added successfully";
            return msgIcon;




        }

        #endregion Insert

        [WebMethod]
        public static EmployeeRegistrationModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<EmployeeRegistrationModel> details = new List<EmployeeRegistrationModel>();

            EmployeeRegistrationbal objBAL = new EmployeeRegistrationbal();
            try
            {
                details = objBAL.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Delete(int UserID)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            EmployeeRegistrationbal objBal = new EmployeeRegistrationbal();
            try
            {
                i = objBal.Delete(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i > 0)
                msgIcon.Message = "User not deleted";
            else
                msgIcon.Message = "User deleted successfully";

            return msgIcon;
        }

        [WebMethod]
        public static EmployeeRegistrationModel SelectByID(int UserID)
        {
            AccessController.checkAccess(pageName);
            EmployeeRegistrationModel Register = new EmployeeRegistrationModel();
            EmployeeRegistrationbal objBal = new EmployeeRegistrationbal();
            try
            {
                Register = objBal.SelectByID(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Register;
        }

        #region Update

        public static bool ValidUpdate(string FirstName, string LastName, string Username, string EmailID, string MobileNumber, string DateOfReliving, string EmployeeMentStatus)
        {
            Validation validation = new Validation();
            if (validation.IsValidString(FirstName))
                return true;
            if (validation.IsValidString(LastName))
                return true;
            if (validation.IsValidString(Username))
                return true;
            if (validation.IsValidString(EmailID))
                return true;
            if (validation.IsEmailValid(EmailID))
                return true;
            if (validation.IsValidString(MobileNumber))
                return true;
            if (validation.IsValidString(DateOfReliving))
                return true;
            if (validation.IsValidString(EmployeeMentStatus))
                return true;

            return false;
        }

        [WebMethod]
        public static MessageWithIcon Update(string FirstName, string LastName, string Username, string EmailID, string MobileNumber, int UserID, string DateOfReliving, string EmployeeMentStatus, string DateOfJoining)
        {
            AccessController.checkAccess(pageName);
            EmployeeRegistrationModel objModel = new EmployeeRegistrationModel();
            MessageWithIcon msgIcon = new MessageWithIcon();

            if (!ValidUpdate(FirstName, LastName, Username, EmailID, MobileNumber, DateOfReliving, EmployeeMentStatus))
            {
                msgIcon.Icon = "Error";
                msgIcon.Message = "Missing paramter";
                return msgIcon;
            }
            int i = 0;

            objModel.FirstName = FirstName;
            objModel.LastName = LastName;
            objModel.Username = Username;
            objModel.EmailID = EmailID;
            objModel.UserID = UserID;
            objModel.MobileNumber = MobileNumber;
            objModel.DateOfJoining = Convert.ToDateTime(DateOfJoining);
            objModel.DateOfReliving = Convert.ToDateTime(DateOfReliving);
            objModel.EmployeeMentStatus = EmployeeMentStatus;


            EmployeeRegistrationbal objBAL = new EmployeeRegistrationbal();
            try
            {
                i = objBAL.Update(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "User details not updated";
            else
                msgIcon.Message = "User details updated successfully";

            return msgIcon;
        }

        #endregion Update
    }
}