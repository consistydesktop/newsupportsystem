﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class SystemName : System.Web.UI.Page
    {

        private static string pageName = "SystemName.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {

            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static SystemNameModel[] SelectCustomerName()
        {

            AccessController.checkAccess(pageName);
            List<SystemNameModel> details = new List<SystemNameModel>();
            SystemNameBal objBAL = new SystemNameBal();
            try
            {
                details = objBAL.SelectCustomerName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Insert(string SystemName, int UserID, int ServiceID, int ProductID, string URL, string HostingIP)
        {

            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            SystemNameModel objModel = new SystemNameModel();

            int i = -1;
            objModel.SystemName = SystemName;
            objModel.UserID = UserID;
            objModel.ServiceID = ServiceID;
            objModel.ProductID = ProductID;
            objModel.URL = URL;
            SystemNameBal objBAL = new SystemNameBal();
            try
            {
                i = objBAL.Insert(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "System not assigned";

            AddWebSubscriptionModel objModelSub = new AddWebSubscriptionModel();
            int j = -1;
            if (URL == "")
            {
                msgIcon.Message = "URL cannot be blank.";
                return msgIcon;
            }

            objModelSub.SystemID = i;
            objModelSub.WebsiteURL = URL;
            objModelSub.HostingIP = HostingIP;
            objModelSub.EndDate = Convert.ToDateTime(DateTime.Now.AddDays(15));
            objModelSub.PaymentStatus = "Pending";
            objModelSub.Description = "Initial";
            AccessController ac = new AccessController();
            objModelSub.Token = ac.getRandomToken();
            AddWebSubscriptionBal ObjBAL = new AddWebSubscriptionBal();
            try
            {
                j = ObjBAL.Insert(objModelSub);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (j < 0)
                msgIcon.Message = "System assigned and subscription not added";
            else
                msgIcon.Message = "System assigned and subscription added successfully";

            return msgIcon;
        }

        [WebMethod]
        public static SystemNameModel[] Select()
        {

            AccessController.checkAccess(pageName);
            List<SystemNameModel> details = new List<SystemNameModel>();

            SystemNameBal objBAL = new SystemNameBal();
            try
            {
                details = objBAL.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static SystemNameModel[] SelectService()
        {

            AccessController.checkAccess(pageName);
            List<SystemNameModel> details = new List<SystemNameModel>();

            SystemNameBal objBAL = new SystemNameBal();
            try
            {
                details = objBAL.SelectService();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static SystemNameModel[] SelectProduct()
        {

            AccessController.checkAccess(pageName);
            List<SystemNameModel> details = new List<SystemNameModel>();
            SystemNameBal objBAL = new SystemNameBal();
            try
            {
                details = objBAL.SelectProduct();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Delete(int SystemID)
        {

            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            SystemNameBal objBal = new SystemNameBal();
            try
            {
                i = objBal.Delete(SystemID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i > 0)
                msgIcon.Message = "System details not deleted";
            else
                msgIcon.Message = "System details deleted Successfully";

            return msgIcon;
        }

        [WebMethod]
        public static SystemNameModel SelectByID(int SystemID)
        {

            AccessController.checkAccess(pageName);
            SystemNameModel Register = new SystemNameModel();

            SystemNameBal objBal = new SystemNameBal();
            try
            {
                Register = objBal.SelectByID(SystemID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Register;
        }

        [WebMethod]
        public static MessageWithIcon Update(string SystemName, int UserID, int SystemID, int ServiceID, int ProductID, string URL)
        {

            AccessController.checkAccess(pageName);
            SystemNameModel objModel = new SystemNameModel();
            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            objModel.SystemName = SystemName;
            objModel.UserID = UserID;
            objModel.SystemID = SystemID;
            objModel.ServiceID = ServiceID;
            objModel.ProductID = ProductID;
            objModel.URL = URL;

            SystemNameBal objBAL = new SystemNameBal();
            try
            {
                i = objBAL.Update(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 1)
                msgIcon.Message = "System details not updated";
            else
                msgIcon.Message = "System details updated successfully";

            return msgIcon;
        }

    }
}