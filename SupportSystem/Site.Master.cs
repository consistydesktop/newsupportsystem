using System;
using System.Web;

namespace SupportSystem
{
    public partial class Site : System.Web.UI.MasterPage
    {
        private static string pageName = "Site.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            int role = 0;
            try
            {
                role = Convert.ToInt32(HttpContext.Current.Session["UserTypeID"].ToString());
            }
            catch (Exception ex)
            {
            }

            #region ADMIN

            if (role == RoleMaster.Admin)
            {
                LabelEmployeeDashboard.Visible = false;
                LabelAddTicketClient.Visible = false;
                LabelClientDashboard.Visible = false;
                LabelViewTicketEmployee.Visible = false;
                labelViewProjectEmployee.Visible = false;
                LabelViewTaskEmployee.Visible = false;
                LabelTimeSheet.Visible = false;
                LabelLeaveRequest.Visible = false;
                LabelHolidays.Visible = false;
                LabelTransactionDetailReport.Visible = false;
                LabelViewTicketClient.Visible = false;
                LabelSubscritpionRenovation.Visible = false;
            }

            #endregion ADMIN

            #region Employee

            if (role == RoleMaster.Employee)
            {
                LiSystemandSubscription.Visible = false;
                LabelFinoPerformance.Visible = false;
                LabelSubscriptionTaxInvoice.Visible = false;
                LabelFinovativeVersionHistory.Visible = false;
                LabelSubscriptionInvoice.Visible = false;
                LabelClientDashboard.Visible = false;
                LabelWebSubscription.Visible = false;
                LabelMemberDetails.Visible = false;
                LabelEmployeeRegistration.Visible = false;
                LabelMemberDetails.Visible = false;
                LabelCustomerCall.Visible = false;
                LabelAttendance.Visible = false;
                LabelNotification.Visible = false;
                LabelAssignProject.Visible = false;
                labelAddTicketAdmin.Visible = false;
                LabelAdminDashboard.Visible = false;
                LabelUserInformation.Visible = false;
                LabelSubscription.Visible = false;
                //LabelTask.Visible = false;
                LabelReminder.Visible = false;
                LabelAddHoliday.Visible = false;
                LabelAddTicketClient.Visible = false;
                LabelAllTimeSheetReport.Visible = false;
                LabelSystemReport.Visible = false;
                LabelLeaveRequestReport.Visible = false;
                LabelSendMessage.Visible = false;
                LabelTransactionDetailReport.Visible = false;
                LabelViewTicketClient.Visible = false;
                LabelAccount.Visible = false;
                LabelTransactionDetailReport.Visible = false;
                LabelSubscritpionRenovationReport.Visible = false;
                LabelSubscritpionRenovation.Visible = false;
                


            }

            #endregion Employee

            #region Support

            if (role == RoleMaster.Support)
            {
                LiSystemandSubscription.Visible = false;
                LabelFinoPerformance.Visible = false;
                LabelSubscriptionTaxInvoice.Visible = false;
                LabelFinovativeVersionHistory.Visible = false;
                LabelSubscriptionInvoice.Visible = false;
                LabelReminder.Visible = false;
                LabelClientDashboard.Visible = false;
                LabelEmployeeDashboard.Visible = false;
                LabelAddTicketClient.Visible = false;
                LabelViewTicketEmployee.Visible = false;
                LabelViewTaskEmployee.Visible = false;
                LabelAndroidData.Visible = false;
                LabelLeaveRequestReport.Visible = false;
                LabelSendMessage.Visible = false;
                LabelAccount.Visible = false;
                LabelTransactionDetailReport.Visible = false;
                LabelViewTicketClient.Visible = false;
                LabelSubscritpionRenovationReport.Visible = false;
                LabelSubscritpionRenovation.Visible = false;
              
            }

            #endregion Support

            #region Client

            if (role == RoleMaster.Customer)
            {
                LiSystemandSubscription.Visible = false;
                LabelProviderConfiguration.Visible = false;
                LabelFinoPerformance.Visible = false;
                LabelFinovativeVersionHistory.Visible = false;
                LabelTimeSheet.Visible = false;
                LabelReminder.Visible = false;
                labelViewProjectEmployee.Visible = false;
                LabelEmployeeDashboard.Visible = false;
                LabelAndroidData.Visible = false;
                LabelViewTicketEmployee.Visible = false;
                LabelViewTaskEmployee.Visible = false;
                LabelWebSubscription.Visible = false;
                LabelMemberDetails.Visible = false;
                LabelEmployeeRegistration.Visible = false;
                LabelMemberDetails.Visible = false;
                LabelCustomerCall.Visible = false;
                LabelAttendance.Visible = false;
                LabelNotification.Visible = false;
                LabelAssignProject.Visible = false;
                labelAddTicketAdmin.Visible = false;
                LabelAdminDashboard.Visible = false;
                LabelUserInformation.Visible = false;
                LabelSubscription.Visible = false;
                LabelTask.Visible = false;
                LabelTestingPanel.Visible = false;
                LabelReminder.Visible = false;
                LabelAddHoliday.Visible = false;

                LabelAllTimeSheetReport.Visible = false;
                LabelSystemReport.Visible = false;
                LabelLeaveRequest.Visible = false;
                LabelLeaveRequestReport.Visible = false;
                LabelHolidays.Visible = false;
                LabelSendMessage.Visible = false;
                LabelAccount.Visible = false;
                LabelTransactionDetailReport.Visible = false;
                LabelKnowledgeBaseReport.Visible = false;
                LabelSampleURL.Visible = false;
                LabelSampleResponse.Visible = false;
                LabelAPIProvider.Visible = false;

            }

            #endregion Client
        }
    }
}