﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AndroidSupportReport.aspx.cs" Inherits="SupportSystem.AndroidSupportReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>

            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Android Report
                                <button class="btn btn-info buttons float-right" type="button" onclick='location.href="/AndroidSupport.aspx"' title="Add Reminder">
                          Android Credentials
                                </button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>

            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputFromDate">From Date </label>
                        <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputToDate">To Date</label>
                        <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()' />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputStatus">System</label>
                             <select class="form-control loginput" id="inputSystemName">
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return AndroidDataSelect();" id="SearchButton" title="Search">Search</button>
                            </div>
                        </div>
                        <label id="ResponseMsgLbl"></label>
                    </div>
                </div>
            </form>
        </section>

        <div class="loader" id="loader" style="display: none"></div>

        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="projects" role="tabpanel" aria-labelledby="projects-tab">
                            <div class="table-responsive">
                                <table id="AndroidDetails" class="table table-bordered nowrap table-hover">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="JS/AndroidSupport.js"></script>

    <script>
        $("#inputFromDate").datepicker({
            dateFormat: 'd MM, yy',
            changeMonth: true,
            changeYear: true,
        });

        $("#inputFromDate").datepicker().datepicker("setDate", new Date());
        $("#inputToDate").datepicker({
            dateFormat: 'd MM, yy',
            changeMonth: true,
            changeYear: true,
        }); $("#inputToDate").datepicker().datepicker("setDate", new Date());
        SystemMasterSelect();
    AndroidDataSelect();
    </script>
</asp:Content>
