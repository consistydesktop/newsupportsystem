﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewTicketClient.aspx.cs" Inherits="SupportSystem.ViewTicketClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>View Tickets
                                 <button class="btn btn-info buttons float-right" type="button" onclick="location.href='/AddTicketClient.aspx'" title="View Tickets">Add Tickets</button></h6>

                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputFromDate">From Date </label>
                        <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputToDate">To Date</label>
                        <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()' />
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="TicketInformationSelectByDateForClient();" id="SearchButton" title="Search">Search</button>
                            </div>
                        </div>
                        <label id="ResponseMsgLbl"></label>
                    </div>
                </div>
            </form>
        </section>
       
        <form class="formbox mt-4">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="Div1">
                            <div class="tab-pane fade show active" id="Div2" role="tabpanel" aria-labelledby="projects-tab">
                                <div class="table-responsive">
                                    <table id="TicketDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
     <div class="loader" id="loader" style="display: none"></div>
    <div class="modal fade" id="modal_Sender" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Tickets Description</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <span id="txtDescription" style="height: 100%; width: 100%"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <script src="/JS/ViewTicketClient.js"></script>
    <script>

        $(document).ready(function () {
            $("#inputFromDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });
            $("#inputToDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true
            });

            var d = new Date();
            var currMonth = d.getMonth();
            var currYear = d.getFullYear();

            var startDate = new Date(currYear, currMonth, 1);
            $("#inputFromDate").datepicker("setDate", startDate);

            $("#inputToDate").datepicker('option', 'minDate', startDate);
            $("#inputToDate").datepicker("setDate", d);

            TicketInformationSelectByClient();

        });

    </script>

</asp:Content>
