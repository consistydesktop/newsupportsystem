﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class InsertNewBug : System.Web.UI.Page
    {
        private static string pageName = "InsertNewBug.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static InsertNewBugModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);

            List<InsertNewBugModel> details = new List<InsertNewBugModel>();

            InsertNewBugBal objBAL = new InsertNewBugBal();
            try
            {
                details = objBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static InsertNewBugModel[] SelectEmployeeName()
        {
            AccessController.checkAccess(pageName);
            List<InsertNewBugModel> details = new List<InsertNewBugModel>();

            InsertNewBugBal objBAL = new InsertNewBugBal();
            try
            {
                details = objBAL.SelectEmployeeName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static InsertNewBugModel[] SelectIssue()
        {
            AccessController.checkAccess(pageName);
            List<InsertNewBugModel> details = new List<InsertNewBugModel>();

            InsertNewBugBal objBAL = new InsertNewBugBal();
            try
            {
                details = objBAL.SelectIssue();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static List<List<InsertNewBugModel>> Search(string FromDate, string ToDate, string Status)
        {
            List<List<InsertNewBugModel>> details = new List<List<InsertNewBugModel>>();
            InsertNewBugBal objBAL = new InsertNewBugBal();
            try
            {
                details = objBAL.Search(FromDate, ToDate);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;

        }

        [WebMethod]

        public static MessageWithIcon Insert(int ProjectBugMasterID,
        string TestingIssueID,
        string BugTitle,
        string FileIDs,
        string PageURL,
        string Priority,
        string Description,
        string ExpectedResult)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            InsertNewBugModel objModel = new InsertNewBugModel();
            int k = -1;
            int i = -1;
            objModel.ProjectBugMasterID = ProjectBugMasterID;
            objModel.BugTitle = BugTitle;
            objModel.PageURL = PageURL;
            objModel.Status = "New";
            objModel.TestingIssueID = (TestingIssueID).ToUpper();
            objModel.Priority = Priority;
            objModel.Description = Description;
            objModel.ExpectedResult = ExpectedResult;

            InsertNewBugBal objBAL = new InsertNewBugBal();
            try
            {
                i = objBAL.Insert(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (i < 0)
            {
                msgIcon.Message = "Bug not reported ";
                return msgIcon;
            }
            else
            {
                msgIcon.Message = "Bug #" + i + " has been reported successfully";
                if (FileIDs != "0")
                {
                    int j = 0;
                    String Files = FileIDs;
                    string[] fileID = Files.Split('#');

                    for (j = 1; j < fileID.Length; j++)
                    {
                        var currentFileID = fileID[j];
                        objModel.FileID = Convert.ToInt32(currentFileID);
                        objModel.BugID = i;
                        objModel.UploadType = UploadTypeMaster.UploadBug;
                        InsertNewBugBal ObjBal = new InsertNewBugBal();

                        try
                        {
                            k = ObjBal.FileMappingInsert(objModel);
                        }
                        catch (Exception Ex) { new Logger().write(Ex); }
                        if (k < 0)
                            msgIcon.Message += "  Attachment is not added ";
                    }
                }
            }
            return msgIcon;
        }





        [WebMethod]
        public static string SelectProjectName(int ProjectBugMasterID)
        {
            AccessController.checkAccess(pageName);

            string  ProjectName ="";
            if (ProjectBugMasterID == 0)
                return ProjectName;

            InsertNewBugBal objBAL = new InsertNewBugBal();
            try
            {
                ProjectName = objBAL.SelectProjectName(ProjectBugMasterID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return ProjectName;
        }



        [WebMethod]
        public static InsertNewBugModel[] Select(int ProjectBugMasterID,string FromDate, string ToDate, string Status, string searchtype)
        {
            AccessController.checkAccess(pageName);
            List<InsertNewBugModel> details = new List<InsertNewBugModel>();

            InsertNewBugBal objBAL = new InsertNewBugBal();
            try
            {
                details = objBAL.Select(ProjectBugMasterID, FromDate,  ToDate, Status, searchtype);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static InsertNewBugModel SelectByID(int BugID)
        {
            AccessController.checkAccess(pageName);
            InsertNewBugModel Register = new InsertNewBugModel();

            InsertNewBugBal objBal = new InsertNewBugBal();
            try
            {
                Register = objBal.SelectByID(BugID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Register;
        }

        [WebMethod]

        public static MessageWithIcon Update(string BugTitle,
        string PageURL,
        string TestingIssueID,
        string Priority,
        string FileIDs,
        string Description,
        string Comment,
        string Status,
        string ExpectedResult,
        int BugID)
        {
            AccessController.checkAccess(pageName);
            InsertNewBugModel objModel = new InsertNewBugModel();
            MessageWithIcon msgIcon = new MessageWithIcon();
            int k = -1; 
            
            int LoginUserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());

            int i = 0;
            objModel.BugID = BugID;

            objModel.BugTitle = BugTitle;
            objModel.PageURL = PageURL;
            objModel.TestingIssueID = TestingIssueID;
            objModel.Priority = Priority;
            objModel.Description = Description;
            objModel.ExpectedResult = ExpectedResult;
            objModel.Comment = Comment;
            objModel.Status = Status;
            objModel.DeveloperID = LoginUserID;

            InsertNewBugBal objBAL = new InsertNewBugBal();
            try
            {
                i = objBAL.Update(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (i < 1)
                msgIcon.Message = "Bug  not updated";
            else
            {
                msgIcon.Message = "Bug  updated successfully ";

                if (FileIDs != "0")
                {
                    int j = 0;
                    String Files = FileIDs;
                    string[] fileID = Files.Split('#');

                    for (j = 1; j < fileID.Length; j++)
                    {
                        var currentFileID = fileID[j];
                        objModel.FileID = Convert.ToInt32(currentFileID);
                        objModel.BugID = i;
                        objModel.UploadType = UploadTypeMaster.UploadBug;
                        InsertNewBugBal ObjBal = new InsertNewBugBal();
                        try
                        {
                            k = ObjBal.FileMappingInsert(objModel);
                        }
                        catch (Exception ex) { new Logger().write(ex); }
                        if (k < 0)
                            msgIcon.Message += " File  not updated";
                    }
                }
            }
            return msgIcon;
        }

        [WebMethod]
        public static MessageWithIcon Delete(int BugID)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            InsertNewBugBal objBal = new InsertNewBugBal();
            try
            {
                i = objBal.Delete(BugID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "Bug data not deleted";
            else
                msgIcon.Message = "Bug data deleted Successfully";

            return msgIcon;
        }

        [WebMethod]
        public static InsertNewBugModel[] SelectAttachment(int BugID)
        {
            AccessController.checkAccess(pageName);
            InsertNewBugModel objModel = new InsertNewBugModel();
            List<InsertNewBugModel> details = new List<InsertNewBugModel>();

            objModel.UploadType = UploadTypeMaster.UploadBug;
            objModel.BugID = BugID;
            InsertNewBugBal objBAL = new InsertNewBugBal();
            try
            {
                details = objBAL.SelectAttachment(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Exist(string File, int ProjectBugMasterID)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            List<InsertNewBugModel> details = new List<InsertNewBugModel>();
            int i = -1;

            InsertNewBugBal objBAL = new InsertNewBugBal();
            try
            {
                i = objBAL.Exist(File, ProjectBugMasterID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (i > -1)
            {
                msgIcon.Message = "File Already Exists";
            }

            return msgIcon;
        }
    }
}