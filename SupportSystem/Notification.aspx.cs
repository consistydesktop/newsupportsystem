﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class Notification : System.Web.UI.Page
    {
        private static string pageName = "Notification.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
           
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static MessageWithIcon Insert(string Notification)
        {
     
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            NotificationModel objModel = new NotificationModel();
            int i = -1;
            objModel.Notification = Notification;
            NotificationBal objBAL = new NotificationBal();
            try
            {
                i = objBAL.Insert(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "Notification not submitted";         
            else
                msgIcon.Message = "Notification assigned successfully";

            return msgIcon;
        }

        [WebMethod]
        public static NotificationModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<NotificationModel> details = new List<NotificationModel>();

            NotificationBal objBAL = new NotificationBal();
            try
            {
                details = objBAL.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static NotificationModel[] SelectNotification()
        {
            string functionName = "SelectNotification";
            AccessController.checkAccess(functionName);
            List<NotificationModel> details = new List<NotificationModel>();
            NotificationBal objBAL = new NotificationBal();
            try
            {
                details = objBAL.SelectNotification();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Delete(int NotificationID)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            NotificationBal objBal = new NotificationBal();
            try
            {
                i = objBal.Delete(NotificationID);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i > 0)
                msgIcon.Message = "Notification not deleted";
            else
                msgIcon.Message = "Notification deleted Successfully";
            return msgIcon;
        }

        [WebMethod]
        public static NotificationModel SelectByID(int NotificationID)
        {
            AccessController.checkAccess(pageName);
            NotificationModel Register = new NotificationModel();

            NotificationBal objBal = new NotificationBal();
            try
            {
                Register = objBal.SelectByID(NotificationID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Register;
        }

        [WebMethod]
        public static MessageWithIcon Update(string Notification, int NotificationID)
        {
            AccessController.checkAccess(pageName);
            NotificationModel objModel = new NotificationModel();
            MessageWithIcon msgIcon = new MessageWithIcon();

            int i = 0;

            objModel.Notification = Notification;
            objModel.NotificationID = NotificationID;


            NotificationBal objBAL = new NotificationBal();
            try
            {
                i = objBAL.Update(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "Notification  not updated";
            else
                msgIcon.Message = "Notification updated successfully";

            return msgIcon;
        }
   
    }
}