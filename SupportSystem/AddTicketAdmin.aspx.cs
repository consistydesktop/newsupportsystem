﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class AddTicketAdmin : System.Web.UI.Page
    {
        private static string pageName = "AddTicketAdmin.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        #region Insert

        public static bool ValidUserValidation(string SystemName, int SystemID, int ServiceID, string Description, string Priority)
        {
            Validation validation = new Validation();

            if (!validation.IsValidString(SystemName))
                return false;
            if (!validation.IsValidInt(SystemID))
                return false;
            if (!validation.IsValidInt(ServiceID))
                return false;
            if (!validation.IsValidString(Priority))
                return false;
            return true;
        }

        [WebMethod]
        public static MessageWithIcon Insert(string SystemName, int DeveloperID, int SystemID, int ServiceID, string RequestType, string Description, string TeamAny, string FileIDs, string Priority, string TicketGenerationDate)
        {
            AccessController.checkAccess(pageName);

            AddTicketAdminModel objModel = new AddTicketAdminModel();
            AddTicketAdminBal objBAL = new AddTicketAdminBal();

            MessageWithIcon msgIcon = new MessageWithIcon();
            int k = -1;
            int i = -1;
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            if (!ValidUserValidation(SystemName, SystemID, ServiceID, Description, Priority))
                msgIcon.Message = "Missing paramter";
            else
            {
                TicketGenerator Obj = new TicketGenerator();
                string TicketNo = Obj.GenerateTicketNo();
                DataTable dt = new DataTable();
                objModel.SystemID = SystemID;
                objModel.DeveloperID = DeveloperID;
                objModel.UserID = UserID;
                objModel.ServiceID = ServiceID;
                objModel.Description = Description;
                objModel.TeamAny = TeamAny;
                objModel.Priority = Priority;
                objModel.TicketNo = TicketNo;
                objModel.RequestType = RequestType;
                try
                {
                    i = objBAL.Insert(objModel);
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }

                if (i < 0)
                    msgIcon.Message = "Ticket not added";
                else
                {
                    msgIcon.Message = " Ticket added successfully \n Ticket Number is " + TicketNo;
                    if (FileIDs != "0")
                    {
                        int j = 0;
                        String Files = FileIDs;
                        string[] fileID = Files.Split('#');

                        for (j = 1; j < fileID.Length; j++)
                        {
                            var currentFileID = fileID[j];
                            objModel.FileID = Convert.ToInt32(currentFileID);
                            objModel.TicketID = i;
                            objModel.UploadType = UploadTypeMaster.UploadTicketAdmin;
                            AddTicketAdminBal ObjBal = new AddTicketAdminBal();
                            try
                            {
                                k = ObjBal.FileMappingInsert(objModel);
                            }
                            catch (Exception ex) { new Logger().write(ex); }
                            if (k < 0)
                                msgIcon.Message += " File not added";
                        }
                    }
                }
            }
            return msgIcon;
        }

        #endregion Insert

        [WebMethod]
        public static AddTicketAdminModel[] SelectServiceName()
        {
            AccessController.checkAccess(pageName);
            List<AddTicketAdminModel> details = new List<AddTicketAdminModel>();
            AddTicketAdminBal objBAL = new AddTicketAdminBal();
            try
            {
                details = objBAL.SelectServiceName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static AddTicketAdminModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);

            List<AddTicketAdminModel> details = new List<AddTicketAdminModel>();
            AddTicketAdminBal objBAL = new AddTicketAdminBal();
            try
            {
                details = objBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static AddTicketAdminModel[] SelectEmployeeName()
        {
            List<AddTicketAdminModel> details = new List<AddTicketAdminModel>();
            AddTicketAdminBal ObjBAL = new AddTicketAdminBal();

            try
            {
                details = ObjBAL.SelectEmployeeName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }
    }
}