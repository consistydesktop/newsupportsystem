﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class Attendance : System.Web.UI.Page
    {
        private static string pageName = "Attendance.aspx";


        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }


        [WebMethod]
        public static Employee[] Select(int Month, int Year)
        {
            AccessController.checkAccess(pageName);

            if (Month == 0)
                return (new List<Employee>().ToArray());
            
            if(Month>(DateTime.Now.Month))
                return (new List<Employee>().ToArray());

            List<Employee> details = new List<Employee>();
            AttendanceBal ObjBAL = new AttendanceBal();
            try
            {
                details = ObjBAL.Select(Month, Year);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }
    }
}