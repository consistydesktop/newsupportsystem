﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebSubscriptionRenew.aspx.cs" Inherits="SupportSystem.WebSubscriptionRenew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Subscritpion Renovation 
                            </h6>
                        </div>
                    </div>
                </div>
            </section>

            <div class="form-group row mt-3">

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputSystemName">System Name</label>
                    <select class="form-control loginput" onchange="return SelectSubscriptionDetails();"    id="inputSystemName" title="Select System">
                    </select>
                </div>


            </div>

        </section>
        <div class="loader" id="loader" style="display: none"></div>
        <input type="hidden" id="ID" />

            <input type="hidden" id="uid" />
    </div>


    <label id="SubscriptionID" style="display: none"></label>

    <script src="JS/WebSubscriptionRenew.js"></script>

    <script src="JS/Validation.js"></script>
    <script>
        $(document).ready(function () {
            var id = ('<%= Session["UserTypeID"] %>');
            $("#ID").text(id);

            var lblUser = ('<%= Session["UserID"] %>');
            $("#lblUserID").text(lblUser);

            var uid = ('<%= Session["UserID"] %>');
            $("#uid").text(uid);
            SystemMasterSelect();
        });
    </script>
</asp:Content>
