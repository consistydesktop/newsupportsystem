﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddTicketClient.aspx.cs" Inherits="SupportSystem.AddTicketClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <input type="text" class="form-control" id="TicketGenerationDate" style="display: none" name="TicketGenerationDate" />
        <section>

            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Add Ticket
                                <button class="btn btn-info buttons float-right" type="button" onclick="location.href='/ViewTicketClient.aspx'" title="View Tickets">View Tickets</button></h6>
                        </div>
                    </div>
                </div>
            </section>

            <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputTitle">Ticket Issue</label>
                        <input type="text" id="inputTitle" placeholder="Ticket Issue" autofocus="autofocus" class="form-control  " />
                        <label for="inputTitle" style="display: none; color: red;" id="ErrorInputTitle"></label>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputService">Service Name</label>
                        <select id="inputService" class="form-control loginput"></select>
                        <label for="inputService" style="display: none; color: red;" id="ErrorInputService"></label>
                    </div>

                </div>
                <div class="form-group row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputRequestType">Request Type</label>
                        <select id="inputRequestType" class="form-control loginput">
                            <option value='0'>Select Request Type</option>
                            <option value="CHANGE" data-toggle="tooltip" data-placement="top" title="Change request is new development">Change</option>
                            <option value="ISSUE" data-toggle="tooltip" data-placement="top" title="Fault/Error in an existing system">Issue</option>
                        </select>
                        <label for="inputRequestType" style="display: none; color: red;" id="ErrorInputRequestType"></label>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputPriority">Priority</label>
                        <select id="inputPriority" class="form-control loginput">
                            <option value='0'>Select Priority</option>
                            <option value="LOW">Low</option>
                            <option value="MEDIUM">Medium</option>
                            <option value="HIGH">High</option>
                            <option value="CRITICAL">Critical</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row mt-3">

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                        <label for="inputRemoteAccess">Remote Access (Optional) - ID Password</label>
                        <select id="inputRemoteAccess" class="form-control loginput" onchange="SelectRemoteAccessOption()">
                            <option value="0">Select Remote Access</option>
                            <option value="ANYDESK">AnyDesk</option>
                            <option value="TEAMVIEWER">TeamViewer</option>
                            <option value="ULTRAVIEWER">UltraViewer</option>
                        </select>

                        <div id="RemoteAccessGroup" style="display: none">
                            <label for="inputAnyDeskID" id="LabelinputAnyDeskID">TeamViewer/AnyDesk ID (Optional)</label>
                            <input type="text" id="inputAnyDeskID" class="form-control loginput" onkeypress="return IsAlphaNumeric(event)" />
                        </div>
                    </div>


                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputAttachment">Attachment (Optional)</label>
                        <input type="file" id="inputAttachment" onchange="readURLTicketImage(this);" class="form-control" />

                    </div>
                </div>

                <div class="form-group row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputDescription">Description</label>
                        <textarea id="inputDescription" placeholder="Description" class="form-control " style="height: 165px"></textarea>

                    </div>
                   
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                        <%--<div id="AttachmentsDiv">
                                <table id="AttachmentTab" class="form-control table table-bordered nowrap table-hover">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>--%>
                        <label for="inputDescription">&nbsp;</label>
                        <img id="imageTicket" style="width: 297px; height: 210px;" src="//placehold.it/297x210" accept="images/*" />


                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" onclick="" title="Add Ticket">Submit</button>
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton buttons" id="UpdateButton" onclick="" style="display: none" title="Update Ticket">Update</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" id="CancelButton" onclick="Reset()" title="Clear All">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <label id="TicketID" style="display: none"></label>
        </section>
    </div>
    <script src="/JS/AddTicketClient.js"></script>
    <script>

        $(document).ready(function () {
            ServiceMasterSelect();
            if (window.location.href.indexOf("TicketID") > -1) {
                var url = window.location.href;
                var TicketID = url.split('=')
                TicketInformationSelectByTicketID(TicketID[1]);
                TicketInformationSelectAttachments(TicketID[1]);
            }
            $("#ddlServiceName").focus();
            $("#inputPriority").chosen();
            $("#inputRemoteAccess").chosen();
            $("#inputRequestType").chosen();
        });



    </script>
</asp:Content>
