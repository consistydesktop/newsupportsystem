﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class UserInformation : System.Web.UI.Page
    {
        private static string pageName = "UserInformation.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {

            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static UserInformationModel[] Select(int UserTypeID)
        {

            AccessController.checkAccess(pageName);
            List<UserInformationModel> details = new List<UserInformationModel>();
            UserInformationBal objBAL = new UserInformationBal();
            try
            {
                string LoginUserID = HttpContext.Current.Session["UserID"].ToString();
                details = objBAL.Select(UserTypeID,Convert.ToInt32(LoginUserID));
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Delete(int UserID)
        {

            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;
            UserInformationBal objBal = new UserInformationBal();
            try
            {
                i = objBal.Delete(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i > 0)
                msgIcon.Message = "User not deleted";
            else
                msgIcon.Message = "User deleted successfully";


            return msgIcon;
        }

        [WebMethod]
        public static UserInformationModel SelectByID(int UserID)
        {

            AccessController.checkAccess(pageName);
            UserInformationModel Register = new UserInformationModel();
            UserInformationBal objBal = new UserInformationBal();
            try
            {
                Register = objBal.SelectByID(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Register;
        }

        [WebMethod]
        public static MessageWithIcon Update(string Name, string Username, string EmailID, string MobileNumber, int UserID)
        {

            AccessController.checkAccess(pageName);
            UserInformationModel objModel = new UserInformationModel();
            MessageWithIcon msgIcon = new MessageWithIcon();

            int i = 0;

            objModel.Name = Name;
            objModel.Username = Username;
            objModel.EmailID = EmailID;
            objModel.UserID = UserID;
            objModel.MobileNumber = MobileNumber;

            UserInformationBal objBAL = new UserInformationBal();
            try
            {
                i = objBAL.Update(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "User data not updated";
            else
                msgIcon.Message = "User data updated successfully";

            return msgIcon;
        }



        [WebMethod]
        public static string UpdateAciveUser(int UserID, int IsActive)
        {

            AccessController.checkAccess(pageName);

            string ReturnMessage = "User not updated.";
            if (UserID == 0)
            {
                return ReturnMessage;
            }
            int Result = -1;
            UserInformationBal objBal = new UserInformationBal();
            try
            {
                Result = objBal.UpdateAciveUser(UserID, IsActive);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (Result > 0)
                ReturnMessage = "User updated successfully..";



            return ReturnMessage;
        }

    }
}