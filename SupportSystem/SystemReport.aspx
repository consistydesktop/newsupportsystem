﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SystemReport.aspx.cs" Inherits="SupportSystem.SystemReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>System Report                            
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">
                <div class="form-group row mt-4">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputFromDate">From Date </label>
                        <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputToDate">To Date</label>
                        <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()'></input>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputSystemName" style ="margin-bottom:21px">System name</label>
                        <select id="inputSystemName" class="form-control loginput select"></select>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick=" Search();" id="SearchButton" title="Search">Search</button>
                            </div>
                        </div>
                        <label id="ResponseMsgLbl"></label>
                    </div>
                </div>
            </form>
        </section>
        <form class="formbox mt-4">
            <h6><b>TimeSheet</b></h6>
            <div class="container-fluid mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="projects" role="tabpanel" aria-labelledby="projects-tab">
                                <div class="table-responsive">
                                    <table id="TimeSheetReport" class="table table-bordered nowrap table-hover " style="width: 100%">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form class="formbox mt-4">
            <h6><b>Task Report</b></h6>
            <div class="container-fluid mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="Div1">
                            <div class="tab-pane fade show active" id="Div2" role="tabpanel" aria-labelledby="projects-tab">
                                <div class="table-responsive">
                                    <table id="TaskReport" class="table table-bordered nowrap table-hover " style="width: 100%">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form class="formbox mt-4">
            <h6><b>Ticket Report</b></h6>
            <div class="container-fluid mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="Div3">
                            <div class="tab-pane fade show active" id="Div4" role="tabpanel" aria-labelledby="projects-tab">
                                <div class="table-responsive">
                                    <table id="TicketReport" class="table table-bordered nowrap table-hover " style="width: 100%">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>


    </div>

    <script src="/JS/SystemReport.js"></script>
    <script>
        SystemMasterSelect();


        $("#inputFromDate").datepicker({
            dateFormat: 'd MM, yy',
            changeMonth: true,
            changeYear: true,

        });
        $("#inputFromDate").datepicker().datepicker("setDate", new Date());
        $("#inputToDate").datepicker({
            dateFormat: 'd MM, yy',
            changeMonth: true,
            changeYear: true,

        });
        $("#inputToDate").datepicker().datepicker("setDate", new Date());
        //  ReminderMasterSelect();
    </script>
</asp:Content>
