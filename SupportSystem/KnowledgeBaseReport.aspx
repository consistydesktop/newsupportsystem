﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="KnowledgeBaseReport.aspx.cs" Inherits="SupportSystem.KnowledgeBaseReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Knowledge Base Report</h6>
                            <button class="btn btn-info buttons float-right" type="button" onclick="location.href='/KnowledgeBase.aspx'">Add</button>
                        </div>
                    </div>
                </div>
            </section>
             <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputFromDate">From Date </label>
                        <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputToDate">To Date</label>
                        <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()'></input>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons"  onclick="return KnowledgeBaseSelectByDate();" id="ShowButton" title="Show">Show</button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </section>
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="projects" role="tabpanel" aria-labelledby="projects-tab">
                            <div class="table-responsive">
                                <table id="KnowledgeBaseDetails" class="table table-bordered nowrap table-hover">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
     </div>
        <script src="JS/KnowledgeBaseReport.js"></script>

        <script>

            $(document).ready(function () {

                $("#inputFromDate").datepicker({
                    dateFormat: 'd MM, yy',
                    changeMonth: true,
                    changeYear: true,

                });
                $("#inputToDate").datepicker({
                    dateFormat: 'd MM, yy',
                    changeMonth: true,
                    changeYear: true,

                });

                KnowledgeBaseSelect();


            });
    </script>
</asp:Content>
