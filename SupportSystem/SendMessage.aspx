﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SendMessage.aspx.cs" Inherits="SupportSystem.SendMessage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Compose Meassage
                                  
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">

                <div class="form-group row mt-3">

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputUser">User</label>
                        <select class="form-control loginput" id="inputUser" title="Select User" onchange="UserInformationSelectForAll()">
                            <option value="0">Select User</option>
                            <option value="1">Admin </option>
                            <option value="4">Support </option>
                            <option value="2">Employee </option>
                            <option value="3">Client </option>
                        </select>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                        <label for="inputName">Name</label>

                        <select class="form-control loginput" id="inputName" name="ddlUserMessage" title="User Name" multiple='multiple' onchange="CheckUserSelected()" style="display: none">
                        </select>
                    </div>
                </div>

            </form>
            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <label>Message</label>
                    <div class="col-12">
                        <textarea class="form-control "
                            id="inputMessage"
                            title="Enter Message"
                            autofocus="autofocus"
                            style="height: 195px">

                        </textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 col-md-6">
                        <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" title="Clear All">Cancel</button>
                    </div>
                    <div class="col-6 col-md-6">
                        <button type="button"
                            class="btn btn-info w-100 mt-4 logbutton buttons"
                            id="SubmitButton"
                            onclick="SendMessage()"
                            title="Report Bug">
                            Send

                        </button>
                    </div>
                </div>

            </form>
        </section>
    </div>
    <script src="/JS/SendMessage.js"></script>
    <script>
    </script>
</asp:Content>
