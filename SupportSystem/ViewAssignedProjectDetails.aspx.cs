﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class ViewAssignedProjectDetails : System.Web.UI.Page
    {
        private static string pageName = "ViewAssignedProjectDetails.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
              
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static ViewAssignedProjectDetailsModel[] Select()
        {
              
            AccessController.checkAccess(pageName);

            List<ViewAssignedProjectDetailsModel> details = new List<ViewAssignedProjectDetailsModel>();
            ViewAssignedProjectDetailsBal objBAL = new ViewAssignedProjectDetailsBal();
            try
            {
                details = objBAL.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static ViewAssignedProjectDetailsModel[] SelectByDate(string FromDate, string ToDate)
        {
              
            AccessController.checkAccess(pageName);
            List<ViewAssignedProjectDetailsModel> details = new List<ViewAssignedProjectDetailsModel>();

            ViewAssignedProjectDetailsBal objBal = new ViewAssignedProjectDetailsBal();
            try
            {
                details = objBal.SelectByDate(FromDate, ToDate);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        
            [WebMethod]
        public static ViewAssignedProjectDetailsModel[] Search(string FromDate, string ToDate)
        {
              
            AccessController.checkAccess(pageName);

            List<ViewAssignedProjectDetailsModel> details = new List<ViewAssignedProjectDetailsModel>();
            ViewAssignedProjectDetailsBal ObjBAL = new ViewAssignedProjectDetailsBal();

            ViewAssignedProjectDetailsModel ObjModel = new ViewAssignedProjectDetailsModel();

            ObjModel.FromDate = FromDate;
            ObjModel.ToDate = ToDate;

            try
            {
                details = ObjBAL.Search(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Delete(int ProjectDetailMasterID)
        {
              
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = -1;
            ViewAssignedProjectDetailsBal objBal = new ViewAssignedProjectDetailsBal();
            try
            {
                i = objBal.Delete(ProjectDetailMasterID);
            }
            catch (Exception ex) { new Logger().write(ex); }
            if (i <= 0)

                msgIcon.Message = "Project not deleted";
            else

                msgIcon.Message = "Project deleted successfully";
            
            return msgIcon;
        }

        [WebMethod]
        public static MessageWithIcon Update(string Status, int ProjectDetailMasterID)
        {
              
            AccessController.checkAccess(pageName);
            ViewAssignedProjectDetailsModel objModel = new ViewAssignedProjectDetailsModel();
            MessageWithIcon msgIcon = new MessageWithIcon();

            int i = 0;

            objModel.ProjectDetailMasterID = ProjectDetailMasterID;
            objModel.Status = Status.ToUpper();

            ViewAssignedProjectDetailsBal objBAL = new ViewAssignedProjectDetailsBal();
            try
            {
                i = objBAL.Update(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "Status not updated";
            else
                msgIcon.Message = "Status updated successfully";

            return msgIcon;
        }

        [WebMethod]
        public static ViewAssignedProjectDetailsModel[] SelectEmployee(int ProjectDetailMasterID)
        {
              
            AccessController.checkAccess(pageName);

            List<ViewAssignedProjectDetailsModel> details = new List<ViewAssignedProjectDetailsModel>();

            ViewAssignedProjectDetailsBal objBAL = new ViewAssignedProjectDetailsBal();
            try
            {
                details = objBAL.SelectEmployee(ProjectDetailMasterID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static ViewAssignedProjectDetailsModel[] SelectByProjectDetailMasterID(int ProjectDetailMasterID)
        {
              
            AccessController.checkAccess(pageName);

            List<ViewAssignedProjectDetailsModel> details = new List<ViewAssignedProjectDetailsModel>();
            ViewAssignedProjectDetailsModel objModel = new ViewAssignedProjectDetailsModel();

            objModel.UploadType = UploadTypeMaster.UploadProject;
            objModel.ProjectDetailMasterID = ProjectDetailMasterID;
            ViewAssignedProjectDetailsBal objBAL = new ViewAssignedProjectDetailsBal();
            try
            {
                details = objBAL.SelectByProjectDetailMasterID(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon DeleteFile(int FileID)
        {
              
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;
            ViewAssignedProjectDetailsBal objBal = new ViewAssignedProjectDetailsBal();
            try
            {
                i = objBal.DeleteFile(FileID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i > -1)
                msgIcon.Message = "File not deleted";
            else
                msgIcon.Message = "File deleted successfully";
            return msgIcon;
        }
    }
}