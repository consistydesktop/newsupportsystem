﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeRegistration.aspx.cs" Inherits="SupportSystem.EmployeeRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Registration
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputFirstName">FirstName</label>
                        <input type="text" id="inputFirstName" title="FirstName" class="form-control loginput" placeholder="FirstName" autofocus="autofocus" />
                    </div>
                     <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputLastName">LastName</label>
                        <input type="text" id="inputLastName" title="LastName" class="form-control loginput" placeholder="LastName" autofocus="autofocus" />
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputUserName">UserName</label>
                        <input type="text" id="inputUserName" title="User Name" placeholder="User name" class="form-control loginput" onchange='$("#inputEmailID").focus();' />
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputEmailID">Email ID</label>
                        <input type="text" id="inputEmailID" title="Email ID" placeholder="Email ID" class="form-control loginput" onchange='$("#inputMobileNumber").focus();' />
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputMobileNumber">Mobile No</label>
                        <input type="text" id="inputMobileNumber" title="Mobile No" placeholder="Mobile No" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control loginput" onchange='$("#SubmitButton").focus();' />
                    </div>

                </div>
                <div class="form-group row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputDateOfJoining">Date Of Joining </label>
                        <input type="text" class="form-control loginput" id="inputDateOfJoining" title="Date Of Joining" aria-describedby="emailHelp" placeholder="Date Of Joining" />
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputDateOfReliving">Date Of Reliving </label>
                        <input type="text" class="form-control loginput" id="inputDateOfReliving" title="Date Of Reliving" aria-describedby="emailHelp" placeholder="Date Of Reliving" />
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputEmployeeMentStatus">Status</label>
                        <select class="form-control loginput" id="inputEmployeeMentStatus" title="Select Status">
                            <option value="0">Select Status</option>
                            <option value="Active">Active </option>
                            <option value="Resign">Resign </option>
                           
                        </select>
                    </div>
                </div>
                <label id="userID" style="display: none"></label>

                <div class="form-group row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" onclick="return UserInformationInsert();" title="Submit">Submit</button>
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return UserInformationUpdate();" id="UpdateButton" title="Update" style="display: none;">Update</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick="ResetAll();" title="Clear All">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>

    <script src="/JS/EmployeeRegistration.js"></script>

    <script>
        $(document).ready(function () {

            $("#inputFirstName").focus();
            $("#inputLastName").focus();

            if (window.location.href.indexOf("EmpRegID") > -1) {
                var url = window.location.href;
                var id = url.split('=');
                UserInformationSelectByID(id[1]);
            }
            $("#inputDateOfJoining").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });
            $("#inputDateOfReliving").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,
            });

        });
        function isNumberKey(e) {
            var r = e.which ? e.which : event.keyCode;
            return r > 31 && (48 > r || r > 57) && (e.which != 46 || $(this).val().indexOf('.') != -1) ? !1 : void 0
        }
        $('#inputFirstName').bind("cut copy paste", function (e) {

            e.preventDefault();

        });
        $('#inputLastName').bind("cut copy paste", function (e) {

            e.preventDefault();

        });


        //$(function () {
        //    $('#inputUserName').keydown(function (e) {
        //        {
        //            var key = e.keyCode;
        //            if (!((key == 8) || (key == 9) || (key == 11) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {

        //                e.preventDefault();
        //            }
        //        }
        //    });
        //})

        $(function () {
            $('#inputFirstName').keydown(function (e) {
                {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 9) || (key == 11) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {

                        e.preventDefault();
                    }
                }
            });
            $('#inputLastName').keydown(function (e) {
                {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 9) || (key == 11) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {

                        e.preventDefault();
                    }
                }
            });
        })


    </script>
</asp:Content>
