﻿using Bal;
using Model;
using Newtonsoft.Json;
using SupportProjectEmployee.Controller;
using SupportSystem.Controller;
using SupportSystem.StaticClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace SupportSystem.WebService
{
    /// <summary>
    /// Summary description for FinnoPerformance
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FinnoPerformance : System.Web.Services.WebService
    {

        [WebMethod]
        public void Insert(string Request, string Keydata)
        {
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            FinoPerformanceModel finovativePerformance = new FinoPerformanceModel();

            try
            {
                finovativePerformance = JsonConvert.DeserializeObject<FinoPerformanceModel>(Request);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (finovativePerformance == null)
                return;

            DateTime TxnDoc = DateTime.Now;
            try
            {
                TxnDoc = new Validation().ParseRequestDate(finovativePerformance.TxnDoc);
            }
            catch (Exception)
            {
                TxnDoc = DateTime.Now.AddDays(-1);
            }

            FinnoPerformanceBal objBAL = new FinnoPerformanceBal();
            try
            {
                objBAL.Insert(finovativePerformance);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }



        }
    }
}
