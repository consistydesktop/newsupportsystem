﻿using Bal;
using Model;
using Newtonsoft.Json;
using SupportSystem.StaticClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Description;

namespace SupportSystem.WebService
{
    /// <summary>
    /// Summary description for FinnoSampleResponse
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FinnoSampleResponse : System.Web.Services.WebService
    {


        private Message msg = new Message();

        [WebMethod]
        public void SampleResponseReport(string Token, string Keydata)
        {
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            APIResponse apiResponse = new APIResponse();
            string jsonResponse = "";
            try
            {

                //if (!isValidReportRequest(Token))
                //{

                //    apiResponse.Status = AllMessages.Failure;
                //    apiResponse.Response = AllMessages.invalidUserInput;
                //    jsonResponse = JsonConvert.SerializeObject(apiResponse);
                //    Context.Response.Write(jsonResponse);
                //    return;

                //}

                Report(apiResponse);

                jsonResponse = JsonConvert.SerializeObject(apiResponse);
                Context.Response.Write(jsonResponse);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                apiResponse.Status = AllMessages.Failure;
                apiResponse.Response = AllMessages.invalidUserInput;
                jsonResponse = JsonConvert.SerializeObject(apiResponse);

                Context.Response.Write(jsonResponse);
                return;
            }
        }

        private bool isValidReportRequest(string Token)
        {
            if (Token.Length <= 0)
            {
                return false;
            }


            DataTable dt = new System.Data.DataTable();
            LoginBal objBAL = new LoginBal();
            dt = objBAL.SelectByToken(Token);


            if (dt.Rows.Count <= 0)
            {
                return false;
            }


            return true;
        }

        public void Report(APIResponse apiResponse)
        {
            string jsonResponse = "";

            DataTable dt = new DataTable();
            try
            {
                Model.SampleResponseViewModel objModel = new Model.SampleResponseViewModel();
                FinovativeSampleResponseBal objBAL = new FinovativeSampleResponseBal();

                dt = objBAL.selectSampleResponseReport();
                List<Model.SampleResponseViewModel> details = new List<Model.SampleResponseViewModel>();

                if (dt.Rows.Count <= 0)
                {

                    apiResponse.Status = AllMessages.Failure;
                    apiResponse.Response = AllMessages.DataNotFound;
                    jsonResponse = JsonConvert.SerializeObject(apiResponse);
                    Context.Response.Write(jsonResponse);
                    return;


                }
                foreach (DataRow dtrow in dt.Rows)
                {

                    Model.SampleResponseViewModel report = new Model.SampleResponseViewModel();
                    report.SampleResponseID = Convert.ToInt32(dtrow["SampleResponseID"].ToString().Trim());
                    report.ProviderID = Convert.ToInt32(dtrow["ProviderID"].ToString().Trim());
                    report.DeveloperName = dtrow["DeveloperName"].ToString();
                    report.Type = (dtrow["Type"].ToString());
                    report.TextMustExit = (dtrow["TextMustExit"].ToString());
                    report.Response = (dtrow["Response"].ToString());
                    report.Status = (dtrow["Status"].ToString());
               
                    details.Add(report);
                }

                apiResponse.Status = AllMessages.Success;
                apiResponse.Response = JsonConvert.SerializeObject(details);
                return;


            }
            catch (Exception ex)
            {

                new Logger().write(ex);
                apiResponse.Status = AllMessages.Failure;
                apiResponse.Response = AllMessages.invalidUserInput;
                jsonResponse = JsonConvert.SerializeObject(apiResponse);

                Context.Response.Write(jsonResponse);
                return;
            }
        }

    }
}
