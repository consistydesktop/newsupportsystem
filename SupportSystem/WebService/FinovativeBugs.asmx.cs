﻿using Bal;
using Model;
using Newtonsoft.Json;
using SupportSystem.Controller;
using SupportSystem.StaticClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace SupportSystem.WebService
{
    /// <summary>
    /// Summary description for FinovativeBugInsert
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FinovativeBugs : System.Web.Services.WebService
    {

        [WebMethod]
        public void Insert(string Request, string Keydata)
        {
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            FinnovativeBug finovativeBug = new FinnovativeBug();

            try
            {

                finovativeBug = JsonConvert.DeserializeObject<FinnovativeBug>(Request);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (finovativeBug == null)
                return;

            InsertNewBugModel objModel = new InsertNewBugModel();
            objModel.ProjectBugMasterID = AllMessages.FinovativeProjectMasterID;
            objModel.BugTitle = finovativeBug.Company + " - " + finovativeBug.Exception;
            objModel.PageURL = "";
            objModel.Status = "New";
            objModel.TestingIssueID = "FUNCTIONAL";
            objModel.Priority = "High";
            objModel.Description = finovativeBug.Exception;
            objModel.ExpectedResult = "";

            InsertNewBugBal objBAL = new InsertNewBugBal();
            try
            {
                objBAL.Insert(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }



        }
    }
}
