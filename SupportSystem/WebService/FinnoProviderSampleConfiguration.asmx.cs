﻿using Bal;
using Model;
using Newtonsoft.Json;
using SupportSystem.StaticClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Description;

namespace SupportSystem.WebService
{
    /// <summary>
    /// Summary description for FinnoProviderSampleConfiguration
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FinnoProviderSampleConfiguration : System.Web.Services.WebService
    {


        private Message msg = new Message();

        [WebMethod]
        public void SampleConfigurationReport(string Token, string Keydata)
        {
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            APIResponse apiResponse = new APIResponse();
            string jsonResponse = "";
            try
            {

                Report(apiResponse);

                jsonResponse = JsonConvert.SerializeObject(apiResponse);
                Context.Response.Write(jsonResponse);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                apiResponse.Status = AllMessages.Failure;
                apiResponse.Response = AllMessages.invalidUserInput;
                jsonResponse = JsonConvert.SerializeObject(apiResponse);

                Context.Response.Write(jsonResponse);
                return;
            }
        }

        private bool isValidReportRequest(string Token)
        {
            if (Token.Length <= 0)
            {
                return false;
            }


            DataTable dt = new System.Data.DataTable();
            LoginBal objBAL = new LoginBal();
            dt = objBAL.SelectByToken(Token);


            if (dt.Rows.Count <= 0)
            {
                return false;
            }


            return true;
        }

        public void Report(APIResponse apiResponse)
        {
            string jsonResponse = "";

            List<Model.ProviderConfigurationViewModel> details = new List<Model.ProviderConfigurationViewModel>();
            DataTable dt = new DataTable();
            try
            {
             
             
                ProviderConfigurationBal objBAL = new ProviderConfigurationBal();
                 dt = objBAL.SelectConfiguration();
            

                if (dt.Rows.Count <= 0)
                {

                    apiResponse.Status = AllMessages.Failure;
                    apiResponse.Response = AllMessages.DataNotFound;
                    jsonResponse = JsonConvert.SerializeObject(apiResponse);
                    Context.Response.Write(jsonResponse);
                    return;
                }


                foreach (DataRow dtrow in dt.Rows)
                {
                    ProviderConfigurationViewModel report = new ProviderConfigurationViewModel();
                    report.ProviderID = Convert.ToInt32(dtrow["ProviderID"]);

                    report.OpCode = dtrow["OperatorCode"].ToString();
                    report.OperatorID = Convert.ToInt32(dtrow["OperatorID"].ToString());
                    report.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString());
                    report.Provider = dtrow["ProviderName"].ToString();
                    report.ServiceName = dtrow["ServiceName"].ToString();
                    report.OperatorName = dtrow["OperatorName"].ToString();
                    report.Type = dtrow["Param1"].ToString();

                    details.Add(report);
                }

                apiResponse.Status = AllMessages.Success;
                apiResponse.Response = JsonConvert.SerializeObject(details);
                return;
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                apiResponse.Status = AllMessages.Failure;
                apiResponse.Response = AllMessages.invalidUserInput;
                jsonResponse = JsonConvert.SerializeObject(apiResponse);

                Context.Response.Write(jsonResponse);
                return;
            }
        }
    }
}
