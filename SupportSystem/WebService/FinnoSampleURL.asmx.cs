﻿using Bal;
using Model;
using Newtonsoft.Json;
using SupportSystem.StaticClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Description;

namespace SupportSystem.WebService
{
    /// <summary>
    /// Summary description for FinnoSampleURL
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FinnoSampleURL : System.Web.Services.WebService
    {


        private Message msg = new Message();

        [WebMethod]
        public void SampleURLReport(string Token, string Keydata)
        {
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            APIResponse apiResponse = new APIResponse();
            string jsonResponse = "";
            try
            {

                //if (!isValidReportRequest(Token))
                //{

                //    apiResponse.Status = AllMessages.Failure;
                //    apiResponse.Response = AllMessages.invalidUserInput;
                //    jsonResponse = JsonConvert.SerializeObject(apiResponse);
                //    Context.Response.Write(jsonResponse);
                //    return;

                //}

                Report(apiResponse);

                jsonResponse = JsonConvert.SerializeObject(apiResponse);
                Context.Response.Write(jsonResponse);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                apiResponse.Status = AllMessages.Failure;
                apiResponse.Response = AllMessages.invalidUserInput;
                jsonResponse = JsonConvert.SerializeObject(apiResponse);

                Context.Response.Write(jsonResponse);
                return;
            }
        }

        private bool isValidReportRequest(string Token)
        {
            if (Token.Length <= 0)
            {
                return false;
            }


            DataTable dt = new System.Data.DataTable();
            LoginBal objBAL = new LoginBal();
            dt = objBAL.SelectByToken(Token);


            if (dt.Rows.Count <= 0)
            {
                return false;
            }


            return true;
        }

        public void Report(APIResponse apiResponse)
        {
            string jsonResponse = "";

            DataTable dt = new DataTable();
            try
            {
                Model.SampleURLViewModel objModel = new Model.SampleURLViewModel();
                FinovativeSampleURLBal objBAL = new FinovativeSampleURLBal();

                dt = objBAL.selectSampleURLReport();
                List<Model.SampleURLViewModel> details = new List<Model.SampleURLViewModel>();

                if (dt.Rows.Count <= 0)
                {

                    apiResponse.Status = AllMessages.Failure;
                    apiResponse.Response = AllMessages.DataNotFound;
                    jsonResponse = JsonConvert.SerializeObject(apiResponse);
                    Context.Response.Write(jsonResponse);
                    return;
                }
                foreach (DataRow dtrow in dt.Rows)
                {
                    Model.SampleURLViewModel report = new Model.SampleURLViewModel();
                    report.SampleURLID = Convert.ToInt32(dtrow["SampleURLID"].ToString());
                    report.ProviderID = Convert.ToInt32(dtrow["ProviderID"].ToString().Trim());
                    report.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString().Trim());
                    report.DeveloperName = (dtrow["DeveloperName"].ToString());
                    report.ServiceName = (dtrow["ServiceName"].ToString());
                    report.URL = (dtrow["URL"].ToString());
                    report.URLType = (dtrow["URLType"].ToString());
                    report.MethodType = (dtrow["MethodType"].ToString());
                    report.HeaderJSON = (dtrow["HeaderJSON"].ToString());
                    report.DateFormat = (dtrow["DateFormat"].ToString());
                    details.Add(report);
                }

                apiResponse.Status = AllMessages.Success;
                apiResponse.Response = JsonConvert.SerializeObject(details);
                return;
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                apiResponse.Status = AllMessages.Failure;
                apiResponse.Response = AllMessages.invalidUserInput;
                jsonResponse = JsonConvert.SerializeObject(apiResponse);

                Context.Response.Write(jsonResponse);
                return;
            }
        }

    }
}
