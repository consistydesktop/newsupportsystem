﻿using Bal;
using Model;
using Newtonsoft.Json;
using SupportSystem.Controller;
using SupportSystem.StaticClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services;


namespace SupportSystem.WebService
{
    /// <summary>
    /// Summary description for WebSubscription
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class WebSubscription : System.Web.Services.WebService
    {
        private EncryptDerypt ed = new EncryptDerypt();
        private string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        [WebMethod]
        public void GetRemainingDays(string URL, string Token)
        {
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            WebSubscriptionResponseModel Response = new WebSubscriptionResponseModel();
            string jsonResponse = "";

            if (URL == "")
            {
                Response.Status = AllMessages.Failure;
                string ReturnMessage = AllMessages.Blank;
                ReturnMessage = ReturnMessage.Replace("[Parameter]", "URL");
                Response.Message = ReturnMessage;
                jsonResponse = JsonConvert.SerializeObject(Response);
                Context.Response.Write(jsonResponse);
                return;
            }

            if (!Uri.IsWellFormedUriString(URL, UriKind.RelativeOrAbsolute))
            {
                Response.Status = AllMessages.Failure;
                string ReturnMessage = AllMessages.Invalid;
                ReturnMessage = ReturnMessage.Replace("[Parameter]", "URL");
                Response.Message = ReturnMessage;
                jsonResponse = JsonConvert.SerializeObject(Response);
                Context.Response.Write(jsonResponse);
                return;
            }

           
            string HostedIP = GetIPAddress();

            if (HostedIP == "")
            {
                Response.Status = AllMessages.Failure;
                string ReturnMessage = AllMessages.Blank;
                ReturnMessage = ReturnMessage.Replace("[Parameter]", "HostedIP");
                Response.Message = ReturnMessage;
                jsonResponse = JsonConvert.SerializeObject(Response);
                Context.Response.Write(jsonResponse);
                return;
            }
            string host = "";
            try
            {
                Uri myUri = new Uri(URL);
                host = myUri.Host;
            }
            catch (Exception ex) { new Logger().write(ex); }


            DateTime EndDate = getRemainingDays(host, HostedIP, Token);


            if (DateTime.Compare(EndDate, default(DateTime)) == 0)
            {
                Response.Status = AllMessages.Failure;
                string ReturnMessage = AllMessages.NotFound;
                ReturnMessage = ReturnMessage.Replace("[Parameter]", "Requested URL");
                Response.Message = ReturnMessage;
                jsonResponse = JsonConvert.SerializeObject(Response);
                Context.Response.Write(jsonResponse);
                return;
            }

            DateTime Today = DateTime.Now;
            int remainingDays = (EndDate - Today).Days;


            Response.Status = AllMessages.Success;
            Response.EndDate = EndDate;
            Response.Message = "Remaining days are ." + remainingDays;
            Response.RemainingDays = remainingDays;
            jsonResponse = JsonConvert.SerializeObject(Response);
            Context.Response.Write(jsonResponse);
            return;
        }

        private DateTime getRemainingDays(string URL, string HostedIP, string Token)
        {
            DataTable dt = new DataTable();
            AddWebSubscriptionBal ObjBal = new AddWebSubscriptionBal();
            dt = ObjBal.getEndDate(URL, HostedIP, Token);

            if (dt.Rows.Count < 1)
            {
                return default(DateTime);
            }

            DateTime EndDate = Convert.ToDateTime(dt.Rows[0]["EndDate"]);
            return EndDate;
        }
    }
}
