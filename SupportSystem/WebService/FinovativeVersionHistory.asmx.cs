﻿
using Bal;
using Model;
using Newtonsoft.Json;
using SupportSystem.StaticClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Description;

namespace SupportSystem.WebService
{

    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class FinovativeVersionHistory : System.Web.Services.WebService
    {


        private Message msg = new Message();

        [WebMethod]
        public void VersionHistoryReport(string Token, string Keydata)
        {
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            APIResponse apiResponse = new APIResponse();
            string jsonResponse = "";
            try
            {
                
                    //if (!isValidVersionHistoryReportRequest(Token))
                    //{

                    //    apiResponse.Status = AllMessages.Failure;
                    //    apiResponse.Response = AllMessages.invalidUserInput;
                    //    jsonResponse = JsonConvert.SerializeObject(apiResponse);
                    //    Context.Response.Write(jsonResponse);
                    //    return;

                    //}

                    Report(apiResponse);

                    jsonResponse = JsonConvert.SerializeObject(apiResponse);
                    Context.Response.Write(jsonResponse);
              
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                apiResponse.Status = AllMessages.Failure;
                apiResponse.Response = AllMessages.invalidUserInput;
                jsonResponse = JsonConvert.SerializeObject(apiResponse);

                Context.Response.Write(jsonResponse);
                return;
            }
        }

        private bool isValidVersionHistoryReportRequest(string Token)
        {
            if (Token.Length <= 0)
            {
                return false;
            }


            DataTable dt = new System.Data.DataTable();
            LoginBal objBAL = new LoginBal();
            dt = objBAL.SelectByToken(Token);


            if (dt.Rows.Count <= 0)
            {
                return false;
            }


            return true;
        }

        public void Report(APIResponse apiResponse)
        {
            string jsonResponse = "";

            DataTable dt = new DataTable();
            try
            {
                Model.VersionHistoryReport objModel = new Model.VersionHistoryReport();
                FinovativeVersionHistoryBal objBAL = new FinovativeVersionHistoryBal();

                dt = objBAL.selectVersionHistoryReport();
                List<Model.VersionHistoryReport> details = new List<Model.VersionHistoryReport>();

                if (dt.Rows.Count <= 0)
                {

                    apiResponse.Status = AllMessages.Failure;
                    apiResponse.Response = AllMessages.DataNotFound;
                    jsonResponse = JsonConvert.SerializeObject(apiResponse);
                    Context.Response.Write(jsonResponse);
                    return;


                }
                foreach (DataRow dtrow in dt.Rows)
                {

                    Model.VersionHistoryReport report = new Model.VersionHistoryReport();

                    report.Version = dtrow["Version"].ToString().ToUpper().Trim();
                    report.Title = (dtrow["Title"].ToString());
                    report.Description = (dtrow["Description"].ToString());
                    report.DOC = (dtrow["DOC"].ToString());
                    details.Add(report);
                }

                apiResponse.Status = AllMessages.Success;
                apiResponse.Response = JsonConvert.SerializeObject(details);
                //jsonResponse = JsonConvert.SerializeObject(apiResponse);
            //    Context.Response.Write(apiResponse);
                return;


            }
            catch (Exception ex)
            {

                new Logger().write(ex);
                apiResponse.Status = AllMessages.Failure;
                apiResponse.Response = AllMessages.invalidUserInput;
                jsonResponse = JsonConvert.SerializeObject(apiResponse);

                Context.Response.Write(jsonResponse);
                return;
            }
        }

    }
}
