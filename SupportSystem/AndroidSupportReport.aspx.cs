﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Web.Services;

namespace SupportSystem
{
    public partial class AndroidSupportReport : System.Web.UI.Page
    {
        static string pageName = "AndroidSupportReport.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }


        [WebMethod]
        public static AndroidSupportViewModel[] AndroidDataSelect(string FromDate, string ToDate, int SystemID)
        {

            AccessController.checkAccess(pageName);

            List<AndroidSupportViewModel> details = new List<AndroidSupportViewModel>();
            AndroidSupportBal ObjBAL = new AndroidSupportBal();

            AndroidSupportViewModel ObjModel = new AndroidSupportViewModel();

            ObjModel.FormDate = FromDate;
            ObjModel.Todate = ToDate;
            ObjModel.SystemID = SystemID;
            try
            {
                details = ObjBAL.Search(ObjModel);
            }
            catch (Exception ex)
            {
                new Model.Logger().write(ex);
            }

            return details.ToArray();
        }

    }
}