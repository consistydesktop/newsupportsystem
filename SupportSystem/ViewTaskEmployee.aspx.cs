﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class ViewTaskEmployee : System.Web.UI.Page
    {
        private static string pageName = "ViewTaskEmployee.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static AssignTaskModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<AssignTaskModel> details = new List<AssignTaskModel>();
            ViewTaskEmployeeBal objBal = new ViewTaskEmployeeBal();

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);

            try
            {
                details = objBal.Select(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static AssignTaskModel[] Search(string FromDate, string ToDate)
        {
            AccessController.checkAccess(pageName);
            List<AssignTaskModel> details = new List<AssignTaskModel>();
            ViewTaskEmployeeBal objBal = new ViewTaskEmployeeBal();
            try
            {
                details = objBal.Search(FromDate, ToDate);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }
    }
}