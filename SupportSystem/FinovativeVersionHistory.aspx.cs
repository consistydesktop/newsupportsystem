﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class FinovativeVersionHistory : System.Web.UI.Page
    {
        private static string pageName = "FinovativeVersionHistory.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }
        static bool IsValidInsert(string Title, string Description)
        {

            if (string.IsNullOrEmpty(Title))
            {
                return false;
            }
            if (string.IsNullOrEmpty(Description))
            {
                return false;
            }

            return true;
        }
        [WebMethod]
        public static MessageWithIcon InsertFinovativeVersionHistory(string Title, string Description, string Version, string Date)
        {
            AccessController.checkAccess(pageName);



            MessageWithIcon msgIcon = new MessageWithIcon();
            if (!IsValidInsert(Title, Description))
            {
                msgIcon.Message = Message.InvalidData;
                return msgIcon;
            }

            msgIcon.Message = "Version history not added";
            int result = -1;

            try
            {

                FinovativeVersionHistoryModel ObjModel = new FinovativeVersionHistoryModel();
                ObjModel.Title = Title;
                ObjModel.Description = Description;
                ObjModel.Version = Version;
                ObjModel.Date = Convert.ToDateTime(Date);

                FinovativeVersionHistoryBal ObjBAL = new FinovativeVersionHistoryBal();
                result = ObjBAL.InsertFinovativeVersionHistory(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result < 1)
                return msgIcon;


            msgIcon.Message = "Version history added";
            return msgIcon;
        }


        [WebMethod]
        public static VersionHistoryReport[] Select()
        {
            AccessController.checkAccess(pageName);
            List<VersionHistoryReport> details = new List<VersionHistoryReport>();
            DataTable dt = new DataTable();

            try
            {
                Model.VersionHistoryReport objModel = new Model.VersionHistoryReport();
                FinovativeVersionHistoryBal objBAL = new FinovativeVersionHistoryBal();

                dt = objBAL.selectVersionHistoryReport();
                if (dt.Rows.Count <= 0)
                {
                    return details.ToArray();
                }
                foreach (DataRow dtrow in dt.Rows)
                {

                    Model.VersionHistoryReport report = new Model.VersionHistoryReport();

                    report.FinnovativeVersionHistoryID = Convert.ToInt32(dtrow["FinnovativeVersionHistoryID"]);
                    report.Version = dtrow["Version"].ToString().ToUpper().Trim();
                    report.Title = (dtrow["Title"].ToString());
                    report.Description = (dtrow["Description"].ToString());
                    report.DOC = (dtrow["DOC"].ToString());
                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }


        [WebMethod]
        public static VersionHistoryReport SelectByID(int FinnovativeVersionHistoryID)
        {
            AccessController.checkAccess(pageName);
            VersionHistoryReport report = new VersionHistoryReport();

            FinovativeVersionHistoryBal objBal = new FinovativeVersionHistoryBal();
            try
            {
                report = objBal.SelectByID(FinnovativeVersionHistoryID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return report;
        }

        [WebMethod]
        public static MessageWithIcon Update(int FinnovativeVersionHistoryID, string Title, string Description, string Version, string Date)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();

            msgIcon.Message = "History not updated";
            int result = -1;
            try
            {
                FinovativeVersionHistoryModel ObjModel = new FinovativeVersionHistoryModel();
                ObjModel.FinnovativeVersionHistoryID = FinnovativeVersionHistoryID;
                ObjModel.Title = Title;
                ObjModel.Description = Description;
                ObjModel.Version = Version;
                ObjModel.Date = Convert.ToDateTime(Date);


                FinovativeVersionHistoryBal ObjBAL = new FinovativeVersionHistoryBal();
                result = ObjBAL.Update(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result < 1)
                return msgIcon;


            msgIcon.Message = "History updated successfully";
            return msgIcon;
        }



        [WebMethod]
        public static MessageWithIcon Delete(int FinnovativeVersionHistoryID)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            FinovativeVersionHistoryBal objBal = new FinovativeVersionHistoryBal();
            try
            {
                i = objBal.Delete(FinnovativeVersionHistoryID);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i > 0)
                msgIcon.Message = "History not deleted";
            else
                msgIcon.Message = "History deleted Successfully";
            return msgIcon;
        }
    }
}