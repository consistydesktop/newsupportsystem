﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SubscriptionInvoice.aspx.cs" Inherits="SupportSystem.SubscriptionInvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style type="text/css">
        .payreceipt {
            border: 1px solid #ccc;
            padding: 15px;
        }

        .receipthead h1 {
            font-size: 24px;
            font-family: arial;
            font-weight: bold;
            text-transform: uppercase;
        }

        .receipthead h2 {
            font-size: 18px;
            font-weight: bold;
        }

        .payreceipt table tbody tr td {
            border-top: none;
        }

        .table td, .table th {
            padding: .75rem;
            vertical-align: top;
            border-top: 1px solid #212529 !important;
        }
    </style>


    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Subscritpion Payment Report
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <div class="form-group row mt-3">
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                    <label for="inputFromDate">From Date </label>
                    <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                </div>

                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                    <label for="inputToDate">To Date</label>
                    <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()' />
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12" id="divCustomer"  style="display: none;">
                    <label for="inputClientName">Customer</label>
                    <select class="form-control loginput" id="inputClientName" onchange="return  SystemMasterSelect();" title="Client Name"></select>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputSystemName">System Name</label>
                    <select class="form-control loginput" id="inputSystemName" title="Select System" >
                    </select>
                </div>

                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="SelectPurchaseReport();" id="SearchButton" title="Search">Search</button>
                        </div>
                    </div>
                    <label id="ResponseMsgLbl"></label>
                </div>
            </div>


            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="Div1">
                            <div class="tab-pane fade show active" id="Div2" role="tabpanel" aria-labelledby="projects-tab">
                                <div class="table-responsive">
                                    <table id="tblGstReport" class="table table-bordered nowrap table-hover" style="width: 100%">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="loader" id="loader" style="display: none"></div>
        <input type="hidden" id="ID" />

        <label style="display: none" id="lblUserID"></label>
    </div>



    <div class="wrapper">

        <section>
            <div class="container-fluid">
                <div class="row justify-content-center mb-2" id="Payment-POS">
                    <div class="col-md-8">
                        <div class="payreceipt">
                            <div class="receipthead text-center">
                                <strong>
                                    <h3>PAYMENT VOUCHER</h3>
                                </strong>
                                <h4>Consisty System Pvt.Ltd.</h4>
                                <h5>Specialty Business Center, Office No.310, 3rd Floor, Opposite SKP Campus,<br>
                                    Above SBI, Balewadi, Pune - 411045.<br>
                                    <strong>GSTIN:</strong> 27AJAPT1352R1ZP<br>
                                </h5>

                            </div>

                            <table class="table receiptouter">
                                <thead>
                                    <tr>
                                        <th scope="col">Voucher No.</th>
                                        <th scope="col">
                                            <label id="lblVoucherNo"></label>
                                        </th>
                                        <th scope="col">Date :</th>
                                        <th scope="col">
                                            <label id="lblPurchaseDate"></label>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td colspan="4">
                                            <table class="table">

                                                <thead>
                                                    <tr>
                                                        <th scope="col">Particulars</th>
                                                        <th scope="col">Debit(Rs.)</th>
                                                        <th scope="col">Credit(Rs.)</th>
                                                        <th scope="col">Narration</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <label id="lblParty" />
                                                        </td>
                                                        <td>
                                                            <label id="lblDebit" style="text-align: right" />
                                                        </td>
                                                        <td>0</td>
                                                        <td>Dr.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Consisty System Pvt.Ltd.</td>
                                                        <td>0</td>
                                                        <td>
                                                            <label id="lblCredit" style="text-align: right" />
                                                        </td>
                                                        <td>Cr.
                                                        </td>
                                                    </tr>
                                                </tbody>

                                                <tfoot>
                                                    <tr>
                                                        <td></td>
                                                        <td><strong>
                                                            <label id="lblCredit1" style="text-align: right" />
                                                        </strong></td>
                                                        <td><strong>
                                                            <label id="lblDebit1" style="text-align: right" />
                                                        </strong></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <td colspan="5"><strong>Rupees :
                                                        <label id="lblAmountInWords" />
                                        </strong></td>
                                    </tr>


                                    <tr>
                                        <td colspan="3" style="text-align: left;"><span class="pt-5 d-block">Prepared By</span></td>
                                        <td colspan="3" style="text-align: center;"><span class="pt-5 d-block">Authorised By </span></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <input type="button" id="Payment-POS-btn"   value="Print" style="margin-left: 46%;" onclick="printDiv('Payment-POS')" />

        </section>
    </div>
    <input type="hidden" id="uid" />
    <script src="JS/SubscriptionInvoice.js"></script>

    <script src="JS/Validation.js"></script>
    <script>
        $(document).ready(function () {

            var id = ('<%= Session["UserTypeID"] %>');
            $("#ID").text(id);

            var lblUser = ('<%= Session["UserID"] %>');
            $("#lblUserID").text(lblUser);

            var uid = ('<%= Session["UserID"] %>');
            $("#uid").text(uid);


            if (id == 1) {

                $("#divCustomer").css("display", "block");
                UserInformationSelectCustomerName();
            }
            $("#inputFromDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });
            $("#inputToDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true
            });

            var d = new Date();
            var currMonth = d.getMonth();
            var currYear = d.getFullYear();

            var startDate = new Date(currYear, currMonth, 1);
            $("#inputFromDate").datepicker("setDate", startDate);

            $("#inputToDate").datepicker('option', 'minDate', startDate);
            $("#inputToDate").datepicker("setDate", d);


            SystemMasterSelect();
        });
    </script>
</asp:Content>
