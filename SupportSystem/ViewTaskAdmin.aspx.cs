﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Web.Services;

namespace SupportSystem
{
    public partial class ViewTaskAdmin : System.Web.UI.Page
    {
        private static string pageName = "ViewTaskAdmin.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static AssignTaskModel[] SelectEmployeeName()
        {
            AccessController.checkAccess(pageName);

            List<AssignTaskModel> details = new List<AssignTaskModel>();
            ViewTaskAdminBal ObjBAL = new ViewTaskAdminBal();
            try
            {
                details = ObjBAL.SelectEmployeeName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static AssignTaskModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<AssignTaskModel> details = new List<AssignTaskModel>();
            ViewTaskAdminBal objBal = new ViewTaskAdminBal();
            try
            {
                details = objBal.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static AssignTaskModel[] Search(string FromDate, string ToDate,int UserID,string Status)
        {
            AccessController.checkAccess(pageName);
            List<AssignTaskModel> details = new List<AssignTaskModel>();
            ViewTaskAdminBal objBal = new ViewTaskAdminBal();
            try
            {
                details = objBal.Search(FromDate, ToDate,UserID,Status);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }



        [WebMethod]
        public static AssignTaskModel[] SearchByUserID(string FromDate, string ToDate, int UserID)
        {
            AccessController.checkAccess(pageName);
            List<AssignTaskModel> details = new List<AssignTaskModel>();
            ViewTaskAdminBal objBal = new ViewTaskAdminBal();
            try
            {
                details = objBal.SearchByUserID(FromDate, ToDate, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        public string Status { get; set; }

        public int Count { get; set; }
    }
}