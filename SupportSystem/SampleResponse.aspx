﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SampleResponse.aspx.cs" Inherits="SupportSystem.SampleResponse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Sample Response 

                                 <button class="btn btn-info buttons float-right" type="button" onclick=' location.href="\APIProvider.aspx"'>Add New Provider</button>
              
 <button class="btn btn-info buttons float-right" type="button" onclick=' location.href="\SampleResponseReport.aspx"'>Report</button>
              

                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">


                <div class="row" style="margin-top: 30px;">
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12 mx-auto">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">API Provider</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">

                                    <select class="form-control loginput" id="txtProviderName" title="Select System">
                                    </select>
                                </div>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">TextMustExit</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <input type="text" class="form-control loginput" id="txtTextMustExit" placeholder="TextMustExit" title="TextMustExit" />
                                    <label style="display: none; color: red;" id="lblname">ServiceName is required</label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">Type</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <select id="ddlType" class="form-control loginput">

                                        <option value="Initial">Initial</option>
                                        <option value="View">View</option>
                                        <option value="CallBack">CallBack</option>
                                        <option value="StatusCheck">StatusCheck</option>
                                        <option value="BalanceCheck">BalanceCheck</option>
                                    </select>
                                    <label style="display: none; color: red;" id="lblname">URL is required</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">Status</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <select id="ddlStatus" class="form-control allinputtxt" style="height: 100%">
                                        <option value="PROCESS" selected="selected">PROCESS</option>
                                        <option value="SUCCESS">SUCCESS</option>
                                        <option value="FAILURE">FAILURE</option>
                                    </select>

                                    <label style="display: none; color: red;" id="lblname">URL is required</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">Response</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <textarea class="form-control loginput" rows="5" id="txtResponse" title="Response"></textarea>
                                    <label style="display: none; color: red;" id="lblname">URL is required</label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="parameters" style="margin-top: 27px; font-size: 20px; color: green">
                                        Parameters : &nbsp;[RequestID] [Amount] [Number] [TxnID] [RefID] [Message] [ClBal] [DMTBalance] [ApiCommPer][ROffer][DueDate][BBPSViewID]
  
                                        
                                        [LapuNo] [ApiComm] [CustomerName] [Param1] [Param2] [Param3] [NoUse1] 
                                          [NoUse2] [NoUse3]
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mt-3">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                <label id="txtSampleURLID" style="display: none"></label>
                            </div>

                            <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-9">
                                <div class="row">
                                    <div class="col-6">
                                        <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" onclick="return SampleResponseInsert();" title="Add Response Details">ADD</button>
                                        <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return SampleURLUpdate();" id="UpdateButton" title="Update Response Details" style="display: none;">Update</button>
                                    </div>
                                    <div class="col-6">
                                        <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick="ResetAll()" title="Clear All">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <label id="txtSampleResponseID" style="display: none"></label>
            <div class="container-fluid mt-5" style="display: none">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="myTabContent">
                            <div class="table-responsive">
                                <table id="SampleResponseDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                    <thead></thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="JS/SampleResponse.js"></script>
    <script>

        $(document).ready(function () {
            ProviderMasterSelect();
           //Select();

            if (window.location.href.indexOf("SampleResponseID") > 0) {
                var url = window.location.href;
                var id = url.split('=');
                SampleResponseSelectByID(id[1]);
            }


        });


    </script>
</asp:Content>

