﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PubliMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SupportSystem.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="bodyinner">
        <div id="slider">
          
            <div id="da-slider" class="da-slider">
                <div class="da-slide">
                    <div class="bannerhead">
                        <h2>Consistency & Quality</h2>
                    </div>
                    <div class="bannercont">
                        <p>
                            Providing consistent services will create customers satisfaction & Quality means
                            doing it right when no one is looking, Consistent Quality is our passion.
                        </p>
                    </div>

                    <div class="bannerout">
                        <div class="da-img">
                            <img src="/Public/images/slider1.png" />
                        </div>
                    </div>

                </div>
                <div class="da-slide">
                    <div class="bannerhead">
                        <h2>Recharge Application</h2>
                    </div>
                    <div class="bannercont">
                        <p>
                            We are the leaders in API based as well as Machine based recharge Applications,
                            We have 25+ satisfied customers in this year who uses our applications.
                        </p>
                    </div>
                    <div class="bannerout">
                        <div class="da-img">
                            <img src="Public/images/slider2.png" />
                        </div>
                    </div>
                </div>
                <div class="da-slide">
                    <div class="bannerhead">
                        <h2>Real Time Solution</h2>
                    </div>
                    <div class="bannercont">
                        <p>
                            Our OMR & Fast form fill product gives real time solution for different coaching
                            institutes & colleges.
                        </p>
                    </div>
                    <div class="bannerout">
                        <div class="da-img">
                            <img src="/Public/images/slider3.png" />
                        </div>
                    </div>
                </div>
                <div class="da-slide">
                    <div class="bannerhead">
                        <h2>Build Better Software</h2>
                    </div>
                    <div class="bannercont">
                        <p>
                            We create effective products that meet business needs and deliver results. We understand
                            client requirements and creating robust, Secure user-friendly, Software for our
                            clients.
                        </p>
                    </div>
                    <div class="bannerout">
                        <div class="da-img">
                            <img src="/Public/images/slider4.png" />
                        </div>
                    </div>
                </div>
                <div class="da-slide">
                    <div class="bannerhead">
                        <h2>Software for Society
                        </h2>
                    </div>
                    <div class="bannercont">
                        <p>
                            Provides experience with applying computing to social issues. Case studies on multiple
                            issues. For example: Distributed Computing, security, privacy, copyright,education.
                        </p>
                    </div>
                    <div class="bannerout">
                        <div class="da-img">
                            <img src="/Public/images/slider5.png" />
                        </div>
                    </div>
                </div>

                <nav class="da-arrows">
                    <span class="da-arrows-prev"></span><span class="da-arrows-next"></span>
                </nav>
                <div style="clear: both;">
                </div>
            </div>
            <div style="clear: both;">
            </div>
        </div>
    </div>
    <div id="content">
        <div class="welcomesect">
            <div class="welcomesectinner">
                <h2 style="text-align: center;">Welcome to Consisty System</h2>
                <p>
                    We at Consisty provides consistently high quality solutions to its valued customers.
                    Consisty specializes in building varied applications, some of our key solutions
                    are On-line recharge, Optical character recognition (OCR), Android applications
                    and research projects.
                    <br />
                    <b>
                        <br />
                        Why Our 75+ customers choose us? </b>
                    <br />
                    Consisty understands the need of customers. We help you to utilize the potential
                    of right technology in your application.
                    <br />
                    • We believe that, providing optimal solution within stipulated time is most crucial
                    part for every Business.
                    <br />
                    • Provides 24x7 instant support.
                    <br />
                    • We carry out R & D projects, published in reputed journals, and utilize it accordingly
                    in practical applications.<br />
                    Our goal is to make technology as fun and accessible to everyone and we want to
                    show you how easy it really is to do great things, and manage your business effectively
                    without any barrier.
                </p>
            </div>
        </div>
        <div class="borderless">
            &nbsp;
        </div>
        <div class="wework">
            <h2>We work well with</h2>
            <div class="dobox">
                <div class="symbol">
                    <img src="/Public/images/sym_com.png" width="112" height="112" />
                </div>
                <h3>Mobility Solutions</h3>
                <p>
                    Consisty System provides Mobility Solution to create an innovative and effective
                    web and software applications that create value and generate visible savings for
                    client organizations.
                </p>

            </div>
            <div class="dobox">
                <div class="symbol">
                    <img src="/Public/images/sym_hand.png" width="112" height="112" />
                </div>
                <h3>Managed Services</h3>
                <p>
                    Consisty System leaders are at an inflection point in terms of making the right
                    technology investments, delivering value and building a future-ready IT landscape.
                </p>

            </div>
            <div class="dobox">
                <div class="symbol">
                    <img src="/Public/images/sym_bulb.png" width="112" height="112" />
                </div>
                <h3>Research Project</h3>
                <p>
                    ‘Consisty System' focuses on creating innovative and effective web applications
                    that create value and generate visible savings for client organizations.
                </p>

            </div>
            <div style="clear: both;">
            </div>
        </div>



    </div>
    <link href="/Public/css/style2.css" rel="stylesheet" />
    <script>
        $(function () {

            $('#da-slider').cslider({
                autoplay: true,
                bgincrement: 450
            });

        });
    </script>

</asp:Content>
