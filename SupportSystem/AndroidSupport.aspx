﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AndroidSupport.aspx.cs" Inherits="SupportSystem.AndroidSupport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Android Credentials
                                <button class="btn btn-info buttons float-right" onclick='location.href="/AndroidSupportReport.aspx"' type="button">View Report</button></h6>
                        </div>
                    </div>
                </div>
            </section>

            <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputSystemName">Project Name</label>
                        <select class="form-control loginput" id="inputSystemName">
                        </select>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputPlayStoreURLID">Play Store URL</label>
                        <input type="text" class="form-control loginput" id="inputPlayStoreURLID" aria-describedby="emailHelp" placeholder="Play Store URL" />
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputGCUserName">GC UserName</label>
                        <input type="text" class="form-control loginput" id="inputGCUserName" aria-describedby="emailHelp" placeholder="GC UserName" />
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputGCPassword">GC Password</label>
                        <input type="email" class="form-control loginput" id="inputGCPassword" aria-describedby="emailHelp" placeholder="GC Password" />
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputCodeRepository">Code Repository</label>
                        <input type="text" class="form-control loginput" id="inputCodeRepository" aria-describedby="emailHelp" placeholder="Code Repository" />
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputDescription">Description</label>
                        <textarea class="form-control loginput" id="inputDescription" aria-describedby="emailHelp" placeholder="Description" title="Description"></textarea>
                    </div>


                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputAttachment">JKS Filepath  </label>
                        <input type="file" class="form-control loginput" id="inputAttachment" aria-describedby="emailHelp" placeholder="Upload File" />

                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputAttachment">Key Store Filepath </label>
                        <input type="file" class="form-control loginput" id="inputKeyStoreAttachment" aria-describedby="emailHelp" placeholder="Upload File" />

                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                        <button type="button" id="AddButton" class="btn btn-info w-100 mt-4 logbutton buttons">ADD</button>

                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                        <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn">Cancel</button>

                    </div>


                </div>

                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                            </div>
                            <div class="col-6">
                            </div>
                        </div>
                    </div>


                </div>

            </form>

        </section>

    </div>
    <script src="JS/AndroidSupport.js"></script>
    <script>

        $(document).ready(function () {
            $("#inputSystemName").focus();
            SystemMasterSelect();
        });
    </script>
    
</asp:Content>
