﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LeaveRequest.aspx.cs" Inherits="SupportSystem.LeaveRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">

        <section class="formhead">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <h6>Leave Request </h6>
                        <button class="btn btn-info buttons float-right" type="button" onclick="location.href='/LeaveRequestReport.aspx'">View Request Report</button>
                    </div>
                </div>
            </div>
        </section>

        <form class="formbox mt-4">

            <div class="form-group row mt-3">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputFromDate">From Date </label>
                    <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
            </div>

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputToDate">To Date</label>
                    <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()'></input>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="Description">Description</label>
                    <textarea class="form-control loginput" id="inputDescription" aria-describedby="emailHelp" placeholder="Description"></textarea>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-6">
                            <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return LeaveRequestInsert();" id="SubmitButton" title="Submit">Submit</button>
                        </div>
                    </div>

                </div>
            </div>


        </form>
    </div>
    <script src="/JS/LeaveRequest.js"></script>
    <script>
        $(document).ready(function () {
            $("#inputFromDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });
            $("#inputToDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });



        });
    </script>
</asp:Content>
