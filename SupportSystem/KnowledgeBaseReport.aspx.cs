﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class KnowledgeBaseReport : System.Web.UI.Page
    {
        static string pageName = "KnowledgeBaseReport.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }
        [WebMethod]
        public static KnowledgeBaseModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<KnowledgeBaseModel> details = new List<KnowledgeBaseModel>();
            KnowledgeBaseReportbal ObjBAL = new KnowledgeBaseReportbal();
            try
            {
                string FromDate = DateTime.Now.AddDays(-30).ToString("dd MMM, yyyy");
                string ToDate = DateTime.Now.ToString("dd MMM, yyyy");
                details = ObjBAL.SelectByDate(FromDate, ToDate);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }
        [WebMethod]
        public static KnowledgeBaseModel[] SelectByDate(string FromDate,string ToDate)
        {
            AccessController.checkAccess(pageName);
            List<KnowledgeBaseModel> details = new List<KnowledgeBaseModel>();
            KnowledgeBaseReportbal ObjBAL = new KnowledgeBaseReportbal();

            try
            {
                details = ObjBAL.SelectByDate(FromDate, ToDate);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }
        
        
        
    }
}