﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CustomerCallScreen.aspx.cs" Inherits="SupportSystem.CustomerCallScreen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Customer call
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputSystemName">System Name</label>
                        <select class="form-control loginput" id="inputSystemName" title="Select System">
                        </select>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputEmployeeName">Employee Name (Optional)</label>
                        <select class="form-control loginput" id="inputEmployeeName" title="Select Employee  ">
                        </select>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputIssueType">Issue Type (Optional)</label>
                        <select class="form-control loginput" id="inputIssueType" title="Select Issue Type ">
                        </select>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputMobileNumber">Mobile Number  </label>
                        <input type="text" class="form-control loginput" id="inputMobileNumber" onkeypress="return isNumberKey(event)" maxlength="10" title="Mobile Number" aria-describedby="emailHelp" />
                    </div>
                </div>

                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputDescription">Description </label>
                        <textarea class="form-control loginput" id="inputDescription" title="Description"></textarea>

                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputAnyDeskID">Any Desk (Optional) </label>

                        <input type="text" class="form-control loginput" onkeypress="return IsAlphaNumeric(event)" name="inputAnyDeskID" title="AnyDeskID" id="inputAnyDeskID" />
                    </div>
                    <label id="CallID" style="display: none"></label>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" onclick="return CallMasterInsert();" title="Add Call Call Details">ADD</button>
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return CallMasterUpdate();" id="UpdateButton" title="Update Call Details" style="display: none;">Update</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick="ResetAll()" title="Clear All">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="container-fluid mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="myTabContent">
                            <div class="table-responsive">
                                <table id="CallDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                    <thead></thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="modal_description" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="h1">Description</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span id="Description" style="height: 100%; width: 100%;"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="/JS/CustomerCallScreen.js"></script>
    <script>
        function isNumberKey(e) {
            var r = e.which ? e.which : event.keyCode;
            return r > 31 && (48 > r || r > 57) && (e.which != 46 || $(this).val().indexOf('.') != -1) ? !1 : void 0
        }
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122));
            return ret;
        }
        $(document).ready(function () {
            SystemMasterSelect();
            UserInformationSelectByEmployeeID();
            CallIssueMasterSelect();
            CallMasterSelect();
        });

        $('#inputMobileNumber').bind("cut copy paste", function (e) {

            e.preventDefault();

        });
    </script>
</asp:Content>
