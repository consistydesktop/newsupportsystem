﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class SampleResponse : System.Web.UI.Page
    {
        private static string pageName = "SampleResponse.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }


        static bool IsValidInsert( string TextMustExit, string Type, string Status, string Response)
        {

          
            if (string.IsNullOrEmpty(TextMustExit))
            {
                return false;
            }
            if (string.IsNullOrEmpty(Type))
            {
                return false;
            }
            if (string.IsNullOrEmpty(Status))
            {
                return false;
            }
            if (string.IsNullOrEmpty(Response))
            {
                return false;
            }
           
            if (Type.Equals("Callback", StringComparison.InvariantCultureIgnoreCase))
            {
                if (!Response.Contains("[RequestID]"))
                {
                    return false;
                }
               
            }
            return true;
        }

        [WebMethod]
        public static MessageWithIcon Insert(int ProviderID, string TextMustExit, string Type, string Status, string Response)
        {
            AccessController.checkAccess(pageName);



            MessageWithIcon msgIcon = new MessageWithIcon();
            if (!IsValidInsert( TextMustExit, Type, Status, Response))
            {
                msgIcon.Message = Message.InvalidData;
                return msgIcon;
            }

            msgIcon.Message = "Response not added";
            int result = -1;

            try
            {

                SampleResponseViewModel ObjModel = new SampleResponseViewModel();
                ObjModel.ProviderID = ProviderID;
                ObjModel.TextMustExit = TextMustExit;
                ObjModel.Type = Type;
                ObjModel.Status = Status;
                ObjModel.Response = Response;
                FinovativeSampleResponseBal ObjBAL = new FinovativeSampleResponseBal();
                result = ObjBAL.Insert(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result < 1)
                return msgIcon;


            msgIcon.Message = " Sample Response added successfully";
            return msgIcon;
        }





        [WebMethod]
        public static SampleResponseViewModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<SampleResponseViewModel> details = new List<SampleResponseViewModel>();
            DataTable dt = new DataTable();

            try
            {

                FinovativeSampleResponseBal objBAL = new FinovativeSampleResponseBal();

                dt = objBAL.selectSampleResponseReport();
                if (dt.Rows.Count <= 0)
                {
                    return details.ToArray();
                }
                foreach (DataRow dtrow in dt.Rows)
                {

                    Model.SampleResponseViewModel report = new Model.SampleResponseViewModel();
                    report.SampleResponseID = Convert.ToInt32(dtrow["SampleResponseID"].ToString());
                    report.ProviderID = Convert.ToInt32(dtrow["ProviderID"].ToString());
                    report.DeveloperName = (dtrow["DeveloperName"].ToString());
                    report.Type = (dtrow["Type"].ToString());
                    report.TextMustExit = (dtrow["TextMustExit"].ToString());
                    report.Response = (dtrow["Response"].ToString());
                    report.Status = (dtrow["Status"].ToString());

                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Model.Logger().write(ex);
            }
            return details.ToArray();
        }


        [WebMethod]
        public static SampleResponseViewModel SelectByID(int SampleResponseID)
        {
            AccessController.checkAccess(pageName);
            SampleResponseViewModel report = new SampleResponseViewModel();

            FinovativeSampleResponseBal objBal = new FinovativeSampleResponseBal();
            try
            {
                report = objBal.SelectByID(SampleResponseID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return report;
        }

        [WebMethod]
        public static MessageWithIcon Update(int SampleResponseID, int ProviderID, string TextMustExit, string Type, string Status, string Response)
        {
            AccessController.checkAccess(pageName);



            MessageWithIcon msgIcon = new MessageWithIcon();
            if (!IsValidInsert( TextMustExit, Type, Status, Response))
            {
                msgIcon.Message = Message.InvalidData;
                return msgIcon;
            }

            msgIcon.Message = "Response not updated";
            int result = -1;

            try
            {

                SampleResponseViewModel ObjModel = new SampleResponseViewModel();
                ObjModel.SampleResponseID = SampleResponseID;
                ObjModel.ProviderID = ProviderID;
                ObjModel.TextMustExit = TextMustExit;
                ObjModel.Type = Type;
                ObjModel.Status = Status;
                ObjModel.Response = Response;

                FinovativeSampleResponseBal ObjBAL = new FinovativeSampleResponseBal();
                result = ObjBAL.Update(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result < 1)
                return msgIcon;


            msgIcon.Message = " Sample Response updated successfully";
            return msgIcon;
        }



        [WebMethod]
        public static MessageWithIcon Delete(int SampleResponseID)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            FinovativeSampleResponseBal objBal = new FinovativeSampleResponseBal();
            try
            {
                i = objBal.Delete(SampleResponseID);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i > 0)
                msgIcon.Message = "Sample Response not deleted";
            else
                msgIcon.Message = "Sample Response deleted Successfully";
            return msgIcon;
        }

        
            [WebMethod]
        public static SampleResponseViewModel[] SelectBYProviderID(int ProviderID)
        {
            AccessController.checkAccess(pageName);
            List<SampleResponseViewModel> details = new List<SampleResponseViewModel>();
            DataTable dt = new DataTable();

            try
            {

                FinovativeSampleResponseBal objBAL = new FinovativeSampleResponseBal();

                dt = objBAL.SelectBYProviderID(ProviderID);
                if (dt.Rows.Count <= 0)
                {
                    return details.ToArray();
                }
                foreach (DataRow dtrow in dt.Rows)
                {

                    Model.SampleResponseViewModel report = new Model.SampleResponseViewModel();
                    report.SampleResponseID = Convert.ToInt32(dtrow["SampleResponseID"].ToString());
                    report.ProviderID = Convert.ToInt32(dtrow["ProviderID"].ToString());
                    report.DeveloperName = (dtrow["DeveloperName"].ToString());
                    report.Type = (dtrow["Type"].ToString());
                    report.TextMustExit = (dtrow["TextMustExit"].ToString());
                    report.Response = (dtrow["Response"].ToString());
                    report.Status = (dtrow["Status"].ToString());

                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Model.Logger().write(ex);
            }
            return details.ToArray();
        }
    }
}