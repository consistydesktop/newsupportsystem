﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TimeSheet123.aspx.cs" Inherits="SupportSystem.TimeSheet123" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Time Sheet
                                <button class="btn btn-info buttons float-right" type="button" title="View TimeSheet" onclick="location.href='/TimeSheetReport.aspx'">View TimeSheet</button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
        </section>

        <form class="formbox mt-4">

            <div class="form-group row mt-3">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputDate">Date</label>
                    <input type="text" class="form-control loginput" id="inputDate" aria-describedby="emailHelp" placeholder="Select Date" title="Select Date" onchange='$("#inputRemark").focus()' />
                </div>

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputRemark">Remark</label>
                    <textarea class="form-control loginput" id="inputRemark" aria-describedby="emailHelp" placeholder="Enter Remark" title="Enter Remark"></textarea>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" onclick=" return TimeSheetMasterInsert();" title="Submit TimeSheet">Submit</button>
                    <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="UpdateButton" style="display: none" onclick=" return TimeSheetMasterUpdate();" title="Update TimeSheet">Update</button>
                </div>
            </div>

            <div class="form-group row mt-3">
                <label id="TimeSheetMasterID" style="display: none"></label>
                <label id="TimeSheetDetailID" style="display: none"></label>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                    <table id="EditTimeSheet" class="table" style="width: 100%" title="Today's Time Sheet">
                        <thead>
                            <tr>
                                <th>System Name </th>
                                <th>Task</th>
                                <th>Time (In Min)</th>
                                <th>ADD/Update</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="row">

                        <div class="col-6">
                            <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="AddButton" onclick="InsertRecord()" title="Add Record">Add</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="wrapper">
        <section>
            <div class="container-fluid mt-4">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="TimeSheetDetails" class="table table-bordered nowrap table-hover">
                                <thead>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="/JS/TimeSheet.js"></script>
    <script>
        var count = 0;
        var List = [];
        var JSonObjects = [];
        var TotalTime = 0;
        var SystemObjects = [];

        $(document).ready(function () {
            SystemMasterSelect();
            TimeSheetMasterSelect();
            $("#AddButton").focus();

        });
        $("#inputDate").datepicker({
            //dateFormat: 'dd-mm-yy',

            dateFormat: 'd MM, yy',
            maxDate: 0,
            changeMonth: true,
            changeYear: true
        });
        $("#inputDate").datepicker("setDate", new Date());

        function IsNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57));
            return ret;
        }
    </script>
</asp:Content>