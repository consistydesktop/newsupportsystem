﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class NewTestProject : System.Web.UI.Page
    {
        private static string pageName = "NewBug.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {

            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static MessageWithIcon Insert(string ProjectName)
        {

            AccessController.checkAccess(pageName);
            int result = 0;
            MessageWithIcon msgIcon = new MessageWithIcon();
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());

            NewBugModel ObjModel = new NewBugModel();

            ObjModel.ProjectName = ProjectName.Trim();

            NewBugBal ObjBal = new NewBugBal();

            if (ObjBal.ExistProjectName(ObjModel.ProjectName))
            {
                msgIcon.Message = "Project already exists.";
                return msgIcon;
            }

            msgIcon.Message = "Project not created.";
            try
            {
                result = ObjBal.Insert(ObjModel, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (result > 0)
                msgIcon.Message = "Project created successfully.";

            return msgIcon;
        }

        [WebMethod]
        public static NewBugModel[] Select()
        {

            AccessController.checkAccess(pageName);
            List<NewBugModel> details = new List<NewBugModel>();
            NewBugBal objBAL = new NewBugBal();
            try
            {
                details = objBAL.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Delete(int ProjectBugMasterID)
        {

            AccessController.checkAccess(pageName);
            int result = 0;
            MessageWithIcon msgIcon = new MessageWithIcon();
            NewBugBal ObjBal = new NewBugBal();
            try
            {
                result = ObjBal.Delete(ProjectBugMasterID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            msgIcon.Message = "Project not Deleted.";

            if (result > 0)
                msgIcon.Message = "Project Deleted successfully.";


            return msgIcon;
        }
    }
}