﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class Forgot : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static MessageWithIcon ForgotPassword(string MobileNumber, string EmailID)
        {
            AccessController objAC = new AccessController();
            ForgotModel objModel = new ForgotModel();
            MessageWithIcon msgIcon = new MessageWithIcon();


            DataTable dt = new DataTable();
            ForgotBal objBAL = new ForgotBal();
            string Password = objAC.autoGeneratePass();
            objModel.MobileNumber = MobileNumber;
            objModel.Password = Password;
            objModel.EmailID = EmailID;
            try
            {
                dt = objBAL.ForgotPassword(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (dt.Rows.Count <= 0)
            {
                msgIcon.Message = "Enter registered mobile number or email";
                return msgIcon;
            }
            else
            {
                foreach (DataRow dtrow in dt.Rows)
                {
                    Password = dtrow["Password"].ToString().ToUpper().Trim();
                    MobileNumber = dtrow["MobileNumber"].ToString().ToUpper().Trim();
                    EmailID = dtrow["EmailID"].ToString().Trim();

                    string PasswordMessage = string.Format("Your Password is {0}", Password);
                  
                    MessageProcessing.SendPassword(MobileNumber, Password);

                    string Subject = "Consisty Support System";
                    string Email = dt.Rows[0]["EmailID"].ToString(); ;
                    Forgot obj = new Forgot();
                  
                    PasswordMessage = obj.TransferMsg(PasswordMessage);
                  
                    MessageProcessing.SendEmail(Email, PasswordMessage, Subject);

                 
                    msgIcon.Message = "New password has been sent on your registered mobile number and email";
                    return msgIcon;
                }
            }

            return msgIcon;
        }

        private string TransferMsg(string Message)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Password.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{0}", Message);

            return body;
        }
    }
}