﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

namespace SupportSystem
{
    public partial class FinoPerformance : System.Web.UI.Page
    {
        static string pageName = "FinoPerformance.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }
        [WebMethod]
        public static FinoPerformanceViewModel[] Select(string FromDate, string ToDate, string ClientName, string SalesType)
        {

            AccessController.checkAccess(pageName);

            List<FinoPerformanceViewModel> details = new List<FinoPerformanceViewModel>();
            FinnoPerformanceBal ObjBAL = new FinnoPerformanceBal();
            try
            {
                FinoPerformanceViewModel ObjModel = new FinoPerformanceViewModel();

                ObjModel.FormDate = FromDate;
                ObjModel.Todate = ToDate;
                ObjModel.ClientName = ClientName;
                ObjModel.SalesType = SalesType;


                DataTable dt = ObjBAL.Search(ObjModel);

                if (dt.Rows.Count <= 0)
                {
                    return details.ToArray();
                }
                Validation validation = new Validation();

                foreach (DataRow dtrow in dt.Rows)
                {
                    FinoPerformanceViewModel report = new FinoPerformanceViewModel();

                    report.FinnoPerformanceID = Convert.ToInt32(dtrow["FinnoPerformanceID"].ToString());
                    report.SuccessCount = Convert.ToInt32(dtrow["SuccessCount"].ToString());
                    report.FailCount = Convert.ToInt32(dtrow["FailCount"].ToString());
                    //report.SuccessAmount = Convert.ToDecimal(dtrow["SuccessAmount"].ToString());


                    report.SuccessAmount = validation.AmountInIndianCurrency(Convert.ToDecimal(dtrow["SuccessAmount"].ToString()));
                    report.FailAmount = validation.AmountInIndianCurrency(Convert.ToDecimal(dtrow["FailAmount"].ToString()));

                    //  report.FailAmount = Convert.ToDecimal(dtrow["FailAmount"].ToString());
                    report.ClientName = dtrow["ClientName"].ToString().Trim();
                    report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                    report.DOC = dtrow["DOC"].ToString().Trim();
                    if (report.DOC.Length > 0)
                    {
                        DateTime datet = Convert.ToDateTime(report.DOC);
                        report.DOC = datet.ToString("yyyy-MM-dd");
                    }

                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }
        [WebMethod]
        public static ClientViewModel[] ClientSelect()
        {

            AccessController.checkAccess(pageName);
            List<ClientViewModel> details = new List<ClientViewModel>();
            FinnoPerformanceBal objBAL = new FinnoPerformanceBal();
            try
            {
                details = objBAL.ClientSelect();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }
    }
}