﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class TransactionDetailReport : System.Web.UI.Page
    {
        static string pageName = "TransactionDetailReport.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }
        [WebMethod]
        public static TransactionMasterModel[] SelectTransactionMaster()
        {
            AccessController.checkAccess(pageName);
            List<TransactionMasterModel> details = new List<TransactionMasterModel>();
            AccountBal ObjBAL = new AccountBal();
            try
            {
                string FromDate = DateTime.Now.AddDays(-30).ToString("dd MMM, yyyy");
                string ToDate = DateTime.Now.ToString("dd MMM, yyyy");
                

                details = ObjBAL.Select(FromDate, ToDate);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }
        [WebMethod]
        public static TransactionMasterModel[] SelectByDate(string FromDate, string ToDate,string PaymentType,string TransactionType,int BankMasterID,int IsGST)
        {
            AccessController.checkAccess(pageName);
            List<TransactionMasterModel> details = new List<TransactionMasterModel>();
            AccountBal ObjBAL = new AccountBal();

            try
            {
                details = ObjBAL.SelectByDate(FromDate, ToDate,PaymentType,TransactionType,BankMasterID,IsGST);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }
    }
}