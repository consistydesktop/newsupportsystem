﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Notification.aspx.cs" Inherits="SupportSystem.Notification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Notification
                                
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 ">
                        <label for="inputNotification">Enter Notification</label>
                        <textarea class="form-control loginput" id="inputNotification" title="Notification" aria-describedby="emailHelp" placeholder="Enter Notification"></textarea>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">

                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="NotificationMasterInsert()" id="SubmitButton" title="Add Notification">ADD</button>
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" style="display: none" onclick="NotificationMasterUpdate()" id="UpdateButton" title="Update Notification">Update</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick=" Reset();" title="Cancel Notification">Cancel</button>
                            </div>
                        </div>
                    </div>



                </div>
            </form>

            <label id="NotificationID" style="display: none"></label>

            <div class="table-responsive">
                <table id="NotificationMasterDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                    <thead></thead>
                    <tbody></tbody>
                </table>
            </div>
        </section>
    </div>
    <script src="/JS/Notification.js"></script>
    <script>
        $(document).ready(function () {

            NotificationMasterSelect();
            $("#inputNotification").focus();
        });

        function Reset() {
            $("#inputNotification").val("");
        }
    </script>
</asp:Content>
