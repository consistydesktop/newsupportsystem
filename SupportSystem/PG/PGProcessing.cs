﻿
using Bal;

using Model;
using Newtonsoft.Json;
using SupportProjectEmployee.Controller;
using SupportSystem.Controller;
using SupportSystem.StaticClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SupportSystem.PG
{
    public class PGProcessing
    {
        private Message message = new Message();
        private Logger log = new Logger();

        public string ExtendSubscriptionProcessing(LoadMoneyModel objModel)
        {
            string result = "";
            try
            {
                //ExtendSubscription
                SubscritpionRenovationBal objBAL = new SubscritpionRenovationBal();
                int Month = Convert.ToInt32(objModel.Month);

                int i = objBAL.ExtendSubscription(objModel.ProductID, objModel.DaysMonthType, objModel.UserID, objModel.SystemID);

                if (i > 0)
                {
                    DataTable dt1 = objBAL.SelectUser(objModel.UserID);
                    string MobileNo = dt1.Rows[0]["MobileNumber"].ToString();
                    MessageProcessing.SendSMS(MobileNo, AllMessages.PGSubscriptionMessage);

                    SubscriptionCallBack(objModel.ProductID, objModel.UserID, objModel.SystemID);

                }



            }
            catch (Exception ex)
            {
                log.write(ex);
                return string.Format(message.somethingWentWrong);
            }
            return result;
        }


        public string SubscriptionCallBack(int ProductID, int UserID, int SystemID)
        {
            EncryptDeryptSubscription ed = new EncryptDeryptSubscription();
            string jsonResponse = "";
            string result = "";
            try
            {
                WebSubscriptionResponseModel Response = new WebSubscriptionResponseModel();


                AddWebSubscriptionBal ObjBal = new AddWebSubscriptionBal();
                DataTable dt1 = new DataTable();
                dt1 = ObjBal.getEndDateForFinovativeBySystemID(SystemID);

                if (dt1.Rows.Count < 1)
                {
                    Response.Status = AllMessages.Failure;
                    string ReturnMessage = AllMessages.NotFound;
                    ReturnMessage = ReturnMessage.Replace("[Parameter]", "Requested URL");
                    Response.Message = ReturnMessage;
                    jsonResponse = JsonConvert.SerializeObject(Response);
                }

                DateTime EndDate = Convert.ToDateTime(dt1.Rows[0]["EndDate"]);

                if (DateTime.Compare(EndDate, default(DateTime)) == 0)
                {
                    Response.Status = AllMessages.Failure;
                    string ReturnMessage = AllMessages.NotFound;
                    ReturnMessage = ReturnMessage.Replace("[Parameter]", "Requested URL");
                    Response.Message = ReturnMessage;
                    jsonResponse = JsonConvert.SerializeObject(Response);

                }

                DateTime Today = DateTime.Now;
                int remainingDays = (EndDate - Today).Days;


                Response.Status = AllMessages.Success;
                Response.EndDate = EndDate;
                Response.Message = "Remaining days are ." + remainingDays;
                Response.RemainingDays = remainingDays;
                jsonResponse = JsonConvert.SerializeObject(Response);
                jsonResponse = ed.Encrypt(jsonResponse);


                //  string   jsonResponse1 = ed.Decrypt(jsonResponse);
                string URL = "";
                URL = dt1.Rows[0]["URL"].ToString();

                HitCallBack(jsonResponse, UserID, ProductID, SystemID, URL);

            }
            catch (Exception ex)
            {
                log.write(ex);
                return string.Format(message.somethingWentWrong);
            }
            return result;
        }

        private void HitCallBack(string jsonResponse, int UserID, int ProductID, int SystemID, string URL = "")
        {
            SubscritpionRenovationBal objBAL = new SubscritpionRenovationBal();
            DataTable dt1 = objBAL.SelectCallBackURL(UserID, ProductID, SystemID);
            string SubscriptionCallbackURL = "";
            string Methodtype = "GET";
            string HeaderJson = "{\"Content - Type\":\"application / json\"}";



            if (dt1.Rows.Count < 1)
            {

                if (URL.Equals("", StringComparison.InvariantCultureIgnoreCase))
                    return;


                if (!URL.ToLower().Contains("http"))
                    URL = "http://" + URL;


                string[] arr = System.Text.RegularExpressions.Regex.Split(URL, "://");
                string Markup = arr[0];
                string DomainURL = arr[1];

                DomainURL = "api." + DomainURL;
                URL = Markup + DomainURL;

                SubscriptionCallbackURL = URL;

                if (ProductID == 6)
                    SubscriptionCallbackURL = SubscriptionCallbackURL + "/SubscriptionCallBack/SubscriptionResponse";

                if (ProductID == 1)
                    SubscriptionCallbackURL = SubscriptionCallbackURL + "/SubscriptionCallBack.aspx";

            }

            if (dt1.Rows.Count > 1)
            {
                SubscriptionCallbackURL = dt1.Rows[0]["SubscriptionCallbackURL"].ToString();
                Methodtype = dt1.Rows[0]["Methodtype"].ToString();
                HeaderJson = dt1.Rows[0]["HeaderJson"].ToString();
            }

            string Url = "[CallBackUrl]?[Response]";

            Url = Url.Replace("[CallBackUrl]", SubscriptionCallbackURL.Trim());
            Url = Url.Replace("[Response]", jsonResponse.Trim());
            if (Methodtype.Equals("GET", StringComparison.InvariantCultureIgnoreCase))
            {
                Task.Run(() =>
                {
                    HttpProcessing ht = new HttpProcessing();
                    string message = ht.postMethod(Url);
                });
            }



        }
    }
}