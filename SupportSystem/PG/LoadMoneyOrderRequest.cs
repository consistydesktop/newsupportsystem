﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportSystem.PG
{
    public class LoadMoneyOrderRequest
    {
        public int amount { get; set; }
        public string currency { get; set; }
        public string receipt { get; set; }

    }
}