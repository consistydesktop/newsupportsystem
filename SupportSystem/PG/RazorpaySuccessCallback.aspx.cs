﻿
using Bal;
using Model;
using Newtonsoft.Json;
using SupportProjectEmployee.Controller;
using SupportSystem.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem.PG
{
    public partial class RazorpaySuccessCallback : System.Web.UI.Page
    {
        private Message msg = new Message();
        protected void Page_Load(object sender, EventArgs e)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            string paymentId = "";
            string orderID = "";
            string signature = "";
            string status = "";
            string payMode = "";
            string TransactionID = "";
            int UserId = 0;

            string pgStatus = "", pgResponse = "";

            string response = "";

            foreach (string key in Request.Form.Keys)
            {
                parameters.Add(key.Trim(), Request.Form[key].Trim());

                response += "  Key :" + key.Trim() + "  Value: " + Request.Form[key].Trim();
            }
            response = response.Trim();

            new Logger().LogInfo("RazorpaySuccessCallback", "Page_Load", response);


            if (parameters.ContainsKey("razorpay_payment_id"))
            {
                paymentId = parameters["razorpay_payment_id"];
            }

            if (parameters.ContainsKey("razorpay_order_id"))
            {
                orderID = parameters["razorpay_order_id"];
            }

            if (parameters.ContainsKey("razorpay_signature"))
            {
                signature = parameters["razorpay_signature"];
            }


            string VerifySignatureResponse = VerifySignature(paymentId, orderID, signature);

            SubscritpionRenovationBal objPGBal = new SubscritpionRenovationBal();
            DataTable dt = objPGBal.Select(orderID);

            if (VerifySignatureResponse.Equals("FAILURE"))
            {
                lblAmount.Text = "";
                lblOrderNumber.Text = "";
                lblPgRef.Text = "";
                lblResponseMessage.Text = "Invalid Signature";
                lblTransactionMode.Text = ""; // obj.Txndate.ToString("dd/MM/yyyy hh:mm");
                lblTransactionStatus.Text = ""; // obj.Status;
                #region SessionManagement

                try
                {
                    HttpContext.Current.Session["UserID"] = UserId;
                    HttpContext.Current.Session["UserTypeID"] = Convert.ToInt32(dt.Rows[0]["UserTypeID"].ToString());
                    HttpContext.Current.Session["UserType"] = dt.Rows[0]["UserType"].ToString();
                    HttpContext.Current.Session["MobileNumber"] = dt.Rows[0]["MobileNumber"].ToString();
                    HttpContext.Current.Session["Username"] = dt.Rows[0]["Username"].ToString();
                    HttpContext.Current.Session["EmailID"] = dt.Rows[0]["EmailID"].ToString();
                    HttpContext.Current.Session["Token"] = "sadsadasdds";
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }

                #endregion SessionManagement

                return;
            }

            if (dt == null)
            {
                //Response.Write("Invalid Data");
                //lblAgRef.Text = "";
                lblAmount.Text = "";
                lblOrderNumber.Text = "";
                lblPgRef.Text = "";
                lblResponseMessage.Text = "Invalid Data";
                lblTransactionMode.Text = ""; // obj.Txndate.ToString("dd/MM/yyyy hh:mm");
                lblTransactionStatus.Text = ""; // obj.Status;

                #region SessionManagement

                try
                {
                    HttpContext.Current.Session["UserID"] = UserId;
                    HttpContext.Current.Session["UserTypeID"] = Convert.ToInt32(dt.Rows[0]["UserTypeID"].ToString());
                    HttpContext.Current.Session["UserType"] = dt.Rows[0]["UserType"].ToString();
                    HttpContext.Current.Session["MobileNumber"] = dt.Rows[0]["MobileNumber"].ToString();
                    HttpContext.Current.Session["Username"] = dt.Rows[0]["Username"].ToString();
                    HttpContext.Current.Session["EmailID"] = dt.Rows[0]["EmailID"].ToString();
                    HttpContext.Current.Session["Token"] = "sadsadasdds";
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }

                #endregion SessionManagement

                return;
            }

            if (dt.Rows.Count <= 0)
            {
                //Response.Write("Invalid Data");

                lblAmount.Text = "";
                lblOrderNumber.Text = "";
                lblPgRef.Text = "";
                lblResponseMessage.Text = "Invalid Data";
                lblTransactionMode.Text = ""; // obj.Txndate.ToString("dd/MM/yyyy hh:mm");
                lblTransactionStatus.Text = ""; // obj.Status;

                #region SessionManagement

                try
                {
                    HttpContext.Current.Session["UserID"] = UserId;
                    HttpContext.Current.Session["UserTypeID"] = Convert.ToInt32(dt.Rows[0]["UserTypeID"].ToString());
                    HttpContext.Current.Session["UserType"] = dt.Rows[0]["UserType"].ToString();
                    HttpContext.Current.Session["MobileNumber"] = dt.Rows[0]["MobileNumber"].ToString();
                    HttpContext.Current.Session["Username"] = dt.Rows[0]["Username"].ToString();
                    HttpContext.Current.Session["EmailID"] = dt.Rows[0]["EmailID"].ToString();
                    HttpContext.Current.Session["Token"] = "sadsadfdfvasdds";
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }

                #endregion SessionManagement

                return;
            }

            if (!dt.Rows[0]["status"].ToString().Equals(new Message().pending, StringComparison.InvariantCultureIgnoreCase))
            {
                lblAmount.Text = "";
                lblOrderNumber.Text = "";
                lblPgRef.Text = "";
                lblResponseMessage.Text = "Txn not pending Mismatch";
                lblTransactionMode.Text = ""; // obj.Txndate.ToString("dd/MM/yyyy hh:mm");
                lblTransactionStatus.Text = ""; // obj.Status;
                //Response.Write("Txn not pending Mismatch");

                #region SessionManagement

                try
                {
                    HttpContext.Current.Session["UserID"] = UserId;
                    HttpContext.Current.Session["UserTypeID"] = Convert.ToInt32(dt.Rows[0]["UserTypeID"].ToString());
                    HttpContext.Current.Session["UserType"] = dt.Rows[0]["UserType"].ToString();
                    HttpContext.Current.Session["MobileNumber"] = dt.Rows[0]["MobileNumber"].ToString();
                    HttpContext.Current.Session["Username"] = dt.Rows[0]["Username"].ToString();
                    HttpContext.Current.Session["EmailID"] = dt.Rows[0]["EmailID"].ToString();
                    HttpContext.Current.Session["Token"] = "sadsadfdfvasdds";
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }

                #endregion SessionManagement

                return;
            }
            HttpProcessing processing = new HttpProcessing();

            string Response = processing.GetMethodForPayment(paymentId);

            RazorpayStatusCheckByPaymentIDResponse objStatus = new RazorpayStatusCheckByPaymentIDResponse();

            try
            {

                objStatus = JsonConvert.DeserializeObject<RazorpayStatusCheckByPaymentIDResponse>(Response);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }


            int amt = Convert.ToInt32(objStatus.amount) / 100;

            decimal PGAmount = Convert.ToDecimal(amt);
            decimal DBAmount = Convert.ToDecimal(dt.Rows[0]["Amount"].ToString());

            int result = 0;
            PGModel objModel = new PGModel();

            if (objStatus == null)
            {
                objModel.TransactionID = "";
                objModel.status = new Message().failure;
                objModel.OrderId = orderID;
                objModel.Amount = DBAmount;
                objModel.PaymentGatewayID = 1;
                objModel.PayMode = "";
                objModel.TransactionID = objModel.TransactionID;
                objModel.PaymentId = "";
                objModel.response = Response;

                result = objPGBal.Update(objModel);
                return;
            }

            pgStatus = objStatus.status;
            objModel.TransactionID = objStatus.acquirer_data.rrn;
            if (objModel.TransactionID == null)
            {
                objModel.TransactionID = objStatus.acquirer_data.auth_code;
            }
            if (objModel.TransactionID == null)
            {
                objModel.TransactionID = objStatus.acquirer_data.bank_transaction_id;
            }
            objModel.PayMode = objStatus.method;

            if (pgStatus.Equals("captured", StringComparison.InvariantCultureIgnoreCase) || pgStatus.Equals("authorized", StringComparison.InvariantCultureIgnoreCase))
            {
                objModel.status = new Message().success;
            }
            else if (pgStatus.Equals("failed", StringComparison.InvariantCultureIgnoreCase))
            {
                objModel.TransactionID = "";
                objModel.status = new Message().failure;
            }
            else
            {
                objModel.status = new Message().pending;
            }


            if (DBAmount != PGAmount)
            {
                //Response.Write("Amount Mismatch");

                lblAmount.Text = "";
                lblOrderNumber.Text = "";
                lblPgRef.Text = "";
                lblResponseMessage.Text = "Amount Mismatch";
                lblTransactionMode.Text = ""; // obj.Txndate.ToString("dd/MM/yyyy hh:mm");
                lblTransactionStatus.Text = ""; // obj.Status;

                #region SessionManagement

                try
                {
                    HttpContext.Current.Session["UserID"] = UserId;
                    HttpContext.Current.Session["UserTypeID"] = Convert.ToInt32(dt.Rows[0]["UserTypeID"].ToString());
                    HttpContext.Current.Session["UserType"] = dt.Rows[0]["UserType"].ToString();
                    HttpContext.Current.Session["MobileNumber"] = dt.Rows[0]["MobileNumber"].ToString();
                    HttpContext.Current.Session["Username"] = dt.Rows[0]["Username"].ToString();
                    HttpContext.Current.Session["EmailID"] = dt.Rows[0]["EmailID"].ToString();
                    HttpContext.Current.Session["Token"] = "sadsacfsafdfdfvasdds";
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }

                #endregion SessionManagement

                return;
            }


            objModel.OrderId = orderID;
            objModel.Amount = PGAmount;
            objModel.CommisionAmount = 0;
            objModel.PaymentGatewayID = 1;
            objModel.PaymentId = paymentId;
            objModel.Signature = signature;
            objModel.response = Response;

            result = objPGBal.Update(objModel);



            UserId = Convert.ToInt32(dt.Rows[0]["UserID"].ToString());

            LoginModel loginModel = new LoginModel();
            loginModel.UserID = UserId;

            string token = "000";
            DataTable dtToken = new LoginBal().SelectTokenForGateway(loginModel);

            if (dtToken.Rows.Count <= 0)
            {
                token = "000";
            }
            else
            {
                token = dtToken.Rows[0]["Token"].ToString();
            }

            LoadMoneyModel objLoadMoneyUPI = new LoadMoneyModel();
            objLoadMoneyUPI.Amount = objModel.Amount;
          //  objLoadMoneyUPI.DaysMonth = Convert.ToInt32(dt.Rows[0]["DaysMonth"].ToString());
            objLoadMoneyUPI.DaysMonthType = dt.Rows[0]["DaysMonthType"].ToString();
            objLoadMoneyUPI.SystemID = Convert.ToInt32(dt.Rows[0]["SystemID"].ToString());
            objLoadMoneyUPI.ProductID = Convert.ToInt32(dt.Rows[0]["ProductID"].ToString());
            objLoadMoneyUPI.UserID = UserId;
            objLoadMoneyUPI.PaytmTransactionID = objModel.TransactionID;
            objLoadMoneyUPI.Paymode = objModel.PayMode.Trim();
            objLoadMoneyUPI.Remark = "Payment received from Amanmultilink PG(Web)";

            DataTable dt1 = new DataTable();
            AddWebSubscriptionBal ObjBal = new AddWebSubscriptionBal();
            dt1 = ObjBal.getEndDateForFinovativeBySystemID(objLoadMoneyUPI.SystemID);

            string SubscriptionEndDate = "";
            int remainingDays = 0;
            if (dt1.Rows.Count < 1)
            {
                SubscriptionEndDate = "";
                remainingDays = 0;
            }

            DateTime EndDate = Convert.ToDateTime(dt1.Rows[0]["EndDate"]);

            DateTime Today = DateTime.Now;
            remainingDays = (EndDate - Today).Days;
            SubscriptionEndDate = (EndDate.ToString("dd/MM/yyyy"));


            //// Check payment made successfully

            if (result <= 0)
            {
                lblAmount.Text = objModel.Amount.ToString();
                lblOrderNumber.Text = objModel.OrderId;
                lblPgRef.Text = objModel.TransactionID;
                lblResponseMessage.Text = "";
                lblTransactionMode.Text = objModel.PayMode.ToUpper();
                lblTransactionStatus.Text = objModel.status;

                #region SessionManagement

                try
                {
                    HttpContext.Current.Session["UserID"] = UserId;
                    HttpContext.Current.Session["UserTypeID"] = Convert.ToInt32(dt.Rows[0]["UserTypeID"].ToString());
                    HttpContext.Current.Session["UserType"] = dt.Rows[0]["UserType"].ToString();
                    HttpContext.Current.Session["MobileNumber"] = dt.Rows[0]["MobileNumber"].ToString();
                    HttpContext.Current.Session["Username"] = dt.Rows[0]["Username"].ToString();
                    HttpContext.Current.Session["EmailID"] = dt.Rows[0]["EmailID"].ToString();
                    HttpContext.Current.Session["Token"] = "sadsadfdfvasdds";
                    HttpContext.Current.Session["RemainingDays"] = remainingDays.ToString();
                    HttpContext.Current.Session["SubscriptionEndDate"] = SubscriptionEndDate.ToString();

                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }

                #endregion SessionManagement

                return;
            }




            if (objModel.status.Contains(new Message().success))
            {
                //lblAgRef.Text = obj.Orderid;
                lblAmount.Text = objModel.Amount.ToString();
                lblOrderNumber.Text = objModel.OrderId;
                lblPgRef.Text = objModel.TransactionID;
                lblResponseMessage.Text = "";
                lblTransactionMode.Text = objModel.PayMode.ToUpper();
                lblTransactionStatus.Text = objModel.status;

                #region UpdateBalanceSuccess

                PGProcessing Processing = new PGProcessing();
                Processing.ExtendSubscriptionProcessing(objLoadMoneyUPI);


             



                #endregion UpdateBalanceSuccess

                #region SessionManagement

                try
                {
                    HttpContext.Current.Session["UserID"] = UserId;
                    HttpContext.Current.Session["UserTypeID"] = Convert.ToInt32(dt.Rows[0]["UserTypeID"].ToString());
                    HttpContext.Current.Session["UserType"] = dt.Rows[0]["UserType"].ToString();
                    HttpContext.Current.Session["MobileNumber"] = dt.Rows[0]["MobileNumber"].ToString();
                    HttpContext.Current.Session["Username"] = dt.Rows[0]["Username"].ToString();
                    HttpContext.Current.Session["EmailID"] = dt.Rows[0]["EmailID"].ToString();
                    HttpContext.Current.Session["Token"] = token;
                    HttpContext.Current.Session["RemainingDays"] = remainingDays.ToString();
                    HttpContext.Current.Session["SubscriptionEndDate"] = SubscriptionEndDate.ToString();

                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }

                #endregion SessionManagement

                return;

            }
            else
            {
                //return RedirectToAction("Failed");
            }

        }


        [WebMethod]
        public string VerifySignature(string PaymentID, string OrderID, string Signature)
        {
            string json = new Message().failure;
            try
            {
                string generated_signature = CalcHMACSHA256Hash(OrderID + "|" + PaymentID, RazorpayInfo.RazorpaySecret);

                if (generated_signature.Equals(Signature))
                {
                    json = new Message().success;
                    return json;
                }
                SubscritpionRenovationBal objPGBal = new SubscritpionRenovationBal();
                PGModel objModel = new PGModel();
                objModel.OrderId = OrderID;
                objModel.Amount = 0;
                objModel.CommisionAmount = 0;
                objModel.PaymentGatewayID = 1;
                objModel.status = json;
                objModel.PayMode = "";
                objModel.TransactionID = "";
                objModel.PaymentId = PaymentID;
                objModel.Signature = Signature;
                objModel.response = PaymentID + "|" + OrderID + "|" + Signature;

                int result = objPGBal.Update(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                json = JsonConvert.SerializeObject(msg.ExceptionOccurred);
                return json;
            }
            return json;
        }
        public static string CalcHMACSHA256Hash(string message, string secret)
        {
            try
            {
                var encoding = new System.Text.ASCIIEncoding();
                byte[] keyByte = encoding.GetBytes(secret);
                byte[] messageBytes = encoding.GetBytes(message);
                using (var hmacsha256 = new HMACSHA256(keyByte))
                {
                    byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                    return BitConverter.ToString(hashmessage).Replace("-", string.Empty).ToLower();
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

    }
}