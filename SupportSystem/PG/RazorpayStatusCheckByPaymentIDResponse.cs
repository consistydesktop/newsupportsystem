﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportSystem.PG
{
    public class RazorpayStatusCheckByPaymentIDResponse
    {

        private string _id;

        public string id
        {
            get { return _id == null ? "" : _id.Trim(); }
            set { _id = value == null ? "" : value.Trim(); }
        }

        private string _entity;

        public string entity
        {
            get { return _entity == null ? "" : _entity.Trim(); }
            set { _entity = value == null ? "" : value.Trim(); }
        }

        private string _currency;

        public string currency
        {
            get { return _currency == null ? "" : _currency.Trim(); }
            set { _currency = value == null ? "" : value.Trim(); }
        }
        private string _status;

        public string status
        {
            get { return _status == null ? "" : _status.Trim(); }
            set { _status = value == null ? "" : value.Trim(); }
        }
        private string _order_id;

        public string order_id
        {
            get { return _order_id == null ? "" : _order_id.Trim(); }
            set { _order_id = value == null ? "" : value.Trim(); }
        }


        private string _invoice_id;

        public string invoice_id
        {
            get { return _invoice_id == null ? "" : _invoice_id.Trim(); }
            set { _invoice_id = value == null ? "" : value.Trim(); }
        }


        private string _international;

        public string international
        {
            get { return _international == null ? "" : _international.Trim(); }
            set { _international = value == null ? "" : value.Trim(); }
        }
        private string _method;

        public string method
        {
            get { return _method == null ? "" : _method.Trim(); }
            set { _method = value == null ? "" : value.Trim(); }
        }

        private string _refund_status;

        public string refund_status
        {
            get { return _refund_status == null ? "" : _refund_status.Trim(); }
            set { _refund_status = value == null ? "" : value.Trim(); }
        }

        private string _description;

        public string description
        {
            get { return _description == null ? "" : _description.Trim(); }
            set { _description = value == null ? "" : value.Trim(); }
        }


        private string _card_id;

        public string card_id
        {
            get { return _card_id == null ? "" : _card_id.Trim(); }
            set { _card_id = value == null ? "" : value.Trim(); }
        }

        private string _bank;

        public string bank
        {
            get { return _bank == null ? "" : _bank.Trim(); }
            set { _bank = value == null ? "" : value.Trim(); }
        }
        private string _wallet;

        public string wallet
        {
            get { return _wallet == null ? "" : _wallet.Trim(); }
            set { _wallet = value == null ? "" : value.Trim(); }
        }
        private string _vpa;

        public string vpa
        {
            get { return _vpa == null ? "" : _vpa.Trim(); }
            set { _vpa = value == null ? "" : value.Trim(); }
        }
        private string _email;

        public string email
        {
            get { return _email == null ? "" : _email.Trim(); }
            set { _email = value == null ? "" : value.Trim(); }
        }
        private string _contact;

        public string contact
        {
            get { return _contact == null ? "" : _contact.Trim(); }
            set { _contact = value == null ? "" : value.Trim(); }
        }
        private string _error_code;

        public string error_code
        {
            get { return _error_code == null ? "" : _error_code.Trim(); }
            set { _error_code = value == null ? "" : value.Trim(); }
        }
        private string _error_description;

        public string error_description
        {
            get { return _error_description == null ? "" : _error_description.Trim(); }
            set { _error_description = value == null ? "" : value.Trim(); }
        }
        private string _error_source;

        public string error_source
        {
            get { return _error_source == null ? "" : _error_source.Trim(); }
            set { _error_source = value == null ? "" : value.Trim(); }
        }
        private string _error_step;

        public string error_step
        {
            get { return _error_step == null ? "" : _error_step.Trim(); }
            set { _error_step = value == null ? "" : value.Trim(); }
        }
        private string _error_reason;

        public string error_reason
        {
            get { return _error_reason == null ? "" : _error_reason.Trim(); }
            set { _error_reason = value == null ? "" : value.Trim(); }
        }


        private string _fee;

        public string fee
        {
            get { return _fee == null ? "" : _fee.Trim(); }
            set { _fee = value == null ? "" : value.Trim(); }
        }
        private string _tax;

        public string tax
        {
            get { return _tax == null ? "" : _tax.Trim(); }
            set { _tax = value == null ? "" : value.Trim(); }
        }

        private string _amount;

        public string amount
        {
            get { return _amount == null ? "" : _amount.Trim(); }
            set { _amount = value == null ? "" : value.Trim(); }
        }

        public AcquirerData acquirer_data { get; set; }
 

        private string _amount_refunded;

        public string amount_refunded
        {
            get { return _amount_refunded == null ? "" : _amount_refunded.Trim(); }
            set { _amount_refunded = value == null ? "" : value.Trim(); }
        }
        private string _created_at;

        public string created_at
    {
            get { return _created_at == null ? "" : _created_at.Trim(); }
            set { _created_at = value == null ? "" : value.Trim(); }
        }

        public bool captured { get; set; }



        public List<object> notes { get; set; }
    }



}