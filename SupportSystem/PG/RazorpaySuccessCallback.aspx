﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RazorpaySuccessCallback.aspx.cs" Inherits="SupportSystem.PG.RazorpaySuccessCallback" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <style>
        table, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        td {
            padding: 10px;
            font-size: 16px;
        }
    </style>



  <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Receipt
                            </h6>
                        </div>
                    </div>
                </div>
            </section>



            <div class="equalheight row" style="padding-top: 10px;">
                <div id="cs-main-body" class="cs-text-size-default pad-bottom">
                    <div class="col-lg-12  equalheight-col pad-top">
                        <div style="padding-bottom: 13px;">
                            <p style="font-size: 27px; padding: 6px 0px;">Thank you!</p>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p style="font-size: 16px; padding: 6px 0px;">Your payment is completed. Here is the details for it</p>
                                </div>
                                <div class="col-lg-12">
                                    <p style="font-size: 16px; padding: 6px 0px;">
                                        <asp:Label ID="lblResponseMessage" runat="server"> </asp:Label>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <table style="width: 100%; margin-left: 16px;">
                                    <tr>
                                        <td>Order Number</td>
                                        <td>
                                            <asp:Label ID="lblOrderNumber" runat="server"> </asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Amount</td>
                                        <td>
                                            <asp:Label ID="lblAmount" runat="server"> </asp:Label></td>
                                    </tr>
                                   

                                    <tr>
                                        <td>Transaction ID</td>
                                        <td>
                                            <asp:Label ID="lblPgRef" runat="server"> </asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Transaction Status</td>
                                        <td>
                                            <asp:Label ID="lblTransactionStatus" runat="server"> </asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Transaction Mode</td>
                                        <td>
                                            <asp:Label ID="lblTransactionMode" runat="server"> </asp:Label></td>
                                    </tr>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4"></div>
                <div class="col-lg-4">
                    <a href="/SubscritpionRenovation.aspx" class="form-control btn btn-primary allinputbtn">Make Another Transaction</a>
                </div>
            </div>

        </section>
    </div>
    <script>

        $(document).ready(function () {
            hideLoader();
        });

    </script>
</asp:Content>
