﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PGDefaultCommission.aspx.cs" Inherits="SupportSystem.PG.PGSlab.PGDefaultCommission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="rechargereport mt-5">
        <div class="reporthead">
            <p>Default Commission</p>
        </div>

        <div class="form-row">

            <div class="col-lg-2 col-sm-12 col-md-2 col-xs-12">
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="fullbox">

                    <form class="form-block">
                        <div class="form-group">
                            <div class="row">

                                <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
                                    <label class="col-form-label bluedark">
                                        From Amount
                                    </label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-sm-12">
                                    <div class="input-group">
                                        <input type="text" id="txtFromAmount" title="From Amount" maxlength="6" placeholder="From Amount" class="form-control allinputtxt UprCase" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row vtop">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
                                    <label class="col-form-label bluedark">
                                        To Amount
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                    <div class="input-group">
                                        <input type="text" id="txtToAmount" title="To Amount" maxlength="6" placeholder="To Amount" class="form-control allinputtxt UprCase" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row vtop">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
                                    <label class="col-form-label bluedark">
                                        Retailer FixRs
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                    <div class="input-group">
                                        <input type="text" id="txtRetailerFixRs" title="Retailer FixRs" maxlength="6" placeholder="Retailer FixRs" class="form-control allinputtxt UprCase" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row vtop">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
                                    <label class="col-form-label bluedark">
                                        Retailer Flexi(%)
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                    <div class="input-group">
                                        <input type="text" id="txtRetailerFlexi" title="Retailer Flexi" maxlength="6" placeholder="Retailer Flexi" class="form-control allinputtxt UprCase" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row vtop">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
                                    <label class="col-form-label bluedark">
                                        Distributor FixRs
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                    <div class="input-group">
                                        <input type="text" id="txtDistributorFixRs" title="Distributor FixRs" maxlength="5" placeholder="Distributor FixRs" class="form-control allinputtxt UprCase" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row vtop">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
                                    <label class="col-form-label bluedark">
                                        Distributor Flexi(%)
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                    <div class="input-group">
                                        <input type="text" id="txtDistributorFlexi" title="Distributor Flexi" maxlength="5" placeholder="Distributor Flexi" class="form-control allinputtxt UprCase" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row vtop">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
                                    <label class="col-form-label bluedark">
                                        Super Distributor FixRs
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                    <div class="input-group">
                                        <input type="text" id="txtSuperDistributorFixRs" title="Super Distributor FixRs" maxlength="5" placeholder="Super Distributor FixRs" class="form-control allinputtxt UprCase" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row vtop">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
                                    <label class="col-form-label bluedark">
                                        Super Distributor Flexi(%)
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                    <div class="input-group">
                                        <input type="text" id="txtSuperDistributorFlexi" title="Super Distributor Flexi" maxlength="5" placeholder="Super Distributor Flexi" class="form-control allinputtxt UprCase" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
                                    <label for="ddlServices" class="col-form-label bluedark">PayMode</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-sm-12">
                                    <select class="form-control" id="ddlCardType" title="Select Card Type">
                                        <option value="0">Select PayMode</option>
                                        <option value="credit">Credit Card</option>
                                        <option value="netbanking">Net Banking</option>
                                        <option value="upi">UPI</option>
                                        <option value="emi">EMI</option>
                                        <option value="freecharge">FreeCharge</option>
                                        <option value="payzapp">PayZapp</option>
                                        <option value="airtelmoney">Airtel Money</option>
                                        <option value="mobikwik">MobiKwik</option>
                                        <option value="jiomoney">JioMoney</option>
                                        <option value="phonepe">PhonePe Wallet</option>
                                        <option value="paypal">PayPal Wallet</option>
                                        <option value="amazonpay">Amazon Pay Wallet</option>
                                        <option value="paytm">Paytm Wallet</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row vtop">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
                                    <label class="col-form-label bluedark">
                                        API Commission
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                    <div class="input-group">
                                        <input type="text" id="txtAdminCommission" title="API Commission" maxlength="5" placeholder="API Commission" class="form-control allinputtxt UprCase" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row vtop">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
                                    <label class="col-form-label bluedark">
                                        API Commission Type
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                    <div class="input-group">
                                        <select id="txtAdminCommissionType">
                                            <option value="P" selected="selected">%</option>
                                            <option value="RS">Rs</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row vtop">

                                <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
                                    <label class="col-form-label bluedark"></label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <button class="form-control btn btn-primary allinputbtn" type="button" id="btnAdd" title="ADD" onclick="AddCommission();">ADD</button>

                                    <input type="hidden" id="hdfID" />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <button class="form-control btn btn-primary allinputbtn" type="button" title="Reset" onclick="resetCommission();">Reset</button>
                                </div>
                            </div>
                            &nbsp&nbsp&nbsp&nbsp&nbsp

                                <label style="color: green">Note: Minus(-) indicates charges.</label>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-2 col-sm-12 col-md-2 col-xs-12">
            </div>
        </div>
    </div>
    <div class="allreport">
        <div class="reporthead">
            <p>Commission</p>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="table-responsive">

                    <table id="tblCommissionDtl" class="table table-striped table-bordered " cellspacing="0" width="100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="PGDefaultCommission.js"></script>

    <script>
        $(function () {
       
            SelectMoneyTransferCommission();
            $("#ddlCardType").chosen();
        });
    </script>
</asp:Content>
