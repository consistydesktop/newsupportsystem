﻿
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem.PG.PGSlab
{
    public partial class PGDefaultCommission : System.Web.UI.Page
    {
        //private static string pageName = "PGDefaultCommission.aspx";

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    //
        //    {
        //        AccessControl.checkAccess(pageName);
        //        AccessControl.checkToken();
        //    }
        //}

        //[WebMethod]
        //public static string AddDefaultCommission(decimal FromAmount, decimal ToAmount, decimal RetailerFixRs, decimal RetailerFlexi,
        //    decimal DistributorFixRs, decimal DistributorFlexi, decimal SuperDistributorFixRs, decimal SuperDistributorFlexi,
        //    decimal APICommission, string APICommissionType, string Paymode)
        //{
        //    string Response = string.Empty;
        //    try
        //    {
        //        Message message = new Message();
        //        if (InputValidationAddDefaultCommission(FromAmount, ToAmount, RetailerFixRs, RetailerFlexi, DistributorFixRs, DistributorFlexi, SuperDistributorFixRs, SuperDistributorFlexi) == false)
        //        {
        //            Response = message.validData;
        //            return Response;
        //        }

        //        UPIModel objMoneyTransferCommissionModel = new UPIModel();
        //        objMoneyTransferCommissionModel.FromAmount = FromAmount;
        //        objMoneyTransferCommissionModel.ToAmount = ToAmount;
        //        objMoneyTransferCommissionModel.RetailerFixRs = RetailerFixRs;
        //        objMoneyTransferCommissionModel.RetailerFlexi = RetailerFlexi;
        //        objMoneyTransferCommissionModel.DistributorFixRs = DistributorFixRs;
        //        objMoneyTransferCommissionModel.DistributorFlexi = DistributorFlexi;
        //        objMoneyTransferCommissionModel.SuperDistributorFixRs = SuperDistributorFixRs;
        //        objMoneyTransferCommissionModel.SuperDistributorFlexi = SuperDistributorFlexi;
        //        objMoneyTransferCommissionModel.APICommission = APICommission;
        //        objMoneyTransferCommissionModel.APICommissionType = APICommissionType;
        //        objMoneyTransferCommissionModel.Paymode = Paymode;

        //        UPIBal objDefaultMoneyTransferCommBAL = new UPIBal();
        //        int i = objDefaultMoneyTransferCommBAL.AddDefaultCommission(objMoneyTransferCommissionModel);

        //        if (i > 0)
        //        {
        //            Response = string.Format(message.successAdded, "Default commission");
        //            return Response;
        //        }

        //        Response = message.CommissionExistsAmount;
        //        return Response;
        //    }
        //    catch (Exception ex)
        //    {
        //        new Logger().write(ex);
        //    }
        //    return Response;
        //}

        //private static bool InputValidationAddDefaultCommission(decimal FromAmount, decimal ToAmount, decimal RetailerFixRs, decimal RetailerFlexi, decimal DistributorFixRs, decimal DistributorFlexi, decimal SuperDistributorFixRs, decimal SuperDistributorFlexi)
        //{
        //    if (FromAmount <= 0)
        //    {
        //        return false;
        //    }
        //    if (ToAmount <= 0)
        //    {
        //        return false;
        //    }

        //    if (FromAmount >= ToAmount)
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        //[WebMethod]
        //public static UPIModel[] SelectMoneyTransferCommission()
        //{
        //    List<UPIModel> listMoneyTransferCommissionModel = new List<UPIModel>();
        //    try
        //    {
        //        UPIBal objDefaultMoneyTransferCommBAL = new UPIBal();
        //        DataTable dt = objDefaultMoneyTransferCommBAL.SelectMoneyTransferCommission();
        //        if (dt == null || dt.Rows.Count == 0)
        //        {
        //            return listMoneyTransferCommissionModel.ToArray();
        //        }

        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            UPIModel objMoneyTransferCommissionModel = new UPIModel();
        //            objMoneyTransferCommissionModel.ID = Convert.ToInt32(dr["ID"]);
        //            objMoneyTransferCommissionModel.FromAmount = Convert.ToDecimal(dr["FromAmount"]);
        //            objMoneyTransferCommissionModel.ToAmount = Convert.ToDecimal(dr["ToAmount"]);
        //            objMoneyTransferCommissionModel.RetailerFixRs = Convert.ToDecimal(dr["RetailerFixRs"]);
        //            objMoneyTransferCommissionModel.RetailerFlexi = Convert.ToDecimal(dr["RetailerFlexi"]);
        //            objMoneyTransferCommissionModel.DistributorFixRs = Convert.ToDecimal(dr["DistributorFixRs"]);
        //            objMoneyTransferCommissionModel.DistributorFlexi = Convert.ToDecimal(dr["DistributorFlexi"]);
        //            objMoneyTransferCommissionModel.SuperDistributorFixRs = Convert.ToDecimal(dr["SuperDistributorFixRs"]);
        //            objMoneyTransferCommissionModel.SuperDistributorFlexi = Convert.ToDecimal(dr["SuperDistributorFlexi"]);
        //            objMoneyTransferCommissionModel.APICommission = Convert.ToDecimal(dr["APICommission"]);
        //            objMoneyTransferCommissionModel.APICommissionType = dr["APICommissionType"].ToString();
        //            objMoneyTransferCommissionModel.Paymode = dr["Paymode"].ToString();
        //            listMoneyTransferCommissionModel.Add(objMoneyTransferCommissionModel);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        new Logger().write(ex);
        //    }
        //    return listMoneyTransferCommissionModel.ToArray();
        //}

        //[WebMethod]
        //public static string DeleteDefaultSlab(int ID)
        //{
        //    string Response = string.Empty;
        //    UPIBal objDefaultMoneyTransferCommBAL = new UPIBal();
        //    try
        //    {
        //        Response = objDefaultMoneyTransferCommBAL.DeleteDefaultSlab(ID);
        //    }
        //    catch (Exception ex)
        //    {
        //        Response = "Something went Wrong";
        //    }
        //    return Response;
        //}

        //[WebMethod]
        //public static string UpdateDefaultCommission(decimal FromAmount, decimal ToAmount, decimal RetailerFixRs, decimal RetailerFlexi,
        //    decimal DistributorFixRs, decimal DistributorFlexi, decimal SuperDistributorFixRs, decimal SuperDistributorFlexi, int ID,
        //    decimal APICommission, string APICommissionType)
        //{
        //    string Response = string.Empty;
        //    try
        //    {
        //        if (InputValidationUpdateDefaultCommission(FromAmount, ToAmount, RetailerFixRs, RetailerFlexi, DistributorFixRs, DistributorFlexi, SuperDistributorFixRs, SuperDistributorFlexi) == false)
        //        {
        //            Response = "Please enter valid data";
        //            return Response;
        //        }

        //        UPIModel objMoneyTransferCommissionModel = new UPIModel();
        //        objMoneyTransferCommissionModel.FromAmount = FromAmount;
        //        objMoneyTransferCommissionModel.ToAmount = ToAmount;
        //        objMoneyTransferCommissionModel.RetailerFixRs = RetailerFixRs;
        //        objMoneyTransferCommissionModel.RetailerFlexi = RetailerFlexi;
        //        objMoneyTransferCommissionModel.DistributorFixRs = DistributorFixRs;
        //        objMoneyTransferCommissionModel.DistributorFlexi = DistributorFlexi;
        //        objMoneyTransferCommissionModel.SuperDistributorFixRs = SuperDistributorFixRs;
        //        objMoneyTransferCommissionModel.SuperDistributorFlexi = SuperDistributorFlexi;
        //        objMoneyTransferCommissionModel.APICommission = APICommission;
        //        objMoneyTransferCommissionModel.APICommissionType = APICommissionType;
        //        objMoneyTransferCommissionModel.ID = ID;
        //        UPIBal objDefaultMoneyTransferCommBAL = new UPIBal();
        //        int i = objDefaultMoneyTransferCommBAL.UpdateDefaultCommission(objMoneyTransferCommissionModel);
        //        if (i > 0)
        //        {
        //            Message message = new Message();
        //            Response = string.Format(message.successUpdate, "Default Commission");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        new Logger().write(ex);
        //    }
        //    return Response;
        //}

        //private static bool InputValidationUpdateDefaultCommission(decimal FromAmount, decimal ToAmount, decimal RetailerFixRs, decimal RetailerFlexi, decimal DistributorFixRs, decimal DistributorFlexi, decimal SuperDistributorFixRs, decimal SuperDistributorFlexi)
        //{
        //    if (FromAmount <= 0)
        //    {
        //        return false;
        //    }
        //    if (ToAmount <= 0)
        //    {
        //        return false;
        //    }

        //    return true;
        //}
    }
}