﻿function AddCommission() {
    var FromAmount = $("#txtFromAmount").val();
    var ToAmount = $("#txtToAmount").val();
    var RetailerFixRs = $("#txtRetailerFixRs").val();
    var RetailerFlexi = $("#txtRetailerFlexi").val();
    var DistributorFixRs = $("#txtDistributorFixRs").val();
    var DistributorFlexi = $("#txtDistributorFlexi").val();
    var SuperDistributorFixRs = $("#txtSuperDistributorFixRs").val();
    var SuperDistributorFlexi = $("#txtSuperDistributorFlexi").val();
    var APICommission = $("#txtAdminCommission").val();
    var APICommissionType = $("#txtAdminCommissionType").val();
    var Paymode = $("#ddlCardType").val();

    if (FromAmount == "") {
        $.alert.open({
            content: 'Please enter from amount', icon: 'warning', title: ClientName,
            callback: function () {
            }
        });
        return false;
    }

    if (ToAmount == "") {
        $.alert.open({
            content: 'Please enter to amount', icon: 'warning', title: ClientName,
            callback: function () {
            }
        });
        return false;
    }
    if (RetailerFixRs == "") {
        $.alert.open({
            content: 'Please enter retailer FixRs', icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtRetailerFixRs").focus();
            }
        });
        return false;
    }

    if (RetailerFlexi == "") {
        $.alert.open({
            content: 'Please enter retailer Flexi', icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtRetailerFlexi").focus();
            }
        });
        return false;
    }

    if (DistributorFixRs == "") {
        $.alert.open({
            content: 'Please enter distributor FixRs', icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtDistributorFixRs").focus();
            }
        });
        return false;
    }

    if (DistributorFlexi == "") {
        $.alert.open({
            content: 'Please enter distributor Flexi', icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtDistributorFlexi").focus();
            }
        });
        return false;
    }

    if (SuperDistributorFixRs == "") {
        $.alert.open({
            content: 'Please enter superDistributor FixRs', icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtSuperDistributorFixRs").focus();
            }
        });
        return false;
    }

    if (SuperDistributorFlexi == "") {
        $.alert.open({
            content: 'Please enter superDistributor Flexi', icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtSuperDistributorFlexi").focus();
            }
        });
        return false;
    }
    if (Paymode == "0") {
        $.alert.open({
            content: 'Please select paymode', icon: 'warning', title: ClientName,
            callback: function () {
            }
        });
        return false;
    }

    var ID = $("#hdfID").val();

    if (ID > 0) {
        var jsonObj = {
            FromAmount: FromAmount,
            ToAmount: ToAmount,
            RetailerFixRs: RetailerFixRs,
            RetailerFlexi: RetailerFlexi,
            DistributorFixRs: DistributorFixRs,
            DistributorFlexi: DistributorFlexi,
            SuperDistributorFixRs: SuperDistributorFixRs,
            SuperDistributorFlexi: SuperDistributorFlexi,
            APICommission: parseFloat(APICommission),
            APICommissionType: APICommissionType,
            ID: ID,
            Paymode: Paymode
        };
        var jData = JSON.stringify(jsonObj)

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "PGDefaultCommission.aspx/UpdateDefaultCommission",
            data: jData,
            dataType: "json",
            contentType: "application/json",
            async: false,
            success: function (data) {
                debugger;
                $("#hdfID").val(0);
                hideLoader();
                $.alert.open({
                    content: data.d, icon: 'warning', title: ClientName,
                    callback: function () {
                        reset();
                    }
                });
            },
            error: function (xhr, status, error) {
                hideLoader();
                alert(xhr.responseText);
            }
        });
        SelectMoneyTransferCommission();
    }
    else {
        var jsonObj = {
            FromAmount: FromAmount,
            ToAmount: ToAmount,
            RetailerFixRs: RetailerFixRs,
            RetailerFlexi: RetailerFlexi,
            DistributorFixRs: DistributorFixRs,
            DistributorFlexi: DistributorFlexi,
            SuperDistributorFixRs: SuperDistributorFixRs,
            SuperDistributorFlexi: SuperDistributorFlexi,
            APICommission: parseFloat(APICommission),
            APICommissionType: APICommissionType,
            Paymode: Paymode
        };
        var jData = JSON.stringify(jsonObj)

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "PGDefaultCommission.aspx/AddDefaultCommission",
            data: jData,
            dataType: "json",
            contentType: "application/json",
            async: false,
            success: function (data) {
                debugger;
                hideLoader();
                $.alert.open({
                    content: data.d, icon: 'warning', title: ClientName,
                    callback: function () {
                        reset();
                        SelectMoneyTransferCommission();
                    }
                });
            },
            error: function (xhr, status, error) {
                hideLoader();
                alert(xhr.responseText);
            }
        });
    }
    resetCommission();
}

function resetCommission() {
    debugger;

    $("#btnAdd").html('ADD');
    $("#txtFromAmount").val('');
    $("#txtToAmount").val('');
    $("#txtRetailerFixRs").val('');
    $("#txtRetailerFlexi").val('');
    $("#txtDistributorFixRs").val('');
    $("#txtDistributorFlexi").val('');
    $("#txtSuperDistributorFixRs").val('');
    $("#txtSuperDistributorFlexi").val('');
    $("#hdfID").val(0);
    $("#txtFromAmount").prop('disabled', false)
    $("#txtToAmount").prop('disabled', false)
    $("#txtAdminCommission").val('');

}

function SelectMoneyTransferCommission() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "PGDefaultCommission.aspx/SelectMoneyTransferCommission",
        data: '{}',
        dataType: "json",
        contentType: "application/json",
        async: false,
        success: function (data) {
            debugger;
            hideLoader();
            $("#tblCommissionDtl").html("<thead ><tr><th>#</th><th>Paymode</th><th>FromAmount</th><th>ToAmount</th><th>RetailerFixRs</th><th>RetailerFlexi</th><th>DistributorFixRs</th><th>DistributorFlexi</th><th>SuperDistributorFixRs</th><th>SuperDistributorFlexi</th><th>APICom</th><th>APIComType</th><th>Update</th><th>Delete</th></thead><tbody></tbody>");

            for (var i = 0; i < data.d.length; i++) {
                var strRow = [];

                strRow.push(String.format('<tr>'));
                strRow.push(String.format('<td class="text-right"> {0} </td>', i + 1));
                strRow.push(String.format('<td class="text-left" > {0} </td>', data.d[i].Paymode));
                strRow.push(String.format('<td class="text-right" > {0} </td>', data.d[i].FromAmount));
                strRow.push(String.format('<td class="text-right"> {0} </td>', data.d[i].ToAmount));
                strRow.push(String.format('<td class="text-right" > {0} </td>', data.d[i].RetailerFixRs));
                strRow.push(String.format('<td class="text-right" > {0} </td>', data.d[i].RetailerFlexi));

                strRow.push(String.format('<td class="text-right" > {0} </td>', data.d[i].DistributorFixRs));
                strRow.push(String.format('<td class="text-right"> {0} </td>', data.d[i].DistributorFlexi));
                strRow.push(String.format('<td class="text-right" > {0} </td>', data.d[i].SuperDistributorFixRs));
                strRow.push(String.format('<td class="text-right" > {0} </td>', data.d[i].SuperDistributorFlexi));

                strRow.push(String.format('<td class="text-right" > {0} </td>', data.d[i].APICommission));

                //strRow.push(String.format('<td><input type="text" id="txtAdminCommission{0}" style="width:80px;text-align: right" value="{1}"  onkeypress= "return fnAllowNumeric(event);"/> </td>', i, data.d[i].APICommission));
                //strRow.push(String.format('<td><input type="text" id="txtAdminCommissionType{0}" style="width:80px;text-align: right" value="{1}"  onkeypress= "return fnAllowNumeric(event);"/> </td>', i, data.d[i].AdminCommissionType));

                if (data.d[i].APICommissionType == "P") {
                    strRow.push(String.format('<td class="text-right" ><select id="txtAdminCommissionType{0}" ><option value="P" selected>%</option><option value="RS">Rs</option></select> </td>', i));
                }
                else {
                    strRow.push(String.format('<td class="text-right" ><select id="txtAdminCommissionType{0}" ><option value="P">%</option><option value="RS" selected>Rs</option></select> </td>', i));
                }

                strRow.push(String.format("<td class='text-center'> <a value='Edit'  title='Edit' class='fa fa-pencil-square-o'  onclick=\"Edit({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},'{10}','{11}');\"></a></td>", data.d[i].ID, data.d[i].FromAmount, data.d[i].ToAmount, data.d[i].RetailerFixRs, data.d[i].RetailerFlexi, data.d[i].DistributorFixRs, data.d[i].DistributorFlexi, data.d[i].SuperDistributorFixRs, data.d[i].SuperDistributorFlexi, data.d[i].APICommission, data.d[i].APICommissionType, data.d[i].Paymode));
                strRow.push(String.format("<td class='text-center'> <a value='Delete'  title='Delete' class='btn btn-danger' onclick=\"DeleteDefaultSlab({0});\"><span class='fa fa-trash'></span></a></td>", data.d[i].ID));
                strRow.push('</tr>');
                $("#tblCommissionDtl").append(strRow.join(""));

                $("#txtAdminCommissionType" + i).val(data.d[i].APICommissionType);
            }

            $('#tblCommissionDtl').DataTable({
                "bDestroy": true,
                dom: 'Blfrtip',
                buttons: [
                   'excelHtml5',
                   'pdfHtml5'
                ]
            });
        },
        error: function (xhr, status, error) {
            hideLoader();
            alert(xhr.responseText);
        }
    });
}

function DeleteDefaultSlab(ID) {
    var message = String.format(cont);

    $.alert.open({
        type: 'confirm',
        content: message,
        icon: 'warning',
        maxHeight: 300,
        title: ClientName,
        async: false,
        callback: function (button) {
            if (button == 'yes') {
                var jsonObj = {
                    ID: parseInt(ID)
                };

                var jData = JSON.stringify(jsonObj)

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "PGDefaultCommission.aspx/DeleteDefaultSlab",
                    data: jData,
                    dataType: "json",
                    contentType: "application/json",
                    success: function (data) {
                        hideLoader();
                        $.alert.open({
                            content: data.d, icon: 'warning', title: ClientName,
                            callback: function () {
                                reset();
                                SelectMoneyTransferCommission();
                            }
                        });
                    },
                    error: function (xhr, status, error) {
                        hideLoader();
                        alert(xhr.responseText);
                    }
                });
            }
        }
    });
}

function Edit(ID, FromAmount, ToAmount, RetailerFixRs, RetailerFlexi, DistributorFixRs, DistributorFlexi,
    SuperDistributorFixRs, SuperDistributorFlexi, APICommission, APICommissionType, Paymode) {
    $("#btnAdd").html('Update');
    $("#hdfID").val(ID);
    $("#txtFromAmount").val(FromAmount);
    $("#txtToAmount").val(ToAmount);
    $("#txtRetailerFixRs").val(RetailerFixRs);
    $("#txtRetailerFlexi").val(RetailerFlexi);
    $("#txtDistributorFixRs").val(DistributorFixRs);
    $("#txtDistributorFlexi").val(DistributorFlexi);
    $("#txtSuperDistributorFixRs").val(SuperDistributorFixRs);
    $("#txtSuperDistributorFlexi").val(SuperDistributorFlexi);
    $("#ddlCardType").val(Paymode);
    $("#ddlCardType").chosen({ width: '100%' });
    $('#ddlCardType').trigger('chosen:updated');
    $("#txtAdminCommission").val(APICommission);
    $("#txtAdminCommissionType").val(APICommissionType);

    //$("#txtFromAmount").prop('disabled', true);
    //$("#txtToAmount").prop('disabled', true);
}

$('#txtFromAmount').on("input", function () {
    var dInput = this.value;
    if (isNaN(dInput) || dInput <= 0) {
        $("#txtFromAmount").val('');
    }
});

$('#txtToAmount').on("input", function () {
    var dInput = this.value;
    if (isNaN(dInput) || dInput <= 0) {
        $("#txtToAmount").val('');
    }
});

$("#txtRetailerFixRs").keypress(function (e) {
    if (e.which != 46 && e.which != 45 && e.which != 46 && e.which != 8 &&
        !(e.which >= 48 && e.which <= 57)) {
        return false;
    }
});

$("#txtRetailerFlexi").keypress(function (e) {
    if (e.which != 46 && e.which != 45 && e.which != 46 && e.which != 8 &&
        !(e.which >= 48 && e.which <= 57)) {
        return false;
    }
});

$('#txtDistributorFixRs').on("input", function () {
    var dInput = this.value;
    if (dInput == '-') {
        return true;
    }
    if (isNaN(dInput)) {
        $("#txtDistributorFixRs").val('');
    }
});

$('#txtDistributorFlexi').on("input", function () {
    var dInput = this.value;
    if (isNaN(dInput) || dInput < 0) {
        $("#txtDistributorFlexi").val('');
    }
});

$('#txtSuperDistributorFixRs').on("input", function () {
    var dInput = this.value;

    if (dInput == '-') {
        return true;
    }
    if (isNaN(dInput)) {
        $("#txtSuperDistributorFixRs").val('');
    }
});

$('#txtSuperDistributorFlexi').on("input", function () {
    var dInput = this.value;
    if (isNaN(dInput) || dInput < 0) {
        $("#txtSuperDistributorFlexi").val('');
    }
});