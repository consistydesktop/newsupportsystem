﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SampleURL.aspx.cs" Inherits="SupportSystem.SampleURL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Sample URL
                                  <button class="btn btn-info buttons float-right" type="button" onclick=' location.href="\APIProvider.aspx"'>Add New Provider</button>

                                <button class="btn btn-info buttons float-right" type="button" onclick=' location.href="\SampleURLReport.aspx"'>Report</button></h6>
                                                       
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">
                <div class="row" style="margin-top: 30px;">
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12 mx-auto">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">API Provider</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <select  onchange="Select();" class="form-control loginput" id="txtProviderName" title="Select System">
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">Service</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <select id="txtServiceName" class="form-control loginput">
                                        <option value="0">Select Service</option>
                                        <option value="11">Mobile</option>
                                        <option value="13">Dth</option>
                                        <option value="14">Post Paid</option>
                                        <option value="12">Electricity</option>

                                        <option value="2">Sms</option>
                                        <option value="3">Payment Gateway</option>
                                        <option value="4">Aeps</option>
                                        <option value="5">M-Atm</option>
                                        <option value="6">PrepaidActivation</option>
                                        <option value="7">Cable TV</option>
                                        <option value="8">Dth Booking</option>
                                        <option value="9">Phone Booking</option>
                                        <option value="10">Money Transfer</option>

                                        <option value="15">Data Card</option>
                                        <option value="16">Bbps</option>

                                        <option value="19">General Insurance</option>
                                        <option value="20">FRC</option>
                                        <option value="21">Life Insurance Premium</option>
                                        <option value="22">FasTag</option>
                                        <option value="23">Landline</option>
                                        <option value="24">Broadband</option>
                                        <option value="25">Gas Cylinder Booking</option>
                                        <option value="26">Water-Bill</option>
                                        <option value="27">Piped-Gas</option>
                                        <option value="28">Payout</option>
                                        <option value="29">Loan Re-Payment</option>


                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">URL</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">

                                    <textarea class="form-control loginput" id="txtURL" rows="5" aria-describedby="emailHelp" title="URL" placeholder="URL"></textarea>


                                    <label style="display: none; color: red;" id="lblname">URL is required</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">DateFormat</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">

                                    <select id="txtDateFormat" class="form-control loginput">

                                        <option value="0">Select Format</option>
                                        <option value="MM/DD/YY">MM/DD/YY -  Month-Day-Year with leading zeros (02/17/2009)</option>
                                        <option value="DD/MM/YY">DD/MM/YY -  Day-Month-Year with leading zeros (17/02/2009)</option>
                                        <option value="YY/MM/DD">YY/MM/DD -  Year-Month-Day with leading zeros (2009/02/17)</option>
                                        <option value="Month D, Yr">Month D, Yr -  Month name-Day-Year with no leading zeros(February 17, 2009)</option>
                                        <option value="M/D/YY">M/D/YY -  Month-Day-Year with no leading zeros (2/17/2009)</option>
                                        <option value="D/M/YY">D/M/YY -  Day-Month-Year with no leading zeros (17/2/2009)</option>
                                        <option value="YY/M/D">YY/M/D -  Year-Month-Day with no leading zeros (2009/2/17)</option>
                                        <option value="bM/bD/YY">bM/bD/YY -  Month-Day-Year with spaces instead of leading zeros( 2/17/2009)</option>
                                        <option value="bD/bM/YY">bD/bM/YY -  Day-Month-Year with spaces instead of leading zeros(17/ 2/2009)</option>
                                        <option value="YY/bM/bD">YY/bM/bD -  Year-Month-Day with spaces instead of leading zeros (2009/ 2/17)</option>
                                        <option value="MMDDYY">MMDDYY -  Month-Day-Year with no separators (02172009)</option>
                                        <option value="DDMMYY">DDMMYY -  Day-Month-Year with no separators (17022009)</option>
                                        <option value="YYMMDD">YYMMDD -  Year-Month-Day with no separators (20090217)</option>
                                        <option value="MonDDYY">MonDDYY -  Month abbreviation-Day-Year with leading zeros (Feb172009)</option>
                                        <option value="DDMonYY">DDMonYY -  Day-Month abbreviation-Year with leading zeros (17Feb2009)</option>
                                        <option value="YYMonDD">YYMonDD -  Year-Month abbreviation-Day with leading zeros (2009Feb17)</option>
                                        <option value="day/YY">day/YY -  Day of year (counting consecutively from January 1)-Year (48/2009)</option>
                                        <option value="YY/day">YY/day -  Year-Day of Year (counting consecutively from January 1—often called the Julian date format) (2009/48)</option>
                                        <option value="D Month, Yr">D Month, Yr -  Day-Month name-Year (17 February, 2009)</option>
                                        <option value="Yr, Month D">Yr, Month D -  Year-Month name-Day (2009, February 17)</option>
                                        <option value="Mon-DD-YYYY">Mon-DD-YYYY -  Month abbreviation, Day with leading zeros, Year(Feb 17, 2009)</option>
                                        <option value="DD-Mon-YYYY">DD-Mon-YYYY -  Day with leading zeros, Month abbreviation, Year17 Feb, 2009.</option>
                                        <option value="YYYYY-Mon-DD">YYYYY-Mon-DD -  Year, Month abbreviation, Day with leading zeros(2009, Feb 17)</option>
                                        <option value="Mon DD, YYYY">Mon DD, YYYY-  Month abbreviation, Day with leading zeros, Year(Feb 17, 2014)</option>
                                        <option value="DD Mon, YYYY">DD Mon, YYYY -  Day with leading zeros, Month abbreviation, Year(17 Feb, 2014)</option>
                                        <option value="YYYY, Mon DD">YYYY, Mon DD -  Year, Month abbreviation, Day with leading zeros(2014, Feb 17)</option>

                                    </select>


                                    <%--<input type="text" class="form-control loginput" id="txtDateFormat" placeholder="DateFormat" title="DateFormat" />
                                    <label style="display: none; color: red;" id="lblname">URL is required</label>--%>
                                </div>
                            </div>
                        </div>




                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">URLType</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <select id="ddlURLType" class="form-control loginput">

                                        <option value="VIEW">VIEW</option>
                                        <option value="Offer & Complaint">Offer & Complaint</option>
                                        <option value="PAY">PAY</option>
                                        <option value="BALANCE">BALANCE</option>
                                        <option value="STATUS">STATUS</option>
                                    </select>
                                    <label style="display: none; color: red;" id="lblname">URL is required</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">MethodType</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <select id="ddlMethodType" class="form-control allinputtxt" style="height: 100%">
                                        <option value="GET" selected="selected">GET</option>
                                        <option value="POST">POST</option>
                                    </select>

                                    <label style="display: none; color: red;" id="lblname">URL is required</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="display: none" id="divHeaderJson">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">HeaderJSON</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <input type="text" class="form-control loginput" id="txtHeaderJSON" placeholder="HeaderJSON" title="HeaderJSON" />
                                    <label style="display: none; color: red;" id="lblname">URL is required</label>
                                </div>
                            </div>
                        </div>




                        <div class="form-group row mt-3">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                <label id="txtSampleURLID" style="display: none"></label>
                            </div>

                            <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-9">
                                <div class="row">
                                    <div class="col-6">
                                        <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" onclick="return SampleURLInsert();" title="Add URL Details">ADD</button>
                                        <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return SampleURLUpdate();" id="UpdateButton" title="Update URL Details" style="display: none;">Update</button>
                                    </div>
                                    <div class="col-6">
                                        <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick="ResetAll()" title="Clear All">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <label for="parameters" style="margin-top: 27px; font-size: 20px; color: green">
                                    Parameters : &nbsp;[RequestID] [Amount] [Number] [TxnID] [RefID] [Message] [ClBal] [DMTBalance] [Operator] [Type] [Date] [Circle] [LoginID] [Token] [IFSC] [Remark] [StateCode]
                                        [LapuNo] [ApiComm] [CustomerName] [Param1] [Param2] [Param3] [Param4] [Param5] [NoUse1] 
                                          [NoUse2] [NoUse3]
                                </label>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
            <div class="container-fluid mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="myTabContent">
                            <div class="table-responsive">
                                <table id="SampleURLDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                    <thead></thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
  
    <script src="JS/SampleURL.js"></script>
    <script>

        $(document).ready(function () {

            ProviderMasterSelect();
            $('#ddlMethodType').on('change', '', function (e) {
                if ($("#ddlMethodType").val() == 'POST') {
                    $("#divHeaderJson").css('display', 'block');
                }
                else {
                    $("#divHeaderJson").css('display', 'none');
                }
            });

            if (window.location.href.indexOf("SampleURLID") > 0) {
                var url = window.location.href;
                var id = url.split('=');
                SampleURLSelectByID(id[1]);
            }

            //   Select();
        });


    </script>
</asp:Content>

