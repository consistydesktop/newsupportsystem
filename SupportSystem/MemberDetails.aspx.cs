﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class MemberDetails : System.Web.UI.Page
    {
        private static string pageName = "MemberDetails.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {

            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static MemberDetailsModel[] Select()
        {

            AccessController.checkAccess(pageName);
            List<MemberDetailsModel> details = new List<MemberDetailsModel>();

            MemberDetailsBal objBAL = new MemberDetailsBal();
            try
            {
                details = objBAL.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon AcceptRejectCustomer(int RegistrationID, int IsAprroved, string MobileNumber)
        {

            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = -1;

            MemberDetailsBal objBAL = new MemberDetailsBal();
            AccessController objAC = new AccessController();

            string Password = objAC.autoGeneratePass();
            string PasswordMessage = string.Format("Your Password is {0}", Password);

            // MessageProcessing.SendPassword(MobileNumber, Password);
            try
            {
                i = objBAL.AcceptRejectCustomer(RegistrationID, IsAprroved, Password);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i <=0 )
                msgIcon.Message = "customer not approved";
            else
                msgIcon.Message = "customer is approved";



            return msgIcon;
        }



        [WebMethod]
        public static MemberDetailsModel SelectByRegistrationID(int RegistrationID)
        {

            AccessController.checkAccess(pageName);
            MemberDetailsModel details = new MemberDetailsModel();

            MemberDetailsBal objBAL = new MemberDetailsBal();
            try
            {
                details = objBAL.SelectByRegistrationID(RegistrationID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }



        [WebMethod]
        public static MemberDetailsModel[] SelectCustomerName()
        {

            AccessController.checkAccess(pageName);
            List<MemberDetailsModel> details = new List<MemberDetailsModel>();

            MemberDetailsBal objBAL = new MemberDetailsBal();
            try
            {
                details = objBAL.SelectCustomerName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        private string TransferMsg(string Message)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Password.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{0}", Message);

            return body;
        }
    }
}