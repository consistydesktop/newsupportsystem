﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="APIProvider.aspx.cs" Inherits="SupportSystem.APIProvider" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>API Provider

  <button class="btn btn-info buttons float-right" type="button" onclick=' location.href="\SampleURL.aspx"'>Add Sample URL</button>
           <button class="btn btn-info buttons float-right" type="button" onclick=' location.href="\SampleResponse.aspx"'>Add Sample Response</button></h6>
    
                          
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">


                <div class="row" style="margin-top: 30px;">
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12 mx-auto">


                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">Provider Name</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <input type="text" class="form-control loginput" id="txtProviderName" placeholder="ProviderName" title="ProviderName" />
                                    <label style="display: none; color: red;" id="lblname">Developer Name is required</label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">URL</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <textarea class="form-control loginput" id="txtURL" aria-describedby="emailHelp" title="URL" placeholder="URL"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mt-3">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                <label id="txtAPIProviderID" style="display: none"></label>
                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-9">
                                <div class="row">
                                    <div class="col-6">
                                        <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" onclick="return APIProviderInsert();" title="Add URL Details">ADD</button>
                                        <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return APIProviderUpdate();" id="UpdateButton" title="Update URL Details" style="display: none;">Update</button>
                                    </div>
                                    <div class="col-6">
                                        <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick="ResetAll()" title="Clear All">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="container-fluid mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="myTabContent">
                            <div class="table-responsive">
                                <table id="APIProviderDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                    <thead></thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>     <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                <label id="txtProviderID" style="display: none"></label>
                            </div>
        </section>
    </div>

    <script src="JS/APIProvider.js"></script>
    <script>

        $(document).ready(function () {
        
            Select();
        });


    </script>
</asp:Content>


