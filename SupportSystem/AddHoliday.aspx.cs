﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class AddHoliday : System.Web.UI.Page
    {
        private static string pageName = "AddHoliday.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        private static bool IsVallidInsert(string Date)
        {
            if (Date == "")
                return false;

            return true;
        }

        [WebMethod]
        public static MessageWithIcon Update(int DayID, string Date, string Description)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            if (!IsVallidInsert(Date))
            {
                msgIcon.Message = "Date can not be blank";
                return msgIcon;
            }

            HoliDayModel objmodel = new HoliDayModel();
            objmodel.DayID = DayID;
            objmodel.Date = Convert.ToDateTime(Date);
            objmodel.Description = Description;

            AddHoliDayBal ObjBal = new AddHoliDayBal();
            int Result = -1;
            try
            {
                Result = ObjBal.Update(objmodel);
            }
            catch (Exception ex)
            { new Logger().write(ex); }

            if (Result > 0)
                msgIcon.Message = "Holiday updated successfully";
            else
                msgIcon.Message = "Holiday not updated";

            return msgIcon;
        }

        [WebMethod]
        public static MessageWithIcon Insert(string Date, string Description)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            if (!IsVallidInsert(Date))
            {
                msgIcon.Message = "Date can not be blank";
                return msgIcon;
            }

            HoliDayModel objmodel = new HoliDayModel();
            objmodel.Date = Convert.ToDateTime(Date);
            objmodel.Description = Description;

            AddHoliDayBal ObjBal = new AddHoliDayBal();
            int Result = -1;
            try
            {
                Result = ObjBal.Insert(objmodel);
            }
            catch (Exception ex)
            { new Logger().write(ex); }

            if (Result > 0)
                msgIcon.Message = "Holiday submitted successfully";
            else
                msgIcon.Message = "Holiday not submitted";

            return msgIcon;
        }


        [WebMethod]
        public static List<HoliDayModel> Select()
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();

            AddHoliDayBal ObjBal = new AddHoliDayBal();

            DataTable dt = new DataTable();
            try
            {
                dt = ObjBal.Select();
            }
            catch (Exception ex)
            { new Logger().write(ex); }


            if (dt.Rows.Count <= 0)
                new List<HoliDayModel>();

            List<HoliDayModel> report = new List<HoliDayModel>();
            foreach (DataRow dtrow in dt.Rows)
            {
                HoliDayModel obj = new HoliDayModel();
                obj.DayID = Convert.ToInt32(dtrow["DayID"].ToString());
                obj.Date = Convert.ToDateTime(dtrow["Date"].ToString());
                obj.Description = (dtrow["Description"].ToString());

                report.Add(obj);
            }

            return report;

        }

        [WebMethod]
        public static List<HoliDayModel> SelectByID(int DayID)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();

            AddHoliDayBal ObjBal = new AddHoliDayBal();

            DataTable dt = new DataTable();
            try
            {
                dt = ObjBal.SelectByID(DayID);
            }
            catch (Exception ex)
            { new Logger().write(ex); }


            if (dt.Rows.Count <= 0)
                new List<HoliDayModel>();

            List<HoliDayModel> report = new List<HoliDayModel>();
            foreach (DataRow dtrow in dt.Rows)
            {
                HoliDayModel obj = new HoliDayModel();
                obj.DayID = Convert.ToInt32(dtrow["DayID"].ToString());
                obj.Date = Convert.ToDateTime(dtrow["Date"].ToString());
                obj.Description = (dtrow["Description"].ToString());

                report.Add(obj);
            }

            return report;

        }

        [WebMethod]
        public static MessageWithIcon Delete(int DayID)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();


            AddHoliDayBal ObjBal = new AddHoliDayBal();
            int Result = -1;
            try
            {
                Result = ObjBal.Delete(DayID);
            }
            catch (Exception ex)
            { new Logger().write(ex); }

            if (Result > 0)
                msgIcon.Message = "Holiday deleted successfully";
            else
                msgIcon.Message = "Holiday not deleted";

            return msgIcon;
        }


    }
}