﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class Dashboard : System.Web.UI.Page
    {
        private static string pageName = "Dashboard.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }


        [WebMethod]
        public static List<ViewTicketsAdminModel> SelectTicketCount()
        {
            AccessController.checkAccess(pageName);
            int UserID = -1;
            UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());



            DashboardBal objBal = new DashboardBal();
            DataTable dt = new DataTable();
            try
            {
                dt = objBal.SelectTicketCounts(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();

            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTicketsAdminModel report = new ViewTicketsAdminModel();
                report.TicketStatus = (dtrow["TicketStatus"].ToString());
                report.Count = Convert.ToInt32(dtrow["Count"].ToString().Trim());
                details.Add(report);
            }
            return details;
        }

        [WebMethod]
        public static List<ViewTaskAdmin> SelectTaskCount()
        {
            AccessController.checkAccess(pageName);
            int UserID = -1;
            DataTable dt = new DataTable();
            UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());

            DashboardBal objBal = new DashboardBal();

            try
            {
                dt = objBal.SelectTaskCount(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            List<ViewTaskAdmin> details = new List<ViewTaskAdmin>();

            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTaskAdmin report = new ViewTaskAdmin();
                report.Status = (dtrow["Status"].ToString());
                report.Count = Convert.ToInt32(dtrow["TaskCount"].ToString().Trim());
                details.Add(report);
            }
            return details;
        }



        [WebMethod]
        public static List<UnpaidSubscriptionViewModel> SelectUnpaidSubscription()
        {
            AccessController.checkAccess(pageName);

            DataTable dt = new DataTable();

            DashboardBal objBal = new DashboardBal();

            try
            {
                dt = objBal.SelectUnpaidSubscription();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            List<UnpaidSubscriptionViewModel> details = new List<UnpaidSubscriptionViewModel>();

            foreach (DataRow dtrow in dt.Rows)
            {
                UnpaidSubscriptionViewModel report = new UnpaidSubscriptionViewModel();
                report.Name = (dtrow["Name"].ToString());
                report.LastDOC = (dtrow["LastDOC"].ToString());
                report.WebSiteURL = (dtrow["WebSiteURL"].ToString());

                report.ProductName = (dtrow["ProductName"].ToString());
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Month = "";
                report.LastAmount = 0;
                if (report.ProductName.Equals("Finovative", StringComparison.InvariantCultureIgnoreCase))
                {
                    DataTable dt1 = objBal.SelectLastSubscriptionDetails(report.SystemID, report.UserID);
                    if (dt1.Rows.Count < 1)
                    {
                        report.Month = "";
                        report.LastAmount = 0;
                    }
                    else
                    {
                        report.Month = dt1.Rows[0]["DaysMonthType"].ToString();
                        report.LastAmount = Convert.ToDecimal(dt1.Rows[0]["LastAmount"].ToString().Trim());
                    }


                }

                details.Add(report);
            }
            return details;
        }


        [WebMethod]
        public static decimal SelectpaidSubscriptionDetails()
        {
            AccessController.checkAccess(pageName);
            decimal Amount = 0;
            DataTable dt = new DataTable();
            DashboardBal objBal = new DashboardBal();

            try
            {
                dt = objBal.SelectpaidSubscriptionDetails();
                 Amount = Convert.ToDecimal(dt.Rows[0]["Amount"].ToString().Trim());

               

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Amount;
        }
    }
}