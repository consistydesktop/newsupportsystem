﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProviderConfiguration.aspx.cs" Inherits="SupportSystem.ProviderConfiguration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Provider
                                 <button class="btn btn-info buttons float-right" type="button" onclick=' location.href="\APIProvider.aspx"'>Add New Provider</button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">


                <div class="row" style="margin-top: 30px;">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mx-auto">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">API Provider</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">

                                    <select class="form-control loginput" id="txtProviderName"  title="Select System">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mx-auto">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">Service</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">

                                    <select class="form-control loginput" id="txtServiceName" title="Select Service">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-8">
                                <button type="button" class="btn btn-info buttons float-right" onclick="return Select();" title="Search">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <label id="txtSampleResponseID" style="display: none"></label>
            <div class="container-fluid mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="myTabContent">
                            <div class="table-responsive">

                                <a id="ViewBeneForm" title="btnUpdateAll" class="btn btn-info buttons float-right" style="color: black; float: right; cursor: pointer" onclick="UpdateProviderSetUp()">Update All
                                </a>

                                <table id="SampleConfigurationDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                    <thead></thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <label id="lblProviderID" style="display: none;"></label>
    <label id="lblServiceID"></label>
    <script src="JS/ProviderConfiguration.js"></script>
    <script>
        $(document).ready(function () {
            ProviderMasterSelect();
            ServiceSelect();
        });

    </script>
</asp:Content>

