﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FinoPerformance.aspx.cs" Inherits="SupportSystem.FinoPerformance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>FinoPerformance Report</h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                        <label for="inputFromDate">From Date </label>
                        <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                        <label for="inputToDate">To Date</label>
                        <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()'></input>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                        <label for="inputRequestType">Sales</label>
                        <select id="ddlSalesType" class="form-control loginput">
                            <option value="ServiceWise">ServiceWithDate</option>
                            <option value="All">Date wise</option>
                            <option value="ClientWise">ClientWise</option>

                        </select>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12" id="divCustomer">
                        <label for="inputClientName">Client</label>
                        <select class="form-control loginput" id="inputClientName" title="Client Name"></select>
                    </div>


                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-12">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return FinoPerformanceSelect();" id="SearchButton" title="Search">Search</button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </section>

        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="projects" role="tabpanel" aria-labelledby="projects-tab">
                            <div class="table-responsive">
                                <table id="FinoPerformanceDetails" class="table table-bordered nowrap table-hover">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="JS/Validation.js"></script>
    <script src="JS/FinoPerformance.js"></script>
    <script>

        $(document).ready(function () {

            $("#inputFromDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            }); $("#inputFromDate").datepicker().datepicker("setDate", new Date());
            $("#inputToDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });
            $("#inputToDate").datepicker().datepicker("setDate", new Date());

            ClientSelect();
            FinoPerformanceSelect();


        });
    </script>

</asp:Content>

