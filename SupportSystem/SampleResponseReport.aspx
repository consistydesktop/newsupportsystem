﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SampleResponseReport.aspx.cs" Inherits="SupportSystem.SampleResponseReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Sample Response 
 <button class="btn btn-info buttons float-right" type="button" onclick=' location.href="\SampleResponse.aspx"'>Add Sample Response</button>
              

                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">


                <div class="row" style="margin-top: 30px;">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 mx-auto">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">API Provider</label>
                                </div>
                                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">

                                    <select class="form-control loginput"   onchange="Select();" id="txtProviderName" title="Select System">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mt-3">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                <label id="txtSampleURLID" style="display: none"></label>
                            </div>

                   
                        </div>
                    </div>
                </div>
            </form>
            <label id="txtSampleResponseID" style="display: none"></label>
            <div class="container-fluid mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="myTabContent">
                            <div class="table-responsive">
                                <table id="SampleResponseDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                    <thead></thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="JS/SampleResponse.js"></script>
    <script>

        $(document).ready(function () {
            ProviderMasterSelect();
           Select();


        });


    </script>
</asp:Content>

