﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddSubscriptionScreen.aspx.cs" Inherits="SupportSystem.AddSubscriptionScreen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Add Subscription
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputSystemName">System Name</label>
                        <label id="inputEditSystemName" style="display: none"></label>
                        <select class="form-control loginput" id="inputSystemName" title="Select System" style="display: block">
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputDesktopType">Desktop Type</label>
                        <select class="form-control loginput" id="inputDesktopType" title="Select Desktop Type " >
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputProductType">Demo/Year</label>
                        <br />
                        <input type="radio" name="inputDemoYear" value="Demo" id="inputDemo" />
                        Demo
                            <input type="radio" name="inputDemoYear" value="Year" id="inputYear" />
                        Year

                        
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputPaymentStatus">Payment Status</label>
                        <br />
                        <input type="radio" name="inputPaymentStatus" value="Pending" id="inputPending" />
                        Pending
                            <input type="radio" name="inputPaymentStatus" value="Received" id="inputReceived" />
                        Recieved
                    </div>
                </div>

                <div class="form-group row mt-3">
                   
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputDescription">Description (Optional) </label>
                        <textarea class="form-control loginput" id="inputDescription" title="Description" aria-describedby="emailHelp"></textarea>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" onclick="return SubscriptionMasterInsert();" title="Add Subscription">ADD</button>
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return SubscriptionMasterUpdate();" id="UpdateButton" title="Update Subscription" style="display: none;">Update</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick="Reset()">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="loader" id="loader" style="display: none"></div>
            <div class="container-fluid mt-5">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="myTabContent">
                            <div class="table-responsive">
                                <table id="SubscriptionDetails" class="table table-bordered nowrap table-hover dataTable" style="width: 100%">
                                    <thead></thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="modal_Sender" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="h1">Description</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span id="Description" style="height: 100%; width: 100%;"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <label id="SubscriptionID" style="display: none"></label>

    <script src="/JS/AddSubscriptionScreen.js"></script>
    <script>
        $(document).ready(function () {
            SystemMasterSelect();
            DesktopMasterSelect();
            SubscriptionMasterSelect();

        });
    </script>
</asp:Content>
