﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class LeaveRequest : System.Web.UI.Page
    {
        private static string pageName = "LeaveRequest.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }


        static bool IsValidInsert(DateTime FromDate, DateTime ToDate, string Description)
        {

            if (FromDate == null)
            {
                return false;
            }
            if (ToDate == null)
            {
                return false;
            }
            if (Description == null || Description == "")
            {
                return false;
            }
            if (FromDate <= ToDate)
            {
                return false;
            }

            return true;
        }

        [WebMethod]
        public static MessageWithIcon Insert(DateTime FromDate, DateTime ToDate, string Description)
        {
            AccessController.checkAccess(pageName);

            bool IsVallid = IsValidInsert(FromDate, ToDate, Description);
            MessageWithIcon msgIcon = new MessageWithIcon();

            if (IsVallid == false)
            {
                msgIcon.Message = "Input parameter is not in correct format.";
            }

            msgIcon.Message = "Leave request not submitted";
            
            int result = -1;
            int UserID = -1;
            try
            {
                UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());

                LeaveRequestModel ObjModel = new LeaveRequestModel();
                ObjModel.UserID = UserID;
                ObjModel.FromDate = FromDate;
                ObjModel.ToDate = ToDate;
                ObjModel.Description = Description;

                LeaveRequestBal ObjBAL = new LeaveRequestBal();
                result = ObjBAL.Insert(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result > 0)
            {
                msgIcon.Message = "Leave request submitted successfully";
            }

            return msgIcon;
        }

    }
}