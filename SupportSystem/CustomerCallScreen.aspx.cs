﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Web.Services;

namespace SupportSystem
{
    public partial class CustomerCallScreen : System.Web.UI.Page
    {
        private static string pageName = "CustomerCallScreen.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static CustomerCallScreenModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);

            List<CustomerCallScreenModel> details = new List<CustomerCallScreenModel>();

            CustomerCallScreenBal objBAL = new CustomerCallScreenBal();

            try
            {
                details = objBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static CustomerCallScreenModel[] SelectEmployeeName()
        {
            AccessController.checkAccess(pageName);
            List<CustomerCallScreenModel> details = new List<CustomerCallScreenModel>();

            CustomerCallScreenBal objBAL = new CustomerCallScreenBal();
            try
            {
                details = objBAL.SelectEmployeeName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static CustomerCallScreenModel[] SelectIssue()
        {
            AccessController.checkAccess(pageName);
            List<CustomerCallScreenModel> details = new List<CustomerCallScreenModel>();
            CustomerCallScreenBal objBAL = new CustomerCallScreenBal();
            try
            {
                details = objBAL.SelectIssue();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Insert(int SystemID, int UserID, int CallIssueID, string MobileNumber, string Description, string TeamAny)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            CustomerCallScreenModel objModel = new CustomerCallScreenModel();
            int i = -1;

            objModel.SystemID = SystemID;
            objModel.UserID = UserID;
            objModel.CallIssueID = CallIssueID;
            objModel.MobileNumber = MobileNumber;
            objModel.Description = Description;
            objModel.TeamAny = TeamAny;

            CustomerCallScreenBal objBAL = new CustomerCallScreenBal();

            try
            {
                i = objBAL.Insert(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "Call issue not assigned ";
            else
                msgIcon.Message = "Call issue assigned";

            return msgIcon;
        }

        [WebMethod]
        public static CustomerCallScreenModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<CustomerCallScreenModel> details = new List<CustomerCallScreenModel>();

            CustomerCallScreenBal objBAL = new CustomerCallScreenBal();
            try
            {
                details = objBAL.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static CustomerCallScreenModel SelectByID(int CallID)
        {
            AccessController.checkAccess(pageName);
            CustomerCallScreenModel Register = new CustomerCallScreenModel();
            CustomerCallScreenBal objBal = new CustomerCallScreenBal();
            try
            {
                Register = objBal.SelectByID(CallID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Register;
        }

        [WebMethod]
        public static MessageWithIcon Update(int SystemID, int UserID, int CallIssueID, string MobileNumber, string Description, int CallID, string TeamAny)
        {
            AccessController.checkAccess(pageName);
            CustomerCallScreenModel objModel = new CustomerCallScreenModel();
            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            objModel.SystemID = SystemID;
            objModel.UserID = UserID;
            objModel.CallIssueID = CallIssueID;
            objModel.MobileNumber = MobileNumber;
            objModel.Description = Description;
            objModel.CallID = CallID;
            objModel.TeamAny = TeamAny;

            CustomerCallScreenBal objBAL = new CustomerCallScreenBal();
            try
            {
                i = objBAL.Update(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "Call data not updated";
            else
                msgIcon.Message = "Call data updated successfully";

            return msgIcon;
        }

        [WebMethod]
        public static MessageWithIcon Delete(int CallID)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            CustomerCallScreenBal objBal = new CustomerCallScreenBal();
            try
            {
                i = objBal.Delete(CallID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i > 0)
                msgIcon.Message = "Call data not deleted";
            else
                msgIcon.Message = "Call data deleted Successfully";

            return msgIcon;
        }
    }
}