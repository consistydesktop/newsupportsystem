﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Holidays.aspx.cs" Inherits="SupportSystem.Holidays" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">

        <section class="formhead">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <h6>Holidays</h6>

                    </div>
                </div>
            </div>
        </section>

        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-12">

                    <div class="table-responsive">
                        <table id="HolidayDetails" class="table table-bordered nowrap table-hover">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="/JS/Holidays.js"></script>

    <script>
        HolidayMasterSelect();
        </script>
</asp:Content>
