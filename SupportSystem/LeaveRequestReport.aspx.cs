﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using SupportSystem.StaticClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class LeaveRequestReport : System.Web.UI.Page
    {
        static string pageName = "LeaveRequestReport.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {

            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static LeaveRequestModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<LeaveRequestModel> details = new List<LeaveRequestModel>();
            LeaveRequestReportbal ObjBAL = new LeaveRequestReportbal();
            try
            {


                int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
                string FromDate = DateTime.Now.AddDays(-30).ToString("dd MMM, yyyy");
                string ToDate = DateTime.Now.ToString("dd MMM, yyyy");
                details = ObjBAL.SelectByDate(FromDate, ToDate, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }
        [WebMethod]
        public static LeaveRequestModel[] SelectByDate(string FromDate, string ToDate)
        {
            AccessController.checkAccess(pageName);
            List<LeaveRequestModel> details = new List<LeaveRequestModel>();
            LeaveRequestReportbal ObjBAL = new LeaveRequestReportbal();
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            try
            {
                details = ObjBAL.SelectByDate(FromDate, ToDate, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();

        }
        [WebMethod]
        public static MessageWithIcon LeaveRequestApprove(int LeaveID, int IsApprove)
        {
            AccessController.checkAccess("LeaveRequestApprove.aspx");

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = -1;


            AccessController objAC = new AccessController();

            int ApprovedBy = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());

            try
            {
                LeaveRequestReportbal objBAL = new LeaveRequestReportbal();
                i = objBAL.LeaveRequestApprove(LeaveID, IsApprove, ApprovedBy);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                msgIcon.Message = "Somthing went wrong.";
                return msgIcon;
            }

            LeaveRequestModel ReturnModel = new LeaveRequestModel();
            try
            {
                LeaveRequestReportbal objBAL = new LeaveRequestReportbal();
                ReturnModel = objBAL.SelectByID(LeaveID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);

            }

            if (i <= 0)
            {
                string EmailRejectMessage = AllMessages.LeaveRequestReject;
                EmailRejectMessage = EmailRejectMessage.Replace("[Username]", ReturnModel.Name);
                EmailRejectMessage = EmailRejectMessage.Replace("[FromDate]", ReturnModel.FromDate.ToString());
                EmailRejectMessage = EmailRejectMessage.Replace("[ToDate]", ReturnModel.ToDate.ToString());

                msgIcon.Message = "Leave not approved";


                MessageProcessing.SendEmail(ReturnModel.EMailID, EmailRejectMessage, msgIcon.Message);
                return msgIcon;
            }
            msgIcon.Message = "Leave is Approved";
            string EmailAcceptMessage = AllMessages.LeaveRequestAccept;

            EmailAcceptMessage = EmailAcceptMessage.Replace("[Username]", ReturnModel.Name);
            EmailAcceptMessage = EmailAcceptMessage.Replace("[FromDate]", ReturnModel.FromDate.ToString());
            EmailAcceptMessage = EmailAcceptMessage.Replace("[ToDate]", ReturnModel.ToDate.ToString());

            MessageProcessing.SendEmail(ReturnModel.EMailID, EmailAcceptMessage, msgIcon.Message);
            return msgIcon;
        }


    }
}
