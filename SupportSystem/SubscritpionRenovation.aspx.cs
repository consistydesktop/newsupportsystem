﻿
using Bal;
using Model;
using Newtonsoft.Json;
using SupportProjectEmployee.Controller;
using SupportSystem.Controller;
using SupportSystem.PG;
using SupportSystem.StaticClass;

using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class SubscritpionRenovation : System.Web.UI.Page
    {

        private static string pageName = "SubscritpionRenovation.aspx";
        string RazorpayCallbackURL = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDropdowList();
                SelectSubscriptionDays();
            }

            AccessController.checkAccess(pageName);

            RazorpayCallbackURL = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath + "PG/RazorpaySuccessCallback.aspx";

        }

        private void SelectSubscriptionDays()
        {
            try
            {

                lbl_SubscriptionEndDate.Text = "";
                lbl_RemainingDays.Text = "0";
                lbl_SubscriptionEndDate.Text = HttpContext.Current.Session["SubscriptionEndDate"].ToString();
                lbl_RemainingDays.Text = HttpContext.Current.Session["RemainingDays"].ToString();


            }
            catch (Exception ex)
            {
                lbl_SubscriptionEndDate.Text = "";
                lbl_RemainingDays.Text = "0";
                new Logger().write(ex);
            }

        }

        private void BindDropdowList()
        {
            // List<CardTypeViewModel> details = new List<CardTypeViewModel>();
            SubscritpionRenovationBal objBAL = new SubscritpionRenovationBal();
            DataTable dt = new DataTable();

            try
            {
                dt = objBAL.CardTypeSelect();
                ddlPayMode.DataSource = dt;
                ddlPayMode.DataBind();
                ddlPayMode.DataTextField = "CardType";
                ddlPayMode.DataValueField = "CardTypeID";
                ddlPayMode.DataBind();

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

        }
        protected void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string daysType = DaysType.SelectedItem.Value;
            //   string Value = DaysValue.SelectedItem.Value;
            DataTable dt = new DataTable();
            int ProductID = Products.Finovative;

            try
            {
                ProductID = Convert.ToInt32(HttpContext.Current.Session["ProductID"].ToString());
            }
            catch (Exception ex)
            { ProductID = Products.Finovative; }

            try
            {
                SubscritpionRenovationBal objBAL = new SubscritpionRenovationBal();
                dt = objBAL.SelectCost(daysType, ProductID);
            }
            catch (Exception ex)
            { new Logger().write(ex); }


            if (dt.Rows.Count <= 0)
            {
                lbl_Cost.Text = "";
                lblAmountInWords.Text = "";
                return;
            }
            decimal amount = Convert.ToDecimal(dt.Rows[0]["Amount"].ToString());

            string AmountStr = amount.ToString(("N0"));

            lbl_Cost.Text = AmountStr;

            NumberToWords words = new NumberToWords();
            lblAmountInWords.Text = words.ConvertAmountToString(Convert.ToDouble(amount));

        }

        [WebMethod]
        public static CardTypeViewModel[] CardTypeSelect()
        {
            AccessController.checkAccess(pageName);
            List<CardTypeViewModel> details = new List<CardTypeViewModel>();
            SubscritpionRenovationBal objBAL = new SubscritpionRenovationBal();
            try
            {
                //  details = objBAL.CardTypeSelect();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }



        [WebMethod]
        public static SubscritpionCostViewModel SelectCost(string DaysType)
        {
            AccessController.checkAccess(pageName); int ProductID = Products.Finovative;
            SubscritpionCostViewModel cost = new SubscritpionCostViewModel();

            SubscritpionRenovationBal ObjBal = new SubscritpionRenovationBal();
            DataTable dt = new DataTable();
            try
            {
                ProductID = Convert.ToInt32(HttpContext.Current.Session["ProductID"].ToString());
            }
            catch (Exception ex)
            { ProductID = Products.Finovative; }


            try
            {
                dt = ObjBal.SelectCost(DaysType, ProductID);
            }
            catch (Exception ex)
            { new Logger().write(ex); }


            if (dt.Rows.Count <= 0)
                new List<SubscritpionCostViewModel>();

            cost.Amount = Convert.ToDecimal(dt.Rows[0]["Amount"].ToString());
            // cost.Days = Convert.ToInt32(dt.Rows[0]["Days"].ToString());
            cost.DaysMonthType = dt.Rows[0]["DaysMonthType"].ToString();

            return cost;
        }


        [WebMethod]
        public static SubscritpionCostViewModel SelectSlab(string PayMode)
        {
            AccessController.checkAccess(pageName);
            SubscritpionCostViewModel cost = new SubscritpionCostViewModel();

            SubscritpionRenovationBal ObjBal = new SubscritpionRenovationBal();
            DataTable dt = new DataTable();
            try
            {
                dt = ObjBal.SelectSlab(PayMode);
            }
            catch (Exception ex)
            { new Logger().write(ex); }


            if (dt.Rows.Count <= 0)
                new List<SubscritpionCostViewModel>();

            cost.Amount = Convert.ToDecimal(dt.Rows[0]["Amount"].ToString());
            cost.Month = dt.Rows[0]["Month"].ToString();

            return cost;
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            try
            {
                int ProductID = Products.Finovative;
                lbl_Message.InnerText = "";
                string MobileNumber = HttpContext.Current.Session["MobileNumber"].ToString();
                string UserName = HttpContext.Current.Session["Username"].ToString();
                string EmailID = HttpContext.Current.Session["EmailID"].ToString();
                string Address = ""; //HttpContext.Current.Session["Address"].ToString();
                string WebURL = HttpContext.Current.Session["WebURL"].ToString();
                ProductID = Convert.ToInt32(HttpContext.Current.Session["ProductID"].ToString());


                int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                string ipAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                SubscritpionRenovationBal objPGBal = new SubscritpionRenovationBal();
                int SystemID = objPGBal.SelectSystemID(WebURL);



                SubscritpionCostViewModel cost = new SubscritpionCostViewModel();
                cost = SelectCost(DaysType.SelectedItem.Value);

                // SelectSlab(ddlPayMode.Value);
                // calculate charge with amount here

                //  int Amount = 0;


                Random randomObj = new Random();
                string transactionId = randomObj.Next(10000000, 100000000).ToString();
                int amount = Convert.ToInt32(cost.Amount);

                LoadMoneyOrderRequest request = new LoadMoneyOrderRequest();
                request.amount = amount * 100;
                request.currency = "INR";
                request.receipt = transactionId;

                string body = JsonConvert.SerializeObject(request);

                LoadMoneyOrderResponse res = new LoadMoneyOrderResponse();
                HttpProcessing processing = new HttpProcessing();

                string Reqponse = processing.PostMethodForPayOut(body);

                res = JsonConvert.DeserializeObject<LoadMoneyOrderResponse>(Reqponse);

                string orderId = res.id;

                if (!objPGBal.CheckDuplicateOrderID(orderId))
                {

                    Reqponse = processing.PostMethodForPayOut(body);
                    res = JsonConvert.DeserializeObject<LoadMoneyOrderResponse>(Reqponse);
                    orderId = res.id;

                }

                // Create order model for return on view
                OrderModel orderModel = new OrderModel
                {
                    orderId = orderId,
                    razorpayKey = RazorpayInfo.RazorpayKey,
                    amount = amount * 100,
                    currency = "INR",
                    name = UserName,
                    email = EmailID,
                    contactNumber = MobileNumber,
                    address = Address,
                    description = "Payment"
                };

                PGModel objModel = new PGModel();
                objModel.UserID = UserID;
                objModel.OrderId = orderId;
                objModel.Amount = Convert.ToDecimal(amount);
                objModel.CommisionAmount = 0;
                objModel.DaysMonthType = DaysType.SelectedItem.Value;
                objModel.PaymentGatewayID = 1;
                objModel.status = new Message().pending;
                objModel.response = "";
                objModel.IPAddress = ipAddress;
                objModel.SystemID = SystemID;
                objModel.ProductID = ProductID;


                objPGBal.Insert(objModel);

                string strForm = PreparePOSTForm(orderModel);

                Response.Write(strForm);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

        }
        private string PreparePOSTForm(OrderModel orderModel)      // post form
        {
            string outputHTML = "<form action='" + RazorpayCallbackURL + "' method='POST'>";
            outputHTML += "<script>";
            outputHTML += "window.onload = function(){";
            outputHTML += "document.getElementsByClassName('razorpay-payment-button')[0].click()";
            outputHTML += "}</script>";
            outputHTML += "<script ";
            outputHTML += "src='https://checkout.razorpay.com/v1/checkout.js'";
            outputHTML += "data-key='" + RazorpayInfo.RazorpayKey + "'";
            outputHTML += "data-amount='" + orderModel.amount + "'";
            outputHTML += "data-currency='" + orderModel.currency + "'";
            outputHTML += "data-order_id='" + orderModel.orderId + "'";
            outputHTML += "data-name='Consisty System'";
            outputHTML += "data-description='" + orderModel.description + "'";
            outputHTML += "data-image='http://support.consisty.com/PublicSite/images/logo.svg'";
            outputHTML += "data-prefill.name='" + orderModel.name + "'";
            outputHTML += "data-prefill.email='" + orderModel.email + "'";
            outputHTML += "data-prefill.contact='" + orderModel.contactNumber + "'";
            outputHTML += "data-theme.color='#ff4a00";
            outputHTML += "></script>";
            outputHTML += "<input type='hidden' custom='Hidden Element' name='hidden'>";
            outputHTML += "</form>";
            return outputHTML;
        }



    }
}