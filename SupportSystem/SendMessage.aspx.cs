﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class SendMessage : System.Web.UI.Page
    {
        private static string pageName = "SendMessage.aspx";
        [WebMethod]
        public static MessageWithIcon InsertMessage(string UserID, string Message,string Usertype)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            MessageMasterModel ObjModel = new MessageMasterModel();
            SendMessageBal objBal = new SendMessageBal();
            int result = -1;
            string[] Reg = UserID.Split(',');
            try
            {
                foreach (string se in Reg)
                {
                    if (se != "")
                    {

                        ObjModel.UserID = Convert.ToInt32(se);
                        ObjModel.Message = Message;
                        ObjModel.Usertype = Usertype;
                        DataTable dt = objBal.getMobileNumberfromUserID(ObjModel.UserID);
                        string MobileNumber = dt.Rows[0]["MobileNumber"].ToString();
                        MessageProcessing.SendSMS(MobileNumber, Message);
                        result = objBal.insertMessage(ObjModel);
                    }
                }

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result > 0)
            {
                msgIcon.Message = "Message Send successfully.";
            }
            else
            {
                msgIcon.Message = "Message Not  Send successfully.";
            }

            return msgIcon;
        }
    }
}