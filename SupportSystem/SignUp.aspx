﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="SupportSystem.SignUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />



    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="/Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="/CSS/style.css" rel="stylesheet" />
    <link rel="/stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />

    <link href="/Styles/alert/css/alert.css" rel="stylesheet" type="text/css" />

    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <link href="Styles/alert/themes/default/theme.css" rel="stylesheet" type="text/css" />
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script>
    <script src="/Styles/alert/js/alert.js" type="text/javascript"></script>
    <script src="/Scripts/jquery-ui.min.js"></script>
    <script src="/Styles/alert/js/alert.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css" href="/PublicSite/css/style.css" />


    <title>Welcome to Consisty Support System</title>

</head>
<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12 mx-auto">
                <div class="loginform">
                    <p class="text-center">
                        <img src="/PublicSite/images/logo.svg" width="250px" />
                    </p>
                    <form>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtName">Full Name</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <input type="text" class="form-control loginput" id="txtName" onmousedown="$('#lblname').hide()" aria-describedby="emailHelp" placeholder="Full Name" title="Full Name" onkeypress="return isAlphabateKey(event)" />
                                    <label style="display: none; color: red;" id="lblname">Name is required</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row" style="display: none">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtUserName">UserName</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <input type="text" class="form-control loginput" id="txtUserName" onmousedown="$('#lblUsername').hide()" placeholder="Username" title="User Name" />
                                    <label style="display: none; color: red;" id="lblUsername">Username is required</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtEmail">Email ID</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <input type="text" class="form-control loginput" id="txtEmail" onmousedown="$('#lblEmailID').hide()" placeholder="Email ID" title="Email ID" />
                                    <label style="display: none; color: red;" id="lblEmailIDFormat"></label>
                                    <br />
                                    <label style="display: none; color: red;" id="lblEmailID">Email is required</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtMobileNumber">Mobile Number</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <input type="text" class="form-control loginput" id="txtMobileNumber" onmousedown="$('#lblMobileNumber').hide()" onkeypress="return isNumberKey(event)" maxlength="10" placeholder="Mobile Number" title="Mobile Number" />
                                    <label style="display: none; color: red;" id="lblMobileNumber">Mobile number is required</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtPANNo">PAN No</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <input type="text" class="form-control loginput" maxlength="10" onmousedown="$('#lblPANNo').hide()" id="txtPANNo" placeholder="PAN No" style="text-transform: uppercase" title="PAN No" onkeypress="return isPANKey(event,'txtPANNo')" />
                                    <label style="display: none; color: red;" id="lblPANNoFormat"></label>
                                    <br />
                                    <label style="display: none; color: red;" id="lblPANNo">PAN no is required</label>

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtGSTNo">GST No</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <input type="text" class="form-control loginput" id="txtGSTNo" maxlength="15" placeholder="GST NO" style="text-transform: uppercase" title="GST No" onkeypress="return isGSTKey(event,'txtGSTNo')" />
                                    <label style="display: none; color: red;" id="lblGSTNoFormat"></label>
                                    <br />
                                    <label style="display: none; color: red;" id="lblGSTNo">GST no is required</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtAddress">Address</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <textarea class="form-control loginput" id="txtAddress" onmousedown="$('#lblAddress').hide()" placeholder="Address" title="Address"></textarea>
                                    <label style="display: none; color: red;" id="lblAddress">Address is required</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="txtState">Select State</label>
                                </div>
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <select id="txtState" class="form-control loginput" onchange="$('#lblState').hide()">
                                        <option value="0">Select State</option>
                                        <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                        <option value="Andhra Pradesh">Andhra Pradesh</option>
                                        <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                        <option value="Assam">Assam</option>
                                        <option value="Bihar">Bihar</option>
                                        <option value="Chandigarh">Chandigarh</option>
                                        <option value="Chhattisgarh">Chhattisgarh</option>
                                        <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                        <option value="Daman and Diu">Daman and Diu</option>
                                        <option value="Delhi">Delhi</option>
                                        <option value="Goa">Goa</option>
                                        <option value="Gujarat">Gujarat</option>
                                        <option value="Haryana">Haryana</option>
                                        <option value="Himachal Pradesh">Himachal Pradesh</option>
                                        <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                        <option value="Jharkhand">Jharkhand</option>
                                        <option value="Karnataka">Karnataka</option>
                                        <option value="Kerala">Kerala</option>
                                        <option value="Lakshadweep">Lakshadweep</option>
                                        <option value="Madhya Pradesh">Madhya Pradesh</option>
                                        <option value="Maharashtra">Maharashtra</option>
                                        <option value="Manipur">Manipur</option>
                                        <option value="Meghalaya">Meghalaya</option>
                                        <option value="Mizoram">Mizoram</option>
                                        <option value="Nagaland">Nagaland</option>
                                        <option value="Orissa">Orissa</option>
                                        <option value="Pondicherry">Pondicherry</option>
                                        <option value="Punjab">Punjab</option>
                                        <option value="Rajasthan">Rajasthan</option>
                                        <option value="Sikkim">Sikkim</option>
                                        <option value="Tamil Nadu">Tamil Nadu</option>
                                        <option value="Tripura">Tripura</option>
                                        <option value="Uttaranchal">Uttaranchal</option>
                                        <option value="Uttar Pradesh">Uttar Pradesh</option>
                                        <option value="West Bengal">West Bengal</option>
                                    </select>
                                    <label style="display: none; color: red;" id="lblState">State  is required</label>
                                </div>
                            </div>
                        </div>


                        <button type="submit" class="btn btn-info buttons w-100 logbutton" onclick="return CustomerRegistrationInsert();" title="Sign Up">Sign Up</button>
                        <p class="text-center mt-3">Already have an account? <a href="Login.aspx" title="Sign Up">Sign in</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>


</body>

<script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
<%--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>--%>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<%--<script src="/Scripts/jquery-ui.js" type="text/javascript"></script>--%>
<script src="/Styles/alert/js/alert.js" type="text/javascript"></script>
<script src="/Scripts/jquery-ui.min.js"></script>
<script src="/Styles/alert/js/alert.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
<script src="/JS/SignUp.js"></script>

<script>
    var ClientName = "Conisty System";
    $("#txtName").focus();
    $(document).ready(function () {
        if (window.location.href.indexOf("CustomerRegID") > -1) {
            var url = window.location.href;
            var id = url.split('=')
            UserInformationSelectByID(id[1]);
            $("#UserID").html(id[1]);
            $("#submit").css('display', 'none');
            $("#lblAlready").css('display', 'none');
            $("#SignIn").css('display', 'none');
            $("#Update").show();
        }
    });

    function Hide() {
        $("#lblname").css('display', 'none');
        $("#lblUsername").css('display', 'none');
        $("#lblEmailID").css('display', 'none');
        $("#lblMobileNumber").css('display', 'none');
        $("#lblAddress").css('display', 'none');
        $("#lblState").css('display', 'none');


    }


    function isNumberKey(e) {
        var r = e.which ? e.which : event.keyCode;
        return r > 31 && (48 > r || r > 57) && (e.which != 46 || $(this).val().indexOf('.') != -1) ? !1 : void 0
    }


    jQuery('#txtMobileNumber').keyup(function () {
        var str = jQuery('#txtMobileNumber').val();

    });

    function nospaces(t) {

        if (t.value.match(/\s/g)) {
            $.alert.open({
                content: String.format("Sorry, you are not allowed to enter any spaces"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#txtUserName").focus();
                }
            });
            t.value = t.value.replace(/\s/g, '');
        }
    }

    $(function () {
        $('#txtName').keydown(function (e) {
            {
                Hide();


                var key = e.keyCode;
                if (!((key == 8) || (key == 9) || (key == 11) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {

                    e.preventDefault();
                }
            }
        });
    })

    jQuery('#txtName').keyup(function () {
        var str = jQuery('#txtName').val();

        var spart = str.split(" ");
        for (var i = 0; i < spart.length; i++) {
            var j = spart[i].charAt(0).toUpperCase();
            spart[i] = j + spart[i].substr(1);
        }
        jQuery('#txtName').val(spart.join(" "));

    });

    //$('#txtMobileNumber').bind("cut copy paste", function (e) {

    //    e.preventDefault();

    //});

    function isAlphabateKey(event) {
        var inputValue = event.which;
        if (!(inputValue >= 65 && inputValue <= 90) && !(inputValue >= 97 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    }
</script>
</html>
