﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class ViewTicketClient : System.Web.UI.Page
    {

        private static string pageName = "ViewTicketClient.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }


        [WebMethod]
        public static AddTicketClientModel[] Select()
        {
            int UserID = -1;
            DataTable dt = new DataTable();
            List<AddTicketClientModel> details = new List<AddTicketClientModel>();

            ViewTicketClientBal objBal = new ViewTicketClientBal();
            try
            {

                UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());

                dt = objBal.Select(UserID);


                foreach (DataRow dtrow in dt.Rows)
                {
                    AddTicketClientModel report = new AddTicketClientModel();
                    report.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());
                    report.TicketNo = dtrow["TicketNo"].ToString();
                    report.Title = dtrow["Title"].ToString();
                    report.DOC = Convert.ToDateTime(dtrow["TicketGeneratedDate"].ToString().Trim());
                    report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                    report.Description = dtrow["Description"].ToString().Trim();

                    report.TicketStatus = dtrow["TicketStatus"].ToString().Trim();
                    report.EmployeeRemark = dtrow["EmployeeRemark"].ToString().Trim();
                    report.AdminRemark = dtrow["AdminRemark"].ToString().Trim();
                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();

        }


        [WebMethod]
        public static AddTicketClientModel[] SelectByDate(string FromDate, string ToDate)
        {
            int UserID = -1;
            DataTable dt = new DataTable();
            List<AddTicketClientModel> details = new List<AddTicketClientModel>();

            ViewTicketClientBal objBal = new ViewTicketClientBal();
            try
            {

                UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());

                dt = objBal.SelectByDate(UserID, FromDate, ToDate);


                foreach (DataRow dtrow in dt.Rows)
                {
                    AddTicketClientModel report = new AddTicketClientModel();
                    report.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());
                    report.TicketNo = dtrow["TicketNo"].ToString();

                    report.DOC = Convert.ToDateTime(dtrow["TicketGeneratedDate"].ToString().Trim());
                    report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                    report.Description = dtrow["Description"].ToString().Trim();

                    report.TicketStatus = dtrow["TicketStatus"].ToString().Trim();
                    report.EmployeeRemark = dtrow["EmployeeRemark"].ToString().Trim();
                    report.AdminRemark = dtrow["AdminRemark"].ToString().Trim();
                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();

        }



        [WebMethod]
        public static string UpdateStatusByClient(string Status, string TicketNo)
        {

            AccessController.checkAccess(pageName);
            AddTicketClientBal objBAL = new AddTicketClientBal();

            int Result = -1;
            try
            {
                Result = objBAL.UpdateStatusByClient(TicketNo, Status);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (Result < 1)
                return "Status not updated.";


            return "Status updated successfully.";
        }



        [WebMethod]
        public static string Delete(int TicketID)
        {
            AccessController.checkAccess(pageName);
            AddTicketClientBal objBAL = new AddTicketClientBal();

            int Result = -1;
            try
            {
                Result = objBAL.Delete(TicketID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (Result < 1)
                return "Ticket not deleted.";


            return "Ticket deleted successfully.";
        }

    }
}