﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class TimeSheet123 : System.Web.UI.Page
    {
        private static string pageName = "TimeSheet.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static TimeSheetModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);

            List<TimeSheetModel> details = new List<TimeSheetModel>();
            TimeSheetBal objBAL = new TimeSheetBal();
            try
            {
                details = objBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Insert(List<TimeSheetModel> ObjModel)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon messageIcon = new MessageWithIcon();

            int result = -1;
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            // int UserID = 45;
            TimeSheetBal ObjBAL = new TimeSheetBal();
            try
            {
           //     result = ObjBAL.Insert(ObjModel, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (result <= 0)
                messageIcon.Message = "Record Not Submitted ";
            else
                messageIcon.Message = "Record Submitted";

            return messageIcon;
        }

        [WebMethod]
        public static TimeSheetModel[] Select()
        {
            AccessController.checkAccess(pageName);

            List<TimeSheetModel> details = new List<TimeSheetModel>();
            TimeSheetBal ObjBAL = new TimeSheetBal();
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            // int UserID = 45;
            try
            {
                details = ObjBAL.Select(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static TimeSheetModel[] SelectByID(string Date)
        {
            AccessController.checkAccess(pageName);

            List<TimeSheetModel> ObjModel = new List<TimeSheetModel>();
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            // int UserID = 45;
            TimeSheetBal objBal = new TimeSheetBal();
            try
            {
                ObjModel = objBal.SelectByID(Date, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return ObjModel.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon UpdateByID(int TimeSheetDetailID, int SystemID, string Task, int Time)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon messageIcon = new MessageWithIcon();
            TimeSheetModel ObjModel = new TimeSheetModel();
            int result = -1;

            ObjModel.TimeSheetDetailID = TimeSheetDetailID;
            ObjModel.SystemID = SystemID;
            ObjModel.Task = Task;
            ObjModel.Time = Time;
            TimeSheetBal objBal = new TimeSheetBal();
            try
            {
                result = objBal.UpdateByID(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result <= 0)
                messageIcon.Message = "Record Not Updated ";
            else
                messageIcon.Message = "Record Updated";

            return messageIcon;
        }

        [WebMethod]
        public static MessageWithIcon Update(int TimeSheetMasterID, string Date, string Reason)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon messageIcon = new MessageWithIcon();
            int result = -1;
            TimeSheetModel ObjModel = new TimeSheetModel();
            ObjModel.TimeSheetMasterID = TimeSheetMasterID;
            ObjModel.Date = Date;
            ObjModel.Reason = Reason;
            TimeSheetBal ObjBAL = new TimeSheetBal();
            try
            {
                result = ObjBAL.Update(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result <= 0)
                messageIcon.Message = "Record Not Updated";
            else
                messageIcon.Message = "Record Updated";

            return messageIcon;
        }

        [WebMethod]
        public static MessageWithIcon Delete(string Date)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon messageIcon = new MessageWithIcon();

            int result = -1;
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            // int UserID = 45;
            TimeSheetBal ObjBAL = new TimeSheetBal();
            try
            {
                result = ObjBAL.Delete(Date, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result <= 0)
                messageIcon.Message = "Record Not Deleted";
            else
                messageIcon.Message = "Record Deleted";

            return messageIcon;
        }
    }
}