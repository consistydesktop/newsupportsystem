﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Account.aspx.cs" Inherits="SupportSystem.Account" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .bs-example {
            margin: 20px;
        }
    </style>
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Account
                                <button class="btn btn-info buttons float-right" type="button" onclick="location.href='/TransactionDetailReport.aspx'">View</button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <div class="bs-example">
                <ul id="myTab" class="nav nav-pills">
                    <li class="nav-item">
                        <a href="#TransactionMaster" class="nav-link active">Transaciton Master</a>

                    </li>
                    <li class="nav-item">
                        <a href="#BankMaster" class="nav-link" onclick="return BankMasterSelect()">Bank Master</a>
                    </li>
                    <li class="nav-item">
                        <a href="#TargetMaster" class="nav-link" onclick="return TargetMasterSelect()">Target Master</a>
                    </li>
                    
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="TransactionMaster">
                        <form class="formbox mt-4">

                            <div class="form-group row mt-3">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    <label for="inputDate">Date </label>
                                    <input type="text" class="form-control loginput" id="inputDate" title="Date" aria-describedby="emailHelp" placeholder="Date" />
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    <label for="inputName">Name</label>
                                    <input type="text" class="form-control loginput" id="inputName" title="Name" aria-describedby="emailHelp" placeholder="Enter Name" onchange='$("#inputType").focus()' />
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    <label for="inputReferenceNo">Transaction Id</label>
                                    <input type="text" class="form-control loginput" id="inputReferenceNo" title="Transaction Id" aria-describedby="emailHelp" placeholder="Transaction Id"></input>
                                </div>
                            </div>
                            <div class="form-group row mt-3">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    <label for="inputAmount">Amount</label>
                                    <input type="text" class="form-control loginput" id="inputAmount" title="Transaction Id" aria-describedby="emailHelp" placeholder="Amount"></input>
                                </div>


                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    <label for="inputCRDR">Transaction Type</label>
                                    <select class="form-control loginput" id="inputCRDR" title="Select Type">
                                        <option value="0">Select Type</option>
                                        <option value="CR">CR </option>
                                        <option value="DR">DR </option>

                                    </select>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    <label for="inputBank" style="margin-bottom: 20px">Bank Name</label>
                                    <select class="form-control loginput select" id="inputBank" title="Select Bank">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mt-3">


                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="inputPayment" style="margin-bottom: 30px">Payment Type</label>
                                    <select class="form-control loginput select" id="inputPayment" title="Select Payment Type">
                                        <option value="0">Select Payment Type</option>
                                        <option value="Website Payment">Website Payment</option>
                                        <option value="Desktop Payment">Desktop Payment</option>
                                        <option value="Website Subscription">Website subscription</option>
                                        <option value="Desktop subscription">Desktop subscription</option>
                                        <option value="Salary">Salary </option>
                                        <option value="Bill Tea">Bill Tea</option>
                                        <option value="SMS ">SMS </option>
                                        <option value="LighBill">LightBill</option>
                                        <option value="Net Bill">Net Bill</option>
                                        <option value="Rent">Rent</option>
                                        <option value="Domain">Domain</option>
                                        <option value="Other">Other</option>

                                    </select>
                                </div>

                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="inputRemark">Remark</label>
                                    <textarea class="form-control loginput" id="inputRemark" aria-describedby="emailHelp" placeholder="Remark"></textarea>
                                </div>

                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="inputIsGST">
                                        <input type="checkbox" checked="checked" class="form-control loginput" id="inputIsGST" title="Is GST" aria-describedby="emailHelp" />Is GST
                                    </label>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="inputSaveButton" title="SaveButton" onclick="TransactionMasterInsert()">Save</button>
                                            <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return TransactionMasterUpdate();" id="inputUpdateButton" title="Update Transaction" style="display: none">Update</button>
                                            <input type="hidden" id="hdnTransactionMasterID">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>


                    <div class="tab-pane fade" id="BankMaster">
                        <form class="formbox mt-4">
                            <div class="form-group row mt-3">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="inputBankName">Name</label>
                                    <input type="text" class="form-control loginput" id="inputBankName" title="Bank Name" aria-describedby="emailHelp" placeholder="Name"></input>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="inputAccountNumber">Account Number</label>
                                    <input type="text" class="form-control loginput" id="inputAccountNumber" title="Account Number" aria-describedby="emailHelp" placeholder="Account Number"></input>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="inputBranchName">Branch Name</label>
                                    <input type="text" class="form-control loginput" id="inputBranchName" title="Branch Name" aria-describedby="emailHelp" placeholder="Branch Name"></input>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="inputIFSC">IFSC Code</label>
                                    <input type="text" class="form-control loginput" id="inputIFSC" title="IFSC Code" aria-describedby="emailHelp" placeholder="IFSC Code"></input>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="inputAdd" title="Add" onclick="BankMasterInsert()">Add</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                        <div class="container-fluid ">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="projects" role="tabpanel" aria-labelledby="projects-tab">
                                            <div class="table-responsive">
                                                <table id="BankDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                                    <thead>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="TargetMaster">
                        <form class="formbox mt-4">
                            <div class="form-group row mt-3">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="inputMonth">Month</label>
                                    <select class="form-control loginput" id="inputMonth" title="Select Month">
                                        <option value="0">Select Month</option>
                                        <option value="Jan">Jan</option>
                                        <option value="Feb">Feb</option>
                                        <option value="March">March </option>
                                        <option value="April">April</option>
                                        <option value="May">May</option>
                                        <option value="Jun">Jun</option>
                                        <option value="July">July</option>
                                        <option value="Aug">Aug </option>
                                        <option value="Sept">Sept</option>
                                        <option value="Oct">Oct</option>
                                        <option value="Nov">Nov</option>
                                        <option value="Dec">Dec</option>

                                    </select>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="inputYear">Year</label>
                                    <select class="form-control loginput" id="inputYear" title="Select Year">
                                        <option value="">Select Year</option>
                                    </select>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <label for="inputTargetAmount">Target Amount</label>
                                    <input type="text" class="form-control loginput" id="inputTargetAmount" title="Target Amount" aria-describedby="emailHelp" placeholder="Target Amount"></input>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="inputSet" title="Set" onclick="TargetMasterInsert()">Set</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="container-fluid ">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tab-content" id="myTabContent1">
                                        <div class="tab-pane fade show active" id="projects1" role="tabpanel" aria-labelledby="projects-tab">
                                            <div class="table-responsive">
                                                <table id="TargetMasterDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                                    <thead>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>


    <script src="JS/TransactionMaster.js"></script>
    <script src="JS/BankMaster.js"></script>
    <script src="JS/TargetMaster.js"></script>

    <script type="text/javascript">
        var Users = [];
        $(document).ready(function () {

            $("#myTab a").click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            $("#inputDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });

            $("#inputDate").datepicker().datepicker("setDate", new Date());
            var Status = "activated";


            $("#inputName").focus();
            SelectUser();

            autocomplete(document.getElementById("inputName"), Users);


            var n = new Date().getFullYear();
            for (y = n; y <= n + 100; y++) {
                var optn = document.createElement("OPTION");
                optn.text = y;
                optn.value = y;

                // if year is 2015 selected
                if (y == 2019) {
                    optn.selected = true;
                }

                document.getElementById('inputYear').options.add(optn);
            }
            SelectBankName();

            if (window.location.href.indexOf("TransactionMasterID") > 0) {
                var url = window.location.href;
                var id = url.split('=');
                $("#hdnTransactionMasterID").val(id[1]);
                TransactionMasterSelectByID(id[1]);
                $("#inputSaveButton").css('display', 'none');
                $("#inputUpdateButton").show();

            }

        });

    </script>


</asp:Content>
