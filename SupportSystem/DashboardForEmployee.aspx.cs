﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class DashboardForEmployee : System.Web.UI.Page
    {
        private static string pageName = "DashboardForEmployee.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static DashboardForEmployeeModel[] SelectProjectDetails()
        {
            AccessController.checkAccess(pageName);

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            List<DashboardForEmployeeModel> details = new List<DashboardForEmployeeModel>();

            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                details = objBAL.SelectProjectDetails(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static int SelectPendingProjects()
        {
            AccessController.checkAccess(pageName);

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            int result = 0;

            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                result = objBAL.SelectPendingProjects(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        [WebMethod]
        public static int SelectPendingCalls()
        {
            AccessController.checkAccess(pageName);

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            int result = 0;

            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                result = objBAL.SelectPendingCalls(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        [WebMethod]
        public static int SelectPendingTickets()
        {
            AccessController.checkAccess(pageName);

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            int result = 0;

            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                result = objBAL.SelectPendingTickets(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        [WebMethod]
        public static int SelectPendingTasks()
        {
            AccessController.checkAccess(pageName);

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            int result = 0;

            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                result = objBAL.SelectPendingTasks(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        [WebMethod]
        public static int SelectPendingBugs()
        {
            AccessController.checkAccess(pageName);

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            int result = 0;

            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                result = objBAL.SelectPendingBugs(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        [WebMethod]
        public static DashboardForEmployeeModel[] SelectTicketDetails()
        {
            AccessController.checkAccess(pageName);

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            List<DashboardForEmployeeModel> details = new List<DashboardForEmployeeModel>();

            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                details = objBAL.SelectTicketDetails(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static DashboardForEmployeeModel[] SelectCallDetails()
        {
            AccessController.checkAccess(pageName);

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            List<DashboardForEmployeeModel> details = new List<DashboardForEmployeeModel>();

            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                details = objBAL.SelectCallDetails(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static DashboardForEmployeeModel[] SelectBugDetails()
        {
            AccessController.checkAccess(pageName);

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            List<DashboardForEmployeeModel> details = new List<DashboardForEmployeeModel>();

            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                details = objBAL.SelectBugDetails(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static DashboardForEmployeeModel[] SelectTaskDetails()
        {
            AccessController.checkAccess(pageName);

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            List<DashboardForEmployeeModel> details = new List<DashboardForEmployeeModel>();

            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                details = objBAL.SelectTaskDetails(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon UpdateStatus(string Status, int TaskID, string FileIDs)
        {
            AccessController.checkAccess(pageName);
            DashboardForEmployeeModel objModel = new DashboardForEmployeeModel();
            MessageWithIcon msgIcon = new MessageWithIcon();

            int i = 0;

            objModel.TaskID = TaskID;
            objModel.Status = Status;
            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                i = objBAL.UpdateStatus(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "Status not updated";
            else
            {
                msgIcon.Message = "Status updated successfully";

                int j = 0;
                String Files = FileIDs;
                string[] fileID = Files.Split('#');

                if (fileID.Length < 1)
                    return msgIcon;

                for (j = 1; j < fileID.Length; j++)
                {
                    var currentFileID = fileID[j];
                    objModel.FileID = Convert.ToInt32(currentFileID);
                    objModel.TaskID = i;
                    objModel.UploadType = UploadTypeMaster.UploadTaskResolved;

                    try
                    {
                        objBAL.FileMappingInsert(objModel);
                    }
                    catch (Exception Ex) { new Logger().write(Ex); }
                }
            }
            return msgIcon;
        }

        [WebMethod]
        public static DashboardForEmployeeModel[] SelectByTaskID(int TaskID)
        {
            AccessController.checkAccess(pageName);
            DashboardForEmployeeModel objModel = new DashboardForEmployeeModel();
            List<DashboardForEmployeeModel> details = new List<DashboardForEmployeeModel>();

            objModel.UploadType = UploadTypeMaster.UploadTask;
            objModel.TaskID = TaskID;
            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                details = objBAL.SelectByTaskID(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }
        [WebMethod]
        public static string  SelectDescriptionByTaskID(int TaskID)
        {
            AccessController.checkAccess(pageName);
            DashboardForEmployeeModel objModel = new DashboardForEmployeeModel();
            string description=""; 

            
            objModel.TaskID = TaskID;
            DashboardForEmployeeBal objBAL = new DashboardForEmployeeBal();
            try
            {
                description = objBAL.SelectDescriptionByTaskID(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return description;
        }
       
    }
}