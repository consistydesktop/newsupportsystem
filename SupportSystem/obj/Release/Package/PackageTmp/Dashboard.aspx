﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="SupportSystem.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Dashboard                          
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">
                <div class="form-group row mt-3">
    
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="card" style="width: 18rem;">

                            <div class="card-body">
                                <h5 class="card-title">Ticket Details</h5>
                                <hr />
                                <p>
                                    <b>Total :</b><a href="ViewTicketsAdmin.aspx"><label id="TotalTickets"></label></a>
                                </p>
                                <p>
                                    <b>Resolved :  </b><a href="ViewTicketsAdmin.aspx">
                                        <label id="ResolvedTickets"></label>
                                    </a>
                                </p>
                                <p>
                                    <b>Pending :  </b><a href="ViewTicketsAdmin.aspx">
                                        <label id="PendingTickets"></label>
                                    </a>
                                </p>
                                <p>
                                    <b>New :  </b><a href="ViewTicketsAdmin.aspx">
                                        <label id="NewTickets"></label>
                                    </a>

                                </p>
                                <p>
                                </p>
                            </div>

                        </div>
                    </div>
       
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12" style="display: none">
                        <div class="card" style="width: 18rem;">

                            <div class="card-body">
                                <h5 class="card-title">Task Details</h5>
                                <hr />
                                <p>
                                    Total Task:<label id="TotalTasks"></label>
                                </p>
                                <p>
                                    Completed Tickets:<label id="CompletedTasks"></label>
                                </p>
                                <p>
                                    Pending Tickets:<label id="PendingTasks"></label>
                                </p>
                                <p>
                                    New :<label id="NewTasks"></label>
                                </p>
                                <a href="/ViewTaskAdmin.aspx" class="btn btn-primary">View Tasks</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                          <div class="card" style="width: 18rem;">

                            <div class="card-body">
                                <h5 class="card-title">Subscription Details</h5>
                                <hr />
                                <p>
                                    <b>Total Subscription Amount For Current Month :</b><a href="SubscriptionRenovationReport.aspx"><label id="TotalSubscriptionAmount"></label></a>
                                </p>
                           
                                <p>
                                </p>
                            </div>

                        </div>

                    </div>
                </div>
            </form>
        </section>
                <section>

             <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Unpaid Subscriptions                          
                            </h6>
                        </div>
                    </div>
                </div>
            </section>

        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-12">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="projects" role="tabpanel" aria-labelledby="projects-tab">
                            <div class="table-responsive">
                                <table id="SubscriptionDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </section>
    </div>
    <script src="/JS/Dashboard.js"></script>
    <script>
        SelectTicketCount();
        SelectUnpaidSubscription(); 
        SelectpaidSubscriptionDetails();
        //     SelectTaskCount();
    </script>
</asp:Content>
