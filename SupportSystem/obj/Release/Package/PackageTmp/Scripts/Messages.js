﻿var successInsert = '{0} inserted successfully';
var successUpdate = '{0} updated successfully';
var successDelete = '{0} deleted successfully';
var errorMessage = 'Oops ,Something wrong during {0}';
var pleaseEnter = 'Please, enter {0}';
var pleaseSelect = 'Please, select {0}';
var request = '{0} request accepted successfully';
var notRequest = '{0} request not accepted successfully';
var duplicate = 'Alraedy exit {0}';
var notpresent = '{0} is not present';
var balance = 'Your opening balance is not enough {0}';
var balanceReverse = 'Insufficient balance {0}';
var transfer = '{0} successfully';
var notfound = 'Unable to find data';
var notupdate = 'Unable to update';
var notdelete = 'Unable to delete';
var notinsert = 'Unable to insert';
var nottransfer = 'Unable to transfer';
var missing = 'Missing Parameter';
var Activate = 'Activate successfully';
var commission = 'Retailer commission can not be greather than total commission';
var update = 'Are you sure to update?';
var comup = 'Are you sure to update all operator commission';
var actdeact = 'Are you sure to activate/deactivate services';
var validData = 'Please enter valid data';

//var rechargeDialog = "Please verify the payment and click 'Ok' to proceed.<br/>Recharge Number :{0} <br/>Recharge Amount :{1} <br/>Operator Name :{2} <br/>By cliking on the 'Ok' button,the recharge amount will be deducted from your current balance.";
var rechargeInProcess = "Recharge of number {0} and Amount {1} In Process";

var rechargeDialog = 'Are you sure to recharge?</br> Number:  <b style="color:green;font-size:large;"> {0}</b>,</br> Amount: <b style="color:green;font-size:large;"> {1}, </b></br> Operator: <b style="color:green;font-size:large;">{2}</b>';

//var rechargeInProcess = 'Recharge of number <b style="color:green;font-size:large;">{0}</b> and Amount <b style="color:green;font-size:large;">{1}</b> is accepted successfully';

var session = "Session Expired,Please Re-login";
var nodata = "Data not available";
var deleteask = "Are you sure to delete";
var pay = "Are you sure to pay?";
var cont = "Are you sure to continue?";
var requestBal = "Are you sure to Request Amount of";
var reject1 = "Request rejected successfully";
var rejectask = "Are you sure to reject request?";
var insufficientbalance = "Insufficient balance please contact to admin.";
var confirmation = 'Are you sure to update?';
var reject = 'Refund rejected';
var balUserCheck = "Sorry user not transfer balance to self";
var duplicateUserName = "User name already exist.";
var duplicateMobileNo = "Mobile number already exist";
var duplicateEmailID = "Email ID already exist.";
var Days = "Unable to change {0},Please change after{1}";
var R = "R0";
var D = "D0";
var A = "00";
var F = "C0";
var P = "A0";
var S = "S0";
var age = "User should be minimum 18Yrs old";
var correctOTP = "Please enter correct OTP";
var tc = "Please check the terms and condition";
var limit = "OTP limitation is only 5";
var DetailsNotFound = "{0} not found";

var EnquiryReply = "Enquiry reply send successfully with TxnID: {0}";

var SenderRegistrationSuccess = "Sender registerd successfully";

var BeneficairyAddedSuccess = "Beneficairy added successfully";
var BeneficairyAddedFail = "Something wrong during Beneficairy add";

var SenderNotRegistered = "Sender Is Not Registered , Kindly Register First !";

var BeneficiaryNotfound = "No beneficiary found! Kindly Add Beneficiary First";

function getPrefixName(UserTypeID, UserID) {
    if (UserTypeID == 3) {
        return String.format(R + UserID);
    }
    else if (UserTypeID == 2) {
        return String.format(D + UserID);
    }
    else if (UserTypeID == 6) {
        return String.format(F + UserID);
    }
    else if (UserTypeID == 4) {
        return String.format(P + UserID);
    }
    else if (UserTypeID == 5) {
        return String.format(S + UserID);
    }
    else {
        return String.format(A + UserID);
    }
}

function getID(RechargeID) {
    var str = "" + RechargeID;
    var pad = "0000"
    var ID = pad.substring(0, pad.length - str.length) + str
    return ID;
}