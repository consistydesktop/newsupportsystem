﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserInformation.aspx.cs" Inherits="SupportSystem.UserInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>User Information
                                  
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">

                <div class="form-group row mt-3">

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputUser">User</label>
                        <select class="form-control loginput" id="inputUser" title="Select User" onchange="UserInformationSelectForAll()">
                            <option value="0">Select User</option>
                            <option value="1">Admin </option>
                            <option value="4">Support </option>
                            <option value="2">Employee </option>
                            <option value="3">Client </option>
                        </select>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    </div>
                </div>
            </form>
        </section>
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-12">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="projects" role="tabpanel" aria-labelledby="projects-tab">
                            <div class="table-responsive">
                                <table id="UserInformationDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>

                                <table id="ClientInformation" class="table table-bordered nowrap table-hover">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

   
               
    <script src="/JS/UserInformation.js"></script>
    <script>


    </script>
</asp:Content>
