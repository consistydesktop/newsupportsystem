﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewTestProject.aspx.cs" Inherits="SupportSystem.NewTestProject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Test Panel                                   
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                        <label for="inputSearchProjectName">Jump to Project</label>
                        <select id="inputSearchProjectName" onchange="OpenProject()"></select>
                    </div>
                </div>
            </form>
            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                        <label for="inputProjectName">Create New Project</label>
                        <input type="text" class="form-control loginput" id="inputProjectName" title="Project Name" aria-describedby="emailHelp" placeholder="Project Name" onchange='$("#SubmitButton").focus()' />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" style="border-radius: 12px;" onclick=" CreateNewProject();" id="SubmitButton" title="Create">Create</button>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table id="NewProjects" class="table table-bordered nowrap table-hover" >
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
    <script src="/JS/NewTestProject.js"></script>
    <script>

        var ProjetBugID = 0;
        var count = 0;
        ProjectBugMasterSelect();

        $(document).ready(function () {
            $("#inputProjectName").focus();

        });
    </script>
</asp:Content>
