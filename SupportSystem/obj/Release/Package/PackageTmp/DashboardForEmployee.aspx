﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DashboardForEmployee.aspx.cs" Inherits="SupportSystem.DashboardForEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead mt-3">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link tasks active" id="tasks-tab" data-toggle="tab" href="#tasks" role="tab" aria-controls="tasks" aria-selected="false">

                                        <span>Tasks</span>
                                        <span class="badge"></span>
                                    </a>

                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tickets" id="tickets-tab" data-toggle="tab" href="#tickets" role="tab" aria-controls="tickets" aria-selected="false">Tickets</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link calls" id="calls-tab" data-toggle="tab" href="#calls" role="tab" aria-controls="calls" aria-selected="false">Calls</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link projects" id="projects-tab" data-toggle="tab" href="#projects" role="tab" aria-controls="projects" aria-selected="true">Projects</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-fluid mt-5">
                <div class="row">
                    <div class="col-12">

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade " id="projects" role="tabpanel" aria-labelledby="projects-tab">

                                <div class="table-responsive">
                                    <table id="ProjectDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                        <thead></thead>
                                        <tbody></tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="tickets" role="tabpanel" aria-labelledby="tickets-tab">
                                <div class="table-responsive">
                                    <table id="TicketDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                        <thead></thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="calls" role="tabpanel" aria-labelledby="calls-tab">
                                <div class="table-responsive">
                                    <table id="CallDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                        <thead></thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade show active" id="tasks" role="tabpanel" aria-labelledby="tasks-tab">
                                <div class="table-responsive ">
                                    <table id="TaskDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                        <thead></thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    </div>

    <label id="TaskID" style="display: none"></label>

    <div class="modal fade" id="modal_Sender" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="H2">Description</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span id="txtDescription" style="height: 100%;  width: 100%;" disabled="disabled"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_status" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="H1">Update Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <select name="Status" id="ddlStatus" class="form-control" data-toggle="tooltip" data-placement="top" title="Ticket Status" style="height: 36px;">
                        <option value="0">Select Status</option>
                        <option value="INPROGRESS">Inprogress</option>
                        <option value="COMPLETE">Completed</option>
                    </select>
                    <br />
                    <input type="file" id="inputTaskAttachment" title="Attach file" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="return TaskMasterMasterUpdateStatus();" title="Update status" data-dismiss="modal">Update Status</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_attachment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="H3">Attachment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid alltrans">
                        <div class="table-responsive">
                            <table id="Attachments" class="table table-bordered nowrap" cellspacing="0" width="100%">
                                <thead>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="/JS/DashboardForEmployee.js"></script>
    <script>

        ProjectDetailMasterSelectForEmployee();
        TicketInformationSelectForEmployee();
        CallMasterSelectForEmployee();
        TaskMasterSelectForEmployee();
        $(document).ready(function () {

        });
    </script>
</asp:Content>
