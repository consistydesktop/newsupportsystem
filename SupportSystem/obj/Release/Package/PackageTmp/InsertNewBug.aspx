﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InsertNewBug.aspx.cs" Inherits="SupportSystem.InsertNewBug" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>::
                                <label id="ProjectName"></label>
                                ::
                            </h6>
                        </div>
                    </div>
                </div>

                <div class="row" style="padding: 14px">
                    <div class="col-md-4">
                        <span class="btn btn-info" style="color: white">
                            <a data-toggle="modal" onclick="return ReportNewBug()"  data-target="#modal_ReportNewBug">
                                <i class="fa fa-bug"></i>Report new bug                         
                            </a>
                        </span>
                    </div>

                    <div class="col-md-4">
                        <label id="OutputMsg" style="float: right;"></label>
                    </div>
                    <div class="col-md-4">
                        <span class="btn btn-info" style="float: right; color: white">
                            <a href="NewTestProject.aspx" style="color: white">
                                <i class="fa fa-arrow-left"></i>Back To Project
                            </a>
                        </span>

                    </div>
                </div>
            </section>
            <label id="ProjectDetailMasterID" style="display: none"></label>
            <label id="BugID" style="display: none"></label>
            <label id="ProjectBugMasterID" style="display: none"></label>
            <form class="formbox mt-4">
                <div class="form-group row mt-4">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputFromDate">From Date </label>
                        <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputToDate">To Date</label>
                        <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()'></input>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                         <div class="row" >

                                        <label for="inputStatus"><b> Status </b></label>
                                        <select class="form-control loginput" id="txtStatus" title="Select Issue Type " onchange='IsStatusFeedback()'>
                                            <option value='0'>Select Status </option>
                                            <option value="NEW" selected="selected">New</option>
                                            <option value="CONFIRMED">Confirmed</option>
                                            <option value="ACKNOWLEDGED">Acknowledged</option>
                                            <option value="INPROGRESS">In progress</option>
                                            <option value="FEEDBACK">Feedback</option>
                                            <option value="RESOLVED">Resolved</option>
                                            <option value="CLOSED">Closed</option>
                                        </select>
                                    </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return BugMasterSelect('search')" id="SearchButton" title="Search">Search</button>
                            </div>
                        </div>
                        <label id="ResponseMsgLbl"></label>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table id="BugDetails" class="table table-bordered  table-hover">
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <div class="modal fade" id="modal_ReportNewBug" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document" style="min-width: 70%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="InsertHeader"><i class="fa fa-bug"></i>Report a new bug</h5>
                            <h5 class="modal-title" id="UpdateHeader" style="display: none"><i class="fa fa-bug"></i>Update a bug</h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body row">
                            <div class="col-md-6">
                                <div class="container">
                                    <h5><i class="fa fa-pencil">Information</i> </h5>
                                    <hr />
                                    <div class="row">
                                        <label for="inputBugTitle"><b>Bug Title</b></label>

                                        <input type="text" class="form-control loginput" id="inputBugTitle" title="Bug Title" aria-describedby="emailHelp" onfocus="$('#lblinputBugTitle').hide()" autofocus="autofocus" autocomplete="off" />
                                        <label style="display: none; color: red;" id="lblinputBugTitle">
                                            Title is required
                                        </label>
                                        <hr />
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <label for="inputIssueType"><b>Select Issue Type (Optional)</b></label>
                                            <select class="form-control loginput" id="inputIssueType" title="Select Issue Type " onchange='$("#inputPriority").focus()'>
                                                <option value='0'>Select Issue Type </option>
                                                <option value="FUNCTIONAL">Functional</option>
                                                <option value="USABILITY">Usability</option>
                                                <option value="TEXT">Text</option>
                                                <option value="VISUAL">Visual</option>
                                                <option value="SUGGESTION">Suggestion</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <label for="inputPriority"><b>Select Priority</b></label>
                                            <select class="form-control loginput" id="inputPriority" title="Select Priority" onchange='$("#inputProductType").focus()' onfocus="$('#lblinputPriority').hide()">
                                                <option value='0'>Select Priority</option>
                                                <option value="LOW">Low</option>
                                                <option value="MEDIUM">Medium</option>
                                                <option value="HIGH">High</option>
                                                <option value="CRITICAL">Critical</option>
                                            </select>
                                            <label style="display: none; color: red;" id="lblinputPriority">
                                                Please select Priority
                                            </label>
                                        </div>
                                    </div>


                                    <hr />
                                    <div class="row">
                                        <label for="inputAttachment"><i class="fa fa-paperclip"><b>Attachments (Optional)</b></i></label>
                                        <input type="file" class="form-control loginput" id="inputAttachment" title="Attachment" aria-describedby="emailHelp" multiple="multiple" />
                                    </div>
                                    <div class="row">
                                        <table id="Attachments" class="table table-bordered nowrap table-hover">
                                            <thead>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="container">
                                    <h5><i class="fa fa-pencil-square-o">Extra Details</i> </h5>
                                    <hr />
                                    <div class="row">
                                        <label for="inputDescription"><b>Description (Optional)</b></label>
                                        <textarea class="form-control loginput" id="inputDescription" title="Description" aria-describedby="emailHelp"></textarea>
                                    </div>
                                    <div class="row">
                                        <label for="inputExpectedResult"><b>Expected Result</b></label>
                                        <textarea class="form-control loginput" id="inputExpectedResult" onfocus="$('#lblinputExpectedResult').hide()"></textarea>
                                        <label style="display: none; color: red;" id="lblinputExpectedResult">
                                            Please enter expected result
                                        </label>
                                    </div>
                                    <div class="row">
                                        <label for="inputURL"><b>URL (Optional)</b></label>
                                        <input type="text" class="form-control loginput" id="inputURL" title="URL" aria-describedby="emailHelp" onchange='$("#inputEmployee").focus()' />
                                    </div>
                                    <div class="row" id="StatusDiv" style="display: none">

                                        <label for="inputStatus"><b>Change status </b></label>
                                        <select class="form-control loginput" id="inputStatus" title="Select Issue Type " onchange='IsStatusFeedback()'>
                                            <option value='0'>Select Status </option>
                                            <option value="NEW" selected="selected">New</option>
                                            <option value="CONFIRMED">Confirmed</option>
                                            <option value="ACKNOWLEDGED">Acknowledged</option>
                                            <option value="INPROGRESS">In progress</option>
                                            <option value="FEEDBACK">Feedback</option>
                                            <option value="RESOLVED">Resolved</option>
                                            <option value="CLOSED">Closed</option>
                                        </select>
                                    </div>

                                    <div class="row" id="CommentSection" style="display: none">
                                        <textarea id="inputComment" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="container">
                                <div class="row">
                                    <div class="col-6 col-md-6">
                                        <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick="ResetAll();" title="Clear All">Clear</button>

                                    </div>
                                    <div class="col-6 col-md-6">
                                        <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="SubmitBug" id="SubmitButton" title="Report Bug">Report</button>
                                        <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="updateBug" id="UpdateButton" title="Update " style="display: none">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>

    <div class="loader" id="loader" style="display: none"></div>

    <div class="modal fade" id="modal_ViewReportedNewBug" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="min-width: 70%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="H2">View Bug </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row">

                    <div class="col-md-7">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5><i class="fa fa-pencil">Information</i> </h5>
                                    <hr />
                                    <div class="row">
                                        <b># 
                                    <label id="LblViewBugID"></label>
                                        </b>
                                    </div>
                                    <div class="row">
                                        <b>
                                            <label id="LblViewBugTitle"></label>
                                        </b>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                </div>

                            </div>


                            <div class="row">
                                <h5>Description</h5>
                                <textarea class="form-control loginput" id="viewDescription"></textarea>
                            </div>
                            <div class="row">
                                <h5>Expected Result</h5>
                                <textarea class="form-control loginput" id="viewExpectedResult"></textarea>
                            </div>
                            <div class="row">
                                <h5>Attachments</h5>
                                <hr />
                                <div class="row">
                                    <input type="file" multiple="multiple"/>
                                </div>
                                <div class="row" id="viewAttachments">
                                </div>

                                <hr />


                            </div>
                        </div>


                    </div>

                    <div class="col-md-5">
                        <div class="container">
                            <a class="btn-info" data-toggle="modal" data-target="#modal_ReportNewBug"><i class="fa fa-bug"></i>Report new bug</a>
                            <hr />
                            <h5><i class="fa fa-pencil-square-o">Extra Details</i> </h5>
                            <hr />
                            <div class="row">
                                <label for="updateURL"><b>URL</b></label>
                                <input type="text" class="form-control loginput" id="updateURL" title="URL" aria-describedby="emailHelp" onchange='$("#inputEmployee").focus()' />
                            </div>

                            <div class="row">
                                <label for="inputURL"><b>Type</b></label>
                                <select class="form-control loginput" id="updateIssueType" title="Select Issue Type " onchange='$("#inputPriority").focus()'>
                                    <option value='0'>Select Issue Type </option>
                                    <option value="Functional">Functional</option>
                                    <option value="Usability">Usability</option>
                                    <option value="Text">Text</option>
                                    <option value="Visual">Visual</option>
                                    <option value="Suggestion">Suggestion</option>
                                </select>

                            </div>


                            <div class="row">
                                <h5>Select Priority </h5>
                                <select class="form-control loginput" id="updatePriority" title="Select Priority" onchange='$("#inputProductType").focus()' onfocus="$('#lblinputPriority').hide()">
                                    <option value='0'>Select Priority</option>
                                    <option value="Low">Low</option>
                                    <option value="Medium">Medium</option>
                                    <option value="High">High</option>
                                    <option value="Critical">Critical</option>
                                </select>
                            </div>


                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <div class="container">
                        <div class="row">
                            <div class="col-6 col-md-6">
                            </div>
                            <div class="col-6 col-md-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick="ResetAll();" title="Clear All">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


    <div class="modal fade" id="modal_attachment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Bug Attachment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_ViewBug" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="">View Bug </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="DisplayAttachment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="min-width: 100%">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="H3">Attachment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <img src="#" class="modal-content" id="img1" />
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>


    <script src="/JS/InsertNewBug.js"></script>
    <script>


        $(document).ready(function () {


            $("#inputFromDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });
            $("#inputFromDate").datepicker().datepicker("setDate", new Date());
            $("#inputToDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });
            $("#inputToDate").datepicker().datepicker("setDate", new Date());

            $("#inputBugTitle").focus();

            //   UserInformationSelectByEmployeeID();
            // SystemMasterSelect();
            //    TestingIssueMasterSelect();

            ProjectBugMasterSelect();
            ReportNewBug();
            BugMasterSelect("Initial");

        });

        function IsStatusFeedback() {
            var Status = $("#inputStatus").val();

            if (Status == "FEEDBACK".toUpperCase()) {
                $("#CommentSection").show();

            }
            else {
                $("#CommentSection").hide();
            }
        }

        function viewAttachments(src) {

        }
    </script>
</asp:Content>
