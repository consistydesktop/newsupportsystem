﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DashboardForCustomer.aspx.cs" Inherits="SupportSystem.DashboardForCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Dashboard                          
                            </h6>
                        </div>
                    </div>
                </div>
            </section>

            <div class="row">
                <div class="card" style="width: 18rem;">

                    <div class="card-body">
                        <h5 class="card-title">Ticket Details</h5>
                        <p>Total Tickets:<label id="TotalTickets"></label>
                        </p>
                        <p></p>
                        <a href="/ViewTicketClient.aspx" class="btn btn-primary">View Tickets</a>
                    </div>

                </div>


            </div>

        </section>
    </div>
    <script src="/JS/DashboardForCustomer.js"></script>

    <script>
        SelectTicketCount();
    </script>
</asp:Content>
