﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TimeSheet.aspx.cs" Inherits="SupportSystem.TimeSheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
        .chosen-container {
            width: 100% !important;
        }
    </style>

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Time Sheet
                                <button class="btn btn-info buttons float-right" type="button" title="View TimeSheet" onclick="location.href='/TimeSheetReport.aspx'">View TimeSheet</button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputDate">Date</label>
                        <input type="text" class="form-control loginput" id="inputDate" aria-describedby="emailHelp" placeholder="Select Date" title="Select Date" onchange=" TimeSheetMasterSelectByDate('')" />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    </div>
                </div>

                <div class="form-group row mt-3">
                    <div class="table table-responsive " style="overflow: inherit;">
                        <table id="TimeSheetDetails" class="table table-bordered nowrap table-hover ">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </form>
        </section>
    </div>

    <script src="/JS/TimeSheet.js"></script>
    <script>
        var count = 0;
        var List = [];
        var JSonObjects = [];
        var TotalTime = 0;
        var TimeSheetObjects = [];
        var SystemObjects = [];
        var time = 0;

        SystemMasterSelect();

        $("#inputDate").datepicker({
            dateFormat: 'd MM, yy',
            maxDate: 0,
            minDate: '-5d',
            changeMonth: true,
            changeYear: true
        });
        $("#inputDate").datepicker().datepicker("setDate", new Date());


        var TimeSheetData = "";

        function IsNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57));
            return ret;
        }

        TimeSheetMasterSelectByDate($("#inputDate").val());

    </script>
</asp:Content>
