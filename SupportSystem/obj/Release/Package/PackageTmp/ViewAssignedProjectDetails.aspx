﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewAssignedProjectDetails.aspx.cs" Inherits="SupportSystem.ViewAssignedProjectDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Assigned Project
                                <button class="btn btn-info buttons float-right" type="button" onclick='location.href="/ProjectAssignScreen.aspx"' title="View Assigned Project">
                                    Assign Project
                                </button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <form class="formbox mt-4">

            <div class="form-group row mt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    <label for="inputFromDate">From Date </label>
                    <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    <label for="inputToDate">To Date</label>
                    <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" />
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-6">
                            <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return Search();" id="SearchButton" title="Search">Search</button>
                        </div>

                    </div>

                </div>
            </div>
        </form>
        <div class="loader" id="loader" style="display: none"></div>
        <label id="ProjectDetailMasterID" style="display: none"></label>
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-12">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="projects" role="tabpanel" aria-labelledby="projects-tab">
                            <div class="table-responsive">
                                <table id="ViewAssignedEmployeeDetails" class="table" style="width: 100%">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_developers" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Assigned Employees</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="Developers" class="table table-bordered nowrap" style="width: 100%" cellspacing="0">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_attachment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="h5">Attachments</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="Attachments" class="table table-bordered nowrap" style="height: 100%; width: 100%">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_Sender" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="H2">Description</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea id="txtDescription" style="height: 100%; width: 100%;"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modal_status" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: rgb(16, 206, 239);">
                    <h5 class="modal-title">Project Status </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close" style="margin-top: -21px">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group row text-center">
                        <div class="col-12">

                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2"></div>
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
                                <select name="Status" id="ddlStatus" class="form-control" data-toggle="tooltip" data-placement="top" title="Ticket Status" style="height: 36px;">
                                    <option value="0">Select Status</option>
                                    <option value="INPROGRESS">Inprogress</option>
                                    <option value="COMPLETED">Completed</option>
                                    <option value="CANCELLED">Cancelled</option>
                                </select>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"></div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <button type="button" class="btn form-control btn-success" data-dismiss="modal" onclick="return ProjectDetailMasterUpdateStatus();" title="Status">Update Status</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="JS/ViewAssignedProjectDetails.js"></script>

    <script>
        $("#inputFromDate").datepicker({
            dateFormat: 'd MM, yy',
            changeMonth: true,
            changeYear: true,

        });
        $("#inputToDate").datepicker({
            dateFormat: 'd MM, yy',
            changeMonth: true,
            changeYear: true,

        });
        $(document).ready(function () {

            ProjectDetailMasterSelect();

            $("#SearchButton").focus();
        });
    </script>

</asp:Content>
