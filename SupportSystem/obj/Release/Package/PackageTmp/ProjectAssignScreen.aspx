﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProjectAssignScreen.aspx.cs" Inherits="SupportSystem.ProjectAssignScreen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Assign Project
                               <button class="btn btn-info buttons float-right" type="button" onclick='location.href="/ViewAssignedProjectDetails.aspx"' title="View Assigned Project">
                                   View Assigned Project
                               </button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>

            <form class="formbox mt-4">
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputProjectName">Project Name</label>
                        <input type="text" class="form-control loginput" id="inputProjectName" title="Project Name" aria-describedby="emailHelp" onchange='$("#inputService").focus()' />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputService">Service</label>
                        <select class="form-control loginput" id="inputService" title="Select Service" onchange='$("#inputProductType").focus()'>
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputEmployee">Employees (s)</label>
                        <select class="form-control loginput" id="inputEmployee" multiple="multiple" title="Select Employees " onchange='$("#inputDescription").focus()'>
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputAttachment">Document (Optional)</label>
                        <input type="file" class="form-control loginput" id="inputAttachment" multiple="multiple" title="Attachment" aria-describedby="emailHelp" onchange='$("#inputEmployee").focus()' />
                        <button type='button' id="DownloadAttachmentButton" class='btn' title='download' style='display: none; background-color: white; border-color: #fff;' onclick='return DownloadAttachment()'><i class='fa fa-download' style='font-size: 20px; color: blue'></i></button>
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputProductType">Product Type (Optional)</label>
                        <select class="form-control loginput" id="inputProductType" title="Select Product Type" onchange='OtherProduct()'>
                        </select>
                        <label for="inputOtherProductType" id="labelinputOtherProductType" style="display: none">Product Type</label>
                        <input type="text" style="display: none" class="form-control loginput" id="inputOtherProductType" title="Other Product Type" aria-describedby="emailHelp" onchange='$("#inputAttachment").focus()' />
                    </div>
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                        <label for="inputDescription">Description (Optional)</label>
                        <textarea class="form-control loginput" id="inputDescription" onchange='$("inputClientName").focus()'></textarea>
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputClientName">Client Name (Optional)</label>
                        <input type="text" class="form-control loginput" id="inputClientName" title="Client Name" aria-describedby="emailHelp" onchange='$("#inputEmail").focus()' />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputEmail">Email ID (Optional)</label>
                        <input type="email" class="form-control loginput" id="inputEmail" title="Email ID" aria-describedby="emailHelp" onchange='$("#inputMobileNumber").focus()' />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputMobileNumber">Mobile Number (Optional)</label>
                        <input type="text" class="form-control loginput" id="inputMobileNumber" title="Mobile Number" aria-describedby="emailHelp" onchange='$("#inputExpectedDeliveryDate").focus()' />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputExpectedDeliveryDate">Expected Delivery Date</label>
                        <input type="text" class="form-control loginput" id="inputExpectedDeliveryDate" title="Mobile Number" aria-describedby="emailHelp" onchange='$("#").focus()' />
                    </div>
                </div>

                <div class="form-group row mt-3">
                    <label id="ProjectDetailMasterID" style="display: none"></label>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" title="Assign Project">Assign</button>
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return ProjectUpdate();" id="UpdateButton" title="Update Project" style="display: none;">Update</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick="ResetAll();" title="Clear All">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    </div>
                </div>

            </form>
              <div class="loader" id="loader" style="display: none"></div>
            <div class="modal fade" id="modal_attachment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="H3">Attachment</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid alltrans">
                                <div class="table-responsive">
                                    <table id="Attachments" class="table table-bordered nowrap" cellspacing="0" width="100%">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>


    <script src="/JS/ProjectAssignScreen.js"></script>
    <script>
        var JSonObjects = [];

        $(document).ready(function () {
            $("#inputProjectName").focus()
        });

        $("#inputExpectedDeliveryDate").datepicker({
            //dateFormat: 'dd-mm-yy',
            dateFormat: 'd MM, yy',
            minDate: "0",
            changeMonth: true,
            changeYear: true
        });
        $(document).ready(function () {
            UserInformationSelectEmployeeName();
            ProductMasterSelect();
            ServiceMasterSelect();
            $("#inputProjectName").focus();
        });
        $(document).ready(function () {
            if (window.location.href.indexOf("ProjectDetailMasterID") > -1) {
                var url = window.location.href;
                var id = url.split('=')
              
              
               
                ProjectDetailMasterSelectByID(id[1]);
                $("#ProjectDetailMasterID").html(id[1]);
                $("#SubmitButton").css('display', 'none');
                $("#UpdateButton").show();
            }
        });

    </script>
</asp:Content>
