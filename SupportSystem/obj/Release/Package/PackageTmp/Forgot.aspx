﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Forgot.aspx.cs" Inherits="SupportSystem.Forgot" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css" href="/PublicSite/css/style.css" />



    <link href="/Styles/alert/css/alert.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/alert/themes/default/theme.css" rel="stylesheet" type="text/css" />

    <title>Welcome to Consisty Support System</title>
</head>
<body>
    <div class="container mt-5">
        <div class="row justify-content-md-center">
            <div class="col-12 text-center">
                <h3 class=" mb-3">Forgot Password?</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12 mx-auto">
                <div class="loginform">
                    <p class="text-center">
                        <img src="/PublicSite/images/logo.svg" width="250px" />
                    </p>
                    <form>
                        <div class="form-group">
                            <label for="inputMobileNumber">Registered Mobile Number</label>
                            <input type="text" class="form-control loginput" id="inputMobileNumber" aria-describedby="emailHelp" onkeypress="return isNumberKey(event)" maxlength="10" placeholder="Registered mobile number" autofocus="autofocus" />
                        </div>
                        <p class="text-center mb-0 mt-3">OR</p>
                        <div class="form-group">
                            <label for="inputEmail">Registered Email ID</label>
                            <input type="email" class="form-control loginput" id="inputEmail" aria-describedby="emailHelp" placeholder="Registered email ID" />
                        </div>

                        <div class="row mt-4">
                            <div class="col-12 text-center">
                                <p class="text-center mt-3">I've remembered my password <a href="Login.aspx">Sign In</a></p>
                                <button type="button" onclick="return UserInformationSelectForForgotPassword();" class="btn btn-primary text-uppercase logbutton">Submit</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

   <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="/Scripts/jquery-ui.js" type="text/javascript"></script>
<script src="/Styles/alert/js/alert.js" type="text/javascript"></script>
<script src="/Scripts/jquery-ui.min.js"></script>
<script src="/Styles/alert/js/alert.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>


    <script src="/Scripts/String.js" type="text/javascript"></script>

    <script src="/JS/Forgot.js"></script>
    <script>

        function isNumberKey(e) {
            var r = e.which ? e.which : event.keyCode;
            return r > 31 && (48 > r || r > 57) && (e.which != 46 || $(this).val().indexOf('.') != -1) ? !1 : void 0
        }
    </script>
</body>
</html>
