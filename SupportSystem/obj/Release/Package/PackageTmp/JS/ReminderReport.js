﻿function ReminderMasterSelect() {
    $("#loader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ReminderReport.aspx/Select",
        data: {},
        dataType: "JSON",
        success: function (data) {
            $("#loader").hide();
            BindDataReminderMaster(data);
        },
        error: function (result) {
            alert("Not Found");
        }
    });
}

function BindDataReminderMaster(data) {

    var mainData = [];

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];

        var color = 'black';
        if (data.d[i].Status == "deactivated")
            color = 'RED';
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', (i + 1), color));
        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].ReminderDate, color));

        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].CustomerName, color));

        strRow.push(String.format('<td> <span style="color:{1}" class="text-right"> {0} </span>  </td>', data.d[i].Amount, color));

        strRow.push(String.format('<td> <span style="color:{1}"> {0} </span>  </td>', data.d[i].TypeName, color));

        strRow.push(String.format('<td> <span style="color:{1}"><textarea rows="2" cols="40"> {0} </textarea> </span>  </td>', data.d[i].Description, color));
       
       
        strRow.push(String.format("<td > <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return ReminderMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].ReminderMasterID));
        strRow.push(String.format("<td > <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return ReminderMasterDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].ReminderMasterID));

        mainData.push(strRow);
    }
    $("#ReminderDetails").html("");
    $("#ReminderDetails").html("<thead> <tr> <th>#</th> <th>Date</th>  <th>Name</th>  <th>Amount</th>  <th>Type</th>   <th>Description</th>       <th>Edit</th> <th>Delete</th>  </tr> </thead>")
    $("#ReminderDetails").append("<tfoot> <tr> <th></th>   <th></th>     <th></th>      <th></th>       <th></th>          <th></th>                 <th></th>     <th></th>       </tr> </tfoot>")

    createDataTable('ReminderDetails', mainData, [3]);
}

function Validation() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();

    if (FromDate == "") {
        $.alert.open({
            content: "Select From date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: "Select To date", title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });
        return false;
    }

    return true;
}

function Search() {
    if (!Validation())
        return false;

    var FromDate = "";
    var ToDate = "";
    var Status = "";

    FromDate = $("#inputFromDate").val();
    ToDate = $("#inputToDate").val();
    Status = $("#inputStatus").val();

    var JsonObj =
    {
        FromDate: FromDate,
        ToDate: ToDate,
        Status: Status,
    };

    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ReminderReport.aspx/Search",
        data: JData,
        dataType: "json",

        success: function (data) {
            if (data.d.length < 1) {
                $.alert.open({
                    content: "Record Not found", icon: 'error', title: ClientName,
                    callback: function () {
                    }
                });
            }
            else {
                BindDataReminderMaster(data);
            }
        },

        error: function (data) {
        }
    });
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
        }
    });
}

function ReminderMasterSelectByID(ReminderMasterID) {
    location.href = "Reminder.aspx?ReminderMasterID=" + ReminderMasterID;
}

function ReminderMasterDelete(ReminderMasterID) {
    var JsonObj =
    {
        ReminderMasterID: ReminderMasterID,
    };

    var JData = JSON.stringify(JsonObj);

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ReminderReport.aspx/Delete",
                data: JData,
                dataType: "json",

                success: function (data) {
                    $.alert.open({
                        content: 'Data is Deleted', title: 'Consisty',
                    });
                },

                error: function (data) {
                    $.alert.open({
                        content: 'Data is not Deleted', title: 'Consisty',
                    });
                }
            });
            location.reload();
        }
    });
}