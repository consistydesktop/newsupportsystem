﻿function EmailValidation(EmailID) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(EmailID)) {
        return false;
    } else {
        return true;
    }
}

function CustomerRegistrationValidation() {
    var Name = $("#inputName").val();
    var Username = $("#inputUserName").val();
    var EmailID = $("#inputEmailID").val();
    var MobileNumber = $("#inputMobileNumber").val();

    var PANNo = $("#inputPANNo").val();
    var GSTNo = $("#inputGSTNo").val();

    var State = $("#inputState").val();


    if (Name == "") {
        $("#ErrorInputName").show();
        return false;
    }

    if (Username == "") {
        $("#ErrorInputUserName").show();
        return false;
    }

    if (EmailID == "") {

        $("#ErrorInputEmailID").show();

        return false;
    }

    if (MobileNumber == "") {

        $("#ErrorInputMobileNumber").show();
        return false;
    }

    if (MobileNumber.length != 10) {
        $("#ErrorInputMobileNumber").html("Please enter valid mobile number");
        $("#ErrorInputMobileNumber").show();

        return false;
    }


    if (PANNo == "" && GSTNo == "") {
        $.alert.open({
            content: "Please enter PAN no or GST no ", icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputPANNo").focus();
            }
        });
        return false;
    }
    if (State == "0") {

        $("#ErrorInputState").show();
        return false;
    }



    return true;
}

function ShowRespose(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
           
        }
    });
}

function isPANKey(e, controlName) {
    var r = e.which ? e.which : event.keyCode;
    var txtLength = $("#" + controlName).val().length;

    var txt = document.getElementById(controlName);
    var selectedTextLen = txt.value.substr(txt.selectionStart, (txt.selectionEnd - txt.selectionStart)).length;
    txtLength = txtLength - selectedTextLen;

    if (txtLength < 5 || txtLength > 8) {
        return (64 > r || r > 91) && (96 > r || r > 122) && (e.which != 8) ? !1 : void 0
    }
    else {
        return r > 31 && (48 > r || r > 57) && (e.which != 46) && (e.which != 8) ? !1 : void 0
    }
}

function isGSTKey(e, controlName) {
    var r = e.which ? e.which : event.keyCode;
    //console.log("length:" + $("#" + controlName).val().length);

    var txt = document.getElementById(controlName);
    var selectedTextLen = txt.value.substr(txt.selectionStart, (txt.selectionEnd - txt.selectionStart)).length;
    var contentLength = $("#" + controlName).val().length;

    contentLength = contentLength - selectedTextLen;

    if (contentLength <= 1) {
        //number
        return r > 31 && (48 > r || r > 57) && (e.which != 46) && (e.which != 8) ? !1 : void 0
    }
    else if (contentLength >= 2 && contentLength <= 6) {
        //alphanumberic
        return (64 > r || r > 91) && (96 > r || r > 122) && (e.which != 8) ? !1 : void 0
    }
    else if (contentLength >= 7 && contentLength <= 10) {
        return r > 31 && (48 > r || r > 57) && (e.which != 46) && (e.which != 8) ? !1 : void 0
    }
    else if (contentLength == 11) {
        return (64 > r || r > 91) && (96 > r || r > 122) && (e.which != 8) ? !1 : void 0
    }
    else if (contentLength == 12) {
        return r > 31 && (48 > r || r > 57) && (e.which != 46) && (e.which != 8) ? !1 : void 0
    }
    else if (contentLength == 13) {
        return (64 > r || r > 91) && (96 > r || r > 122) && (e.which != 8) ? !1 : void 0
    }
    else if (contentLength == 14) {
        return (48 > r || r > 57) && (64 > r || r > 91) && (96 > r || r > 122) && (e.which != 8) ? !1 : void 0
    }
    else {
        return (64 > r || r > 91) && (96 > r || r > 122) && (e.which != 8) ? !1 : void 0
    }
}

function CustomerRegistrationInsert() {
    var Name = $("#inputName").val();
    var Username = $("#inputUserName").val();
    var EmailID = $("#inputEmailID").val();
    var MobileNumber = $("#inputMobileNumber").val();

    if (CustomerRegistrationValidation() == false) {
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Are you want to submit',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'Yes'.toUpperCase()) {
                return false;
            }
            var jsonObj = {
                Name: Name,
                Username: Username,
                EmailID: EmailID,
                MobileNumber: MobileNumber,
            };
            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "CustomerRegistration.aspx/Insert",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowRespose(data);
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing Worng"), icon: 'error', title: ClientName,
                    });
                }
            });
            return false;
        }
    });
}

function ResetAll() {
    $("#inputName").val("");
    $("#inputUserName").val("");
    $("#inputEmailID").val("");
    $("#inputMobileNumber").val("");
    $("#userID").html("");


    $("#inputPANNo").val("");

    $("#inputGSTNo").val("");

    $("#inputState").val(0).trigger('chosen:updated');
    $("#inputName").focus();
}

function UserInformationSelectByID() {


    var url = window.location.href;
    var id = url.split('=')
    $("#UserID").html(id[1]);




    var ID = id[1]
    $("#inputName").val("Please Wait...");
    $("#inputUserName").val("Please Wait...");
    $("#inputEmailID").val("Please Wait...");
    $("#inputMobileNumber").val("Please Wait...");

    $("#UserID").html(ID);
    var jsonObj = {
        UserID: ID,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "CustomerRegistration.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#inputName").val(data.d.Name);
            $("#inputUserName").val(data.d.Username);
            $("#inputEmailID").val(data.d.EmailID);
            $("#inputMobileNumber").val(data.d.MobileNumber);
            $("#inputURL").val(data.d.URL);
            $("#UserID").html(ID);

            $("#inputPANNo").val(data.d.PANNo);

            $("#inputGSTNo").val(data.d.GSTNo);

            $("#inputState").val(data.d.State).trigger('chosen:updated');
            if (data.d.State == "Not Updated")
                $("#inputState").val(0).trigger('chosen:updated');




            $("#SubmitButton").css('display', 'none');
            $("#UpdateButton").show();
        },
        error: function (result) {
            $.alert.open({ content: "Not updated", icon: 'error', title: ClientName, });
        }
    });
}

function UserInformationUpdate() {
    var Name = $("#inputName").val();
    var Username = $("#inputUserName").val();
    var EmailID = $("#inputEmailID").val();
    var MobileNumber = $("#inputMobileNumber").val();

    var UserID = $("#UserID").html();
    if (window.location.href.indexOf("CustomerRegID") > -1) {
        var url = window.location.href;
        var id = url.split('=')
        UserID = id[1];
    }

    var PANNo = $("#inputPANNo").val();
    var GSTNo = $("#inputGSTNo").val();
    var State = $("#inputState").val()

    if (CustomerRegistrationValidation() == false) {
        return false;
    }



    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to update',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                Name: Name,
                Username: Username,
                EmailID: EmailID,
                MobileNumber: MobileNumber,
                UserID: UserID,
                PANNo: PANNo,
                GSTNo: GSTNo,
                State: State
            };
            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "CustomerRegistration.aspx/Update",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                    UserInformationSelectByID();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not updated'), icon: 'error', title: ClientName, });
        }
    });
}

function Redirect() {
    window.location.href = "Login.aspx";
}