﻿function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "CustomerCallScreen.aspx/SelectSystemName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataSystemMaster(data);
            $("#ddlSystemName").focus();
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSystemMaster(data) {
    $("#inputSystemName").html("");
    $("#inputSystemName").append("<option value=0>Select System </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#inputSystemName").append(strRowDropdown);
    }
    $("#inputSystemName").chosen();
}

function UserInformationSelectByEmployeeID() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "CustomerCallScreen.aspx/SelectEmployeeName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataSelectEmployeeName(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSelectEmployeeName(data) {
    $("#inputEmployeeName").html("");
    $("#inputEmployeeName").append("<option value=0>Select Developer</option> ");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
        $("#inputEmployeeName").append(strRowDropdown);
    }
    $("#inputEmployeeName").chosen();
}

function CallIssueMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "CustomerCallScreen.aspx/SelectIssue",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataCallIssueMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataCallIssueMaster(data) {
    $("#inputIssueType").html("");
    $("#inputIssueType").append("<option value=0>Select Issue</option> ");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].CallIssueID, data.d[i].IssueName));
        $("#inputIssueType").append(strRowDropdown);
    }
    $("#inputIssueType").chosen();
}

function CallCustomerValidation() {
    var SystemID = $("#inputSystemName").val();
    var MobileNumber = $("#inputMobileNumber").val();
    var Description = $("#inputDescription").val();
    if (SystemID == '0') {
        $.alert.open({
            content: String.format("Please select system  name"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });

        return false;
    }

    if (MobileNumber == "") {
        $.alert.open({
            content: String.format("Please enter mobile number"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputMobileNumber").focus();
            }
        });
        return false;
    }
    if (MobileNumber.length != 10) {
        $.alert.open({
            content: String.format("Please enter valid mobile number"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputMobileNumber").focus();
            }
        });
        return false;
    }

    if (Description == "") {
        $.alert.open({
            content: String.format("Please enter description"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputDescription").focus();
            }
        });
        return false;
    }
}

function CallMasterInsert() {
    if (CallCustomerValidation() == false) {
        return false;
    }

    var SystemID = $("#inputSystemName").val();
    var UserID = $("#inputEmployeeName").val();
    var CallIssueID = $("#inputIssueType").val();
    var MobileNumber = $("#inputMobileNumber").val();
    var Description = $("#inputDescription").val();
    var TeamAny = $("#inputAnyDeskID").val();


    var jsonObj = {
        SystemID: SystemID,
        UserID: UserID,
        CallIssueID: CallIssueID,
        MobileNumber: MobileNumber,
        Description: Description,
        TeamAny: TeamAny
    };

    var jData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "CustomerCallScreen.aspx/Insert",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            ShowRespose(data);
            location.reload();
        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });

    return false;
}

function BindDataCallMaster(data) {
    if (data.d.length > 0) {
        $("#CallDetails").html("");
        var mainData = [];
        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

          //  strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));

            strRow.push(String.format('<td> {0} </td>', data.d[i].DOCInString));
            strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
            strRow.push(String.format('<td> {0} </td>', data.d[i].Name));
            strRow.push(String.format('<td> {0} </td>', data.d[i].IssueName));
            strRow.push(String.format('<td> {0} </td>', data.d[i].MobileNumber));


            if (data.d[i].Description == "") {
                strRow.push(String.format('<td> {0} </td>', ""));
            } else {
                strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Description' style='width:50%; background-color: white; border-color: #fff'' onclick=\"ViewDescription('{0}')\" ><i class='fa fa-file-text' style='font-size:20px;color:blue'></i></button> </td>", (data.d[i].Description).replace(/\n/ig, '</br>')));
            }
            strRow.push(String.format("<td class='text-center'> <button type='button'class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff'' onclick='return CallMasterSelectByID({0})' ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].CallID));
            strRow.push(String.format("<td class='text-center'> <button type='button'class='btn'  title='Delete' style='width:50% background-color: white; border-color: #fff'' onclick='return CallMasterDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].CallID));


            mainData.push(strRow);
        }
        // strRow.push('</tr>');
        //   $("#CallDetails").append(strRow.join(""));
        $("#CallDetails").html("");
        $("#CallDetails").html("<thead><tr>    <th>#</th>  <th>Date</th>  <th>SystemName</th> <th>Developer</th> <th>Issue</th>       <th>Mobile</th>  <th>Description</th>     <th>Edit</th>        <th>Delete</th>       </tr></thead>");
        $("#CallDetails").append("<tfoot> <tr>  <th></th>   <th></th>       <th></th>             <th></th>         <th></th>              <th></th>          <th></th>            <th></th>              <th></th>          </tr> </tfoot>")
        createDataTable('CallDetails', mainData, []);

    }

}

function ViewDescription(Description) {
    $("#modal_description").modal('show');
    $("#Description").html(Description);
}

function CallMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "CustomerCallScreen.aspx/Select",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindDataCallMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function CallMasterSelectByID(CallID) {
    var CallID = CallID;
    $("#ddlSystemName ").val("Please Wait...").trigger('chosen:updated');
    $("#ddlDeveloperName ").val("Please Wait...").trigger('chosen:updated');
    $("#ddlIssueType ").val("Please Wait...").trigger('chosen:updated');
    $("#txtMobileNumber").val("Please Wait...");
    $("#txtEditor").val("Please Wait...");

    var jsonObj = {
        CallID: CallID,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "CustomerCallScreen.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#inputSystemName ").val(data.d.SystemID).trigger('chosen:updated');
            $("#inputEmployeeName ").val(data.d.UserID).trigger('chosen:updated');
            $("#inputIssueType ").val(data.d.CallIssueID).trigger('chosen:updated');
            $("#inputMobileNumber").val(data.d.MobileNumber);
            $("#inputDescription").val(data.d.Description);
            $("#inputAnyDeskID").val(data.d.TeamAny);
            $("#CallID").html(CallID);
            $("#SubmitButton").css('display', 'none');
            $("#UpdateButton").show();
            $("#inputSystemName ").focus();
        },
        error: function (result) {
            $.alert.open({ content: "not Updated", icon: 'error', title: ClientName, });
        }
    });
}

function CallMasterUpdate() {
    var SystemID = $("#inputSystemName").val();
    var UserID = $("#inputEmployeeName").val();
    var CallIssueID = $("#inputIssueType").val();
    var MobileNumber = $("#inputMobileNumber").val();
    var Description = $("#inputDescription").val();
    var CallID = $("#CallID").text();
    var TeamAny = $("#inputAnyDeskID").val();

    if (CallCustomerValidation() == false) {
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Update',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                CallID: CallID,
                SystemID: SystemID,
                UserID: UserID,
                CallIssueID: CallIssueID,
                MobileNumber: MobileNumber,
                Description: Description,
                TeamAny: TeamAny,
            };
            var jData = JSON.stringify(jsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "CustomerCallScreen.aspx/Update",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not inserted'), icon: 'error', title: ClientName, });
        }
    });
}

function CallMasterDelete(CallID) {
    var CallID = CallID;

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to Delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                CallID: CallID,
            };

            var jData = JSON.stringify(jsonObj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "CustomerCallScreen.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('not updated'), icon: 'error', title: ClientName, });
        }
    });
}

function ResetAll() {
    $("#inputSystemName").val(0).trigger('chosen:updated');
    $("#inputEmployeeName").val(0).trigger('chosen:updated');
    $("#inputIssueType").val(0).trigger('chosen:updated');
    $("#inputMobileNumber").val("");
    $("#inputDescription").val("");
    $("#CallID").text("");
    $("#inputAnyDeskID").val("");
}

function ShowRespose(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            CallMasterSelect();
            ResetAll();
        }
    });
}