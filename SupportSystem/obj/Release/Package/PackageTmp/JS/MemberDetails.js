﻿function CustomerRegistrationSelectCustomerName() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "MemberDetails.aspx/SelectCustomerName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            $("#MemberDetails").html("");
            $("#ddlUser").append("<option value='{0}'> -- All Customer -- </option> ");
            for (var i = 0; i < data.d.length; i++) {
                var strRowDropdown = [];
                strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].RegistrationID, data.d[i].CustomerName));
                $("#ddlUser").append(strRowDropdown);
            }
            $("#ddlUser").chosen();
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function CustomerRegistrationSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "MemberDetails.aspx/Select",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataCustomerRegistration(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName });
        }
    });
}

function BindDataCustomerRegistration(data) {
    $("#MemberDetails").html("");
    $("#MemberDetails").html("<thead><tr><th>#</th><th>Customer Name</th><th>User Name</th><th>GST NO</th><th>PAN NO</th><th>Date </th><th>View</th><th>Approve</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';
        var checked = 'checked';
        if (data.d[i].IsApproved != 1) {
            color = "red";
            checked = '';
        }

        strRow.push(String.format('<tr style="color:{0}; ">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td>{0}</td>', data.d[i].CustomerName));
        strRow.push(String.format('<td>{0}</td>', data.d[i].UserName));

        strRow.push(String.format('<td> {0} </td>', data.d[i].GSTNo));
        strRow.push(String.format('<td> {0} </td>', data.d[i].PANNo));
        strRow.push(String.format('<td>{0}</td>', data.d[i].DateAndTime));
      //  strRow.push(String.format('<td> <input type="button" value="View" class="btn" title="View" style="width:50%; background-color: white; border-color: #fff" id="ViewBtn{0}"  onclick=\'CustomerRegistrationSelectByID({1})\' {3}/> </td>', i, data.d[i].RegistrationID));
        strRow.push(String.format("<td class='text-center'> <span  class='green' title='View'  onclick='return CustomerRegistrationSelectByID(({1}))' data-toggle='modal' data-target='#'>View</span> </td>", i, data.d[i].RegistrationID));
      
        if (data.d[i].IsApproved == 1) {
            data.d[i].IsApproved.disabled = false;
            strRow.push(String.format('<td> <input type="checkbox" name="{0}" id="checks{0}"  onchange=\'UserInformationInsertForApprovedCustomer({0},{1},{2})\' {3} CHECKED DISABLED/> </td>', i, data.d[i].RegistrationID, data.d[i].MobileNumber, checked));
        }
        else {
            strRow.push(String.format('<td> <input type="checkbox" name="{0}" id="checks{0}"  onchange=\'UserInformationInsertForApprovedCustomer({0},{1},{2})\' {3}/> </td>', i, data.d[i].RegistrationID, data.d[i].MobileNumber, checked));
        }
        strRow.push('</tr>');
        $("#MemberDetails").append(strRow.join(""));
    }
    addProperties(MemberDetails);
}

function UserInformationInsertForApprovedCustomer(i, RegistrationID, MobileNumber) {
    var IsAprroved = 0;

    if ($("#checks" + i).is(":checked")) {
        IsAprroved = 1;
    }
    else {
        IsAprroved = 0;
    }
    var jsonObj = {
        RegistrationID: RegistrationID,
        IsAprroved: IsAprroved,
        MobileNumber: MobileNumber
    };
    var jsonData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "MemberDetails.aspx/AcceptRejectCustomer",
        data: jsonData,
        dataType: "json",
        success: function (data) {
            ShowRespose(data);
            CustomerRegistrationSelect();
        },
        error: function (data) {
            $.alert.open({ content: String.format('not update'), icon: 'error', title: ClientName });
        }
    });
}

function ShowRespose(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
    });
}

function CustomerRegistrationSelectByID(RegistrationID) {

    var jsonObj = {
        RegistrationID: RegistrationID,
    };
    var jData = JSON.stringify(jsonObj)


    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "MemberDetails.aspx/SelectByRegistrationID",
        data: jData,
        dataType: "json",
        success: function (data) {
            BindDataCustomerRegistrationReport(data);
          
        }
    });


}

function BindDataCustomerRegistrationReport(data) {

    $("#LabelCustomerName").html(data.d.CustomerName);

    $("#LabelUserName").html(data.d.UserName);
    $("#LabelGSTNo").html(data.d.GSTNo);
    $("#LabelPANNo").html(data.d.PANNo);
    $("#LabelAppliedDate").html(data.d.DateAndTime);
    $("#LabelEmailID").html(data.d.EmailID);
    $("#LabelMobileNo").html(data.d.MobileNumber);


    $("#modal_CustomerInformation").modal('show');
}