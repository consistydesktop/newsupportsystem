﻿function UserInformationSelectEmployeeName() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "AddTicketAdmin.aspx/SelectEmployeeName",
        data: {},
        dataType: "json",

        success: function (data) {
            $("#inputEmployeeName").html("");
            $("#inputEmployeeName").append("<option value=0>Select Employee</option> ");
            for (var i = 0; i < data.d.length; i++) {
                var strRowDropdown = [];
                strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
                $("#inputEmployeeName").append(strRowDropdown);
            }
            $("#inputEmployeeName").chosen();
        }
    });
}

function readURLTicketImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageTicket')
                .attr('src', e.target.result)
                .width(250)
                .height(250);
        };

        reader.readAsDataURL(input.files[0]);
    }

}
function ServiceMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AddTicketAdmin.aspx/SelectServiceName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataServiceMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataServiceMaster(data) {
    $("#inputService").html("");
    $("#inputService").append("<option value='0'>Select Service </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].ServiceID, data.d[i].ServiceName));
        $("#inputService").append(strRowDropdown);
    }
    $("#inputService").chosen();
    $("#inputService").focus();
}

function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AddTicketAdmin.aspx/SelectSystemName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataSystemMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data Not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSystemMaster(data) {
    $("#inputSystemName").html("");
    $("#inputSystemName").append("<option value='0'>Select System </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#inputSystemName").append(strRowDropdown);
    }
    $("#inputSystemName").chosen();
}

function AddTicketValidation() {
    var SystemID = $("#inputSystemName").val();
    var DeveloperID = $("#inputEmployeeName").val();
    var ServiceID = $("#inputService").val();
    var RequestType = $("#inputRequestType").val();

    var Priority = $("#inputPriority").val();

    if (ServiceID == '0') {
        $.alert.open({
            content: String.format("Please select service"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputService").focus();
            }
        });
        return false;
    }

    if (DeveloperID == 0) {
        $.alert.open({
            content: String.format("Please select Developer"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputEmployeeName").focus();
            }
        });
        return false;
    }
    if (SystemID == '0') {
        $.alert.open({
            content: String.format("Please select system"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });
        return false;
    }
    if (RequestType == '0') {
        $.alert.open({
            content: String.format("Please select request type"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputRequestType").focus();
            }
        });
        return false;
    }

    if (Priority == '0') {
        $.alert.open({
            content: String.format("Please select priority"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputPriority").focus();
            }
        });

        return false;
    }
    return true;
}

$("#SubmitButton").click(function (evt) {
    if (AddTicketValidation() == false) {
        return false;
    }
    var File = $("#inputAttachment").val();
    if (File == "") {
        var result = 0;
        TicketInformationInsertTicket(result);
    }

    else {
        var data = new FormData();
        var fileUpload = $("#inputAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForTicket.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {

                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else {
                    TicketInformationInsertTicket(result);
                }
            },

            error: function (err) {
                alert('Error:' + err)
            }
        });
    }

    evt.preventDefault();
});
function ShowResponseFile(result) {

    $.alert.open({
        content: result, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}
function TicketInformationInsertTicket(result) {
    if (AddTicketValidation() == false) {
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Do you want to add ticket',
        icon: 'confirm',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var SystemName = $("#inputSystemName option:selected").text();
            var DeveloperID = $("#inputEmployeeName").val();
            var SystemID = $("#inputSystemName").val();
            var ServiceID = $("#inputService").val();
            var RequestType = $("#inputRequestType").val();
            var Description = $.trim($("#inputDescription").val());
            var TeamAny = $("#inputAnyDeskID ").val();
            var FileIDs = result;
            var Priority = $("#inputPriority").val();
            var TicketGenerationDate = $("#TicketGenerationDate").val();

            var jsonObj = {
                SystemName: SystemName,
                DeveloperID: DeveloperID,
                SystemID: SystemID,
                ServiceID: ServiceID,
                Description: Description,
                RequestType: RequestType,
                TeamAny: TeamAny,
                FileIDs: FileIDs,
                Priority: Priority,
                TicketGenerationDate: TicketGenerationDate
            };

            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AddTicketAdmin.aspx/Insert",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowResponse(data);
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing worng"), icon: 'error', title: ClientName,
                    });
                }
            });
        }
    });
}

function Reset() {
    $("#inputSystemName").val(0).trigger('chosen:updated');;
    $("#inputService").val(0).trigger('chosen:updated');;
    $("#inputAnyDeskID").val("");
    $("#inputRequestType").val(0).trigger('chosen:updated');
    $("#inputEmployeeName").val(0).trigger('chosen:updated');

    $("#inputDescription").val("");
    $("#inputAttachment").val("");
    $("#inputPriority").val("");
    $("#TicketGenerationDate").val("");

    $("#inputService").focus();
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}