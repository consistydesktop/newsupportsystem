﻿function TimeSheetMasterSelectForReport() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "TimeSheetReport.aspx/Select",
        data: {},
        dataType: "json",

        success: function (data) {
            BindDataTimeSheetMaster(data);
        }
    });
}

function BindDataTimeSheetMaster(data) {
    var mainData = [];
    $("#TimeSheetDetails").html("<thead><tr><th>Sr.No</th><th>Date</th><th>Total Time</th><th>View</th></tr></thead><tbody></tbody>")
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';
        var TotalTime = Math.floor(data.d[i].TotalTime / 60);
        if (TotalTime < 8)
            color = 'red';


        strRow.push(String.format('<td class="Grcenter"> {0} </td>', (i + 1)));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Date));
        strRow.push(String.format('<td><b> {0}:{1} Hours</b></td>', Math.floor(data.d[i].TotalTime / 60), (data.d[i].TotalTime % 60)));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick='return TimeSheetMasterSelectByIDForReport(\"{0}\")' data-toggle='modal' data-target='#exampleModalCenter'  ><i class='fa fa-eye' style='font-size:20px;color:blue'></i></button> </td>", (data.d[i].Date)));


        mainData.push(strRow);
    }
    // addProperties(TimeSheetDetails);

    $("#TimeSheetDetails").html("");
    $("#TimeSheetDetails").html("<thead> <tr> <th>Sr.No</th> <th>Date</th> <th>Total Time</th> <th>View</th></tr></thead>")
    $("#TimeSheetDetails").append("<tfoot> <tr> <th></th>   <th></th>           <th></th>         <th></th> </tr> </tfoot>")
    createDataTable('TimeSheetDetails', mainData, []);
}

function Validation() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();

    if (FromDate == "") {
        $.alert.open({
            content: "Select From date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: "Select To date", title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });
        return false;
    }
    return true;
}

function Search() {
    if (!Validation())
        return false;

    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();

    var JsonObj =
    {
        FromDate: FromDate,
        ToDate: ToDate,
    };

    var JData = JSON.stringify(JsonObj);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "TimeSheetReport.aspx/Search",
        data: JData,
        dataType: "json",
        success: function (data) {
            if (data.d.length < 1) {
                $.alert.open({
                    content: "Record Not found", icon: 'error', title: ClientName,
                    callback: function () {
                    }
                });
            }
            else {
                BindDataTimeSheetMaster(data);
            }
        },
        error: function (data) {
        }
    });
}

function TimeSheetMasterSelectByIDForReport(Date) {
    var jsonObj =
        {
            Date: Date,
        };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "TimeSheetReport.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            BindDataTimeSheetMasterReport(data);
            $("#modal_TimeSheetReport").modal('show');
        },
        error: function (result) {
            $.alert.open({ content: "not Updated", icon: 'error', title: ClientName, });
        }
    });
}




function BindDataTimeSheetMasterReport(data) {

    var totalTime = 0;
    var strRow;
    $("#TimeSheetReport").html("");
    $("#TimeSheetReport").html("<thead><tr><th>System Name</th><th>Task</th><th> Time(In minutes)</th></tr></thead><tbody></tbody>");
    for (var i = 0; i < data.d.length; i++) {
        strRow = [];
        var color = 'black';
        strRow.push(String.format("<tr style='color:{0};'>", color));
        strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
        strRow.push(String.format('<td> {0} </td>', data.d[i].Task));
        strRow.push(String.format('<td> {0} minutes</td>', data.d[i].Time));

        strRow.push('</tr>');

        $("#TimeSheetReport").append(strRow.join(""));
        var Time = data.d[i].Time;
        totalTime = totalTime + Time;

    }
    strRow = [];
    strRow.push(String.format('<tr> <td></td> <td></td> <td>{0}:{1} Hours </td> </tr>', Math.floor(totalTime / 60), (totalTime % 60)));
    $("#TimeSheetReport").append(strRow.join(""));
   

    // addProperties(TimeSheetReport);
}


function DownloadReport() {
}