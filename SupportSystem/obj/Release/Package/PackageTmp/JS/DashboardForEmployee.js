﻿/****************** Project Details****************************/
function ProjectDetailMasterSelectForEmployee() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectProjectDetails",
        data: "{}",
        dataType: "JSON",
        success: function (data) {

            BindDataProjectDetailMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataProjectDetailMaster(data) {

    if (data.d.length > 0) {
        $("#ProjectDetails").html("");
        $("#ProjectDetails").html("<thead><tr><th>#</th><th>Project Name</th></tr></thead><tbody></tbody>");

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));

            strRow.push(String.format('<td> <a href="#" onclick="ViewProjectDetails({0})" >{1}</a> </td>', data.d[i].ProjectDetailMasterID, data.d[i].ProjectName));

            strRow.push('</tr>');
            $("#ProjectDetails").append(strRow.join(""));
        }
        addProperties(ProjectDetails);
    }

}


function ViewProjectDetails(ProjectDetailMasterID) {
    location.href = "ViewProjectDetailsForEmployee.aspx?ProjectDetailMasterID=" + ProjectDetailMasterID;
}
/**************************************************************/


/* ***************** Ticket Details ***************************/
function TicketInformationSelectForEmployee() {


    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectTicketDetails",
        data: "{}",
        dataType: "JSON",
        success: function (data) {

            BindDataTicketInformation(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataTicketInformation(data) {
    if (data.d.length > 0) {
        $("#TicketDetails").html("");
        $("#TicketDetails").html("<thead><tr><th>#</th><th>System</th><th>Pending Issues</th><th>Resolved Issues</th></tr></thead><tbody></tbody>");

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));

            strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
            strRow.push(String.format('<td> <a href="#" onclick="DashPending({0})" >{1}</a> </td>', data.d[i].SystemID, data.d[i].PendingTickets));
            strRow.push(String.format('<td> <a href="#" onclick="DashResolved({0})" >{1}</a> </td>', data.d[i].SystemID, data.d[i].ResolvedTickets));

            strRow.push('</tr>');
            $("#TicketDetails").append(strRow.join(""));
        }
        addProperties(TicketDetails);
    }
}

/**************************************************************/

/******************* Call Details *****************************/

function CallMasterSelectForEmployee() {

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectCallDetails",
        data: "{}",
        dataType: "JSON",
        success: function (data) {

            BindDataCallMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataCallMaster(data) {
    if (data.d.length > 0) {
        $("#CallDetails").html("");
        $("#CallDetails").html("<thead><tr><th>#</th><th>Date</th><th>System</th><th>Description</th><th>Issue Type</th><th>Mobile No</th></tr></thead><tbody></tbody>");

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));

            strRow.push(String.format('<td> {0} </td>', data.d[i].DOCInString));
            strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));

            if (data.d[i].Description == "") {
                strRow.push(String.format('<td> {0} </td>', " "));
            }
            else {
                strRow.push(String.format('<td> {0} </td>', data.d[i].Description));
            }

            if (data.d[i].IssueName == "") {
                strRow.push(String.format('<td> {0} </td>', " "));
            } else {
                strRow.push(String.format('<td> {0} </td>', data.d[i].IssueName));
            }
            if (data.d[i].MobileNumber == "") {
                strRow.push(String.format('<td> {0} </td>', " "));
            }
            else {
                strRow.push(String.format('<td> {0} </td>', data.d[i].MobileNumber));
            }
            strRow.push('</tr>');
            $("#CallDetails").append(strRow.join(""));
        }
        addProperties(CallDetails);
    }
}
/**************************************************************/

/******************* Task Details ****************************/
function TaskMasterSelectForEmployee() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectTaskDetails",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataTaskMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataTaskMaster(data) {

    if (data.d.length > 0) {
        $("#TaskDetails").html("");
        $("#TaskDetails").html("<thead><tr><th>#</th><th>System</th><th>Task</th><th>Due date</th><th>Description</th><th>Status</th><th>Attachment</th></tr></thead><tbody></tbody>");

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            if (data.d[i].Status == 'INCOMPLETE') {
                color = 'red';
            }



            if (data.d[i].Status == 'COMPLETE') {
                color = 'green';
            }

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));
            strRow.push(String.format('<td> {0} </td>', data.d[i].SystemName));
            strRow.push(String.format('<td> {0} </td>', data.d[i].Task));
            strRow.push(String.format('<td> {0} </td>', data.d[i].Deadline));
            if (data.d[i].Description != "") {
                strRow.push(String.format("<td class='text-center'> <button type='button' class='btn'  title='Description' style='width:50%; background-color: white; border-color: #fff' onclick=\"SelectDiscription('{0}')\" ><i class='fa fa-file-text' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TaskID));
                //  strRow.push(String.format("<td class='text-center'> <button type='button' class='btn'  title='Description' style='width:50%; background-color: white; border-color: #fff' onclick=\"ViewDescription('{0}')\" ><i class='fa fa-file-text' style='font-size:20px;color:blue'></i></button> </td>", (data.d[i].Description).replace(/[^a-zA-Z ]/g, "")));
                //var val = new String((data.d[i].Description));
                //val = (val.replace(/"/g, '\\"'));
                //strRow.push(String.format("<td class='text-center'> <button type='button' class='btn'  title='Description' style='width:50%; background-color: white; border-color: #fff' onclick=\"ViewDescription('{0}')\" ><i class='fa fa-file-text' style='font-size:20px;color:blue'></i></button> </td>", val));
            }
            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Status' style='width:50%; background-color: white; border-color: #fff' onclick=' TaskMasterUpdate({0})' ><i class='fa fa-pencil' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TaskID));

            strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='download' style='width:50%; background-color: white; border-color: #fff' onclick='return DownloadAttachment({0})' ><i class='fa fa-download' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TaskID));

            strRow.push('</tr>');
            $("#TaskDetails").append(strRow.join(""));
        }
    }

    addProperties(TaskDetails);
}

function ViewDescription(Description) {
    $("#modal_Sender").modal('show');
    $("#txtDescription").html(Description);
}

function TaskMasterUpdate(TaskID) {
    $("#TaskID").html(TaskID);
    $("#modal_status").modal('show');
}

function TaskMasterMasterUpdateStatus() {
    var Status = $("#ddlStatus").val();
    var File = $("#inputTaskAttachment").val();
    if (Status != "0") {
        Status = Status;
    }
    else {
        if (Status == "0") {
            $.alert.open({
                content: String.format("Please select status"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#modal_status").modal('show');
                    $("#ddlStatus").focus();
                }
            });

            return false;
        }
    }

    if (File == 0) {
        TaskMasterUpdateForStatus(0);
    }
    else {
        var data = new FormData();
        var fileUpload = $("#inputTaskAttachment").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForTask.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {

                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else {

                    TaskMasterUpdateForStatus(result);
                }


            },
            error: function (err) {
                alert('Error:' + err)
            }
        });
    }
    Reset();
}

function TaskMasterUpdateForStatus(result) {
    var Status = $("#ddlStatus").val();
    var TaskID = $("#TaskID").text();
    var FileIDs = result;
    var jsonObj = {
        Status: Status,
        TaskID: TaskID,
        FileIDs: FileIDs,
    };
    var jData = JSON.stringify(jsonObj)

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to update status ',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "DashboardForEmployee.aspx/UpdateStatus",
                data: jData,
                dataType: "json",
                success: function (data) {
                    location.reload();
                    TaskMasterSelectForEmployee();
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not inserted'), icon: 'error', title: ClientName, });
        }
    });
}

function DownloadAttachment(TaskID) {
    var jsonObj = {
        TaskID: TaskID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "DashboardForEmployee.aspx/SelectByTaskID",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            if (data.d.length <= 0) {


                $.alert.open({
                    content: String.format("No file attached"), icon: 'warning', title: ClientName,
                    callback: function () {

                    }
                });
                return false;
            }

            BindDataAttachment(data);
            $("#modal_attachment").modal('show');

        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function BindDataAttachment(data) {
    if (data.d.length > 0) {

        $("#Attachments").html("");
        $("#Attachments").html("<thead><tr><th>#</th><th>Attachment</th></tr></thead><tbody></tbody>");

        for (var i = 0; i < data.d.length; i++) {
            var strRow = [];
            var color = 'black';

            strRow.push(String.format('<tr style="color:{0};">', color));
            strRow.push(String.format('<td> {0} </td>', (i + 1)));
            strRow.push(String.format("<td class='text-center'> <a href='{0}'target='_blank'>{1}</a> </td>", data.d[i].Attachment, data.d[i].OriginalFileName));
            strRow.push('</tr>');
            $("#Attachments").append(strRow.join(""));
        }
    }
}

function SelectDiscription(TaskID) {
    var jsonObj = {
        TaskID: TaskID,
    };
    var jData = JSON.stringify(jsonObj);

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForEmployee.aspx/SelectDescriptionByTaskID",
        data: jData,
        dataType: "JSON",
        success: function (data) {
            $("#modal_Sender").modal('show');
            $("#txtDescription").html(data.d);

        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}


/**************************************************************/