﻿function UserInformationSelectForCustomer() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForCustomer.aspx/Select",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataForCustomer(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataForCustomer(data) {
    $("#DashboardForCustomer").html("");
    $("#DashboardForCustomer").html("<thead><tr><th>Sr.No</th><th>System Name</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td> <a href="#" onclick="TicketReportForCustomer({0})" >{1}</a> </td>', data.d[i].SystemID, data.d[i].SystemName));
        strRow.push('</tr>');
        $("#DashboardForCustomer").append(strRow.join(""));
    }

    $('#DashboardForCustomer').dataTable({
        "scrollX": true,
        "bDestroy": true,
        dom: 'Blfrtip',
        buttons: ['excelHtml5', 'pdfHtml5'
        ]
    });
}

function TicketReportForCustomer(SystemID) {
    location.href = "TicketReportForCustomer.aspx?SystemID=" + SystemID;
}

function SelectTicketCount() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "DashboardForCustomer.aspx/SelectTicketCount",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            $("#TotalTickets").html(data.d);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}