﻿function ShowRespose(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            UserInformationSelectForAll();
        }
    });
}

function BindData(data) {
    if (data.d.length > 0) {
        if (data.d[0].UserTypeID == 3) {
            $("#UserInformationDetails").hide();
            $("#ClientInformation").html("");
            $("#ClientInformation").html("<thead><tr><th>#</th><th>Name</th><th>Mobile No</th><th>GST</th><th>PAN</th><th>State</th><th>Edit</th><th>IsActive</th><th>Delete</th><th>Token</th></thead><tbody></tbody>");




            for (var i = 0; i < data.d.length; i++) {
                var strRow = [];
                var color = 'black';
                var checked = 'checked';
                if (data.d[i].IsActive != 1) {
                    color = "red";
                    checked = '';
                }


                strRow.push(String.format('<tr style="color:{0};">', color));
                strRow.push(String.format('<td> {0} </td>', (i + 1)));

                strRow.push(String.format('<td >{0}</td>', data.d[i].Name));
                //   strRow.push(String.format('<td>{0}</td>', data.d[i].Username));
                strRow.push(String.format('<td>{0}</td>', data.d[i].MobileNumber));
                strRow.push(String.format('<td> {0}</td>', data.d[i].GSTNo));
                strRow.push(String.format('<td>{0}</td>', data.d[i].PANNo));
                strRow.push(String.format('<td>{0}</td>', data.d[i].State));

               // strRow.push(String.format("<td><span  class='green' title='View'  onclick='return UserInformationSelectByIDForAll({0},{1})'  >Edit</span></td>", data.d[i].UserID, 3));
                strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick=\"UserInformationSelectByIDForAll({0},'{1}')\" ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].UserID,3));
                if (data.d[i].IsActive == 1) {

                    strRow.push(String.format('<td> <input type="checkbox" class="form-control" name="{0}" id="checks{0}"  onchange=\'UserInformationUpdateForActive({0},{1},{2})\'  CHECKED /> </td>', i, data.d[i].UserID, checked));
                }
                else {
                    strRow.push(String.format('<td> <input type="checkbox" class="form-control" name="{0}" id="checks{0}"  onchange=\'UserInformationUpdateForActive({0},{1},{2})\'   /> </td>', i, data.d[i].UserID, checked));
                }
              //  strRow.push(String.format("<td><span  class='red' title='Delete'  onclick='return UserInformationDelete({0})'  >Delete</span></td>", data.d[i].UserID));
                strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return UserInformationDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].UserID));
                strRow.push(String.format('<td>{0}</td>', data.d[i].LoginToken));
                strRow.push('</tr>');
                $("#ClientInformation").append(strRow.join(""));
            }

            addProperties(ClientInformation);
        }
        else {

            $("#ClientInformation").hide();
            $("#UserInformationDetails").html("");
            $("#UserInformationDetails").html("");
            $("#UserInformationDetails").html("<thead><tr><th>#</th><th>Name</th><th>Username</th><th>EmailID</th><th>Mobile</th><th>Edit</th><th>Delete</th></tr></thead><tbody></tbody>");

            for (var i = 0; i < data.d.length; i++) {
                var strRow = [];
                var color = 'black';

                strRow.push(String.format('<tr style="color:{0};">', color));
                strRow.push(String.format('<td> {0} </td>', (i + 1)));

                strRow.push(String.format('<td> {0} </td>', data.d[i].Name));
                strRow.push(String.format('<td> {0} </td>', data.d[i].Username));
                strRow.push(String.format('<td> {0} </td>', data.d[i].EmailID));
                strRow.push(String.format('<td> {0} </td>', data.d[i].MobileNumber));

                if (data.d[i].UserTypeID == 2)
                    strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick=\"UserInformationSelectByIDForAll({0},'{1}')\" ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].UserID, data.d[i].UserTypeID));
                
                else if(data.d[i].UserTypeID == 1)
                    strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Edit' style='width:50%; background-color: white; border-color: #fff' onclick=\"UserInformationSelectByIDForAll({0},'{1}')\" ><i class='fa fa-edit' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].UserID, data.d[i].UserTypeID));

                else
                    strRow.push(String.format('<td> {0} </td>', ""));


               // strRow.push(String.format("<td><span  class='red' title='Delete'  onclick='return UserInformationDelete({0})'  >Delete</span></td>", data.d[i].UserID));

              
               strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Delete' style='width:50%; background-color: white; border-color: #fff' onclick='return UserInformationDelete({0})'><i class='fa fa-trash-o' style='font-size:20px;color:Red'></i></button> </td>", data.d[i].UserID));
                strRow.push('</tr>');
                $("#UserInformationDetails").append(strRow.join(""));
            }
            addProperties(UserInformationDetails);
        }

    }
}

function UserInformationSelectForAll() {



    var UserTypeID = document.getElementById("inputUser").value;

    if (UserTypeID == "0") {
        location.reload();
    }

    var JsonObj =
   {
       UserTypeID: UserTypeID,
   };

    var JData = JSON.stringify(JsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "UserInformation.aspx/Select",
        data: JData,
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindData(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function UserInformationDelete(ID) {


    var ID = ID;
    var jsonObj = {
        UserID: ID,
    };

    var jData = JSON.stringify(jsonObj)
    $.alert.open({
        type: 'confirm',
        content: 'Are you sure? Do you want to delete',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "UserInformation.aspx/Delete",
                data: jData,
                dataType: "json",
                success: function (data) {
                    ShowRespose(data);
                }
            });
        },
        error: function (data) {
            $.alert.open({ content: String.format('Not updated'), icon: 'error', title: ClientName, });
        }
    });
}

function UserInformationSelectByIDForAll(ID, UserTypeID) {
    if (UserTypeID == 2) {
        location.href = "EmployeeRegistration.aspx?EmpRegID =" + ID;
    }
    if (UserTypeID == 3) {
        location.href = "CustomerRegistration.aspx?CustomerRegID =" + ID;
    }
}

function UserInformationUpdateForActive(i, UserID) {


    var IsActive = 0;

    if ($("#checks" + i).is(":checked")) {
        IsActive = 1;
    }


    var jsonObj = {
        UserID: UserID,
        IsActive: IsActive,

    };
    var jsonData = JSON.stringify(jsonObj)

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "UserInformation.aspx/UpdateAciveUser",
        data: jsonData,
        dataType: "json",
        success: function (data) {
            $.alert.open({
                content: data.d, icon: 'warning', title: ClientName,
            });
            UserInformationSelectForAll();
        },
        error: function (data) {
            $.alert.open({ content: String.format('not update'), icon: 'error', title: ClientName });
        }
    });


}




