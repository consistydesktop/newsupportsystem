﻿function TicketInformationSelectBySystemID(SystemID) {
    var SystemID = SystemID;
    var jsonObj = {
        SystemID: SystemID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "TicketReportForCustomer.aspx/SelectBySystemID",
        data: jData,
        dataType: "JSON",
        success: function (data) {
            BinddataTicketReport(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function TicketInformationSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "TicketReportForCustomer.aspx/Select",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BinddataTicketReport(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BinddataTicketReport(data) {
    $("#TicketReportForCustomer").html("");
    $("#TicketReportForCustomer").html("<thead><tr><th>Sr.No</th><th>TicketNo</th><th>Generated</th><th>Resolved</th><th>Service Name</th><th>RequestType</th><th>Priority</th><th>Description</th><th>Ticket Status</th><th>Reopen</th></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td>{0}</td>', data.d[i].TicketNo));
        strRow.push(String.format('<td>{0}</td>', data.d[i].GeneratedDate));
        strRow.push(String.format('<td>{0}</td>', data.d[i].ResolvedDate));
        strRow.push(String.format('<td>{0}</td>', data.d[i].ServiceName));
        strRow.push(String.format('<td>{0}</td>', data.d[i].RequestType));
        strRow.push(String.format('<td>{0}</td>', data.d[i].Priority));
        strRow.push(String.format("<td contenteditable='true'>{0}</td>", data.d[i].Description));
        strRow.push(String.format('<td>{0}</td>', data.d[i].TicketStatus));
        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='Reopen' style='width:50%; background-color: white; border-color: #fff' onclick='return TicketInformationSelectByID({0})' ><i class='fa fa-folder-open'  style='font-size:20px;color:blue'></i></button> </td>", data.d[i].TicketID));

        strRow.push('</tr>');
        $("#TicketReportForCustomer").append(strRow.join(""));
    }
    $('#TicketReportForCustomer').dataTable({
        "scrollX": true,
        "bDestroy": true,
        dom: 'Blfrtip',
        buttons: ['excelHtml5', 'pdfHtml5']
    });
}

function TicketInformationSelectByDateForCustomer() {
    var FromDate = $("#txtFromDate").val();
    var ToDate = $("#txtToDate").val();
    if (FromDate == "") {
        $.alert.open({
            content: String.format("Please select fromdate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: String.format("Please select Todate"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtToDate").focus();
            }
        });
        return false;
    }

    var jsonObj = {
        FromDate: FromDate,
        ToDate: ToDate,
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "TicketReportForCustomer.aspx/Search",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BinddataTicketReport(data);
        },
        error: function (result) {
            $.alert.open({
                content: String.format("No data available between given date"), icon: 'error', title: ClientName,
            });
        }
    });
    return false;
}

function TicketInformationSelectByID(TicketID) {
    window.location.href = "AddTicketForCustomer.aspx?TicketID=" + TicketID;
}