﻿function SelectTicketCount() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "Dashboard.aspx/SelectTicketCount",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            var TotalTickets = 0;
            var NewTasks = 0;
            var Pending = 0;
            var Resolved = 0;

            for (var i = 0; i < data.d.length; i++) {
                TotalTickets = TotalTickets + data.d[i].Count;
            }
            Pending = data.d[0].Count;
            New = data.d[1].Count;
            Resolved = data.d[2].Count;

            $("#TotalTickets").html(TotalTickets);
            $("#ResolvedTickets").html(Resolved);
            $("#PendingTickets").html(Pending);
            $("#NewTickets").html(New);

        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function SelectTaskCount() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "Dashboard.aspx/SelectTaskCount",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            var Total = 0;
            var New = 0;
            var Pending = 0;
            var Resolved = 0;

            for (var i = 0; i < data.d.length; i++) {
                Total = Total + data.d[i].Count;
            }

            Pending = data.d[1].Count;
            New = data.d[2].Count;
            Resolved = data.d[0].Count;

            $("#TotalTasks").html(Total);
            $("#CompletedTasks").html(Resolved);
            $("#PendingTasks").html(Pending);
            $("#NewTasks").html(New);

        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}


function SelectpaidSubscriptionDetails() {

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "Dashboard.aspx/SelectpaidSubscriptionDetails",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            $("#TotalSubscriptionAmount").html(data.d.toFixed(2));
        },
        error: function (result) {

        }
    });
}



function SelectUnpaidSubscription() {

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "Dashboard.aspx/SelectUnpaidSubscription",
        data: "{}",
        title: ClientName,
        dataType: "JSON",
        success: function (data) {
            BindData(data);
        },
        error: function (result) {
          
        }
    });
}


function BindData(data) {


    $("#SubscriptionDetails").html("");
    $("#SubscriptionDetails").html("<thead><tr><th>#</th><th>Name</th><th>URL</th><th>EndDate</th><th>Last Paid Amt</th><th>Month</th><th>Product</th></thead><tbody></tbody>");
    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';


        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));

        strRow.push(String.format('<td >{0}</td>', data.d[i].Name));
        strRow.push(String.format('<td>{0}</td>', data.d[i].WebSiteURL));
        strRow.push(String.format('<td>{0}</td>', data.d[i].LastDOC));
        strRow.push(String.format('<td> {0}</td>', data.d[i].LastAmount));
        strRow.push(String.format('<td>{0}</td>', data.d[i].Month));
        strRow.push(String.format('<td>{0}</td>', data.d[i].ProductName));

        strRow.push('</tr>');
        $("#SubscriptionDetails").append(strRow.join(""));
    }
    addProperties(SubscriptionDetails);
}