﻿function ProjectDetailMasterSelectAllForEmployee() {

    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewProjectDetailsForEmployee.aspx/Select",
        data: '',
        dataType: "JSON",
        success: function (data) {
            BindDataProjectDetailMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function ProjectDetailMasterSelectForEmployee(ProjectDetailMasterID) {
    var ProjectDetailMasterID = ProjectDetailMasterID;
    var jsonObj = {
        ProjectDetailMasterID: ProjectDetailMasterID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "ViewProjectDetailsForEmployee.aspx/SelectByID",
        data: jData,
        dataType: "JSON",
        success: function (data) {
            BindDataProjectDetailMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function DateValidation() {
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();
    if (FromDate == "") {
        $.alert.open({
            content: "Select From date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });
        return false;
    }
    if (ToDate == "") {
        $.alert.open({
            content: "Select To date", title: ClientName,
            callback: function () {
                $("#inputToDate").focus();
            }
        });

        return false;
    }

    if (new Date(FromDate) > new Date(ToDate)) {
        $.alert.open({
            content: "Invallid date", title: ClientName,
            callback: function () {
                $("#inputFromDate").focus();
            }
        });

        return false;
    }

    return true;
}

function ProjectDetailSelectByDate() {
    if (!DateValidation())
    { return false; }
    var FromDate = $("#inputFromDate").val();
    var ToDate = $("#inputToDate").val();

    var jsonObj = {
        FromDate: FromDate,
        ToDate: ToDate,
    };
    var jData = JSON.stringify(jsonObj);




    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewProjectDetailsForEmployee.aspx/Search",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            BindDataProjectDetailMaster(data);
        },
        error: function (result) {
            $.alert.open({
                content: String.format("No data available between given date"), icon: 'error', title: ClientName,
            });
        }
    });
    return false;
}

function BindDataProjectDetailMaster(data) {
    if (data.d.length < 1) {
        $.alert.open({
            content: "Project not found", title: ClientName,
            callback: function () {
               
            }
        });
        return false;
    }

    $("#ProjectDetailMasterDetails").html("");
    $("#ProjectDetailMasterDetails").html("<thead><tr><th>#</th><th>Assigned Date</th><th>Project</th><th>Description</th><th>Attachment</th></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format('<td>{0}</td>', data.d[i].DOCInString));
        strRow.push(String.format('<td>{0}</td>', data.d[i].ProjectName));
        strRow.push(String.format('<td>{0}</td>', data.d[i].Description));

        strRow.push(String.format("<td class='text-center'> <button type='button' class='btn' title='download' style='width:50%; background-color: white; border-color: #fff' onclick='return DownloadAttachment({0})' ><i class='fa fa-download' style='font-size:20px;color:blue'></i></button> </td>", data.d[i].ProjectDetailMasterID));
        strRow.push('</tr>');
        $("#ProjectDetailMasterDetails").append(strRow.join(""));
    }

    addProperties(ProjectDetailMasterDetails);
}

function DownloadAttachment(ProjectDetailMasterID) {
    var jsonObj = {
        ProjectDetailMasterID: ProjectDetailMasterID
    };
    var jData = JSON.stringify(jsonObj)
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "ViewProjectDetailsForEmployee.aspx/SelectAttachment",
        data: jData,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            if (data.d.length < 1) {
                $.alert.open({
                    content: String.format("There is no attachment"), icon: 'error', title: ClientName,
                });
            }
            else {
                BindDataAttachment(data);
                $("#modal_attachment").modal('show');
            }
        },
        error: function (result) {
            $.alert.open({
                content: String.format("Somthing worng"), icon: 'error', title: ClientName,
            });
        }
    });
}

function BindDataAttachment(data) {
    $("#Attachments").html("");
    $("#Attachments").html("<thead><tr><th>Sr.No</th><th>Attachment</th></tr></thead><tbody></tbody>");

    for (var i = 0; i < data.d.length; i++) {
        var strRow = [];
        var color = 'black';

        strRow.push(String.format('<tr style="color:{0};">', color));
        strRow.push(String.format('<td> {0} </td>', (i + 1)));
        strRow.push(String.format("<td class='text-center'> <a href='{0}'  target='_blank'>{1}</a> </td>", data.d[i].Attachment, data.d[i].OriginalFileName));

        strRow.push('</tr>');
        $("#Attachments").append(strRow.join(""));
    }

    $('#Attachments').dataTable({
        "bDestroy": true,
    });
}