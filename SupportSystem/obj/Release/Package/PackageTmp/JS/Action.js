﻿function TicketInformationInsertAction() {
    $.alert.open({
        type: 'confirm',
        content: 'Are you want to submit action ',
        icon: 'confirm',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }
            if (button.toUpperCase() != 'Yes'.toUpperCase()) {
                return false;
            }

            var ResolvedDate = $("#inputResolvedDate").val();
            var TicketStatus = $("#inputAction").val();
            var EmployeeRemark = $("#inputEmployeeRemark").val();
            var TicketID = $("#TicketID").text();
            var UserID = $("#UserID").text();
            var TicketNo = $("#TicketNo").text();

            var jsonObj = {
                ResolvedDate: ResolvedDate,
                TicketStatus: TicketStatus,
                EmployeeRemark: EmployeeRemark,
                TicketID: TicketID,
                UserID: UserID,
                TicketNo: TicketNo
            };
            var jData = JSON.stringify(jsonObj)
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Action.aspx/Insert",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowResponse(data);
                    location.href = "/ViewTicketEmployee.aspx";
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing worng"), icon: 'warning', title: ClientName,
                    });
                }
            });
        }
    });
}

function ShowResponse(data) {
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}

function Reset() {
    $("#TicketID").text("");
    $("#inputResolvedDate").val("");
    $("#inputAction").val("");
    $("#inputDescription").val("");
    $("#inputEmployeeRemark").val("");
    $("#inputPriority").val("");
}

function TicketInformationSelectByTicketID(TicketID) {
    var TicketID = TicketID;
    $("#txtDescription").val("Please Wait...");
    $("#txtPriority").val("Please Wait...");

    var jsonObj = {
        TicketID: TicketID,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "Action.aspx/SelectByTicketID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#inputDescription").val(data.d.Description);
            $("#inputPriority").val(data.d.Priority);
            $("#TicketID").html(TicketID);
            $("#UserID").html(data.d.UserID);
            $("#TicketNo").html(data.d.TicketNo);
            $("#inputAction").val(data.d.TicketStatus).trigger('chosen:updated');
            $("#inputEmployeeRemark").val(data.d.EmployeeRemark);
        },
        error: function (result) {
            $.alert.open({ content: "Not updated", icon: 'warning', title: ClientName, });
        }
    });
}