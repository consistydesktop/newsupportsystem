﻿function ShowRespose(data) {
    Icon = data.d.Icon;
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: Icon, title: ClientName,
        callback: function () {
            Cancel();
        }
    });
}

function Cancel() {
    $("#txtOldPassword").val("");
    $("#txtNewPassword").val("");
    $("#txtConfirmPassword").val("");
}

function ResetPasswordValidation() {
    var OldPassword = $("#txtOldPassword").val();
    var NewPassword = $("#txtNewPassword").val();
    var ConfirmPassword = $("#txtConfirmPassword").val();

    if (OldPassword == "") {
        $.alert.open({
            content: String.format("Please enter old password"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtOldPassword").focus();
            }
        });

        return false;
    }

    if (NewPassword == "") {
        $.alert.open({
            content: String.format("Please enter new password"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtNewPassword").focus();
            }
        });

        return false;
    }
    if (ConfirmPassword == "") {
        $.alert.open({
            content: String.format("Please enter password again"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtConfirmPassword").focus();
            }
        });

        return false;
    }
    if (ConfirmPassword != NewPassword) {
        $.alert.open({
            content: String.format("Not matching"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtConfirmPassword").focus();
            }
        });

        return false;
    }
}

function UserInformationUpdatePassword() {
    var OldPassword = $("#txtOldPassword").val();
    var NewPassword = $("#txtNewPassword").val();
    var ConfirmPassword = $("#txtConfirmPassword").val();

    if (ResetPasswordValidation() == false) {
        return false;
    }

    var jsonObj = {
        OldPassword: OldPassword,
        NewPassword: NewPassword,
    };

    var jData = JSON.stringify(jsonObj)

    $.alert.open({
        type: 'confirm',
        content: 'Are you sure ?Want to change password ',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }
            if (button.toUpperCase() != 'Yes'.toUpperCase()) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ResetPassword.aspx/Update",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowRespose(data);
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing worng"), icon: 'error', title: ClientName,
                    });
                }
            });
            Cancel();
        }
    });
    return false;
}