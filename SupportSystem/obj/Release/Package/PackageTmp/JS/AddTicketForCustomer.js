﻿function ServiceMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AddTicketForCustomer.aspx/SelectServiceName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataServiceMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataServiceMaster(data) {
    $("#ddlServiceName").html("");
    $("#ddlServiceName").append("<option value='0'>Select Service </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].ServiceID, data.d[i].ServiceName));
        $("#ddlServiceName").append(strRowDropdown);
    }
    $("#ddlServiceName").chosen();
}

function SystemMasterSelect() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AddTicketForCustomer.aspx/SelectSystemName",
        data: "{}",
        dataType: "JSON",
        success: function (data) {
            BindDataSystemMaster(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSystemMaster(data) {
    $("#ddlSystemName").html("");
    $("#ddlSystemName").append("<option value='0'>Select System </option>");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#ddlSystemName").append(strRowDropdown);
    }
    $("#ddlSystemName").chosen();
}

function AddTicketCustomerForValidation() {
    var SystemName = $("#ddlSystemName option:selected").text();
    var SystemID = $("#ddlSystemName").val();
    var ServiceID = $("#ddlServiceName").val();
    var RequestType = $("#ddlRequestType").val();
    var Description = $("#txtEditor").val();
    var Priority = $("#ddlPriority").val();
    var TicketGenerationDate = $("#TicketGenerationDate").val();

    if (ServiceID == '0') {
        $.alert.open({
            content: String.format("Please select service"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#ddlServiceName").focus();
            }
        });
        return false;
    }

    if (RequestType == '0') {
        $.alert.open({
            content: String.format("Please select Request Type"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#ddlRequestType").focus();
            }
        });
        return false;
    }

    if (Priority == '0') {
        $.alert.open({
            content: String.format("Please select priority"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#ddlPriority").focus();
            }
        });

        return false;
    }
    if (Description == "") {
        $.alert.open({
            content: String.format("Please write some description"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtEditor").focus();
            }
        });
        return false;
    }
}

$("#btnSubmit").click(function (evt) {
    if (AddTicketCustomerForValidation() == false) {
        return false;
    }
    var File = $("#PersonalFile").val();
    if (File == "") {
        var result = 0;
        TicketInformationInsertTicket(result);
    }

    else {
        var data = new FormData();
        var fileUpload = $("#PersonalFile").get(0);
        var files = fileUpload.files;

        for (var i = 0; i < files.length; i++) {
            data.append('PersonalDoc', files[i]);
        }

        $.ajax({
            url: "FileUploadHandlerForTicket.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {

                if (result == "Only jpg / jpeg /pdf and png files are allowed!" || result == "File size must under 2MB!") {
                    ShowResponseFile(result);
                }
                else {
                    TicketInformationInsertTicket(result);
                }

                
            },

            error: function (err) {
                alert('Error:' + err)
            }
        });
    }

    evt.preventDefault();
});


function ShowResponseFile(result) {

    $.alert.open({
        content: result, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}




function TicketInformationInsertTicket(result) {
    var SystemName = $("#ddlSystemName option:selected").text();
    var SystemID = $("#ddlSystemName").val();
    var ServiceID = $("#ddlServiceName").val();
    var RequestType = $("#ddlRequestType").val();
    var Description = $("#txtEditor").val();
    var TeamAny = $("#TMID ").val();
    var FileIDs = result;
    var Priority = $("#ddlPriority").val();
    var TicketGenerationDate = $("#TicketGenerationDate").val();

    if (AddTicketCustomerForValidation() == false) {
        return false;
    }

    $.alert.open({
        type: 'confirm',
        content: 'Do you want to add ticket',
        icon: 'confirm',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }

            if (button.toUpperCase() != 'yes'.toUpperCase()) {
                return false;
            }

            var jsonObj = {
                SystemName: SystemName,
                SystemID: SystemID,
                ServiceID: ServiceID,
                RequestType: RequestType,
                Description: Description,
                TeamAny: TeamAny,
                FileIDs: FileIDs,
                Priority: Priority,
                TicketGenerationDate: TicketGenerationDate
            };

            var jData = JSON.stringify(jsonObj)

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AddTicketForCustomer.aspx/Insert",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowRespose(data);
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing worng"), icon: 'error', title: ClientName,
                    });
                }
            });
        }
    });
}

function Reset() {
    $("#ddlSystemName").val("0");
    $("#ddlServiceName").val("0");
    $("#ddlRequestType").val("0");
    $("#ddlSystemName").val("");
    $("#ddlServiceName").val("");
    var htmlEditorExtender = $('.ajax__html_editor_extender_texteditor');
    htmlEditorExtender.html("");
    $("#TMID").val("");
    $("#fileAttachment").val("");
    $("#ddlPriority").val("0");
    $("#TicketGenerationDate").val("");
}

function ShowRespose(data) {
    Icon = data.d.Icon;
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}

function TicketInformationSelectByTicketID(TicketID) {
    var TicketID = TicketID;
    $("#ddlServiceName").val("Please Wait...").trigger('chosen:updated');
    $("#ddlSystemName").val("Please Wait...").trigger('chosen:updated');
    $("#ddlRequestType option:selected").text("Please Wait...");
    $("#TMID").val("Please Wait...");
    $("#txtEditor").val("Please Wait...");
    $("#ddlPriority option:selected").text("Please Wait...");
    $("#TicketID").html(TicketID);

    var jsonObj = {
        TicketID: TicketID,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AddTicketForCustomer.aspx/SelectByTicketID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#TMID").val(data.d.TeamAny);
            var htmlEditorExtender = $('.ajax__html_editor_extender_texteditor');
            htmlEditorExtender.html(data.d.Description);
            $("#ddlPriority option:selected").text(data.d.Priority);
            $("#ddlServiceName").val(data.d.ServiceID).trigger('chosen:updated');
            $("#ddlSystemName").val(data.d.SystemID).trigger('chosen:updated');
            $("#TicketID").html(TicketID);
        },
        error: function (result) {
            $.alert.open({ content: "Not updated", icon: 'error', title: ClientName, });
        }
    });
}