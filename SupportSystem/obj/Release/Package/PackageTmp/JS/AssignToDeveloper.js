﻿function UserInformationSelectByEmployeeID() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AssignToDeveloper.aspx/SelectEmployeeName",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataSelectEmployeeName(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSelectEmployeeName(data) {
    $("#inputEmployeeName").html("");
    $("#inputEmployeeName").append("<option value=0> Select Employee </option> ");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].UserID, data.d[i].Name));
        $("#inputEmployeeName").append(strRowDropdown);
    }
    $("#inputEmployeeName").chosen();
}

function Validation() {
    var AssignEmployeeID = $("#inputEmployeeName").val();
    var Description = $("#inputDescription").val();
    var TicketID = $("#TicketID").text();
    var SystemID = $("#inputSystemName").val();
    if (AssignEmployeeID == 0) {
        $.alert.open({
            content: String.format("Please select employee"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputEmployeeName").focus();
            }
        });
        return false;
    }

    if (Description == "") {
        $.alert.open({
            content: String.format("Please enter Description"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputDescription").focus();
            }
        });
        return false;
    }

    if (SystemID == "0") {
        $.alert.open({
            content: String.format("Please select System"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#inputSystemName").focus();
            }
        });
        return false;
    }
    return true;
}

function TicketInformationInsert() {
    if (!Validation())
        return false;
    $.alert.open({
        type: 'confirm',
        content: 'Are you want to assign ticket to developer ',
        icon: 'confirm',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }
            if (button.toUpperCase() != 'Yes'.toUpperCase()) {
                return false;
            }

            var AssignEmployeeID = $("#inputEmployeeName").val();
            AssignDate = $("#AssignDatepicker").val();
            var AdminRemark = $("#inputAdminRemark").val();
            var TicketID = $("#TicketID").text();
            var Description = $("#inputDescription").val();
            var SystemID = $("#inputSystemName").val();


            var TicketNo = $("#TicketNo").text();

            var jsonObj = {
                AssignEmployeeID: AssignEmployeeID,
                AssignDate: AssignDate,
                AdminRemark: AdminRemark,
                TicketID: TicketID,
                Description: Description,
                SystemID: SystemID,
                TicketNo: TicketNo,
            };
            var jData = JSON.stringify(jsonObj)
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AssignToDeveloper.aspx/Insert",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    ShowResponse(data);
                    location.href = "ViewTicketsAdmin.aspx";
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing worng"), icon: 'error', title: ClientName,
                    });
                }
            });
        }
    });
}

function ShowResponse(data) {
    Icon = data.d.Icon;
    Message = data.d.Message;
    $.alert.open({
        content: Message, icon: 'warning', title: ClientName,
        callback: function () {
            Reset();
        }
    });
}

function Reset() {
    $("#inputEmployeeName").val(0).trigger('chosen:updated');
    $("#inputSystemName").val("");
    $("#inputAnyDeskID").val("");
    $("#inputDescription").val("");
    $("#AssignDatepicker").val("");
    $("#inputAdminRemark").val("");
}

function TicketInformationSelectByTicketID(TicketID) {
    var TicketID = TicketID;
    $("#inputSystemName").val("Please Wait...").trigger('chosen:updated');
    $("#inputAnyDeskID").val("Please Wait...");
    $("#inputDescription").val("Please Wait...");
    $("#inputAdminRemark").val("Please Wait...");
    $("#TicketID").html(TicketID);
    $("#inputTitle").val("Please Wait...");
    var jsonObj = {
        TicketID: TicketID,
    };
    var jData = JSON.stringify(jsonObj);
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AssignToDeveloper.aspx/SelectByTicketID",
        data: jData,
        dataType: "JSON",
        title: ClientName,
        success: function (data) {
            $("#inputEmployeeName").val(data.d.DeveloperID).trigger('chosen:updated');
            $("#inputTitle").val(data.d.Title);
            $("#inputSystemName").val(data.d.SystemName).trigger('chosen:updated');
            $("#inputAnyDeskID").val(data.d.TeamAny);
            $("#inputDescription").val(data.d.Description);
            $("#TicketID").html(TicketID);
            $("#inputAdminRemark").val(data.d.AdminRemark);
            $("#TicketNo").html(data.d.TicketNo);
        },
        error: function (result) {
            $.alert.open({ content: "Not updated", icon: 'error', title: ClientName, });
        }
    });
}

function SelectSystemMaster() {
    $.ajax({
        type: "POST",
        contentType: "Application/json;charset:UTF-8",
        url: "AssignToDeveloper.aspx/SelectSystem",
        data: "{}",
        dataType: "JSON",
        async: false,
        success: function (data) {
            BindDataSystem(data);
        },
        error: function (result) {
            $.alert.open({ content: "Data not found", icon: 'error', title: ClientName, });
        }
    });
}

function BindDataSystem(data) {
    $("#inputSystemName").html("");
    $("#inputSystemName").append("<option value=0> Select System </option> ");
    for (var i = 0; i < data.d.length; i++) {
        var strRowDropdown = [];
        strRowDropdown.push(String.format("<option value='{0}'> {1} </option> ", data.d[i].SystemID, data.d[i].SystemName));
        $("#inputSystemName").append(strRowDropdown);
    }
    $("#inputSystemName").chosen();
}