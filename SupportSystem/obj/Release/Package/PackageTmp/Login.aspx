﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SupportSystem.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="shortcut icon" type="image/x-icon" href="/Images/favicon.ico" />
    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />
    
     <link rel="stylesheet" type="text/css" href="/PublicSite/css/style.css" />
   
    <link href="/Styles/alert/css/alert.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/alert/themes/default/theme.css" rel="stylesheet" type="text/css" />
    <title>Welcome to Consisty Support System</title>

</head>
<body>
    <div class="container mt-5">
       
        <div class="row">
            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12 mx-auto">
                <div class="loginform">
                    <p class="text-center">
                        <img src="/PublicSite/images/logo.svg" width="250px" />
                    </p>
                    <form>
                        <div class="form-group">
                            <label for="inputUserName">Username</label>
                            <input type="text" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control loginput" id="inputUserName" title="Mobile number" aria-describedby="emailHelp" placeholder="Mobile Number" autofocus="autofocus"  />
                            <label style="display: none; color: red;" id="lblUsername">
                                Mobile number is required
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword">Password</label>
                            <input type="password" class="form-control loginput" id="inputPassword" placeholder="Password" title="Password" />
                            <label style="display: none; color: red;" id="lblPassword">
                                Password is required
                            </label>
                        </div>
                        <div class="form-group row mt-3">
                            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
                                <img id="captcha" src="Captcha.aspx?" />
                                <img id="referesh" src="/Images/captcha-refresh.png" onclick="return reloadCaptcha();" title="Referesh Captcha" />
                            </div>
                            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                                <fieldset class="styled-input">
                                    <label for="inputCaptcha">Captcha</label>
                                    <input type="text" class="form-control loginput" id="inputCaptcha" aria-describedby="emailHelp" placeholder="Enter captcha" title="Captcha" maxlength="4" autocomplete="off"  />
                                    <label style="display: none; color: red;" id="lblCaptcha">
                                        Captcha is required
                                    </label>
                                </fieldset>
                            </div>
                        </div>
                        <div class="form-group row">
  
                        </div>
                      
                        <button type="button" class="btn btn-info buttons w-100 logbutton" onclick="return SignIn();">Sign In</button>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 text-center text-xl-left text-lg-left text-md-left text-sm-left"><a href="Forgot.aspx">Forgot Password?</a></div>
                        <p class="text-center mt-3">Don't have an account? <a href="SignUp.aspx">Sign Up</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

    
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="/Scripts/jquery-ui.js" type="text/javascript"></script>
<script src="/Styles/alert/js/alert.js" type="text/javascript"></script>
<script src="/Scripts/jquery-ui.min.js"></script>
<script src="/Styles/alert/js/alert.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>

<script src="/Scripts/String.js"></script>

<script src="/JS/Login.js"></script>
<script>
    function isNumberKey(e) {
        $("#lblUsername").css('display', 'none');
        $("#lblPassword").css('display', 'none');
        $("#lblCaptcha").css('display', 'none');
        var r = e.which ? e.which : event.keyCode;
        return r > 31 && (48 > r || r > 57) && (e.which != 46 || $(this).val().indexOf('.') != -1) ? !1 : void 0
    }
    function reloadCaptcha() {
        document.getElementById('captcha').src = document.getElementById('captcha').src + '?' + new Date();
        document.getElementById("inputCaptcha").value = "";
    }
    $("#txtotpnew").focus();
    $("#resendOTP").css("display", "none");
    setTimeout(function () {
        $("#resendOTP").css("display", "block");
    }, 30000);
</script>
</html>
