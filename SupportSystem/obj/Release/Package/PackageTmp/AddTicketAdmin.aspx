﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddTicketAdmin.aspx.cs" Inherits="SupportSystem.AddTicketAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <input type="text" class="form-control" id="TicketGenerationDate" style="display: none" name="TicketGenerationDate" />
        <section>

            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Add Ticket
                                <button class="btn btn-info buttons float-right" type="button" onclick="location.href='/ViewTicketsAdmin.aspx'" title="View Tickets">View Report</button></h6>
                        </div>
                    </div>
                </div>
            </section>

            <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputService">Service Name</label>
                        <select id="inputService" class="form-control loginput"></select>
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputEmployeeName">Developer Name</label>
                        <select id="inputEmployeeName" class="form-control"></select>
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputSystemName">System Name</label>
                        <select id="inputSystemName" class="form-control"></select>
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputRequestType">Request Type</label>
                        <select id="inputRequestType" class="form-control">
                            <option value='0'>Select Request Type</option>
                            <option value="Change" data-toggle="tooltip" data-placement="top" title="Change request is new development">Change</option>
                            <option value="Issue" data-toggle="tooltip" data-placement="top" title="Fault/Error in an existing system">Issue</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputPriority">Priority</label>
                        <select id="inputPriority" class="form-control">
                            <option value='0'>Select Priority</option>
                            <option value="Low">Low</option>
                            <option value="Medium">Medium</option>
                            <option value="High">High</option>
                            <option value="Critical">Critical</option>
                        </select>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputAnyDeskID">TeamViewer/AnyDesk ID (Optional)</label>
                        <input type="text" id="inputAnyDeskID" class="form-control" placeholder="TeamViewer/AnyDesk ID" onkeypress="return IsAlphaNumeric(event)" />
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputAttachment">Attachment (Optional)</label>
                        <input type="file" id="inputAttachment" onchange="readURLTicketImage(this);" class="form-control" />
                        <img id="imageTicket" style="width: 297px; height: 210px;" src="//placehold.it/297x210" accept="images/*" />

                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <label for="inputDescription">Description (Optional)</label>
                        <textarea id="inputDescription" placeholder="Description" class="form-control" style="height: 165px"></textarea>
                    </div>
                </div>
                
                <div class="form-group row mt-3">

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" id="SubmitButton" onclick="" title="Add Ticket">Submit</button>
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton buttons" id="UpdateButton" onclick="" style="display: none" title="Update Ticket">Update</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" id="CancelButton" onclick="Reset()" title="Clear All">Cancel</button>
                            </div>
                        </div>
                    </div>

                </div>

            </form>
        </section>
    </div>

    <script src="/JS/AddTicketAdmin.js"></script>
    <script>

        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122));
            return ret;
        }
        $(document).ready(function () {
            ServiceMasterSelect();
            SystemMasterSelect();
            UserInformationSelectEmployeeName();
            if (window.location.href.indexOf("TicketID") > -1) {
                var url = window.location.href;
                var TicketID = url.split('=')
                TicketInformationSelectByTicketID(TicketID[1]);
            }

            $("#ddlServiceName").focus();
            $("#inputPriority").chosen();
            $("#inputRequestType").chosen();
        });

        $(function () {
            $("#TicketGenerationDate").datepicker({
                dateFormat: 'yy-dd-mm',
                changeMonth: true,
                changeYear: true
            });
            $("#TicketGenerationDate").datepicker().datepicker("setDate", new Date());
        });
        function nospaces(t) {

            if (t.value.match(/\s/g)) {
                $.alert.open({
                    content: String.format("Sorry, you are not allowed to enter any spaces"), icon: 'warning', title: ClientName,
                    callback: function () {
                        $("#TMID").focus();
                    }
                });
                t.value = t.value.replace(/\s/g, '');

            }
        }
        function Hide() {
            $("#lblforall").css('display', 'none');
            $("#ddlDeveloper").focus();
        }
    </script>
</asp:Content>
