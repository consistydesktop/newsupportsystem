﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Action.aspx.cs" Inherits="SupportSystem.Action" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Action
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
        </section>

        <form class="formbox mt-4">
            <label id="TicketID" style="display: none"></label>
            <label id="UserID" style="display: none"></label>
            <label id="TicketNo" style="display: none"></label>
            <div class="form-group row mt-3">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <label for="inputResolvedDate">Resolved Date</label>

                    <input type="text" class="form-control" id="inputResolvedDate" title="Issue Resolved Date" />
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <label for="inputPriority">Priority</label>

                    <input type="text" class="form-control" id="inputPriority" title="Priority" />
                </div>
            </div>
            <div class="form-group row mt-3">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <label for="inputAction">Action </label>
                    <select class="form-control loginput" id="inputAction" title="Select Action">
                        <option value="0">Select Status</option>
                        <option value="New">New</option>
                        <option value="Confirmed">Confirmed</option>
                        <option value="Pending">Pending</option>
                        <option value="Resolved">Resolved</option>
                        <option value="Closed">Closed</option>
                    </select>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <label for="inputDescription">Description</label>
                    <textarea class="form-control" id="Textarea1" title="Enter Some Description" placeholder="Enter Description"></textarea>
                </div>

            </div>

            <div class="form-group row mt-3">

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <label for="inputEmployeeRemark">Remark</label>
                    <textarea class="form-control" id="inputEmployeeRemark" title="Enter Remark" placeholder="Enter Remark"></textarea>
                </div>

            </div>
            <div class="form-group row mt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-6">
                            <button type="button" class="btn btn-info buttons w-100 logbutton" title="Action " id="SubmitButton" onclick="TicketInformationInsertAction()">Submit</button>
                        </div>
                        <div class="col-6">
                            <button type="button" class="btn btn-danger w-100  logbutton cancelbtn" onclick="Reset()" title="Reset all" id="CancelButton">Cancel</button>
                        </div>
                    </div>
                    <label id="ResponseMsgLbl"></label>
                </div>
            </div>
        </form>

    </div>


    <script src="/JS/Action.js"></script>
    <script>
        $("#inputResolvedDate").datepicker({
            dateFormat: 'd MM, yy',
            changeMonth: true,
            changeYear: true

        });
        $("#inputResolvedDate").datepicker().datepicker("setDate", new Date());

        $(document).ready(function () {
            $("#inputAction").chosen();
            if (window.location.href.indexOf("TicketID") > -1) {
                var url = window.location.href;
                var TicketID = url.split('=')
                TicketInformationSelectByTicketID(TicketID[1]);
            }
        });
    </script>
</asp:Content>
