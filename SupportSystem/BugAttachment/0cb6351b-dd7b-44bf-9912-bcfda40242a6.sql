USE [AlleRecharge]
GO
/****** Object:  StoredProcedure [dbo].[AndroidTokenInsert]    Script Date: 02/13/2019 11:12:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AndroidTokenInsert] @AndroidToken VARCHAR(500)
	,@UserID INT
AS
BEGIN
	INSERT INTO [dbo].[AndroidToken] (
		[AndroidToken]
		,[UserID]
		)
	VALUES (
		@AndroidToken
		,@UserID
		)
END



GO
/****** Object:  StoredProcedure [dbo].[AndroidVersionUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AndroidVersionUpdate] @Version VARCHAR(300)
	,@UserID INT
AS
BEGIN
	UPDATE AndroidVersion
	SET Version = @Version
		,DOC = (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))
	WHERE ID = 1
END



GO
/****** Object:  StoredProcedure [dbo].[APILogInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[APILogInsert]  
@Request varchar(max),  
@Response varchar(max),
@UserID int = null
as   
begin   
DECLARE @Output INT;   
insert into [dbo].[APIUserAPILog]([Request],[Response],[UserID])values(@Request,@Response,@UserID)   
 SET @Output = @@IDENTITY   
 RETURN @Output  
end  
GO
/****** Object:  StoredProcedure [dbo].[APIUserAPILogInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:  Sushant Yelpale      
-- Create date: 8/8/2018      
-- Description: Insert API User DMR Log To DB      
-- APIUserAPILogInsert 'UserName: 9462500007, Token: oD43af270j1OIpo3fz41, MobileNumber:9975688036'    
-- =============================================      
CREATE PROCEDURE [dbo].[APIUserAPILogInsert] @Request VARCHAR(max)
	,@Response VARCHAR(max)
AS
BEGIN
	DECLARE @Output INT;

	INSERT INTO [dbo].[APIUserAPILog] (
		[Request]
		,[Response]
		)
	VALUES (
		@Request
		,@Response
		)

	SET @Output = @@IDENTITY

	RETURN @Output
END



GO
/****** Object:  StoredProcedure [dbo].[APIUserAPILogUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:  Sushant yelpale    
-- Create date: 8/8/2018    
-- Description: insert DMR Response log to DB    
-- =============================================    
CREATE PROCEDURE [dbo].[APIUserAPILogUpdate]     
 @userID INT    
 ,@Response VARCHAR(Max)    
 ,@RequestLogID Int  
AS    
BEGIN    
 UPDATE [dbo].[APIUserAPILog]    
 SET [UserID] = @userID    
  ,[Response] = @Response    
 where [ID] = @RequestLogID  
END 


GO
/****** Object:  StoredProcedure [dbo].[APIUserCallbackSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
     
    
-- [APIUserCallbackSelect] 'ae072ac4af1147e7b120',4       
CREATE PROCEDURE [dbo].[APIUserCallbackSelect] 
  @RequestId VARCHAR(100)    
 ,@ServiceID INT = NULL    
AS    
BEGIN    
 IF @ServiceID IS NULL    
 BEGIN    
  SELECT R.UserID    
   ,R.RechargeId    
   ,REPLACE(UI.CallBackUrl, '?', '') AS CallBackUrl    
   ,R.APIRequestID    
   ,R.[OpeningBalance]    
  FROM dbo.Recharge(NOLOCK) R    
  INNER JOIN [dbo].[UserInformation](NOLOCK) UI ON UI.UserID = R.UserID    
  WHERE R.[RequestID] = @RequestId    
   AND UI.[UserTypeID] = 4    
 END    
 ELSE    
 BEGIN    
  SELECT R.UserID    
   ,R.RechargeId    
   ,REPLACE(UI.CallBackUrl, '?', '') AS CallBackUrl    
   ,R.APIRequestID    
   ,R.[OpeningBalance]    
  FROM dbo.Recharge(NOLOCK) R    
  INNER JOIN [dbo].[UserInformation](NOLOCK) UI ON UI.UserID = R.UserID    
  WHERE R.[RequestID] = @RequestId    
   AND R.ServiceID = @ServiceID    
   AND UI.[UserTypeID] = 4    
      
  UNION ALL    
      
  SELECT R.UserID    
   ,R.MoneyTransferID AS RechargeId    
   ,REPLACE(UI.CallBackUrl, '?', '') AS CallBackUrl    
   ,R.APIUserRequestID AS APIRequestID    
   ,R.[UserOpeningBalance] AS OpeningBalance    
  FROM dbo.MoneyTransfer(NOLOCK) R    
  INNER JOIN [dbo].[UserInformation](NOLOCK) UI ON UI.UserID = R.UserID    
  WHERE R.ProviderRequestID = @RequestId    
   AND R.DMRServiceID = @ServiceID    
   AND UI.[UserTypeID] = 4    
 END    
END
GO
/****** Object:  StoredProcedure [dbo].[APIUserCallbackSelectData]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[APIUserCallbackSelectData] @RechargeId VARCHAR(100)
AS
BEGIN
	SELECT R.UserID
		,UI.CallBackUrl
		,R.APIRequestID
		,R.[OpeningBalance]
	FROM Recharge R
	INNER JOIN [dbo].[UserInformation] UI ON UI.UserID = R.UserID
	WHERE R.[RechargeID] = @RechargeId
		AND UI.[UserTypeID] = 4
END



GO
/****** Object:  StoredProcedure [dbo].[APIUserRequestResponseInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[APIUserRequestResponseInsert] @UserID VARCHAR(10)
	,@Request VARCHAR(1000)
	,@Response VARCHAR(1000)
	,@Description VARCHAR(500)
AS
BEGIN
	INSERT INTO [dbo].[APIUserRequestResponse] (
		[UserID]
		,[Request]
		,[Response]
		,[Description]
		)
	VALUES (
		@UserID
		,@Request
		,@Response
		,@Description
		);
END



GO
/****** Object:  StoredProcedure [dbo].[AppPoolRestartSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--======================================  
--ALTER d by:Rahul Hembade  
--Date:21/07/2018  
--Reson: Stored Procedure for resend recharge    
--========================================    
CREATE PROCEDURE[dbo].[AppPoolRestartSelect]
AS
BEGIN
	DECLARE @originalPendingCount INT;

	SET @originalPendingCount = 0

	SELECT @originalPendingCount = count(1)
	FROM [dbo].[Recharge](NOLOCK)
	WHERE UPPER([Status]) = UPPER('PROCESS')
		OR UPPER([Status]) = UPPER('PENDING')

	DECLARE @expectedPendingCount INT;

	SET @expectedPendingCount = 1000;

	DECLARE @appPoolRestartURL VARCHAR(500);

	SELECT TOP (1) @expectedPendingCount = [ProccessCount]
		,@appPoolRestartURL = [URL]
	FROM [dbo].[AppPoolRestartSetting]
	ORDER BY ID DESC

	IF (@originalPendingCount < @expectedPendingCount)
	BEGIN
		SELECT '' AS URL

		PRINT '@originalPendingCount : ' + convert(VARCHAR, @originalPendingCount);
		PRINT '@expectedPendingCount : ' + convert(VARCHAR, @expectedPendingCount);

		RETURN (0)
	END

	DECLARE @appPoolRestartLastTime DATETIME;

	SELECT TOP (1) @appPoolRestartLastTime = DOC
	FROM [dbo].[ApppoolHistory]
	ORDER BY id DESC

	DECLARE @CurTime DATETIME;

	SET @CurTime = CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30'), (0))

	DECLARE @diffInMinute INT;

	PRINT '@appPoolRestartLastTime : ' + convert(VARCHAR, @appPoolRestartLastTime);
	PRINT '@CurTime : ' + convert(VARCHAR, @CurTime);

	SELECT @diffInMinute = datediff(minute, @appPoolRestartLastTime, @CurTime)

	IF (@diffInMinute < 15)
	BEGIN
		SELECT '' AS URL

		PRINT '@diffInMinute : ' + convert(VARCHAR, @diffInMinute);

		RETURN (0)
	END

	PRINT 'need restart : ';

	INSERT INTO [dbo].[ApppoolHistory] (
		[Status]
		,[Count]
		)
	VALUES (
		'Restart'
		,@originalPendingCount
		);

	SELECT @appPoolRestartURL AS URL
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceRequestDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================  
--Auther :Priyanka  
--Date :11-3-17  
--Purpose Balance Transfer Delete [dbo].[BalanceRequest]  
--==========================================  
--BalanceRequestDelete   
CREATE PROCEDURE [dbo].[BalanceRequestDelete] @RequestID INT
	,@Remark VARCHAR(1000)
AS
BEGIN
	UPDATE dbo.BalanceRequest
	SET IsDeleted = 1
		,STATUS = 'Rejected'
		,[Reason] = @Remark
	WHERE RequestID = @RequestID
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceRequestInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
--==========================================      
--Auther :Priyanka      
--Date :27-2-17      
--Purpose BalanceRequestInsert      
--Modifay:Rahul Hembade      
--reson:DMR Balance      
--==========================================      
CREATE PROCEDURE [dbo].[BalanceRequestInsert] @UserID INT  
 ,@BalanceType VARCHAR(50)  
 ,@ParentID VARCHAR(100)  
 ,@Amount INT  
 ,@PayMode VARCHAR(100)  
 ,@BankName VARCHAR(50)  
 ,@Remark VARCHAR(50)  
 ,@UTRNo VARCHAR(50)  
 ,@Type CHAR(20)  
 ,@ProviderRequestID VARCHAR(450) = '0'  
 ,@PaymentImagePath  VARCHAR(max)  = null
AS  
BEGIN  
 DECLARE @Balance INT;  
  
 SET @Balance = abs(@Amount);  
  
 IF (@UserID = @ParentID)  
 BEGIN  
  PRINT 'Fail : Balance transaction for same user not possible' -- dont change Fail word, it checking in CS code          
  
  RETURN (0)  
 END  
  
 INSERT INTO BalanceRequest (  
  BalanceType  
  ,UserID  
  ,RequestTo  
  ,Amount  
  ,PayMode  
  ,BankName  
  ,Remark  
  ,UTRNo  
  ,Type  
  ,ProviderRequestID  
  ,ParentID  
  ,[PaymentImagePath]
  )  
 VALUES (  
  @BalanceType  
  ,@UserID  
  ,@ParentID  
  ,@Balance  
  ,@PayMode  
  ,@BankName  
  ,@Remark  
  ,@UTRNo  
  ,@Type  
  ,@ProviderRequestID  
  ,@ParentID  
  ,@PaymentImagePath
  )  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[BalanceRequestSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BalanceRequestSelect] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT;

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	IF (@UserTypeID = 2)
	BEGIN
		SELECT BR.RequestID
			,BR.UserID
			,BR.Amount
			,BR.PayMode
			,IsNull(BR.BillNumber, 'CashPayment') AS BillNumber
			,BR.STATUS
			,BR.IsDeleted
			,UI.UserName AS RequestFrom
			,UI.UserTypeID AS RUserTypeID
			,BR.UserID AS RequestTo
			,UIP.UserName AS ParentName
			,UIP.UserTypeID AS DUserTypeID
			,convert(VARCHAR, BR.DOC, 113) AS DOC
		FROM UserInformation UI
		INNER JOIN BalanceRequest BR ON UI.UserID = BR.UserID
		INNER JOIN UserInformation UIP ON BR.UserID = UIP.UserID
		WHERE @UserID IN (
				BR.UserID
				,BR.UserID
				)
			AND BR.IsDeleted = 0
			AND Type = '1'
		ORDER BY convert(DATETIME, BR.DOC, 103) DESC
	END

	IF (@UserTypeID = 1)
	BEGIN
		SELECT BR.RequestID
			,BR.UserID
			,BR.Amount
			,BR.PayMode
			,IsNull(BR.BillNumber, 'CashPayment') AS BillNumber
			,BR.STATUS
			,BR.IsDeleted
			,UI.UserName AS RequestFrom
			,UI.UserTypeID AS RUserTypeID
			,UIP.UserTypeID AS DUserTypeID
			,BR.UserID AS RequestTo
			,UIP.UserName AS ParentName
			,convert(VARCHAR, BR.DOC, 113) AS DOC
		FROM UserInformation UI
		INNER JOIN BalanceRequest BR ON UI.UserID = BR.UserID
		INNER JOIN UserInformation UIP ON BR.UserID = UIP.UserID
		WHERE BR.IsDeleted = 0
			AND Type = '1'
		ORDER BY convert(DATETIME, BR.DOC, 103) DESC
	END

	IF (@UserTypeID = 3)
	BEGIN
		SELECT BR.RequestID
			,BR.UserID
			,BR.Amount
			,BR.PayMode
			,IsNull(BR.BillNumber, 'CashPayment') AS BillNumber
			,BR.STATUS
			,BR.IsDeleted
			,UI.UserName AS RequestFrom
			,UI.UserTypeID AS RUserTypeID
			,UIP.UserTypeID AS DUserTypeID
			,BR.UserID AS RequestTo
			,UIP.UserName AS ParentName
			,convert(VARCHAR, BR.DOC, 113) AS DOC
		FROM UserInformation UI
		INNER JOIN BalanceRequest BR ON UI.UserID = BR.UserID
		INNER JOIN UserInformation UIP ON BR.UserID = UIP.UserID
		WHERE BR.UserID = @UserID
			AND BR.IsDeleted = 0
			AND Type = '1'
		ORDER BY convert(DATETIME, BR.DOC, 103) DESC
	END
END


GO
/****** Object:  StoredProcedure [dbo].[BalanceRequestSelectAll]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[BalanceRequestSelectAll] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT;

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	IF (@UserTypeID = 2)
	BEGIN
		SELECT BR.RequestID
			,BR.BalanceType
			,BR.UserID
			,BR.Amount
			,BR.PayMode
			,IsNull(BR.BillNumber, 'CashPayment') AS BillNumber
			,BR.STATUS
			,BR.IsDeleted
			,UI.UserName AS RequestFrom
			,UI.UserTypeID AS RUserTypeID
			,BR.RequestTo AS RequestTo
			,UIP.UserName AS ParentName
			,UIP.UserTypeID AS DUserTypeID
			,convert(VARCHAR, BR.DOC, 113) AS DOC
			,BR.BankName
		FROM UserInformation UI
		INNER JOIN BalanceRequest BR ON UI.UserID = BR.UserID
		INNER JOIN UserInformation UIP ON BR.RequestTo = UIP.UserID
		WHERE @UserID IN (
				BR.UserID
				,BR.ParentID
				)
			AND upper(Type) = upper('Transfer')
		ORDER BY convert(DATETIME, BR.DOC, 103) DESC
	END

	IF (@UserTypeID = 1)
	BEGIN
		SELECT BR.RequestID
			,BR.BalanceType
			,BR.UserID
			,BR.Amount
			,BR.PayMode
			,IsNull(BR.BillNumber, 'CashPayment') AS BillNumber
			,BR.ParentID
			,BR.STATUS
			,BR.IsDeleted
			,UI.UserName AS RequestFrom
			,UI.UserTypeID AS RUserTypeID
			,UIP.UserTypeID AS DUserTypeID
			,BR.RequestTo AS RequestTo
			,UIP.UserName AS ParentName
			,convert(VARCHAR, BR.DOC, 113) AS DOC
			,BR.BankName
		FROM UserInformation UI
		INNER JOIN BalanceRequest BR ON UI.UserID = BR.UserID
		INNER JOIN UserInformation UIP ON BR.RequestTo = UIP.UserID
		WHERE upper(Type) = upper('Transfer')
		ORDER BY convert(DATETIME, BR.DOC, 103) DESC
	END

	IF (@UserTypeID = 3)
	BEGIN
		SELECT BR.RequestID
			,BR.BalanceType
			,BR.UserID
			,BR.Amount
			,BR.PayMode
			,IsNull(BR.BillNumber, 'CashPayment') AS BillNumber
			,BR.ParentID
			,BR.STATUS
			,BR.IsDeleted
			,UI.UserName AS RequestFrom
			,UI.UserTypeID AS RUserTypeID
			,UIP.UserTypeID AS DUserTypeID
			,BR.RequestTo AS RequestTo
			,UIP.UserName AS ParentName
			,convert(VARCHAR, BR.DOC, 113) AS DOC
			,BR.BankName
		FROM UserInformation UI
		INNER JOIN BalanceRequest BR ON UI.UserID = BR.UserID
		INNER JOIN UserInformation UIP ON BR.RequestTo = UIP.UserID
		WHERE BR.UserID = @UserID
			AND upper(Type) = upper('Transfer')
		ORDER BY convert(DATETIME, BR.DOC, 103) DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceRequestSelectAmountByRequestID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[BalanceRequestSelectAmountByRequestID] @RequestID INT  
AS  
BEGIN  
 SELECT Amount  
 ,BalanceType
 ,RequestTo as SenderID
 ,UserID As ReceiverID
 FROM [dbo].[BalanceRequest]  
 WHERE [RequestID] = @RequestID  
END  



GO
/****** Object:  StoredProcedure [dbo].[BalanceRequestSelectByStatus]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------    
--==========================================        
--Auther :Priyanka        
--Date :08-3-17        
--Purpose Balance Transfer Report        
--Modifay:Rahul Hembade        
--Reson:DMR Balance        
--==========================================        
--BalanceRequestSelect 2        
-- BalanceRequestSelectByStatus 1319,'All','07/09/2018','07/09/2019'        
CREATE PROCEDURE [dbo].[BalanceRequestSelectByStatus] @UserID INT
	,@Status VARCHAR(100) = NULL
	,@FromDate VARCHAR(250)
	,@ToDate VARCHAR(250)
AS
BEGIN
	DECLARE @UserTypeID INT;
	DECLARE @DistUserTypeID INT = 2;
	DECLARE @FOSUserTypeID INT = 7;
	DECLARE @SuperDistUserTypeID INT = 8;
	DECLARE @AdminUserTypeID INT = 1;
	DECLARE @SupportUserTypeID INT = 9;

	IF (upper(@Status) = upper('All'))
	BEGIN
		SET @Status = ''
	END

	PRINT @Status

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	IF (
			@UserTypeID = @DistUserTypeID
			OR @UserTypeID = @FOSUserTypeID
			OR @UserTypeID = @SuperDistUserTypeID
			)
	BEGIN
		PRINT 'FOR DIST and FOS'

		SELECT BR.RequestID
			,BR.Reason
			,BR.BalanceType
			,BR.UserID
			,BR.Amount
			,BR.PayMode
			,BR.Remark
			,IsNull(BR.BillNumber, 'CashPayment') AS BillNumber
			,BR.STATUS
			,BR.IsDeleted
			,BR.PaymentImagePath
			,UI.UserName AS RequestFrom
			,UI.UserTypeID AS RUserTypeID
			,BR.RequestTo AS RequestTo
			,UIP.UserName AS ParentName
			,UIP.UserTypeID AS DUserTypeID
			,IsNull(BR.UTRNo, '') AS UTRNo
			,convert(VARCHAR(12), BR.DOC, 113) + right(convert(VARCHAR(39), BR.DOC, 22), 11) AS DOC
			,IsNull(BR.BankName, '-') AS BankName
		FROM UserInformation UI
		INNER JOIN BalanceRequest BR ON UI.UserID = BR.UserID
		INNER JOIN UserInformation UIP ON BR.RequestTo = UIP.UserID
		WHERE @UserID IN (
				BR.UserID
				,BR.RequestTo
				)
			AND upper(BR.Type) = upper('Transfer')
			AND BR.STATUS LIKE '%' + @Status + '%'
			AND (
				convert(DATE, BR.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BR.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BR.DOC, 103) DESC
	END
	ELSE IF (
			@UserTypeID = @AdminUserTypeID
			OR @UserTypeID = @SupportUserTypeID
			)
	BEGIN
		SELECT BR.RequestID
			,BR.Reason
			,BR.BalanceType
			,BR.UserID
			,BR.Amount
			,BR.PayMode
			,IsNull(BR.BillNumber, 'CashPayment') AS BillNumber
			,BR.STATUS
			,BR.IsDeleted
			,BR.PaymentImagePath
			,UI.UserName AS RequestFrom
			,UI.UserTypeID AS RUserTypeID
			,BR.RequestTo AS RequestTo
			,UIP.UserName AS ParentName
			,UIP.UserTypeID AS DUserTypeID
			,IsNull(BR.UTRNo, '') AS UTRNo
			,convert(VARCHAR(12), BR.DOC, 113) + right(convert(VARCHAR(39), BR.DOC, 22), 11) AS DOC
			,BR.Remark
			,IsNull(BR.BankName, '-') AS BankName
		--,BT.[Reason] as adminremark        
		FROM UserInformation UI
		INNER JOIN BalanceRequest BR ON UI.UserID = BR.UserID
		--left join [BalanceTransfer] BT on BT.[ReceiverID] = BR.UserID         
		INNER JOIN UserInformation UIP ON BR.RequestTo = UIP.UserID
		WHERE BR.RequestTo = @UserID
			AND upper(BR.Type) = upper('Transfer')
			AND BR.STATUS LIKE '%' + @Status + '%'
			AND (
				convert(DATE, BR.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BR.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BR.DOC, 103) DESC
	END
	ELSE
	BEGIN
		SELECT BR.RequestID
			,BR.Reason
			,BR.BalanceType
			,BR.UserID
			,BR.Amount
			,BR.PayMode
			,IsNull(BR.BillNumber, 'CashPayment') AS BillNumber
			,BR.STATUS
			,BR.IsDeleted
			,BR.PaymentImagePath
			,UI.UserName AS RequestFrom
			,UI.UserTypeID AS RUserTypeID
			,IsNull(BR.UTRNo, '') AS UTRNo
			,BR.RequestTo AS RequestTo
			,UIP.UserName AS ParentName
			,UIP.UserTypeID AS DUserTypeID
			,convert(VARCHAR(12), BR.DOC, 113) + right(convert(VARCHAR(39), BR.DOC, 22), 11) AS DOC
			,BR.Remark
			,IsNull(BR.BankName, '-') AS BankName
		FROM UserInformation UI
		INNER JOIN BalanceRequest BR ON UI.UserID = BR.UserID
		INNER JOIN UserInformation UIP ON BR.RequestTo = UIP.UserID
		WHERE BR.UserID = @UserID
			AND upper(BR.Type) = upper('Transfer')
			AND BR.STATUS LIKE '%' + @Status + '%'
			AND (
				convert(DATE, BR.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BR.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BR.DOC, 103) DESC
	END
END

GO
/****** Object:  StoredProcedure [dbo].[BalanceRequestSelectForBlockAmount]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  -- =============================================    
-- Author:  Sushant Yelpale    
-- Create date: 10 Oct 2018    
-- Description: Select Amount equal to Upline Reversal requested Amount    
-- [BalanceRequestSelectForBlockAmount] 3, 'Recharge'    
-- =============================================    
CREATE PROCEDURE [dbo].[BalanceRequestSelectForBlockAmount] @UserID INT  
 ,@BalanceType VARCHAR(50)  
 ,@BlockAmount DECIMAL(18, 5) OUTPUT  
AS  
BEGIN  
 SELECT @BlockAmount = ISNULL(SUM(Amount), 0)  
 FROM BalanceRequest(NOLOCK)  
 WHERE RequestTo = @userID  
  AND upper(Type) = upper('REVERSE')  
  AND Upper(STATUS) = upper('Pending')  
  AND IsDeleted = 0 
  AND BalanceType =  @BalanceType
  
 IF @BlockAmount < 0  
 BEGIN  
  SET @BlockAmount = 0  
 END  
END  


GO
/****** Object:  StoredProcedure [dbo].[BalanceRequestSelectPendingCount]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		Sushant Yelpale
-- Create date: 27 Dec 2018
-- Description:	Selects pending Balance request Count
-- BalanceRequestSelectPendingCount 1
-- =============================================
CREATE PROCEDURE [dbo].[BalanceRequestSelectPendingCount]
	@UserID int
AS
BEGIN
	SELECT count(RequestID) AS PendingRequest
	from BalanceRequest
	where RequestTo = @UserID
	AND [Type] like '%TRANSFER%'
	AND IsDeleted = 0
	AND [Status] = 'Pending'
END

GO
/****** Object:  StoredProcedure [dbo].[BalanceReverseReportSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BalanceReverseReportSelect] @UserID INT
AS
BEGIN
	SELECT ISNULL(SUM(Amount), 0) AS Amount
	FROM BalanceRequest(NOLOCK)
	WHERE RequestTo = @userID
		AND upper(Type) = upper('REVERSE')
		AND Upper(STATUS) = upper('Pending')
		AND IsDeleted = 0
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceReverseReportSelectDateWise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------    
--==========================================          
--Auther :Rahul Hembade          
--Date :08-3-17          
--Purpose Balance Transfer Report          
--==========================================          
--BalanceRequestSelect 2          
-- BalanceReverseReportSelectDateWise 1,'','11/09/2018','11/10/2018'          
CREATE PROCEDURE [dbo].[BalanceReverseReportSelectDateWise] @UserID INT
	,@Status VARCHAR(100)
	,@FromDate VARCHAR(250)
	,@ToDate VARCHAR(250)
AS
BEGIN
	DECLARE @UserTypeID INT;

	IF (upper(@Status) = upper('All'))
	BEGIN
		SET @Status = ''
	END

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	DECLARE @AdminTypeID INT = 1;
	DECLARE @DistTypeID INT = 2;
	DECLARE @SuperDistTypeID INT = 8;
	DECLARE @RetailerTypeID INT = 3;
	DECLARE @FOSTypeID INT = 7;
	DECLARE @SupportTypeID INT = 9;

	IF (
			@UserTypeID = @DistTypeID
			OR @UserTypeID = @SuperDistTypeID
			OR @UserTypeID = @FOSTypeID
			)
	BEGIN
		PRINT 'FOR DIST and FOS'

		SELECT BR.RequestID
			,BR.UserID
			,BR.BalanceType
			,BR.Amount
			,BR.PayMode
			,IsNull(BR.BillNumber, 'CashPayment') AS BillNumber
			,BR.STATUS
			,BR.IsDeleted
			,UI.UserName AS RequestFrom
			,UI.UserTypeID AS RUserTypeID
			,UIP.UserTypeID AS DUserTypeID
			,BR.RequestTo AS RequestTo
			,UIP.UserName AS ParentName
			,convert(VARCHAR(12), BR.DOC, 113) + right(convert(VARCHAR(39), BR.DOC, 22), 11) AS DOC
			,UB.CurrentBalance
			,BR.Remark
			,IsNull(BR.Reason, '-') AS Reason
		FROM UserInformation UI
		INNER JOIN BalanceRequest BR ON UI.UserID = BR.UserID
		INNER JOIN UserInformation UIP ON BR.RequestTo = UIP.UserID
		INNER JOIN UserBalance UB ON UB.UserID = BR.RequestTo
		WHERE @UserID IN (
				BR.UserID
				,BR.ParentID
				)
			AND UPPER(BR.Type) = UPPER('REVERSE')
			AND BR.STATUS LIKE '%' + @Status + '%'
			AND (
				convert(DATE, BR.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BR.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BR.DOC, 103) DESC
	END
	ELSE IF (@UserTypeID = @AdminTypeID)
	BEGIN
		PRINT 'Admin'

		SELECT BR.RequestID
			,BR.UserID
			,BR.BalanceType
			,BR.Amount
			,BR.PayMode
			,IsNull(BR.BillNumber, 'CashPayment') AS BillNumber
			,BR.STATUS
			,BR.IsDeleted
			,UI.UserName AS RequestFrom
			,UI.UserTypeID AS RUserTypeID
			,UIP.UserTypeID AS DUserTypeID
			,BR.RequestTo AS RequestTo
			,UIP.UserName AS ParentName
			,convert(VARCHAR(12), BR.DOC, 113) + right(convert(VARCHAR(39), BR.DOC, 22), 11) AS DOC
			,UB.CurrentBalance
			,BR.Remark
			,IsNull(BR.Reason, '-') AS Reason
		FROM UserInformation UI
		INNER JOIN BalanceRequest BR ON UI.UserID = BR.UserID
		INNER JOIN UserInformation UIP ON BR.RequestTo = UIP.UserID
		INNER JOIN UserBalance UB ON UB.UserID = BR.RequestTo
		WHERE UPPER(BR.Type) = UPPER('REVERSE')
			AND BR.STATUS LIKE '%' + @Status + '%'
			AND (
				convert(DATE, BR.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BR.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BR.DOC, 103) DESC
	END
	ELSE
	BEGIN
		SELECT BR.RequestID
			,BR.UserID
			,BR.BalanceType
			,BR.Amount
			,BR.PayMode
			,IsNull(BR.BillNumber, 'CashPayment') AS BillNumber
			,BR.STATUS
			,BR.IsDeleted
			,UI.UserName AS RequestFrom
			,UI.UserTypeID AS RUserTypeID
			,UIP.UserTypeID AS DUserTypeID
			,BR.RequestTo AS RequestTo
			,UIP.UserName AS ParentName
			,convert(VARCHAR(12), BR.DOC, 113) + right(convert(VARCHAR(39), BR.DOC, 22), 11) AS DOC
			,UB.CurrentBalance
			,BR.Remark
			,IsNull(BR.Reason, '-') AS Reason
		FROM UserInformation UI
		INNER JOIN BalanceRequest BR ON UI.UserID = BR.UserID
		INNER JOIN UserInformation UIP ON BR.RequestTo = UIP.UserID
		INNER JOIN UserBalance UB ON UB.UserID = BR.RequestTo
		WHERE BR.RequestTo = @UserID
			AND UPPER(BR.Type) = UPPER('REVERSE')
			AND BR.STATUS LIKE '%' + @Status + '%'
			AND (
				convert(DATE, BR.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BR.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BR.DOC, 103) DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceReverseSelectDatewise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- [BalanceReverseSelectDatewise] 1,'01/08/2018','01/09/2018'  
CREATE PROCEDURE [dbo].[BalanceReverseSelectDatewise] @UserID INT
	,@FromDate VARCHAR(250)
	,@ToDate VARCHAR(250)
AS
BEGIN
	DECLARE @UserTypeID INT
		,@SenderName VARCHAR(50);

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	DECLARE @Admin INT = 1;
	DECLARE @SuperDistributor INT = 8;
	DECLARE @Distributor INT = 2;
	DECLARE @Retailer INT = 3;
	DECLARE @FOS INT = 7;
	DECLARE @APIUser INT = 4;
	DECLARE @Support INT = 9;

	--Admin Recharge Report    
	IF (
			@UserTypeID = @Admin
			OR @UserTypeID = @Support
			)
	BEGIN
		SELECT TOP (1000) BT.BalanceTransferID
			,BT.SenderID
			,BT.ReceiverID
			,BT.Amount
			,BT.Reason
			,BT.OpeningBalanceSender
			,BT.CloseBalanceSender
			,convert(VARCHAR(12), BT.DOC, 113) + right(convert(VARCHAR(39), BT.DOC, 22), 11) AS DOC
			,UI.UserName AS Receiver
			,UI.UserTypeID AS RUserTypeID
			,UIS.UserName AS Sender
			,UIS.UserTypeID AS DUserTypeID
			,BT.BalanceType
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID
		INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
		WHERE BT.Type IN (
				'RE'
				,'REVERSE'
				)
			AND (
				convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY DOC DESC
	END

	IF (
			@UserTypeID = @Distributor
			OR @UserTypeID = @Retailer
			OR @UserTypeID = @SuperDistributor
			OR @UserTypeID = @FOS
			OR @UserTypeID = @APIUser
			)
	BEGIN
		SELECT TOP (1000) BT.BalanceTransferID
			,BT.SenderID
			,BT.ReceiverID
			,BT.Amount
			,BT.Reason
			,BT.OpeningBalanceSender
			,BT.CloseBalanceSender
			,convert(VARCHAR(12), BT.DOC, 113) + right(convert(VARCHAR(39), BT.DOC, 22), 11) AS DOC
			,UI.UserName AS Receiver
			,UI.UserTypeID AS RUserTypeID
			,UIS.UserName AS Sender
			,UIS.ParentID
			,UIS.UserTypeID AS DUserTypeID
			,BT.BalanceType
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID
		INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
		WHERE @UserID IN (
				BT.SenderID
				,UIS.ParentID
				)
			AND BT.Type IN (
				'RE'
				,'REVERSE'
				)
			AND (
				convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY DOC DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceTransferInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BalanceTransferInsert] --2,10,3,'SUCCESS','T',1                      
	@BalanceType VARCHAR(10)
	,@SenderID INT
	,@Amount DECIMAL(18, 2)
	,@ReceiverID INT
	,@Status VARCHAR(50)
	,@Type VARCHAR(10)
	,@RequestID INT = 0
	,@AdminRemark VARCHAR(200)
	,@OUTPUT VARCHAR(500) OUT
AS
BEGIN
	DECLARE @TRANS_DESC VARCHAR(100);
	DECLARE @OpeningAmount DECIMAL(18, 5);
	DECLARE @MinimumBalance DECIMAL(18, 5);
	DECLARE @ClosingAmount DECIMAL(18, 5);
	DECLARE @CloseBalanceReceiver DECIMAL(18, 5);
	DECLARE @OpeningBalanceReceiver DECIMAL(18, 5);
	DECLARE @RESULT INT;
	DECLARE @TxnID INT;
	DECLARE @SenderName VARCHAR(50)
	DECLARE @ReceiverName VARCHAR(50)
	DECLARE @DocType VARCHAR(50)
	DECLARE @TransDesc VARCHAR(200)

	IF (@SenderID = @ReceiverID)
	BEGIN
		SET @Output = 'Fail : Balance transaction for same user not possible' -- dont change Fail word, it checking in CS code            

		RETURN (0)
	END

	DECLARE @return_value INT
		,@OutputValidation VARCHAR(100);

	EXEC @return_value = [dbo].[BalanceTransferSelectCheckDuplicate] @SenderUserID = @SenderID
		,@ReceiverUserID = @ReceiverID
		,@BalanceType = @BalanceType
		,@Amount = @Amount
		,@Output = @OutputValidation OUTPUT

	IF (@OutputValidation != 'OK')
	BEGIN
		SET @Output = 'Fail ' + @OutputValidation

		RETURN (0)
	END

	IF (@BalanceType = 'Recharge')
	BEGIN
		--Sender Current Balance                  
		SELECT @OpeningAmount = ISNULL(CurrentBalance, 0)
			,@MinimumBalance = ISNULL([MinimumBalance], 0)
		FROM dbo.UserBalance
		WHERE UserID = @SenderID

		DECLARE @BlockAmount DECIMAL(18, 5);

		EXECUTE [BalanceRequestSelectForBlockAmount] @UserID = @SenderID
			,@BalanceType = 'Recharge'
			,@BlockAmount = @BlockAmount OUT

		IF (
				UPPER(@Type) = 'REVERSE'
				OR UPPER(@Type) = 'RE'
				)
		BEGIN
			SET @BlockAmount = 0;
		END

		--Sender ClosingAmount                       
		IF (@Amount > (@OpeningAmount - @MinimumBalance - @BlockAmount))
		BEGIN
			IF (@Amount > (@OpeningAmount - @MinimumBalance))
			BEGIN
				SET @Output = 'Fail : Not enough balance'
			END

			IF (@Amount > (@OpeningAmount - @MinimumBalance - @BlockAmount))
			BEGIN
				DECLARE @blAmt DECIMAL(18, 2) = @BlockAmount

				SET @Output = 'Fail : Your ' + convert(VARCHAR(20), @blAmt) + ' is blocked.'
			END
			ELSE
			BEGIN
				SET @Output = 'Fail : Not enough balance'
			END

			RETURN (0)
		END

		SET @ClosingAmount = @OpeningAmount - ABS(@Amount)

		--Receiver Current Balance                  
		SELECT @OpeningBalanceReceiver = ISNULL(CurrentBalance, 0)
		FROM dbo.UserBalance
		WHERE UserID = @ReceiverID

		--Receiver Close Amount                  
		SET @CloseBalanceReceiver = @OpeningBalanceReceiver + ABS(@Amount)

		--if(@OpeningAmount > @Amount)                  
		INSERT INTO [BalanceTransfer] (
			BalanceType
			,SenderID
			,OpeningBalanceSender
			,CloseBalanceSender
			,Amount
			,ReceiverID
			,STATUS
			,Type
			,OpeningBalanceReceiver
			,CloseBalanceReceiver
			,RequestBalanceID
			,Reason
			)
		VALUES (
			'Recharge'
			,@SenderID
			,@OpeningAmount
			,@ClosingAmount
			,@Amount
			,@ReceiverID
			,@Status
			,@Type
			,@OpeningBalanceReceiver
			,@CloseBalanceReceiver
			,@RequestID
			,@AdminRemark
			)

		SELECT @RESULT = @@IDENTITY

		SET @TxnID = (
				SELECT @@identity
				)

		PRINT @RESULT

		-------------- Insert Balance Transfer Entry in Oustanding table----------          
		DECLARE @UserParent INT;

		SELECT @UserParent = ParentID
		FROM Userinformation
		WHERE UserID = @ReceiverID

		IF (
				(
					Upper(@type) = upper('T')
					OR Upper(@type) = upper('TRANSFER')
					)
				AND @UserParent = @SenderID
				)
		BEGIN
			EXECUTE OutstandingBalanceInsertBalanceTransfer @ReceiverID = @ReceiverID
				,@SenderID = @SenderID
				,@Amount = @Amount
		END

		-------------------------End Outstanding Balance Entry--------------         
		--Sender update                  
		UPDATE UserBalance
		SET CurrentBalance = @ClosingAmount
		WHERE UserID = @SenderID

		--Receiver update                  
		UPDATE UserBalance
		SET CurrentBalance = @CloseBalanceReceiver
		WHERE UserID = @ReceiverID

		IF (
				Upper(@type) = upper('T')
				OR Upper(@type) = upper('TRANSFER')
				)
		BEGIN
			SET @Output = 'Balance transfer successfully with Txn ID:' + convert(VARCHAR, @TxnID);-- dont change success word, it checking in CS code            
		END
		ELSE IF (
				Upper(@type) = upper('RE')
				OR UPPER(@Type) = upper('REVERSE')
				)
		BEGIN
			SET @Output = 'Balance reverse successfully with Txn ID:' + convert(VARCHAR, @TxnID);-- dont change success word, it checking in CS code            
		END
		ELSE
		BEGIN
			SET @Output = 'Balance transaction successfully with Txn ID:' + convert(VARCHAR, @TxnID);-- dont change success word, it checking in CS code            
		END

		UPDATE BalanceRequest
		SET STATUS = 'Accepted'
			,Reason = @AdminRemark
		WHERE RequestID = @RequestID

		SELECT @SenderName = name
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @SenderID;

		SELECT @ReceiverName = name
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @ReceiverID;

		PRINT @SenderName

		------TransactionDetails insert For SenderID---                
		SET @TransDesc = 'Recharge Balance transfer Amount: ' + CONVERT(VARCHAR, @Amount) + ' To ' + @ReceiverName;
		SET @DocType = 'Balance Transfer';

		IF (
				Upper(@type) = upper('RE')
				OR UPPER(@Type) = upper('REVERSE')
				)
		BEGIN
			SET @DocType = 'Balance Reverse';
			SET @TransDesc = 'Recharge Balance Reverse Amount: ' + CONVERT(VARCHAR, @Amount) + ' Send To ' + @ReceiverName;
		END

		PRINT @DocType;

		EXEC dbo.TransactionDetailsInsertByUser @SenderID
			,@RESULT
			,18
			,@ReceiverID
			,''
			,--as Consumer no                 
			@DocType
			,@OpeningAmount
			,@ClosingAmount
			,@Amount --@TransactionAmount                                            
			,0
			,'DEBIT'
			,''
			,@TransDesc

		------TransactionDetails insert For Receiver---                
		SET @TransDesc = 'Recharge Balance Receive Amount: ' + CONVERT(VARCHAR, @Amount) + ' From ' + @SenderName;
		SET @DocType = 'Balance Receive';

		EXEC dbo.TransactionDetailsInsertByUser @ReceiverID
			,@RESULT
			,18
			,@SenderID
			,''
			,--as Consumer no                       
			@DocType
			,@OpeningBalanceReceiver
			,@CloseBalanceReceiver
			,@Amount --@TransactionAmount                                                   
			,0
			,'CREDIT'
			,''
			,@TransDesc

		RETURN (0);
	END

	--this block for DMR Balance            
	IF (@BalanceType = 'DMT')
	BEGIN
		--Sender Current Balance                  
		SELECT @OpeningAmount = ISNULL(DMRBalance, 0)
			,@MinimumBalance = ISNULL([MinimumBalance], 0)
		FROM dbo.UserBalance
		WHERE UserID = @SenderID

		DECLARE @BlockAmountDMT DECIMAL(18, 5);

		EXECUTE [BalanceRequestSelectForBlockAmount] @UserID = @SenderID
			,@BalanceType = 'DMT'
			,@BlockAmount = @BlockAmountDMT OUT

		IF (
				UPPER(@Type) = 'REVERSE'
				OR UPPER(@Type) = 'RE'
				)
		BEGIN
			SET @BlockAmountDMT = 0;
		END

		--Sender ClosingAmount                       
		IF (@Amount > (@OpeningAmount - @MinimumBalance - @BlockAmountDMT))
		BEGIN
			IF (@Amount > (@OpeningAmount - @MinimumBalance))
			BEGIN
				SET @Output = 'Fail : Not enough balance'
			END

			IF (@Amount > (@OpeningAmount - @MinimumBalance - @BlockAmount))
			BEGIN
				DECLARE @blAmtDMT DECIMAL(18, 2) = @BlockAmountDMT

				SET @Output = 'Fail : Your ' + convert(VARCHAR(20), @blAmtDMT) + ' is blocked.'
			END
			ELSE
			BEGIN
				SET @Output = 'Fail : Not enough balance'
			END

			RETURN (0)
		END

		SET @ClosingAmount = @OpeningAmount - ABS(@Amount)

		--Receiver Current Balance                  
		SELECT @OpeningBalanceReceiver = ISNULL(DMRBalance, 0)
		FROM dbo.UserBalance
		WHERE UserID = @ReceiverID

		--Receiver Close Amount                  
		SET @CloseBalanceReceiver = @OpeningBalanceReceiver + ABS(@Amount)

		--if(@OpeningAmount > @Amount)                  
		INSERT INTO [BalanceTransfer] (
			BalanceType
			,SenderID
			,OpeningBalanceSender
			,CloseBalanceSender
			,Amount
			,ReceiverID
			,STATUS
			,Type
			,OpeningBalanceReceiver
			,CloseBalanceReceiver
			,RequestBalanceID
			,Reason
			)
		VALUES (
			'DMT'
			,@SenderID
			,@OpeningAmount
			,@ClosingAmount
			,@Amount
			,@ReceiverID
			,@Status
			,@Type
			,@OpeningBalanceReceiver
			,@CloseBalanceReceiver
			,@RequestID
			,@AdminRemark
			)

		SELECT @RESULT = @@IDENTITY

		SET @TxnID = (
				SELECT @@identity
				)

		PRINT @RESULT

		-------------- Insert Balance Transfer Entry in Oustanding table----------          
		DECLARE @UserParentDMT INT;

		SELECT @UserParentDMT = ParentID
		FROM Userinformation
		WHERE UserID = @ReceiverID

		IF (
				(
					Upper(@type) = upper('T')
					OR Upper(@type) = upper('TRANSFER')
					)
				AND @UserParentDMT = @SenderID
				)
		BEGIN
			EXECUTE OutstandingBalanceInsertBalanceTransfer @ReceiverID = @ReceiverID
				,@SenderID = @SenderID
				,@Amount = @Amount
		END

		-------------------------End Outstanding Balance Entry--------------         
		--Sender update                  
		UPDATE UserBalance
		SET DMRBalance = @ClosingAmount
		WHERE UserID = @SenderID

		--Receiver update                  
		UPDATE UserBalance
		SET DMRBalance = @CloseBalanceReceiver
		WHERE UserID = @ReceiverID

		IF (
				Upper(@type) = upper('T')
				OR Upper(@type) = upper('TRANSFER')
				)
		BEGIN
			SET @Output = 'Balance transfer successfully with Txn ID:' + convert(VARCHAR, @TxnID);-- dont change success word, it checking in CS code            
		END
		ELSE IF (
				Upper(@type) = upper('RE')
				OR UPPER(@Type) = upper('REVERSE')
				)
		BEGIN
			SET @Output = 'Balance reverse successfully with Txn ID:' + convert(VARCHAR, @TxnID);-- dont change success word, it checking in CS code            
		END
		ELSE
		BEGIN
			SET @Output = 'Balance transaction successfully with Txn ID:' + convert(VARCHAR, @TxnID);-- dont change success word, it checking in CS code            
		END

		UPDATE BalanceRequest
		SET STATUS = 'Accepted'
			,Reason = @AdminRemark
		WHERE RequestID = @RequestID

		SELECT @SenderName = name
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @SenderID;

		SELECT @ReceiverName = name
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @ReceiverID;

		PRINT @SenderName

		------TransactionDetails insert For SenderID---                
		SET @TransDesc = 'DMT Balance transfer Amount: ' + CONVERT(VARCHAR, @Amount) + ' To ' + @ReceiverName;
		SET @DocType = 'Balance Transfer DMT';

		IF (
				Upper(@type) = upper('RE')
				OR UPPER(@Type) = upper('REVERSE')
				)
		BEGIN
			SET @DocType = 'Balance Reverse DMT';
			SET @TransDesc = 'DMT Balance Reverse Amount: ' + CONVERT(VARCHAR, @Amount) + ' Send To ' + @ReceiverName;
		END

		PRINT @DocType;

		EXEC dbo.TransactionDetailsInsertByUser @SenderID
			,@RESULT
			,18
			,@ReceiverID
			,''
			,--as Consumer no                 
			@DocType
			,@OpeningAmount
			,@ClosingAmount
			,@Amount --@TransactionAmount                                            
			,0
			,'DEBIT'
			,''
			,@TransDesc

		------TransactionDetails insert For Receiver---                
		SET @TransDesc = 'DMT Balance Receive Amount: ' + CONVERT(VARCHAR, @Amount) + ' From ' + @SenderName;
		SET @DocType = 'Balance Receive DMT'

		EXEC dbo.TransactionDetailsInsertByUser @ReceiverID
			,@RESULT
			,18
			,@SenderID
			,''
			,--as Consumer no                       
			@DocType
			,@OpeningBalanceReceiver
			,@CloseBalanceReceiver
			,@Amount --@TransactionAmount                                                   
			,0
			,'CREDIT'
			,''
			,@TransDesc

		RETURN (0);
	END
END

GO
/****** Object:  StoredProcedure [dbo].[BalanceTransferReceiveSelectDatewise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Fixed By Sushant on 14/11/2018  
-- [BalanceTransferReceiveSelectDatewise] 1, 0, '21/12/2018', '21/12/2018'  
CREATE PROC [dbo].[BalanceTransferReceiveSelectDatewise] @LoginID INT  
 ,@UserID INT  
 ,@FromDate VARCHAR(250)  
 ,@ToDate VARCHAR(250)  
AS  
BEGIN  
 DECLARE @BalanceTransferService INT = 18  
 DECLARE @UserTypeID INT  
  ,@SenderName VARCHAR(50);  
  
 SET @UserTypeID = (  
   SELECT UserTypeID  
   FROM UserInformation  
   WHERE UserID = @LoginID  
   )  
  
 DECLARE @tempDownline TABLE (UserID INT)  
  
 IF (@UserID = 0)  
 BEGIN  
  INSERT @tempDownline  
  SELECT *  
  FROM FuncUserInformationSelectDownlineTree(@LoginID)  
 END  
 ELSE  
 BEGIN  
  INSERT @tempDownline (UserID)  
  SELECT @UserID  
 END  
 --select * from @tempDownline
 DECLARE @Admin INT = 1;  
 DECLARE @SuperDistributor INT = 8;  
 DECLARE @Distributor INT = 2;  
 DECLARE @Retailer INT = 3;  
 DECLARE @FOS INT = 7;  
 DECLARE @APIUser INT = 4;  
 DECLARE @Support INT = 9;  
  
 SELECT TOP (1000) BT.BalanceTransferID  
  ,BT.SenderID  
  ,BT.ReceiverID  
  ,BT.Amount  
  ,convert(VARCHAR(12), BT.DOC, 113) + right(convert(VARCHAR(39), BT.DOC, 22), 11) AS DOC  
  ,UI.UserName AS Receiver  
  ,UI.UserTypeID AS RUserTypeID  
  ,UIS.UserName AS Sender  
  ,UIS.UserTypeID AS DUserTypeID  
  ,BT.Reason  
  ,CASE   
   WHEN (  
     BT.Type = 'REVERSE'  
     AND BT.SenderID = TD.UserID  
     )  
    THEN 'Balance Reverse - Send'  
   WHEN (  
     BT.Type = 'REVERSE'  
     AND BT.ReceiverID = TD.UserID  
     )  
    THEN 'Balance Reverse - Receive'  
   ELSE TD.DOCType  
   END AS DOCType  
  ,BT.BalanceType  
  ,CASE   
   WHEN (  
     TD.DOCType = 'Balance Transfer'  
     OR TD.DOCType = 'Balance Transfer: T'  
     OR TD.DOCType = 'Balance Reverse'  
     )  
    THEN BT.OpeningBalanceSender  
   ELSE BT.[OpeningBalanceReceiver]  
   END AS OpeningBalanceSender  
  ,CASE   
   WHEN (  
     TD.DOCType = 'Balance Receive'  
     --OR TD.DOCType = 'Balance Reverse'  
     )  
    THEN BT.[CloseBalanceReceiver]  
   ELSE BT.CloseBalanceSender  
   END AS CloseBalanceSender  
 FROM BalanceTransfer BT  
 INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID  
 INNER JOIN [TransactionDetails] TD ON TD.RechargeID = BT.BalanceTransferID  
 INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID  
 WHERE BT.Type IN (  
   'T'  
   ,'TRANSFER'  
   ,'REVERSE'  
   )  
  AND (  
   convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)  
   AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)  
   )  
  AND TD.ServiceID = @BalanceTransferService  
  AND TD.UserID IN (  
   SELECT UserID  
   FROM @tempDownline  
   )  
  AND TD.DOCType IN (  
   'Balance Transfer'  
   ,'Balance Receive'  
   ,'Balance Reverse'  
   ,'Balance Transfer: T'  
   --,'REVERSE'  
   ,'Balance Receive DMT'
   ,'Balance Transfer DMT'
   ,'Balance Reverse DMT'  
   )  
  AND (  
   BT.SenderID IN (  
    SELECT UserID  
    FROM @tempDownline  
    )  
   OR BT.ReceiverID IN (  
    SELECT UserID  
    FROM @tempDownline  
    )  
   )  
 ORDER BY BT.DOC DESC  
END  
GO
/****** Object:  StoredProcedure [dbo].[BalanceTransferSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :27-2-17
--Purpose Balance Transfer Report
--==========================================
--BalanceTransferSelect 2
CREATE PROCEDURE [dbo].[BalanceTransferSelect] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT
		,@SenderName VARCHAR(50);

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	--Admin Recharge Report
	IF (@UserTypeID = 1)
	BEGIN
		SELECT TOP (1000) BT.BalanceTransferID
			,BT.SenderID
			,BT.ReceiverID
			,BT.Amount
			,BT.OpeningBalanceSender AS OpeningBalance
			,BT.CloseBalanceSender AS ClosingBalance
			,convert(VARCHAR, BT.DOC, 113) AS DOC
			,UI.UserName AS Receiver
			,UI.UserTypeID AS RUserTypeID
			,UIS.UserName AS Sender
			,UIS.UserTypeID AS DUserTypeID
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID
		INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
		WHERE BT.Type IN (
				'T'
				,'B'
				)
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END

	IF (@UserTypeID = 2)
	BEGIN
		SELECT TOP (1000) BT.BalanceTransferID
			,BT.SenderID
			,BT.ReceiverID
			,BT.Amount
			,BT.OpeningBalanceSender AS OpeningBalance
			,BT.CloseBalanceSender AS ClosingBalance
			,convert(VARCHAR, BT.DOC, 113) AS DOC
			,UI.UserName AS Receiver
			,UI.UserTypeID AS RUserTypeID
			,UIS.UserName AS Sender
			,UIS.ParentID
			,UIS.UserTypeID AS DUserTypeID
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID
		INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
		WHERE @UserID IN (
				BT.SenderID
				,UIS.ParentID
				)
			AND BT.Type IN (
				'B'
				,'T'
				)
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceTransferSelectByID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
--modifay:Rahul Hembade  
--Reson: DMR Balance  
--=========================  
-- [BalanceTransferSelectByID] 2 ,'Recharge'  
CREATE PROCEDURE [dbo].[BalanceTransferSelectByID] @BalanceTransferID INT  
 ,@BalanceType VARCHAR(20)  
AS  
BEGIN  
 SELECT TOP (1000) BT.BalanceTransferID  
  ,BT.BalanceType  
  ,BT.SenderID  
  ,BT.ReceiverID  
  ,BT.Amount  
  ,BT.OpeningBalanceSender  
  ,BT.CloseBalanceSender  
  ,CAST(BT.DOC as varchar) AS DOC
  ,UI.UserName AS Receiver  
  ,UI.UserTypeID AS RUserTypeID  
  ,UT.UserType  
  ,UIS.MobileNumber  
  ,UI.EmailID AS TEmailID  
  ,UIS.EmailID AS DEmailID  
  ,BT.OpeningBalanceReceiver  
  ,BT.CloseBalanceReceiver  
  ,UI.MobileNumber AS TMobileNumber  
  ,UIS.UserName AS Sender  
  ,UIS.UserTypeID AS DUserTypeID  
 FROM BalanceTransfer BT  
 INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID  
 INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID  
 INNER JOIN UserTypeMaster UT ON UI.UserTypeID = UT.UserTypeID  
 WHERE BT.BalanceTransferID = @BalanceTransferID  
  AND BT.BalanceType = @BalanceType  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[BalanceTransferSelectCheckDuplicate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BalanceTransferSelectCheckDuplicate]
  @SenderUserID INT  
 ,@ReceiverUserID INT  
 ,@BalanceType VARCHAR(20)  
 ,@Amount DECIMAL(18, 5)  
 ,@Output VARCHAR(100) OUT  
AS  
BEGIN  
 IF EXISTS (  
   SELECT TOP (1) *  
   FROM dbo.BalanceTransfer(nolock)  
   WHERE SenderID = @SenderUserID  
    AND ReceiverID = @ReceiverUserID  
    AND Amount = @Amount  
    AND BalanceType = @BalanceType  
    AND convert(DATE, DOC, 103) = convert(DATE, getdate(), 103)  
   ORDER BY doc DESC  
   )  
 BEGIN  
  DECLARE @DateTimeOfTransfer DATETIME  
   
  
  SELECT TOP 1 @DateTimeOfTransfer = DOC  
  FROM dbo.BalanceTransfer(nolock)    
  WHERE SenderID = @SenderUserID  
   AND ReceiverID = @ReceiverUserID  
   AND Amount = @Amount  
   AND BalanceType = @BalanceType  
  ORDER BY doc DESC  
  
  DECLARE @HoursDiff INT  
  
  SELECT @HoursDiff = DATEDIFF(MINUTE, convert(DATETIME, @DateTimeOfTransfer, 103), convert(DATETIME, getdate(), 103))  
  
  PRINT @HoursDiff  
  
  IF (@HoursDiff <5)  -- 5 minitues
  BEGIN  
   SET @Output = 'Can not transfer same amount within 5 minutes.'  
  END  
  
   
 END  
 ELSE  
 BEGIN  
  SET @Output = 'OK'  
 END  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[BalanceTransferSelectDatewise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[BalanceTransferSelectDatewise] @UserID INT  
 ,@FromDate VARCHAR(250)  
 ,@ToDate VARCHAR(250)  
AS  
BEGIN  
 DECLARE @UserTypeID INT;  
 --,@SenderName VARCHAR(50);    
 DECLARE @Admin INT = 1;  
 DECLARE @SuperDistributor INT = 8;  
 DECLARE @Distributor INT = 2;  
 DECLARE @Retailer INT = 3;  
 DECLARE @FOS INT = 7;  
 DECLARE @APIUser INT = 4;  
 DECLARE @Support INT = 9;  
  
 SET @UserTypeID = (  
   SELECT UserTypeID  
   FROM UserInformation  
   WHERE UserID = @UserID  
   )  
  
 --Admin Recharge Report    
 IF (  
   @UserTypeID = @Admin  
   OR @UserTypeID = @Support  
   )  
 BEGIN  
  SELECT TOP (1000) BT.BalanceTransferID  
   ,BT.SenderID  
   ,BT.ReceiverID  
   ,BT.Amount  
   ,BT.OpeningBalanceSender  
   ,BT.CloseBalanceSender  
   ,convert(VARCHAR(12), BT.DOC, 113) + right(convert(VARCHAR(39), BT.DOC, 22), 11) AS DOC  
   ,UI.UserName AS Receiver  
   ,UI.UserTypeID AS RUserTypeID  
   ,UIS.UserName AS Sender  
   ,UIS.UserTypeID AS DUserTypeID  
   ,BT.Reason  
   ,BT.BalanceType  
  FROM BalanceTransfer BT  
  INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID  
  INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID  
  WHERE BT.Type IN (  
    'T'  
    ,'TRANSFER'  
    )  
   AND (  
    convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)  
    AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)  
    )  
  ORDER BY convert(DATETIME, BT.DOC, 103) DESC  
 END  
  
 --Distributor or Retailer or Client Transfer Report      
 IF (  
   @UserTypeID = @Distributor  
   OR @UserTypeID = @Retailer  
   OR @UserTypeID = @APIUser  
   OR @UserTypeID = @SuperDistributor  
   OR @UserTypeID = @FOS  
   )  
 BEGIN  
  SELECT TOP (1000) BT.BalanceTransferID  
   ,BT.SenderID  
   ,BT.ReceiverID  
   ,BT.Amount  
   ,BT.Reason  
   ,BT.OpeningBalanceSender  
   ,BT.CloseBalanceSender  
   ,convert(VARCHAR(12), BT.DOC, 113) + right(convert(VARCHAR(39), BT.DOC, 22), 11) AS DOC  
   ,UI.UserName AS Receiver  
   ,UI.UserTypeID AS RUserTypeID  
   ,UIS.UserName AS Sender  
   ,UIS.ParentID  
   ,UIS.UserTypeID AS DUserTypeID  
   ,BT.BalanceType  
  FROM BalanceTransfer BT  
  INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID  
  INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID  
  WHERE @UserID IN (BT.SenderID)  
   AND BT.Type IN (  
    'T'  
    ,'TRANSFER'  
    )  
   AND (  
    convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)  
    AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)  
    )  
  ORDER BY convert(DATETIME, BT.DOC, 103) DESC  
 END  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[BalanceTransferSelectDatewiseOnlyAdmin]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :27-2-17
--Purpose Balance Transfer Report Datewise
--==========================================
--BalanceTransferSelect 2
CREATE PROCEDURE [dbo].[BalanceTransferSelectDatewiseOnlyAdmin] @UserID INT
	,@FromDate VARCHAR(250)
	,@ToDate VARCHAR(250)
AS
BEGIN
	--Admin Recharge Report
	IF (@UserID = 0)
	BEGIN
		SELECT TOP (1000) BT.BalanceTransferID
			,BT.SenderID
			,BT.ReceiverID
			,BT.Amount
			,BT.Reason
			,BT.OpeningBalanceSender
			,BT.CloseBalanceSender
			,convert(VARCHAR(12), BT.DOC, 113) + right(convert(VARCHAR(39), BT.DOC, 22), 11) AS DOC
			,UI.UserName AS Receiver
			,UI.UserTypeID AS RUserTypeID
			,UIS.UserName AS Sender
			,UIS.UserTypeID AS DUserTypeID
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID
		INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
		WHERE BT.Type IN ('T')
			AND (
				convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
			AND BT.SenderID = 1
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END
	ELSE
	BEGIN
		SELECT TOP (1000) BT.BalanceTransferID
			,BT.SenderID
			,BT.ReceiverID
			,BT.Amount
			,BT.Reason
			,BT.OpeningBalanceSender
			,BT.CloseBalanceSender
			,convert(VARCHAR(12), BT.DOC, 113) + right(convert(VARCHAR(39), BT.DOC, 22), 11) AS DOC
			,UI.UserName AS Receiver
			,UI.UserTypeID AS RUserTypeID
			,UIS.UserName AS Sender
			,UIS.UserTypeID AS DUserTypeID
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID
		INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
		WHERE BT.Type IN ('T')
			AND (
				convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
			AND BT.SenderID = 1
			AND BT.ReceiverID = @UserID
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceTransferSelectReceive]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :27-2-17
--Purpose Balance Receive Report
--==========================================
--BalanceTransferSelectReceive 3
CREATE PROCEDURE [dbo].[BalanceTransferSelectReceive] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT;

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	--Admin Recharge Report
	IF (@UserTypeID = 1)
	BEGIN
		SELECT BT.BalanceTransferID
			,BT.SenderID
			,BT.Amount
			,BT.CloseBalanceReceiver AS ClosingBalance
			,BT.OpeningBalanceReceiver AS OpeningBalance
			,convert(VARCHAR, BT.DOC, 113) AS DOC
			,UI.UserName AS Sender
			,BT.ReceiverID
			,UIR.UserName AS Receiver
			,UI.UserTypeID AS DUserTypeID
			,UIR.UserTypeID AS RUserTypeID
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.SenderID = UI.UserID
		INNER JOIN UserInformation UIR ON BT.ReceiverID = UIR.UserID
		WHERE BT.Type IN (
				'B'
				,'T'
				)
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END

	--Distributor Recharge Report
	IF (@UserTypeID = 2)
	BEGIN
		SELECT BT.BalanceTransferID
			,BT.SenderID
			,BT.Amount
			,BT.CloseBalanceReceiver AS ClosingBalance
			,BT.OpeningBalanceReceiver AS OpeningBalance
			,convert(VARCHAR, BT.DOC, 113) AS DOC
			,UI.UserName AS Sender
			,BT.ReceiverID
			,UIR.UserName AS Receiver
			,UI.UserTypeID AS DUserTypeID
			,UIR.UserTypeID AS RUserTypeID
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.SenderID = UI.UserID
		INNER JOIN UserInformation UIR ON BT.ReceiverID = UIR.UserID
		WHERE @UserID IN (
				BT.ReceiverID
				,UIR.ParentID
				)
			AND BT.Type IN (
				'B'
				,'T'
				)
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END

	--Retailer Recharge Report
	IF (@UserTypeID = 3)
	BEGIN
		SELECT BT.BalanceTransferID
			,BT.SenderID
			,BT.Amount
			,BT.CloseBalanceReceiver AS ClosingBalance
			,BT.OpeningBalanceReceiver AS OpeningBalance
			,convert(VARCHAR, BT.DOC, 113) AS DOC
			,UI.UserName AS Sender
			,BT.ReceiverID
			,UIR.UserName AS Receiver
			,UI.UserTypeID AS DUserTypeID
			,UIR.UserTypeID AS RUserTypeID
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.SenderID = UI.UserID
		INNER JOIN UserInformation UIR ON BT.ReceiverID = UIR.UserID
		WHERE BT.ReceiverID = @UserID
			AND BT.Type IN (
				'B'
				,'T'
				)
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceTransferSelectReceiveDatewise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BalanceTransferSelectReceiveDatewise] @UserID INT
	,@FromDate VARCHAR(250)
	,@ToDate VARCHAR(250)
AS
BEGIN
	DECLARE @UserTypeID INT;
	DECLARE @Admin INT = 1;
	DECLARE @SuperDistributor INT = 8;
	DECLARE @Distributor INT = 2;
	DECLARE @Retailer INT = 3;
	DECLARE @FOS INT = 7;
	DECLARE @APIUser INT = 4;
	DECLARE @Support INT = 9;

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	--Admin Recharge Report          
	IF (
			@UserTypeID = @Admin
			OR @UserTypeID = @Support
			)
	BEGIN
		SELECT BT.BalanceTransferID
			,BT.SenderID
			,BT.Amount
			,BT.CloseBalanceReceiver
			,BT.OpeningBalanceReceiver
			,convert(VARCHAR(12), BT.DOC, 113) + right(convert(VARCHAR(39), BT.DOC, 22), 11) AS DOC
			,UI.UserName AS Sender
			,BT.ReceiverID
			,BT.Reason
			,UIR.UserName AS Receiver
			,UI.UserTypeID AS DUserTypeID
			,UIR.UserTypeID AS RUserTypeID
			,UBR.OutStanding
			,BT.BalanceType
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.SenderID = UI.UserID
		INNER JOIN UserInformation UIR ON BT.ReceiverID = UIR.UserID
		INNER JOIN UserBalance UBR ON BT.ReceiverID = UBR.UserID
		WHERE BT.Type IN (
				'T'
				,'RE'
				,'TRANSFER'
				,'REVERSE'
				)
			AND (
				convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END

	--Distributor Recharge Report          
	IF (
			@UserTypeID = @Distributor
			OR @UserTypeID = @FOS
			OR @UserTypeID = @Retailer
			OR @UserTypeID = @APIUser
			OR @UserTypeID = @SuperDistributor
			)
	BEGIN
		SELECT BT.BalanceTransferID
			,BT.SenderID
			,BT.Amount
			,BT.Reason
			,BT.CloseBalanceReceiver
			,BT.OpeningBalanceReceiver
			,convert(VARCHAR(12), BT.DOC, 113) + right(convert(VARCHAR(39), BT.DOC, 22), 11) AS DOC
			,UI.UserName AS Sender
			,BT.ReceiverID
			,UIR.UserName AS Receiver
			,UI.UserTypeID AS DUserTypeID
			,UIR.UserTypeID AS RUserTypeID
			,UBR.OutStanding
			,BT.BalanceType
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.SenderID = UI.UserID
		INNER JOIN UserInformation UIR ON BT.ReceiverID = UIR.UserID
		INNER JOIN UserBalance UBR ON BT.ReceiverID = UBR.UserID
		WHERE @UserID IN (BT.ReceiverID)
			AND BT.Type IN (
				'T'
				,'RE'
				,'TRANSFER'
				,'REVERSE'
				)
			AND (
				convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END
			--Retailer Recharge Report          
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceTransferSelectReceiveUserwise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- [BalanceTransferSelectReceiveUserwise] 2
CREATE PROCEDURE[dbo].[BalanceTransferSelectReceiveUserwise] @UserID INT  
AS  
BEGIN  
 SELECT TOP (10) BT.BalanceTransferID  
  ,BT.SenderID  
  ,BT.ReceiverID  
  ,BT.Amount  
  ,BT.Reason  
  ,BT.BalanceType
  ,BT.[CloseBalanceReceiver]  
  ,BT.[OpeningBalanceReceiver]  
  ,convert(VARCHAR(12), BT.DOC, 113) + right(convert(VARCHAR(39), BT.DOC, 22), 11) AS DOC  
  ,UI.UserName AS Receiver  
  ,UI.UserTypeID AS RUserTypeID  
  ,UIS.UserName AS Sender  
  ,UIS.UserTypeID AS DUserTypeID
  ,0 As Outstanding  
 FROM BalanceTransfer BT  
 INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID  
 INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
 --INNER JOIN UserBalance UB on UB.UserID =  @UserID 
 WHERE BT.Type IN ('RE', 'REVERSE')  
  AND BT.ReceiverID = @UserID  
 ORDER BY convert(DATETIME, BT.DOC, 103) DESC  
END  


GO
/****** Object:  StoredProcedure [dbo].[BalanceTransferSelectReportByUserID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BalanceTransferSelectReportByUserID] @UserID INT
	,@SenderID INT
AS
BEGIN
	SELECT BT.BalanceTransferID
		,BT.SenderID
		,BT.ReceiverID
		,BT.Amount
		,BT.Reason
		,BT.OpeningBalanceSender
		,BT.CloseBalanceSender
		,convert(VARCHAR(12), BT.DOC, 113) + right(convert(VARCHAR(39), BT.DOC, 22), 11) AS DOC
		,UI.UserName AS Receiver
		,UI.UserTypeID AS RUserTypeID
		,UIS.UserName AS Sender
		,UIS.UserTypeID AS DUserTypeID
		,BT.Type
		,BT.BalanceType
	FROM BalanceTransfer BT
	INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID
	INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
	WHERE BT.Type IN (
			'T'
			,'TRANSFER'
			)
		AND BT.SenderID = @UserID
		AND BT.ReceiverID = @SenderID
	ORDER BY convert(DATETIME, BT.DOC, 103) DESC
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceTransferSelectUserwise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BalanceTransferSelectUserwise] @UserID INT
AS
BEGIN
	SELECT TOP (10) BT.BalanceTransferID
		,BT.SenderID
		,BT.ReceiverID
		,BT.Amount
		,BT.Reason
		,BT.OpeningBalanceSender
		,BT.CloseBalanceSender
		,convert(VARCHAR(12), BT.DOC, 113) + right(convert(VARCHAR(39), BT.DOC, 22), 11) AS DOC
		,UI.UserName AS Receiver
		,UI.UserTypeID AS RUserTypeID
		,UIS.UserName AS Sender
		,UIS.UserTypeID AS DUserTypeID
	FROM BalanceTransfer BT
	INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID
	INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
	WHERE BT.Type IN ('T')
		AND BT.SenderID = @UserID
	ORDER BY convert(DATETIME, BT.DOC, 103) DESC
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceWalletSelectReceive]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :27-2-17
--Purpose Balance Receive Report
--==========================================
--BalanceWalletSelectReceive 3,'W'
CREATE PROCEDURE [dbo].[BalanceWalletSelectReceive] @UserID INT
	,@FromDate VARCHAR(250)
	,@ToDate VARCHAR(250)
AS
BEGIN
	DECLARE @UserTypeID INT;

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	--Admin Recharge Report
	IF (@UserTypeID = 1)
	BEGIN
		SELECT BT.BalanceTransferID
			,BT.SenderID
			,BT.Amount
			,BT.CloseBalanceReceiver AS ClosingBalance
			,BT.OpeningBalanceReceiver AS OpeningBalance
			,convert(VARCHAR, BT.DOC, 113) AS DOC
			,UI.UserName AS Sender
			,BT.ReceiverID
			,UIR.UserName AS Receiver
			,UI.UserTypeID AS DUserTypeID
			,UIR.UserTypeID AS RUserTypeID
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.SenderID = UI.UserID
		INNER JOIN UserInformation UIR ON BT.ReceiverID = UIR.UserID
		WHERE BT.Type IN ('W')
			AND (
				convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END

	--Distributor Recharge Report
	IF (@UserTypeID = 2)
	BEGIN
		SELECT BT.BalanceTransferID
			,BT.SenderID
			,BT.Amount
			,BT.CloseBalanceReceiver AS ClosingBalance
			,BT.OpeningBalanceReceiver AS OpeningBalance
			,convert(VARCHAR, BT.DOC, 113) AS DOC
			,UI.UserName AS Sender
			,BT.ReceiverID
			,UIR.UserName AS Receiver
			,UI.UserTypeID AS DUserTypeID
			,UIR.UserTypeID AS RUserTypeID
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.SenderID = UI.UserID
		INNER JOIN UserInformation UIR ON BT.ReceiverID = UIR.UserID
		WHERE @UserID IN (
				BT.ReceiverID
				,UIR.ParentID
				)
			AND BT.Type IN ('W')
			AND (
				convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END

	--Retailer Recharge Report
	IF (@UserTypeID = 3)
	BEGIN
		SELECT BT.BalanceTransferID
			,BT.SenderID
			,BT.Amount
			,BT.CloseBalanceReceiver AS ClosingBalance
			,BT.OpeningBalanceReceiver AS OpeningBalance
			,convert(VARCHAR, BT.DOC, 113) AS DOC
			,UI.UserName AS Sender
			,BT.ReceiverID
			,UIR.UserName AS Receiver
			,UI.UserTypeID AS DUserTypeID
			,UIR.UserTypeID AS RUserTypeID
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.SenderID = UI.UserID
		INNER JOIN UserInformation UIR ON BT.ReceiverID = UIR.UserID
		WHERE BT.ReceiverID = @UserID
			AND BT.Type IN ('W')
			AND (
				convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceWalletTransferSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :27-2-17
--Purpose Balance Transfer Report
--==========================================
--BalanceWalletTransferSelect 3
CREATE PROCEDURE [dbo].[BalanceWalletTransferSelect] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT
		,@SenderName VARCHAR(50);

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	--Admin Recharge Report
	IF (@UserTypeID = 1)
	BEGIN
		SELECT TOP (1000) BT.BalanceTransferID
			,BT.SenderID
			,BT.ReceiverID
			,BT.Amount
			,BT.OpeningBalanceSender AS OpeningBalance
			,BT.CloseBalanceSender AS ClosingBalance
			,convert(VARCHAR, BT.DOC, 113) AS DOC
			,UI.UserName AS Receiver
			,UI.UserTypeID AS RUserTypeID
			,UIS.UserTypeID AS DUserTypeID
			,UIS.UserName AS Sender
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID
		INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
		WHERE BT.Type IN ('W')
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END

	IF (@UserTypeID = 3)
	BEGIN
		SELECT TOP (1000) BT.BalanceTransferID
			,BT.SenderID
			,BT.ReceiverID
			,BT.Amount
			,BT.OpeningBalanceSender AS OpeningBalance
			,BT.CloseBalanceSender AS ClosingBalance
			,convert(VARCHAR, BT.DOC, 113) AS DOC
			,UI.UserName AS Receiver
			,UI.UserTypeID AS RUserTypeID
			,UIS.UserTypeID AS DUserTypeID
			,UIS.UserName AS Sender
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID
		INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
		WHERE @UserID IN (BT.SenderID)
			AND BT.Type IN ('W')
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[BalanceWalletTransferSelectDateWise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================  
--Auther :Priyanka  
--Date :16-8-17  
--Purpose Balance Transfer Report  
--==========================================  
--BalanceWalletTransferSelect 3  
CREATE PROCEDURE [dbo].[BalanceWalletTransferSelectDateWise] @UserID INT
	,@FromDate VARCHAR(250)
	,@ToDate VARCHAR(250)
AS
BEGIN
	DECLARE @UserTypeID INT
		,@SenderName VARCHAR(50);

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	--Admin Recharge Report  
	IF (@UserTypeID = 1)
	BEGIN
		SELECT TOP (1000) BT.BalanceTransferID
			,BT.SenderID
			,BT.ReceiverID
			,BT.Amount
			,BT.OpeningBalanceSender AS OpeningBalance
			,BT.CloseBalanceSender AS ClosingBalance
			,convert(VARCHAR, BT.DOC, 113) AS DOC
			,UI.UserName AS Receiver
			,UI.UserTypeID AS RUserTypeID
			,UIS.UserTypeID AS DUserTypeID
			,UIS.UserName AS Sender
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID
		INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
		WHERE BT.Type IN ('W')
			AND (
				convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END

	IF (
			@UserTypeID = 3
			OR @UserTypeID = 4
			)
	BEGIN
		SELECT TOP (1000) BT.BalanceTransferID
			,BT.SenderID
			,BT.ReceiverID
			,BT.Amount
			,BT.OpeningBalanceSender AS OpeningBalance
			,BT.CloseBalanceSender AS ClosingBalance
			,convert(VARCHAR, BT.DOC, 113) AS DOC
			,UI.UserName AS Receiver
			,UI.UserTypeID AS RUserTypeID
			,UIS.UserTypeID AS DUserTypeID
			,UIS.UserName AS Sender
		FROM BalanceTransfer BT
		INNER JOIN UserInformation UI ON BT.ReceiverID = UI.UserID
		INNER JOIN UserInformation UIS ON BT.SenderID = UIS.UserID
		WHERE @UserID IN (BT.SenderID)
			AND BT.Type IN ('W')
			AND (
				convert(DATE, BT.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, BT.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, BT.DOC, 103) DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[Bank_DTLCheckIFSC]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[Bank_DTLCheckIFSC] @IFSC VARCHAR(11)
AS
BEGIN
	IF EXISTS (
			SELECT *
			FROM [dbo].[Bank_DTL]
			WHERE IFSC = @IFSC
			)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END



GO
/****** Object:  StoredProcedure [dbo].[Bank_DTLSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[Bank_DTLSelect]
AS
BEGIN
	SELECT BankID
		,BankName
		,IFSC
	FROM [dbo].[Bank_DTL]
END



GO
/****** Object:  StoredProcedure [dbo].[Bank_DTLSelectByID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE[dbo].[Bank_DTLSelectByID] @BankID INT  
AS  
BEGIN  
 SELECT *  
 FROM [dbo].[Bank_DTL]  
 WHERE BankID = @BankID  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[BankDetailsDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BankDetailsDelete] @BankID INT
AS
BEGIN
	DELETE
	FROM BankDetails
	WHERE BankID = @BankID
END



GO
/****** Object:  StoredProcedure [dbo].[BankDetailsInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[BankDetailsInsert] @BankName VARCHAR(250)

	,@AccountNo VARCHAR(50)

	,@IFSCCode VARCHAR(50)

	,@AccountName VARCHAR(250)

	,@BranchName VARCHAR(250)

AS

BEGIN


if Not Exists(select BankID from BankDetails where AccountNo=@AccountNo)
Begin
	INSERT INTO BankDetails (

		BankName

		,AccountNo

		,IFSCCode

		,AccountName

		,BranchName

		)

	VALUES (

		@BankName

		,@AccountNo

		,@IFSCCode

		,@AccountName

		,@BranchName

		)

End
END



GO
/****** Object:  StoredProcedure [dbo].[BankDetailsSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BankDetailsSelect] @BankID INT = 0
AS
BEGIN
	SELECT BankID
		,BankName
		,AccountNo
		,IFSCCode
		,AccountName
		,BranchName
	FROM BankDetails
END



GO
/****** Object:  StoredProcedure [dbo].[BankDetailsSelectByBankID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  create PROCEDURE[dbo].[BankDetailsSelectByBankID] @BankID INT  
AS  
BEGIN  
 SELECT [BankName]
      ,[AccountNo]
      ,[IsActive]
      ,[IFSCCode]
      ,[AccountName]
      ,[BranchName]  
 FROM [dbo].[BankDetails]  
 WHERE BankID = @BankID  
END  
  
GO
/****** Object:  StoredProcedure [dbo].[BankDetailsUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BankDetailsUpdate] @BankID INT
	,@BankName VARCHAR(50)
	,@AccountNo VARCHAR(50)
	,@IFSCCode VARCHAR(50)
	,@AccountName VARCHAR(50)
	,@BranchName VARCHAR(50)
AS
BEGIN
	UPDATE BankDetails
	SET AccountNo = @AccountNo
		,IFSCCode = @IFSCCode
		,AccountName = @AccountName
		,BranchName = @BranchName
	WHERE BankID = @BankID
END



GO
/****** Object:  StoredProcedure [dbo].[BeneficiaryCheckAccountExist]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[BeneficiaryCheckAccountExist] @Mobile VARCHAR(10)
	,@AccountNo VARCHAR(18)
	,@IFSC VARCHAR(11)
AS
BEGIN
	DECLARE @SenderID INT

	SET @SenderID = 0

	SELECT @SenderID = SenderID
	FROM [dbo].[Sender]
	WHERE Mobile = @Mobile

	IF EXISTS (
			SELECT *
			FROM [dbo].[Beneficiary]
			WHERE [SenderID] = @SenderID
				AND [AccountNo] = @AccountNo
				AND IFSC = @IFSC
				AND IsVerifyOTP = 1
				AND Isdelete = 0
			)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END



GO
/****** Object:  StoredProcedure [dbo].[BeneficiaryDeleteByID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[BeneficiaryDeleteByID] @BeneficiaryID INT
AS
BEGIN
	UPDATE [dbo].[Beneficiary]
	SET Isdelete = 1
	WHERE BeneficiaryID = @BeneficiaryID
END



GO
/****** Object:  StoredProcedure [dbo].[BeneficiaryInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:  Sushant Yelpale  
-- Create date: 9 feb 2019  
-- Description: Insert Beneficiary Against Sender  
-- =============================================  
CREATE PROCEDURE [dbo].[BeneficiaryInsert]  
 @SenderId Varchar(50)  
 ,@AccountNumber Varchar(50)  
 ,@IFSC Varchar(50)  
 ,@Bank Varchar(50)  
 ,@FirstName Varchar(50)  
 ,@LastName Varchar(50)  
 ,@BeneficiaryId Varchar(50) = '' OUT  
AS  
BEGIN  
 Declare @PresentBeneficiaryId Varchar(50)  
 SELECT Top 1 @PresentBeneficiaryId = Convert(Varchar(50), BeneficiaryId)  
 from Beneficiary   
 where AccountNumber = @AccountNumber   
  
 IF(@PresentBeneficiaryId is Null)  
 BEGIN  
  INSERT INTO [dbo].[Beneficiary]  
           ([FirstName]  
           ,[LastName]  
           ,[AccountNumber]  
           ,[IFSC]  
           ,[BankName])  
  VALUES  
           (@FirstName  
           ,@LastName  
           ,@AccountNumber  
           ,@IFSC  
           ,@Bank)  

  SELECT @BeneficiaryId = BeneficiaryId
  from Beneficiary
  where AccountNumber = @AccountNumber
  
  INSERT INTO [dbo].[SenderBeneficiaryMapping]  
      ([SenderId]  
      ,[BeneficiaryId])  
  VALUES  
      (@SenderId  
      ,@BeneficiaryId)  
  RETURN;  
 END  
  
 SET @BeneficiaryId = @PresentBeneficiaryId;  
  
 Declare @SenderBeneficiaryMappingId int;  
 Declare @IsActive int;  
 Declare @IsVerified bit  
 SELECT TOp 1 @SenderBeneficiaryMappingId = SBM.[SenderBeneficiaryMappingId]  
 ,@IsActive = SBM.IsActive  
 ,@IsVerified = B.IsActive  
 from [SenderBeneficiaryMapping] SBM  
 INNER JOIN Beneficiary B ON SBM.BeneficiaryId = Convert(Varchar(50), B.BeneficiaryId)  
 where SBM.SenderId = @SenderId  
 AND SBM.BeneficiaryId = @BeneficiaryId   
  
 IF(@SenderBeneficiaryMappingId Is Null)  
 BEGIN  
  INSERT INTO [dbo].[SenderBeneficiaryMapping]  
      ([SenderId]  
      ,[BeneficiaryId])  
  VALUES  
      (@SenderId  
      ,@BeneficiaryId)  
  RETURN;  
 END  
  
 IF(@SenderBeneficiaryMappingId Is NOT Null AND @IsActive = 0)  
 BEGIN  
  update [dbo].[SenderBeneficiaryMapping]  
  SET IsActive = 1  
  where SenderId = @SenderId  
  AND BeneficiaryId = @BeneficiaryId   
  
  RETURN;  
 END  
END  
GO
/****** Object:  StoredProcedure [dbo].[BeneficiarySelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		Sushant yelpale
-- Create date: 9 Feb 2019
-- Description:	Select Single beneficiary by Sender and Beneficiary
-- =============================================
CREATE PROCEDURE [dbo].[BeneficiarySelect]
	@SenderId varchar(50)
	,@BeneficiaryId Varchar(50)
AS
BEGIN
	SELECT TOP 1 B.[BeneficiaryId]
	  ,@SenderId As SenderId
      ,B.[FirstName]
      ,B.[LastName]
      ,B.[AccountNumber]
      ,B.[IFSC]
      ,B.[BankName] 
      ,SBM.[DOC]
      ,B.[IsVerified]
      ,SBM.[IsActive]
  FROM [dbo].[Beneficiary] B
  INNER JOIN [dbo].[SenderBeneficiaryMapping] SBM 
  ON SBM.[BeneficiaryId] = Convert(Varchar(50) ,B.[BeneficiaryId])
  AND SBM.SenderId = @SenderId
  AND SBM.IsActive = 1
END

GO
/****** Object:  StoredProcedure [dbo].[BeneficiarySelectBySender]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:  Sushant Yelpale  
-- Create date: 9 Feb 2019  
-- Description: Select Active Beneficiary by Sender id   
-- =============================================  
CREATE PROCEDURE [dbo].[BeneficiarySelectBySender]  
 @SenderId Varchar(50)  
 ,@BeneficiaryAccountNumber Varchar(50)  
AS  
BEGIN  
 SELECT TOP 1 B.[BeneficiaryId]  
      ,B.[FirstName]  
      ,B.[LastName]  
      ,B.[AccountNumber]  
      ,B.[IFSC]  
      ,B.[BankName]  
      ,B.[DOC]  
      ,B.[IsVerified]  
      ,SBM.[IsActive]  
 from Beneficiary B  
 INNER JOIN [dbo].[SenderBeneficiaryMapping] SBM ON SBM.[BeneficiaryId] = B.[BeneficiaryId]  
 AND SBM.SenderId = @SenderId  
 WHERE B.[AccountNumber] = @BeneficiaryAccountNumber  
 AND SBM.[IsActive] = 1  
END  
GO
/****** Object:  StoredProcedure [dbo].[BeneficiarySelectBySenderNoAccNo]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[BeneficiarySelectBySenderNoAccNo] @SenderNo VARCHAR(10)
	,@AccountNo VARCHAR(20)
AS
BEGIN
	DECLARE @SenderID INT;

	SELECT @SenderID = SenderID
	FROM sender
	WHERE [Mobile] = @SenderNo

	SELECT *
	FROM [dbo].[Beneficiary]
	WHERE SenderID = @SenderID
		AND AccountNo = @AccountNo
		AND IsVerifyOTP = 1
		--AND Isdelete = 0
END



GO
/****** Object:  StoredProcedure [dbo].[BeneficiaryVerifyAccountByID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[BeneficiaryVerifyAccountByID] @BeneficiaryID INT
	,@BeneficiaryName VARCHAR(100)
AS
BEGIN
	UPDATE [dbo].[Beneficiary]
	SET IsVerified = 1
		,name = @BeneficiaryName
	WHERE BeneficiaryID = @BeneficiaryID
END



GO
/****** Object:  StoredProcedure [dbo].[CallBackResponseInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  --==========================================          
--Auther :Sushant Yelpale  
--Date  :18/07/2018  
--Purpose :Insert Callback response parameters to database  
--==========================================     
--  
CREATE proc [dbo].[CallBackResponseInsert]       
 @RequestID Varchar(100)  
 ,@Status varchar(50)  
 ,@ConsumerNo varchar(100)  
 ,@Amount int  
 ,@TransID Varchar(100)  
 ,@ResponseText Varchar(Max)  
as          
begin          
 INSERT INTO [dbo].[CallBackResponse]  
           ([RequestID]  
           ,[Status]  
           ,[ConsumerNo]  
           ,[Amount]  
           ,[TransID]  
           ,[ResponseText])  
     VALUES  
           (@RequestID  
           ,@Status  
           ,@ConsumerNo  
           ,@Amount  
           ,@TransID  
           ,@ResponseText)  
end          
      
      
    
    
    
GO
/****** Object:  StoredProcedure [dbo].[CallBackResponseSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:  Sushant Yelpale  
-- Create date: 18/07/2018  
-- Description: Selects callback response  
-- [CallBackResponseSelect] '10/07/2018', '19/07/2018', 0, 100  
-- =============================================  
CREATE PROCEDURE [dbo].[CallBackResponseSelect]  
@FromDate Varchar(20)  
,@ToDate varchar(20)  
,@ProviderID int  
,@Records int  
AS  
BEGIN  
  
select TOP(@Records)  
 R.RechargeProviderID  
 ,RPM.RechargeProviderName  
 ,CR.[RequestID]  
 ,CR.[Status]  
 ,CR.[ConsumerNo]  
 ,CR.[Amount]  
 ,CR.[TransID]   
 ,CR.[DOC]  
 ,CR.[ResponseText]   
from [CallBackResponse] CR  
inner Join Recharge R ON CR.RequestID = R.RequestID  
inner Join RechargeProviderMaster RPM ON RPM.RechargeProviderID = R.RechargeProviderID  
where CR.Status in ('FAILURE', 'SUCCESS')  
AND (convert(date, CR.DOC, 103) >= convert(date, @FromDate, 103)  
 and convert(date, CR.DOC, 103) <= convert(date, @ToDate, 103))  
AND ((@ProviderID != 0 AND R.RechargeProviderID = @ProviderID)  
 OR (@ProviderID = 0))  
order by DOC ASC  
END  
  
--[CallBackResponseSelect] '31/07/2018', '31/07/2018', 0, 100
GO
/****** Object:  StoredProcedure [dbo].[CheckDuplicateRequestID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[CheckDuplicateRequestID] @RequestID VARCHAR(50)
AS
BEGIN
	SELECT *
	FROM Recharge(NOLOCK)
	WHERE APIRequestID = @RequestID
END



GO
/****** Object:  StoredProcedure [dbo].[checkRechargeAPIOperatorValidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================    
-- Author:  Sushant yelpale    
-- ALTER  date: 31/07/2018     
-- Description: Check operator if off for perticular API Sequence    
-- checkRechargeAPIOperatorValidation 1,2,2, @OUT    
-- =============================================    
CREATE PROCEDURE [dbo].[checkRechargeAPIOperatorValidation] @SERVICEID INT  
 ,@OPERATORID INT  
 ,@PROVIDERID INT  
 ,@OUTPUT VARCHAR(500) OUTPUT  
AS  
BEGIN  
 DECLARE @IsActiveOperator BIT = 0;  
 DECLARE @IsActiveService BIT = 0;  
  
 SET @OUTPUT = 'OK';  
  
 IF NOT EXISTS (  
   SELECT TOP (1) IsActive  
   FROM dbo.RechargeProviderOperatorMaster
   WHERE ServiceID = @SERVICEID  
    AND OperatorID = @OPERATORID  
    AND RechargeProviderID = @PROVIDERID  
    AND IsActive = 1  
   )  
 BEGIN  
  SET @OUTPUT = 'Operator is OFF, Please contact to admin'  
 END  
  
 IF NOT EXISTS (  
   SELECT TOP (1) IsActive  
   FROM dbo.RechargeProviderServices  
   WHERE RechargeProviderID = @PROVIDERID  
    AND ServiceID = @ServiceID  
    AND IsActive = 1  
   )  
 BEGIN  
  SET @OUTPUT = 'Service is OFF, Please contact to admin.';  
 END  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[CheckRechargeStatus]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[CheckRechargeStatus] @RequestID VARCHAR(50)
	,@UserID INT
AS
BEGIN
	SELECT *
	FROM Recharge(NOLOCK)
	WHERE APIRequestID = @RequestID
		AND UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[CheckRechargeValidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CheckRechargeValidation] @USERID INT  
 ,@SERVICEID VARCHAR(10)  
 ,@CONSUMERNO VARCHAR(20)  
 ,@AMOUNT NUMERIC(18, 5)  
 ,@OPERATORID INT  
 ,@OUTPUT VARCHAR(500) OUT  
AS  
BEGIN  
 DECLARE @IsServiceOn INT = 0  
 DECLARE @MoneyTransferService INT = 21;  
 DECLARE @BeneVerificationOperratorID INT = 45;  
 DECLARE @ElectricityServiceID INT = 14;  
  
 SET NOCOUNT ON  
  
 -- Check Service is On/Off (Start) -------                        
 SELECT @IsServiceOn = COUNT(ServiceID)  
 FROM dbo.ServiceMaster(NOLOCK)  
 WHERE IsDeleted = 0  
  AND ServiceID = @SERVICEID  
  
 IF @IsServiceOn <= 0  
 BEGIN  
  PRINT 'Service is off, please contact to Admin';  
  
  SET @OUTPUT = 'Service is off, please contact to Admin';  
  
  RETURN (0);  
 END  
  
 -- User-service                         
 -- Service balance                        
 -- Check Service is On/Off (End) -------                        
 DECLARE @IsOperatorOn INT = 0  
  
 SELECT @IsOperatorOn = COUNT(OperatorID)  
 FROM DBO.OperatorMaster(NOLOCK)  
 WHERE OperatorID = @OPERATORID  
  AND IsOn = 1  
  
 IF @IsOperatorOn <= 0  
 BEGIN  
  PRINT 'Operator is off, please contact to Admin';  
  
  SET @OUTPUT = 'Operator is off, please contact to Admin';  
  
  RETURN (0);  
 END  
  
 DECLARE @MinAmount NUMERIC(18, 5) = 0.0  
 DECLARE @MaxAmount NUMERIC(18, 5) = 0.0  
  
 SELECT @MinAmount = MinimumRechargeAmount  
  ,@MaxAmount = MaximumRechargeAmount  
 FROM dbo.OperatorMaster(NOLOCK)  
 WHERE OperatorID = @OPERATORID  
  
 IF (  
   @Amount < @MinAmount  
   OR @Amount > @MaxAmount  
   )  
 BEGIN  
  PRINT 'Invalid recharge amount';  
  
  SET @OUTPUT = 'Invalid recharge amount';  
  
  RETURN (0);  
 END  
  
 DECLARE @UserWiseOperatorCount INT = 0  
  
 SELECT @UserWiseOperatorCount = COUNT(UserCommissionID)  
 FROM UserCommission(NOLOCK)  
 WHERE UserID = @USERID  
  AND OperatorId = @OPERATORID  
  
 IF (@UserWiseOperatorCount >= 1)  
 BEGIN  
  SELECT @UserWiseOperatorCount = COUNT(UserCommissionID)  
  FROM UserCommission(NOLOCK)  
  WHERE UserID = @USERID  
   AND OperatorId = @OPERATORID  
   AND IsOn = 1  
  
  IF (@UserWiseOperatorCount <= 0)  
  BEGIN  
   PRINT 'Operator is disabled for you, please contact to Admin';  
  
   SET @OUTPUT = 'Operator is disabled for you, please contact to Admin';  
  
   RETURN (0);  
  END  
 END  
  
 -----Added By sdy - check userwise table for Operator -----                  
 DECLARE @UserWiseOperatorCount1 INT = 0  
  
 IF EXISTS (  
   SELECT operatorID  
   FROM [dbo].OperatorUserwise(NOLOCK)  
   WHERE UserID = @USERID  
    AND [OperatorID] = @OPERATORID  
   )  
 BEGIN  
  ------ Check For Userwise Operator --------                  
  SELECT @UserWiseOperatorCount1 = COUNT(operatorID)  
  FROM [dbo].OperatorUserwise(NOLOCK)  
  WHERE UserID = @USERID  
   AND [OperatorID] = @OPERATORID  
   AND [IsOn] = 1  
   AND [IsDelete] = 0  
  
  IF @UserWiseOperatorCount1 <= 0  
  BEGIN  
   PRINT 'Your Operator is disabled, please contact to Admin';  
  
   SET @OUTPUT = 'Your Operator is disabled, please contact to Admin';  
  
   RETURN (0);  
  END  
  
  ------ Check For Userwise UsageLimit --------                  
  DECLARE @DailyUsageLimit DECIMAL(18, 5) = 0;  
  
  SELECT @DailyUsageLimit = [UsageLimit]  
  FROM [dbo].OperatorUserwise(NOLOCK)  
  WHERE UserID = @USERID  
   AND [OperatorID] = @OPERATORID  
   AND [IsOn] = 1  
   AND [IsDelete] = 0  
  
  DECLARE @Usage DECIMAL(18, 5) = 0;  
  
  SELECT @Usage = ISNULL(Sum(TransactionAmount), 0)  
  FROM TransactionDetails TD  
  INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID  
  WHERE TD.UserID = @USERID  
   AND convert(DATE, TD.DOC, 103) >= convert(DATE, GetDate(), 103)  
   AND R.OperatorID = @OPERATORID  
   AND TD.RechargeID NOT IN (  
    SELECT RechargeID  
    FROM transactionDetails  
    WHERE convert(DATE, TD.DOC, 103) >= convert(DATE, GetDate(), 103)  
     AND DocType = 'REFUND'  
    )  
  
  PRINT '@DailyUsageLimit:'  
  PRINT @DailyUsageLimit  
  PRINT '@Usage'  
  PRINT @Usage  
  PRINT '@Usage + @AMOUNT'  
  PRINT @Usage + @AMOUNT  
  PRINT '@AMOUNT'  
  PRINT @AMOUNT  
  
  IF (@DailyUsageLimit != - 1)  
  BEGIN  
   IF (@Usage + @AMOUNT > @DailyUsageLimit)  
   BEGIN  
    DECLARE @Limit DECIMAL(18, 2) = @DailyUsageLimit - @Usage;  
  
    PRINT '@Limit'  
  
    IF (@Limit < 0)  
    BEGIN  
     SET @Limit = 0  
    END  
  
    PRINT @Limit  
    PRINT 'Your Remaining Daily Usage limit for this operator is RS. ' + Convert(VARCHAR(20), @Limit);  
  
    SET @OUTPUT = 'Your Remaining Daily Usage limit for this operator is RS. ' + Convert(VARCHAR(20), @Limit);  
  
    RETURN;  
   END  
  END  
 END  
  
 DECLARE @UserWiseServiceCount INT = 0  
  
 SELECT @UserWiseServiceCount = COUNT(UserServiceID)  
 FROM [dbo].UserServiceMapping(NOLOCK)  
 WHERE UserID = @USERID  
  AND ServiceID = @SERVICEID  
  AND IsActive = 1  
  AND IsBlock = 0  
  
 IF @UserWiseServiceCount <= 0  
 BEGIN  
  PRINT 'Service is disabled for you, please contact to Admin';  
  
  SET @OUTPUT = 'Service is disabled for you, please contact to Admin';  
  
  RETURN (0);  
 END  
  
 DECLARE @MinServiceBalance INT = 0  
 DECLARE @CurrentBalance NUMERIC(18, 5) = 0  
  
 SELECT @MinServiceBalance = ISNULL(MinimumBalanace, 0)  
 FROM dbo.ServiceMaster(NOLOCK)  
 WHERE IsDeleted = 0  
  AND ServiceID = @SERVICEID  
  
 DECLARE @MinimumBalance DECIMAL(18, 5);  
  
 IF (@SERVICEID = @ElectricityServiceID)  
 BEGIN  
  SELECT @CurrentBalance = ISNULL(DMRBalance, 0)  
   ,@MinimumBalance = ISNULL([MinimumBalance], 0)  
  FROM dbo.UserBalance(NOLOCK)  
  WHERE UserID = @USERID  
 END  
 ELSE  
 BEGIN  
  SELECT @CurrentBalance = ISNULL(CurrentBalance, 0)  
   ,@MinimumBalance = ISNULL([MinimumBalance], 0)  
  FROM dbo.UserBalance(NOLOCK)  
  WHERE UserID = @USERID  
 END  
  
 IF @MinimumBalance > @MinServiceBalance  
 BEGIN  
  SET @MinServiceBalance = @MinimumBalance  
 END  
  
 -- Commission amount + recharge amount should be less than balance               
 IF (@SERVICEID != @MoneyTransferService)  
 BEGIN  
  DECLARE @comAmt DECIMAL(18, 5);  
  
  EXECUTE [dbo].RechargeSelectRetailerCommission @UserID = @UserID  
   ,@OperatorID = @OperatorID  
   ,@TransactionAmount = @Amount  
   ,@comAmt = @comAmt OUTPUT  
  
  DECLARE @BlockAmount DECIMAL(18, 5);  
  
  EXECUTE [BalanceRequestSelectForBlockAmount] @UserID = @UserID  
   ,@BalanceType = 'Recharge'  
   ,@BlockAmount = @BlockAmount OUT  
  
  IF (@Amount - @comAmt) > (@CurrentBalance - @MinServiceBalance - @BlockAmount)  
  BEGIN  
   DECLARE @comAmtConverted DECIMAL(18, 2) = @comAmt;  
   DECLARE @comAmtStr VARCHAR(100) = 'Commission is ' + convert(VARCHAR(20), @comAmtConverted) + '.';  
   DECLARE @MinSerBal DECIMAL(18, 2) = @MinServiceBalance;  
   DECLARE @MinServiceBalanceStr VARCHAR(100) = 'Minimum service balance is ' + Convert(VARCHAR(20), @MinSerBal) + '.';  
   DECLARE @BlockAmt DECIMAL(18, 2) = @BlockAmount;  
   DECLARE @BlockAmtStr VARCHAR(100) = 'Your ' + convert(VARCHAR(50), @BlockAmt) + ' is blocked.';  
   DECLARE @BarAmt DECIMAL(18, 2) = @CurrentBalance - (@BlockAmount + @MinServiceBalance - @comAmt);  
   DECLARE @BarAmtStr VARCHAR(100) = 'Put amount up to ' + CONVERT(VARCHAR(100), @BarAmt) + '.';  
   DECLARE @CurrentBalanceconverted DECIMAL(18, 2) = @CurrentBalance;  
   DECLARE @CurBalanceStr VARCHAR(100) = 'Your current balance is ' + Convert(VARCHAR(20), @CurrentBalanceconverted) + '.'  
   DECLARE @OUTPUTStr VARCHAR(2000) = '';  
  
   IF (@comAmt != 0)  
   BEGIN  
    SET @OUTPUTStr = @comAmtStr;  
   END  
  
   IF (@MinServiceBalance != 0)  
   BEGIN  
    SET @OUTPUTStr = @OUTPUTStr + ' ' + @MinServiceBalanceStr;  
   END  
  
   IF (@BlockAmount != 0)  
   BEGIN  
    SET @OUTPUTStr = @OUTPUTStr + ' ' + @BlockAmtStr;  
   END  
  
   SET @OUTPUT = @CurBalanceStr + ' ' + @OUTPUTStr + ' ' + @BarAmtStr + ' ' + 'Please contact to admin.';  
  
   RETURN (0);  
  END  
 END  
  
 -------In case Of Money transfer--- Check Bal+Commission validation--------                
 DECLARE @DMRCurrentBalance DECIMAL(18, 5);  
  
 SELECT @DMRCurrentBalance = ISNull(DMRBalance, 0)  
 FROM UserBalance(NOLOCK)  
 WHERE UserID = @USERID  
  
 IF (  
   @SERVICEID = @MoneyTransferService  
   AND @OperatorID != @BeneVerificationOperratorID  
   )  
 BEGIN  
  DECLARE @RetailerCommAmt DECIMAL(18, 5);  
  DECLARE @DistributorCommAmt DECIMAL(18, 5);  
  DECLARE @SuperDistributorCommAmt DECIMAL(18, 5)  
  
  EXECUTE [dbo].SlabCommissionSelectByUser @USERID = @USERID  
   ,@TransactionAmount = @Amount  
   ,@RetailerCommAmt = @RetailerCommAmt OUTPUT  
   ,@DistributorCommAmt = @DistributorCommAmt OUTPUT  
   ,@SuperDistributorCommAmt = @SuperDistributorCommAmt OUTPUT  
  
  DECLARE @BlockAmountDMR DECIMAL(18, 5);  
  
  EXECUTE [BalanceRequestSelectForBlockAmount] @UserID = @UserID  
   ,@BalanceType = 'DMT'  
   ,@BlockAmount = @BlockAmountDMR OUT  
  
  IF (@Amount - @RetailerCommAmt) > (@DMRCurrentBalance - @MinServiceBalance - @BlockAmountDMR)  
  BEGIN  
   DECLARE @comAmtConvertedDMR DECIMAL(18, 2) = @RetailerCommAmt;  
   DECLARE @comAmtStrDMR VARCHAR(100) = 'Commission is ' + convert(VARCHAR(20), @comAmtConvertedDMR) + '.';  
   DECLARE @MinSerBalDMR DECIMAL(18, 2) = @MinServiceBalance;  
   DECLARE @MinServiceBalanceStrDMR VARCHAR(100) = 'Minimum service balance is ' + Convert(VARCHAR(20), @MinSerBalDMR) + '.';  
   DECLARE @BlockAmtDMR DECIMAL(18, 2) = @BlockAmountDMR;  
   DECLARE @BlockAmtStrDMR VARCHAR(100) = 'Your ' + convert(VARCHAR(50), @BlockAmtDMR) + ' is blocked.';  
   DECLARE @BarAmtDMR DECIMAL(18, 2) = @DMRCurrentBalance - (@BlockAmountDMR + @MinServiceBalance - @RetailerCommAmt);  
   DECLARE @BarAmtStrDMR VARCHAR(100) = 'Put amount up to ' + CONVERT(VARCHAR(100), @BarAmtDMR) + '.';  
   DECLARE @CurrentBalanceconvertedDMR DECIMAL(18, 2) = @DMRCurrentBalance;  
   DECLARE @CurBalanceStrDMR VARCHAR(100) = 'Your current balance is ' + Convert(VARCHAR(20), @CurrentBalanceconvertedDMR) + '.'  
   DECLARE @OUTPUTStrDMR VARCHAR(2000) = '';  
  
   IF (@RetailerCommAmt != 0)  
   BEGIN  
    SET @OUTPUTStrDMR = @comAmtStrDMR;  
   END  
  
   IF (@MinServiceBalance != 0)  
   BEGIN  
    SET @OUTPUTStrDMR = @OUTPUTStrDMR + ' ' + @MinServiceBalanceStrDMR;  
   END  
  
   IF (@BlockAmountDMR != 0)  
   BEGIN  
    SET @OUTPUTStrDMR = @OUTPUTStrDMR + ' ' + @BlockAmtStrDMR;  
   END  
  
   SET @OUTPUT = @CurBalanceStrDMR + ' ' + @OUTPUTStrDMR + ' ' + @BarAmtStrDMR + ' ' + 'Please contact to admin.';  
  
   RETURN (0);  
  END  
 END  
  
 DECLARE @return_rows INT  
  
 EXECUTE @return_rows = RechargeProviderSelectByOperatorUserwise @OPERATORID  
  ,@AMOUNT  
  ,@USERID  
  
 IF @return_rows <= 0  
 BEGIN  
  PRINT 'Operator is down, please contact to Admin';  
  
  SET @OUTPUT = 'Operator is down, please contact to Admin';  
  
  RETURN (0);  
 END  
  
 DECLARE @BlockedTill DATETIME;  
  
 SELECT @BlockedTill = BlockedTill  
 FROM OperatorMaster  
 WHERE OperatorID = @OPERATORID  
  AND IsTimelyBlocked = 1  
  
 IF (@BlockedTill IS NOT NULL)  
 BEGIN  
  IF (@BlockedTill > CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30'), (0)))  
  BEGIN  
   PRINT 'Operator is down,  please contact to Admin.';  
  
   SET @OUTPUT = 'Operator is down, please contact to Admin.';  
  
   RETURN (0);  
  END  
 END  
  
 PRINT 'OK';  
  
 SET @OUTPUT = 'OK'  
END  
GO
/****** Object:  StoredProcedure [dbo].[CheckRechargeValidationForService]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sushant Yelpale
-- Create date: 25/01/2019
-- Description:	Check Service is Down
-- =============================================
CREATE PROCEDURE [dbo].[CheckRechargeValidationForService]
	@SERVICEID varchar(20)
	,@USERID INT 
	,@OUTPUT VARCHAR(500) OUT 
AS
BEGIN

DECLARE @IsServiceOn INT = 0    
	-- Check Service is On/Off (Start) -------                          
 SELECT @IsServiceOn = COUNT(ServiceID)    
 FROM dbo.ServiceMaster(NOLOCK)    
 WHERE IsDeleted = 0    
  AND ServiceID = @SERVICEID    
    
 IF @IsServiceOn <= 0    
 BEGIN    
  PRINT 'Service is off, please contact to Admin';    
  SET @OUTPUT = 'Service is off, please contact to Admin';    
  RETURN (0);    
 END    

 DECLARE @UserWiseServiceCount INT = 0    
    
 SELECT @UserWiseServiceCount = COUNT(UserServiceID)    
 FROM [dbo].UserServiceMapping(NOLOCK)    
 WHERE UserID = @USERID    
  AND ServiceID = @SERVICEID    
  AND IsActive = 1    
  AND IsBlock = 0    
    
 IF @UserWiseServiceCount <= 0    
 BEGIN    
  PRINT 'Service is disabled for you, please contact to Admin';    
  SET @OUTPUT = 'Service is disabled for you, please contact to Admin';    
  RETURN (0);    
  END    

  PRINT 'OK';    
 SET @OUTPUT = 'OK'    

END

GO
/****** Object:  StoredProcedure [dbo].[CheckRechargeValidationNew]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================                  
-- Author:  <Sainath Bhujbal>                  
-- ALTER  date: <27 Feb 2017 ,>                  
-- Description: <Description,,>          
--@modified by Somnath : Check provider seqence is set properly              
-- =============================================              
--[dbo].[CheckRechargeValidation] 3,1,'8421536091',10,18,out          
CREATE PROCEDURE [dbo].[CheckRechargeValidationNew] @USERID INT
	,@SERVICEID VARCHAR(10)
	,@CONSUMERNO VARCHAR(20)
	,@AMOUNT NUMERIC(18, 5)
	,@OPERATORID INT
	,@OUTPUT VARCHAR(500) OUT
AS
BEGIN
	DECLARE @IsServiceOn INT = 0
	DECLARE @MoneyTransferService INT = 21;
	DECLARE @BeneVerificationOperratorID INT = 45;

	SET NOCOUNT ON

	-- Check Service is On/Off (Start) -------          
	SELECT @IsServiceOn = COUNT(ServiceID)
	FROM dbo.ServiceMaster(NOLOCK)
	WHERE IsDeleted = 0
		AND ServiceID = @SERVICEID

	IF @IsServiceOn <= 0
	BEGIN
		PRINT 'Service is off, please contact to Admin';

		SET @OUTPUT = 'Service is off, please contact to Admin';

		RETURN (0);
	END

	-- User-service           
	-- Service balance          
	-- Check Service is On/Off (End) -------          
	DECLARE @IsOperatorOn INT = 0

	SELECT @IsOperatorOn = COUNT(OperatorID)
	FROM DBO.OperatorMaster(NOLOCK)
	WHERE OperatorID = @OPERATORID
		AND IsOn = 1

	IF @IsOperatorOn <= 0
	BEGIN
		PRINT 'Operator is off, please contact to Admin';

		SET @OUTPUT = 'Operator is off, please contact to Admin';

		RETURN (0);
	END

	DECLARE @MinAmount NUMERIC(18, 5) = 0.0
	DECLARE @MaxAmount NUMERIC(18, 5) = 0.0

	SELECT @MinAmount = MinimumRechargeAmount
		,@MaxAmount = MaximumRechargeAmount
	FROM dbo.OperatorMaster(NOLOCK)
	WHERE OperatorID = @OPERATORID

	IF (
			@Amount < @MinAmount
			OR @Amount > @MaxAmount
			)
	BEGIN
		PRINT 'Invalid recharge amount';

		SET @OUTPUT = 'Invalid recharge amount';

		RETURN (0);
	END

	DECLARE @UserWiseOperatorCount INT = 0

	SELECT @UserWiseOperatorCount = COUNT(UserCommissionID)
	FROM UserCommission(NOLOCK)
	WHERE UserID = @USERID
		AND OperatorId = @OPERATORID

	IF (@UserWiseOperatorCount >= 1)
	BEGIN
		SELECT @UserWiseOperatorCount = COUNT(UserCommissionID)
		FROM UserCommission(NOLOCK)
		WHERE UserID = @USERID
			AND OperatorId = @OPERATORID
			AND IsOn = 1

		IF (@UserWiseOperatorCount <= 0)
		BEGIN
			PRINT 'Operator is disabled for you, please contact to Admin';

			SET @OUTPUT = 'Operator is disabled for you, please contact to Admin';

			RETURN (0);
		END
	END

	-----Added By sdy - check userwise table for Operator -----    
	DECLARE @UserWiseOperatorCount1 INT = 0

	IF EXISTS (
			SELECT operatorID
			FROM [dbo].OperatorUserwise(NOLOCK)
			WHERE UserID = @USERID
				AND [OperatorID] = @OPERATORID
			)
	BEGIN
		------ Check For Userwise Operator --------    
		SELECT @UserWiseOperatorCount1 = COUNT(operatorID)
		FROM [dbo].OperatorUserwise(NOLOCK)
		WHERE UserID = @USERID
			AND [OperatorID] = @OPERATORID
			AND [IsOn] = 1
			AND [IsDelete] = 0

		IF @UserWiseOperatorCount1 <= 0
		BEGIN
			PRINT 'Your Operator is disabled, please contact to Admin';

			SET @OUTPUT = 'Your Operator is disabled, please contact to Admin';

			RETURN (0);
		END

		------ Check For Userwise UsageLimit --------    
		DECLARE @DailyUsageLimit DECIMAL(18, 5) = 0;

		SELECT @DailyUsageLimit = [UsageLimit]
		FROM [dbo].OperatorUserwise(NOLOCK)
		WHERE UserID = @USERID
			AND [OperatorID] = @OPERATORID
			AND [IsOn] = 1
			AND [IsDelete] = 0

		DECLARE @Usage DECIMAL(18, 5) = 0;

		SELECT @Usage = ISNULL(Sum(TransactionAmount), 0)
		FROM TransactionDetails TD
		INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID
		WHERE TD.UserID = @USERID
			AND convert(DATE, TD.DOC, 103) >= convert(DATE, GetDate(), 103)
			AND R.OperatorID = @OPERATORID
			AND TD.RechargeID NOT IN (
				SELECT RechargeID
				FROM transactionDetails
				WHERE convert(DATE, TD.DOC, 103) >= convert(DATE, GetDate(), 103)
					AND DocType = 'REFUND'
				)

		PRINT '@DailyUsageLimit:'
		PRINT @DailyUsageLimit
		PRINT '@Usage'
		PRINT @Usage
		PRINT '@Usage + @AMOUNT'
		PRINT @Usage + @AMOUNT
		PRINT '@AMOUNT'
		PRINT @AMOUNT

		IF (@DailyUsageLimit != - 1)
		BEGIN
			IF (@Usage + @AMOUNT > @DailyUsageLimit)
			BEGIN
				DECLARE @Limit DECIMAL(18, 2) = @DailyUsageLimit - @Usage;

				PRINT '@Limit'

				IF (@Limit < 0)
				BEGIN
					SET @Limit = 0
				END

				PRINT @Limit
				PRINT 'Your Remaining Daily Usage limit for this operator is RS. ' + Convert(VARCHAR(20), @Limit);

				SET @OUTPUT = 'Your Remaining Daily Usage limit for this operator is RS. ' + Convert(VARCHAR(20), @Limit);

				RETURN;
			END
		END
	END

	DECLARE @UserWiseServiceCount INT = 0

	SELECT @UserWiseServiceCount = COUNT(UserServiceID)
	FROM [dbo].UserServiceMapping(NOLOCK)
	WHERE UserID = @USERID
		AND ServiceID = @SERVICEID
		AND IsActive = 1
		AND IsBlock = 0

	IF @UserWiseServiceCount <= 0
	BEGIN
		PRINT 'Service is disabled for you, please contact to Admin';

		SET @OUTPUT = 'Service is disabled for you, please contact to Admin';

		RETURN (0);
	END

	DECLARE @MinServiceBalance INT = 0
	DECLARE @CurrentBalance NUMERIC(18, 5) = 0

	SELECT @MinServiceBalance = ISNULL(MinimumBalanace, 0)
	FROM dbo.ServiceMaster(NOLOCK)
	WHERE IsDeleted = 1
		AND ServiceID = @SERVICEID

	SELECT @CurrentBalance = ISNULL(CurrentBalance, 0)
	FROM dbo.UserBalance(NOLOCK)
	WHERE UserID = @USERID

	IF @CurrentBalance < @MinServiceBalance
	BEGIN
		PRINT 'Invalid amount for service';

		SET @OUTPUT = 'Invalid amount for service';

		RETURN (0);
	END

	-- Commission amount + recharge amount should be less than balance 
	IF (@SERVICEID != @MoneyTransferService)
	BEGIN
		DECLARE @comAmt DECIMAL(18, 5);

		EXECUTE [dbo].RechargeSelectRetailerCommission @UserID = @UserID
			,@OperatorID = @OperatorID
			,@TransactionAmount = @Amount
			,@comAmt = @comAmt OUTPUT

		IF (@Amount - @comAmt) > (@CurrentBalance - @MinServiceBalance)
		BEGIN
			SET @OUTPUT = 'Operator usage limit is over , Please contact to admin.';

			RETURN (0);
		END
	END

	-------In case Of Money transfer--- Check Bal+Commission validation--------  
	IF (
			@SERVICEID = @MoneyTransferService
			AND @OperatorID != @BeneVerificationOperratorID
			)
	BEGIN
		DECLARE @RetailerCommAmt DECIMAL(18, 5);
		DECLARE @DistributorCommAmt DECIMAL(18, 5);
		DECLARE @SuperDistributorCommAmt DECIMAL(18, 5)

		EXECUTE [dbo].SlabCommissionSelectByUser @USERID = @USERID
			,@TransactionAmount = @Amount
			,@RetailerCommAmt = @RetailerCommAmt OUTPUT
			,@DistributorCommAmt = @DistributorCommAmt OUTPUT
			,@SuperDistributorCommAmt = @SuperDistributorCommAmt OUTPUT

		IF (@CurrentBalance + @RetailerCommAmt) < @MinServiceBalance
		BEGIN
			SET @OUTPUT = 'Operator usage limit is over , Please contact to admin';

			RETURN (0);
		END
	END

	DECLARE @return_rows INT

	EXECUTE @return_rows = RechargeProviderSelectByOperatorUserwise @OPERATORID
		,@AMOUNT
		,@USERID

	IF @return_rows <= 0
	BEGIN
		PRINT 'Operator is down, please contact to Admin';

		SET @OUTPUT = 'Operator is down, please contact to Admin';

		RETURN (0);
	END

	DECLARE @BlockedTill DATETIME;

	SELECT @BlockedTill = BlockedTill
	FROM OperatorMaster
	WHERE OperatorID = @OPERATORID
		AND IsTimelyBlocked = 1

	IF (@BlockedTill IS NOT NULL)
	BEGIN
		IF (@BlockedTill > CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30'), (0)))
		BEGIN
			PRINT 'Operator is down,  please contact to Admin.';

			SET @OUTPUT = 'Operator is down, please contact to Admin.';

			RETURN (0);
		END
	END

	PRINT 'OK';

	SET @OUTPUT = 'OK'
END



GO
/****** Object:  StoredProcedure [dbo].[checkValidToken]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- checkValidToken '9168336267','MAHI1612'  
CREATE proc [dbo].[checkValidToken]  
@Username varchar(100),  
@Token varchar(100)  
as  
begin  
Select * from UserInformation(nolock) where MobileNumber=@Username and Token=@Token and UserTypeID=4  
end  
GO
/****** Object:  StoredProcedure [dbo].[ClientInformationSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Author :Sainath Bhujbal    
--Date : 15 Feb 2018    
CREATE PROCEDURE [dbo].[ClientInformationSelect]
AS
BEGIN
	SELECT [ContactNumber]
		,[EmailID]
	FROM [ClientInformation]
END



GO
/****** Object:  StoredProcedure [dbo].[Cyberresponse]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE[dbo].[Cyberresponse] @mobileNumber VARCHAR(20)
	,@Type NVARCHAR(max)
	,@Request NVARCHAR(max)
	,@Response NVARCHAR(max)
AS
BEGIN
	INSERT INTO RechargeProviderRechargeLogID (
		ConsumerNumber
		,[type]
		,[request]
		,[response]
		)
	VALUES (
		@mobileNumber
		,@Type
		,@Request
		,@Response
		)
END



GO
/****** Object:  StoredProcedure [dbo].[DeleteGroup]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Name:Rahul Hembade
--Reson: Delete gropup 
--==============
CREATE PROCEDURE [dbo].[DeleteGroup] @GroupID VARCHAR(100)
AS
BEGIN
	DELETE
	FROM UserwiseSlabCommission
	WHERE GroupID = @GroupID;

	DELETE
	FROM GroupDetail
	WHERE ID = @GroupID;
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteRecords]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
--Modifay:Rahul Hembade  
--Reson:adding  log table   
--================================   
--[dbo].[DeleteRecords] 50
CREATE PROCEDURE[dbo].[DeleteRecords] @Day INT  
AS  
BEGIN  
 SET @Day = 50  
  
 IF (@Day > 45)  
 BEGIN  
  
  
  DELETE  
  FROM [dbo].[Recharge]  
  WHERE DATEDIFF(DAY, DOC, (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))) > @Day  
  
  PRINT 'Recharge'  
  
  DELETE  
  FROM [dbo].[Inbox]  
  WHERE DATEDIFF(DAY, DOC, (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))) > @Day  
  
  PRINT '[Inbox]'  
  
  DELETE  
  FROM [dbo].[LoginHistory]  
  WHERE DATEDIFF(DAY, DOC, (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))) > @Day  
  
  PRINT '[[LoginHistory]]'  
  
  DELETE  
  FROM [dbo].[RechargeProviderRechargeLog]  
  WHERE DATEDIFF(DAY, DOC, (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))) > @Day  
  
  PRINT '[[RechargeProviderRechargeLog]]'  
  
  DELETE  
  FROM [dbo].[WebScrappingReply]  
  WHERE DATEDIFF(DAY, DOC, (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))) > @Day  
  
  DELETE  
  FROM [dbo].[Log]  
  WHERE DATEDIFF(DAY, [Date], (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))) > @Day  
  
  PRINT '[[Log]]'  
 END  
END  


GO
/****** Object:  StoredProcedure [dbo].[DistributionEnquiryInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DistributionEnquiryInsert] @Name VARCHAR(50)
	,@Email VARCHAR(50)
	,@MobileNumber VARCHAR(50)
	,@Message VARCHAR(50)
	,@Location VARCHAR(50)
	,@Investment VARCHAR(50)
AS
BEGIN
	INSERT INTO DistributionEnquiry (
		name
		,Email
		,MobileNumber
		,Message
		,Location
		,Investment
		)
	VALUES (
		@Name
		,@Email
		,@MobileNumber
		,@Message
		,@Location
		,@Investment
		)

	DECLARE @RefNumber INT;

	SET @RefNumber = (
			SELECT @@identity
			)

	RETURN @RefNumber;
END



GO
/****** Object:  StoredProcedure [dbo].[DistributorRechargeSelectStatusCount]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- [DistributorRechargeSelectStatusCount] 2610  

CREATE PROCEDURE [dbo].[DistributorRechargeSelectStatusCount] @UserID INT

	,@date DATE = NULL

AS

BEGIN

	IF (@date IS NULL)

	BEGIN

		SET @date = (switchoffset(sysdatetimeoffset(), '+05:30'))

	END



	DECLARE @UserTypeId INT



	SELECT @UserTypeId = UserTypeId

	FROM UserInformation

	WHERE UserId = @UserID



	PRINT '@UserTypeId'

	PRINT @UserTypeId



	IF (@UserTypeId = 2)

	BEGIN

		SELECT IsNull(Sum(Success), 0) AS Success

			,IsNull(Sum(SuccessAmount), 0) AS SuccessAmount

			,IsNull(Sum(Fail), 0) AS Fail

			,IsNull(Sum(FailAmount), 0) AS FailAmount

			,IsNull(Sum(Process), 0) AS Process

			,IsNull(Sum(Autorefund), 0) AS Autorefund

			,IsNull(Sum(ProcessAmount), 0) AS ProcessAmount

			,IsNull(Sum(AutorefundAmount), 0) AS AutorefundAmount

			,Count(1) AS Total

			,IsNull(Sum(FLOOR(TotalAmount)), 0) AS TotalAmount

		FROM (

			SELECT IsNull(Sum(CASE 

							WHEN STATUS = 'SUCCESS'

								THEN 1

							ELSE 0

							END), 0) AS Success

				,IsNull(Sum(CASE 

							WHEN STATUS = 'SUCCESS'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS SuccessAmount

				,IsNull(Sum(CASE 

							WHEN STATUS = 'FAILURE'

								THEN 1

							ELSE 0

							END), 0) AS Fail

				,IsNull(Sum(CASE 

							WHEN STATUS = 'FAILURE'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS FailAmount

				,IsNull(Sum(CASE 

							WHEN STATUS = 'PROCESS'

								THEN 1

							ELSE 0

							END), 0) AS Process

				,IsNull(Sum(CASE 

							WHEN STATUS = 'REFUND'

								THEN 1

							ELSE 0

							END), 0) AS Autorefund

				,IsNull(Sum(CASE 

							WHEN STATUS = 'PROCESS'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS ProcessAmount

				,IsNull(Sum(CASE 

							WHEN STATUS = 'REFUND'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS AutorefundAmount

				,Count(1) AS Total

				,Sum(FLOOR(Amount)) AS TotalAmount

			FROM Recharge R(NOLOCK)

			INNER JOIN UserInformation UI ON UI.UserID = R.UserID

			WHERE CONVERT(VARCHAR, R.[DOC], 103) = CONVERT(VARCHAR, @date, 103)

				AND @UserID IN (UI.ParentID)

			

			UNION ALL

			

			SELECT IsNull(Sum(CASE 

							WHEN STATUS = 'SUCCESS'

								THEN 1

							ELSE 0

							END), 0) AS Success

				,IsNull(Sum(CASE 

							WHEN STATUS = 'SUCCESS'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS SuccessAmount

				,IsNull(Sum(CASE 

							WHEN STATUS = 'FAILURE'

								THEN 1

							ELSE 0

							END), 0) AS Fail

				,IsNull(Sum(CASE 

							WHEN STATUS = 'FAILURE'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS FailAmount

				,IsNull(Sum(CASE 

							WHEN STATUS = 'PROCESS'

								THEN 1

							ELSE 0

							END), 0) AS Process

				,IsNull(Sum(CASE 

							WHEN STATUS = 'REFUND'

								THEN 1

							ELSE 0

							END), 0) AS Autorefund

				,IsNull(Sum(CASE 

							WHEN STATUS = 'PROCESS'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS ProcessAmount

				,IsNull(Sum(CASE 

							WHEN STATUS = 'REFUND'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS AutorefundAmount

				,Count(1) AS Total

				,Sum(FLOOR(Amount)) AS TotalAmount

			FROM [dbo].[MoneyTransfer] R(NOLOCK)

			INNER JOIN UserInformation UI ON UI.UserID = R.UserID

			WHERE CONVERT(VARCHAR, R.[DOC], 103) = CONVERT(VARCHAR, @date, 103)

				AND @UserID IN (UI.ParentID)

			) AS MainTable

	END



	IF (@UserTypeId = 8)

	BEGIN

		DECLARE @TempTbl TABLE (UserID VARCHAR(200));



		WITH cte

		AS (

			SELECT *

			FROM UserInformation

			WHERE ParentID = @UserID

			

			UNION ALL

			

			SELECT UserInformation.*

			FROM UserInformation

			INNER JOIN cte ON cte.UserID = UserInformation.ParentID

			)

		INSERT INTO @TempTbl (UserID)

		SELECT UserID

		FROM cte



		SELECT IsNull(Sum(Success), 0) AS Success

			,IsNull(Sum(SuccessAmount), 0) AS SuccessAmount

			,IsNull(Sum(Fail), 0) AS Fail

			,IsNull(Sum(FailAmount), 0) AS FailAmount

			,IsNull(Sum(Process), 0) AS Process

			,IsNull(Sum(Autorefund), 0) AS Autorefund

			,IsNull(Sum(ProcessAmount), 0) AS ProcessAmount

			,IsNull(Sum(AutorefundAmount), 0) AS AutorefundAmount

			,Count(1) AS Total

			,Isnull(Sum(FLOOR(TotalAmount)), 0) AS TotalAmount

		FROM (

			SELECT IsNull(Sum(CASE 

							WHEN STATUS = 'SUCCESS'

								THEN 1

							ELSE 0

							END), 0) AS Success

				,IsNull(Sum(CASE 

							WHEN STATUS = 'SUCCESS'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS SuccessAmount

				,IsNull(Sum(CASE 

							WHEN STATUS = 'FAILURE'

								THEN 1

							ELSE 0

							END), 0) AS Fail

				,IsNull(Sum(CASE 

							WHEN STATUS = 'FAILURE'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS FailAmount

				,IsNull(Sum(CASE 

							WHEN STATUS = 'PROCESS'

								THEN 1

							ELSE 0

							END), 0) AS Process

				,IsNull(Sum(CASE 

							WHEN STATUS = 'REFUND'

								THEN 1

							ELSE 0

							END), 0) AS Autorefund

				,IsNull(Sum(CASE 

							WHEN STATUS = 'PROCESS'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS ProcessAmount

				,IsNull(Sum(CASE 

							WHEN STATUS = 'REFUND'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS AutorefundAmount

				,Count(1) AS Total

				,Sum(FLOOR(Amount)) AS TotalAmount

			FROM Recharge R(NOLOCK)

			INNER JOIN UserInformation UI ON UI.UserID = R.UserID

			INNER JOIN @TempTbl TT ON TT.UserID = R.UserID

			WHERE CONVERT(VARCHAR, R.[DOC], 103) = CONVERT(VARCHAR, @date, 103)

			

			UNION ALL

			

			SELECT IsNull(Sum(CASE 

							WHEN STATUS = 'SUCCESS'

								THEN 1

							ELSE 0

							END), 0) AS Success

				,IsNull(Sum(CASE 

							WHEN STATUS = 'SUCCESS'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS SuccessAmount

				,IsNull(Sum(CASE 

							WHEN STATUS = 'FAILURE'

								THEN 1

							ELSE 0

							END), 0) AS Fail

				,IsNull(Sum(CASE 

							WHEN STATUS = 'FAILURE'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS FailAmount

				,IsNull(Sum(CASE 

							WHEN STATUS = 'PROCESS'

								THEN 1

							ELSE 0

							END), 0) AS Process

				,IsNull(Sum(CASE 

							WHEN STATUS = 'REFUND'

								THEN 1

							ELSE 0

							END), 0) AS Autorefund

				,IsNull(Sum(CASE 

							WHEN STATUS = 'PROCESS'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS ProcessAmount

				,IsNull(Sum(CASE 

							WHEN STATUS = 'REFUND'

								THEN FLOOR(Amount)

							ELSE 0

							END), 0) AS AutorefundAmount

				,Count(1) AS Total

				,Sum(FLOOR(Amount)) AS TotalAmount

			FROM [dbo].[MoneyTransfer] R(NOLOCK)

			INNER JOIN UserInformation UI ON UI.UserID = R.UserID

			INNER JOIN @TempTbl TT ON TT.UserID = R.UserID

			WHERE CONVERT(VARCHAR, R.[DOC], 103) = CONVERT(VARCHAR, @date, 103)

			) AS MainTable

	END

END



GO
/****** Object:  StoredProcedure [dbo].[DistributorRechargeSelectTodaysFailRecharge]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DistributorRechargeSelectTodaysFailRecharge] @UserID INT
	,@date DATE = NULL
AS
BEGIN
	IF (@date IS NULL)
	BEGIN
		SET @date = (switchoffset(sysdatetimeoffset(), '+05:30'))
	END

	DECLARE @UserTypeId INT

	SELECT @UserTypeId = UserTypeId
	FROM UserInformation
	WHERE UserId = @UserID

	IF (@UserTypeId = 2)
	BEGIN
		(
				SELECT R.OperatorID
					,OM.OperatorName
					,sum(R.Amount) AS Amount
					,Count(R.RechargeID) AS Count
				FROM OperatorMaster OM
				INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID
				INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
				INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID
				INNER JOIN UserInformation UI ON UI.UserID = R.UserID
				WHERE R.STATUS LIKE '%Fail%' or R.STATUS LIKE '%Refund%'
					AND convert(VARCHAR(20), R.DOC, 103) = convert(VARCHAR(20), @date, 103)
					AND @UserID IN (UI.ParentID)
				GROUP BY SM.ServiceName
					,RPM.RechargeProviderName
					,OM.OperatorName
					,R.RechargeProviderID
					,R.OperatorID
					,R.ServiceID
				
				UNION ALL
				
				SELECT E.[DMROperatorID]
					,OM.OperatorName
					,sum(E.Amount) AS Amount
					,Count(E.MoneyTransferID) AS Count
				FROM OperatorMaster OM
				INNER JOIN [dbo].[MoneyTransfer] E ON OM.OperatorID = E.[DMROperatorID]
				INNER JOIN ServiceMaster SM ON E.[DMRServiceID] = SM.ServiceID
				INNER JOIN RechargeProviderMaster RPM ON E.[DMRProviderID] = RPM.RechargeProviderID
				INNER JOIN UserInformation UI ON UI.UserID = E.UserID
				WHERE E.STATUS LIKE '%Fail%' or E.STATUS LIKE '%Refund%'
					AND convert(VARCHAR(20), E.DOC, 103) = convert(VARCHAR(20), @date, 103)
					AND @UserID IN (UI.ParentID)
				GROUP BY SM.ServiceName
					,RPM.RechargeProviderName
					,OM.OperatorName
					,E.[DMRProviderID]
					,E.[DMROperatorID]
					,E.[DMRServiceID]
				)
		ORDER BY count DESC
	END

	IF (@UserTypeId = 7)
	BEGIN
		(
				SELECT R.OperatorID
					,OM.OperatorName
					,sum(R.Amount) AS Amount
					,Count(R.RechargeID) AS Count
				FROM OperatorMaster OM
				INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID
				INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
				INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID
				INNER JOIN UserInformation UI ON UI.UserID = R.UserID
				WHERE R.STATUS LIKE '%Fail%' or R.STATUS LIKE '%Refund%'
					AND convert(VARCHAR(20), R.DOC, 103) = convert(VARCHAR(20), @date, 103)
					AND R.UserID IN (
						SELECT UserId
						FROM dbo.UserInformation
						WHERE ParentId IN (
								SELECT UserId
								FROM dbo.UserInformation
								WHERE ParentId = @UserID
								)
						)
				GROUP BY SM.ServiceName
					,RPM.RechargeProviderName
					,OM.OperatorName
					,R.RechargeProviderID
					,R.OperatorID
					,R.ServiceID
				
				UNION ALL
				
				SELECT E.[DMROperatorID]
					,OM.OperatorName
					,sum(E.Amount) AS Amount
					,Count(E.[MoneyTransferID]) AS Count
				FROM OperatorMaster OM
				INNER JOIN [dbo].[MoneyTransfer] E ON OM.OperatorID = E.[DMROperatorID]
				INNER JOIN ServiceMaster SM ON E.[DMRServiceID] = SM.ServiceID
				INNER JOIN RechargeProviderMaster RPM ON E.[DMRProviderID] = RPM.RechargeProviderID
				INNER JOIN UserInformation UI ON UI.UserID = E.UserID
				WHERE E.STATUS LIKE '%Fail%' or E.STATUS LIKE '%Refund%'
					AND convert(VARCHAR(20), E.DOC, 103) = convert(VARCHAR(20), @date, 103)
					AND E.UserID IN (
						SELECT UserId
						FROM dbo.UserInformation
						WHERE ParentId IN (
								SELECT UserId
								FROM dbo.UserInformation
								WHERE ParentId = @UserID
								)
						)
				GROUP BY SM.ServiceName
					,RPM.RechargeProviderName
					,OM.OperatorName
					,E.[DMRProviderID]
					,E.[DMROperatorID]
					,E.[DMRServiceID]
				)
		ORDER BY count DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[DistributorRechargeSelectTodaysPendingRech]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[DistributorRechargeSelectTodaysPendingRech] @UserID INT  
 ,@date DATE = NULL  
AS  
BEGIN  
 IF (@date IS NULL)  
 BEGIN  
  SET @date = (switchoffset(sysdatetimeoffset(), '+05:30'))  
 END  
  
 DECLARE @UserTypeId INT  
  
 SELECT @UserTypeId = UserTypeId  
 FROM UserInformation  
 WHERE UserId = @UserID  
  
 IF (@UserTypeId = 2)  
 BEGIN  
  (  
    SELECT R.OperatorID  
     ,OM.OperatorName  
     ,sum(R.Amount) AS Amount  
     ,Count(R.RechargeID) AS Count  
    FROM OperatorMaster OM  
    INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID  
    INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
    INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID  
    INNER JOIN UserInformation UI ON UI.UserID = R.UserID  
    WHERE R.STATUS = Upper('Process')  
     AND convert(VARCHAR(20), R.DOC, 103) = convert(VARCHAR(20), @date, 103)  
     AND @UserID IN (UI.ParentID)  
    GROUP BY SM.ServiceName  
     ,RPM.RechargeProviderName  
     ,OM.OperatorName  
     ,R.RechargeProviderID  
     ,R.OperatorID  
     ,R.ServiceID  

    UNION ALL  
      
    SELECT E.[DMROperatorID]  
     ,OM.OperatorName  
     ,sum(E.Amount) AS Amount  
     ,Count(E.[MoneyTransferID]) AS Count  
    FROM OperatorMaster OM  
    INNER JOIN [dbo].[MoneyTransfer] E ON OM.OperatorID = E.[DMROperatorID]  
    INNER JOIN ServiceMaster SM ON E.[DMRServiceID] = SM.ServiceID  
    INNER JOIN RechargeProviderMaster RPM ON E.[DMRProviderID] = RPM.RechargeProviderID  
    INNER JOIN UserInformation UI ON UI.UserID = E.UserID  
    WHERE E.STATUS = Upper('Process')  
     AND convert(VARCHAR(20), E.DOC, 103) = convert(VARCHAR(20), @date, 103)  
     AND @UserID IN (UI.ParentID)  
    GROUP BY SM.ServiceName  
     ,RPM.RechargeProviderName  
     ,OM.OperatorName  
     ,E.[DMRProviderID]  
     ,E.[DMROperatorID]  
     ,E.[DMRServiceID]  
    )  
  ORDER BY Count DESC  
 END  
  
 IF (@UserTypeId = 7)  
 BEGIN  
  (  
    SELECT R.OperatorID  
     ,OM.OperatorName  
     ,sum(R.Amount) AS Amount  
     ,Count(R.RechargeID) AS Count  
    FROM OperatorMaster OM  
    INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID  
    INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
    INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID  
    INNER JOIN UserInformation UI ON UI.UserID = R.UserID  
    WHERE R.STATUS = Upper('Process')  
     AND convert(VARCHAR(20), R.DOC, 103) = convert(VARCHAR(20), @date, 103)  
     AND R.UserID IN (  
      SELECT UserId  
      FROM dbo.UserInformation  
      WHERE ParentId IN (  
        SELECT UserId  
        FROM dbo.UserInformation  
        WHERE ParentId = @UserID  
        )  
      )  
    GROUP BY SM.ServiceName  
     ,RPM.RechargeProviderName  
     ,OM.OperatorName  
     ,R.RechargeProviderID  
     ,R.OperatorID  
     ,R.ServiceID  
 
    UNION ALL  
      
    SELECT E.[DMROperatorID]  
     ,OM.OperatorName  
     ,sum(E.Amount) AS Amount  
     ,Count(E.[MoneyTransferID]) AS Count  
    FROM OperatorMaster OM  
    INNER JOIN [dbo].[MoneyTransfer] E ON OM.OperatorID = E.[DMROperatorID]  
    INNER JOIN ServiceMaster SM ON E.[DMRServiceID] = SM.ServiceID  
    INNER JOIN RechargeProviderMaster RPM ON E.[DMRProviderID] = RPM.RechargeProviderID  
    INNER JOIN UserInformation UI ON UI.UserID = E.UserID  
    WHERE E.STATUS = Upper('Process')  
     AND convert(VARCHAR(20), E.DOC, 103) = convert(VARCHAR(20), @date, 103)  
     AND E.UserID IN (  
      SELECT UserId  
      FROM dbo.UserInformation  
      WHERE ParentId IN (  
        SELECT UserId  
        FROM dbo.UserInformation  
        WHERE ParentId = @UserID  
        )  
      )  
    GROUP BY SM.ServiceName  
     ,RPM.RechargeProviderName  
     ,OM.OperatorName  
     ,E.[DMRProviderID]  
     ,E.[DMROperatorID]  
     ,E.[DMRServiceID]  
    )  
  ORDER BY Count DESC  
 END  
END  


GO
/****** Object:  StoredProcedure [dbo].[DistributorRechargeSelectTodaysSuccessRecharge]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
CREATE PROCEDURE [dbo].[DistributorRechargeSelectTodaysSuccessRecharge] @UserID INT    
 ,@date DATE = NULL    
AS    
BEGIN    
 Declare @DistType int = 2  
 Declare @SuperDistType int = 8  
  
 IF (@date IS NULL)    
 BEGIN    
  SET @date = (switchoffset(sysdatetimeoffset(), '+05:30'))    
 END    
    
 DECLARE @UserTypeId INT    
    
 SELECT @UserTypeId = UserTypeId    
 FROM UserInformation    
 WHERE UserId = @UserID    
    
 IF (@UserTypeId = @DistType)    
 BEGIN    
  (    
    SELECT R.OperatorID    
     ,OM.OperatorName    
     ,sum(R.Amount) AS Amount    
     ,Count(R.RechargeID) AS Count    
    FROM OperatorMaster OM    
    INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID    
    INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID    
    INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID    
    INNER JOIN UserInformation UI ON UI.UserID = R.UserID    
    WHERE R.STATUS = Upper('Success')    
     AND convert(VARCHAR(20), R.DOC, 103) = convert(VARCHAR(20), @date, 103)    
     AND @UserID IN (UI.ParentID)    
    GROUP BY SM.ServiceName    
     ,RPM.RechargeProviderName    
     ,OM.OperatorName    
     ,R.RechargeProviderID    
     ,R.OperatorID    
     ,R.ServiceID    
    
    UNION ALL    
        
    SELECT M.[DMROperatorID]    
     ,OM.OperatorName    
     ,sum(M.Amount) AS Amount    
     ,Count(M.[MoneyTransferID]) AS Count    
    FROM OperatorMaster OM    
    INNER JOIN [dbo].[MoneyTransfer] M ON OM.OperatorID = M.[DMROperatorID]    
    INNER JOIN ServiceMaster SM ON M.[DMRServiceID] = SM.ServiceID    
    INNER JOIN RechargeProviderMaster RPM ON M.DMRProviderID = RPM.RechargeProviderID    
    INNER JOIN UserInformation UI ON UI.UserID = M.UserID    
    WHERE M.STATUS = Upper('Success')    
     AND convert(VARCHAR(20), M.DOC, 103) = convert(VARCHAR(20), @date, 103)    
     AND @UserID IN (UI.ParentID)    
    GROUP BY SM.ServiceName    
     ,RPM.RechargeProviderName    
     ,OM.OperatorName    
     ,M.[DMRProviderID]    
     ,M.[DMROperatorID]    
     ,M.[DMRServiceID]    
    )    
  ORDER BY Count DESC    
 END    
    
 IF (@UserTypeId = @SuperDistType)    
 BEGIN    
  (    
    SELECT R.OperatorID    
     ,OM.OperatorName    
     ,sum(R.Amount) AS Amount    
     ,Count(R.RechargeID) AS Count    
    FROM OperatorMaster OM    
    INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID    
    INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID    
    INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID    
    INNER JOIN UserInformation UI ON UI.UserID = R.UserID    
    WHERE R.STATUS = Upper('Success')    
     AND convert(VARCHAR(20), R.DOC, 103) = convert(VARCHAR(20), @date, 103)    
     AND R.UserID IN (    
      SELECT UserId    
      FROM dbo.UserInformation    
      WHERE ParentId IN (    
        SELECT UserId    
        FROM dbo.UserInformation    
        WHERE ParentId = @UserID    
        )    
      )    
    GROUP BY SM.ServiceName    
     ,RPM.RechargeProviderName    
     ,OM.OperatorName    
     ,R.RechargeProviderID    
     ,R.OperatorID    
     ,R.ServiceID    

    UNION ALL    
        
    SELECT M.[DMROperatorID]    
     ,OM.OperatorName    
     ,sum(M.Amount) AS Amount    
     ,Count(M.[MoneyTransferID]) AS Count    
    FROM OperatorMaster OM    
    INNER JOIN [dbo].[MoneyTransfer] M ON OM.OperatorID = M.[DMROperatorID]    
    INNER JOIN ServiceMaster SM ON M.[DMRServiceID] = SM.ServiceID    
    INNER JOIN RechargeProviderMaster RPM ON M.DMRProviderID = RPM.RechargeProviderID    
    INNER JOIN UserInformation UI ON UI.UserID = M.UserID    
    WHERE M.STATUS = Upper('Success')    
     AND convert(VARCHAR(20), M.DOC, 103) = convert(VARCHAR(20), @date, 103)    
     AND M.UserID IN (    
      SELECT UserId    
      FROM dbo.UserInformation    
      WHERE ParentId IN (    
        SELECT UserId    
        FROM dbo.UserInformation    
        WHERE ParentId = @UserID    
        )    
      )    
    GROUP BY SM.ServiceName    
     ,RPM.RechargeProviderName    
     ,OM.OperatorName    
     ,M.[DMRProviderID]    
     ,M.[DMROperatorID]    
     ,M.[DMRServiceID]    
    )    
  ORDER BY Count DESC    
 END    
END 


GO
/****** Object:  StoredProcedure [dbo].[ElectricityBillAmountRefund]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================                    
-- Author:  <Sainath Bhujbal>                    
-- ALTER  date: <16 March 2017 ,>                    
-- Description: <Description,,>                    
-- =============================================         
CREATE PROCEDURE [dbo].[ElectricityBillAmountRefund] @UserID INT  
 --,@RECHARGE_TO VARCHAR(1)        
 ,@TransType VARCHAR(10)  
 ,@ServiceID INT  
 ,@OpeningBalance NUMERIC(18, 5)  
 ,@Amount NUMERIC(18, 5) = 0  
 ,@Discount NUMERIC(18, 5) = 0  
 ,@ConsumerNo VARCHAR(20) = NULL  
 ,@OperatorID INT = NULL  
 ,@Status VARCHAR(50) = NULL  
 ,@RechargeDescription VARCHAR(100) = NULL  
 ,@RechargeProviderID INT  
 ,@Through VARCHAR(100) = NULL  
 ,@IPAddress VARCHAR(50) = NULL  
 ,@RechargeId INT = 0  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 DECLARE @RESULT INT  
 DECLARE @comAmt NUMERIC(18, 5) = 0;  
 DECLARE @success VARCHAR(20) = 'SUCCESS';  
 DECLARE @failure VARCHAR(20) = 'FAILURE';  
 DECLARE @refund VARCHAR(20) = 'REFUND';  
 DECLARE @Output INT  
  
 SET @Output = 0  
  
 IF (@Status = (@failure))  
 BEGIN  
  SET @TransType = 'REFUNDED'  
  SET @Status = @refund  
  
  --STEP1.1: SELECT USER BALANCE BEFORE TRANSCTION...      
  DECLARE @beforeTransAmt NUMERIC(18, 5);  
  
  SELECT @beforeTransAmt = CurrentBalance  
  FROM DBO.UserBalance(NOLOCK)  
  WHERE UserID = @UserID  
  
  UPDATE dbo.Recharge  
  SET RechargeProviderID = @RechargeProviderID  
   ,RechargeDescription = @RechargeDescription  
   ,[STATUS] = @Status  
   ,TransactionType = @TransType  
   ,OpeningBalance = @beforeTransAmt  
   ,UpdateDoc = (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))  
  WHERE RechargeID = @RechargeId  
  
  SELECT @RESULT = @@ROWCOUNT  
  
  SET @Output = @RESULT  
  
  --DECLARE @TransDesc VARCHAR(100)        
  EXEC dbo.[TransactionDetailsRefundInsert] @UserID  
   ,@RechargeID  
   ,@ServiceID  
   ,@ConsumerNo  
   ,@Amount  
   ,@OperatorID  
   ,@RechargeProviderID  
 END  
  
 RETURN @Output  
END  


GO
/****** Object:  StoredProcedure [dbo].[ElectricityBillingUnitsSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[ElectricityBillingUnitsSelect]
AS
BEGIN
	SET NOCOUNT ON;

	--Select '0' as Unit,'--Select--' as BillingUnitName    
	--union    
	SELECT Unit
		,BillingUnitName
	FROM dbo.ElectricityBillingUnits(NOLOCK)
	ORDER BY UNIT
END



GO
/****** Object:  StoredProcedure [dbo].[ElectricityBillSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
--==========================================        
--Auther :Priyanka        
--Date :27-2-17        
--Purpose Recharge report        
--==========================================   
--ElectricityBillSelect 1  
CREATE PROCEDURE [dbo].[ElectricityBillSelect] @UserID INT  
AS  
BEGIN  
 DECLARE @UserTypeID INT;  
 DECLARE @retailer VARCHAR(20) = 'RETAILER';  
 DECLARE @Customer VARCHAR(20) = 'Customer';  
 DECLARE @distributor VARCHAR(20) = 'DISTRIBUTOR';  
 DECLARE @admin VARCHAR(20) = 'ADMIN';  
 DECLARE @apiUser VARCHAR(20) = 'API USER';  
 DECLARE @userType VARCHAR(20);  
  
 SELECT @userType = UTM.UserType  
 FROM [dbo].UserInformation(NOLOCK) UI  
 INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID  
 WHERE UI.UserID = @USERID  
  
 SET @UserTypeID = (  
   SELECT UserTypeID  
   FROM UserInformation  
   WHERE UserID = @UserID  
   )  
  
 --Admin Recharge Report        
 IF (Upper(@userType) = Upper(@admin))  
 BEGIN  
  SELECT *  
  FROM (  
   SELECT TOP 1000 R.UserID  
    ,R.TransactionID  
    ,TD.RechargeID  
    ,TD.serviceID  
    ,TD.ConsumerNumber  
    ,TD.DOCType  
    ,TD.OpeningBalance  
    ,TD.ClosingBalance  
    ,TD.TransactionAmount AS Amount  
    ,TD.DiscountAmount  
    ,TD.AmountCreditDebit  
    ,TD.CommissionCreditDebit  
    ,convert(VARCHAR, R.DOC, 113) AS DOC  
    ,R.IsDispute  
    ,R.OperatorID  
    ,SM.ServiceName  
    ,OM.OperatorName  
    ,UI.UserName  
    ,UI.UserTypeID  
    ,R.STATUS  
    ,UIS.UserID AS ParentID  
    ,UIS.UserName AS ParentName  
    ,UIS.UserTypeID AS DUserTypeID  
    ,ROW_NUMBER() OVER (  
     PARTITION BY TD.RechargeID ORDER BY R.DOC DESC  
     ) [SR]  
   FROM TransactionDetails TD  
   INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID  
    AND R.UserID = TD.UserID  
   INNER JOIN UserInformation UI ON TD.UserID = UI.UserID  
   INNER JOIN UserInformation UIS ON UIS.UserID = UI.ParentID  
   INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
   INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID  
   ORDER BY convert(DATETIME, R.DOC, 103) DESC  
   ) t   
  WHERE [SR] = 1  
 END  
  
 --Distributor Recharge Report        
 IF (Upper(@userType) = Upper(@distributor))  
 BEGIN  
  SELECT *  
  FROM (  
   SELECT TOP 1000 R.UserID  
    ,R.TransactionID  
    ,TD.RechargeID  
    ,TD.serviceID  
    ,TD.ConsumerNumber  
    ,TD.DOCType  
    ,TD.OpeningBalance  
    ,TD.ClosingBalance  
    ,TD.TransactionAmount AS Amount  
    ,TD.DiscountAmount  
    ,TD.AmountCreditDebit  
    ,TD.CommissionCreditDebit  
    ,convert(VARCHAR, R.DOC, 113) AS DOC  
    ,R.IsDispute  
    ,R.OperatorID  
    ,SM.ServiceName  
    ,OM.OperatorName  
    ,UI.UserName  
    ,UI.UserTypeID  
    ,R.STATUS  
    ,UIS.UserID AS ParentID  
    ,UIS.UserName AS ParentName  
    ,UIS.UserTypeID AS DUserTypeID  
    ,ROW_NUMBER() OVER (  
     PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC  
     ) [SR]  
   FROM TransactionDetails TD  
   INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID  
    AND R.UserID = TD.UserID  
   INNER JOIN UserInformation UI ON TD.UserID = UI.UserID  
   INNER JOIN UserInformation UIS ON UIS.UserID = UI.ParentID  
   INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
   INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID  
   WHERE 8 IN (  
     TD.UserID  
     ,UI.ParentID  
     )  
   ORDER BY convert(DATETIME, R.DOC, 103) DESC  
   ) t  
  WHERE [SR] = 1  
 END  
  
 --Retailer,Customer Recharge Report        
 IF (  
   Upper(@userType) = Upper(@retailer)  
   OR Upper(@userType) = Upper(@Customer)  
   )  
 BEGIN  
  SELECT *  
  FROM (  
   SELECT TOP 1000 R.UserID  
    ,R.TransactionID  
    ,TD.RechargeID  
    ,TD.serviceID  
    ,TD.ConsumerNumber  
    ,TD.DOCType  
    ,TD.OpeningBalance  
    ,TD.ClosingBalance  
    ,TD.TransactionAmount AS Amount  
    ,TD.DiscountAmount  
    ,TD.AmountCreditDebit  
    ,TD.CommissionCreditDebit  
    ,convert(VARCHAR, R.DOC, 113) AS DOC  
    ,R.IsDispute  
    ,R.OperatorID  
    ,SM.ServiceName  
    ,OM.OperatorName  
    ,UI.UserName  
    ,UI.UserTypeID  
    ,R.STATUS  
    ,UIS.UserID AS ParentID  
    ,UIS.UserName AS ParentName  
    ,UIS.UserTypeID AS DUserTypeID  
    ,ROW_NUMBER() OVER (  
     PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC  
     ) [SR]  
   FROM TransactionDetails TD  
   INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID  
    AND R.UserID = TD.UserID  
   INNER JOIN UserInformation UI ON TD.UserID = UI.UserID  
   INNER JOIN UserInformation UIS ON UIS.UserID = UI.ParentID  
   INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
   INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID  
   WHERE TD.UserID = 3  
   ORDER BY convert(DATETIME, R.DOC, 103) DESC  
   ) t  
  WHERE [SR] = 1  
 END  
END  


GO
/****** Object:  StoredProcedure [dbo].[ElectricityConfigurationSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Sushant Yelpale    
-- Create date: 23/10/2018    
-- Description: Get Electricity URL from table    
-- [ElectricityConfigurationSelect] 'SOUTHCO - ODISHA'    
-- =============================================    
CREATE PROCEDURE [dbo].[ElectricityConfigurationSelect]    
 @OperatorName varchar(50)    
AS    
BEGIN    
 SELECT     
 [Opcode]    
 ,[ParamJson]    
  FROM [dbo].[ElectricityConfiguration]    
  where UPPER(RTRIM(LTRIM([OperatorName]))) = UPPER(RTRIM(LTRIM(@OperatorName)))    
END 


GO
/****** Object:  StoredProcedure [dbo].[EmailIDvalidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:Priyanka Deshmukh
-- ALTER  date:5-4-2017
-- Description:EmailID validation
-- =============================================  
CREATE PROCEDURE [dbo].[EmailIDvalidation] @EmailID VARCHAR(100)
	,@UserTypeID INT
AS
BEGIN
	DECLARE @Customer INT = 6;

	IF (@UserTypeID = @Customer)
	BEGIN
		SELECT UserID
		FROM [dbo].UserInformation
		WHERE EmailID = @EmailID
			AND IsActive = 1
	END

	IF (@UserTypeID <> @Customer)
	BEGIN
		SELECT UserID
		FROM [dbo].UserInformation
		WHERE EmailID = @EmailID
	END
END



GO
/****** Object:  StoredProcedure [dbo].[EnquiryDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :1-3-17
--Purpose Enquiry Select
--==========================================
CREATE PROCEDURE [dbo].[EnquiryDelete] @ContactUsID INT
AS
BEGIN
	UPDATE Enquiry
	SET IsDeleted = 1
	WHERE ContactUsID = @ContactUsID
END



GO
/****** Object:  StoredProcedure [dbo].[EnquiryInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EnquiryInsert] @Name VARCHAR(50)
	,@Email VARCHAR(50)
	,@MobileNumber VARCHAR(50)
	,@Message VARCHAR(50)
AS
BEGIN
	INSERT INTO Enquiry (
		name
		,Email
		,MobileNumber
		,Message
		)
	VALUES (
		@Name
		,@Email
		,@MobileNumber
		,@Message
		)

	DECLARE @RefNumber INT;

	SET @RefNumber = (
			SELECT @@identity
			)

	RETURN @RefNumber;
END



GO
/****** Object:  StoredProcedure [dbo].[EnquirySelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :1-3-17
--Purpose Enquiry Select
--==========================================
CREATE PROCEDURE [dbo].[EnquirySelect]
AS
BEGIN
	SELECT TOP (1000) [ContactUsID]
		,[Name]
		,[Email]
		,[MobileNumber]
		,[Message]
		,convert(VARCHAR, DOC, 113) AS DOC
		,[IsDeleted]
	FROM Enquiry
	WHERE IsDeleted = 0
	ORDER BY convert(DATETIME, DOC, 103) DESC
END



GO
/****** Object:  StoredProcedure [dbo].[FCMHistoryInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		Sushant Yelaple
-- ALTER  date: 3/ Sept 2018
-- Description:	Insert FCm History
-- =============================================
CREATE PROCEDURE [dbo].[FCMHistoryInsert] @To VARCHAR(500)
	,@Message VARCHAR(5000)
	,@Type VARCHAR(20)
AS
BEGIN
	INSERT INTO [dbo].[FCMHistory] (
		[To]
		,[Message]
		,[Type]
		)
	VALUES (
		@To
		,@Message
		,@Type
		)
END



GO
/****** Object:  StoredProcedure [dbo].[getAllCallBackURL]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[getAllCallBackURL] @UserID VARCHAR(100)
AS
BEGIN
	SELECT UserID
		,UserName
		,CallBackUrl
		,IP
	FROM dbo.UserInformation(NOLOCK)
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[getAPIResponseHitBack]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getAPIResponseHitBack]
AS
BEGIN
	SELECT R.UserID AS RegID
		,R.ConsumerNumber AS MobileNO_vcno
		,cast(R.Amount AS INT) AS Amount
		,R.STATUS
		,R.APIRequestID AS API_RequestID
		,R.TransactionID AS TX_ID
		,T.ClosingBalance AS CL_BAL
		,R.RechargeID AS Recharge_id
		,U.CallBackUrl
	FROM dbo.UserInformation(NOLOCK) AS U
	INNER JOIN dbo.Recharge(NOLOCK) AS R ON U.UserID = R.UserID
	INNER JOIN dbo.TransactionDetails AS T ON T.UserID = R.UserID
	WHERE U.UserTypeID = '4'
		AND R.HitBack = 0
		AND T.RechargeID = R.RechargeID
		AND R.STATUS NOT IN ('PROCESS')
END



GO
/****** Object:  StoredProcedure [dbo].[GetCities]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[GetCities] @StateID INT
AS
BEGIN
	SELECT CityMasterID AS ID
		,CityName AS name
	FROM dbo.CityMaster
	WHERE StateID = @StateID
END



GO
/****** Object:  StoredProcedure [dbo].[GetCyberData]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[GetCyberData] @OperatorID VARCHAR(20)
AS
BEGIN
	SELECT *
	FROM dbo.CyberOperators
	WHERE OperatorID = @OperatorID
END



GO
/****** Object:  StoredProcedure [dbo].[getOperatorIDbyCode]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getOperatorIDbyCode] --getOperatorIDbyCode VD  
	@OpCode VARCHAR(30)
AS
BEGIN
	SELECT TOP 1 OperatorID
		,ServiceID
		,OperatorName
	FROM [dbo].[OperatorMaster]
	WHERE [OperatorCode] = @OpCode
END



GO
/****** Object:  StoredProcedure [dbo].[getRechargeDetails]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[getRechargeDetails] --[getRechargeDetails] 4840 
	@RechargeId VARCHAR(500)
AS
BEGIN
	SELECT TOP 1 R.[RechargeID] AS RechargeID
		,R.Amount
		,UI.[MobileNumber] AS MobileNumber
		,OM.OperatorName AS OperatorName
		,TD.ClosingBalance AS ClosingBal
		,convert(VARCHAR, R.DOC, 100) AS DOC
		,ISNULL((
				SELECT Sum([Amount])
				FROM [dbo].[Recharge](NOLOCK)
				WHERE [Status] = 'SUCCESS'
					AND UserID = R.[UserID]
					AND convert(DATETIME, DOC) = getdate()
				), 0) AS Sales
		,TD.[TransactionAmount] - TD.[DiscountAmount] AS Debit
	FROM [dbo].[Recharge](NOLOCK) AS R
	INNER JOIN [dbo].[UserInformation](NOLOCK) AS UI ON R.[UserID] = UI.[UserID]
	INNER JOIN [dbo].[OperatorMaster](NOLOCK) AS OM ON R.OperatorID = OM.OperatorID
	INNER JOIN [dbo].[TransactionDetails](NOLOCK) AS TD ON R.RechargeID = TD.RechargeID
	WHERE R.[RechargeID] = @RechargeId
		AND R.[Through] = 'SMS'
		AND TD.[UserID] = R.[UserID]
		AND (
			TD.[DOCType] = 'RECHARGE'
			OR TD.[DOCType] = 'REFUND'
			)
END



GO
/****** Object:  StoredProcedure [dbo].[getRechargeIDbyRequestID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[getRechargeIDbyRequestID] --[getOperatorIDbyCode] AT    
	@RequestID VARCHAR(100)
	,@RechargeProviderId INT
AS
BEGIN
	SELECT TOP 1 [RechargeID]
	FROM [dbo].[Recharge](NOLOCK)
	WHERE [RequestID] = @RequestID
		AND [RechargeProviderID] = @RechargeProviderId
END



GO
/****** Object:  StoredProcedure [dbo].[GetStates]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[GetStates]
AS
BEGIN
	SELECT StateID
		,name
	FROM dbo.StateMaster
END



GO
/****** Object:  StoredProcedure [dbo].[getUserIDByMobile]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[getUserIDByMobile] --[getOperatorIDbyCode] AT  
	@SenderNumber VARCHAR(10)
AS
BEGIN
	SELECT TOP 1 [UserID]
	FROM [dbo].[UserInformation](NOLOCK)
	WHERE [MobileNumber] = @SenderNumber
END


GO
/****** Object:  StoredProcedure [dbo].[getVerficationCharge]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[getVerficationCharge]              
 @BeneOperatorID int        
AS              
BEGIN              
select CONVERT(DECIMAL(10, 2), abs(RetailerCommissionAmount)) AS Charge              
from operatormaster           
where operatorid = @BeneOperatorID        
END 


GO
/****** Object:  StoredProcedure [dbo].[GroupDetailInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[GroupDetailInsert] --5,5000,-5,1,0,1,0,1,1
	@FromAmount DECIMAL(18, 5)
	,@ToAmount DECIMAL(18, 5)
	,@RetailerFixRs DECIMAL(18, 5)
	,@RetailerFlexi DECIMAL(18, 5)
	,@DistributorFixRs DECIMAL(18, 5)
	,@DistributorFlexi DECIMAL(18, 5)
	,@SuperDistributorFixRs DECIMAL(18, 5)
	,@SuperDistributorFlexi DECIMAL(18, 5)
	,@GroupID INT
AS
BEGIN
	IF EXISTS (
			SELECT *
			FROM [dbo].[GroupDetail]
			--WHERE @FromAmount >= [FromAmount]      
			-- AND @ToAmount <= [ToAmount]      
			-- and [GroupID]=@GroupID        
			WHERE (
					@ToAmount >= [FromAmount]
					AND @ToAmount <= [ToAmount]
					AND [GroupID] = @GroupID
					)
				OR (
					@FromAmount >= [FromAmount]
					AND @ToAmount <= [ToAmount]
					AND [GroupID] = @GroupID
					)
				OR (
					@FromAmount >= [FromAmount]
					AND @FromAmount <= [ToAmount]
					AND [GroupID] = @GroupID
					)
			)
	BEGIN
		RETURN;
	END

	INSERT INTO [dbo].[GroupDetail] (
		[FromAmount]
		,[ToAmount]
		,[RetailerFixRs]
		,[RetailerFlexi]
		,[DistributorFixRs]
		,[DistributorFlexi]
		,[SuperDistributorFixRs]
		,[SuperDistributorFlexi]
		,[GroupID]
		)
	VALUES (
		@FromAmount
		,@ToAmount
		,@RetailerFixRs
		,@RetailerFlexi
		,@DistributorFixRs
		,@DistributorFlexi
		,@SuperDistributorFixRs
		,@SuperDistributorFlexi
		,@GroupID
		)
END



GO
/****** Object:  StoredProcedure [dbo].[GroupDetailSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[GroupDetailSelect]
AS
BEGIN
	SELECT GD.ID
		,[FromAmount]
		,[ToAmount]
		,[RetailerFixRs]
		,[RetailerFlexi]
		,[DistributorFixRs]
		,[DistributorFlexi]
		,[SuperDistributorFixRs]
		,[SuperDistributorFlexi]
		,[GroupID]
		,GM.[GroupName]
	FROM [GroupDetail] GD
	INNER JOIN [dbo].[GroupMaster] GM ON GM.[ID] = GD.[GroupID]
END



GO
/****** Object:  StoredProcedure [dbo].[GroupDetailUpdateByID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[GroupDetailUpdateByID] @GroupID INT
	,@FromAmount DECIMAL(18, 5)
	,@ToAmount DECIMAL(18, 5)
	,@RetailerFixRs DECIMAL(18, 5)
	,@RetailerFlexi DECIMAL(18, 5)
	,@DistributorFixRs DECIMAL(18, 5)
	,@DistributorFlexi DECIMAL(18, 5)
	,@SuperDistributorFixRs DECIMAL(18, 5)
	,@SuperDistributorFlexi DECIMAL(18, 5)
	,@ID INT
AS
BEGIN
	UPDATE [dbo].[GroupDetail]
	SET [RetailerFixRs] = @RetailerFixRs
		,[RetailerFlexi] = @RetailerFlexi
		,[DistributorFixRs] = @DistributorFixRs
		,[DistributorFlexi] = @DistributorFlexi
		,[SuperDistributorFixRs] = @SuperDistributorFixRs
		,[SuperDistributorFlexi] = @SuperDistributorFlexi
	WHERE ID = @ID
END



GO
/****** Object:  StoredProcedure [dbo].[GroupMasterDeleteGroup]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-------------
 --Sainath Bhujbal
 --22-Nov-2018
 -------------
CREATE PROCEDURE[dbo].[GroupMasterDeleteGroup] @ID INT

AS

BEGIN

	DELETE

	FROM [GroupDetail]

	WHERE [GroupID] = @ID



	DELETE

	FROM [UserwiseSlabCommission]

	WHERE [GroupID] = @ID



	DELETE

	FROM [dbo].[GroupMaster]

	WHERE ID = @ID

	

END







GO
/****** Object:  StoredProcedure [dbo].[GroupMasterInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--GroupMasterInsert Test
CREATE PROCEDURE[dbo].[GroupMasterInsert] @GroupName VARCHAR(50)
AS
BEGIN
	IF EXISTS (
			SELECT *
			FROM [dbo].[GroupMaster]
			WHERE [GroupName] = @GroupName
			)
	BEGIN
		RETURN;
	END

	INSERT INTO [dbo].[GroupMaster] ([GroupName])
	VALUES (@GroupName)
END



GO
/****** Object:  StoredProcedure [dbo].[GroupMasterSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[GroupMasterSelect]
AS
BEGIN
	SELECT [ID]
		,[GroupName]
	FROM [dbo].[GroupMaster](NOLOCK)
END



GO
/****** Object:  StoredProcedure [dbo].[GroupMasterUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[GroupMasterUpdate] @GroupName VARCHAR(50)
	,@ID INT
AS
BEGIN
	IF EXISTS (
			SELECT *
			FROM [dbo].[GroupMaster]
			WHERE [GroupName] = @GroupName
			)
	BEGIN
		RETURN;
	END

	UPDATE [dbo].[GroupMaster]
	SET [GroupName] = @GroupName
	WHERE ID = @ID
END



GO
/****** Object:  StoredProcedure [dbo].[GSTTDSSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:  Sushant Yelpale      
-- Create date: 01/01/2019      
-- Description: Selects GST and TDS for transaction      
-- =============================================      
CREATE PROCEDURE [dbo].[GSTTDSSelect]      
 @Amount Decimal(18,5)      
 ,@Commission Decimal(18,5)      
 ,@ServiceID Int      
 ,@GST Decimal(18,5) OUT      
 ,@TDS Decimal(18,5) OUT      
AS      
BEGIN      
 Declare @GSTPercentage Decimal(18,5);      
 Declare @TDSPercentage Decimal(18,5);      
 Declare @CCFPercentage Decimal(18,5);      
 Declare @CCFwithpercentage Decimal(18,5);      
      
 SET @Commission = @Commission * (-1);      
       
 SET @GST = 0;      
 SET @TDS = 0;   
   
 if(@Amount<=1)  
 begin   
  Return;   
 end     
      
 Select       
 @GSTPercentage = IsNull(GST, 0)      
 ,@TDSPercentage = IsNull(TDS, 0)      
 ,@CCFPercentage = IsNull(CCF, 0)      
 From TaxMaster      
 where ServiceID = @ServiceID      
     
 IF @GSTPercentage is NULL      
 BEGIN      
  SET @GSTPercentage = 0      
 END      
      
 IF @TDSPercentage is NULL      
 BEGIN      
  SET @TDSPercentage = 0      
 END      
      
 IF @CCFPercentage is NULL      
 BEGIN      
  SET @CCFPercentage = 0      
 END      
      
  IF @GSTPercentage =0 AND @TDSPercentage=0 And @CCFPercentage=0    
  BEGIN    
 Return;    
  END    
    
 PRINT @TDSPercentage      
      
 IF((@Amount * (@CCFPercentage/100)) > 6)      
 BEGIN      
  SET @CCFwithpercentage = (@Amount *(@CCFPercentage/100))      
 END      
 ELSE      
 BEGIN      
  SET @CCFwithpercentage = 6;  -- Minimum Percentage      
 END      
      
 PRINT '@CCFwithpercentage ' + Convert(varchar(50),@CCFwithpercentage);      
    
  PRINT '@Commission ' + Convert(varchar(50),@Commission);      
    
 Declare @BasicAmount Decimal(18,5);      
 SET @BasicAmount = ISNULL((@CCFwithpercentage - @Commission),0)      
 PRINT '@BasicAmount ' + Convert(varchar(50),@BasicAmount);      
      
 BEGIN try      
  SET @GST = @CCFwithpercentage - (@CCFwithpercentage/@GSTPercentage)      
 END try      
 BEGIN Catch      
  SET @GST =  0;      
 END Catch      
      
   PRINT '@GST ' + Convert(varchar(50),@GST);      
    
 BEGIN try      
  SET @TDS = (@BasicAmount-@GST)*(@TDSPercentage/100)      
 END try      
 BEGIN Catch      
  SET @TDS = 0;      
 END Catch      
      
 --PRINT @TDSPercentage     
 PRINT '@TDS ' + Convert(varchar(50),@TDS);      
 SET @TDS = 0;      
 IF @TDS < 0       
 BEGIN      
  SET @TDS = 0;      
 END      
      
END 
GO
/****** Object:  StoredProcedure [dbo].[InboxInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <Sainath Bhujbal>        
-- ALTER  date: <21 Feb 2017 ,>        
-- Description: <Description,,>        
-- =============================================        
CREATE PROCEDURE [dbo].[InboxInsert] @ServiceID INT
	,@UserID INT
	,@Amount NUMERIC(18, 5)
	,@Number VARCHAR(20)
	,@OperatorID INT
AS
BEGIN
	DECLARE @Output INT;

	INSERT INTO Inbox (
		ServiceID
		,UserID
		,Amount
		,ConsumerNumber
		,OperatorID
		)
	VALUES (
		@ServiceID
		,@UserID
		,@Amount
		,@Number
		,@OperatorID
		)

	SET @Output = @@IDENTITY

	RETURN @Output
END



GO
/****** Object:  StoredProcedure [dbo].[insertAPIUserResponse]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[insertAPIUserResponse] @CallBackResponse VARCHAR(max)
	,@Message VARCHAR(max)
AS
BEGIN
	INSERT INTO dbo.APIUserResponse (
		Response
		,Message
		)
	VALUES (
		@CallBackResponse
		,@Message
		);
END



GO
/****** Object:  StoredProcedure [dbo].[insertCallbackURL]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[insertCallbackURL] @UserID VARCHAR(50)
	,@CallBackURL VARCHAR(MAX)
	,@IpAddress VARCHAR(50)
AS
BEGIN
	UPDATE [dbo].[UserInformation]
	SET [CallBackUrl] = @CallBackURL
		,[IP] = @IpAddress
	WHERE [UserID] = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[InsertUserServiceMapping]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertUserServiceMapping] @UserId INT
	,@ServiceId INT
	,@IsActive BIT
AS
BEGIN
	INSERT dbo.UserServiceMapping (
		UserID
		,ServiceID
		,IsActive
		)
	VALUES (
		@UserId
		,@ServiceId
		,@IsActive
		)
END



GO
/****** Object:  StoredProcedure [dbo].[IsRefCodePresent]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[IsRefCodePresent] @ProviderID INT
	,@ResponsText VARCHAR(100)
AS
BEGIN
	SELECT IsRefCodePresent
	FROM dbo.ResponseGroup
	WHERE RechargeProviderID = @ProviderID
		AND [RegularExpression] LIKE '%' + @ResponsText + '%'
		AND IsRefCodePresent = 1
END



GO
/****** Object:  StoredProcedure [dbo].[KYCDocumentInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from kycdocument
CREATE PROCEDURE[dbo].[KYCDocumentInsert] @UserID INT
	,@DocumentName VARCHAR(50)
	,@Path VARCHAR(max)
AS
BEGIN
	SELECT [Path]
	FROM [kycdocument]
	WHERE [UserID] = @UserID
		AND [DocumentName] = @DocumentName

	IF NOT EXISTS (
			SELECT [Path]
			FROM [kycdocument]
			WHERE [UserID] = @UserID
				AND [DocumentName] = @DocumentName
			)
	BEGIN
		INSERT INTO [dbo].[kycdocument] (
			[UserID]
			,[DocumentName]
			,[Path]
			)
		VALUES (
			@UserID
			,@DocumentName
			,@Path
			);
	END
	ELSE
	BEGIN
		UPDATE [kycdocument]
		SET [Path] = @Path
		WHERE [UserID] = @UserID
			AND [DocumentName] = @DocumentName
	END
END



GO
/****** Object:  StoredProcedure [dbo].[KYCDocumentSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[KYCDocumentSelect] @UserID INT
AS
BEGIN
	SELECT [KYCID]
		,[DocumentName]
		,[Path]
		,[DOC]
	FROM [dbo].[KYCDocument]
	WHERE [UserID] = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[LoginHistoryInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================            
-- Author:  <Sainath Bhujbal>            
-- ALTER  date: <6 April 2017 ,>            
-- Description: <Login History Insert.>            
-- =============================================         
--LoginHistoryInsert 23,2 
CREATE PROCEDURE [dbo].[LoginHistoryInsert] @UserID INT
	,@IpAddress VARCHAR(50)
	,@Date VARCHAR(50)
	,@Through VARCHAR(50)
	,@Token VARCHAR(100)
AS
BEGIN
	INSERT INTO dbo.LoginHistory (
		UserID
		,ApplicationType
		,LastActivity
		,IPAddress
		,Token
		)
	VALUES (
		@UserID
		,@Through
		,@Date
		,@IpAddress
		,@Token
		)
END



GO
/****** Object:  StoredProcedure [dbo].[LoginHistoryselectToken]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---------------------------------------
--ALTER d By : Priyanka Deshmukh
--Reason : Select Token
--------------------------------------------
----Select * from LoginHistory order by doc desc
--[LoginHistoryselectToken] 1,'ANDROID','oD43af270j1OIEbhfz41'
CREATE PROCEDURE [dbo].[LoginHistoryselectToken] @UserID INT
	,@ApplicationType VARCHAR(100)
	,@Token VARCHAR(100)
AS
BEGIN
	DECLARE @NewToken VARCHAR(100);

	SET @NewToken = (
			SELECT TOP (1) Token
			FROM LoginHistory
			WHERE UserID = @UserID
				AND ApplicationType = @ApplicationType
			ORDER BY doc DESC
			)

	IF (@NewToken = @Token)
	BEGIN
		SELECT @NewToken
	END
END



GO
/****** Object:  StoredProcedure [dbo].[LoginHistoryselectToken_V1]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---------------------------------------  
--ALTER d By : Rahul Hembade
--Reason : Select Token multipal 
--------------------------------------------  
----Select * from LoginHistory order by doc desc  
--[LoginHistoryselectToken] 1,'ANDROID','oD43af270j1OIEbhfz41'  
CREATE PROCEDURE [dbo].[LoginHistoryselectToken_V1] @UserID INT
	,@ApplicationType VARCHAR(100)
	,@UserCount INT
	--,@Token varchar(100)  
AS
BEGIN
	DECLARE @NewToken VARCHAR(100);

	SELECT TOP (@UserCount) Token
	FROM dbo.LoginHistory(NOLOCK)
	WHERE UserID = @UserID
		AND ApplicationType = @ApplicationType
	ORDER BY LoginHistoryID DESC
END



GO
/****** Object:  StoredProcedure [dbo].[MessageDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[MessageDelete] @MessageID INT
	,@UserID INT
AS
BEGIN
	DELETE
	FROM ChatMessage
	WHERE MessageID = @MessageID
		AND UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[MessageInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[MessageInsert] @UserID VARCHAR(30)
	,@Message VARCHAR(1000)
AS
BEGIN
	INSERT INTO MessageAdmin (
		UserID
		,Message
		)
	VALUES (
		@UserID
		,@Message
		)
END



GO
/****** Object:  StoredProcedure [dbo].[MessageSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---MessageAdminSelect 157,'',''
CREATE PROCEDURE [dbo].[MessageSelect] --157,'',''
	@UserID INT
AS
BEGIN
	DECLARE @UserTypeID VARCHAR(100);

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	IF (@UserTypeID = '1')
	BEGIN
		SELECT M.Message
			,M.MessageID
		FROM MessageAdmin M
		ORDER BY M.DOC DESC
	END
	ELSE
	BEGIN
		SELECT TOP (10) M.Message
			,M.MessageID
			,DOC
		FROM MessageAdmin M
		WHERE M.UserID = @UserID
			AND M.IsDeleted = 0
		ORDER BY M.DOC DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[MobileValidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:  <Sainath M.Bhujabal>    
-- ALTER  date: <16-jan-2017>    
-- Description: <Description,,>    
-- =============================================    
CREATE PROCEDURE [dbo].[MobileValidation] @MobileNo VARCHAR(10)
	,@UserTypeID INT
AS
DECLARE @Customer INT = 6;

BEGIN
	IF (@UserTypeID = @Customer)
	BEGIN
		SELECT UserID
		FROM [dbo].UserInformation
		WHERE MobileNumber = @MobileNo
			AND IsActive = 1
	END

	IF (@UserTypeID <> @Customer)
	BEGIN
		SELECT UserID
		FROM [dbo].UserInformation
		WHERE MobileNumber = @MobileNo
	END
END



GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferActivationSelectFlag]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[MoneyTransferActivationSelectFlag] @UserID INT
AS
BEGIN
	IF EXISTS (
			SELECT [UserID]
			FROM [MoneyTransferActivation]
			WHERE [UserID] = @UserID
			)
	BEGIN
		SELECT IsActive
		FROM [dbo].[MoneyTransferActivation]
		WHERE [UserID] = @UserID
	END
	ELSE
	BEGIN
		SELECT '0' AS IsActive
		FROM [dbo].[MoneyTransferActivation]
	END
END



GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferActivationUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[MoneyTransferActivationUpdate] @UserID INT
	,@IsActive INT
	,@OUTPUT VARCHAR(500) OUT
AS
BEGIN
	DECLARE @ServiceID INT;

	SET @ServiceID = 21;

	DECLARE @ParentID INT;

	SET @ParentID = (
			SELECT [ParentID]
			FROM [dbo].[UserInformation]
			WHERE [UserID] = @UserID
			);

	DECLARE @RechargeID INT;

	SET @RechargeID = 0;

	DECLARE @TransDesc VARCHAR(50);

	SET @TransDesc = '';

	DECLARE @Balance DECIMAL(18, 5);
	DECLARE @CurrBl DECIMAL(18, 5);
	DECLARE @ServiceCharge DECIMAL(18, 5);
	DECLARE @EndDate VARCHAR(50);

	SET @CurrBl = (
			SELECT [CurrentBalance]
			FROM [dbo].[UserBalance]
			WHERE UserID = @UserID
			);
	SET @ServiceCharge = (
			SELECT [OneTime]
			FROM [dbo].[ServiceMaster]
			WHERE [ServiceID] = @ServiceID
			);

	IF EXISTS (
			SELECT UserID
			FROM [dbo].[MoneyTransferActivation]
			WHERE UserID = @UserID
			)
	BEGIN
		UPDATE [MoneyTransferActivation]
		SET IsActive = @IsActive
		WHERE [UserID] = @UserID

		IF (@IsActive = 0)
		BEGIN
			SET @Output = 'Your service deactivated successfully';

			RETURN (0);
		END
		ELSE
		BEGIN
			SET @Output = 'Your service activated successfully';

			RETURN (0);
		END
	END
	ELSE
	BEGIN
		IF (@IsActive = 1)
		BEGIN
			SET @Balance = @CurrBl - @ServiceCharge;

			DECLARE @sysBalance DECIMAL(18, 5);
			DECLARE @oldBalance DECIMAL(18, 5);

			UPDATE [dbo].[UserBalance]
			SET [CurrentBalance] = @Balance
			WHERE [UserID] = @UserID

			SET @oldBalance = (
					SELECT [CurrentBalance]
					FROM [dbo].[UserBalance]
					WHERE UserID = 60
					)
			SET @sysBalance = @oldBalance + @ServiceCharge;

			UPDATE [dbo].[UserBalance]
			SET [CurrentBalance] = @sysBalance
			WHERE [UserID] = 60

			INSERT INTO [MoneyTransferActivation] (
				[UserID]
				,[IsActive]
				)
			VALUES (
				@UserID
				,@IsActive
				);

			INSERT INTO [dbo].[ServiceChargeHistory] (
				[UserID]
				,[ServiceID]
				,[Amount]
				,[OpeningBalance]
				,[ClosingBalance]
				)
			VALUES (
				@UserID
				,@ServiceID
				,@ServiceCharge
				,@CurrBl
				,@Balance
				);

			SET @RechargeID = @@identity
			SET @TransDesc = 'Service charge of amount ' + CONVERT(VARCHAR, @ServiceCharge) + ' deducted for money transfer';

			DECLARE @SystemTrans VARCHAR(2000);

			SET @SystemTrans = 'Service charge of amount ' + CONVERT(VARCHAR, @ServiceCharge) + 'credited for money transfer by user' + CONVERT(VARCHAR, @UserID);

			EXEC dbo.TransactionDetailsInsertByUser @UserID
				,@RechargeID
				,@ServiceID
				,'0'
				,''
				,'Service Charge'
				,@CurrBl
				,@Balance
				,@ServiceCharge
				,0
				,'DEBIT'
				,''
				,@TransDesc

			EXEC dbo.TransactionDetailsInsertByUser 60
				,@RechargeID
				,@ServiceID
				,@UserID
				,''
				,'Service Charge'
				,@oldBalance
				,@sysBalance
				,@ServiceCharge
				,0
				,'CREDIT'
				,''
				,''

			SET @Output = 'Your service activated successfully';

			RETURN (0);
				--End
		END
	END
END



GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferAmountRefund]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[dbo].[MoneyTransferAmountRefund] 3,null,21,3,'20356390959',44,'FAILED','No Response API',8,'ONLINE',':1',97          
--Modifay:Rahul Hembade          
--Date:21 May 2018          
--Reson:  add Money tranfer DMRBalance          
--===============================          
CREATE PROCEDURE [dbo].[MoneyTransferAmountRefund] @UserID INT  
 ,@TransType VARCHAR(10) = NULL  
 ,@ServiceID INT  
 ,@Amount DECIMAL(18, 5) = 0  
 ,@ConsumerNo VARCHAR(20) = NULL  
 ,@OperatorID INT = NULL  
 ,@Status VARCHAR(50) = NULL  
 ,@MoneyTransferDescription VARCHAR(100) = NULL  
 ,@RechargeProviderID INT  
 ,@Through VARCHAR(100) = NULL  
 ,@IPAddress VARCHAR(50) = NULL  
 ,@MoneyTransferID INT = 0  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 DECLARE @RESULT INT  
 DECLARE @comAmt NUMERIC(18, 5) = 0;  
 DECLARE @success VARCHAR(20) = 'SUCCESS';  
 DECLARE @failure VARCHAR(20) = 'FAILED';  
 DECLARE @refund VARCHAR(20) = 'REFUND';  
 DECLARE @Process VARCHAR(20) = 'PROCESS';  
 DECLARE @Output INT  
  
 SET @Output = 0  
  
 IF (  
   Upper(@Status) = (@failure)  
   )  
 BEGIN  
  SET @TransType = 'REFUNDED'  
  SET @Status = @refund  
  
  --STEP1.1: SELECT USER BALANCE BEFORE TRANSCTION...            
  DECLARE @beforeTransAmt NUMERIC(18, 5);  
  
  SELECT @beforeTransAmt = DMRBalance  
  FROM DBO.UserBalance(NOLOCK)  
  WHERE UserID = @UserID  
  
  UPDATE dbo.MoneyTransfer  
  SET [Description] = @MoneyTransferDescription  
   ,[STATUS] = @Status  
   ,TransactionType = @TransType  
   ,UserOpeningBalance = @beforeTransAmt  
   ,UpdateDOC = getdate()
  --,LastProcessedDateTime = (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))          
  WHERE MoneyTransferID = @MoneyTransferID  
  
  SELECT @RESULT = @@ROWCOUNT  
  
  SET @Output = @RESULT  
  
  DECLARE @TransDesc VARCHAR(100)  
  
  PRINT @UserID  
  PRINT @MoneyTransferID  
  PRINT @ServiceID  
  PRINT @ConsumerNo  
  PRINT @Amount  
  PRINT @OperatorID  
  PRINT @RechargeProviderID  
  
  EXEC dbo.[TransactionDetailsMoneyTranferRefundInsert] @UserID  
   ,@MoneyTransferID  
   ,@ServiceID  
   ,@ConsumerNo  
   ,@Amount  
   ,@OperatorID  
   ,@RechargeProviderID  
 END  
  
 RETURN @Output  
END  
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MoneyTransferInsert] @UserID INT          
 ,@ServiceID INT          
 ,@OperatorID INT          
 ,@RechargeProviderID INT          
 ,@Through VARCHAR(100)          
 ,@IPAddress VARCHAR(50) = NULL          
 ,@SenderID VARCHAR(50) = NULL          
 ,@SenderNumber VARCHAR(15)          
 ,@BeneficiaryID VARCHAR(15) = NULL          
 ,@BeneficiaryUserName VARCHAR(100) = NULL          
 ,@BeneficiaryAccountNo VARCHAR(50) = NULL          
 ,@TransType VARCHAR(50) = NULL          
 ,@Amount NUMERIC(18, 5) = 0          
 ,@Charges VARCHAR(50) = NULL          
 ,@NetAmount NUMERIC(18, 5) = 0          
 ,@ProviderDOCNo VARCHAR(50)          
 ,@ProviderRefNo VARCHAR(50)          
 ,@ProviderOprID VARCHAR(50)          
 ,@ProviderRequestID VARCHAR(50) = '0'          
 ,@APIOpeningBal VARCHAR(50) = '0'          
 ,@APIClosingBal VARCHAR(50) = '0'          
 ,@Status VARCHAR(150) = NULL          
 ,@StatusCode VARCHAR(50) = NULL          
 ,@Bank VARCHAR(100) = NULL          
 ,@UserClosingBal VARCHAR(100) = NULL          
 ,@IFSC VARCHAR(100) = NULL          
 ,@SenderName VARCHAR(100) = NULL          
 ,@Response VARCHAR(1000) = NULL          
 ,@TransactionType VARCHAR(1000) = NULL          
 ,@BillNumber VARCHAR(1000) = NULL    
 ,@APIUserRequestID Varchar(100) = 0  
AS          
BEGIN          
 DECLARE @beforeTransAmt NUMERIC(18, 5);          
 DECLARE @Output INT;   
        
 SELECT @beforeTransAmt = DMRBalance          
 FROM dbo.UserBalance          
 WHERE UserID = @UserID          
 
  DECLARE @providerComm NUMERIC(18, 5) = 0;  

 SELECT @providerComm = isnull(CommissionAdmin, 0)  
 FROM dbo.RechargeProviderOperatorMaster  
 WHERE RechargeProviderID = @RechargeProviderID  
  AND OperatorID = @OperatorId  
   
 INSERT INTO [dbo].[MoneyTransfer] (          
  [UserID]          
  ,[Amount]          
  ,[Status]          
  ,[SenderID]          
  ,[BeneficiaryID]          
  ,[BeneficiaryName]          
  ,[BeneficiaryAccountNo]          
  ,[ProviderDOCNo]          
  ,[ProviderRefNo]          
  ,[ProviderOprID]          
  ,[DMRProviderID]          
  ,[DMRProviderOpeningBalance]          
  ,[DMRProviderClosingBalance]          
  ,[DMROperatorID]          
  ,[Through]          
  ,[IPAddress]          
  ,[DMRServiceID]          
  ,[PaymentType]          
  ,[SenderNumber]          
  ,[ProviderRequestID]          
  ,[StatusCode]          
  ,[NetAmount]          
  ,[Charges]          
  ,Bank          
  ,UserOpeningBalance          
  ,UserClosingBalance          
  ,IFSC          
  ,[SenderName]          
  ,[Description]          
  ,TransactionType          
  ,BillNumber      
  ,APIUserRequestID   
  ,DMRCommissionn   
  )          
 VALUES (          
  @UserID          
  ,@Amount          
  ,@Status          
  ,@SenderID          
  ,@BeneficiaryID          
  ,@BeneficiaryUserName          
  ,@BeneficiaryAccountNo          
  ,@ProviderDOCNo          
  ,@ProviderRefNo          
  ,@ProviderOprID         
  ,@RechargeProviderID          
  ,@APIOpeningBal          
  ,@APIClosingBal          
  ,@OperatorID          
  ,@Through          
  ,@IPAddress          
  ,@ServiceID          
  ,@TransType          
  ,@SenderNumber          
  ,@ProviderRequestID          
  ,@StatusCode          
  ,@NetAmount          
  ,@Charges          
  ,@Bank          
  ,@beforeTransAmt          
  ,@UserClosingBal          
  ,@IFSC          
  ,@SenderName          
  ,@Response          
  ,@TransactionType          
  ,@BillNumber    
  ,@APIUserRequestID 
  ,@providerComm      
  )          
 SET @Output = @@IDENTITY          
 PRINT '@Output:' + convert(VARCHAR(30), @UserID) + ':' + convert(VARCHAR(30), @Output) + ':' 
 + convert(VARCHAR(30), @ServiceID) + ':' + convert(VARCHAR(30), @SenderNumber) + ':' 
 + convert(VARCHAR(30), @Amount) + ':' + convert(VARCHAR(30), @OperatorID) 
 + ':' + convert(VARCHAR(30), @RechargeProviderID)     
      
 DECLARE @TransactionAmount NUMERIC(18, 5);          
 SET @TransactionAmount = @Amount          
 DECLARE @ConsumerNo VARCHAR(15);          
 SET @ConsumerNo = @SenderNumber          
 DECLARE @RechargeID INT;          
 SET @RechargeID = @Output;          
 IF (@Output >= 1)          
 BEGIN          
  EXEC dbo.[TransactionDetailsInsertMoneyTransfer] @UserID          
   ,@RechargeID ----@RechargeID INT                               
   ,@ServiceID          
   ,@BeneficiaryAccountNo          
   ,@TransactionAmount ----@TransactionAmount                                         
   ,@OperatorID          
   ,@RechargeProviderID          
 END          
 RETURN @Output          
END 
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferInsertIDMR]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MoneyTransferInsertIDMR] @UserID INT
	,@ServiceID INT
	,@OperatorID INT
	,@RechargeProviderID INT
	,@Through VARCHAR(100)
	,@IPAddress VARCHAR(50) = NULL
	,@SenderID VARCHAR(50) = NULL
	,@SenderNumber VARCHAR(15)
	,@BeneficiaryID VARCHAR(15)
	,@BeneficiaryUserName VARCHAR(100) = NULL
	,@BeneficiaryAccountNo VARCHAR(50) = NULL
	,@TransType VARCHAR(50) = NULL
	,@Amount NUMERIC(18, 5) = 0
	,@Charges VARCHAR(50) = NULL
	,@NetAmount NUMERIC(18, 5) = 0
	,@ProviderDOCNo VARCHAR(50)
	,@ProviderRefNo VARCHAR(50)
	,@ProviderOprID VARCHAR(50)
	,@ProviderRequestID VARCHAR(50)
	,@InstapayOpeningBal VARCHAR(50)
	,@InstapayClosingBal VARCHAR(50)
	,@Status VARCHAR(150) = NULL
	,@StatusCode VARCHAR(50) = NULL
	,@Bank VARCHAR(100) = NULL
	,@UserClosingBal VARCHAR(100) = NULL
	,@IFSC VARCHAR(100) = NULL
	,@SenderName VARCHAR(100) = NULL
	,@BillNumber VARCHAR(1000) = NULL
AS
BEGIN
	DECLARE @beforeTransAmt NUMERIC(18, 5);
	DECLARE @Output INT;
	SELECT @beforeTransAmt = CurrentBalance
	FROM dbo.UserBalance
	WHERE UserID = @UserID
	INSERT INTO [dbo].[MoneyTransfer] (
		[UserID]
		,[Amount]
		,[Status]
		,[SenderID]
		,[BeneficiaryID]
		,[BeneficiaryName]
		,[BeneficiaryAccountNo]
		,[ProviderDOCNo]
		,[ProviderRefNo]
		,[ProviderOprID]
		,[DMRProviderID]
		,[DMRProviderOpeningBalance]
		,[DMRProviderClosingBalance]
		,[DMROperatorID]
		,[Through]
		,[IPAddress]
		,[DMRServiceID]
		,[PaymentType]
		,[SenderNumber]
		,[ProviderRequestID]
		,[StatusCode]
		,[NetAmount]
		,[Charges]
		,Bank
		,UserOpeningBalance
		,UserClosingBalance
		,IFSC
		,[SenderName]
		,BillNumber
		)
	VALUES (
		@UserID
		,@Amount
		,@Status
		,@SenderID
		,@BeneficiaryID
		,@BeneficiaryUserName
		,@BeneficiaryAccountNo
		,@ProviderDOCNo
		,@ProviderRefNo
		,@ProviderOprID
		,@RechargeProviderID
		,@InstapayOpeningBal
		,@InstapayClosingBal
		,@OperatorID
		,@Through
		,@IPAddress
		,@ServiceID
		,@TransType
		,@SenderNumber
		,@ProviderRequestID
		,@StatusCode
		,@NetAmount
		,@Charges
		,@Bank
		,@beforeTransAmt
		,@UserClosingBal
		,@IFSC
		,@SenderName
		,@BillNumber
		)
	SET @Output = @@IDENTITY
	PRINT '@Output:' + convert(VARCHAR(30), @UserID) + ':' + convert(VARCHAR(30), @Output) + ':' + convert(VARCHAR(30), @ServiceID) + ':' + convert(VARCHAR(30), @SenderNumber) + ':' + convert(VARCHAR(30), @Amount) + ':' + convert(VARCHAR(30), @OperatorID) + ':' + convert(VARCHAR(30), @RechargeProviderID)
	DECLARE @TransactionAmount NUMERIC(18, 5);
	SET @TransactionAmount = @Amount
	DECLARE @ConsumerNo VARCHAR(15);
	SET @ConsumerNo = @SenderNumber
	EXEC dbo.[TransactionDetailsInsertMoneyTransfer] @UserID
		,@Output ----@RechargeID INT                                  
		,@ServiceID
		,@ConsumerNo
		,@Amount ----@TransactionAmount                                   
		,@OperatorID
		,@RechargeProviderID
	RETURN @Output
END

GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferSelectAPIRequestIDByMID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE[dbo].[MoneyTransferSelectAPIRequestIDByMID] @MoneyTransferID INT
AS
BEGIN
	SELECT isnull([ProviderRequestID], '') AS ApiRequestID
	FROM MoneyTransfer
	WHERE  [MoneyTransferID]= @MoneyTransferID
END


GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferSelectByDatewise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[MoneyTransferSelectByDatewise] @UserID INT
	,@FromDate VARCHAR(250)
	,@ToDate VARCHAR(250)
	,@SenderNumber VARCHAR(100) = NULL
AS
BEGIN
	DECLARE @retailer VARCHAR(20) = 'RETAILER';
	DECLARE @Customer VARCHAR(20) = 'Customer';
	DECLARE @distributor VARCHAR(20) = 'DISTRIBUTOR';
	DECLARE @admin VARCHAR(20) = 'ADMIN';
	DECLARE @apiUser VARCHAR(20) = 'APIUSER';
	DECLARE @userType VARCHAR(20);

	SELECT @userType = UTM.UserType
	FROM [dbo].UserInformation(NOLOCK) UI
	INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @UserID

	IF (Upper(@userType) = Upper(@admin))
	BEGIN
		IF (
				@SenderNumber = ''
				OR @SenderNumber = NULL
				)
		BEGIN
			SELECT MT.ID AS MoneyTransferID
				,convert(VARCHAR(12), MT.DOC, 113) + right(convert(VARCHAR(39), MT.DOC, 22), 11) AS DOC
				,MT.SenderNumber
				,B.[BeneficiaryID]
				,B.[Name] AS BeneficiaryName
				,UI.UserID
				,B.BankName
				,UI.UserName AS UserName
				,MT.[BeneficiaryID]
				,UI.UserTypeID AS RUserTypeID
				,MT.BeneficiaryAccountNo
				,MT.BeneficiaryIFSCCode
				,MT.Amount
				,MT.TransferType
				,MT.TransactionID
				,MT.[Status]
				,TD.DiscountAmount
				,TD.ClosingBalance
				,TD.[OpeningBalance]
				,UIS.UserID AS ParentID
				,UIS.UserName AS ParentName
				,UIS.UserTypeID AS DUserTypeID
				,MT.[TransactionType]
				,MT.[OperatorID]
			FROM dbo.MoneyTransfer MT
			INNER JOIN [dbo].[TransactionDetails] TD ON TD.RechargeID = MT.[MoneyTransferID]
			INNER JOIN [dbo].[UserInformation] UI ON UI.UserID = MT.UserID
			INNER JOIN [dbo].[Beneficiary] B ON B.[BeneficiaryID] = MT.[BeneficiaryID]
			INNER JOIN UserInformation UIS ON UIS.UserID = UI.ParentID
			WHERE OperatorID != 45
				AND UI.[UserTypeID] IN (
					3
					,7
					)
				AND TD.DOCTYPE = 'MoneyTransfer'
				--AND AmountCreditDebit = 'DEBIT'  
				AND (
					convert(DATE, MT.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, MT.DOC, 103) <= convert(DATE, @ToDate, 103)
					)
			--and @UserID = TD.UserID  
			ORDER BY MT.doc DESC
		END
		ELSE
		BEGIN
			SELECT MT.ID AS MoneyTransferID
				,convert(VARCHAR(12), MT.DOC, 113) + right(convert(VARCHAR(39), MT.DOC, 22), 11) AS DOC
				,MT.SenderNumber
				,B.[BeneficiaryID]
				,B.[Name] AS BeneficiaryName
				,UI.UserID
				,B.BankName
				,UI.UserName AS UserName
				,MT.[BeneficiaryID]
				,UI.UserTypeID AS RUserTypeID
				,MT.BeneficiaryAccountNo
				,MT.BeneficiaryIFSCCode
				,MT.Amount
				,MT.TransferType
				,MT.TransactionID
				,MT.[Status]
				,TD.DiscountAmount
				,TD.ClosingBalance
				,TD.[OpeningBalance]
				,UIS.UserID AS ParentID
				,UIS.UserName AS ParentName
				,UIS.UserTypeID AS DUserTypeID
				,MT.[TransactionType]
				,MT.[OperatorID]
			FROM dbo.MoneyTransfer MT
			INNER JOIN [dbo].[TransactionDetails] TD ON TD.RechargeID = MT.[MoneyTransferID]
			INNER JOIN [dbo].[UserInformation] UI ON UI.UserID = MT.UserID
			INNER JOIN [dbo].[Beneficiary] B ON B.[BeneficiaryID] = MT.[BeneficiaryID]
			INNER JOIN UserInformation UIS ON UIS.UserID = UI.ParentID
			WHERE OperatorID != 45
				AND UI.[UserTypeID] IN (
					3
					,7
					)
				AND TD.DOCTYPE = 'MoneyTransfer'
				AND MT.SenderNumber = @SenderNumber
				AND (
					convert(DATE, MT.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, MT.DOC, 103) <= convert(DATE, @ToDate, 103)
					)
			--and @UserID = TD.UserID  
			ORDER BY MT.doc DESC
		END
	END

	IF (Upper(@userType) = Upper(@distributor))
	BEGIN
		IF (
				@SenderNumber = ''
				OR @SenderNumber = NULL
				)
		BEGIN
			SELECT MT.ID AS MoneyTransferID
				,convert(VARCHAR(12), MT.DOC, 113) + right(convert(VARCHAR(39), MT.DOC, 22), 11) AS DOC
				,MT.SenderNumber
				,B.[Name] AS BeneficiaryName
				,UI.UserID
				,B.BankName
				,UI.UserTypeID AS RUserTypeID
				,UI.UserName AS UserName
				,MT.[BeneficiaryID]
				,MT.BeneficiaryAccountNo
				,MT.BeneficiaryIFSCCode
				,MT.Amount
				,MT.TransferType
				,MT.TransactionID
				,MT.[Status]
				,TD.DiscountAmount
				,TD.ClosingBalance
				,TD.[OpeningBalance]
				,UIS.UserID AS ParentID
				,UIS.UserName AS ParentName
				,UIS.UserTypeID AS DUserTypeID
				,MT.[TransactionType]
				,MT.[OperatorID]
			FROM dbo.MoneyTransfer MT
			INNER JOIN [dbo].[TransactionDetails] TD ON TD.RechargeID = MT.[MoneyTransferID]
			INNER JOIN [dbo].[UserInformation] UI ON UI.UserID = MT.UserID
			INNER JOIN [dbo].[Beneficiary] B ON B.[BeneficiaryID] = MT.[BeneficiaryID]
			INNER JOIN UserInformation UIS ON UIS.UserID = UI.ParentID
			WHERE OperatorID != 45
				AND UI.[UserTypeID] IN (
					3
					,7
					)
				AND TD.DOCTYPE = 'MoneyTransfer'
				AND AmountCreditDebit = ''
				AND CommissionCreditDebit = 'CREDIT'
				AND (
					convert(DATE, MT.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, MT.DOC, 103) <= convert(DATE, @ToDate, 103)
					)
				AND @UserID IN (UI.ParentID)
			ORDER BY MT.doc DESC
		END
		ELSE
		BEGIN
			SELECT MT.ID AS MoneyTransferID
				,convert(VARCHAR(12), MT.DOC, 113) + right(convert(VARCHAR(39), MT.DOC, 22), 11) AS DOC
				,MT.SenderNumber
				,B.[Name] AS BeneficiaryName
				,UI.UserID
				,B.BankName
				,UI.UserTypeID AS RUserTypeID
				,UI.UserName AS UserName
				,MT.[BeneficiaryID]
				,MT.BeneficiaryAccountNo
				,MT.BeneficiaryIFSCCode
				,MT.Amount
				,MT.TransferType
				,MT.TransactionID
				,MT.[Status]
				,TD.DiscountAmount
				,TD.ClosingBalance
				,TD.[OpeningBalance]
				,UIS.UserID AS ParentID
				,UIS.UserName AS ParentName
				,UIS.UserTypeID AS DUserTypeID
				,MT.[TransactionType]
				,MT.[OperatorID]
			FROM dbo.MoneyTransfer MT
			INNER JOIN [dbo].[TransactionDetails] TD ON TD.RechargeID = MT.[MoneyTransferID]
			INNER JOIN [dbo].[UserInformation] UI ON UI.UserID = MT.UserID
			INNER JOIN [dbo].[Beneficiary] B ON B.[BeneficiaryID] = MT.[BeneficiaryID]
			INNER JOIN UserInformation UIS ON UIS.UserID = UI.ParentID
			WHERE OperatorID != 45
				AND UI.[UserTypeID] IN (
					3
					,7
					)
				AND TD.DOCTYPE = 'MoneyTransfer'
				AND AmountCreditDebit = ''
				AND CommissionCreditDebit = 'CREDIT'
				AND MT.SenderNumber = @SenderNumber
				AND (
					convert(DATE, MT.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, MT.DOC, 103) <= convert(DATE, @ToDate, 103)
					)
				AND @UserID IN (UI.ParentID)
		END
	END

	IF (Upper(@userType) = Upper(@retailer))
	BEGIN
		IF (
				@SenderNumber = ''
				OR @SenderNumber = NULL
				)
		BEGIN
			SELECT MT.ID AS MoneyTransferID
				,convert(VARCHAR(12), MT.DOC, 113) + right(convert(VARCHAR(39), MT.DOC, 22), 11) AS DOC
				,MT.SenderNumber
				,B.[Name] AS BeneficiaryName
				,UI.UserID
				,B.BankName
				,UI.UserTypeID AS RUserTypeID
				,UI.UserName AS UserName
				,MT.[BeneficiaryID]
				,MT.BeneficiaryAccountNo
				,MT.BeneficiaryIFSCCode
				,MT.Amount
				,MT.TransferType
				,MT.TransactionID
				,MT.[Status]
				,TD.DiscountAmount
				,TD.ClosingBalance
				,TD.[OpeningBalance]
				,UIS.UserID AS ParentID
				,UIS.UserName AS ParentName
				,UIS.UserTypeID AS DUserTypeID
				,MT.[TransactionType]
				,MT.[OperatorID]
			FROM dbo.MoneyTransfer MT
			INNER JOIN [dbo].[TransactionDetails] TD ON TD.RechargeID = MT.[MoneyTransferID]
			INNER JOIN [dbo].[UserInformation] UI ON UI.UserID = MT.UserID
			INNER JOIN [dbo].[Beneficiary] B ON B.[BeneficiaryID] = MT.[BeneficiaryID]
			INNER JOIN UserInformation UIS ON UIS.UserID = UI.ParentID
			WHERE OperatorID != 45
				AND UI.[UserTypeID] IN (
					3
					,7
					)
				AND TD.DOCTYPE = 'MoneyTransfer'
				AND AmountCreditDebit = 'DEBIT'
				AND (
					convert(DATE, MT.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, MT.DOC, 103) <= convert(DATE, @ToDate, 103)
					)
				AND @UserID IN (TD.UserID)
			ORDER BY MT.doc DESC
		END
		ELSE
		BEGIN
			SELECT MT.ID AS MoneyTransferID
				,convert(VARCHAR(12), MT.DOC, 113) + right(convert(VARCHAR(39), MT.DOC, 22), 11) AS DOC
				,MT.SenderNumber
				,B.[Name] AS BeneficiaryName
				,UI.UserID
				,B.BankName
				,UI.UserTypeID AS RUserTypeID
				,UI.UserName AS UserName
				,MT.[BeneficiaryID]
				,MT.BeneficiaryAccountNo
				,MT.BeneficiaryIFSCCode
				,MT.Amount
				,MT.TransferType
				,MT.TransactionID
				,MT.[Status]
				,TD.DiscountAmount
				,TD.ClosingBalance
				,TD.[OpeningBalance]
				,UIS.UserID AS ParentID
				,UIS.UserName AS ParentName
				,UIS.UserTypeID AS DUserTypeID
				,MT.[TransactionType]
				,MT.[OperatorID]
			FROM dbo.MoneyTransfer MT
			INNER JOIN [dbo].[TransactionDetails] TD ON TD.RechargeID = MT.[MoneyTransferID]
			INNER JOIN [dbo].[UserInformation] UI ON UI.UserID = MT.UserID
			INNER JOIN [dbo].[Beneficiary] B ON B.[BeneficiaryID] = MT.[BeneficiaryID]
			INNER JOIN UserInformation UIS ON UIS.UserID = UI.ParentID
			WHERE OperatorID != 45
				AND UI.[UserTypeID] IN (
					3
					,7
					)
				AND TD.DOCTYPE = 'MoneyTransfer'
				AND AmountCreditDebit = 'DEBIT'
				AND (
					convert(DATE, MT.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, MT.DOC, 103) <= convert(DATE, @ToDate, 103)
					)
				AND @UserID IN (TD.UserID)
				AND MT.SenderNumber = @SenderNumber
			ORDER BY MT.doc DESC
		END
	END
END


GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferSelectByID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [MoneyTransferSelectByID] 144          
CREATE proc [dbo].[MoneyTransferSelectByID] @MoneyTransferID int          
as          
begin          
 select MT.UserID             
  ,[DMRServiceID] as ServiceID        
  ,[UserOpeningBalance]          
  ,Amount          
  ,BeneficiaryAccountNo          
  ,[DMROperatorID] as OperatorID        
  ,[Status]          
  ,[Description]     
  ,UB.DMRBalance       
  ,MT.PaymentType  
  ,MT.Bank  
  ,MT.MoneyTransferID  
  ,MT.APIUserRequestID  
  ,MT.ProviderRequestID
  ,MT.ProviderRefNo as TransactionID
 from MoneyTransfer MT     
 inner join UserBalance UB on UB.UserID =  MT.UserID       
 where [MoneyTransferID] = @MoneyTransferID          
end 
 
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferSelectByMoneyTransferID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[MoneyTransferSelectByMoneyTransferID]                      
@MoneyTransferID int                      
as                      
begin                      
 select DMRRequest,           
 DMRResponse,          
 [BeneficiaryAccountNo],           
 ProviderRequestID As [ProviderRequestID],           
 DMRProviderOpeningBalance,          
 [BeneficiaryName], 
 UserID                    
 from MoneyTransfer                       
 where [MoneyTransferID] = @MoneyTransferID                      
end   
  
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferSelectByUserID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [MoneyTransferSelectByUserID] 3
CREATE PROC [dbo].[MoneyTransferSelectByUserID] @UserID INT              
AS              
BEGIN              
  SELECT              
   MT.MoneyTransferID              
   ,convert(VARCHAR(12), MT.DOC, 113) + right(convert(VARCHAR(39), MT.DOC, 22), 11) AS DOC              
   ,MT.SenderNumber              
   ,MT.BeneficiaryAccountNo              
   ,MT.IFSC AS BeneficiaryIFSCCode              
   ,MT.PaymentType AS TransferType              
   ,MT.[ProviderOprID] AS TransactionID              
   ,MT.[Status]              
   ,TD.DiscountAmount              
   ,MT.Amount              
   ,MT.BeneficiaryName AS BeneficiaryName              
   ,UI.ShopeName AS ShopeName              
   ,MT.[Bank] AS BankName              
   ,UI.MobileNumber AS MobileNumber              
   ,MT.BillNumber              
   ,MT.SessionID          
   ,TD.OpeningBalance      
   ,TD.ClosingBalance        
   ,MT.[DMRProviderID]  
   ,Convert(Decimal(18,2),TD.GST) AS GST
   ,Convert(Decimal(18,2),TD.TDS) AS TDS
  FROM dbo.MoneyTransfer MT              
  INNER JOIN [dbo].[TransactionDetails] TD ON TD.RechargeID = MT.MoneyTransferID              
   AND TD.UserID = MT.UserID              
  INNER JOIN [dbo].[UserInformation] UI ON UI.UserID = MT.UserID              
                
  WHERE MT.UserID = @UserID              
   AND Upper(TD.DOCTYPE) = Upper('MoneyTransfer')              
   AND TD.AmountCreditDebit = 'DEBIT'              
  order By MT.MoneyTransferID Desc              
END 
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferSelectDatewise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [MoneyTransferSelectDatewise] 1, '01/12/2018', '01/01/2019', 1000
CREATE PROCEDURE [dbo].[MoneyTransferSelectDatewise] @UserID INT          
 ,@FromDate VARCHAR(100)          
 ,@ToDate VARCHAR(100)          
 ,@Records INT          
AS          
BEGIN         
      
if @Records is null       
begin       
set @Records=1000      
end      
      
if @Records =0      
begin       
set @Records=1000      
end      
       
 DECLARE @UserTypeID INT;          
 DECLARE @retailer VARCHAR(20) = 'RETAILER';          
 DECLARE @admin VARCHAR(20) = 'ADMIN';          
 DECLARE @SUPPORT VARCHAR(20) = 'SUPPORT';          
 DECLARE @apiUser VARCHAR(20) = 'APIUSER';        
 DECLARE @userType VARCHAR(20);          
 DECLARE @distributor VARCHAR(20) = 'DISTRIBUTOR';          
 SELECT @userType = UTM.UserType          
 FROM [dbo].UserInformation(NOLOCK) UI          
 INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID          
 WHERE UI.UserID = @UserID          
 SET @UserTypeID = (          
   SELECT UserTypeID          
   FROM UserInformation          
   WHERE UserID = @UserID          
   )          
 --Admin Recharge Report                                                            
 IF (          
   Upper(@userType) = Upper(@admin)          
   OR Upper(@userType) = Upper(@SUPPORT)          
   )          
 BEGIN          
  SELECT *          
  FROM (          
   SELECT TOP (@Records) R.UserID          
    ,R.[ProviderOprID] AS TransactionID          
    ,TD.RechargeID          
    ,TD.serviceID          
    ,TD.ConsumerNumber AS SenderNumber          
    ,TD.DOCType          
    ,TD.OpeningBalance          
    ,TD.ClosingBalance          
    ,TD.TransactionAmount AS Amount          
    ,TD.DiscountAmount AS Charges          
    ,TD.AmountCreditDebit          
    ,TD.CommissionCreditDebit          
    ,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC          
    ,'0' AS IsDispute          
    ,R.DMROperatorID AS OperatorID          
    ,SM.ServiceName          
    ,OM.OperatorName          
    ,UI.UserName          
    ,UI.UserTypeID          
    ,R.STATUS          
    ,UIS.UserID AS ParentID          
    ,UIS.UserName AS ParentName          
    ,UIS.UserTypeID AS DUserTypeID          
    ,R.Bank          
    ,R.BeneficiaryAccountNo          
    ,R.SenderName          
    ,R.BeneficiaryName          
    ,R.MoneyTransferID          
    ,R.NetAmount          
    ,R.BillNumber          
	 ,R.SessionID        
	 ,R.PaymentType 
	 ,CONVERT(Decimal(18,2),TD.GST) AS GST
	 ,CONVERT(Decimal(18,2),TD.TDS) AS TDS     
    ,ROW_NUMBER() OVER (          
     PARTITION BY TD.RechargeID ORDER BY R.DOC DESC          
     ) [SR]          
   FROM dbo.TransactionDetails(NOLOCK) TD          
   INNER JOIN MoneyTransfer(NOLOCK) R ON R.MoneyTransferID = TD.RechargeID          
    AND R.UserID = TD.UserID          
    AND TD.DOCTYPE = 'MoneyTransfer'          
   INNER JOIN UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID          
   INNER JOIN UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID          
   INNER JOIN ServiceMaster(NOLOCK) SM ON R.DMRServiceID = SM.ServiceID          
   INNER JOIN OperatorMaster(NOLOCK) OM ON R.DMROperatorID = OM.OperatorID          
   WHERE (          
     convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)          
     AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)          
     )          
   ORDER BY R.DOC DESC          
   ) AS maint          
  WHERE [SR] = 1          
 END          
 IF (Upper(@userType) = Upper(@retailer) or Upper(@userType) = Upper(@apiUser))          
 BEGIN          
  SELECT *          
  FROM (          
   SELECT TOP (@Records) R.UserID          
    ,R.[ProviderOprID] AS TransactionID          
    ,TD.RechargeID          
    ,TD.serviceID          
    ,TD.ConsumerNumber AS SenderNumber          
    ,TD.DOCType          
    ,TD.OpeningBalance          
    ,TD.ClosingBalance          
    ,TD.TransactionAmount AS Amount          
    ,TD.DiscountAmount AS Charges          
    ,TD.AmountCreditDebit          
    ,TD.CommissionCreditDebit          
    ,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC          
    ,'0' AS IsDispute          
    ,R.DMROperatorID AS OperatorID          
    ,SM.ServiceName          
    ,OM.OperatorName          
    ,UI.UserName          
    ,UI.UserTypeID          
    ,R.STATUS          
    ,UIS.UserID AS ParentID          
    ,UIS.UserName AS ParentName        
    ,UIS.UserTypeID AS DUserTypeID          
    ,R.Bank          
    ,R.BeneficiaryAccountNo          
    ,R.SenderName          
    ,R.BeneficiaryName          ,R.MoneyTransferID          
    ,R.NetAmount          
    ,R.BillNumber         
    ,R.SessionID         
    ,R.PaymentType  
	,CONVERT(Decimal(18,2),TD.GST) AS GST
	 ,CONVERT(Decimal(18,2),TD.TDS) AS TDS    
    ,ROW_NUMBER() OVER (          
     PARTITION BY TD.RechargeID ORDER BY R.DOC DESC          
     ) [SR]          
   FROM dbo.TransactionDetails(NOLOCK) TD          
   INNER JOIN MoneyTransfer(NOLOCK) R ON R.MoneyTransferID = TD.RechargeID          
    AND R.UserID = TD.UserID          
    AND TD.DOCTYPE = 'MoneyTransfer'          
   INNER JOIN UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID          
   INNER JOIN UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID          
   INNER JOIN ServiceMaster(NOLOCK) SM ON R.DMRServiceID = SM.ServiceID          
   INNER JOIN OperatorMaster(NOLOCK) OM ON R.DMROperatorID = OM.OperatorID          
   WHERE (          
     convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)          
     AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)          
     )          
    AND R.UserID = @UserID          
   ORDER BY R.DOC DESC          
   ) AS maint          
  WHERE [SR] = 1          
 END          
 IF (Upper(@userType) = Upper(@distributor))          
 BEGIN          
  SELECT *          
  FROM (          
   SELECT TOP (@Records) R.UserID          
    ,R.[ProviderOprID] AS TransactionID          
    ,TD.RechargeID          
    ,TD.serviceID          
    ,TD.ConsumerNumber AS SenderNumber          
    ,TD.DOCType          
    ,TD.OpeningBalance          
    ,TD.ClosingBalance          
    ,TD.TransactionAmount AS Amount          
    ,TD.DiscountAmount AS Charges          
    ,TD.AmountCreditDebit          
    ,TD.CommissionCreditDebit          
    ,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC          
    ,'0' AS IsDispute          
    ,R.DMROperatorID AS OperatorID          
    ,SM.ServiceName          
    ,OM.OperatorName AS OperatorName          
    ,UI.UserName          
    ,UI.UserTypeID          
    ,R.STATUS          
    ,UIS.UserID AS ParentID          
    ,UIS.UserName AS ParentName          
    ,UIS.UserTypeID AS DUserTypeID          
    ,R.Bank          
    ,R.BeneficiaryAccountNo          
    ,R.SenderName          
    ,R.BeneficiaryName          
    ,R.MoneyTransferID          
    ,R.NetAmount          
    ,R.BillNumber          
 ,R.SessionID        
 ,R.PaymentType   
 		 ,CONVERT(Decimal(18,2),TD.GST) AS GST
	 ,CONVERT(Decimal(18,2),TD.TDS) AS TDS  
    ,ROW_NUMBER() OVER (          
     PARTITION BY TD.RechargeID ORDER BY R.DOC DESC          
     ) [SR]          
   FROM dbo.TransactionDetails(NOLOCK) TD          
   INNER JOIN dbo.MoneyTransfer(NOLOCK) R ON R.MoneyTransferID = TD.RechargeID          
    AND R.UserID = TD.UserID          
    AND TD.DOCTYPE = 'MoneyTransfer'          
   INNER JOIN UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID          
   INNER JOIN UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID          
   INNER JOIN ServiceMaster(NOLOCK) SM ON R.DMRServiceID = SM.ServiceID          
   INNER JOIN OperatorMaster(NOLOCK) OM ON R.DMROperatorID = OM.OperatorID          
   WHERE (          
     convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)          
     AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)          
     )          
    --and R.UserID = @UserID                         
    AND @UserID IN (          
     TD.UserID          
     ,UI.ParentID          
     )          
   ORDER BY R.DOC DESC          
   ) AS maint          
  WHERE [SR] = 1          
 END          
END     
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferSelectForReceipt]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MoneyTransferSelectForReceipt] @BillNumber VARCHAR(50)  
AS  
BEGIN  
 SELECT BillNumber  
  ,DOC  
  ,[Status]  
  ,[Amount]  
  ,[APIOprID]  
  ,[BeneficiaryName]  
  ,[SenderNumber]  
  ,[IFSC]  
  ,[BeneficiaryAccountNo]  
  ,[NetAmount]  
  ,BankName  
  ,SenderName  
  ,TransferType  
  ,ShopeName  
  ,MobileNumber  
  ,RowNumber  
 FROM (  
  SELECT DISTINCT Isnull(BillNumber, 0) BillNumber  
   ,convert(VARCHAR(12), B.DOC, 113) + right(convert(VARCHAR(39), B.DOC, 22), 11) AS DOC  
   ,B.[BeneficiaryName]  
   ,B.[SenderNumber]  
   ,B.[IFSC]  
   ,B.[BeneficiaryAccountNo]  
   ,B.[NetAmount]  
   ,B.Bank AS BankName  
   ,B.SenderName  
   ,B.PaymentType AS TransferType  
   ,UI.ShopeName  
   ,UI.MobileNumber  
   ,ISNULL(STUFF((  
      SELECT ', ' + A.[Status]  
      FROM MoneyTransfer A  
      WHERE A.BillNumber = B.BillNumber  
      FOR XML PATH('')  
      ), 1, 1, ''), B.[Status]) AS [Status]  
   ,ISNULL(STUFF((  
      SELECT ', ' + CAST(CONVERT(DECIMAL(18, 2), MT.[Amount]) AS VARCHAR)  
      FROM MoneyTransfer MT  
      WHERE MT.BillNumber = B.BillNumber  
      FOR XML PATH('')  
      ), 1, 1, ''), CONVERT(DECIMAL(18, 2), B.[Amount])) AS [Amount]  
   ,ISNULL(STUFF((  
      SELECT ', ' + M.[ProviderOprID]  
      FROM MoneyTransfer M  
      WHERE M.BillNumber = B.BillNumber  
      FOR XML PATH('')  
      ), 1, 1, ''), B.[ProviderOprID]) AS [APIOprID]  
   ,ROW_NUMBER() OVER (  
    PARTITION BY BillNumber ORDER BY BillNumber DESC  
     --,UserOpeningBalance DESC      
    ) AS RowNumber  
  FROM MoneyTransfer B  
  INNER JOIN UserInformation UI ON UI.UserID = B.UserID  
  WHERE BillNumber = @BillNumber  
  GROUP BY B.DOC  
   ,B.BillNumber  
   ,B.[Amount]  
   ,B.[ProviderOprID]  
   ,B.[Status]  
   ,B.[BeneficiaryName]  
   ,B.[SenderNumber]  
   ,B.[IFSC]  
   ,B.[BeneficiaryAccountNo]  
   ,B.[NetAmount]  
   ,B.Bank  
   ,B.SenderName  
   ,UI.ShopeName  
   ,UI.MobileNumber  
   ,B.PaymentType  
  ) AS sds  
 WHERE RowNumber = 1  
END  
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferSelectInstapayID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[MoneyTransferSelectInstapayID]                    
@MoneyTransferID int                    
as                    
begin                    
 select DMRRequest,         
 DMRResponse,        
 [BeneficiaryAccountNo],         
 ProviderRequestID As [InstapayRequestID],         
 DMRProviderOpeningBalance,        
 [BeneficiaryName], UserID                  
 from MoneyTransfer                     
 where [MoneyTransferID] = @MoneyTransferID                    
end 


GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferSelectPending]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                
-- Author:  <Author,,Name>                
-- Create date: <Create Date,,>                
-- Description: <Description,,>                
-- MoneyTransferSelect 342, '7507257267'          
-- =============================================            
-- MoneyTransferSelectPending 1              
CREATE PROCEDURE [dbo].[MoneyTransferSelectPending] @UserID INT    
AS    
BEGIN    
 DECLARE @USERTYPE INT    
    
 SET @USERTYPE = ''    
    
 SELECT @USERTYPE = UserTypeID    
 FROM [dbo].[UserInformation](NOLOCK)    
 WHERE [UserID] = @UserID    
    
 BEGIN    
  IF @USERTYPE = 1    
  BEGIN    
   SELECT DISTINCT R.[MoneyTransferID]    
    ,R.[DMRServiceID]    
    ,R.[ProviderOprID]  as TransactionID 
    ,R.[DMROperatorID]    
    ,R.[BeneficiaryAccountNo] AS AccountNumber    
    ,R.Amount    
    ,R.STATUS    
    ,R.[BeneficiaryName] AS BeneficiaryName    
    ,RPM.RechargeProviderName    
    ,RPM.RechargeProviderUrl    
    ,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC    
    ,SM.ServiceName    
    ,OM.OperatorName    
    ,TD.DiscountAmount    
    ,R.UserID    
    ,UI.UserName AS Name    
    ,UI.UserTypeID    
    ,R.[SenderName]    
    ,R.[BeneficiaryID]    
    ,R.IFSC    
    ,R.PaymentType    
    ,R.SessionID    
 ,R.[SenderNumber]  
 ,R.Bank  
   FROM [dbo].[MoneyTransfer] R    
   INNER JOIN UserInformation UI ON R.UserID = UI.UserID    
   INNER JOIN TransactionDetails TD ON R.[MoneyTransferID] = TD.RechargeID    
    AND R.UserID = TD.UserID    
    AND TD.[DOCType] IN ('MoneyTransfer')    
   INNER JOIN ServiceMaster SM ON R.[DMRServiceID] = SM.ServiceID    
   INNER JOIN OperatorMaster OM ON R.[DMROperatorID] = OM.OperatorID    
   LEFT JOIN RechargeProviderMaster RPM ON R.[DMRProviderID] = RPM.RechargeProviderID    
   WHERE UPPER(R.STATUS) = UPPER('PROCESS')    
   ORDER BY doc DESC    
  END    
 END    
END 
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferSelectProviderTransactionIDByMID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  --[MoneyTransferSelectProviderTransactionIDByMID] 2    
CREATE proc [dbo].[MoneyTransferSelectProviderTransactionIDByMID] @MoneyTransferID int
as      
begin      
 select isnull([ProviderRefNo], 0) as ProviderTransactionID      
 from MoneyTransfer      
 where MoneyTransferID = @MoneyTransferID      

end      
      
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferSelectSenderwise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [MoneyTransferSelectSenderwise] '9975688036',3              
            
CREATE PROCEDURE [dbo].[MoneyTransferSelectSenderwise] @SenderNumber VARCHAR(50) = null                  
 ,@UserID VARCHAR(10)                        
AS                        
BEGIN              
  Declare @DMRServiceID int = 21;            
                      
  SELECT                     
convert(VARCHAR(12), MT.DOC, 113) + right(convert(VARCHAR(39), MT.DOC, 22), 11) AS DOC                        
  ,(MT.[SenderName]) AS Name                        
  ,MT.SenderNumber AS SenderNumber                        
  ,CONVERT(DECIMAL(10, 2), MT.Amount) AS Amount                        
  ,MT.STATUS                        
  ,MT.[BeneficiaryName] AS BeneficiaryName                        
  ,MT.BeneficiaryAccountNo AS AccountNo                        
  ,MT.ProviderOprID AS TransactionID                      
  ,CONVERT(DECIMAL(10, 2), ISNULL(TD.openingbalance, 0)) AS opening                       
  ,CONVERT(DECIMAL(10, 2), ISNULL(TD.ClosingBalance, 0)) AS ClosingBal                        
  ,MT.[UserID]                        
  ,ISNULL(Mt.Bank, 'N/A') AS Bank                        
  ,CONVERT(DECIMAL(10, 2), ISNULL((TD.DiscountAmount),0)) AS Charges                        
  ,CONVERT(DECIMAL(10, 2), ISNULL(MT.Amount,0) - ISNULL((TD.DiscountAmount),0) ) AS NetAmount                        
  ,MT.Description AS Response                        
  ,MT.Through AS Through                   
  ,MT.MoneyTransferID                    
   ,MT.BillNumber                        
 FROM [dbo].[MoneyTransfer] AS MT                       
 INNER JOIN TransactionDetails TD ON (TD.RechargeID = MT.MoneyTransferID             
  AND TD.UserID = MT.UserID             
  AND TD.DOCType in ('MoneyTransfer', 'Beneficiary Verification')            
  AND TD.ServiceID = @DMRServiceID )                    
 WHERE --MT.Amount <> 0                       
   MT.[UserID] = @UserID                      
 ORDER BY MT.DOC DESC                    
END 


GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferSelectStatusByBillNumber]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- MoneyTransferStatusByBillNumber 'e66c4ce25f064657ae05'
create proc [dbo].[MoneyTransferSelectStatusByBillNumber]
@BillNumber varchar(100)
as
begin
	select [MoneyTransferID],[Description],
	[BeneficiaryID],[BeneficiaryName],[ProviderOprID],
	[Status],
	TD.ClosingBalance from [MoneyTransfer] M
	left join TransactionDetails TD on TD.RechargeID = M.[MoneyTransferID] and TD.DocType='MoneyTransfer'
	inner join UserInformation UI on UI.UserID = M.UserID and UI.UserID=TD.UserID	
	where [BillNumber] = @BillNumber
end
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferStatusByBillNumber]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- MoneyTransferStatusByBillNumber 'e66c4ce25f064657ae05'
CREATE proc [dbo].[MoneyTransferStatusByBillNumber]
@BillNumber varchar(100)
as
begin
	select [MoneyTransferID],[Description],
	[BeneficiaryID],[BeneficiaryName],[ProviderOprID],
	[Status],
	TD.ClosingBalance from [MoneyTransfer] M
	left join TransactionDetails TD on TD.RechargeID = M.[MoneyTransferID] and TD.DocType='MoneyTransfer'
	inner join UserInformation UI on UI.UserID = M.UserID and UI.UserID=TD.UserID	
	where [BillNumber] = @BillNumber
end
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                                              
-- Author:  Sushant Yelpale                                       
-- Create date: 4 Jan 2019        
-- Description: update DMR callback         
-- [MoneyTransferUpdate]  946, '3510160644', 'SUCCESS', 'Money transfer successful', 2, 44, 21, 500, '305c4b80-81f3-4767-ae02-4cfd863f1284', '836310659182', 150045.928      
-- =============================================                                     
CREATE PROCEDURE [dbo].[MoneyTransferUpdate] @MoneyTransferId INT      
 ,@ConsumerNo VARCHAR(20)      
 ,@Status VARCHAR(20)      
 ,@RechargeDesc VARCHAR(20)      
 ,@RechargeProviderID INT      
 ,@OperatorId INT      
 ,@ServiceID INT      
 ,@Amount NUMERIC(18, 5) = 0      
 ,@RequestID VARCHAR(100)      
 ,@TransID VARCHAR(50)      
 ,@ClosingBalance DECIMAL(18, 5) = NULL      
 ,@OutPut VARCHAR(100) OUT      
AS      
BEGIN      
 DECLARE @providerComm NUMERIC(18, 5) = 0;      
 DECLARE @providerCommType VARCHAR(10) = 'P';      
 DECLARE @providerOpBal NUMERIC(18, 5) = 0;      
 DECLARE @AfterTransAmt NUMERIC(18, 5)      
 DECLARE @TRANS_DESC VARCHAR(max)      
 DECLARE @UserID INT      
 DECLARE @PrevClosingBal DECIMAL(18, 5) = 0;      
 DECLARE @Success VARCHAR(20) = 'SUCCESS';      
 DECLARE @Process VARCHAR(20) = 'PROCESS';      
 DECLARE @Failed VARCHAR(20) = 'FAILED';      
 DECLARE @Refund VARCHAR(20) = 'REFUND';      
 DECLARE @ProviousStatus VARCHAR(20);      
 DECLARE @SystemAccount INT = 60;      
      
 SELECT @UserID = UserID      
  ,@ProviousStatus = [Status]      
 FROM dbo.MoneyTransfer(NOLOCK)      
 WHERE MoneyTransferID = @MoneyTransferId      
      
 IF (      
   NOT (      
    (      
     Upper(@ProviousStatus) = upper(@Process)      
     AND upper(@Status) = upper(@Success)      
     )      
    OR (      
     Upper(@ProviousStatus) = upper(@Process)      
     AND upper(@Status) = upper(@Failed)      
     )      
    OR (      
     Upper(@ProviousStatus) = upper(@Process)      
     AND upper(@Status) = upper(@Refund)      
     )      
    OR (      
     Upper(@ProviousStatus) = upper(@Failed)      
     AND upper(@Status) = upper(@Refund)      
     )      
    )      
   )      
 BEGIN      
  SET @Output = 'Transaction is already Processed';--  Dont Change Message; Check in CS Code      
      
  RETURN (0)      
 END      
      
 SET @Output = 'Something Went Wrong';--  Dont Change Message; Check in CS Code      
 SET @PrevClosingBal = (      
   SELECT TOP (1) DMRProviderClosingBalance      
   FROM dbo.MoneyTransfer(NOLOCK)      
   WHERE DMRProviderID = @RechargeProviderID      
    AND STATUS != @Process      
   ORDER BY MoneyTransferID DESC      
   )      
      
 IF @PrevClosingBal IS NULL      
 BEGIN      
  SELECT @PrevClosingBal = CurrentBalance      
  FROM dbo.RechargeProviderMaster(NOLOCK)      
  WHERE RechargeProviderID = @RechargeProviderID      
 END      
      
 UPDATE RechargeProviderMaster      
 SET CurrentBalance = @PrevClosingBal      
 WHERE RechargeProviderID = @RechargeProviderID      
      
 -----------------------------End------------------------------------                           
 SELECT @providerComm = isnull(CommissionAdmin, 0)      
  ,@providerCommType = Upper(MarginType)      
 FROM dbo.RechargeProviderOperatorMaster      
 WHERE RechargeProviderID = @RechargeProviderID      
  AND OperatorID = @OperatorId      
      
 PRINT '@providerComm' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType      
      
 IF @providerCommType = 'P'      
 BEGIN      
  SET @providerComm = (@providerComm * @Amount) / 100;      
      
  PRINT '@providerComm1' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType      
 END      
 ELSE      
 BEGIN      
  SET @providerComm = @providerComm;      
 END      
      
 IF (      
   Upper(@ProviousStatus) = upper(@Process)      
   AND upper(@Status) = upper(@Success)      
   )      
 BEGIN      
 IF @ClosingBalance is not null    
 BEGIN    
   UPDATE RechargeProviderMaster      
   SET CurrentBalance = @ClosingBalance      
   WHERE RechargeProviderID = @RechargeProviderID      
   END    
    
  UPDATE dbo.MoneyTransfer      
  SET [STATUS] = @Status      
   ,[Description] = @RechargeDesc      
   ,DMRProviderID = @RechargeProviderID      
   ,DMRProviderOpeningBalance = @PrevClosingBal      
   ,DMRCommissionn = @providerComm      
   ,ProviderOprID = @TransID      
   ,DMRProviderClosingBalance = @ClosingBalance   
   ,[UpdateDOC] = getdate()
  WHERE MoneyTransferID = @MoneyTransferId      
      
  --begin try              
  IF (@providerComm IS NOT NULL)      
  BEGIN      
   DECLARE @BeforeTransAmt NUMERIC(18, 5) = 0;      
      
   SELECT @BeforeTransAmt = CurrentBalance      
   FROM DBO.UserBalance      
   WHERE UserID = @SystemAccount      
      
   --Adde For System AccountBal=(ProviderCommission-(RetailerCommission + distributerCommission)                  
   DECLARE @RetailerCommission NUMERIC(18, 5) = 0;      
   DECLARE @DistributerCommission NUMERIC(18, 5) = 0;      
      
   SELECT @RetailerCommission = isnull(DiscountAmount, 0)      
   FROM TransactionDetails      
   WHERE RechargeID = @MoneyTransferId      
    AND ServiceID = @ServiceID      
    AND Upper(AmountCreditDebit) = Upper('DEBIT')      
    AND Upper(DOCType) = Upper('MoneyTransfer')      
      
   PRINT @RetailerCommission      
      
   SELECT @DistributerCommission = isnull(DiscountAmount, 0)      
   FROM TransactionDetails      
   WHERE RechargeID = @MoneyTransferId      
    AND ServiceID = @ServiceID      
    AND AmountCreditDebit = ''      
    AND Upper(DOCType) = Upper('MoneyTransfer')      
    AND UserID != @SystemAccount      
      
   DECLARE @TotalDeductedCom DECIMAL(18, 5) = 0;      
      
   SELECT @TotalDeductedCom = IsNull(Sum(DiscountAmount), 0)      
   FROM TransactionDetails      
   WHERE RechargeID = @MoneyTransferId      
    AND Upper(DOCType) = Upper('MoneyTransfer')      
    AND UserID != @SystemAccount      
      
   --Set @TotalDeductedCom = IsNull(@TotalDeductedCom,0);            
   DECLARE @benefitCommission NUMERIC(18, 5) = 0;      
      
   SET @benefitCommission = ISNULL(( @providerComm - @TotalDeductedCom),0);      
   --set @benefitCommission = @providerComm - (@RetailerCommission + @DistributerCommission)              
   SET @AfterTransAmt = @BeforeTransAmt + @benefitCommission;      
      
  IF @AfterTransAmt is not null    
 BEGIN    
    UPDATE dbo.UserBalance      
    SET CurrentBalance = @AfterTransAmt      
    WHERE UserID = @SystemAccount      
  END    
    
   DECLARE @Uname VARCHAR(50)      
      
   SELECT @Uname = name      
   FROM DBO.UserInformation(NOLOCK)      
   WHERE UserID = @UserID;      
      
   DECLARE @TransDesc VARCHAR(200)      
      
   SET @TransDesc = 'Provider Commision Amount:' + CONVERT(VARCHAR, @benefitCommission) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;      
      
   EXEC dbo.TransactionDetailsInsertByUser @SystemAccount      
    ,-- @UserID                          
    @MoneyTransferId      
    ,--@RechargeID                     
    @ServiceID      
    ,--@serviceID                    
    @UserID      
    ,--@AssignedUserID                    
    @ConsumerNo      
    ,--@ConsumerNumber                     
    'MoneyTransfer'      
    ,--@DOCType                    
    @BeforeTransAmt      
    ,--@OpeningBalance                         
    @AfterTransAmt      
    ,--@ClosingBalance                        
    0 --@TransactionAmount                                                     
    ,@benefitCommission      
    ,--@DiscountAmount                        
    ''      
    ,--@AmountCreditDebit                    
    'CREDIT'      
    ,--@CommissionCreditDebit                    
    @TransDesc --@TransactionDescription                        
  END      
 END      
 ELSE IF (      
   upper(@ProviousStatus) = upper(@Process)      
   AND upper(@Status) = upper(@Failed)      
   ) --- for failed DMR (update status only)        
 BEGIN      
  UPDATE dbo.MoneyTransfer      
  SET Description = 'Request Accepted Successfully'   
   ,STATUS = @Status      
   ,DMRProviderID = @RechargeProviderID      
   ,DMRProviderOpeningBalance = @PrevClosingBal      
   ,DMRCommissionn = @providerComm      
   ,DMRProviderClosingBalance = @ClosingBalance  
   ,[UpdateDOC] = getdate()   
  WHERE MoneyTransferID = @MoneyTransferId      
 END      
 ELSE IF (      
   (      
    upper(@ProviousStatus) = upper(@Process)      
    AND upper(@Status) = upper(@Refund)      
    )      
   OR (      
    upper(@ProviousStatus) = upper(@Failed)      
   AND upper(@Status) = upper(@Refund)      
    )      
   ) --- for Refunded DMR (update staus and reverse commission)        
 BEGIN      
  EXECUTE [MoneyTransferAmountRefund] @UserID      
   ,'REFUND' --@TransType          
   ,@ServiceID      
   ,@Amount      
   ,@ConsumerNo      
   ,@OperatorID      
   ,@Status      
   ,@RechargeDesc      
   ,@RechargeProviderID      
   ,'Callback'      
   ,'Callback' -- IpAddress        
   ,@MoneyTransferID      
 END      
 ELSE --FOR PENDING DMR  OR Fail DMR          
 BEGIN      
  UPDATE dbo.MoneyTransfer      
  SET Description = @RechargeDesc      
   ,DMRProviderID = @RechargeProviderID      
   ,DMRProviderOpeningBalance = @PrevClosingBal      
   ,DMRCommissionn = @providerComm      
   ,DMRProviderClosingBalance = @ClosingBalance  
   ,[UpdateDOC] = getdate()    
  WHERE MoneyTransferID = @MoneyTransferId      
 END      
      
 SET @OUTPUT = 'OK'      
END 
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferUpdateByID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc[dbo].[MoneyTransferUpdateByID] @MoneyTransferID INT          
 ,@Status VARCHAR(10)          
 ,@Message VARCHAR(100)          
 ,@TransactionID VARCHAR(50)          
 ,@ClosingBalance DECIMAL(18, 5)          
 ,@EpocketMID VARCHAR(50)      
 ,@SenderName VARCHAR(50) = null      
 ,@BeneficiaryName VARCHAR(50) = null     
AS          
BEGIN          
 UPDATE [MoneyTransfer]          
 SET [Status] = @Status          
  ,[Description] = @Message          
  ,[ProviderOprID] = @TransactionID          
  ,ProviderRefNo = @EpocketMID        
  ,DMRProviderClosingBalance=@ClosingBalance     
  ,[SenderName] = @SenderName  
  ,[BeneficiaryName] = @BeneficiaryName  
  ,[UpdateDOC] = getdate()
 WHERE MoneyTransferID = @MoneyTransferID          
END 
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferUpdateIDMR]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                        
CREATE PROC [dbo].[MoneyTransferUpdateIDMR] @UserID INT                
 ,@ProviderDOCNo VARCHAR(50) = NULL                
 ,@ProviderRefNo VARCHAR(50) = NULL                
 ,@ProviderOprID VARCHAR(50) = NULL                
 ,@ProviderRequestID VARCHAR(50)                
 ,@InstapayOpeningBal VARCHAR(50) = NULL                
 ,@InstapayClosingBal VARCHAR(50) = NULL                
 ,@Status VARCHAR(150)                
 ,@StatusCode VARCHAR(50) = NULL                
 ,@Description VARCHAR(max) = NULL                
 ,@DMRRequest VARCHAR(max) = NULL                
 ,@DMRResponse VARCHAR(max) = NULL                
 ,@BeneficiaryName VARCHAR(max) = NULL                
 ,@RechargeProviderID INT                
AS                
BEGIN                
 DECLARE @Success VARCHAR(20) = 'SUCCESS';                
 DECLARE @BeforeTransAmt NUMERIC(18, 5) = 0;                
 DECLARE @providerComm NUMERIC(18, 5) = 0;                
 DECLARE @providerCommType VARCHAR(10) = 'P';                
 DECLARE @transactionAmt NUMERIC(18, 5) = 0;                
 DECLARE @RechargeID INT;                
 DECLARE @SystemAccount INT = 60;                
 DECLARE @AdminuserID INT = 1;                
 DECLARE @AfterTransAmt NUMERIC(18, 5) = 0;                
 DECLARE @ServiceID INT;                
                
 DECLARE @BeneficiaryAccountNo VARCHAR(50);                
 DECLARE @DMROperatorID INT;                
                
 SELECT @RechargeId = MoneyTransferID                
  ,@transactionAmt = Amount                
  ,@ServiceID = DMRServiceID                
  ,@BeneficiaryAccountNo = BeneficiaryAccountNo                
  ,@DMROperatorID = DMROperatorID                
 FROM dbo.MoneyTransfer(NOLOCK)                
 WHERE ProviderRequestID = @ProviderRequestID                
                
 SELECT @providerComm = isnull(CommissionAdmin, 0)                
  ,@providerCommType = Upper(MarginType)                
 FROM dbo.RechargeProviderOperatorMaster                
 WHERE RechargeProviderID = @RechargeProviderID                
  AND OperatorID = @DMROperatorID                
                
 PRINT '@providerComm' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType                
                
 IF @providerCommType = 'P'                
 BEGIN                
  SET @providerComm = (@providerComm * @transactionAmt) / 100;                
  PRINT '@providerComm1' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType                
 END                
 ELSE                
 BEGIN                
  SET @providerComm = @providerComm;                
 END                
                
 PRINT '@providerComm'                
 PRINT @providerComm                
                
 IF (upper(@Status) = upper(@Success))                
 BEGIN                
  UPDATE [MoneyTransfer]                
  SET ProviderDOCNo = @ProviderDOCNo                
   ,ProviderRefNo = @ProviderRefNo                
   ,ProviderOprID = @ProviderOprID                
   ,DMRProviderOpeningBalance = @InstapayOpeningBal                
   ,DMRProviderClosingBalance = @InstapayClosingBal                
   ,[Status] = @Status                
   ,StatusCode = @StatusCode                
   ,[Description] = @Description              
   ,DMRRequest = @DMRRequest                
   ,DMRResponse = @DMRResponse                
   ,[BeneficiaryName] = @BeneficiaryName                
  WHERE UserID = @UserID                
   AND ProviderRequestID = @ProviderRequestID                
   AND [Status] = 'PROCESS'                
                
  IF (@providerComm IS NOT NULL)                
  BEGIN                
   SELECT @BeforeTransAmt = DMRBalance                
   FROM DBO.UserBalance(NOLOCK)                
   WHERE UserID = @SystemAccount                
     
   DECLARE @RetailerCommission NUMERIC(18, 5) = 0;                
                
   SELECT @RetailerCommission = isnull(DiscountAmount, 0)                
   FROM TransactionDetails                
   WHERE RechargeID = @RechargeId                
    AND Upper(AmountCreditDebit) = Upper('DEBIT')                
    AND Upper(DOCType) = Upper('MoneyTransfer')        
    AND UserId NOT IN (                
     @SystemAccount                
     ,@AdminuserID                
     )                
                
   PRINT '@RetailerCommission'                
   PRINT @RetailerCommission                
                
   DECLARE @UplineTotalCommission NUMERIC(18, 5) = 0;                
                
   SELECT @UplineTotalCommission = ISNULL(SUM(isnull(DiscountAmount, 0)),0)          
   FROM TransactionDetails                
  WHERE RechargeID = @RechargeId                
    AND AmountCreditDebit = ''                
    AND Upper(DOCType) in (Upper('MoneyTransfer'),Upper('Beneficiary Verification'))                
    AND UserId NOT IN (                
     @SystemAccount                
     ,@AdminuserID                
     )                      
   PRINT '@UplineTotalCommission'                
   PRINT @UplineTotalCommission                
                
   DECLARE @benefitCommission NUMERIC(18, 5) = 0;                
                
   SET @benefitCommission = @providerComm - (@RetailerCommission + @UplineTotalCommission)                
                
   PRINT @benefitCommission                
                
   SET @AfterTransAmt = @BeforeTransAmt + @benefitCommission;                
                
   UPDATE dbo.UserBalance                
   SET DMRBalance = @AfterTransAmt                
   WHERE UserID = @SystemAccount                
                
   DECLARE @Uname VARCHAR(50)                
                
   SELECT @Uname = name                
   FROM DBO.UserInformation(NOLOCK)                
   WHERE UserID = @UserID;                
                
   DECLARE @TransDesc VARCHAR(200)                
                
   SET @TransDesc = 'Provider Commision Amount:' + CONVERT(VARCHAR, @benefitCommission) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;                
                
   EXEC dbo.TransactionDetailsInsertByUser @SystemAccount                
    ,-- @UserID                                                
    @RechargeID                
    ,--@RechargeID                                           
    @ServiceID                
    ,--@serviceID                                          
    @UserID                
    ,--@AssignedUserID                                          
    @BeneficiaryAccountNo                
    ,--@ConsumerNumber                                           
    'MoneyTransfer'                
    ,--@DOCType                                          
    @BeforeTransAmt                
    ,--@OpeningBalance                                               
    @AfterTransAmt                
    ,--@ClosingBalance                                              
    0 --@TransactionAmount                                                                           
    ,@benefitCommission                
    ,--@DiscountAmount                                              
    ''                
    ,--@AmountCreditDebit                                          
    'CREDIT'                
    ,--@CommissionCreditDebit                                          
    @TransDesc --@TransactionDescription                                              
  END                
 END                
END 


GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferUpdateMannual]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                                                      
-- Author:  <Sainath Bhujbal>                                                      
-- Create date: <10 March 2017 ,>                                                      
-- Description: <Description,,>                                                      
-- =============================================            
-- [MoneyTransferUpdateMannual] 16,'REFUND','1230',3,'MoneyTransfer Fail-manual'                                          
CREATE PROCEDURE [dbo].[MoneyTransferUpdateMannual] @MoneyTransferId INT    
 ,@Status VARCHAR(10)    
 ,@TransactionID VARCHAR(50)    
 ,@UserID INT    
 ,@Description VARCHAR(100)    
AS    
BEGIN    
 -- pending(process) check             
 DECLARE @OriginalStatus VARCHAR(20);    
    
 SELECT @OriginalStatus = STATUS    
 FROM [dbo].[MoneyTransfer]    
 WHERE [MoneyTransferID] = @MoneyTransferId    
    
 PRINT @OriginalStatus    
    
 IF (Upper(@OriginalStatus) != Upper('PROCESS'))    
 BEGIN    
  --print  'in-Block'        
  RETURN;    
 END    
    
 IF (Upper(@Status) = Upper('SUCCESS'))    
 BEGIN    
  UPDATE [dbo].[MoneyTransfer]    
  SET [Status] = 'SUCCESS'    
   ,[ProviderRefNo] = @TransactionID    
   ,[ProviderOprID] = @TransactionID   
   ,[UpdateDOC]  = getdate()
  WHERE [MoneyTransferID] = @MoneyTransferId    
    
  RETURN;    
 END    
    
 IF (Upper(@Status) = Upper('REFUND'))    
 BEGIN    
  UPDATE [dbo].[MoneyTransfer]    
  SET [Status] = 'REFUND'    
   ,[ProviderOprID] = @Description    
   ,[Description] = @Description 
   ,[UpdateDOC]  = getdate()   
  WHERE [MoneyTransferID] = @MoneyTransferId    
    
  DECLARE @comAmt NUMERIC(18, 5)   
  DECLARE @GST NUMERIC(18, 5)  
  DECLARE @TDS NUMERIC(18, 5)  
    
  DECLARE @TransactionAmount NUMERIC(18, 5)    
  DECLARE @ServiceID INT    
  DECLARE @ConsumerNo VARCHAR(20)    
    
  SELECT @comAmt = DiscountAmount  
 ,@GST = GST  
 ,@TDS = TDS    
   ,@TransactionAmount = [TransactionAmount]    
   ,@ServiceID = [ServiceID]    
   ,@ConsumerNo = [ConsumerNumber]    
  FROM dbo.TransactionDetails(NOLOCK)    
  WHERE RechargeID = @MoneyTransferId    
   AND DOCType = 'MoneyTransfer'    
   AND AmountCreditDebit = 'DEBIT'    
   AND UserID = @UserID    
    
  IF (@comAmt IS NULL)    
  BEGIN    
   PRINT '@dcomAmt was NULL, Set it to 0'    
    
   SET @comAmt = 0    
  END    
    
  DECLARE @beforeTransAmt NUMERIC(18, 5)    
    
  SELECT @beforeTransAmt = DMRBalance    
  FROM dbo.UserBalance    
  WHERE UserID = @UserID    
    
  PRINT '@@beforeTransAmt' + convert(VARCHAR(30), @beforeTransAmt)    
    
  DECLARE @afterTransAmt NUMERIC(18, 5)    
    
  SELECT @afterTransAmt = (@beforeTransAmt + @TransactionAmount) - @comAmt  + (@GST + @TDS);  
    
  PRINT '@@afterTransAmt' + convert(VARCHAR(30), @afterTransAmt)    
    
  UPDATE [DBO].UserBalance    
  SET DMRBalance = @afterTransAmt    
  WHERE UserID = @UserID    
    
  DECLARE @Uname VARCHAR(100)    
    
  SELECT @Uname = name    
  FROM DBO.UserInformation(NOLOCK)    
  WHERE UserID = @UserID;    
    
  DECLARE @TransDesc VARCHAR(500)    
    
  SET @TransDesc = 'CREDIT MoneyTransfer REFUNDED Of Rs.' + CONVERT(VARCHAR, CONVERT(DECIMAL(18, 2), @TransactionAmount)) + ' For Consumer No. ' + @ConsumerNo + ' SERVICE ID.' + Convert(VARCHAR, @ServiceID)    
    
  EXEC dbo.TransactionDetailsInsertByUser @UserID    
   ,@MoneyTransferId    
   ,@ServiceID    
   ,'0'    
   ,@ConsumerNo    
   ,'REFUND DMT'    
   ,@beforeTransAmt    
   ,@afterTransAmt    
   ,@TransactionAmount    
   ,@comAmt    
   ,'CREDIT'    
   ,'CREDIT'    
   ,@TransDesc    
   ,@GST  
   ,@TDS  
  --------------------------------FOR Upline Account-------------------------------------------------------                                  
  DECLARE @dcomAmt NUMERIC(18, 5)    
  DECLARE @ParentID INT = 0;    
    
  BEGIN TRY    
   SELECT @ParentID = ISNULL(ParentID, 0)    
   FROM DBO.UserInformation(NOLOCK)    
   WHERE UserID = @UserID;    
    
   SELECT @Uname = name    
   FROM DBO.UserInformation(NOLOCK)    
   WHERE UserID = @UserID;    
  END TRY    
    
  BEGIN CATCH    
   RETURN    
  END CATCH    
    
  IF (    
    @ParentID IS NOT NULL    
    AND @ParentID != 0    
    )    
  BEGIN    
   SELECT @dcomAmt = (    
     SELECT TOP (1) DiscountAmount    
     FROM dbo.TransactionDetails(NOLOCK)    
     WHERE RechargeID = @MoneyTransferId    
      AND DOCType = 'MoneyTransfer'    
      AND AmountCreditDebit = ''    
      AND CommissionCreditDebit = 'CREDIT'    
      AND UserID = @ParentID    
     )    
    
   PRINT '@dcomAmt' + convert(VARCHAR(30), @dcomAmt) + ':'    
    
   IF (@dcomAmt IS NULL)    
   BEGIN    
    PRINT '@dcomAmt was NULL, Set it to 0'    
    
    SET @dcomAmt = 0    
   END    
    
   -- IF (@comAmt != 0)               
   BEGIN    
    SET @beforeTransAmt = 0;    
    
    SELECT @beforeTransAmt = DMRBalance    
    FROM dbo.UserBalance    
    WHERE UserID = @ParentID    
    
    SET @afterTransAmt = 0;    
    
    SELECT @afterTransAmt = (@beforeTransAmt - @dcomAmt)    
    
    UPDATE [DBO].UserBalance    
    SET DMRBalance = @afterTransAmt    
    WHERE UserID = @ParentID    
    
    SET @TransDesc = 'DEBIT Reversed Commision Of Rs. ' + CONVERT(VARCHAR, CONVERT(DECIMAL(18, 5), @dcomAmt)) + 'For ServiceID Of ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;    
    
    --UPLINE COMM AND BALANCE                                      
    EXEC dbo.TransactionDetailsInsertByUser @ParentID    
     ,@MoneyTransferId    
     ,@ServiceID    
     ,@UserID    
     ,@ConsumerNo    
     ,'REVERSE DMT'    
     ,@beforeTransAmt    
     ,@afterTransAmt    
     ,0 --@TransactionAmount                                                         
     ,@dcomAmt    
     ,'DEBIT'    
     ,'DEBIT'    
     ,@TransDesc    
   END    
  END    
    
  --------------------------  SUPER DISTRIBUTOR ----------------------------------                     
  DECLARE @SuperParentID INT = 0;    
  DECLARE @SuperParentTypeID INT = 0;    
  DECLARE @SuperdcomAmt NUMERIC(18, 5)    
    
  SELECT @SuperParentID = ISNULL(ParentID, 0)    
   ,@SuperParentTypeID = ISNULL([UserTypeID], 0)    
  FROM DBO.UserInformation(NOLOCK)    
  WHERE UserID = @ParentID;    
    
  DECLARE @AdminTypeID INT = 1;    
    
  IF (    
    @SuperParentID IS NOT NULL    
    AND @SuperParentID != 0    
    AND @SuperParentID != @AdminTypeID    
    )    
  BEGIN    
   SELECT @SuperdcomAmt = (    
     SELECT TOP (1) DiscountAmount    
     FROM dbo.TransactionDetails(NOLOCK)    
     WHERE RechargeID = @MoneyTransferId    
      AND DOCType = 'MoneyTransfer'    
      AND AmountCreditDebit = ''    
      AND CommissionCreditDebit = 'CREDIT'    
      AND UserID = @SuperParentID    
     )    
    
   IF (@SuperdcomAmt IS NULL)    
   BEGIN    
    PRINT '@SuperdcomAmt was NULL, Set it to 0'    
    
    SET @SuperdcomAmt = 0    
   END    
    
   PRINT '@SuperdcomAmt' + convert(VARCHAR(30), @SuperdcomAmt) + ':'    
    
   SET @beforeTransAmt = 0;    
    
   SELECT @beforeTransAmt = DMRBalance    
   FROM dbo.UserBalance    
   WHERE UserID = @ParentID    
    
   SET @afterTransAmt = 0;    
    
   SELECT @afterTransAmt = (@beforeTransAmt - @dcomAmt)    
    
   UPDATE [DBO].UserBalance    
   SET DMRBalance = @afterTransAmt    
   WHERE UserID = @ParentID    
    
   SET @TransDesc = 'DEBIT Reversed Commision Of Rs. ' + CONVERT(VARCHAR, CONVERT(DECIMAL(18, 5), @SuperdcomAmt)) + 'For ServiceID Of ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;    
    
   EXEC dbo.TransactionDetailsInsertByUser @ParentID    
    ,@MoneyTransferId    
    ,@ServiceID    
    ,@UserID    
    ,@ConsumerNo    
    ,'REVERSE DMT'    
    ,@beforeTransAmt    
    ,@afterTransAmt    
    ,0 --@TransactionAmount                                                         
    ,@SuperdcomAmt    
    ,'DEBIT'    
    ,'DEBIT'    
    ,@TransDesc    
  END    
 END    
END 
GO
/****** Object:  StoredProcedure [dbo].[MoneyTransferUpdateMannualIDMR]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [MoneyTransferUpdateMannualIDMR] 64,'DIDUCTION','REFUND','REFUND','0'                      
CREATE PROCEDURE [dbo].[MoneyTransferUpdateMannualIDMR] @RechargeID INT            
 ,@TransType VARCHAR(20)            
 ,@Status VARCHAR(20)            
 ,@RechargeDescription VARCHAR(100)            
 ,@TransactionID VARCHAR(50) = NULL            
 ,@DMRRequest VARCHAR(Max) = NULL            
 ,@DMRResponse VARCHAR(Max) = NULL            
 ,@RechargeProviderOpeningBalance NUMERIC(18, 5) = NULL            
 ,@RechargeProviderClosingBalance NUMERIC(18, 5) = NULL            
AS            
BEGIN            
 DECLARE @UserID INT            
 DECLARE @OperatorID INT            
 DECLARE @ServiceID INT            
 DECLARE @Amount NUMERIC(18, 5)            
 DECLARE @ConsumerNo VARCHAR(20)            
 DECLARE @RechargeProviderID INT            
 DECLARE @Output INT            
 DECLARE @Result INT            
 DECLARE @isAlreadyFail INT            
 DECLARE @success VARCHAR(20) = 'SUCCESS';            
 DECLARE @Refund VARCHAR(20) = 'REFUND';            
 DECLARE @Process VARCHAR(20) = 'PROCESS';            
 DECLARE @fail VARCHAR(20) = 'FAIL';            
 DECLARE @SystemAccount INT = 60;            
 DECLARE @OriginalStatus VARCHAR(50)            
 DECLARE @ProviderOpeningBalance DECIMAL(18, 5) = 0;            
 DECLARE @ProviderClosingBalance DECIMAL(18, 5) = 0;            
 DECLARE @UserOpBAL DECIMAL(18, 5) = 0;            
            
 SELECT @UserID = UserID            
  ,@OperatorID = DMROperatorID            
  ,@ServiceID = DMRServiceID            
  ,@Amount = amount            
  ,@ConsumerNo = BeneficiaryAccountNo            
  ,@RechargeProviderID = DMRProviderID            
  ,@OriginalStatus = STATUS            
  ,@UserOpBAL = UserOpeningBalance            
 FROM [dbo].MoneyTransfer(NOLOCK)            
 WHERE MoneyTransferID = @RechargeID            
            
 SET @Output = 0            
            
 PRINT @Output            
            
 IF (upper(@OriginalStatus) != upper(@Process))            
 BEGIN            
  PRINT '--is Already Processed Recharge--'            
  RETURN @Output            
 END            
            
 IF (upper(@Status) = upper(@success)            
  AND upper(@OriginalStatus) = upper(@Process)            
  )            
 BEGIN            
  PRINT '--success--'            
            
  SET @TransType = 'DEDUCTION'            
            
  DECLARE @afterTransAmt NUMERIC(18, 5);            
  DECLARE @beforeTransAmt NUMERIC(18, 5);            
            
  -- Select current balance before transaction...                                                                    
  SELECT @beforeTransAmt = DMRBalance            
  FROM dbo.UserBalance            
  WHERE UserID = @UserID            
            
  --Update Recharge status and RechargeDescription,OpeningBalance                                                              
  UPDATE dbo.MoneyTransfer            
  SET TransactionType = @TransType            
   ,UserOpeningBalance = @beforeTransAmt            
   ,STATUS = @Status            
   ,Description = @RechargeDescription            
   ,ProviderOprID = @TransactionID            
   ,DMRRequest = @DMRRequest            
   ,DMRResponse = @DMRResponse            
  WHERE MoneyTransferID = @RechargeID            
            
  SELECT @Result = @@IDENTITY            
            
  SET @Output = 1            
            
  --------------Transactiondetails Insert-------------------------------------                                                                
  DECLARE @PrevClosingBal DECIMAL(18, 5) = 0;            
            
  SET @ProviderOpeningBalance = (            
    SELECT TOP (1) DMRProviderClosingBalance            
    FROM dbo.MoneyTransfer(NOLOCK)            
    WHERE DMRProviderID = @RechargeProviderID            
     AND STATUS != 'PROCESS'            
    ORDER BY MoneyTransferID DESC            
    )            
            
  IF @PrevClosingBal IS NULL            
  BEGIN            
   SELECT @PrevClosingBal = CurrentBalance    
   FROM dbo.RechargeProviderMaster(NOLOCK)            
   WHERE RechargeProviderID = @RechargeProviderID            
  END            
            
  DECLARE @providerComm NUMERIC(18, 5);            
  DECLARE @providerCommType VARCHAR(10) = 'P';            
            
  SELECT @providerComm = isnull(CommissionAdmin, 0)            
   ,@providerCommType = Upper(MarginType)            
  FROM dbo.RechargeProviderOperatorMaster            
  WHERE RechargeProviderID = @RechargeProviderID            
   AND OperatorID = @OperatorId            
            
  PRINT '@providerComm' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType            
            
  IF @providerCommType = 'P'            
  BEGIN            
   SET @providerComm = (@providerComm * @Amount) / 100;            
            
   PRINT '@providerComm1' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType            
  END            
  ELSE            
  BEGIN            
   SET @providerComm = @providerComm;            
  END            
            
  -- begin try                                                                
  IF (@providerComm IS NOT NULL)            
  BEGIN            
   SELECT @BeforeTransAmt = DMRBalance            
   FROM DBO.UserBalance(NOLOCK)            
   WHERE UserID = @SystemAccount            
            
   --Adde For System AccountBal=(ProviderCommission-(RetailerCommission + distributerCommission)                                                  
   DECLARE @RetailerCommission NUMERIC(18, 5) = 0;            
   DECLARE @DistributerCommission NUMERIC(18, 5) = 0;            
            
   SELECT @RetailerCommission = isnull(DiscountAmount, 0)            
   FROM TransactionDetails            
   WHERE RechargeID = @RechargeId            
    AND Upper(AmountCreditDebit) = Upper('DEBIT')            
    AND (            
     Upper(DOCType) = Upper('MoneyTransfer')            
     OR Upper(DOCType) = Upper('Beneficiary Verification')            
     )            
            
   PRINT @RetailerCommission            
            
   SELECT @DistributerCommission = SUM(isnull(DiscountAmount, 0))            
   FROM TransactionDetails            
   WHERE RechargeID = @RechargeId            
    AND AmountCreditDebit = ''            
    AND Upper(DOCType) = Upper('MoneyTransfer')            
    AND UserID != @SystemAccount            
            
   DECLARE @benefitCommission NUMERIC(18, 5) = 0;            
            
   SET @benefitCommission = @providerComm - (@RetailerCommission + @DistributerCommission)            
   SET @AfterTransAmt = @BeforeTransAmt + @benefitCommission;            
            
   -- set @AfterTransAmt = @BeforeTransAmt + @providerComm;                                                            
   UPDATE dbo.UserBalance            
   SET DMRBalance = @AfterTransAmt            
   WHERE UserID = @SystemAccount            
            
   DECLARE @Uname VARCHAR(50)            
            
   SELECT @Uname = name            
   FROM DBO.UserInformation(NOLOCK)            
   WHERE UserID = @UserID;            
            
   DECLARE @TransDesc VARCHAR(200)            
            
   SET @TransDesc = 'Provider Commision Amount:' + CONVERT(VARCHAR, @benefitCommission) + ', ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;            
            
   EXEC dbo.TransactionDetailsInsertByUser @SystemAccount            
    ,@RechargeID            
    ,@ServiceID            
    ,@UserID            
    ,@ConsumerNo            
    ,'RECHARGE'            
    ,@BeforeTransAmt            
    ,@AfterTransAmt            
    ,0 --@TransactionAmount                                                                                   
    ,@benefitCommission            
    ,''            
    ,'CREDIT'            
    ,@TransDesc            
  END            
            
  RETURN @output            
 END            
            
 IF (            
   upper(@STATUS) = upper(@Refund)            
   AND upper(@OriginalStatus) = upper(@Process)            
 )            
 BEGIN            
  PRINT '--@Refund--'            
            
  UPDATE dbo.MoneyTransfer            
  SET STATUS = @Status            
   ,Description = @RechargeDescription            
   ,TransactionType = @TransType            
   ,ProviderOprID = @TransactionID            
   ,UserClosingBalance = @UserOpBAL            
   ,DMRRequest = @DMRRequest            
   ,DMRResponse = @DMRResponse           
  WHERE MoneyTransferID = @RechargeID            
            
  SELECT @Result = @@IDENTITY            
            
  SET @Output = 1            
            
  PRINT @Output            
  PRINT '--UPDATE STATUS FOR FAIL--'            
            
  --------------Transactiondetails Refund Insert -------------------------------------                                                       
  EXEC dbo.[TransactionDetailsRefundInsertDMR] @UserID        
   ,@RechargeID            
   ,@ServiceID            
   ,@ConsumerNo            
   ,@Amount            
   ,@OperatorID            
   ,@RechargeProviderID            
 END            
            
 RETURN @Output            
END 


GO
/****** Object:  StoredProcedure [dbo].[MoneyTransfreCommissionSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC[dbo].[MoneyTransfreCommissionSelect] @UserID INT
AS

BEGIN
    if exists (
            select [GroupID]
            from UserwiseSlabCommission
            where userID = @UserID
            )
    begin
    SELECT [FromAmount]
        ,[ToAmount]
        ,[RetailerFixRs]
        ,[RetailerFlexi]
        ,[DistributorFixRs]
        ,[DistributorFlexi]
        ,[SuperDistributorFixRs]
        ,[SuperDistributorFlexi]
    FROM [GroupDetail] GD
    INNER JOIN [dbo].[GroupMaster] GM ON GM.[ID] = GD.[GroupID]
    INNER JOIN [dbo].[UserwiseSlabCommission] UM ON GD.[GroupID] = UM.[GroupID]
    INNER JOIN [dbo].[UserInformation] UI ON UI.UserID = UM.[UserID]
    WHERE UI.UserID = @UserID
    
    end
    else
    begin
    SELECT [FromAmount]
        ,[ToAmount]
        ,[RetailerFixRs]
        ,[RetailerFlexi]
        ,[DistributorFixRs]
        ,[DistributorFlexi]
        ,[SuperDistributorFixRs]
        ,[SuperDistributorFlexi]
    FROM [dbo].[SlabCommission]
    end
END


GO
/****** Object:  StoredProcedure [dbo].[NotificationInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :7-3-17
--Purpose Notification Insert
--==========================================
CREATE PROCEDURE [dbo].[NotificationInsert] @Notification VARCHAR(250)
AS
BEGIN
	INSERT INTO Notification (NotificationText)
	VALUES (@Notification)
END



GO
/****** Object:  StoredProcedure [dbo].[NotificationSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NotificationSelect]
AS
BEGIN
	SELECT TOP (1) NotificationText
	FROM Notification
	ORDER BY DOC DESC
END



GO
/****** Object:  StoredProcedure [dbo].[OperatorMasteCommissionUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OperatorMasteCommissionUpdate] @OperatorID INT
	,@OperatorName VARCHAR(50)
	,@OperatorCode VARCHAR(50)
	,@RetailerCommissionAmount DECIMAL(18, 5)
	,@DistributorCommissionAmount DECIMAL(18, 5)
	,@MarginType VARCHAR(2)
	,@SuperDistCommission DECIMAL(18, 5) = 0
AS
BEGIN
	UPDATE OperatorMaster
	SET OperatorName = @OperatorName
		,RetailerCommissionAmount = @RetailerCommissionAmount
		,OperatorCode = @OperatorCode
		,DistributorCommissionAmount = @DistributorCommissionAmount
		,MarginType = @MarginType
		,SuperDistributorCommissionAmount = @SuperDistCommission
	WHERE OperatorID = @OperatorID
	--add block at end ---
	UPDATE [dbo].[RechargeGroupCommission]
	SET OperatorCode = @OperatorCode
	WHERE operatorid = @OperatorID
END

GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterConfigurationUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :9-7-17
--Purpose OperatorMaster Configuration update
--==========================================
CREATE PROCEDURE [dbo].[OperatorMasterConfigurationUpdate] @OperatorID INT
	,@MinimumRechargeAmount DECIMAL(18, 2)
	,@MaximumRechargeAmount DECIMAL(18, 2)
	,@RepeatRecharge INT
	,@IsOn BIT
AS
BEGIN
	UPDATE OperatorMaster
	SET MinimumRechargeAmount = @MinimumRechargeAmount
		,MaximumRechargeAmount = @MaximumRechargeAmount
		,RepeatRecharge = @RepeatRecharge
		,IsOn = @IsOn
	WHERE OperatorID = @OperatorID
END



GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther  :Priyanka
--Date    :8-7-17
--Purpose :Delete Operator from AddOperator
--==========================================
CREATE PROCEDURE [dbo].[OperatorMasterDelete] @OperatorID INT
	,@IsDelete INT
AS
BEGIN
	UPDATE OperatorMaster
	SET IsDelete = @IsDelete
	WHERE OperatorID = @OperatorID
END



GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--==========================================  

--Auther :Priyanka  

--Date :9-7-17  

--Purpose OperatorMaster Insert  

--==========================================  

--OperatorMasterInsert 1,'Idea',1.1,1.1,'%'  

CREATE PROCEDURE [dbo].[OperatorMasterInsert] @ServiceID INT

	,@OperatorName VARCHAR(50)

	,@OperatorCode VARCHAR(50)

	,@MinimumRechargeAmount DECIMAL

	,@MaximumRechargeAmount DECIMAL

	,@RepeatRecharge INT

	,@RetailerCommissionAmount DECIMAL(18, 5)

	,@DistributorCommissionAmount DECIMAL(18, 5)

	,@MarginType VARCHAR(50)

	,@SuperDistCommission decimal (18,5)  

AS

BEGIN

	INSERT INTO OperatorMaster (

		ServiceID

		,OperatorName

		,OperatorCode

		,MinimumRechargeAmount

		,MaximumRechargeAmount

		,RetailerCommissionAmount

		,DistributorCommissionAmount

		,MarginType

		,SuperDistributorCommissionAmount
		)

	VALUES (

		@ServiceID

		,@OperatorName

		,@OperatorCode

		,@MinimumRechargeAmount

		,@MaximumRechargeAmount

		,@RetailerCommissionAmount

		,@DistributorCommissionAmount

		,@MarginType
		,@SuperDistCommission
		);

END



GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterOpcodeValidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OperatorMasterOpcodeValidation] @Opcode VARCHAR(200)
AS
BEGIN
	SELECT OperatorID
		,OperatorCode
	FROM OperatorMaster
	WHERE OperatorCode = @Opcode
END



GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterOperatorByServiceID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OperatorMasterOperatorByServiceID] @ServiceID INT
AS
BEGIN
	SELECT OperatorID
		,e1.ServiceID
		,OperatorName
		,IsNull(OperatorCode, '0') AS OperatorCode
		,MinimumBalance
		,PendingCount
		,MinimumRechargeAmount
		,MaximumRechargeAmount
		,RepeatRecharge
		,IsOn
		,ServiceName
		,dbo.OperatorMaster.BlockedTill
		,dbo.OperatorMaster.IsTimelyBlocked
	FROM dbo.ServiceMaster e1
	INNER JOIN dbo.OperatorMaster ON e1.ServiceID = dbo.OperatorMaster.ServiceID
		AND e1.ServiceID = @ServiceID
	WHERE dbo.OperatorMaster.IsDelete = 0
	ORDER BY dbo.OperatorMaster.ServiceID
		,dbo.OperatorMaster.OperatorName
END



GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OperatorMasterSelect]
AS
BEGIN
	SELECT 'Select Operator' AS OperatorName
		,'' AS OperatorCode
		,0.0 AS RetailerCommissionAmount
		,0.0 AS DistributorCommissionAmount
		,'' AS MarginType
		,'' AS ServiceName
		,0 AS IsDelete
		,0 AS OperatorID
		,0 AS ServiceID
		,0.0 AS SuperDistributorCommissionAmount
	FROM dbo.OperatorMaster OM
	INNER JOIN dbo.ServiceMaster ON OM.ServiceID = ServiceMaster.ServiceID
	
	UNION
	
	SELECT OM.OperatorName
		,OM.OperatorCode
		,OM.RetailerCommissionAmount
		,OM.DistributorCommissionAmount
		,OM.MarginType
		,SM.ServiceName
		,OM.IsDelete
		,OM.OperatorID
		,OM.ServiceID
		,OM.SuperDistributorCommissionAmount
	FROM dbo.OperatorMaster(NOLOCK) OM
	INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON OM.ServiceID = SM.ServiceID
		AND IsOn = 1
		AND OM.IsDelete = 0
	ORDER BY ServiceID
		,OperatorName;
END



GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterSelectForRecharge]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OperatorMasterSelectForRecharge]
AS
BEGIN
	SELECT 'Select Operator' AS OperatorName
		,'' AS ServiceName
		,0 AS OperatorID
		,0 AS ServiceID
	FROM dbo.OperatorMaster OM
	INNER JOIN dbo.ServiceMaster ON OM.ServiceID = ServiceMaster.ServiceID
	
	UNION
	
	SELECT OM.OperatorName
		,SM.ServiceName
		,OM.OperatorID
		,OM.ServiceID
	FROM dbo.OperatorMaster OM
	INNER JOIN dbo.ServiceMaster SM ON OM.ServiceID = SM.ServiceID
		AND IsDelete = 0
		AND IsOn = 1
	ORDER BY OperatorName;
END



GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterSelectNotAllowedQuantity]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Sushant yelpale    
-- Create date: 17/01/2019    
-- Description: select Not Allowed Quantity     
-- =============================================    
CREATE PROCEDURE [dbo].[OperatorMasterSelectNotAllowedQuantity]    
 @OPERATORID int,    
 @OUTPUT Varchar(Max) OUT    
AS    
BEGIN    
 select @OUTPUT= NotAllowedQuantity    
 from OperatorMaster     
 where OperatorID = @OPERATORID    
END    
GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterSelectOperatorCommissionByOperatorID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :9-7-17
--Purpose OperatorMaster Commission update
--==========================================
CREATE PROCEDURE [dbo].[OperatorMasterSelectOperatorCommissionByOperatorID] @OperatorID INT
AS
BEGIN
	SELECT OM.OperatorName
		,OM.OperatorCode
		,OM.RetailerCommissionAmount
		,OM.DistributorCommissionAmount
		,OM.MarginType
		,SM.ServiceID
		,SM.ServiceName
		,OM.IsDelete
		,OM.OperatorID
		,OM.SuperDistributorCommissionAmount
	FROM dbo.OperatorMaster OM
	INNER JOIN dbo.ServiceMaster SM ON OM.ServiceID = SM.ServiceID
	WHERE OperatorID = @OperatorID
		AND IsDelete = 0;
END



GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterSelectOperatorswithConfiguration]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
--==========================================      
--Auther :Priyanka      
--Date :8-2-17      
--Purpose :Get OperatorMaster select operators with configuration      
--==========================================      
CREATE PROCEDURE [dbo].[OperatorMasterSelectOperatorswithConfiguration]  
AS  
BEGIN  
 SELECT OperatorID  
  ,e1.ServiceID  
  ,IsNull(OperatorCode, '0') AS OperatorCode  
  ,OperatorName  
  ,MinimumBalance  
  ,PendingCount  
  ,MinimumRechargeAmount  
  ,MaximumRechargeAmount  
  ,RepeatRecharge  
  ,IsOn  
  ,ServiceName  
  ,dbo.OperatorMaster.BlockedTill  
  ,dbo.OperatorMaster.IsTimelyBlocked 
  ,dbo.OperatorMaster.NotallowedQuantity
 FROM dbo.ServiceMaster e1  
 INNER JOIN dbo.OperatorMaster ON e1.ServiceID = dbo.OperatorMaster.ServiceID  
 WHERE dbo.OperatorMaster.IsDelete = 0  
 ORDER BY dbo.OperatorMaster.ServiceID  
  ,dbo.OperatorMaster.OperatorName  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[OperatorMasterUpdate] @OperatorID INT
	,@Min NUMERIC(18, 5)
	,@Max NUMERIC(18, 5)
	,@Repeat INT
	,@BlockedTill DATETIME = NULL
	,@IsTimelyBlocked BIT = NULL
	,@IsOn INT
AS
BEGIN
	UPDATE [dbo].[OperatorMaster]
	SET [MinimumRechargeAmount] = @Min
		,[MaximumRechargeAmount] = @Max
		,[RepeatRecharge] = @Repeat
		,BlockedTill = @BlockedTill
		,IsTimelyBlocked = @IsTimelyBlocked
		,[IsOn] = @IsOn
	WHERE [OperatorID] = @OperatorID
END



GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterUpdateBlockDenomination]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Sushant Yelpale  
-- Create date: 17/01/2019  
-- =============================================  
CREATE PROCEDURE [dbo].[OperatorMasterUpdateBlockDenomination]  
 @OperatorID int  
 ,@BlockDenomination varchar(Max)  
AS  
BEGIN  
 update OperatorMaster  
 set NotAllowedQuantity = @BlockDenomination  
 where OperatorID = @OperatorID  
END  
GO
/****** Object:  StoredProcedure [dbo].[OperatorMasterValidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :9-7-17
--Purpose OperatorMaster Validation
--==========================================
CREATE PROCEDURE [dbo].[OperatorMasterValidation] @OperatorName VARCHAR(50)
	,@ServiceID INT
AS
BEGIN
	SELECT OperatorID
	FROM OperatorMaster
	WHERE OperatorName = @OperatorName
		AND ServiceID = @ServiceID
END



GO
/****** Object:  StoredProcedure [dbo].[OperatorUserwiseDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:  Sushant Yelpale  
-- ALTER  date: 28/06/2018  
-- Description: Delete all operators userwise  
-- =============================================  
CREATE PROCEDURE [dbo].[OperatorUserwiseDelete] @UserID INT
AS
BEGIN
	DELETE
	FROM OperatorUserwise
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[OperatorUserwiseSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:  Sushant Yelpale    
-- ALTER  date: 27/06/2018    
-- Description: Selects All Operators with Usage Limit    
-- [OperatorUserwiseSelect] 1045    
-- =============================================    
CREATE PROCEDURE [dbo].[OperatorUserwiseSelect] @UserID INT
AS
BEGIN
	SELECT OM.OperatorID
		,ISNULL(OU.IsOn, OM.IsOn) AS IsOn
		,@UserID AS UserID
		,OM.OperatorName
		,ISNULL(OU.UsageLimit, - 1) AS UsageLimit
		,OU.DOC
		,OU.IsDelete
	FROM OperatorMaster OM
	LEFT OUTER JOIN OperatorUserwise OU ON OM.OperatorID = OU.OperatorID
		AND OU.UserID = @UserID
	WHERE OM.IsDelete = 0
	ORDER BY OM.OperatorID;
END



GO
/****** Object:  StoredProcedure [dbo].[OperatorUserwiseUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:  Sushant Yelpale  
-- ALTER  date: 28/06/2018  
-- Description: Update Perticular operator userwise  
-- =============================================  
CREATE PROCEDURE [dbo].[OperatorUserwiseUpdate] @UserID INT
	,@OperatorID INT
	,@UsageLimit DECIMAL(18, 5)
	,@IsOn BIT
AS
BEGIN
	IF EXISTS (
			SELECT TOP (1) UserID
			FROM OperatorUserwise(NOLOCK)
			WHERE UserID = @UserID
				AND OperatorID = @OperatorID
			)
	BEGIN
		UPDATE OperatorUserwise
		SET UsageLimit = @UsageLimit
			,IsOn = @IsOn
		WHERE UserID = @UserID
			AND OperatorID = @OperatorID
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[OperatorUserwise] (
			[UserID]
			,[OperatorID]
			,[IsOn]
			,[UsageLimit]
			)
		VALUES (
			@UserID
			,@OperatorID
			,@IsOn
			,@UsageLimit
			)
	END
END



GO
/****** Object:  StoredProcedure [dbo].[OtherComplaintInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[OtherComplaintInsert] @UserID INT
	,@Description VARCHAR(max)
AS
BEGIN
	INSERT INTO OtherComplaint (
		UserID
		,[Description]
		)
	VALUES (
		@UserID
		,@Description
		)
END



GO
/****** Object:  StoredProcedure [dbo].[OTPCheckValid]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================  
--Auther :Rahul Hembade  
--Date :9-7-17  
--Purpose OperatorMaster Insert  
--==========================================  
--OperatorMasterInsert 1,'Idea',1.1,1.1,'%'  
CREATE PROCEDURE [dbo].[OTPCheckValid] @UserID INT
	,@OTP VARCHAR(10)
AS
BEGIN
	DECLARE @OTPID INT

	SELECT TOP 1 @OTPID = (OTPID)
	FROM [dbo].[OTP](NOLOCK)
	WHERE [UserID] = @UserID
		AND [OTP] = @OTP
	ORDER BY DOC DESC

	IF @OTPID > 0
	BEGIN
		UPDATE [dbo].[OTP]
		SET [IsVerified] = 1
		WHERE OTPID = @OTPID

		RETURN 1
	END
	ELSE
	BEGIN
		RETURN - 2
	END
END



GO
/****** Object:  StoredProcedure [dbo].[OTPInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================  
--Auther :Priyanka  
--Date :9-7-17  
--Purpose OperatorMaster Insert  
--==========================================  
--OperatorMasterInsert 1,'Idea',1.1,1.1,'%'  
CREATE PROCEDURE [dbo].[OTPInsert] @UserID INT
	,@OTP VARCHAR(10)
AS
BEGIN
	INSERT INTO [dbo].[OTP] (
		[UserID]
		,[OTP]
		)
	VALUES (
		@UserID
		,@OTP
		);
END



GO
/****** Object:  StoredProcedure [dbo].[OutstandingBalanceInsertBalanceTransfer]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================    
-- Author:  Sushant Yelpale    
-- ALTER  date: 31/08/2018    
-- Description: Insert Blanace transfer Entry in Outstanding table    
-- =============================================    
CREATE PROCEDURE [dbo].[OutstandingBalanceInsertBalanceTransfer] @ReceiverID INT  
 ,@SenderID INT  
 ,@Amount DECIMAL(18, 5)  
AS  
BEGIN  
 DECLARE @RESULT INT;  
 DECLARE @DocTypeOutstanding VARCHAR(50)  
 DECLARE @TransDescOutstanding VARCHAR(200)  
 DECLARE @OpeningAmountOutstanding DECIMAL(18, 2);  
 DECLARE @ClosingAmountOutstanding DECIMAL(18, 2);  
 DECLARE @SenderName VARCHAR(100)  
  
 SELECT @SenderName = UserName  
 FROM UserInformation  
 WHERE UserId = @SenderID  
  
 SET @DocTypeOutstanding = 'Outstanding';  
  
 SELECT @OpeningAmountOutstanding = Outstanding  
 FROM dbo.UserBalance  
 WHERE UserID = @ReceiverID  
  
 SET @ClosingAmountOutstanding = @OpeningAmountOutstanding + @Amount;  
  
 UPDATE UserBalance  
 SET Outstanding = @ClosingAmountOutstanding  
 WHERE UserID = @ReceiverID  
  
 DECLARE @Description VARCHAR(100) = 'Balance Transfer'  
 Declare @OSAmount Decimal(18,2) = @Amount;
 SET @TransDescOutstanding = @Description + ' Amount: ' + CONVERT(VARCHAR, @OSAmount) + ' From ' + @SenderName;  
  
 PRINT @DocTypeOutstanding;  
  
 INSERT INTO [dbo].[OutstandingBalance] (  
  [UserID]  
  ,[OpeningBalance]  
  ,[Amount]  
  ,[ClosingBalance]  
  ,[CreditDebit]  
  ,[Description]  
  )  
 VALUES (  
  @ReceiverID  
  ,@OpeningAmountOutstanding  
  ,@Amount  
  ,@ClosingAmountOutstanding  
  ,'CREDIT'  
  ,@TransDescOutstanding  
  )  
  
 SELECT @RESULT = @@IDENTITY  
  
 PRINT @RESULT  
  
 SET @Description = 'Outstanding'  
 SET @TransDescOutstanding = @Description + ' Amount: ' + CONVERT(VARCHAR, @OSAmount) + ' From ' + @SenderName;  
  
 EXEC dbo.TransactionDetailsInsertByUser @ReceiverID  
  ,@RESULT  
  ,25  
  ,@SenderID  
  ,''  
  ,@DocTypeOutstanding  
  ,@OpeningAmountOutstanding  
  ,@ClosingAmountOutstanding  
  ,@Amount --@TransactionAmount                                             
  ,0  
  ,'CREDIT'  
  ,''  
  ,@TransDescOutstanding  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[OutstandingBalanceInsertCashCollection]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- [OutstandingBalanceInsertCashCollection] 2,10,3,'SUCCESS','T',0                
CREATE PROCEDURE [dbo].[OutstandingBalanceInsertCashCollection] --2,10,3,'SUCCESS','T',1                        
 @ReceiverID INT  
 ,@ReceivedAmount INT  
 ,@Remark VARCHAR(500)  
 ,@OUTPUT VARCHAR(500) OUT  
AS  
BEGIN  
 DECLARE @RESULT INT;  
 DECLARE @OpeningAmountOutstanding DECIMAL(18, 5);  
 DECLARE @ClosingAmountOutstanding DECIMAL(18, 5);  
 DECLARE @ReceiverName VARCHAR(50)  
 DECLARE @SenderID INT  
 DECLARE @DocTypeOutstanding VARCHAR(50)  
 DECLARE @TransDescOutstanding VARCHAR(200)  
  
 SET @DocTypeOutstanding = 'Outstanding';  
  
 SELECT @SenderID = parentID  
 FROM userinformation  
 WHERE UserID = @ReceiverID  
  
 SELECT @OpeningAmountOutstanding = Outstanding  
 FROM dbo.UserBalance  
 WHERE UserID = @ReceiverID  
  
 PRINT @OpeningAmountOutstanding  
  
 SET @ClosingAmountOutstanding = @OpeningAmountOutstanding - @ReceivedAmount;  
  
 PRINT @ClosingAmountOutstanding  
  
 UPDATE UserBalance  
 SET Outstanding = @ClosingAmountOutstanding  
 WHERE UserID = @ReceiverID  
  
 SELECT @ReceiverName = UserName  
 FROM Userinformation  
 WHERE UserID = @ReceiverID  
  
 DECLARE @Description VARCHAR(100) = 'Outstanding'  
  
 SET @TransDescOutstanding = @Description + ' Amount: ' + CONVERT(VARCHAR, @ReceivedAmount) + ' Deposited By ' + @ReceiverName;  
  
 INSERT INTO [dbo].[OutstandingBalance] (  
  [UserID]  
  ,[OpeningBalance]  
  ,[Amount]  
  ,[ClosingBalance]  
  ,[CreditDebit]  
  ,[Description]  
  )  
 VALUES (  
  @ReceiverID  
  ,@OpeningAmountOutstanding  
  ,@ReceivedAmount  
  ,@ClosingAmountOutstanding  
  ,'DEBIT'  
  ,@Remark  
  )  
  
 SELECT @RESULT = @@IDENTITY  
  
 PRINT @RESULT  
  
 SET @Output = 'Cash Collection successfully with Txn ID:' + convert(VARCHAR, @RESULT);  
  
 ------TransactionDetails insert For Receiver---                
 EXEC dbo.TransactionDetailsInsertByUser @ReceiverID  
  ,@RESULT  
  ,25  
  ,@SenderID  
  ,''  
  ,@DocTypeOutstanding  
  ,@OpeningAmountOutstanding  
  ,@ClosingAmountOutstanding  
  ,@ReceivedAmount  
  ,0  
  ,'DEBIT'  
  ,''  
  ,@TransDescOutstanding  
  
 RETURN (0);  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[OutstandingBalanceSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- Author:  Sushant Yelpale    
-- ALTER  date: 4 Sept 2018    
-- Description: Cash Collection report   if receiver is 0 - All, Else Specific receiver   
-- Also id @ShowReportFor is 'Own' show its own report only
-- [OutstandingBalanceSelect] 125, '01/01/2019', '02/02/2019', 0 , 'Own' 
-- =============================================    
CREATE PROCEDURE [dbo].[OutstandingBalanceSelect] @UserID INT  
 ,@FromDate VARCHAR(50)  
 ,@ToDate VARCHAR(50)  
 ,@ReceiverID INT  
 ,@ShowReportFor varchar(20) = 'Downline'
AS  
BEGIN  
 DECLARE @tempUsers TABLE (UserID INT);  
  
 IF(@ShowReportFor  = 'Downline' )
 BEGIN
 INSERT INTO @tempUsers (UserID)  
 SELECT UserID  
 FROM UserInformation(NOLOCK)  
 WHERE ParentID = @UserID  
 END
 ELSE
 BEGIN
	 SET @ReceiverID = 0

	 INSERT INTO @tempUsers (UserID)  
	 SELECT @UserID
 END 

 SELECT TOP (1000) OB.ID  
  ,OB.[UserID]  
  ,UI.UserName  
  ,OB.[OpeningBalance]  
  ,OB.Amount  
  ,OB.[ClosingBalance]  
  ,convert(VARCHAR(12), OB.[Date], 113) + right(convert(VARCHAR(39), OB.[Date], 22), 11) AS DOC  
  ,UI.UserTypeID AS DUserTypeID  
  ,OB.[CreditDebit]  
  ,OB.[Description]  
 FROM OutstandingBalance OB  
 INNER JOIN UserInformation UI ON OB.[UserID] = UI.UserID  
 INNER JOIN @tempUsers TU ON TU.UserID = OB.UserID  
 WHERE (  
   Convert(DATE, OB.[Date], 103) >= Convert(DATE, @FromDate, 103)  
   AND Convert(DATE, OB.[Date], 103) <= Convert(DATE, @Todate, 103)  
   )  
  AND (  
   (  
    @ReceiverID != 0  
    AND UI.UserID = @ReceiverID  
    )  
   OR @ReceiverID = 0  
   )  
 ORDER BY convert(DATETIME, OB.[Date], 103) DESC  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[OutstandingBalanceValidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		Sushant Yelpale
-- ALTER  date: 4 Sept 2018
-- Description:	Validate Cash Collection Sender
-- =============================================
CREATE PROCEDURE [dbo].[OutstandingBalanceValidation] @SenderID INT
	,@ReceiverID INT
	,@OUTPUT VARCHAR(100) OUT
AS
BEGIN
	DECLARE @Parent INT;

	SELECT @Parent = ParentID
	FROM UserInformation
	WHERE UserID = @ReceiverID

	IF (@SenderID = @Parent)
	BEGIN
		SET @OUTPUT = 'OK'

		RETURN;
	END
	ELSE
	BEGIN
		SET @OUTPUT = 'Can Not insert Outstanding Amount'
	END
END



GO
/****** Object:  StoredProcedure [dbo].[PasswordValidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :21-2-17
--Purpose Refund Validation 
--==========================================
CREATE PROCEDURE [dbo].[PasswordValidation] @UserID INT
	,@OldPassword VARCHAR(100)
AS
BEGIN
	DECLARE @Password VARCHAR(200);

	SET @Password = (
			SELECT Password
			FROM UserInformation
			WHERE UserID = @UserID
			);

	IF (@Password = @OldPassword)
	BEGIN
		SELECT Password
		FROM UserInformation
		WHERE UserID = @UserID
	END
END



GO
/****** Object:  StoredProcedure [dbo].[PaymentGatewayReplyInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================          
--Auther :Sainath Bhujbal          
--Date :31-3-17          
--Purpose :Insert Payment gateway Response   
-- Modified by Somnath : change operatorID from int to varchar     
--==========================================     
--PaymentGatewayReplyInsert 71,'9595215859',1,2,10,'Success','123456789'  
CREATE PROCEDURE[dbo].[PaymentGatewayReplyInsert] @UserID INT
	,@ConsumerNumber VARCHAR(20)
	,@ServiceID INT
	,@OperatorID VARCHAR(50)
	,@Amount INT
	,@Status VARCHAR(20)
	,@TransactionID VARCHAR(50)
AS
BEGIN
	DECLARE @Output INT
	DECLARE @PaumentGatewayID INT

	INSERT INTO dbo.PaymentGatewayReply (
		UserID
		,ConsumerNumber
		,ServiceID
		,OperatorID
		,Amount
		,STATUS
		,TransactionID
		)
	VALUES (
		@UserID
		,@ConsumerNumber
		,@ServiceID
		,@OperatorID
		,@Amount
		,@Status
		,@TransactionID
		)

	SELECT @PaumentGatewayID = @@IDENTITY

	SET @Output = @PaumentGatewayID

	RETURN @Output
END

SELECT *
FROM dbo.PaymentGatewayReply



GO
/****** Object:  StoredProcedure [dbo].[ProviderKeyValueSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Sushant Yelpale  
-- Create date: 23/102018  
-- Description: Selects All Key Values  
-- execute [ProviderKeyValueSelect]  
-- =============================================  
CREATE PROCEDURE [dbo].[ProviderKeyValueSelect]  
AS  
BEGIN  
 Select * from ProviderKeyValue  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeAmountRefund]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================                      
-- Author:  <Sainath Bhujbal>                      
-- Create date: <16 March 2017 ,>                      
-- Description: <Description,,>                      
-- =============================================           
CREATE PROCEDURE [dbo].[RechargeAmountRefund] @UserID INT  
 --,@RECHARGE_TO VARCHAR(1)          
 ,@TransType VARCHAR(10)  
 ,@ServiceID INT  
 ,@OpeningBalance NUMERIC(18, 5)  
 ,@Amount NUMERIC(18, 5) = 0  
 ,@Discount NUMERIC(18, 5) = 0  
 ,@ConsumerNo VARCHAR(20) = NULL  
 ,@OperatorID INT = NULL  
 ,@Status VARCHAR(50) = NULL  
 ,@RechargeDescription VARCHAR(100) = NULL  
 ,@RechargeProviderID INT  
 ,@Through VARCHAR(100) = NULL  
 ,@IPAddress VARCHAR(50) = NULL  
 ,@RechargeId INT = 0  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 DECLARE @RESULT INT  
 DECLARE @comAmt NUMERIC(18, 5) = 0;  
 DECLARE @success VARCHAR(20) = 'SUCCESS';  
 DECLARE @failure VARCHAR(20) = 'FAILURE';  
 DECLARE @refund VARCHAR(20) = 'REFUND';  
 DECLARE @PROCESS VARCHAR(20) = 'PROCESS';  
 DECLARE @Output INT  
 DECLARE @OriginalStatus VARCHAR(50)  
 DECLARE @ElectricityServiceID int = 14;
  
 SET @Output = 0  
  
 SELECT @OriginalStatus = STATUS  
 FROM [dbo].Recharge(NOLOCK)  
 WHERE RechargeID = @RechargeId  
  
 IF (upper(@OriginalStatus) != Upper(@PROCESS))  
 BEGIN  
  PRINT 'Recharge Status is not in PROCESS'  
  
  RETURN @Output  
 END  
  
 IF (@Status = (@failure))  
 BEGIN  
  SET @TransType = 'REFUNDED'  
  SET @Status = @refund  
  
  --STEP1.1: SELECT USER BALANCE BEFORE TRANSCTION...        
  DECLARE @beforeTransAmt NUMERIC(18, 5);  
  
  IF (@ServiceID = @ElectricityServiceID)
  BEGIN
	  SELECT @beforeTransAmt = DMRBalance  
	  FROM DBO.UserBalance(NOLOCK)  
	  WHERE UserID = @UserID  
  END
  ELSE
  BEGIN
	  SELECT @beforeTransAmt = CurrentBalance  
	  FROM DBO.UserBalance(NOLOCK)  
	  WHERE UserID = @UserID  
  END

  UPDATE dbo.Recharge  
  SET RechargeProviderID = @RechargeProviderID  
   ,RechargeDescription = @RechargeDescription  
   ,STATUS = @Status  
   ,TransactionType = @TransType  
   ,OpeningBalance = @beforeTransAmt  
   ,UpdateDoc = (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))  
  WHERE RechargeID = @RechargeId  
  
  SELECT @RESULT = @@ROWCOUNT  
  
  SET @Output = @RESULT  
  
  DECLARE @TransDesc VARCHAR(100)  
  
  IF (@Output > 0)  
  BEGIN  
   EXEC dbo.[TransactionDetailsRefundInsert] @UserID  
    ,@RechargeID  
    ,@ServiceID  
    ,@ConsumerNo  
    ,@Amount  
    ,@OperatorID  
    ,@RechargeProviderID  
  END  
 END  
  
 RETURN @Output  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[RechargeDetailsSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================                  
--Auther :Priyanka                  
--Date :16-8-2017               
--Purpose Recharge report                  
--==========================================                  
--RechargeDetailsSelect 1632      
CREATE PROCEDURE [dbo].[RechargeDetailsSelect] @RechargeID INT
AS
BEGIN
	SELECT *
		,(
			SELECT [RechargePin]
			FROM dbo.[UserInformation] AS UI
			WHERE UI.[UserID] = R.[UserID]
			) AS RechargePIN
	FROM dbo.Recharge(NOLOCK) AS R
	WHERE R.[RechargeID] = @RechargeID
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeFailToSuccessInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                          
-- Author:  <Priyanka Deshmukh>                          
-- Create date: <21/9/2017>                          
-- Description: Update fail recharge            
-- Modifi      
--ed BY :Sainath 7 DEC 2017      
-- =============================================                      
CREATE PROCEDURE [dbo].[RechargeFailToSuccessInsert] @UserID INT
	,@ServiceID INT
	,@Amount NUMERIC(18, 5) = 0
	,@ConsumerNo VARCHAR(20)
	,@OperatorID INT
	,@Status VARCHAR(50) = NULL
	,@RechargeProviderID INT
	,@RechargeID INT
	,@TransId VARCHAR(30) = NULL
AS
BEGIN
	DECLARE @Result INT = 0
	DECLARE @success VARCHAR(20) = 'SUCCESS';
	DECLARE @REFUND VARCHAR(20) = 'REFUND'
	DECLARE @OriginalStatus VARCHAR(50) = '--'

	IF (upper(@Status) != upper(@success))
	BEGIN
		RETURN 0;
	END

	SELECT @OriginalStatus = STATUS
	FROM [dbo].Recharge(NOLOCK)
	WHERE RechargeID = @RechargeID

	IF (upper(@OriginalStatus) = upper(@REFUND))
	BEGIN
		UPDATE dbo.Recharge
		SET STATUS = @Status
			,TransactionID = @TransId
			,RechargeDescription = 'Recharge fail to success-manual (DB)'
		WHERE RechargeID = @RechargeID
			AND UserID = @UserID

		SELECT @Result = @@ROWCOUNT
	END

	IF (@Result >= 1)
	BEGIN
		EXEC dbo.TransactionDetailsInsert @UserID
			,@RechargeID ----@RechargeID INT              
			,@ServiceID
			,@ConsumerNo
			,@Amount ----@TransactionAmount              
			,@OperatorID
			,@RechargeProviderID
	END

	RETURN @RechargeID
END

GO
/****** Object:  StoredProcedure [dbo].[RechargeGroupCommissionDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================    
--Auther :  Rahul Hembade  
--Date :  16 May 2018  
--Purpose : delete group commision before update  
--==========================================    
CREATE PROCEDURE [dbo].[RechargeGroupCommissionDelete] @GroupId INT
AS
BEGIN
	DELETE
	FROM RechargeGroupCommission
	WHERE GroupID = @GroupId
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeGroupCommissionDeleteOperatorComm]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================      
--Auther :Rahul Hembade   
--Date :16 May 2018    
--Purpose RechargeGroupCommission Delete  Operator-wise   and group-wise  
--==========================================      
CREATE PROCEDURE [dbo].[RechargeGroupCommissionDeleteOperatorComm] @GroupId INT
	,@OperatorId INT
AS
BEGIN
	DELETE
	FROM RechargeGroupCommission
	WHERE GroupID = @GroupId
		AND OperatorID = @OperatorId
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeGroupCommissionSelectByService]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================                        
--Auther :Rahul Hembade                       
--Date :  16 May 2018                 
--Purpose : Select Group wise operator commision /Check if present                
--          in RechargeGroupCommission else from OperatorMaster              
--==========================================             
--[RechargeGroupCommissionSelectByService] 1,0            
--[RechargeGroupCommissionSelectByService] 9999,0            
CREATE PROCEDURE[dbo].[RechargeGroupCommissionSelectByService] @GroupID INT
	,@ServiceID INT
AS
BEGIN
	DECLARE @DMRServiceID INT = 21

	IF (@ServiceID = 0)
	BEGIN
		SELECT *
		FROM (
			SELECT *
				,ROW_NUMBER() OVER (
					PARTITION BY OperatorID ORDER BY OperatorID ASC
					) AS SNN
			FROM (
				SELECT OM.OperatorID
					,OM.OperatorName
					,RGC.OperatorCode
					,RGC.RetailerCommissionAmount
					,RGC.DistributorCommissionAmount
					,RGC.SuperDistributorCommissionAmount
					,RGC.IsOn
					,isnull(RGC.TotalCommission, 0) AS TotalCommission
					,--(IsNULL(RGC.DistributorCommissionAmount, 0) + IsNULL(RGC.RetailerCommissionAmount, 0)) as TotalCommission,                 
					RGC.MarginType
					,SM.ServiceName
					,SM.ServiceID
				FROM dbo.RechargeGroupCommission RGC
				INNER JOIN dbo.OperatorMaster OM ON RGC.OperatorID = OM.OperatorID
				INNER JOIN dbo.ServiceMaster SM ON SM.ServiceID = OM.ServiceID
				WHERE RGC.GroupID = @GroupID
					AND OM.[IsDelete] = 0
				
				UNION
				
				SELECT OM.OperatorID
					,OM.OperatorName
					,OM.OperatorCode
					,OM.RetailerCommissionAmount
					,OM.DistributorCommissionAmount
					,OM.SuperDistributorCommissionAmount
					,OM.IsOn
					,isnull(OM.TotalCommission, 0) AS TotalCommission
					,--(IsNULL(OM.DistributorCommissionAmount, 0) + IsNULL(OM.RetailerCommissionAmount, 0)) as TotalCommission  ,              
					OM.MarginType
					,SM.ServiceName
					,SM.ServiceID
				FROM dbo.OperatorMaster OM
				INNER JOIN dbo.ServiceMaster SM ON SM.ServiceID = OM.ServiceID
				WHERE OM.OperatorID NOT IN (
						SELECT RGC.OperatorID
						FROM RechargeGroupCommission RGC
						WHERE RGC.GroupID = @GroupID
						)
					AND OM.[IsDelete] = 0
				) AS tabMain
			) AS outerTab
		WHERE SNN = 1
			AND ServiceID != @DMRServiceID
		ORDER BY ServiceID
			,OperatorName
	END
	ELSE
	BEGIN
		SELECT *
		FROM (
			SELECT *
				,ROW_NUMBER() OVER (
					PARTITION BY OperatorID ORDER BY OperatorID ASC
					) AS SNN
			FROM (
				SELECT OM.OperatorID
					,OM.OperatorName
					,RGC.OperatorCode
					,RGC.RetailerCommissionAmount
					,RGC.DistributorCommissionAmount
					,RGC.SuperDistributorCommissionAmount
					,RGC.IsOn
					,isnull(RGC.TotalCommission, 0) AS TotalCommission
					,--(IsNULL(UC.DistributorCommissionAmount, 0) + IsNULL(UC.RetailerCommissionAmount, 0)) as TotalCommission                
					RGC.MarginType
					,SM.ServiceName
					,SM.ServiceID
				FROM RechargeGroupCommission RGC
				INNER JOIN OperatorMaster OM ON RGC.OperatorID = OM.OperatorID
				INNER JOIN ServiceMaster SM ON SM.ServiceID = OM.ServiceID
				WHERE RGC.GroupID = @GroupID
					AND OM.ServiceID = @ServiceID
					AND OM.[IsDelete] = 0
				
				UNION
				
				SELECT OM.OperatorID
					,OM.OperatorName
					,OM.OperatorCode
					,OM.RetailerCommissionAmount
					,OM.DistributorCommissionAmount
					,OM.SuperDistributorCommissionAmount
					,OM.IsOn
					,isnull(OM.TotalCommission, 0) AS TotalCommission
					,--(IsNULL(OM.DistributorCommissionAmount, 0) + IsNULL(OM.RetailerCommissionAmount, 0)) as TotalCommission,                     
					OM.MarginType
					,SM.ServiceName
					,SM.ServiceID
				FROM dbo.OperatorMaster OM
				INNER JOIN dbo.ServiceMaster SM ON SM.ServiceID = OM.ServiceID
				WHERE OM.ServiceID = @ServiceID
					AND OM.OperatorID NOT IN (
						SELECT RGC.OperatorID
						FROM RechargeGroupCommission RGC
						WHERE RGC.GroupID = @GroupID
						)
					AND OM.[IsDelete] = 0
				) AS tabMain
			) AS outerTab
		WHERE SNN = 1
			AND ServiceID != @DMRServiceID
		ORDER BY ServiceID
			,OperatorName
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeGroupCommissionUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================        
--Auther : Rahul Hembade    
--Date : 16 May 2018    
--Purpose Recharge Group  Commission Update        
--==========================================        
CREATE PROCEDURE[dbo].[RechargeGroupCommissionUpdate] @RetailerComm DECIMAL(4, 3)
	,@TotalComm DECIMAL(4, 3)
	,@OperatorId INT
	,@Type VARCHAR(20)
	,@OperatorCode VARCHAR(50)
	,@GroupID INT
	,@IsActive INT
	,@DistComm DECIMAL(4, 3)
	,@SuperDistComm DECIMAL(4, 3)
AS
BEGIN
	IF NOT EXISTS (
			SELECT GroupID
				,OperatorID
			FROM dbo.RechargeGroupCommission
			WHERE GroupID = @GroupID
				AND OperatorID = @OperatorId
			)
	BEGIN
		INSERT INTO dbo.RechargeGroupCommission (
			GroupID
			,OperatorID
			,OperatorCode
			,RetailerCommissionAmount
			,DistributorCommissionAmount
			,TotalCommission
			,MarginType
			,IsOn
			,SuperDistributorCommissionAmount
			)
		VALUES (
			@GroupID
			,@OperatorId
			,@OperatorCode
			,@RetailerComm
			,@DistComm
			,@TotalComm
			,@Type
			,@IsActive
			,@SuperDistComm
			)

		PRINT 'Comm. added'

		RETURN
	END
	ELSE
	BEGIN
		UPDATE dbo.RechargeGroupCommission
		SET RetailerCommissionAmount = @RetailerComm
			,DistributorCommissionAmount = @DistComm
			,TotalCommission = @TotalComm
			,MarginType = @Type
			,IsOn = @IsActive
			,SuperDistributorCommissionAmount = @SuperDistComm
		WHERE GroupID = @GroupID
			AND OperatorID = @OperatorId

		PRINT 'Comm. Updated'

		RETURN
	END

	PRINT 'Not working'
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeGroupMasterDeleteGroup]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--ALTER d: Rahul Hembade
--Date:16/05/2018
CREATE PROCEDURE[dbo].[RechargeGroupMasterDeleteGroup] @ID INT
AS
BEGIN
	DELETE
	FROM RechargeGroupCommission
	WHERE GroupID = @ID

	DELETE
	FROM UserGroupMapping
	WHERE GroupID = @ID

	DELETE
	FROM [dbo].[RechargeGroupMaster]
	WHERE GroupID = @ID
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeGroupMasterInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--ALTER d: Rahul Hembade
--Date:16/05/2018
CREATE PROCEDURE[dbo].[RechargeGroupMasterInsert] @GroupName VARCHAR(50)
AS
BEGIN
	IF EXISTS (
			SELECT *
			FROM [dbo].[RechargeGroupMaster]
			WHERE [GroupName] = @GroupName
			)
	BEGIN
		RETURN;
	END

	INSERT INTO [dbo].[RechargeGroupMaster] ([GroupName])
	VALUES (@GroupName)
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeGroupMasterSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================                                    
-- Author:  <Rahul Hembade>                                    
-- ALTER  date: <>                                    
-- =============================================               
CREATE PROCEDURE[dbo].[RechargeGroupMasterSelect]
AS
BEGIN
	SELECT GroupID
		,[GroupName]
	FROM [dbo].[RechargeGroupMaster]
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeGroupMasterUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--ALTER d: Rahul Hembade
--Date:16/05/2018
CREATE PROCEDURE[dbo].[RechargeGroupMasterUpdate] @GroupName VARCHAR(50)
	,@ID INT
AS
BEGIN
	IF NOT EXISTS (
			SELECT GroupID
			FROM [RechargeGroupMaster]
			WHERE [GroupName] = @GroupName
			)
	BEGIN
		UPDATE [dbo].[RechargeGroupMaster]
		SET [GroupName] = @GroupName
		WHERE GroupID = @ID
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================                    
-- Author:  <Sainath Bhujbal>                    
-- ALTER  date: <25 Feb 2017 ,>                    
-- Description: <Description,,>                    
-- Modify Mahesh : For APIRequestID      
-- =============================================                 
CREATE PROCEDURE [dbo].[RechargeInsert] @UserID INT  
 ,@ServiceID INT  
 ,@TransType VARCHAR(10)  
 ,@OpeningBalance NUMERIC(18, 5)  
 ,@Amount NUMERIC(18, 5) = 0  
 ,@ConsumerNo VARCHAR(20)  
 ,@OperatorID INT  
 ,@Status VARCHAR(50) = NULL  
 ,@Through VARCHAR(100)  
 ,@IPAddress VARCHAR(50) = NULL  
 ,@RechargeProviderID INT  
 ,@RechargeDescription VARCHAR(100) = NULL  
 ,@InboxID INT  
 ,@RequestID VARCHAR(1000) = NULL  
 ,@APIRequestID VARCHAR(1000) = NULL  
 ,@Optional1 Varchar(100) = '' 
 ,@Optional2 Varchar(100) = ''
 ,@Optional3 Varchar(100) = ''
 ,@Optional4 Varchar(100) = ''
 ,@Optional5 Varchar(100) = ''
 ,@Optional6 Varchar(100) = ''
 ,@CustomerMobileNo Varchar(50) = ''
AS  
BEGIN  
 DECLARE @beforeTransAmt NUMERIC(18, 5);  
 DECLARE @Output INT;  
  
 SELECT @beforeTransAmt = CurrentBalance  
 FROM dbo.UserBalance  
 WHERE UserID = @UserID  
  
 INSERT INTO dbo.Recharge (  
  UserID  
  ,ServiceID  
  ,TransactionType  
  ,ConsumerNumber  
  ,Amount  
  ,OpeningBalance  
  ,STATUS  
  ,OperatorID  
  ,Through  
  ,IPAddress  
  ,RechargeProviderID  
  ,RechargeDescription  
  ,InboxID  
  ,TransactionID  
  ,RequestID  
  ,APIRequestID  
  ,Optional1
  ,Optional2
  ,Optional3
  ,Optional4
  ,Optional5
  ,Optional6
  ,CustomerMobileNo
  )  
 VALUES (  
  @UserID  
  ,@ServiceID  
  ,@TransType  
  ,@ConsumerNo  
  ,@Amount  
  ,@beforeTransAmt  
  ,@Status  
  ,@OperatorID  
  ,@Through  
  ,@IPAddress  
  ,@RechargeProviderID  
  ,@RechargeDescription  
  ,@InboxID  
  ,0  
  ,@RequestID  
  ,@APIRequestID  
  ,@Optional1
  ,@Optional2
  ,@Optional3
  ,@Optional4
  ,@Optional5
  ,@Optional6
  ,@CustomerMobileNo
  )  
  
 SET @Output = @@IDENTITY  
  
 PRINT '@Output: ' + convert(VARCHAR(30), @Output) + '@UserID' + convert(VARCHAR(30), @UserID)  
 PRINT '@ServiceID:' + convert(VARCHAR(30), @ServiceID) + ': @ConsumerNo: ' + convert(VARCHAR(30), @ConsumerNo) + ': @Amount:' + convert(VARCHAR(30), @Amount)  
 PRINT '@OperatorID' + convert(VARCHAR(30), @OperatorID) + ': @RechargeProviderID:' + convert(VARCHAR(30), @RechargeProviderID)  
  
 IF (@Output >= 1)  
 BEGIN  
  EXEC dbo.TransactionDetailsInsert @UserID  
   ,@Output ----@RechargeID INT        
   ,@ServiceID  
   ,@ConsumerNo  
   ,@Amount ----@TransactionAmount         
   ,@OperatorID  
   ,@RechargeProviderID  
 END  
  
 RETURN @Output  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[RechargeMannualSuccessfailselectDatewise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================                        
-- Author:  Priyanka Deshmukh                   
-- ALTER  date: <20 March 2017 ,>                        
-- Description: <select Recharge for Mannual  Recharge success,fail,,>                        
-- =============================================       
-- RechargeMannualSuccessfailselectDatewise               
CREATE PROCEDURE [dbo].[RechargeMannualSuccessfailselectDatewise] @FromDate VARCHAR(250)  
 ,@ToDate VARCHAR(250)  
 ,@Status VARCHAR(250)  
AS  
BEGIN  
  SELECT TOP (500) R.UserID  
   ,RPM.RechargeProviderName  
   ,R.RechargeID  
   ,R.ServiceID  
   ,R.ConsumerNumber  
   ,R.Amount  
   ,R.OpeningBalance  
   ,R.STATUS  
   ,UI.UserName AS Uname  
   ,R.IsDispute  
   ,OM.OperatorName  
   ,SM.ServiceName  
   ,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC  
   ,R.OperatorID  
   ,UI.ParentID  
   ,TD.DiscountAmount  
   ,UI.UserTypeID  
   ,UI.EmailID  
   ,UI.MobileNumber  
  FROM ServiceMaster SM  
  INNER JOIN Recharge R ON SM.ServiceID = R.ServiceID  
  INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID  
  INNER JOIN UserInformation UI ON R.UserID = UI.UserID  
  INNER JOIN RechargeProviderMaster RPM ON RPM.RechargeProviderID = R.RechargeProviderID  
  INNER JOIN TransactionDetails TD ON R.RechargeID = TD.RechargeID  
   AND R.UserID = TD.UserID  
   AND R.UserID IN (  
    SELECT UserID  
    FROM Recharge  
    )  
  WHERE R.STATUS NOT IN ('REFUND')  
   AND (  
    convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)  
    AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)  
    )  
   AND R.STATUS = @Status  
 ORDER BY convert(DATETIME, R.DOC, 103) DESC;  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderConfigurationUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[RechargeProviderConfigurationUpdate] @OperatorID INT
	,@ProviderID INT
	,@Sequence INT
AS
BEGIN
	IF EXISTS (
			SELECT *
			FROM [RechargeProviderConfiguration]
			WHERE [OperatorID] = @OperatorID
				AND [Sequence] = @Sequence
			)
	BEGIN
		UPDATE [dbo].[RechargeProviderConfiguration]
		SET [ProviderID] = @OperatorID
		WHERE [OperatorID] = @OperatorID
			AND [Sequence] = @Sequence
	END
	ELSE
	BEGIN
		INSERT INTO [RechargeProviderConfiguration] (
			[OperatorID]
			,[ProviderID]
			,[Sequence]
			)
		VALUES (
			@OperatorID
			,@ProviderID
			,@Sequence
			);
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderConfigurationUserwiseDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Author: Somnath A. Kadam (11 Jan 2019)
--- To reset all user's sequence to default sequence of give operator
CREATE PROCEDURE [dbo].[RechargeProviderConfigurationUserwiseDelete] @OperatorID INT  
AS  
BEGIN  
 DELETE  
 FROM [RechargeProviderConfigurationUserwise]  
 WHERE OperatorID = @OperatorID  
END  
 
GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderConfigurationUserwiseDeleteUserIDWise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- Author: Somnath A. Kadam (11 Jan 2019)
--- To set default seqence to all user
CREATE PROCEDURE [dbo].[RechargeProviderConfigurationUserwiseDeleteUserIDWise] 
@UserID INT  
AS  
BEGIN  
 DELETE  
 FROM RechargeProviderConfigurationUserwise  
 WHERE UserID = @UserID  
END 
GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderDateWiseTotalSales]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================                                                                
-- AUTHOR:Priyanka Deshmukh                                                                 
-- ALTER  DATE: 22 MARCH 2017                                                                  
-- DESCRIPTION: Recharge Provider DateWise Sales                                                      
-- ========================================================================   
--RechargeProviderDateWiseTotalSales 1,'1/3/2017','22/4/2017','Date'
CREATE PROCEDURE [dbo].[RechargeProviderDateWiseTotalSales] @ProviderID INT
	,@FromDate VARCHAR(100)
	,@ToDate VARCHAR(100)
	,@Type VARCHAR(100)
AS
BEGIN
	DECLARE @TypeStatus VARCHAR(20)

	SET @TypeStatus = @Type;

	IF (@TypeStatus = 'Total')
	BEGIN
		SELECT DISTINCT R.RechargeProviderID
			,RPM.RechargeProviderName
			,Sum(R.Amount) OVER (PARTITION BY R.RechargeProviderID) AS Amount
			,Sum(IsNull(R.RechargeProviderOpeningBalance, 0.00)) OVER (PARTITION BY R.RechargeProviderID) AS RechargeProviderOpeningBalance
			,Sum(IsNull(R.RechargeProviderCommission, 0.00)) OVER (PARTITION BY R.RechargeProviderID) AS RechargeProviderCommission
			,Sum(IsNull(R.RechargeProviderClosingBalance, 0.00)) OVER (PARTITION BY R.RechargeProviderID) AS RechargeProviderClosingBalance
		FROM Recharge R
		INNER JOIN RechargeProviderMaster RPM ON RPM.RechargeProviderID = R.RechargeProviderID
		WHERE (
				convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
			AND R.RechargeProviderID = @ProviderID
	END

	IF (@TypeStatus = 'Date')
	BEGIN
		SELECT R.RechargeProviderID
			,RPM.RechargeProviderName
			,R.Amount
			,IsNull(R.RechargeProviderOpeningBalance, 0.00) AS RechargeProviderOpeningBalance
			,IsNull(R.RechargeProviderCommission, 0.00) RechargeProviderCommission
			,IsNull(R.RechargeProviderClosingBalance, 0.00) RechargeProviderClosingBalance
		FROM Recharge R
		INNER JOIN RechargeProviderMaster RPM ON RPM.RechargeProviderID = R.RechargeProviderID
		WHERE (
				convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
			AND R.RechargeProviderID = @ProviderID
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderHistoryInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RechargeProviderHistoryInsert] @RechargeId VARCHAR(1000)
	,@RequestId VARCHAR(1000) = NULL
	,@RechargeProviderId INT
AS
BEGIN
	INSERT INTO RechargeProviderHistory (
		RechargeId
		,RequestId
		,RechargeProviderId
		)
	VALUES (
		@RechargeId
		,@RequestId
		,@RechargeProviderId
		)
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderHistorySelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RechargeProviderHistorySelect] @RechargeId INT
	,@RechargeProviderId INT
AS
BEGIN
	SELECT RechargeId
		,RechargeProviderId
	FROM RechargeProviderHistory
	WHERE RechargeId = @RechargeId
		AND RechargeProviderId = @RechargeProviderId
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderMasterDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :9-7-17
--Purpose RechargeProviderMaster Delete
--==========================================
CREATE PROCEDURE [dbo].[RechargeProviderMasterDelete] @RechargeProviderID INT
	,@IsDelete INT
AS
BEGIN
	UPDATE RechargeProviderMaster
	SET IsDelete = @IsDelete
	WHERE RechargeProviderID = @RechargeProviderID
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderMasterInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================    
--Auther :Priyanka    
--Date :9-7-17    
--Purpose RechargeProviderMaster Insert    
--==========================================    
CREATE PROCEDURE [dbo].[RechargeProviderMasterInsert] @RechargeProviderName VARCHAR(50)
	,@HostName VARCHAR(500)
	,@URLParameter VARCHAR(500)
	,@RechargeProviderUrL VARCHAR(500)
	,@BalanceUrL VARCHAR(500)
	,@StatusCheckUrl VARCHAR(500)
	,@ProviderUniqueID VARCHAR(max) = NULL
	,@IsResend INT = 0
	,@ResendMessage VARCHAR(1000) = NULL
AS
BEGIN
	DECLARE @output INT

	INSERT INTO RechargeProviderMaster (
		RechargeProviderName
		,HostName
		,URLParameter
		,RechargeProviderUrL
		,BalanceUrL
		,StatusCheckUrl
		,[ProviderUniqueID]
		,IsResend
		,ResendMessage
		)
	VALUES (
		@RechargeProviderName
		,@HostName
		,@URLParameter
		,@RechargeProviderUrL
		,@BalanceUrL
		,@StatusCheckUrl
		,@ProviderUniqueID
		,@IsResend
		,@ResendMessage
		);

	SET @output = @@IDENTITY

	RETURN @output
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderMasterSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================  
--Auther :Priyanka  
--Date :9-7-17  
--Purpose RechargeProviderMaster Select  
--==========================================  
CREATE PROCEDURE [dbo].[RechargeProviderMasterSelect]
AS
BEGIN
	SELECT IsNull(RPM.[CurrentBalance], 0) AS CurrentBalance
		,RPM.RechargeProviderID
		,RPM.RechargeProviderName
		,RPM.IsDelete
		,IsNull(RPM.ProviderUniqueID, '') AS ProviderUniqueID
		,RPM.IsResend
		,IsNull(RPM.ResendMessage, '') AS ResendMessage
	FROM RechargeProviderMaster RPM
	ORDER BY convert(DATETIME, DOC, 103) DESC
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderMasterSelectByID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================  
--Auther :Priyanka  
--Date :9-7-17  
--Purpose RechargeProviderMaster Select  
--==========================================  
--[RechargeProviderMasterSelectByID] 14  
CREATE PROCEDURE [dbo].[RechargeProviderMasterSelectByID] @RechargeProviderID INT
AS
BEGIN
	SELECT [RechargeProviderID]
		,[RechargeProviderName]
		,[HostName]
		,[UrlParameter]
		,[RechargeProviderUrl]
		,[BalanceUrl]
		,[CurrentBalance]
		,[BalanceCheckTimer]
		,[FailureTimer]
		,[lastBalanceCheck]
		,[StatusCheckUrl]
		,[IsDelete]
		,[IsResend]
		,IsNull(ResendMessage, '') AS ResendMessage
	FROM dbo.RechargeProviderMaster(NOLOCK)
	WHERE RechargeProviderID = @RechargeProviderID;

	SELECT *
	FROM (
		SELECT sm.[ServiceID]
			,sm.ServiceName
			,isnull([IsActive], 0) AS Active
			,sm.isdeleted
		FROM dbo.ServiceMaster(NOLOCK) AS SM
		LEFT JOIN [dbo].[RechargeProviderServices](NOLOCK) AS RPS ON SM.[ServiceID] = RPS.[ServiceID]
			AND [RechargeProviderID] = @RechargeProviderID
		) AS Tab
	WHERE tab.isdeleted = 0
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderMasterSelectOperatorWithRechargeProvider]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RechargeProviderMasterSelectOperatorWithRechargeProvider] @ServiceID INT
AS
BEGIN
	SELECT dbo.RechargeProviderMaster.RechargeProviderName
		,dbo.RechargeProviderServices.ServiceID
		,dbo.OperatorMaster.OperatorName
	FROM dbo.RechargeProviderServices
	INNER JOIN dbo.OperatorMaster ON dbo.RechargeProviderServices.ServiceID = @ServiceID
	CROSS JOIN dbo.RechargeProviderMaster
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderMasterSelectProviderID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[RechargeProviderMasterSelectProviderID] @APIID VARCHAR(max)
AS
BEGIN
	SELECT [RechargeProviderID]
	FROM [dbo].[RechargeProviderMaster]
	WHERE [ProviderUniqueID] = @APIID
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderMasterUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================  
--Auther :Priyanka  
--Date :29-3-17  
--Purpose Recharge Provider Master Update  
--==========================================  
CREATE PROCEDURE [dbo].[RechargeProviderMasterUpdate] @RechargeProviderID INT
	,@RechargeProviderName VARCHAR(500)
	,@HostName VARCHAR(500)
	,@URLParameter VARCHAR(500)
	,@RechargeProviderUrL VARCHAR(500)
	,@BalanceUrL VARCHAR(500)
	,@StatusCheckUrl VARCHAR(500)
	,@IsResend INT = 0
	,@ResendMessage VARCHAR(1000) = NULL
AS
BEGIN
	DECLARE @output INT

	UPDATE RechargeProviderMaster
	SET RechargeProviderName = @RechargeProviderName
		,HostName = @HostName
		,URLParameter = @URLParameter
		,RechargeProviderUrL = @RechargeProviderUrL
		,BalanceUrL = @BalanceUrL
		,StatusCheckUrl = @StatusCheckUrl
		,ResendMessage = @ResendMessage
		,IsResend = @IsResend
	WHERE RechargeProviderID = @RechargeProviderID

	SET @output = @RechargeProviderID

	RETURN @output
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderMasterUpdateBalance]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[RechargeProviderMasterUpdateBalance]  
@APIBalance decimal(18,5),  
@ProviderID int  
as  
begin  
 Update RechargeProviderMaster  
 set CurrentBalance = @APIBalance  
 where RechargeProviderID = @ProviderID  
end
GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderMasterUpdateDetail]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[RechargeProviderMasterUpdateDetail] @OperatorID INT
	,@Opcode VARCHAR(10)
	,@MarginType VARCHAR(10)
	,@ProviderMargin NUMERIC(18, 5)
	,@IsActive INT
	,@RechargeProviderID INT
AS
BEGIN
	IF EXISTS (
			SELECT *
			FROM [RechargeProviderOperatorMaster]
			WHERE [RechargeProviderID] = @RechargeProviderID
				AND [OperatorID] = @OperatorID
			)
	BEGIN
		UPDATE [dbo].[RechargeProviderOperatorMaster]
		SET [Opcode] = @Opcode
			,[CommissionAdmin] = @ProviderMargin
			,[MarginType] = @MarginType
			,[IsActive] = @IsActive
		WHERE [RechargeProviderID] = @RechargeProviderID
			AND [OperatorID] = @OperatorID
	END
	ELSE
	BEGIN
		INSERT INTO [RechargeProviderOperatorMaster] (
			[RechargeProviderID]
			,[OperatorID]
			,[Opcode]
			,[CommissionAdmin]
			,[MarginType]
			,[IsActive]
			)
		VALUES (
			@RechargeProviderID
			,@OperatorID
			,@Opcode
			,@ProviderMargin
			,@MarginType
			,@IsActive
			);
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderOperatorMasterDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :7-3-17
--Purpose Recharge Provider Operator Master Delete
--==========================================
CREATE PROCEDURE [dbo].[RechargeProviderOperatorMasterDelete] @RechargeProviderID INT
AS
BEGIN
	DELETE
	FROM RechargeProviderOperatorMaster
	WHERE RechargeProviderID = @RechargeProviderID
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderOperatorMasterSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================        
--Auther :Priyanka        
--Date :7-3-17        
--Purpose Recharge Provider Operator Master Select        
--@Modified by Somnath : Need to select distict data         
--@ Ajit - did order by service id and operator name and get only active operator        
--==========================================        
--RechargeProviderOperatorMasterSelect 7        
CREATE PROCEDURE [dbo].[RechargeProviderOperatorMasterSelect] @RechargeProviderID INT  
AS  
BEGIN  
 SELECT DISTINCT RPS.RechargeProviderID  
  ,RPS.ServiceID  
  ,OM.OperatorID  
  ,OM.OperatorName  
  ,IsNull(RPOM.[PendingCount], 0) AS PendingCount  
  ,IsNull(RPOM.[FailureCount], 0) AS FailureCount  
  ,IsNull(RPOM.[CheckTimeInMin], 0) AS CheckTimeInMin  
  ,IsNull(RPOM.Opcode, OM.OperatorCode) AS OperatorCode  
  ,IsNull(RPOM.MarginType, OM.MarginType) AS MarginType  
  ,IsNull(RPOM.CommissionAdmin, 0.0) AS CommissionAdmin  
  ,SM.ServiceName  
  ,IsNull(RPOM.AllowedQty, 0) AS AllowedQty  
  ,IsNull(RPOM.[Param1], '') AS [Param1]  
  ,IsNull(RPOM.[Param2], '') AS [Param2]  
  ,IsNull(RPOM.HLR, 0) AS HLR  
  ,IsNull((OM.RetailerCommissionAmount + OM.DistributorCommissionAmount + OM.SuperDistributorCommissionAmount), 0) AS TotalCommission  
  ,IsNull(RPOM.IsActive, (  
    SELECT IsActive  
    FROM RechargeProviderServices  
    WHERE RechargeProviderID = RPS.RechargeProviderID  
     AND ServiceID = RPS.ServiceID  
    )) AS IsActive  
 FROM dbo.RechargeProviderServices(NOLOCK) RPS  
 INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON RPS.ServiceID = OM.ServiceID  
  AND OM.IsDelete = 0  
 LEFT JOIN dbo.ServiceMaster(NOLOCK) SM ON RPS.ServiceID = SM.ServiceID  
 LEFT OUTER JOIN dbo.RechargeProviderOperatorMaster(NOLOCK) RPOM ON OM.OperatorID = RPOM.OperatorID  
  AND RPS.RechargeProviderID = RPOM.RechargeProviderID  
 WHERE RPS.RechargeProviderID = @RechargeProviderID  
 ORDER BY RPS.ServiceID  
  ,OM.OperatorName  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderOperatorMasterSelectOpcode]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RechargeProviderOperatorMasterSelectOpcode] @RechargeProviderID INT
	,@OperatorID INT
AS
BEGIN
	SELECT Opcode
		,isnull(Param1, '') AS Param1
	FROM dbo.RechargeProviderOperatorMaster(NOLOCK)
	WHERE [RechargeProviderID] = @RechargeProviderID
		AND [OperatorID] = @OperatorID
		AND IsActive = 1
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderOperatorMasterSelectOpcode_v1]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--======================
--ALTER d by :Rahul Hembade
--Date: 11Jun2018
--Reson: Userwiss Operator change
--=======================================
--[RechargeProviderOperatorMasterSelectOpcode_v1] 2,6,1090
CREATE PROCEDURE [dbo].[RechargeProviderOperatorMasterSelectOpcode_v1] @RechargeProviderID INT
	,@OperatorID INT
	,@UserId INT
AS
BEGIN
	DECLARE @OperatorCode VARCHAR(10)
		,@Opcode VARCHAR(10)
	DECLARE @GroupID INT

	SET @GroupID = '0'
	SET @Opcode = '0'
	SET @OperatorCode = '0'

	SELECT @GroupID = isnull(GroupID, '0')
	FROM [dbo].[UserGroupMapping]
	WHERE UserId = @UserId

	SELECT @OperatorCode = isnull(OperatorCode, '0')
	FROM dbo.RechargeGroupCommission(NOLOCK)
	WHERE OperatorID = @OperatorID
		AND GroupID = @GroupID

	SET @Opcode = (
			SELECT TOP (1) isnull(Opcode, '0')
			FROM dbo.RechargeProviderOperatorMaster(NOLOCK)
			WHERE [RechargeProviderID] = @RechargeProviderID
				AND [OperatorID] = @OperatorID
				AND IsActive = 1
			)

	--order by RechargeProviderID
	PRINT @OperatorCode
	PRINT @Opcode

	IF (@GroupID != '0')
	BEGIN
		IF (@Opcode = @OperatorCode)
		BEGIN
			SELECT @Opcode AS Opcode
		END
		ELSE
		BEGIN
			SELECT @OperatorCode AS Opcode
		END
	END
	ELSE
		SELECT @Opcode AS Opcode
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderOperatorMasterSelectPendingFailureCount]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:  <Sujata Nalavade>  
-- ALTER  date: <30/08/2018>  
-- Description: <Hunting Root>  
CREATE PROCEDURE[dbo].[RechargeProviderOperatorMasterSelectPendingFailureCount] @RechargeProviderID INT
	,@OperatorID INT
AS
BEGIN
	SELECT ISNULL([PendingCount], 0) AS PendingCount
		,ISNULL([FailureCount], 0) AS FailureCount
		,ISNULL([CheckTimeInMin], 0) AS CheckTimeInMin
	FROM [RechargeProviderOperatorMaster]
	WHERE RechargeProviderID = @RechargeProviderID
		AND OperatorID = @OperatorID
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderOperatorMasterUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================    
--Auther :Priyanka    
--Date :7-3-17    
--Purpose Recharge Provider Operator Master Update     
--==========================================    
--RechargeProviderOperatorMasterUpdate 23,7,'AP',1.2,Rs,1    
CREATE PROCEDURE [dbo].[RechargeProviderOperatorMasterUpdate] @RechargeProviderID INT
	,@OperatorID INT
	,@Opcode VARCHAR(100)
	,@ProviderMargin DECIMAL(18, 5) = 0.0
	,@MarginType VARCHAR(10)
	,@IsActive INT
	,@PendingCount INT = 0
	,@FailureCount INT = 0
	,@CheckTimeInMin INT = 0
	,@Param1 VARCHAR(100) = NULL
AS
BEGIN
	DECLARE @UpdateDoc DATETIME
		,@ServiceID INT;

	SET @ServiceID = (
			SELECT ServiceID
			FROM OperatorMaster
			WHERE OperatorID = @OperatorID
			);
	SET @UpdateDOC = getdate();

	IF NOT EXISTS (
			SELECT *
			FROM RechargeProviderOperatorMaster
			WHERE RechargeProviderID = @RechargeProviderID
				AND OperatorID = @OperatorID
			)
	BEGIN
		INSERT INTO RechargeProviderOperatorMaster (
			RechargeProviderID
			,OperatorID
			,Opcode
			,CommissionAdmin
			,MarginType
			,IsActive
			,UpdateDOC
			,ServiceID
			,PendingCount
			,FailureCount
			,CheckTimeInMin
			,Param1
			)
		VALUES (
			@RechargeProviderID
			,@OperatorID
			,@Opcode
			,@ProviderMargin
			,@MarginType
			,@IsActive
			,@UpdateDOC
			,@ServiceID
			,@PendingCount
			,@FailureCount
			,@CheckTimeInMin
			,@Param1
			)
	END
	ELSE
	BEGIN
		UPDATE RechargeProviderOperatorMaster
		SET Opcode = @Opcode
			,CommissionAdmin = @ProviderMargin
			,MarginType = @MarginType
			,IsActive = @IsActive
			,UpdateDOC = @UpdateDOC
			,PendingCount = @PendingCount
			,FailureCount = @FailureCount
			,CheckTimeInMin = @CheckTimeInMin
			,Param1 = @Param1
		WHERE ServiceID = @ServiceID
			AND RechargeProviderID = @RechargeProviderID
			AND OperatorID = @OperatorID
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderOperatorMasterUpdateAllowedQty]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--==========================================            

--Auther :Sainath Bhujabal         

--Date :1 MArch 2018            

--Purpose Recharge Provider Operator Master Update AllowedQty  

--==========================================        

--[RechargeProviderOperatorMasterUpdateAllowedQty]       

CREATE PROCEDURE [dbo].[RechargeProviderOperatorMasterUpdateAllowedQty] @RechargeProviderID INT

	,@OperatorID INT

	,@AllowedQty VARCHAR(max)

AS

BEGIN

	DECLARE @UpdateDoc DATETIME

		,@ServiceID INT;



	SET @ServiceID = (

			SELECT ServiceID

			FROM OperatorMaster

			WHERE OperatorID = @OperatorID

			);

	SET @UpdateDOC = getdate();



	BEGIN

		UPDATE RechargeProviderOperatorMaster

		SET AllowedQty = @AllowedQty

		WHERE RechargeProviderID = @RechargeProviderID

			AND OperatorID = @OperatorID

	END

END





GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderOperatorMasterUpdateHLR]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--==========================================              

--Auther :Sainath Bhujabal           

--Date :1 MArch 2018              

--Purpose Recharge Provider Operator Master Update AllowedQty    

--==========================================          

--[RechargeProviderOperatorMasterUpdateAllowedQty]         

CREATE PROCEDURE [dbo].[RechargeProviderOperatorMasterUpdateHLR] @RechargeProviderID INT

	,@OperatorID INT

	,@HLR VARCHAR(max)

AS

BEGIN

	DECLARE @UpdateDoc DATETIME

		,@ServiceID INT;



	SET @ServiceID = (

			SELECT ServiceID

			FROM OperatorMaster

			WHERE OperatorID = @OperatorID

			);

	SET @UpdateDOC = getdate();



	BEGIN

		UPDATE RechargeProviderOperatorMaster

		SET HLR = @HLR

		WHERE RechargeProviderID = @RechargeProviderID

			AND OperatorID = @OperatorID

	END

END





GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderRechargeLogInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
-- =============================================              
-- Author:  <Sainath Bhujbal>              
-- ALTER  date: <6 March 2017 ,>              
-- Description: <Insert Recharge Provider Request and Response in Recharge Provider Recharge Log.>              
-- =============================================           
--RechargeProviderRechargeLogInsert         
CREATE  PROCEDURE [dbo].[RechargeProviderRechargeLogInsert] @UserID INT      
 ,@ConsumerNo VARCHAR(20)      
 ,@IPAddress VARCHAR(50)      
 ,@Request NVARCHAR(max)      
 ,@Response NVARCHAR(max)      
 ,@RechargeProviderID INT      
 ,@Through VARCHAR(50) = NULL      
 ,@MoneyTransferID INT = NULL      
 ,@Status Varchar(50) = NULL      
 ,@RechargeID int = null    
 ,@ServiceID int = null    
AS      
BEGIN      
 INSERT INTO [DBO].RechargeProviderRechargeLog (      
  UserID      
  ,ConsumerNumber      
  ,Request      
  ,Response      
  ,IPAddress      
  ,RechargeProviderID      
  ,Through      
  ,MoneyTransferID     
  ,Status     
  ,RechargeID    
  ,ServiceID    
  )      
 VALUES (      
  @UserID      
  ,@ConsumerNo      
  ,@Request      
  ,@Response      
  ,@IPAddress      
  ,@RechargeProviderID      
  ,@Through      
  ,@MoneyTransferID      
  ,@Status    
  ,@RechargeID    
  ,@ServiceID    
  )      
  
 SELECT SCOPE_IDENTITY()  
END 
GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderRechargeLogSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RechargeProviderRechargeLogSelect] @ConsumerNo VARCHAR(50)
AS
BEGIN
	SELECT TOP 1000 convert(VARCHAR(12), W.DOC, 113) + right(convert(VARCHAR(39), W.DOC, 22), 11) AS DOC
		,isnull(W.Request, 0) AS Request
		,isnull(W.Response, 0) AS Response
		,R.RechargeProviderName
	FROM [dbo].[RechargeProviderRechargeLog](NOLOCK) AS W
	INNER JOIN dbo.RechargeProviderMaster(NOLOCK) AS R ON R.RechargeProviderID = W.RechargeProviderID
	WHERE W.Request LIKE '%' + @ConsumerNo + '%'
		OR W.[ConsumerNumber] = @ConsumerNo
	ORDER BY W.DOC DESC
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderRechargeLogSelectPendingFail]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Sushant Yelpale  
-- Create date: 15 Jan 2018  
-- Description: select data for pending or fail count  
-- RechargeProviderRechargeLogSelectPendingFail 8, 21, 50    
-- =============================================  
CREATE PROCEDURE [dbo].[RechargeProviderRechargeLogSelectPendingFail]  
  @RechargeProviderID INT      
  ,@OperatorID INT      
  ,@CheckTimeInMin INT      
AS  
BEGIN  
 PRINT ' @CheckTimeInMin:' + convert(VARCHAR, @CheckTimeInMin)  
   
 select   
 top 100  
 --RPRL.RechargeID,   
 --RPRL.rechargeProviderid,   
 --RPRL.status,   
 --RPRL.DOC,  
 --R.RechargeProviderid,   
 --R.rechargeID,  
 --R.status,  
 --R.consumerNumber  
 RPRL.DOC  
 ,case When R.RechargeProviderid = RPRL.rechargeProviderid  
 THEN R.status  
 ELSE RPRL.status  
 END As Status into #tbl  
 from RechargeProviderRechargeLog RPRL  
 Inner Join Recharge R on R.ServiceID = RPRL.ServiceID  
 AND R.RechargeID = RPRL.RechargeID  
 where RPRL.RechargeProviderID = @RechargeProviderID      
   AND OperatorID = @OperatorID      
   AND RPRL.DOC > DATEADD(minute, - @CheckTimeInMin, GETDATE())      
 order by R.DOC Desc  
  
 select  
 CASE when  t.Status = 'FAILED' OR t.Status = 'FAIL'  
 THEN 'REFUND'  
 ELSE t.Status  
 END As status  
 ,DOC  
 from #tbl t  
  
END  
GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderRechargeLogUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		sushant Yelpale
-- Create date: 15 Jan 2018
-- Description:	update rechargeProviderRechargeLog for status
-- =============================================
CREATE PROCEDURE [dbo].[RechargeProviderRechargeLogUpdate]
	@LogID int
	,@Status Varchar(50)
AS
BEGIN
	update RechargeProviderRechargeLog
	set Status = @Status
	where RechargeProviderRechargeLogID = @LogID
END

GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderRechargeLogUpdateForIsMismatch]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[RechargeProviderRechargeLogUpdateForIsMismatch] @ReplyId INT
AS
BEGIN
	UPDATE RechargeProviderRechargeLog
	SET isMismatch = 1
	WHERE RechargeProviderRechargeLogID = @ReplyId
END

GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderSales]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================                                                                
-- AUTHOR:Priyanka Deshmukh                                                                 
-- ALTER  DATE: 22 MARCH 2017                                                                  
-- DESCRIPTION: Recharge Provider DateWise Sales                                                      
-- ========================================================================   
--RechargeProviderSales
CREATE PROCEDURE [dbo].[RechargeProviderSales]
AS
BEGIN
	SELECT TOP (1000) R.RechargeProviderID
		,RPM.RechargeProviderName
		,R.Amount
		,IsNull(R.RechargeProviderOpeningBalance, 0.00) AS RechargeProviderOpeningBalance
		,IsNull(R.RechargeProviderCommission, 0.00) AS RechargeProviderCommission
		,IsNull(R.RechargeProviderClosingBalance, 0.00) AS RechargeProviderClosingBalance
	FROM Recharge R
	INNER JOIN RechargeProviderMaster RPM ON RPM.RechargeProviderID = R.RechargeProviderID
	ORDER BY convert(DATETIME, R.DOC, 103) DESC
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderSelectByOperator]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================                          
-- Author:  <Sainath Bhujbal>                          
-- ALTER  date: <3 March 2017 ,>                          
-- Description: <Description,,>                          
-- =============================================             
--[RechargeProviderSelectByOperator_Allqty] 1018,10           
--[RechargeProviderSelectByOperator_localTest] 6,11           
CREATE PROCEDURE [dbo].[RechargeProviderSelectByOperator] @OperatorID INT
	,@Amount DECIMAL(18, 5) = NULL
AS
DECLARE @Cnt INT
DECLARE @Qty INT -- varchar(10)         
DECLARE @varQty VARCHAR(10)
DECLARE @AllowedQty VARCHAR(max)

SET @Qty = convert(INT, @Amount)
SET @varQty = convert(VARCHAR, @Qty)

PRINT @varQty

BEGIN
	SELECT OperatorID
		,OperatorName
		,ServiceID
		,ServiceName
		,ProviderID
		,RechargeProviderName
		,Sequence
		,AllowedQty
		,HLR
	INTO dbo.#tempAPIwiseAllQty
	FROM (
		SELECT RPC.OperatorID
			,OM.OperatorName
			,SM.ServiceID
			,SM.ServiceName
			,RPC.ProviderID
			,RPM.RechargeProviderName
			,RPC.Sequence AS Sequence
			,isnull(RPOM.AllowedQty, 0) AS AllowedQty
			,isnull(RPOM.HLR, 0) AS HLR
		FROM RechargeProviderConfiguration RPC
		INNER JOIN dbo.OperatorMaster OM ON RPC.OperatorID = OM.OperatorID
		INNER JOIN ServiceMaster SM ON OM.ServiceID = SM.ServiceID
		INNER JOIN dbo.RechargeProviderServices RPS ON SM.ServiceID = RPS.ServiceID
		INNER JOIN dbo.RechargeProviderMaster RPM ON RPS.RechargeProviderID = RPM.RechargeProviderID
			AND RPC.ProviderID = RPM.RechargeProviderID
		INNER JOIN dbo.[RechargeProviderOperatorMaster] RPOM(NOLOCK) ON RPOM.RechargeProviderID = RPM.RechargeProviderID
			AND RPOM.OperatorID = OM.OperatorID
			AND RPOM.ServiceID = SM.ServiceID
		
		UNION
		
		SELECT OperatorID
			,OperatorName
			,ServiceID
			,ServiceName
			,ProviderID
			,RechargeProviderName
			,Sequence
			,AllowedQty
			,HLR
		FROM (
			SELECT OM.OperatorID
				,OM.OperatorName
				,SM.ServiceID
				,SM.ServiceName
				,RPM.RechargeProviderID AS ProviderID
				,RPM.RechargeProviderName
				,999 AS Sequence
				,isnull(RPOM.AllowedQty, 0) AS AllowedQty
				,isnull(RPOM.HLR, 0) AS HLR
			FROM dbo.OperatorMaster OM
			INNER JOIN ServiceMaster SM ON OM.ServiceID = SM.ServiceID
			INNER JOIN dbo.RechargeProviderServices RPS ON SM.ServiceID = RPS.ServiceID
			INNER JOIN dbo.RechargeProviderMaster RPM ON RPS.RechargeProviderID = RPM.RechargeProviderID
			INNER JOIN dbo.[RechargeProviderOperatorMaster] RPOM(NOLOCK) ON RPOM.RechargeProviderID = RPM.RechargeProviderID
				AND RPOM.OperatorID = OM.OperatorID
				AND RPOM.ServiceID = SM.ServiceID
			
			EXCEPT
			
			SELECT RPC.OperatorID
				,OM.OperatorName
				,SM.ServiceID
				,SM.ServiceName
				,RPC.ProviderID
				,RPM.RechargeProviderName
				,999 AS Sequence
				,isnull(RPOM.AllowedQty, 0) AS AllowedQty
				,isnull(RPOM.HLR, 0) AS HLR
			FROM RechargeProviderConfiguration RPC
			INNER JOIN dbo.OperatorMaster OM ON RPC.OperatorID = OM.OperatorID
			INNER JOIN ServiceMaster SM ON OM.ServiceID = SM.ServiceID
			INNER JOIN dbo.RechargeProviderServices RPS ON SM.ServiceID = RPS.ServiceID
			INNER JOIN dbo.RechargeProviderMaster RPM ON RPS.RechargeProviderID = RPM.RechargeProviderID
				AND RPC.ProviderID = RPM.RechargeProviderID
			INNER JOIN dbo.[RechargeProviderOperatorMaster] RPOM(NOLOCK) ON RPOM.RechargeProviderID = RPM.RechargeProviderID
				AND RPOM.OperatorID = OM.OperatorID
				AND RPOM.ServiceID = SM.ServiceID
			) AS exceptTable
		) AS SubTable
	WHERE OperatorID = @OperatorID
		AND Sequence NOT IN (999)
	ORDER BY ServiceID
		,OperatorID
		,Sequence
		,ProviderID

	SET @AllowedQty = (
			SELECT TOP (1) isnull(AllowedQty, 0)
			FROM dbo.#tempAPIwiseAllQty
			WHERE AllowedQty != '0'
				AND AllowedQty LIKE '%' + @varQty + '%'
			ORDER BY AllowedQty DESC
			)

	--print '@AllowedQty' + @AllowedQty        
	SELECT @Cnt = count(1)
	FROM dbo.SplitString(@AllowedQty, ',')
	WHERE item = @varQty

	--print @Cnt        
	IF (@Cnt > 0)
	BEGIN
		--print 'database in'        
		--select * from dbo.#tempAPIwiseAllQty        
		--order by AllowedQty desc    
		SELECT *
			,1 AS Rno
		FROM dbo.#tempAPIwiseAllQty
		WHERE AllowedQty LIKE '%' + @varQty + '%'
		
		UNION
		
		SELECT *
			,2 AS Rno
		FROM dbo.#tempAPIwiseAllQty
		WHERE AllowedQty NOT LIKE '%' + @varQty + '%'
		ORDER BY Rno ASC
	END
	ELSE
	BEGIN
		SELECT *
		FROM dbo.#tempAPIwiseAllQty
		ORDER BY ServiceID
			,OperatorID
			,Sequence
			,ProviderID
			--print 'database out'        
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderSelectByOperatorUserwise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                    

-- Author:  <Sainath Bhujbal>                    

-- ALTER  date: <3 March 2017 ,>                    

-- Description: <Description,,>                    

-- =============================================       

-- RechargeProviderSelectByOperator 2    

-- [RechargeProviderSelectByOperatorUserwise] 1,9,1039    

-- [RechargeProviderSelectByOperatorUserwise] 1,9,3    

CREATE PROCEDURE [dbo].[RechargeProviderSelectByOperatorUserwise] @OperatorID INT

	,@Amount DECIMAL(18, 5) = NULL

	,@UserID INT = 0

AS

BEGIN

	DECLARE @Cnt INT

	DECLARE @Qty INT -- varchar(10)         

	DECLARE @varQty VARCHAR(10)

	DECLARE @AllowedQty VARCHAR(max)



	SET @Qty = convert(INT, @Amount)

	SET @varQty = convert(VARCHAR, @Qty)



	create TABLE #tmpUserwiseProvider (

		ProviderID INT

		,Sequence INT

		);



	INSERT INTO #tmpUserwiseProvider

	SELECT ProviderID

		,Sequence

	FROM RechargeProviderConfigurationUserwise

	WHERE OperatorID = @OperatorID

		AND UserID = @UserID



	DECLARE @countProvider INT;



	SELECT @countProvider = isnull(count(1), 0)

	FROM #tmpUserwiseProvider



	PRINT @varQty;

	PRINT '@countProvider:' + convert(VARCHAR, @countProvider);



	--- Check in Userwise Table For presence of Operator Sequence    

	IF (@countProvider <= 0)

	BEGIN

		EXEC [RechargeProviderSelectByOperator] @OperatorID

			,@Amount



		RETURN @@rowcount

	END



	PRINT 'checking for userwise '



	SELECT OperatorID

		,OperatorName

		,ServiceID

		,ServiceName

		,ProviderID

		,RechargeProviderName

		,Sequence

		,AllowedQty

		,HLR

	INTO dbo.#tempAPIwiseAllQty

	FROM (

		SELECT OM.OperatorID AS OperatorID

			,OM.OperatorName AS OperatorName

			,SM.ServiceID AS ServiceID

			,SM.ServiceName AS ServiceName

			,RPM.RechargeProviderID AS ProviderID

			,RPM.RechargeProviderName AS RechargeProviderName

			,CASE 

				WHEN RPCU.Sequence = NULL

					THEN '999'

				ELSE RPCU.Sequence

				END AS Sequence

			,isnull(RPOM.AllowedQty, 0) AS AllowedQty

			,isnull(RPOM.HLR, 0) AS HLR

		FROM dbo.OperatorMaster OM

		INNER JOIN ServiceMaster SM ON OM.ServiceID = SM.ServiceID

		INNER JOIN dbo.RechargeProviderServices RPS ON SM.ServiceID = RPS.ServiceID

		INNER JOIN dbo.RechargeProviderMaster RPM ON RPS.RechargeProviderID = RPM.RechargeProviderID

		INNER JOIN dbo.[RechargeProviderOperatorMaster] RPOM(NOLOCK) ON RPOM.RechargeProviderID = RPM.RechargeProviderID

			AND RPOM.OperatorID = OM.OperatorID

			AND RPOM.ServiceID = SM.ServiceID

		INNER JOIN #tmpUserwiseProvider RPCU ON RPCU.ProviderID = RPM.RechargeProviderID

		) AS SubTable

	WHERE OperatorID = @OperatorID

		AND Sequence NOT IN ('999')

	ORDER BY ServiceID

		,OperatorID

		,Sequence

		,ProviderID



	SET @AllowedQty = (

			SELECT TOP (1) isnull(AllowedQty, 0)

			FROM dbo.#tempAPIwiseAllQty

			WHERE AllowedQty != '0'

				AND AllowedQty LIKE '%' + @varQty + '%'

			ORDER BY AllowedQty DESC

			)



	PRINT '@AllowedQty' + @AllowedQty



	SELECT @Cnt = count(1)

	FROM dbo.SplitString(@AllowedQty, ',')

	WHERE item = @varQty



	PRINT @Cnt



	IF (@Cnt > 0)

	BEGIN

		PRINT 'database in'



		SELECT *

			,1 AS Rno

		FROM dbo.#tempAPIwiseAllQty

		WHERE AllowedQty LIKE '%' + @varQty + '%'

		

		UNION

		

		SELECT *

			,2 AS Rno

		FROM dbo.#tempAPIwiseAllQty

		WHERE AllowedQty NOT LIKE '%' + @varQty + '%'

		ORDER BY Rno ASC

	END

	ELSE

	BEGIN

		SELECT *

		FROM dbo.#tempAPIwiseAllQty  where (AllowedQty is null or AllowedQty ='0')

		ORDER BY ServiceID

			,OperatorID

			,Sequence

			,ProviderID

	END



	RETURN @@rowcount

END

	



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderSelectURL]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	-- Author:  <Sainath Bhujbal>
	-- Create date: <6 March 2017 ,>
	-- Description: <Description,,>
	-- =============================================
	--RechargeProviderSelectURL
	---Need to change URL when work with
--  [RechargeProviderSelectURL]  2,0
CREATE PROCEDURE [dbo].[RechargeProviderSelectURL] @RechargeProviderID INT
	,@OperatorId INT = NULL
AS
BEGIN
	IF (@OperatorId IS NULL)
	BEGIN
		SET @OperatorId = 0;
	END

	IF @OperatorId = 0
	BEGIN
		SELECT TOP 1 ([HostName] + [UrlParameter]) AS URL
			,([HostName] + RechargeProviderUrl) AS ElectricityBill
			,BalanceURL
			,([StatusCheckUrl]) AS StatusCheckUrl
			,IsResend
			,IsNull(ResendMessage, '') AS ResendMessage
			,'' AS HEADER
			,'' AS BODY
			,'' AS OperatorURL
		FROM [RechargeProviderMaster]
		WHERE [RechargeProviderID] = @RechargeProviderID
	END
	ELSE
	BEGIN
		SELECT TOP 1 ([HostName] + [UrlParameter]) AS URL
			,([HostName] + RechargeProviderUrl) AS ElectricityBill
			,BalanceURL
			,([StatusCheckUrl]) AS StatusCheckUrl
			,IsResend
			,ISNULL (ResendMessage,'') AS ResendMessage
			,ISNULL(Header, '') AS HEADER
			,ISNULL(Body, '') AS BODY
			,ISNULL(URL, '') AS OperatorURL
		FROM [RechargeProviderMaster] RPM
		INNER JOIN rechargeprovideroperatorMaster RPOM ON RPOM.[RechargeProviderID] = RPM.[RechargeProviderID]
		WHERE RPM.[RechargeProviderID] = @RechargeProviderID
			AND RPOM.[RechargeProviderID] = @RechargeProviderID
			AND RPOM.Operatorid = @OperatorId
	END
END

GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderSequenceSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeProviderSequenceSelect] @ServiceID INT
AS
BEGIN
	IF (@ServiceID != 0)
	BEGIN
		SELECT OperatorID
			,OperatorName
			,ServiceID
			,ServiceName
			,ProviderID
			,RechargeProviderName
			,Sequence
		FROM (
			SELECT RPC.OperatorID
				,OM.OperatorName
				,SM.ServiceID
				,SM.ServiceName
				,RPC.ProviderID
				,RPM.RechargeProviderName
				,RPC.Sequence AS Sequence
			FROM dbo.RechargeProviderConfiguration RPC
			INNER JOIN dbo.OperatorMaster OM ON RPC.OperatorID = OM.OperatorID
				AND OM.IsDelete = 0
			INNER JOIN dbo.ServiceMaster SM ON OM.ServiceID = SM.ServiceID
				AND SM.IsDeleted = 0
			INNER JOIN dbo.RechargeProviderServices RPS ON SM.ServiceID = RPS.ServiceID
			INNER JOIN dbo.RechargeProviderMaster RPM ON RPS.RechargeProviderID = RPM.RechargeProviderID
				AND RPM.isDelete = 0
				AND RPC.ProviderID = RPM.RechargeProviderID
			
			UNION
			
			SELECT OperatorID
				,OperatorName
				,ServiceID
				,ServiceName
				,ProviderID
				,RechargeProviderName
				,Sequence
			FROM (
				SELECT OM.OperatorID
					,OM.OperatorName
					,SM.ServiceID
					,SM.ServiceName
					,RPM.RechargeProviderID AS ProviderID
					,RPM.RechargeProviderName
					,999 AS Sequence
				FROM dbo.OperatorMaster OM
				INNER JOIN ServiceMaster SM ON OM.ServiceID = SM.ServiceID
					AND SM.IsDeleted = 0
				INNER JOIN dbo.RechargeProviderServices RPS ON SM.ServiceID = RPS.ServiceID
				INNER JOIN dbo.RechargeProviderMaster RPM ON RPS.RechargeProviderID = RPM.RechargeProviderID
					AND RPM.isDelete = 0
				WHERE OM.IsDelete = 0
				
				EXCEPT
				
				SELECT RPC.OperatorID
					,OM.OperatorName
					,SM.ServiceID
					,SM.ServiceName
					,RPC.ProviderID
					,RPM.RechargeProviderName
					,999 AS Sequence
				FROM dbo.RechargeProviderConfiguration RPC
				INNER JOIN dbo.OperatorMaster OM ON RPC.OperatorID = OM.OperatorID
					AND OM.IsDelete = 0
				INNER JOIN ServiceMaster SM ON OM.ServiceID = SM.ServiceID
					AND SM.IsDeleted = 0
				INNER JOIN dbo.RechargeProviderServices RPS ON SM.ServiceID = RPS.ServiceID
				INNER JOIN dbo.RechargeProviderMaster RPM ON RPS.RechargeProviderID = RPM.RechargeProviderID
					AND RPM.isDelete = 0
					AND RPC.ProviderID = RPM.RechargeProviderID
				) AS exceptTable
			) AS SubTable
		WHERE ServiceID = @ServiceID
		ORDER BY ServiceID
			,OperatorName
			,Sequence
			--OperatorID  
			--,Sequence  
			--,ProviderID  
	END

	IF (@ServiceID = 0)
	BEGIN
		SELECT OperatorID
			,OperatorName
			,ServiceID
			,ServiceName
			,ProviderID
			,RechargeProviderName
			,Sequence
		FROM (
			SELECT RPC.OperatorID
				,OM.OperatorName
				,SM.ServiceID
				,SM.ServiceName
				,RPC.ProviderID
				,RPM.RechargeProviderName
				,RPC.Sequence AS Sequence
			FROM dbo.RechargeProviderConfiguration RPC
			INNER JOIN dbo.OperatorMaster OM ON RPC.OperatorID = OM.OperatorID
				AND OM.IsDelete = 0
			INNER JOIN ServiceMaster SM ON OM.ServiceID = SM.ServiceID
				AND SM.IsDeleted = 0
			INNER JOIN dbo.RechargeProviderServices RPS ON SM.ServiceID = RPS.ServiceID
			INNER JOIN dbo.RechargeProviderMaster RPM ON RPS.RechargeProviderID = RPM.RechargeProviderID
				AND RPM.isDelete = 0
				AND RPC.ProviderID = RPM.RechargeProviderID
			
			UNION
			
			SELECT OperatorID
				,OperatorName
				,ServiceID
				,ServiceName
				,ProviderID
				,RechargeProviderName
				,Sequence
			FROM (
				SELECT OM.OperatorID
					,OM.OperatorName
					,SM.ServiceID
					,SM.ServiceName
					,RPM.RechargeProviderID AS ProviderID
					,RPM.RechargeProviderName
					,999 AS Sequence
				FROM dbo.OperatorMaster OM
				INNER JOIN ServiceMaster SM ON OM.ServiceID = SM.ServiceID
					AND SM.IsDeleted = 0
				INNER JOIN dbo.RechargeProviderServices RPS ON SM.ServiceID = RPS.ServiceID
				INNER JOIN dbo.RechargeProviderMaster RPM ON RPS.RechargeProviderID = RPM.RechargeProviderID
					AND RPM.isDelete = 0
				WHERE OM.IsDelete = 0
				
				EXCEPT
				
				SELECT RPC.OperatorID
					,OM.OperatorName
					,SM.ServiceID
					,SM.ServiceName
					,RPC.ProviderID
					,RPM.RechargeProviderName
					,999 AS Sequence
				FROM RechargeProviderConfiguration RPC
				INNER JOIN dbo.OperatorMaster OM ON RPC.OperatorID = OM.OperatorID
					AND OM.IsDelete = 0
				INNER JOIN ServiceMaster SM ON OM.ServiceID = SM.ServiceID
					AND SM.IsDeleted = 0
				INNER JOIN dbo.RechargeProviderServices RPS ON SM.ServiceID = RPS.ServiceID
				INNER JOIN dbo.RechargeProviderMaster RPM ON RPS.RechargeProviderID = RPM.RechargeProviderID
					AND RPM.isDelete = 0
					AND RPC.ProviderID = RPM.RechargeProviderID
				) AS exceptTable
			) AS SubTable
		ORDER BY ServiceID
			,OperatorName
			,Sequence
			--,OperatorID  
			-- 
			--,ProviderID  
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderSequenceSelectUserwise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Auther :Sushant Yelpale      
--Date  :03/07/2018        
--Purpose :Selects Operator With Providers Userwise, Servicewise      
--==========================================        
--- RechargeProviderSequenceSelect 0        
-- [RechargeProviderSequenceSelectUserwise] 1, 3      
CREATE PROCEDURE [dbo].[RechargeProviderSequenceSelectUserwise] @ServiceID INT  
 ,@UserID INT  
AS  
BEGIN  
 create  TABLE #tempServices (ServiceID INT)  
  
 IF (@ServiceID = 0)  
 BEGIN  
  INSERT INTO #tempServices  
  SELECT ServiceID  
  FROM ServiceMaster  
 END  
 ELSE  
 BEGIN  
  INSERT INTO #tempServices  
  VALUES (@serviceID)  
 END  
  
 SELECT OM.OperatorID AS OperatorID  
  ,OM.OperatorName AS OperatorName  
  ,SM.ServiceID AS ServiceID  
  ,SM.ServiceName AS ServiceName  
  ,RPM.RechargeProviderID AS ProviderID  
  ,RPM.RechargeProviderName AS RechargeProviderName  
  ,ISNULL(RPCU.Sequence, '999') AS Sequence -- set default Sequesnce as 999      
 INTO #temp  
 FROM dbo.OperatorMaster OM  
 INNER JOIN ServiceMaster SM ON OM.ServiceID = SM.ServiceID  
 INNER JOIN dbo.RechargeProviderServices RPS ON SM.ServiceID = RPS.ServiceID  
 INNER JOIN dbo.RechargeProviderMaster RPM ON RPS.RechargeProviderID = RPM.RechargeProviderID  
 LEFT JOIN RechargeProviderConfigurationUserwise RPCU ON RPCU.OperatorID = OM.OperatorID  
  AND RPCU.ProviderID = RPM.RechargeProviderID  
  AND RPCU.UserID = @UserID  
 WHERE OM.IsDelete = 0  
  AND RPM.isDelete = 0  
 ORDER BY ServiceID  
  ,OperatorID  
  ,Sequence  
  ,ProviderID  
  
 -- selects present operators from userwise      
 SELECT DISTINCT OperatorID  
 INTO #UserwiseOperator  
 FROM #temp  
 WHERE Sequence != '999'  
  
 -- selects userwise for operator. If not present for that operator selcts from Defaults.
  Select * from
 (      
 SELECT t.OperatorID  
  ,t.OperatorName  
  ,t.ServiceID  
  ,t.ServiceName  
  ,t.ProviderID  
  ,t.RechargeProviderName  
  ,ISNULL((  
    CASE   
     WHEN t.OperatorID NOT IN (  
       SELECT OperatorID  
       FROM #UserwiseOperator  
       )  
      THEN RPC.Sequence  
     ELSE t.Sequence  
     END  
    ), '999') AS Sequence  
 FROM #temp t  
 LEFT JOIN RechargeProviderConfiguration RPC ON (  
   RPC.OperatorID = t.OperatorID  
   AND RPC.ProviderID = t.ProviderID  
   )  
 WHERE t.serviceID IN (  
   SELECT serviceID  
   FROM #tempServices  
   )  
  ) As d
  order by ServiceID  
  ,OperatorID  
  ,Sequence  
  ,ProviderID  
END  
 -- [RechargeProviderSequenceSelectUserwise] 1, 3093      
 -- [RechargeProviderSequenceSelectUserwise] 0, 3   
  
  
GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderServiceDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--RechargeProviderServiceDelete 
CREATE PROCEDURE [dbo].[RechargeProviderServiceDelete] @RechargeProviderID INT
AS
BEGIN
	DELETE
	FROM RechargeProviderServices
	WHERE RechargeProviderID = @RechargeProviderID
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderServiceMapService]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :9-7-17
--Purpose RechargeProviderService Map Service
--==========================================
--RechargeProviderServiceMapService 25,1
CREATE PROCEDURE [dbo].[RechargeProviderServiceMapService] @RechargeProviderID INT
	,@ServiceID INT
AS
BEGIN
	IF NOT EXISTS (
			SELECT RechargeProviderID
				,ServiceID
			FROM RechargeProviderServices
			WHERE RechargeProviderID = @RechargeProviderID
				AND ServiceID = @ServiceID
			)
	BEGIN
		INSERT INTO RechargeProviderServices (
			RechargeProviderID
			,ServiceID
			)
		VALUES (
			@RechargeProviderID
			,@ServiceID
			)
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeProviderServicesSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [RechargeProviderServicesSelect] 1
CREATE PROCEDURE [dbo].[RechargeProviderServicesSelect] @RechargeProviderID INT
AS
BEGIN
	SELECT RPS.ServiceID
		,SM.ServiceName
		,RPM.RechargeProviderName
		,RPS.RechargeProviderID
	FROM dbo.RechargeProviderServices(NOLOCK) RPS
	INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON RPS.ServiceID = SM.ServiceID
	INNER JOIN dbo.RechargeProviderMaster(NOLOCK) RPM ON RPS.RechargeProviderID = RPM.RechargeProviderID
	WHERE RPS.RechargeProviderID = @RechargeProviderID
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RechargeSelect] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT;
	DECLARE @retailer VARCHAR(20) = 'RETAILER';
	DECLARE @Customer VARCHAR(20) = 'Customer';
	DECLARE @distributor VARCHAR(20) = 'DISTRIBUTOR';
	DECLARE @admin VARCHAR(20) = 'ADMIN';
	DECLARE @apiUser VARCHAR(20) = 'APIUSER';
	DECLARE @userType VARCHAR(20);
	SELECT @userType = UTM.UserType
	FROM [dbo].UserInformation UI(NOLOCK)
	INNER JOIN UserTypeMaster UTM(NOLOCK) ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @USERID
	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)
	IF (
			Upper(@userType) = Upper(@retailer)
			OR Upper(@userType) = Upper(@Customer)
			OR Upper(@userType) = Upper(@apiUser)
			)
	BEGIN
		SELECT *
		FROM (
			SELECT R.UserID
				,R.TransactionID
				,TD.RechargeID
				,TD.serviceID
				,TD.ConsumerNumber
				,TD.DOCType
				,TD.OpeningBalance
				,TD.ClosingBalance
				,TD.TransactionAmount AS Amount
				,TD.DiscountAmount
				,TD.AmountCreditDebit
				,TD.CommissionCreditDebit
				,convert(VARCHAR, R.DOC, 113) AS DOC
				,R.IsDispute
				,R.OperatorID
				,SM.ServiceName
				,OM.OperatorName
				,UI.UserName
				,-- UI.UserName,                
				UI.UserTypeID
				,R.STATUS
				,UIS.UserID AS ParentID
				,UIS.UserName AS ParentName
				,UIS.UserTypeID AS DUserTypeID
				,r.RechargeID as IdNo
				,ROW_NUMBER() OVER (
					PARTITION BY TD.RechargeID ORDER BY TD.TransactionNumberID DESC
					) [SR]
			FROM [dbo].TransactionDetails TD(NOLOCK)
			INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID
				AND R.ServiceID = TD.ServiceID
				AND R.UserID = TD.UserID
			INNER JOIN [dbo].UserInformation UI(NOLOCK) ON TD.UserID = UI.UserID
			INNER JOIN [dbo].UserInformation UIS(NOLOCK) ON UIS.UserID = UI.ParentID
			INNER JOIN [dbo].ServiceMaster SM(NOLOCK) ON R.ServiceID = SM.ServiceID
			INNER JOIN [dbo].OperatorMaster OM(NOLOCK) ON R.OperatorID = OM.OperatorID
			WHERE TD.UserID = @UserID
			) AS tab4
		WHERE [SR] = 1
		ORDER BY IdNo DESC
	END
END

GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectAllPendingRecharge]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectAllPendingRecharge]
AS
BEGIN
	DECLARE @CurTime DATETIME;
	DECLARE @GasServiceID INT = 19;

	SET @CurTime = CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30'), (0))

	--print @CurTime            
	SELECT R.RechargeID
		,R.ServiceID
		,R.TransactionID
		,R.RechargeProviderId
		,R.OperatorID
		,R.ConsumerNumber
		,R.Amount
		,R.STATUS
		,R.IsDispute
		,RPM.RechargeProviderName
		,RPM.RechargeProviderUrl
		,RPM.HostName
		,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
		,SM.ServiceName
		,OM.OperatorName
		,R.OpeningBalance
		,TD.DiscountAmount
		,R.UserID
		,UI.UserName AS Uname
		,UI.UserTypeID
	FROM Recharge R
	INNER JOIN UserInformation UI ON R.UserID = UI.UserID
	INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
	INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID
	INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID
	INNER JOIN TransactionDetails TD ON R.RechargeID = TD.RechargeID
		AND R.ServiceID = TD.ServiceID
		AND R.UserID = TD.UserID
	WHERE UPPER(R.STATUS) = UPPER('PROCESS')
		AND R.ServiceID != @GasServiceID
	ORDER BY R.DOC DESC
END

GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectAndroidDateWise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================    
--Auther :Priyanka    
--Date :11-4-17    
--Purpose Recharge report datewise to android team
--==========================================    
--[RechargeSelectAndroidDateWise] '15/4/2017',1 
CREATE PROCEDURE [dbo].[RechargeSelectAndroidDateWise] @Date VARCHAR(100)
	,@UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT;

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	--Admin Recharge Report
	IF (@UserTypeID = 1)
	BEGIN
		SELECT *
		FROM (
			SELECT TOP 1000 R.UserID
				,TD.RechargeID
				,TD.serviceID
				,TD.ConsumerNumber
				,TD.DOCType
				,TD.OpeningBalance
				,TD.ClosingBalance
				,TD.TransactionAmount AS Amount
				,TD.DiscountAmount
				,TD.AmountCreditDebit
				,TD.CommissionCreditDebit
				,convert(VARCHAR, TD.DOC, 113) AS DOC
				,R.OperatorID
				,R.IsDispute
				,SM.ServiceName
				,UI.UserTypeID
				,OM.OperatorName
				,UI.UserName
				,R.TransactionID
				,R.STATUS
				,ROW_NUMBER() OVER (
					PARTITION BY TD.RechargeID ORDER BY TD.TransactionNumberID DESC
					) [SR]
			FROM dbo.TransactionDetails(NOLOCK) TD
			INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
				AND R.UserID = TD.UserID
			INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
			INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
			INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
			WHERE (convert(DATE, TD.DOC, 103) = convert(DATE, @Date, 103))
			ORDER BY convert(DATETIME, TD.TransactionNumberID, 103) DESC
			) t
		WHERE [SR] = 1
	END

	--Distributor Recharge Report
	IF (@UserTypeID = 2)
	BEGIN
		SELECT *
		FROM (
			SELECT TOP 1000 TD.UserID
				,TD.RechargeID
				,TD.serviceID
				,TD.ConsumerNumber
				,TD.DOCType
				,TD.OpeningBalance
				,TD.ClosingBalance
				,TD.TransactionAmount AS Amount
				,TD.DiscountAmount
				,TD.AmountCreditDebit
				,TD.CommissionCreditDebit
				,convert(VARCHAR, TD.DOC, 113) AS DOC
				,R.OperatorID
				,R.IsDispute
				,SM.ServiceName
				,OM.OperatorName
				,R.TransactionID
				,UI.UserName
				,UI.UserTypeID
				,UI.ParentID
				,R.STATUS
				,ROW_NUMBER() OVER (
					PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
					) [SR]
			FROM TransactionDetails TD
			INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID
				AND R.UserID = TD.UserID
			INNER JOIN UserInformation UI ON TD.UserID = UI.UserID
			INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
			INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID
			WHERE @UserID IN (
					TD.UserID
					,UI.ParentID
					)
				AND (convert(DATE, TD.DOC, 103) = convert(DATE, @Date, 103))
			ORDER BY convert(DATETIME, TD.DOC, 103) DESC
			) t
		WHERE [SR] = 1
	END

	--Retailer Recharge Report
	IF (@UserTypeID = 3)
	BEGIN
		SELECT *
		FROM (
			SELECT TOP 1000 R.UserID
				,TD.RechargeID
				,TD.serviceID
				,TD.ConsumerNumber
				,TD.DOCType
				,TD.OpeningBalance
				,TD.ClosingBalance
				,TD.TransactionAmount AS Amount
				,TD.DiscountAmount
				,TD.AmountCreditDebit
				,TD.CommissionCreditDebit
				,convert(VARCHAR, TD.DOC, 113) AS DOC
				,R.IsDispute
				,R.OperatorID
				,R.TransactionID
				,SM.ServiceName
				,UI.UserTypeID
				,OM.OperatorName
				,UI.UserName
				,R.STATUS
				,ROW_NUMBER() OVER (
					PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
					) [SR]
			FROM TransactionDetails TD
			INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID
				AND R.UserID = TD.UserID
			INNER JOIN UserInformation UI ON TD.UserID = UI.UserID
			INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
			INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID
			WHERE TD.UserID = @UserID
				AND (convert(DATE, TD.DOC, 103) = convert(DATE, @Date, 103))
			ORDER BY convert(DATETIME, TD.DOC, 103) DESC
			) t
		WHERE [SR] = 1
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectByConsumerNo]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================                                        
--Auther :Sainath   Bhujabal                                      
--Date :18-2-17                                        
--Purpose  select  Recharge details by ConsumerNo                                 
--==========================================                                        
--[[RechargeSelectDatewise]] 3                                        
--[RechargeSelectByConsumerNo] 2610, '8007141498'                            
CREATE PROCEDURE [dbo].[RechargeSelectByConsumerNo] @UserID INT
	,@ConsumerNo VARCHAR(100)
AS
BEGIN
	DECLARE @UserTypeID INT;
	DECLARE @DMRServiceID INT = 21;
	DECLARE @retailer VARCHAR(20) = 'RETAILER';
	DECLARE @Customer VARCHAR(20) = 'Customer';
	DECLARE @distributor VARCHAR(20) = 'DISTRIBUTOR';
	DECLARE @admin VARCHAR(20) = 'ADMIN';
	DECLARE @fos VARCHAR(20) = 'FOS';
	DECLARE @apiUser VARCHAR(20) = 'APIUSER';
	DECLARE @Support VARCHAR(20) = 'SUPPORT';
	DECLARE @userType VARCHAR(20);

	SELECT @userType = UTM.UserType
	FROM [dbo].UserInformation(NOLOCK) UI
	INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @UserID

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	--Admin Recharge Report                                        
	IF (
			Upper(@userType) = Upper(@admin)
			OR Upper(@userType) = Upper(@Support)
			)
	BEGIN
		SELECT *
		FROM (
			SELECT *
			FROM (
				SELECT R.UserID
					,R.TransactionID
					,TD.RechargeID
					,TD.serviceID
					,TD.ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					--,convert(VARCHAR, R.DOC, 113) AS DOC            
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,R.IsDispute
					,R.OperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,--UI.UserName,                                  
					UI.UserTypeID
					,R.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY R.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
					AND R.UserID = TD.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
				WHERE (R.ConsumerNumber = @ConsumerNo)
				) AS maint
			WHERE [SR] = 1
			
			UNION ALL
			
			-- for DMR            
			SELECT *
			FROM (
				SELECT M.UserID
					,M.[ProviderOprID] AS TransactionID
					,TD.RechargeID
					,TD.serviceID
					,M.BeneficiaryAccountNo AS ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					--,convert(VARCHAR, R.DOC, 113) AS DOC            
					,convert(VARCHAR(12), M.DOC, 113) + right(convert(VARCHAR(39), M.DOC, 22), 11) AS DOC
					--,M.IsDispute            
					,'' AS IsDispute
					,M.[DMROperatorID]
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,--UI.UserName,                                  
					UI.UserTypeID
					,M.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY M.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.MoneyTransfer(NOLOCK) M ON M.[MoneyTransferID] = TD.RechargeID
					AND M.UserID = TD.UserID
					AND TD.ServiceID = @DMRServiceID
					AND TD.DOCTYPE = 'MoneyTransfer'
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON M.[DMRServiceID] = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON M.[DMROperatorID] = OM.OperatorID
				WHERE LTRIM(RTRIM((M.BeneficiaryAccountNo))) = @ConsumerNo
				) DMRTab
			WHERE [SR] = 1
			) AS UnionBoth
		ORDER BY convert(DATETIME, DOC, 103) DESC
	END

	--Distributor Recharge Report                                  
	IF (Upper(@userType) = Upper(@distributor))
	BEGIN
	SELECT *
		FROM (
		SELECT *
		FROM (
			SELECT R.UserID
				,R.TransactionID
				,TD.RechargeID
				,TD.serviceID
				,TD.ConsumerNumber
				,TD.DOCType
				,TD.OpeningBalance
				,TD.ClosingBalance
				,TD.TransactionAmount AS Amount
				,TD.DiscountAmount
				,TD.AmountCreditDebit
				,TD.CommissionCreditDebit
				--,convert(VARCHAR, R.DOC, 113) AS DOC            
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
				,R.IsDispute
				,R.OperatorID
				,SM.ServiceName
				,OM.OperatorName
				,UI.UserName AS UserName
				,-- UI.UserName,                                  
				UI.UserTypeID
				,R.STATUS
				,UIS.UserID AS ParentID
				,UIS.UserName AS ParentName
				,UIS.UserTypeID AS DUserTypeID
				,ROW_NUMBER() OVER (
					PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
					) [SR]
			FROM TransactionDetails(NOLOCK) TD
			INNER JOIN Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
				AND R.ServiceID = TD.ServiceID
				AND R.UserID = TD.UserID
			INNER JOIN UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
			INNER JOIN UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
			INNER JOIN ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
			INNER JOIN OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
			WHERE (R.ConsumerNumber = @ConsumerNo)
				AND @UserID IN (
					TD.UserID
					,UI.ParentID
					)
			) AS tab2
		WHERE [SR] = 1  
		
		UNION ALL
		
		SELECT *
		FROM (
			SELECT M.UserID
				,M.[ProviderOprID] AS TransactionID
				,TD.RechargeID
				,TD.serviceID
				,M.BeneficiaryAccountNo AS ConsumerNumber
				,TD.DOCType
				,TD.OpeningBalance
				,TD.ClosingBalance
				,TD.TransactionAmount AS Amount
				,TD.DiscountAmount
				,TD.AmountCreditDebit
				,TD.CommissionCreditDebit
				--,convert(VARCHAR, R.DOC, 113) AS DOC            
				,convert(VARCHAR(12), M.DOC, 113) + right(convert(VARCHAR(39), M.DOC, 22), 11) AS DOC
				--,M.IsDispute            
				,'' AS IsDispute
				,M.[DMROperatorID]
				,SM.ServiceName
				,OM.OperatorName
				,UI.UserName AS UserName
				,--UI.UserName,                                  
				UI.UserTypeID
				,M.STATUS
				,UIS.UserID AS ParentID
				,UIS.UserName AS ParentName
				,UIS.UserTypeID AS DUserTypeID
				,ROW_NUMBER() OVER (
					PARTITION BY TD.RechargeID ORDER BY M.DOC DESC
					) [SR]
			FROM dbo.TransactionDetails(NOLOCK) TD
			INNER JOIN dbo.MoneyTransfer(NOLOCK) M ON M.[MoneyTransferID] = TD.RechargeID
				AND M.UserID = TD.UserID
				AND TD.ServiceID = @DMRServiceID
				AND TD.DOCTYPE = 'MoneyTransfer'
			INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
			INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
			INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON M.[DMRServiceID] = SM.ServiceID
			INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON M.[DMROperatorID] = OM.OperatorID
			WHERE LTRIM(RTRIM((M.BeneficiaryAccountNo))) = @ConsumerNo
				AND @UserID IN (
					TD.UserID
					,UI.ParentID
					)
			) AS tab3

		WHERE [SR] = 1
	) AS UnionBoth
		ORDER BY convert(DATETIME, DOC, 103) DESC
	END

	--FOS Recharge Report                                        
	IF (Upper(@userType) = Upper(@fos))
	BEGIN
		SELECT *
		FROM (
			SELECT *
			FROM (
				SELECT R.UserID
					,R.TransactionID
					,TD.RechargeID
					,TD.serviceID
					,TD.ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					--,convert(VARCHAR, R.DOC, 113) AS DOC            
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,R.IsDispute
					,R.OperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,-- UI.UserName,                                  
					UI.UserTypeID
					,R.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
						) [SR]
				FROM TransactionDetails(NOLOCK) TD
				INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID
					AND R.UserID = TD.UserID
					AND R.ServiceID = TD.ServiceID
				INNER JOIN UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
				INNER JOIN OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
				WHERE (R.ConsumerNumber = @ConsumerNo)
					AND UI.ParentFOS = @UserID
				) AS tab4
			WHERE [SR] = 1
			
			UNION ALL
			
			SELECT *
			FROM (
				SELECT M.UserID
					,M.[ProviderOprID] AS TransactionID
					,TD.RechargeID
					,TD.serviceID
					,M.BeneficiaryAccountNo AS ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					--,convert(VARCHAR, R.DOC, 113) AS DOC            
					,convert(VARCHAR(12), M.DOC, 113) + right(convert(VARCHAR(39), M.DOC, 22), 11) AS DOC
					--,M.IsDispute            
					,'' AS IsDispute
					,M.[DMROperatorID]
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,--UI.UserName,                                  
					UI.UserTypeID
					,M.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY M.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.MoneyTransfer(NOLOCK) M ON M.[MoneyTransferID] = TD.RechargeID
					AND M.UserID = TD.UserID
					AND TD.ServiceID = @DMRServiceID
					AND TD.DOCTYPE = 'MoneyTransfer'
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON M.[DMRServiceID] = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON M.[DMROperatorID] = OM.OperatorID
				WHERE LTRIM(RTRIM((M.BeneficiaryAccountNo))) = @ConsumerNo
				) DMRTab
			WHERE [SR] = 1
			) AS UnionBoth
		ORDER BY convert(DATETIME, DOC, 103) DESC
	END

	--Retailer,Customer,APIUser Recharge Report                                        
	IF (
			Upper(@userType) = Upper(@retailer)
			OR Upper(@userType) = Upper(@Customer)
			OR Upper(@userType) = Upper(@apiUser)
			)
	BEGIN
		SELECT *
		FROM (
			SELECT *
			FROM (
				SELECT R.UserID
					,R.TransactionID
					,TD.RechargeID
					,TD.serviceID
					,TD.ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					--,convert(VARCHAR, R.DOC, 113) AS DOC            
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,R.IsDispute
					,R.OperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,-- UI.UserName,                                  
					UI.UserTypeID
					,R.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
						) [SR]
				FROM TransactionDetails(NOLOCK) TD
				INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID
					AND R.UserID = TD.UserID
					AND R.ServiceID = TD.ServiceID
				INNER JOIN UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
				INNER JOIN OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
				WHERE (R.ConsumerNumber = @ConsumerNo)
					AND TD.UserID = @UserID
				) AS tab4
			WHERE [SR] = 1
			
			UNION ALL
			
			SELECT *
			FROM (
				SELECT M.UserID
					,M.[ProviderOprID] AS TransactionID
					,TD.RechargeID
					,TD.serviceID
					,M.BeneficiaryAccountNo AS ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					--,convert(VARCHAR, R.DOC, 113) AS DOC            
					,convert(VARCHAR(12), M.DOC, 113) + right(convert(VARCHAR(39), M.DOC, 22), 11) AS DOC
					--,M.IsDispute            
					,'' AS IsDispute
					,M.DMROperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,--UI.UserName,                                  
					UI.UserTypeID
					,M.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY M.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.MoneyTransfer(NOLOCK) M ON M.[MoneyTransferID] = TD.RechargeID
					AND M.UserID = TD.UserID
					AND TD.ServiceID = @DMRServiceID
					AND TD.DOCTYPE = 'MoneyTransfer'
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON M.[DMRServiceID] = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON M.[DMROperatorID] = OM.OperatorID
				WHERE LTRIM(RTRIM((M.BeneficiaryAccountNo))) = @ConsumerNo
				) DMRTab
			WHERE [SR] = 1
			) AS UnionBoth
		ORDER BY convert(DATETIME, DOC, 103) DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectByRequestID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[RechargeSelectByRequestID] @RechargeID INT  
 ,@ServiceID INT  
AS  
BEGIN  
 IF (  
   @ServiceID IN (  
    1  
    ,2  
    ,3  
    ,4  
    ,6  
    ,14  
    ,19  
    ,20  
    )  
   ) --Recharge  
 BEGIN  
  SELECT [status]  
   ,[ConsumerNumber]  
   ,[Amount]  
   ,[RequestID]  
   ,[TransactionID]  
  FROM Recharge(NOLOCK)  
  WHERE rechargeID = @RechargeID  
 END  
 ELSE IF (@ServiceID = 21) --DMR  
 BEGIN  
  SELECT [status]  
   ,[BeneficiaryAccountNo] AS ConsumerNumber  
   ,[Amount]  
   ,[ProviderRequestID]  
   ,[ProviderOprID]  
  FROM [dbo].[MoneyTransfer](NOLOCK)  
  WHERE [MoneyTransferID] = @RechargeID  
 END  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectByTimeDesc]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROC [dbo].[RechargeSelectByTimeDesc]   
 @RechargeProviderID INT    
 ,@OperatorID INT    
 ,@CheckTimeInMin INT    
AS    
BEGIN    
 PRINT ' @CheckTimeInMin:' + convert(VARCHAR, @CheckTimeInMin)    
  
 SELECT TOP 100 STATUS   
  ,DOC    
 FROM Recharge    
 WHERE RechargeProviderID = @RechargeProviderID    
  AND OperatorID = @OperatorID    
  AND DOC > DATEADD(minute, - @CheckTimeInMin, GETDATE())    
 ORDER BY DOC DESC    
  
END 
GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectDatewise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================                              
--Auther :Priyanka                              
--Date :27-2-17                              
--Purpose Recharge report                              
--==========================================                              
--[[RechargeSelectDatewise]] 3                              
-- [RechargeSelectDatewise] 1384,'20/08/2018','20/08/2018' ,0,100,0        
-- [RechargeSelectDatewise] 1,'27/10/2018','27/10/2018' ,0,10000,0 ,0        
CREATE PROCEDURE [dbo].[RechargeSelectDatewise] @UserID INT
	,@FromDate VARCHAR(100)
	,@ToDate VARCHAR(100)
	,@RechargeProviderID INT
	,@TopRecords INT = 100
	,@OperatorId INT = 0
	,@SelectedUserID INT = 0
AS
BEGIN
	DECLARE @UserTypeID INT;
	DECLARE @retailer VARCHAR(20) = 'RETAILER';
	DECLARE @Customer VARCHAR(20) = 'Customer';
	DECLARE @distributor VARCHAR(20) = 'DISTRIBUTOR';
	DECLARE @SuperDistributor VARCHAR(20) = 'SuperDistributor';
	DECLARE @admin VARCHAR(20) = 'ADMIN';
	DECLARE @apiUser VARCHAR(20) = 'APIUSER';
	DECLARE @Support VARCHAR(20) = 'SUPPORT';
	DECLARE @fos VARCHAR(20) = 'FOS';
	DECLARE @userType VARCHAR(20);
	DECLARE @TopRecordsTemp INT;

	SET @TopRecordsTemp = @TopRecords;

	SELECT @userType = UTM.UserType
	FROM [dbo].UserInformation(NOLOCK) UI
	INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @UserID

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM dbo.UserInformation(NOLOCK) 
			WHERE UserID = @UserID
			)

	PRINT (@userType)

	--Admin Recharge Report                              
	IF (
			Upper(@userType) = Upper(@admin)
			OR Upper(@userType) = Upper(@Support)
			)
	BEGIN
		PRINT '@admin '
		PRINT @admin

		IF (@RechargeProviderID = 0)
		BEGIN
			PRINT '@RechargeProviderID '
			PRINT @RechargeProviderID

			IF (@OperatorId = 0)
			BEGIN
				PRINT '@OperatorId '
				PRINT @OperatorId

				SELECT TOP (@TopRecordsTemp) *
				FROM (
					SELECT R.UserID
						,R.TransactionID
						,TD.RechargeID
						,TD.serviceID
						,TD.ConsumerNumber
						,TD.DOCType
						,TD.OpeningBalance
						,TD.ClosingBalance
						,TD.TransactionAmount AS Amount
						,TD.DiscountAmount
						,TD.AmountCreditDebit
						,TD.CommissionCreditDebit
						,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
						,R.[RechargeDescription]
						,R.IsDispute
						,R.OperatorID
						,SM.ServiceName
						,OM.OperatorName
						,UI.UserName AS UserName
						,UI.UserTypeID
						,R.STATUS
						,UIS.UserID AS ParentID
						,UIS.UserName AS ParentName
						,UIS.UserTypeID AS DUserTypeID
						,TD.TransactionNumberID
						,ROW_NUMBER() OVER (
							PARTITION BY TD.RechargeID ORDER BY R.DOC DESC
							) [SR]
					FROM dbo.TransactionDetails(NOLOCK)  TD
					INNER JOIN dbo.Recharge(NOLOCK)  R ON R.RechargeID = TD.RechargeID
						AND R.UserID = TD.UserID
					INNER JOIN dbo.UserInformation(NOLOCK)  UI ON TD.UserID = UI.UserID
					INNER JOIN dbo.UserInformation(NOLOCK)  UIS ON UIS.UserID = UI.ParentID
					INNER JOIN dbo.ServiceMaster(NOLOCK)  SM ON R.ServiceID = SM.ServiceID
					INNER JOIN dbo.OperatorMaster(NOLOCK)  OM ON R.OperatorID = OM.OperatorID
					WHERE (
							convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
							AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
							)
						AND TD.DOCType IN (
							'RECHARGE'
							,'ElectricityBill'
							,'REFUND'
							)
						AND (
							R.UserID = @SelectedUserID
							OR @SelectedUserID = 0
							)
					) AS mainTAB1
				WHERE [SR] = 1
				ORDER BY TransactionNumberID DESC
			END
			ELSE
			BEGIN
				PRINT '@OperatorId '
				PRINT @OperatorId

				SELECT TOP (@TopRecordsTemp) *
				FROM (
					SELECT R.UserID
						,R.TransactionID
						,TD.RechargeID
						,TD.serviceID
						,TD.ConsumerNumber
						,TD.DOCType
						,TD.OpeningBalance
						,TD.ClosingBalance
						,TD.TransactionAmount AS Amount
						,TD.DiscountAmount
						,TD.AmountCreditDebit
						,TD.CommissionCreditDebit
						,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
						,R.[RechargeDescription]
						,R.IsDispute
						,R.OperatorID
						,SM.ServiceName
						,OM.OperatorName
						,UI.UserName AS UserName
						,UI.UserTypeID
						,R.STATUS
						,UIS.UserID AS ParentID
						,UIS.UserName AS ParentName
						,UIS.UserTypeID AS DUserTypeID
						,TD.TransactionNumberID
						,ROW_NUMBER() OVER (
							PARTITION BY TD.RechargeID ORDER BY R.DOC DESC
							) [SR]
					FROM dbo.TransactionDetails(NOLOCK) TD
					INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
						AND R.UserID = TD.UserID
					INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
					INNER JOIN dbo.UserInformation (NOLOCK) UIS ON UIS.UserID = UI.ParentID
					INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
					INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
					WHERE (
							convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
							AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
							AND R.OperatorID = @OperatorId
							)
						AND TD.DOCType IN (
							'RECHARGE'
							,'ElectricityBill'
							,'REFUND'
							)
						AND (
							R.UserID = @SelectedUserID
							OR @SelectedUserID = 0
							)
					) AS mainTAB1
				WHERE [SR] = 1
				ORDER BY TransactionNumberID DESC
			END
		END
				----Recharge Provider wise Report      
		ELSE
		BEGIN
			PRINT '@RechargeProviderID '
			PRINT @RechargeProviderID

			SELECT TOP (@TopRecordsTemp) *
			FROM (
				SELECT R.UserID
					,R.TransactionID
					,TD.RechargeID
					,TD.serviceID
					,TD.ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,R.[RechargeDescription]
					,R.IsDispute
					,R.OperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,
					--UI.UserName,                        
					UI.UserTypeID
					,R.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,TD.TransactionNumberID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY R.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
					AND R.UserID = TD.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
				WHERE (
						convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
						)
					AND TD.DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						,'REFUND'
						)
					AND RechargeProviderID = @RechargeProviderID
					AND (
						R.UserID = @SelectedUserID
						OR @SelectedUserID = 0
						)
				) AS mainTAB1
			WHERE [SR] = 1
			ORDER BY TransactionNumberID DESC
		END
	END

	--Distributor Recharge Report                              
	IF (Upper(@userType) = Upper(@distributor))
	BEGIN
		CREATE TABLE #TempUserIDForDist (TempUserID INT)

		--@SuperDistributor      
		INSERT INTO #TempUserIDForDist
		SELECT @UserID

		--@Distributor      
		INSERT INTO #TempUserIDForDist
		SELECT [UserID]
		FROM [dbo].[UserInformation]
		WHERE [ParentID] = @UserID

		--@Reatiler      
		INSERT INTO #TempUserIDForDist
		SELECT [UserID]
		FROM [dbo].[UserInformation]
		WHERE [ParentID] IN (
				SELECT TempUserID
				FROM #TempUserIDForDist
				)

		IF (@OperatorId = 0)
		BEGIN
			SELECT TOP (@TopRecordsTemp) *
			FROM (
				SELECT R.UserID
					,R.TransactionID
					,TD.RechargeID
					,TD.serviceID
					,TD.ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,R.[RechargeDescription]
					,R.IsDispute
					,R.OperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,UI.UserTypeID
					,R.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,TD.TransactionNumberID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
					AND R.UserID = TD.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
				WHERE (
						convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
						)
					AND TD.UserID IN (
						SELECT TempUserID
						FROM #TempUserIDForDist
						)
					AND TD.DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						,'REFUND'
						)
					AND (
						R.UserID = @SelectedUserID
						OR @SelectedUserID = 0
						)
				) AS tab3
			WHERE [SR] = 1
			ORDER BY TransactionNumberID DESC
		END
		ELSE
		BEGIN
			SELECT TOP (@TopRecordsTemp) *
			FROM (
				SELECT R.UserID
					,R.TransactionID
					,TD.RechargeID
					,TD.serviceID
					,TD.ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,R.[RechargeDescription]
					,R.IsDispute
					,R.OperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,UI.UserTypeID
					,R.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,TD.TransactionNumberID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
					AND R.UserID = TD.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
				WHERE (
						convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
						AND R.OperatorID = @OperatorId
						)
					AND @UserID IN (
						SELECT TempUserID
						FROM #TempUserIDForDist
						)
					AND TD.DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						,'REFUND'
						)
					AND (
						R.UserID = @SelectedUserID
						OR @SelectedUserID = 0
						)
				) AS tab3
			WHERE [SR] = 1
			ORDER BY TransactionNumberIDDESC
		END
	END

	--SuperDistributor Recharge Report                              
	IF (Upper(@userType) = Upper(@SuperDistributor))
	BEGIN
		CREATE TABLE #TempUserIDForSuperDist (TempUserID INT)

		--@SuperDistributor      
		INSERT INTO #TempUserIDForSuperDist
		SELECT @UserID

		--@Distributor      
		INSERT INTO #TempUserIDForSuperDist
		SELECT [UserID]
		FROM [dbo].[UserInformation]
		WHERE [ParentID] = @UserID

		--@Reatiler      
		INSERT INTO #TempUserIDForSuperDist
		SELECT [UserID]
		FROM [dbo].[UserInformation]
		WHERE [ParentID] IN (
				SELECT TempUserID
				FROM #TempUserIDForSuperDist
				)

		IF (@OperatorId = 0)
		BEGIN
			SELECT TOP (@TopRecordsTemp) *
			FROM (
				SELECT R.UserID
					,R.TransactionID
					,TD.RechargeID
					,TD.serviceID
					,TD.ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,R.[RechargeDescription]
					,R.IsDispute
					,R.OperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,UI.UserTypeID
					,R.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,TD.TransactionNumberID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
					AND R.UserID = TD.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
				WHERE (
						convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
						)
					AND TD.UserID IN (
						SELECT TempUserID
						FROM #TempUserIDForSuperDist
						)
					AND TD.DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						,'REFUND'
						)
					AND (
						R.UserID = @SelectedUserID
						OR @SelectedUserID = 0
						)
				) AS tab3
			WHERE [SR] = 1
			ORDER BY TransactionNumberID DESC
		END
		ELSE
		BEGIN
			SELECT TOP (@TopRecordsTemp) *
			FROM (
				SELECT R.UserID
					,R.TransactionID
					,TD.RechargeID
					,TD.serviceID
					,TD.ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,R.[RechargeDescription]
					,R.IsDispute
					,R.OperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,UI.UserTypeID
					,R.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,TD.TransactionNumberID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
					AND R.UserID = TD.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
				WHERE (
						convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
						AND R.OperatorID = @OperatorId
						)
					AND TD.UserID IN (
						SELECT TempUserID
						FROM #TempUserIDForSuperDist
						)
					AND TD.DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						,'REFUND'
						)
					AND (
						R.UserID = @SelectedUserID
						OR @SelectedUserID = 0
						)
				) AS tab3
			WHERE [SR] = 1
			ORDER BY TD.TransactionNumberID DESC
		END
	END

	IF (Upper(@userType) = Upper(@fos))
	BEGIN
		IF (@OperatorId = 0)
		BEGIN
			SELECT TOP (@TopRecordsTemp) *
			FROM (
				SELECT R.UserID
					,R.TransactionID
					,TD.RechargeID
					,TD.serviceID
					,TD.ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,R.[RechargeDescription]
					,R.IsDispute
					,R.OperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,
					-- UI.UserName,                        
					UI.UserTypeID
					,R.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,TD.TransactionNumberID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
					AND R.UserID = TD.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
				WHERE (
						convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
						)
					AND TD.DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						,'REFUND'
						)
					AND UI.ParentFOS = @UserID
					AND (
						R.UserID = @SelectedUserID
						OR @SelectedUserID = 0
						)
				) AS tab3
			WHERE [SR] = 1
			ORDER BY TransactionNumberID DESC
		END
		ELSE
		BEGIN
			SELECT TOP (@TopRecordsTemp) *
			FROM (
				SELECT R.UserID
					,R.TransactionID
					,TD.RechargeID
					,TD.serviceID
					,TD.ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,R.[RechargeDescription]
					,R.IsDispute
					,R.OperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,
					-- UI.UserName,                        
					UI.UserTypeID
					,R.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,TD.TransactionNumberID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
					AND R.UserID = TD.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
				WHERE (
						convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
						AND R.OperatorID = @OperatorId
						)
					AND TD.DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						,'REFUND'
						)
					AND UI.ParentFOS = @UserID
					AND (
						R.UserID = @SelectedUserID
						OR @SelectedUserID = 0
						)
				) AS tab3
			WHERE [SR] = 1
			ORDER BY TransactionNumberID DESC
		END
	END

	--Retailer,Customer,APIUser Recharge Report                              
	IF (
			Upper(@userType) = Upper(@retailer)
			OR Upper(@userType) = Upper(@Customer)
			OR Upper(@userType) = Upper(@apiUser)
			)
	BEGIN
		PRINT '@OperatorId '
		PRINT @OperatorId

		IF (@OperatorId = 0)
		BEGIN
			SELECT TOP (@TopRecordsTemp) *
			FROM (
				SELECT R.UserID
					,R.TransactionID
					,TD.RechargeID
					,TD.serviceID
					,TD.ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,R.[RechargeDescription]
					,R.IsDispute
					,R.OperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,
					-- UI.UserName,                        
					UI.UserTypeID
					,R.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,TD.TransactionNumberID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
					AND R.UserID = TD.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
				WHERE (
						convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
						)
					AND TD.UserID = @UserID
					AND TD.DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						,'REFUND'
						)
				) AS tab5
			WHERE [SR] = 1
			ORDER BY TransactionNumberID DESC
		END
		ELSE
		BEGIN
			SELECT TOP (@TopRecordsTemp) *
			FROM (
				SELECT R.UserID
					,R.TransactionID
					,TD.RechargeID
					,TD.serviceID
					,TD.ConsumerNumber
					,TD.DOCType
					,TD.OpeningBalance
					,TD.ClosingBalance
					,TD.TransactionAmount AS Amount
					,TD.DiscountAmount
					,TD.AmountCreditDebit
					,TD.CommissionCreditDebit
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,R.[RechargeDescription]
					,R.IsDispute
					,R.OperatorID
					,SM.ServiceName
					,OM.OperatorName
					,UI.UserName AS UserName
					,
					-- UI.UserName,                        
					UI.UserTypeID
					,R.STATUS
					,UIS.UserID AS ParentID
					,UIS.UserName AS ParentName
					,UIS.UserTypeID AS DUserTypeID
					,TD.TransactionNumberID
					,ROW_NUMBER() OVER (
						PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC
						) [SR]
				FROM dbo.TransactionDetails(NOLOCK) TD
				INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeID = TD.RechargeID
					AND R.UserID = TD.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UI ON TD.UserID = UI.UserID
				INNER JOIN dbo.UserInformation(NOLOCK) UIS ON UIS.UserID = UI.ParentID
				INNER JOIN dbo.ServiceMaster(NOLOCK) SM ON R.ServiceID = SM.ServiceID
				INNER JOIN dbo.OperatorMaster(NOLOCK) OM ON R.OperatorID = OM.OperatorID
				WHERE (
						convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
						AND R.OperatorID = @OperatorId
						)
					AND TD.DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						,'REFUND'
						)
					AND TD.UserID = @UserID
				) AS tab5
			WHERE [SR] = 1
			ORDER BY TransactionNumberID DESC
		END
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectDetalis]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[RechargeSelectDetalis] @RechargeId INT  
AS  
BEGIN  
 SELECT [ConsumerNumber]  
  ,[Amount]  
  ,[InboxID]  
  ,[RequestID]  
  ,[OperatorID]  
  ,ISNULL([Optional1],'') AS Optional1
  ,ISNULL([Optional2],'') AS Optional2
  ,ISNULL([Optional3],'') AS Optional3
  ,ISNULL([Optional4],'') AS Optional4
  ,ISNULL([Optional5],'') AS Optional5
  ,ISNULL([Optional6],'') AS Optional6
  ,ISNULL([CustomerMobileNo],'') AS [CustomerMobileNo]
 FROM [dbo].[Recharge](NOLOCK)  
 WHERE [RechargeID] = @RechargeID  
END  
  
  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectDuplicateRequestID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[RechargeSelectDuplicateRequestID] @RequestID VARCHAR(50)
	,@UserID INT
AS
BEGIN
	SELECT *
	FROM Recharge(NOLOCK)
	WHERE APIRequestID = @RequestID
		AND [UserID] = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectForAPIUser]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectForAPIUser] @RechargeID INT
AS
BEGIN
	DECLARE @apiUserTypeID INT = 4

	SELECT UI.UserID
		,R.ConsumerNumber
		,cast(R.Amount AS INT) AS Amount
		,R.STATUS
		,R.APIRequestID
		,R.TransactionID
		,R.RechargeID
		,UI.CallBackUrl
	FROM dbo.UserInformation(NOLOCK) AS UI
	INNER JOIN dbo.Recharge(NOLOCK) AS R ON UI.UserID = R.UserID
	WHERE UI.UserTypeID = @apiUserTypeID
		AND UI.CallBackUrl IS NOT NULL
		AND R.HitBack = 0
		AND R.RechargeID = @RechargeID
		AND UPPER(R.STATUS) != UPPER('PROCESS')
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectForMannualSuccessFail]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectForMannualSuccessFail]
AS
BEGIN
	SELECT DISTINCT TOP (1000) R.UserID
		,RPM.RechargeProviderName
		,R.RechargeID
		,R.ServiceID
		,R.ConsumerNumber
		,R.Amount
		,R.OpeningBalance
		,R.STATUS
		,UI.UserName AS Uname
		,R.IsDispute
		,OM.OperatorName
		,SM.ServiceName
		,R.DOC
		,R.OperatorID
		,UI.ParentID
		,TD.DiscountAmount
		,UI.UserTypeID
		,UI.EmailID
		,UI.MobileNumber
	FROM ServiceMaster SM
	INNER JOIN Recharge R ON SM.ServiceID = R.ServiceID
	INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID
	INNER JOIN UserInformation UI ON R.UserID = UI.UserID
	INNER JOIN RechargeProviderMaster RPM ON RPM.RechargeProviderID = R.RechargeProviderID
	INNER JOIN TransactionDetails TD ON R.RechargeID = TD.RechargeID
		AND R.ServiceID = TD.ServiceID
		AND R.UserID = TD.UserID
		AND R.UserID IN (
			SELECT UserID
			FROM Recharge
			)
	WHERE R.STATUS NOT IN ('REFUND')
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectLastTenRecord]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectLastTenRecord] @UserID INT  

AS  

BEGIN  

 DECLARE @UserTypeID INT;  

 DECLARE @retailer VARCHAR(20) = 'RETAILER';  

 DECLARE @Customer VARCHAR(20) = 'Customer';  

 DECLARE @distributor VARCHAR(20) = 'DISTRIBUTOR';  

 DECLARE @admin VARCHAR(20) = 'ADMIN';  

 DECLARE @apiUser VARCHAR(20) = 'APIUSER';  

 DECLARE @userType VARCHAR(20);  

  

 SELECT @userType = UTM.UserType  

 FROM [dbo].UserInformation(NOLOCK) UI  

 INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID  

 WHERE UI.UserID = @USERID  

  

 SET @UserTypeID = (  

   SELECT UserTypeID  

   FROM UserInformation  

   WHERE UserID = @UserID  

   )  

  

 --Admin Recharge Report            

 IF (Upper(@userType) = Upper(@admin))  

 BEGIN  

   SELECT *  

   FROM (  

    SELECT TOP 10 R.UserID  

     ,R.TransactionID  

     ,TD.RechargeID  

     ,TD.serviceID  

     ,TD.ConsumerNumber  

     ,TD.DOCType  

     ,TD.OpeningBalance  

     ,TD.ClosingBalance  

     ,TD.TransactionAmount AS Amount  

     ,TD.DiscountAmount  

     ,TD.AmountCreditDebit  

     ,TD.CommissionCreditDebit  

     ,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC  

     ,R.IsDispute  

     ,R.OperatorID  

     ,SM.ServiceName  

     ,OM.OperatorName  

     ,UI.ShopeName AS UserName  

     ,--UI.UserName,      

     UI.UserTypeID  

     ,R.STATUS  

     ,UIS.UserID AS ParentID  

     ,UIS.UserName AS ParentName  

     ,UIS.UserTypeID AS DUserTypeID  

     ,ROW_NUMBER() OVER (  

      PARTITION BY TD.RechargeID ORDER BY R.DOC DESC  

      ) [SR]  

    FROM TransactionDetails TD  

    INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID  

	 AND R.ServiceID = TD.ServiceID

     AND R.UserID = TD.UserID  

    INNER JOIN UserInformation UI ON TD.UserID = UI.UserID  

    INNER JOIN UserInformation UIS ON UIS.UserID = UI.ParentID  

    INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  

    INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID  

    ORDER BY convert(DATETIME, R.DOC, 103) DESC  

    ) t  

   WHERE [SR] = 1  

    ORDER BY  RechargeID DESC 
 -- ORDER BY  DOC DESC  

 END  

  

 --Distributor Recharge Report            

 IF (Upper(@userType) = Upper(@distributor))  

 BEGIN  

   SELECT *  

   FROM (  

    SELECT TOP 10 R.UserID  

     ,R.TransactionID  

     ,TD.RechargeID  

     ,TD.serviceID  

     ,TD.ConsumerNumber  

     ,TD.DOCType  

     ,TD.OpeningBalance  

     ,TD.ClosingBalance  

     ,TD.TransactionAmount AS Amount  

     ,TD.DiscountAmount  

     ,TD.AmountCreditDebit  

     ,TD.CommissionCreditDebit  

     ,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC  

     ,R.IsDispute  

     ,R.OperatorID  

     ,SM.ServiceName  

     ,OM.OperatorName  

     ,UI.ShopeName AS UserName  

     ,-- UI.UserName,      

     UI.UserTypeID  

     ,R.STATUS  

     ,UIS.UserID AS ParentID  

     ,UIS.UserName AS ParentName  

     ,UIS.UserTypeID AS DUserTypeID  

     ,ROW_NUMBER() OVER (  

      PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC  

      ) [SR]  

    FROM TransactionDetails TD  

    INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID 

	AND R.ServiceID = TD.ServiceID 

     AND R.UserID = TD.UserID  

    INNER JOIN UserInformation UI ON TD.UserID = UI.UserID  

    INNER JOIN UserInformation UIS ON UIS.UserID = UI.ParentID  

    INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  

    INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID  

    WHERE @UserID IN (  

      TD.UserID  

      ,UI.ParentID  

      )  

	      ) t  

   WHERE [SR] = 1   

    ORDER BY  RechargeID DESC 
 -- ORDER BY  DOC DESC  

 END  

  

 --Retailer,Customer Recharge Report            

 IF (  

   Upper(@userType) = Upper(@retailer)  

   OR Upper(@userType) = Upper(@Customer)  

   OR Upper(@userType) = Upper(@apiUser)  

   )  

 BEGIN  

   SELECT *  

   FROM (  

    SELECT TOP 10 R.UserID  

     ,R.TransactionID  

 ,TD.RechargeID  

     ,TD.serviceID  

     ,TD.ConsumerNumber  

     ,TD.DOCType  

     ,TD.OpeningBalance  

     ,TD.ClosingBalance  

     ,TD.TransactionAmount AS Amount  

     ,TD.DiscountAmount  

     ,TD.AmountCreditDebit  

     ,TD.CommissionCreditDebit  

     ,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC  

     ,R.IsDispute  

     ,R.OperatorID  

     ,SM.ServiceName  

     ,OM.OperatorName  

     ,UI.ShopeName AS UserName  

     ,-- UI.UserName,      

     UI.UserTypeID  

     ,R.STATUS  

     ,UIS.UserID AS ParentID  

     ,UIS.UserName AS ParentName  

     ,UIS.UserTypeID AS DUserTypeID  

     ,ROW_NUMBER() OVER (  

      PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC  

      ) [SR]  

    FROM TransactionDetails TD  

    INNER JOIN Recharge R ON R.RechargeID = TD.RechargeID  

	AND R.ServiceID = TD.ServiceID 

     AND R.UserID = TD.UserID  

    INNER JOIN UserInformation UI ON TD.UserID = UI.UserID  

    INNER JOIN UserInformation UIS ON UIS.UserID = UI.ParentID  

    INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  

    INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID  

    WHERE TD.UserID = @UserID  

    ORDER BY convert(DATETIME, R.DOC, 103) DESC  

    ) t  

   WHERE [SR] = 1    

   ORDER BY  RechargeID DESC 
 -- ORDER BY  DOC DESC  

 END  

END  



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectLastTwoThroughSMS]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--RechargeSelectLastTwoThroughSMS 241             
CREATE PROCEDURE[dbo].[RechargeSelectLastTwoThroughSMS] @UserID VARCHAR(20)
AS
BEGIN
	SELECT TOP 2 R.ConsumerNumber
		,R.Amount AS Amount
		,convert(VARCHAR, R.DOC, 109) AS DOC
		,OM.OperatorName
		,OM.OperatorCode
		,R.STATUS AS STATUS
	FROM Recharge R
	INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID
	WHERE R.UserID = @UserID
		AND R.Through = 'SMS'
	ORDER BY convert(DATETIME, R.DOC, 109) DESC
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectOperatorDateWise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [RechargeSelectOperatorDateWise] '1/10/2018','1/11/2018',3,1              
CREATE PROCEDURE [dbo].[RechargeSelectOperatorDateWise] @FromDate VARCHAR(100)
	,@Todate VARCHAR(100)
	,@UserID INT
	,@LoginUserID INT = 0
AS
BEGIN
	DECLARE @AdminType INT = 1;
	DECLARE @DistributorType INT = 2;
	DECLARE @SuperDistributorType INT = 8;
	DECLARE @TempRet TABLE (UserID INT)
	DECLARE @TempDist TABLE (UserID INT)
	DECLARE @UserType INT;

	SELECT @UserType = UserTypeID
	FROM UserInformation
	WHERE userID = @UserID

	PRINT @UserType

	IF (@UserType = @DistributorType)
	BEGIN
		INSERT INTO @TempRet (UserID)
		SELECT UserID
		FROM UserInformation
		WHERE ParentID = @UserID
	END
	ELSE IF (@UserType = @SuperDistributorType)
	BEGIN
		INSERT INTO @TempDist (UserID)
		SELECT UserID
		FROM UserInformation
		WHERE ParentID = @UserID

		INSERT INTO @TempRet (UserID)
		SELECT UserID
		FROM @TempDist

		INSERT INTO @TempRet (UserID)
		SELECT UI.UserID
		FROM @TempDist TD
		INNER JOIN UserInformation UI ON TD.UserID = UI.ParentID
	END
	ELSE
	BEGIN
		INSERT INTO @TempRet (UserID)
		SELECT @UserID
	END

	IF (@LoginUserID = @AdminType)
	BEGIN
		IF (@UserID != 0)
		BEGIN
			SELECT DISTINCT OperatorID
				,OperatorName
				,ServiceName
				,RechargeProviderID
				,RechargeProviderName
				,Sum(DiscountAmount) AS DiscountAmount
				,UserID
				,UserName
				,ServiceID
				,Count(Amount) AS Count
				,Sum(Amount) AS Amount
				,UserType
			FROM (
				SELECT R.OperatorID AS OperatorID
					,OM.OperatorName
					,SM.ServiceName
					,R.RechargeProviderID AS RechargeProviderID
					,RPM.RechargeProviderName
					,TD.DiscountAmount AS DiscountAmount
					,TD.UserID
					,UM.UserName
					,R.ServiceID
					,R.Amount AS Amount
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,UM.UserTypeID AS UserType
				FROM Recharge R
				INNER JOIN OperatorMaster OM ON OM.OperatorID = R.OperatorID
				INNER JOIN TransactionDetails TD ON TD.RechargeID = R.RechargeID
				INNER JOIN UserInformation UM ON UM.UserID = TD.UserID
				INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
				INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID
				WHERE upper(R.STATUS) = upper('SUCCESS')
					AND DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						)
					AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
					AND TD.UserID NOT IN (
						1
						,60
						)
					AND TD.UserID IN (
						SELECT UserID
						FROM @TempRet
						)
				
				UNION ALL
				
				SELECT R.[DMROperatorID] AS OperatorID
					,OM.OperatorName
					,SM.ServiceName
					,R.[DMRProviderID] AS RechargeProviderID
					,RPM.RechargeProviderName
					,TD.DiscountAmount AS DiscountAmount
					,TD.UserID
					,UM.UserName
					,R.[DMRServiceID] AS ServiceID
					,R.Amount AS Amount
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,UM.UserTypeID AS UserType
				FROM MoneyTransfer R
				INNER JOIN OperatorMaster OM ON OM.OperatorID = R.[DMROperatorID]
				INNER JOIN TransactionDetails TD ON TD.RechargeID = R.[MoneyTransferID]
				INNER JOIN UserInformation UM ON UM.UserID = TD.UserID
				INNER JOIN ServiceMaster SM ON R.[DMRServiceID] = SM.ServiceID
				INNER JOIN RechargeProviderMaster RPM ON R.[DMRProviderID] = RPM.RechargeProviderID
				WHERE upper(R.STATUS) = upper('SUCCESS')
					AND DOCType = ('MoneyTransfer')
					AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)           
					AND TD.UserID NOT IN (
						1
						,60
						)
					AND TD.UserID IN (
						SELECT UserID
						FROM @TempRet
						)
				) t
			GROUP BY ServiceName
				,RechargeProviderName
				,OperatorName
				,RechargeProviderID
				,OperatorID
				,ServiceID
				,UserID
				,UserName
				,UserID
				,UserType
			ORDER BY OperatorID DESC
		END
		ELSE
		BEGIN
			SELECT DISTINCT OperatorID
				,OperatorName
				,ServiceName
				,RechargeProviderID
				,RechargeProviderName
				,Sum(DiscountAmount) AS DiscountAmount
				,UserID
				,UserName
				,ServiceID
				,Count(Amount) AS Count
				,Sum(Amount) AS Amount
				,UserType
			FROM (
				SELECT R.OperatorID AS OperatorID
					,OM.OperatorName
					,SM.ServiceName
					,R.RechargeProviderID AS RechargeProviderID
					,RPM.RechargeProviderName
					,TD.DiscountAmount AS DiscountAmount
					,TD.UserID
					,UM.UserName
					,R.ServiceID
					,R.Amount AS Amount
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,UM.UserTypeID AS UserType
				FROM Recharge R
				INNER JOIN OperatorMaster OM ON OM.OperatorID = R.OperatorID
				INNER JOIN TransactionDetails TD ON TD.RechargeID = R.RechargeID
				INNER JOIN UserInformation UM ON UM.UserID = TD.UserID
				INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
				INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID
				WHERE upper(R.STATUS) = upper('SUCCESS')
					AND DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						)
					AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)            
					AND TD.UserID NOT IN (
						1
						,60
						)
				
				UNION ALL
				
				SELECT R.[DMROperatorID] AS OperatorID
					,OM.OperatorName
					,SM.ServiceName
					,R.[DMRProviderID] AS RechargeProviderID
					,RPM.RechargeProviderName
					,TD.DiscountAmount AS DiscountAmount
					,TD.UserID
					,UM.UserName
					,R.[DMRServiceID] AS ServiceID
					,R.Amount AS Amount
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
					,UM.UserTypeID AS UserType
				FROM MoneyTransfer R
				INNER JOIN OperatorMaster OM ON OM.OperatorID = R.[DMROperatorID]
				INNER JOIN TransactionDetails TD ON TD.RechargeID = R.[MoneyTransferID]
				INNER JOIN UserInformation UM ON UM.UserID = TD.UserID
				INNER JOIN ServiceMaster SM ON R.[DMRServiceID] = SM.ServiceID
				INNER JOIN RechargeProviderMaster RPM ON R.[DMRProviderID] = RPM.RechargeProviderID
				WHERE upper(R.STATUS) = upper('SUCCESS')
					AND DOCType = ('MoneyTransfer')
					AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)          
					AND TD.UserID NOT IN (
						1
						,60
						)             
				) t
			GROUP BY ServiceName
				,RechargeProviderName
				,OperatorName
				,RechargeProviderID
				,OperatorID
				,ServiceID
				,UserID
				,UserName
				,UserID
				,UserType
			ORDER BY OperatorID DESC
		END
	END
	ELSE
	BEGIN
		SELECT DISTINCT OperatorID
			,OperatorName
			,ServiceName
			,RechargeProviderID
			,RechargeProviderName
			,Sum(DiscountAmount) AS DiscountAmount
			,UserID
			,UserName
			,ServiceID
			,Count(Amount) AS Count
			,Sum(Amount) AS Amount
			,UserType
		FROM (
			SELECT R.OperatorID AS OperatorID
				,OM.OperatorName
				,SM.ServiceName
				,R.RechargeProviderID AS RechargeProviderID
				,RPM.RechargeProviderName
				,TD.DiscountAmount AS DiscountAmount
				,TD.UserID
				,UM.UserName
				,R.ServiceID
				,R.Amount AS Amount
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
				,UM.UserTypeID AS UserType
			FROM Recharge R
			INNER JOIN OperatorMaster OM ON OM.OperatorID = R.OperatorID
			INNER JOIN TransactionDetails TD ON TD.RechargeID = R.RechargeID
			INNER JOIN UserInformation UM ON UM.UserID = TD.UserID
			INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
			INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID
			WHERE upper(R.STATUS) = upper('SUCCESS')
				AND DOCType IN (
					'RECHARGE'
					,'ElectricityBill'
					)
				AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
				AND TD.UserID IN (
					SELECT UserID
					FROM @TempRet
					)
			
			UNION ALL
			
			SELECT R.[DMROperatorID] AS OperatorID
				,OM.OperatorName
				,SM.ServiceName
				,R.DMRProviderID AS RechargeProviderID
				,RPM.RechargeProviderName
				,TD.DiscountAmount AS DiscountAmount
				,TD.UserID
				,UM.UserName
				,R.DMRServiceID AS ServiceID
				,R.Amount AS Amount
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
				,UM.UserTypeID AS UserType
			FROM MoneyTransfer R
			INNER JOIN OperatorMaster OM ON OM.OperatorID = R.[DMROperatorID]
			INNER JOIN TransactionDetails TD ON TD.RechargeID = R.[MoneyTransferID]
			INNER JOIN UserInformation UM ON UM.UserID = TD.UserID
			INNER JOIN ServiceMaster SM ON R.DMRServiceID = SM.ServiceID
			INNER JOIN RechargeProviderMaster RPM ON R.DMRProviderID = RPM.RechargeProviderID
			WHERE upper(R.STATUS) = upper('SUCCESS')
				AND DOCType = ('MoneyTransfer')
				AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
				AND TD.UserID IN (
					SELECT UserID
					FROM @TempRet
					)
			) t
		GROUP BY ServiceName
			,RechargeProviderName
			,OperatorName
			,RechargeProviderID
			,OperatorID
			,ServiceID
			,UserID
			,UserName
			,UserID
			,UserType
		ORDER BY OperatorID DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectOperatorDateWiseDetail]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [RechargeSelectOperatorDateWiseDetail] '14/11/2018','14/11/2018',0,1                
CREATE PROCEDURE [dbo].[RechargeSelectOperatorDateWiseDetail] @FromDate VARCHAR(100)
	,@Todate VARCHAR(100)
	,@UserID INT
	,@LoginUserID INT = 0
AS
BEGIN
	DECLARE @AdminType INT = 1;
	DECLARE @DistributorType INT = 2;
	DECLARE @SuperDistributorType INT = 8;
	DECLARE @RetailerType INT = 3;
	DECLARE @APIUserType INT = 4;

	DECLARE @TempRet TABLE (UserID INT)
	DECLARE @TempDist TABLE (UserID INT)
	DECLARE @UserType INT;

	SELECT @UserType = UserTypeID
	FROM UserInformation
	WHERE userID = @LoginUserID

	PRINT @UserType

	IF (@UserType = @DistributorType)
	BEGIN
		INSERT INTO @TempRet (UserID)
		SELECT UserID
		FROM UserInformation
		WHERE ParentID = @UserID
	END
	ELSE IF (@UserType = @SuperDistributorType)
	BEGIN
		INSERT INTO @TempDist (UserID)
		SELECT UserID
		FROM UserInformation
		WHERE ParentID = @UserID

		INSERT INTO @TempRet (UserID)
		SELECT UserID
		FROM @TempDist

		INSERT INTO @TempRet (UserID)
		SELECT UI.UserID
		FROM @TempDist TD
		INNER JOIN UserInformation UI ON TD.UserID = UI.ParentID
	END
	ELSE
	BEGIN
		INSERT INTO @TempRet (UserID)
		SELECT @UserID
	END

	IF (@LoginUserID = @AdminType)
	BEGIN
		IF (@UserID != 0)
		BEGIN
			SELECT DISTINCT OperatorID
				,OperatorName
				,ServiceName
				,Sum(DiscountAmount) AS DiscountAmount
				,ServiceID
				,Count(Amount) AS Count
				,Sum(Amount) AS Amount
			FROM (
				SELECT R.OperatorID AS OperatorID
					,OM.OperatorName
					,SM.ServiceName
					,TD.DiscountAmount AS DiscountAmount
					,R.ServiceID
					,R.Amount AS Amount
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
				FROM Recharge R
				INNER JOIN OperatorMaster OM ON OM.OperatorID = R.OperatorID
				INNER JOIN TransactionDetails TD ON TD.RechargeID = R.RechargeID
				INNER JOIN UserInformation UM ON UM.UserID = TD.UserID
				INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
				INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID
				WHERE R.STATUS IN (
						'SUCCESS'
						--,'PROCESS'                
						)
					AND DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						)
					AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
					--and TD.UserID != 1                
					AND TD.UserID NOT IN (
						1
						,60
						)
					AND TD.UserID IN (
						SELECT UserID
						FROM @TempRet
						)
				
				UNION
				
				SELECT R.[DMROperatorID] AS OperatorID
					,OM.OperatorName
					,SM.ServiceName
					,TD.DiscountAmount AS DiscountAmount
					,R.[DMRServiceID] AS ServiceID
					,R.Amount AS Amount
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
				FROM MoneyTransfer R
				INNER JOIN OperatorMaster OM ON OM.OperatorID = R.[DMROperatorID]
				INNER JOIN TransactionDetails TD ON TD.RechargeID = R.[MoneyTransferID]
				INNER JOIN UserInformation UM ON UM.UserID = TD.UserID
				INNER JOIN ServiceMaster SM ON R.[DMRServiceID] = SM.ServiceID
				INNER JOIN RechargeProviderMaster RPM ON R.[DMRProviderID] = RPM.RechargeProviderID
				WHERE R.STATUS IN (
						'SUCCESS'
						--,'PROCESS'                
						)
					AND DOCType = ('MoneyTransfer')
					AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
					--and TD.UserID != 1                
					AND TD.UserID NOT IN (
						1
						,60
						)
					AND TD.UserID IN (
						SELECT UserID
						FROM @TempRet
						)
				) t
			GROUP BY ServiceName
				,OperatorName
				,OperatorID
				,ServiceID
			ORDER BY OperatorID DESC
		END
		ELSE
		BEGIN
			SELECT DISTINCT OperatorID
				,OperatorName
				,ServiceName
				,Sum(DiscountAmount) AS DiscountAmount
				,ServiceID
				,Count(Amount) AS Count
				,Sum(Amount) AS Amount
			FROM (
				SELECT R.OperatorID AS OperatorID
					,OM.OperatorName
					,SM.ServiceName
					,TD.DiscountAmount AS DiscountAmount
					,R.ServiceID
					,R.Amount AS Amount
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
				FROM Recharge R
				INNER JOIN OperatorMaster OM ON OM.OperatorID = R.OperatorID
				INNER JOIN TransactionDetails TD ON TD.RechargeID = R.RechargeID
				INNER JOIN UserInformation UM ON UM.UserID = TD.UserID
				INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
				INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID
				WHERE R.STATUS IN (
						'SUCCESS'
						--,'PROCESS'                
						)
					AND DOCType IN (
						'RECHARGE'
						,'ElectricityBill'
						)
					AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)              
					AND TD.UserID NOT IN (
						1
						,60
						)
					AND (UM.UserTypeID =@RetailerType OR UM.UserTypeID = @APIUserType)

				UNION
				
				SELECT R.[DMROperatorID] AS OperatorID
					,OM.OperatorName
					,SM.ServiceName
					,TD.DiscountAmount AS DiscountAmount
					,R.[DMRServiceID] AS ServiceID
					,R.Amount AS Amount
					,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
				FROM MoneyTransfer R
				INNER JOIN OperatorMaster OM ON OM.OperatorID = R.[DMROperatorID]
				INNER JOIN TransactionDetails TD ON TD.RechargeID = R.[MoneyTransferID]
				INNER JOIN UserInformation UM ON UM.UserID = TD.UserID
				INNER JOIN ServiceMaster SM ON R.[DMRServiceID] = SM.ServiceID
				INNER JOIN RechargeProviderMaster RPM ON R.[DMRProviderID] = RPM.RechargeProviderID
				WHERE R.STATUS IN (
						'SUCCESS'
						--,'PROCESS'                
						)
					AND DOCType = ('MoneyTransfer')
					AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
					--and TD.UserID != 1                
					AND TD.UserID NOT IN (
						1
						,60
						)
					AND (UM.UserTypeID =@RetailerType OR UM.UserTypeID = @APIUserType)
				) t
			GROUP BY ServiceName
				,OperatorID
				,OperatorName
				,ServiceID
			ORDER BY OperatorID DESC
		END
	END
	ELSE
	BEGIN
		SELECT DISTINCT OperatorID
			,OperatorName
			,ServiceName
			,Sum(DiscountAmount) AS DiscountAmount
			,ServiceID
			,Count(Amount) AS Count
			,Sum(Amount) AS Amount
		FROM (
			SELECT R.OperatorID AS OperatorID
				,OM.OperatorName
				,SM.ServiceName
				,TD.DiscountAmount AS DiscountAmount
				,R.ServiceID
				,R.Amount AS Amount
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
			FROM Recharge R
			INNER JOIN OperatorMaster OM ON OM.OperatorID = R.OperatorID
			INNER JOIN TransactionDetails TD ON TD.RechargeID = R.RechargeID
			INNER JOIN UserInformation UM ON UM.UserID = TD.UserID
			INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
			INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID
			WHERE R.STATUS IN (
					'SUCCESS'
					--,'PROCESS'                
					)
				AND DOCType IN (
					'RECHARGE'
					,'ElectricityBill'
					)
				AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
				AND TD.UserID IN (
					SELECT UserID
					FROM @TempRet
					)
			
			UNION
			
			SELECT R.[DMROperatorID] AS OperatorID
				,OM.OperatorName
				,SM.ServiceName
				,TD.DiscountAmount AS DiscountAmount
				,R.DMRServiceID AS ServiceID
				,R.Amount AS Amount
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
			FROM MoneyTransfer R
			INNER JOIN OperatorMaster OM ON OM.OperatorID = R.[DMROperatorID]
			INNER JOIN TransactionDetails TD ON TD.RechargeID = R.[MoneyTransferID]
			INNER JOIN UserInformation UM ON UM.UserID = TD.UserID
			INNER JOIN ServiceMaster SM ON R.DMRServiceID = SM.ServiceID
			INNER JOIN RechargeProviderMaster RPM ON R.DMRProviderID = RPM.RechargeProviderID
			WHERE R.STATUS IN (
					'SUCCESS'
					--,'PROCESS'                
					)
				AND DOCType = ('MoneyTransfer')
				AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
				AND TD.UserID IN (
					SELECT UserID
					FROM @TempRet
					)
			) t
		GROUP BY ServiceName
			,OperatorName
			,OperatorID
			,ServiceID
		ORDER BY OperatorID DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectOperatorIDAndServiceID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- RechargeSelectOperatorIDAndServiceID '2bff9f85-a3af-4f8e-a45f-42a4bafa92b6'
CREATE PROCEDURE[dbo].[RechargeSelectOperatorIDAndServiceID] @RequestId VARCHAR(200)  
 -- RechargeSelectOperatorIDAndServiceID '79ef9746873844e496b3'  
AS  
BEGIN  
 SELECT TOP (1) a.UserID  
  ,a.RechargeID  
  ,a.OperatorID  
  ,a.ServiceID  
  ,a.Amount  
  ,a.ConsumerNumber  
  ,b.UserTypeID  
  ,b.EmailID  
  ,b.UserName  
 FROM dbo.Recharge(NOLOCK) AS A  
 INNER JOIN dbo.userinformation(NOLOCK) AS B ON a.UserID = b.UserID  
 WHERE a.RequestId = @RequestId  
 
 UNION ALL

 SELECT TOP (1) a.UserID  
  ,a.MoneyTransferID As RechargeID  
  ,a.DMROperatorID As OperatorID  
  ,a.DMRServiceID As ServiceID  
  ,a.Amount  
  ,a.BeneficiaryAccountNo As ConsumerNumber  
  ,b.UserTypeID  
  ,b.EmailID  
  ,b.UserName  
 FROM dbo.MoneyTransfer(NOLOCK) AS A  
 INNER JOIN dbo.userinformation(NOLOCK) AS B ON a.UserID = b.UserID  
 WHERE a.ProviderRequestID = @RequestId  
 
END  



  
  
GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectOperatorIDAndServiceIDByRechargeID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectOperatorIDAndServiceIDByRechargeID] @RechargeID INT  
AS  
BEGIN  
 SELECT A.[UserID]  
  ,[RechargeID]  
  ,[OperatorID]  
  ,[ServiceID]  
  ,[Amount]  
  ,[ConsumerNumber]  
  ,RequestID  
  ,B.UserTypeID
 FROM [dbo].[Recharge](NOLOCK) As A
  INNER JOIN dbo.userinformation(NOLOCK) AS B ON a.UserID = b.UserID    
 WHERE [RechargeID] = @RechargeID  


 --UNION ALL  
  
 --SELECT TOP (1) a.UserID    
 -- ,a.MoneyTransferID As RechargeID    
 -- ,a.DMROperatorID As OperatorID    
 -- ,a.DMRServiceID As ServiceID    
 -- ,a.Amount    
 -- ,a.BeneficiaryAccountNo As ConsumerNumber
 -- ,a.ProviderRequestID
 -- ,b.UserTypeID    
 --FROM dbo.MoneyTransfer(NOLOCK) AS A    
 --INNER JOIN dbo.userinformation(NOLOCK) AS B ON a.UserID = b.UserID    
 --WHERE a.MoneyTransferID =  @RechargeID  

END  
GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectOperatorWise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RechargeSelectOperatorWise] @FromDate DATETIME = NULL
	,@Todate DATETIME = NULL
	,@UserID INT
AS
BEGIN
	IF (@FromDate IS NULL)
		SET @FromDate = getdate()

	IF (@Todate IS NULL)
		SET @Todate = getdate()

	IF (@UserID = 1)
	BEGIN
		SELECT R.OperatorID
			,OM.OperatorName
			,SM.ServiceName
			,R.RechargeProviderID
			,RPM.RechargeProviderName
			,R.ServiceID
			,sum(R.Amount) AS Amount
		FROM Recharge R
		INNER JOIN OperatorMaster OM ON OM.OperatorID = R.OperatorID
		INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
		INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID
		WHERE R.STATUS IN (
				'SUCCESS'
				,'PROCESS'
				)
			AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
			AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
		GROUP BY SM.ServiceName
			,RPM.RechargeProviderName
			,OM.OperatorName
			,R.RechargeProviderID
			,R.OperatorID
			,R.ServiceID
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectPending]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[RechargeSelectPending] 23          
CREATE PROCEDURE[dbo].[RechargeSelectPending] @RechargeProviderID INT
AS
BEGIN
	DECLARE @RechargeId INT

	SELECT TOP (1) @RechargeId = RC.RechargeID
	FROM dbo.RECHARGE RC
	INNER JOIN RechargeProviderOperatorMaster ROM ON RC.OperatorID = ROM.OperatorID
		AND RC.RechargeProviderID = ROM.RechargeProviderID
	-- AND AOM.Status <>'Off'      
	WHERE RC.STATUS IN (
			'PROCESS'
			,'PENDING'
			)
		AND RC.RechargeProviderID = @RechargeProviderID
		AND ROM.RechargeProviderID = @RechargeProviderID
		AND RC.IsProcess = 0
		AND MONTH(RC.DOC) = MONTH(getdate())
		AND YEAR(RC.DOC) = YEAR(getdate())
		AND DAY(RC.DOC) = DAY(getdate())

	SELECT RC.ConsumerNumber AS Number
		,convert(INT, RC.Amount) AS Amount
		,ROM.Opcode AS OperatorCode
		,RC.RequestID AS RequestID
		,RC.DOC AS RechargeDateTime
	FROM dbo.RECHARGE RC
	INNER JOIN RechargeProviderOperatorMaster ROM ON RC.OperatorID = ROM.OperatorID
		AND RC.RechargeProviderID = ROM.RechargeProviderID
	-- AND AOM.Status <>'Off'      
	WHERE RC.RechargeID = @RechargeId
		AND RC.STATUS IN (
			'PROCESS'
			,'PENDING'
			)
		AND RC.RechargeProviderID = @RechargeProviderID
		AND ROM.RechargeProviderID = @RechargeProviderID
		AND RC.IsProcess = 0
		AND MONTH(RC.DOC) = MONTH(getdate())
		AND YEAR(RC.DOC) = YEAR(getdate())
		AND DAY(RC.DOC) = DAY(getdate())

	UPDATE dbo.Recharge
	SET IsProcess = 1
	WHERE RechargeID = @RechargeId
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectPendingOrFailCount]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[RechargeSelectPendingOrFailCount] @RechargeProviderID INT
	,@OperatorID INT
	,@CheckTimeInMin INT
	,@expectedCount INT
	,@expectedStatus VARCHAR(100) = 'REFUND'
	,@OUTPUT INT OUT
AS
BEGIN
	PRINT '	@CheckTimeInMin:' + convert(VARCHAR, @CheckTimeInMin)

	IF (@CheckTimeInMin <= 0)
	BEGIN
		SET @OUTPUT = 10000

		PRINT @OUTPUT

		RETURN @OUTPUT
	END

	create  TABLE #tbPendingCount (
		CurrentStatus VARCHAR(100)
		,PreviousStatus VARCHAR(100)
		,DOC DATETIME
		);

	INSERT INTO #tbPendingCount (
		CurrentStatus
		,DOC
		,PreviousStatus
		)
	SELECT STATUS AS CurrentStatus
		,DOC
		,LAG(STATUS, 1, @expectedStatus) OVER (
			ORDER BY DOC DESC
			) AS PreviousStatus
	FROM Recharge
	WHERE RechargeProviderID = @RechargeProviderID
		AND OperatorID = @OperatorID
		AND DOC > DATEADD(minute, - @CheckTimeInMin, GETDATE())
	ORDER BY DOC DESC

	DECLARE @actualCount INT = 0

	SELECT @actualCount = SUM(Flag)
	FROM (
		SELECT TOP (@expectedCount) Flag
		FROM (
			SELECT CurrentStatus
				,PreviousStatus
				,DOC
				,CASE 
					WHEN CurrentStatus = PreviousStatus
						AND upper(CurrentStatus) = upper(@expectedStatus)
						THEN 1
					ELSE 0
					END AS Flag
			FROM #tbPendingCount
			) AS MainTable
		) AS AggerTable

	SET @OUTPUT = (
			SELECT @actualCount AS ActualCount
			)

	PRINT @OUTPUT

	DROP TABLE #tbPendingCount

	RETURN @OUTPUT
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectProviderDateWiseTotalSales]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[RechargeSelectProviderDateWiseTotalSales] '02/02/2019','02/02/2019','Date'  
CREATE PROCEDURE [dbo].[RechargeSelectProviderDateWiseTotalSales] @FromDate VARCHAR(100)
	,@ToDate VARCHAR(100)
	,@Type VARCHAR(100)
AS
BEGIN
	DECLARE @TypeStatus VARCHAR(20)

	SET @TypeStatus = @Type;

	IF (@TypeStatus = 'Total')
	BEGIN
		SELECT DISTINCT RechargeProviderID
			,Count
			,Amount
			,Commission
			,RechargeProviderName
			,RechargeProviderOpeningBalance
			,RechargeProviderClosingBalance
			,DOC
		FROM (
			SELECT RPM.RechargeProviderID
				,COUNT(R.RechargeID) AS Count
				,IsNull(Sum(R.Amount), 0.00) AS Amount
				,IsNull(sum(RechargeProviderCommission), 0.00) AS Commission
				,RPM.RechargeProviderName
				,R.RechargeProviderOpeningBalance
				,IsNull(sum(R.RechargeProviderClosingBalance), 0.00) AS RechargeProviderClosingBalance
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
			FROM RechargeProviderMaster RPM
			LEFT JOIN Recharge R ON RPM.RechargeProviderID = R.RechargeProviderID
				AND R.STATUS = 'SUCCESS'
				AND (
					convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
					)
			GROUP BY RPM.RechargeProviderID
				,RPM.RechargeProviderName
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11)
				,R.RechargeProviderOpeningBalance
				,R.RechargeProviderClosingBalance
			
			UNION ALL
			
			SELECT RPM.RechargeProviderID
				,COUNT(R.[MoneyTransferID]) AS Count
				,IsNull(Sum(R.Amount), 0.00) AS Amount
				,IsNull(sum([DMRCommissionn]), 0.00) AS Commission
				,RPM.RechargeProviderName
				,R.DMRProviderOpeningBalance AS RechargeProviderOpeningBalance
				,IsNull(sum(R.[DMRProviderClosingBalance]), 0.00) AS RechargeProviderClosingBalance
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
			FROM RechargeProviderMaster RPM
			LEFT JOIN [dbo].[MoneyTransfer] R ON RPM.RechargeProviderID = R.[DMRProviderID]
				AND R.STATUS = 'SUCCESS'
				AND (
					convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
					)
			GROUP BY RPM.RechargeProviderID
				,RPM.RechargeProviderName
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11)
				,R.DMRProviderOpeningBalance
				,R.[DMRProviderClosingBalance]
			) AS tab1
		WHERE Count != 0
		ORDER BY DOC DESC
	END

	IF (@TypeStatus = 'Date')
	BEGIN
		SELECT DISTINCT RechargeProviderID
			,Count
			,Amount
			,Commission
			,RechargeProviderName
			,DOC
			,RechargeProviderOpeningBalance
			,RechargeProviderClosingBalance
		FROM (
			SELECT RPM.RechargeProviderID
				,COUNT(1) AS Count
				,IsNull(Sum(R.Amount), 0.00) AS Amount
				,IsNull(sum(RechargeProviderCommission), 0.00) AS Commission
				,RPM.RechargeProviderName
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
				,R.RechargeProviderOpeningBalance
				,IsNull(sum(R.RechargeProviderClosingBalance), 0.00) AS RechargeProviderClosingBalance
			FROM RechargeProviderMaster RPM
			LEFT JOIN Recharge R ON RPM.RechargeProviderID = R.RechargeProviderID
				AND R.STATUS = 'SUCCESS'
			WHERE (
					convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
					)
			GROUP BY RPM.RechargeProviderID
				,RPM.RechargeProviderName
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11)
				,R.RechargeProviderOpeningBalance
				,R.RechargeProviderClosingBalance
			
			UNION ALL
			
			SELECT RPM.RechargeProviderID
				,COUNT(1) AS Count
				,IsNull(Sum(R.Amount), 0.00) AS Amount
				,IsNull(sum(DMRCommissionn), 0.00) AS Commission
				,RPM.RechargeProviderName
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC
				,R.DMRProviderOpeningBalance AS RechargeProviderOpeningBalance
				,IsNull(sum(R.[DMRProviderClosingBalance]), 0.00) AS RechargeProviderClosingBalance
			FROM RechargeProviderMaster RPM
			LEFT JOIN [dbo].[MoneyTransfer] R ON RPM.RechargeProviderID = R.[DMRProviderID]
				AND R.STATUS = 'SUCCESS'
			WHERE (
					convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)
					)
			GROUP BY RPM.RechargeProviderID
				,RPM.RechargeProviderName
				,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11)
				,R.DMRProviderOpeningBalance
				,R.[DMRProviderClosingBalance]
			) AS tab2
		ORDER BY DOC DESC
	END
END

GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectRetailerCommission]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectRetailerCommission] @UserID INT
	,@OperatorID INT
	,@TransactionAmount DECIMAL(18, 5)
	,@comAmt DECIMAL(18, 5) OUT
AS
BEGIN
	DECLARE @mType VARCHAR(50);
	DECLARE @UserGroupId INT

	SELECT @UserGroupId = GroupID
	FROM UserGroupMapping
	WHERE UserId = @UserID

	PRINT '@UserGroupId:' + convert(VARCHAR(30), @UserGroupId)

	IF (@UserGroupId IS NOT NULL)
	BEGIN
		SELECT @comAmt = RetailerCommissionAmount
			,@mType = Upper(MarginType)
		FROM [DBO].RechargeGroupCommission
		WHERE GroupID = @UserGroupId
			AND OperatorID = @OperatorID;
	END

	PRINT 'commission Amt:' + convert(VARCHAR(30), @comAmt)
	PRINT 'margin Type:' + convert(VARCHAR(30), @mType)

	IF (@comAmt IS NULL)
	BEGIN
		SELECT @comAmt = ISNULL(RetailerCommissionAmount, 0)
			,@mType = Upper(MarginType)
		FROM dbo.OperatorMaster(NOLOCK)
		WHERE OperatorID = @OperatorID
	END

	IF @mType = 'P'
	BEGIN
		SET @comAmt = (@comAmt * @TransactionAmount) / 100;
	END
	ELSE
	BEGIN
		SET @comAmt = @comAmt;
	END

	PRINT 'Final commission Amt:' + convert(VARCHAR(30), @comAmt)
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectSalesSummary]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select Distinct doctype from TransactionDetails
-- [RechargeSelectSalesSummary] '24/01/2019','24/01/2019' , 87,'0', 'Recharge'                         
CREATE PROCEDURE [dbo].[RechargeSelectSalesSummary] @FromDate VARCHAR(50)
	,@Todate VARCHAR(50)
	,@UserID INT = 0 -- For All Users                     
	,@Type VARCHAR(50) = 0
	,@BalanceType VARCHAR(1000) = 'Recharge'
AS
BEGIN
	DECLARE @UserTypeID INT;
	DECLARE @All INT = 0;
	DECLARE @Retailer INT = 3;
	DECLARE @Distributor INT = 2;
	DECLARE @SuperDistributor INT = 8;

	-- Services To be Considered in DMR 
	-- here DMR and Electricity
	Declare  @tempDMRServiceId table (ServiceID int )
	insert into @tempDMRServiceId values (14); -- Electricity
	insert into @tempDMRServiceId values (18); -- Balance Transfer
	insert into @tempDMRServiceId values (21); -- DMR
	
	-- Services removed from recharge
	Declare  @tempRechargeServiceId table (ServiceID int )
	insert into @temprechargeServiceId values (14); -- Electricity

	SELECT @UserTypeID = UserTypeID
	FROM Userinformation
	WHERE userID = @UserID

	SELECT UI.Userid
	INTO #tblUserID
	FROM userinformation UI
	WHERE UI.IsActive = 1
		AND UI.UserID NOT IN (
			60
			,1
			)
		AND (
			@UserID = 0
			OR UI.UserID = @UserID
			)

	IF (@BalanceType = 'DMT')
	BEGIN
		SELECT *
			,ISNULL(sales - refund, 0) AS ExactSale
			,((opbalance + receiveBalance + reverseBalance +commissionValue +refund)-
			(clbalance+transferBalance+reverseBalanceDeb+sales +GST+TDS)) As [Difference]
		FROM (
			SELECT DISTINCT UI.UserID
				,UI.UserName AS name
				,UI.MobileNumber AS mobileno
				,UIM.UserType AS ROLE
				,UIP.UserName AS upline
				,UIP.MobileNumber AS uplinemobileno
				,isnull((
						SELECT TOP (1) OpeningBalance
						FROM dbo.TransactionDetails(NOLOCK) TD
						WHERE UserID = UI.UserID
							AND upper(DOCType) IN (
								upper('MoneyTransfer')
								,UPPER('ElectricityBill')
								,upper('Balance Transfer DMT')
								,upper('Balance Receive DMT')
								,upper('Balance Reverse DMT')
								,upper('REFUND DMT')
								,upper('REVERSE DMT')
								,upper('REFUND')
								)
							AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
							AND ServiceID in (Select ServiceID from @tempDMRServiceId)
						ORDER BY TD.DOC ASC
						), isnull((
							SELECT TOP (1) ClosingBalance
							FROM dbo.TransactionDetails(NOLOCK) TD
							WHERE UserID = UI.UserID
								AND Convert(DATE, TD.DOC, 103) <= Convert(DATE, @Todate, 103)
								AND upper(DOCType) IN (
									upper('MoneyTransfer')
									,UPPER('ElectricityBill')
									,upper('Balance Transfer DMT')
									,upper('Balance Receive DMT')
									,upper('Balance Reverse DMT')
									,upper('REFUND DMT')
									,upper('REVERSE DMT')
									,upper('REFUND')
									)
								AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
								AND ServiceID in (Select ServiceID from @tempDMRServiceId)
							ORDER BY DOC DESC
							), (
							SELECT CurrentBalance
							FROM [dbo].[UserBalance]
							WHERE UserID = UI.UserID
							))) AS opbalance
				,isnull((
						SELECT TOP (1) ClosingBalance
						FROM dbo.TransactionDetails(NOLOCK) TD
						WHERE UserID = UI.UserID
							AND upper(DOCType) IN (
								upper('MoneyTransfer')
								,UPPER('ElectricityBill')
								,upper('Balance Transfer DMT')
								,upper('Balance Receive DMT')
								,upper('Balance Reverse DMT')
								,upper('REFUND DMT')
								,upper('REVERSE DMT')
								,upper('REFUND')
								)
							AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
							AND ServiceID in (Select ServiceID from @tempDMRServiceId)
						ORDER BY DOC DESC
						), isnull((
							SELECT TOP (1) ClosingBalance
							FROM dbo.TransactionDetails(NOLOCK) TD
							WHERE UserID = UI.UserID
								AND upper(DOCType) IN (
									upper('MoneyTransfer')
									,UPPER('ElectricityBill')
									,upper('Balance Transfer DMT')
									,upper('Balance Receive DMT')
									,upper('Balance Reverse DMT')
									,upper('REFUND DMT')
									,upper('REVERSE DMT')
									,upper('REFUND')
									)
								--AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)          
								AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
								AND ServiceID in (Select ServiceID from @tempDMRServiceId)
							ORDER BY DOC DESC
							), (
							SELECT CurrentBalance
							FROM [dbo].[UserBalance]
							WHERE UserID = UI.UserID
							))) AS clbalance
				,isnull((
						SELECT sum(TransactionAmount)
						FROM TransactionDetails AS TD
						WHERE UserID = UI.UserID
							AND upper(DOCType) IN (
								upper('Balance Transfer DMT')
								,upper('Balance Receive DMT')
								)
							AND AmountCreditDebit = 'CREDIT'
							AND Convert(DATE, TD.DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, TD.DOC, 103) <= Convert(DATE, @Todate, 103)
						), 0) AS receiveBalance
				,isnull((
						SELECT sum(TD.TransactionAmount)
						FROM TransactionDetails TD
						WHERE UserID = UI.UserID
							AND upper(DOCType) IN (upper('Balance Transfer DMT'))
							AND AmountCreditDebit = 'DEBIT'
							AND Convert(DATE, TD.DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, TD.DOC, 103) <= Convert(DATE, @Todate, 103)
						), 0) AS transferBalance
				,isnull((
						SELECT sum(TransactionAmount)
						FROM TransactionDetails TD
						WHERE UserID = UI.UserID
							--AND Type = 'RE'                       
							AND DOCType IN ('BALANCE REVERSE DMT')
							AND AmountCreditDebit IN ('CREDIT')
							AND Convert(DATE, TD.DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, TD.DOC, 103) <= Convert(DATE, @Todate, 103)
						), 0) AS reverseBalance
				,isnull((
						SELECT sum(TransactionAmount)
						FROM TransactionDetails TD
						WHERE UserID = UI.UserID
							--AND Type = 'RE'                       
							AND DOCType IN ('BALANCE REVERSE DMT')
							AND AmountCreditDebit IN ('DEBIT')
							AND Convert(DATE, TD.DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, TD.DOC, 103) <= Convert(DATE, @Todate, 103)
						), 0) AS reverseBalanceDeb
				,isnull((
						SELECT sum(TransactionAmount)
						FROM Transactiondetails TD
						WHERE UserID = UI.UserID
							AND upper(DOCType) in (upper('REFUND DMT'),upper('REFUND'))
							AND ServiceID in (Select ServiceID from @tempDMRServiceId)
							AND Convert(DATE, TD.DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, TD.DOC, 103) <= Convert(DATE, @Todate, 103)
						), 0) AS refund
				,(
					SELECT ISnull(sum(TransactionAmount), 0)
					FROM TransactionDetails
					WHERE UserID = UI.UserID
						AND upper([DOCType]) IN (upper('MoneyTransfer'), UPPER('ElectricityBill'))
						AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
						AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
					) AS sales
				,ISNULL(ISNULL((
							SELECT ISNULL(sum(isnull(DiscountAmount, 0)), 0)
							FROM TransactionDetails
							WHERE UserID = UI.UserID
								AND upper([DOCType]) IN (upper('MoneyTransfer'),UPPER('ElectricityBill'))
								--AND upper(DOCType) != upper('REFUND DMT')
								AND ServiceID in (Select ServiceID from @tempDMRServiceId)
								AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
								AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
							), 0) - (
						ISNULL((
								SELECT ISNULL(sum(isnull(DiscountAmount, 0)), 0)
								FROM TransactionDetails
								WHERE UserID = UI.UserID
									AND upper(DOCType) in (upper('REFUND DMT'),upper('REFUND'))
									AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
									AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
									AND ServiceID in (Select ServiceID from @tempDMRServiceId)
								), 0)
						), 0) AS commissionValue
				,ISNULL(ISNULL((
						SELECT ISNULL(sum(isnull(GST, 0)), 0)
						FROM TransactionDetails
						WHERE UserID = UI.UserID
							AND upper(DOCType) IN (
								upper('MoneyTransfer')
								,UPPER('ElectricityBill')
								,upper('Balance Transfer DMT')
								,upper('Balance Receive DMT')
								,upper('Balance Reverse DMT')
								,upper('REVERSE DMT')
								)
							AND upper(DOCType) != upper('REFUND DMT')
							AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
						), 0)- (
						ISNULL((
								SELECT ISNULL(sum(isnull(GST, 0)), 0)
								FROM TransactionDetails
								WHERE UserID = UI.UserID
									AND upper(DOCType) in ( upper('REFUND DMT'),upper('REFUND'))
									AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
									AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
									AND ServiceID in (Select ServiceID from @tempDMRServiceId)
								), 0)
						), 0) AS GST
				,ISNULL(ISNULL((
						SELECT ISNULL(sum(isnull(TDS, 0)), 0)
						FROM TransactionDetails
						WHERE UserID = UI.UserID
							AND upper(DOCType) IN (
								upper('MoneyTransfer')
								,UPPER('ElectricityBill')
								,upper('Balance Transfer DMT')
								,upper('Balance Receive DMT')
								,upper('Balance Reverse DMT')
								,upper('REVERSE DMT')
								)
							--AND upper(DOCType) != upper('REFUND DMT')
							AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
						), 0)-
						(
						ISNULL((
								SELECT ISNULL(sum(isnull(TDS, 0)), 0)
								FROM TransactionDetails
								WHERE UserID = UI.UserID
									AND upper(DOCType) in (upper('REFUND DMT'),upper('REFUND'))
									AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
									AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
									AND ServiceID in (Select ServiceID from @tempDMRServiceId)
								), 0)
						), 0) AS TDS
			FROM UserInformation UI(NOLOCK)
			INNER JOIN UserInformation UIP ON UIP.UserID = UI.ParentID
			INNER JOIN UserTypeMaster UIM ON UIM.UserTypeID = UI.UserTypeID
			INNER JOIN UserTypeMaster UIC ON UIC.UserTypeID = UIP.UserTypeID
			INNER JOIN #tblUserID TU ON TU.UserID = UI.UserID
			WHERE (
					@Type = @Distributor
					AND UI.UserTypeID = @Distributor
					)
				OR (@Type = @All)
				OR (
					@Type = @Retailer
					AND UI.UserTypeID = @Retailer
					)
				OR (
					@Type = @SuperDistributor
					AND UI.UserTypeID = @SuperDistributor
					)
			) AS Summary
		ORDER BY ExactSale DESC
	END

	IF (upper(@BalanceType) = upper('Recharge'))
	BEGIN
		SELECT *
			,ISNULL(sales - refund, 0) AS ExactSale
			,((opbalance + receiveBalance + reverseBalance +commissionValue +refund)-
			(clbalance+transferBalance+reverseBalanceDeb+sales +GST+TDS)) As [Difference]
		FROM (
			SELECT DISTINCT UI.UserID
				,UI.UserName AS name
				,UI.MobileNumber AS mobileno
				,UIM.UserType AS ROLE
				,UIP.UserName AS upline
				,UIP.MobileNumber AS uplinemobileno
				,isnull((
						SELECT TOP (1) OpeningBalance
						FROM dbo.TransactionDetails(NOLOCK) TD
						WHERE UserID = UI.UserID
							AND Upper(DOCType) IN (
								Upper('RECHARGE')
								,Upper('Balance Transfer')
								,Upper('Balance Receive')
								,upper('Balance Reverse')
								--,UPPER('ElectricityBill')
								,upper('REFUND')
								)
							AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
							AND ServiceID not in (Select ServiceID from @tempRechargeServiceId)
						ORDER BY TD.DOC ASC
						), isnull((
							SELECT TOP (1) ClosingBalance
							FROM dbo.TransactionDetails(NOLOCK) TD
							WHERE UserID = UI.UserID
								AND Upper(DOCType) IN (
									Upper('RECHARGE')
									--,UPPER('ElectricityBill')
									,Upper('Balance Transfer')
									,Upper('Balance Receive')
									,upper('Balance Reverse')
									,upper('REFUND')
									)
								AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
								AND ServiceID not in (Select ServiceID from @tempRechargeServiceId)
							ORDER BY DOC DESC
							), (
							SELECT CurrentBalance
							FROM [dbo].[UserBalance]
							WHERE UserID = UI.UserID
							))) AS opbalance
				,isnull((
						SELECT TOP (1) ClosingBalance
						FROM dbo.TransactionDetails(NOLOCK) TD
						WHERE UserID = UI.UserID
							AND DOCType IN (
								'RECHARGE'
								--,UPPER('ElectricityBill')
								,'Balance Transfer'
								,'Balance Receive'
								,upper('Balance Reverse')
								,upper('REFUND')
								)
							AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
							AND ServiceID not in (Select ServiceID from @tempRechargeServiceId)
						ORDER BY DOC DESC
						), isnull((
							SELECT TOP (1) ClosingBalance
							FROM dbo.TransactionDetails(NOLOCK) TD
							WHERE UserID = UI.UserID
								AND DOCType IN (
									'RECHARGE'
									--,UPPER('ElectricityBill')
									,'Balance Transfer'
									,'Balance Receive'
									,upper('Balance Reverse')
									,upper('REFUND')
									)     
								AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
								AND ServiceID not in (Select ServiceID from @tempRechargeServiceId)
							ORDER BY DOC DESC
							), (
							SELECT CurrentBalance
							FROM [dbo].[UserBalance]
							WHERE UserID = UI.UserID
							))) AS clbalance
				,isnull((
						SELECT sum(TransactionAmount)
						FROM TransactionDetails AS TD
						WHERE UserID = UI.UserID
							AND DOCType IN (
								'Balance Transfer'
								,'Balance Receive'
								)
							AND AmountCreditDebit = 'CREDIT'
							AND Convert(DATE, TD.DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, TD.DOC, 103) <= Convert(DATE, @Todate, 103)
						), 0) AS receiveBalance
				,isnull((
						SELECT sum(TransactionAmount)
						FROM TransactionDetails TD
						WHERE UserID = UI.UserID                  
							AND DOCType IN ('Balance Transfer')
							AND AmountCreditDebit = 'DEBIT'
							AND Convert(DATE, TD.DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, TD.DOC, 103) <= Convert(DATE, @Todate, 103)
						), 0) AS transferBalance
				,isnull((
						SELECT sum(TransactionAmount)
						FROM TransactionDetails TD
						WHERE UserID = UI.UserID                     
							AND DOCType IN ('Balance Reverse')
							AND AmountCreditDebit IN ('CREDIT')
							AND Convert(DATE, TD.DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, TD.DOC, 103) <= Convert(DATE, @Todate, 103)
						), 0) AS reverseBalance
				,isnull((
						SELECT sum(TransactionAmount)
						FROM TransactionDetails TD
						WHERE UserID = UI.UserID
							AND DOCType IN ('Balance Reverse')
							AND AmountCreditDebit IN ('DEBIT')
							AND Convert(DATE, TD.DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, TD.DOC, 103) <= Convert(DATE, @Todate, 103)
						), 0) AS reverseBalanceDeb
				,isnull((
						SELECT sum(TransactionAmount)
						FROM Transactiondetails TD
						WHERE UserID = UI.UserID
							AND DOCType = 'REFUND'
							AND Convert(DATE, TD.DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, TD.DOC, 103) <= Convert(DATE, @Todate, 103)
							AND ServiceID not in (Select ServiceID from @tempRechargeServiceId)
						), 0) AS refund
				,(
					SELECT ISnull(sum(TransactionAmount), 0)
					FROM TransactionDetails
					WHERE UserID = UI.UserID
						AND [DOCType] IN (
							--'ElectricityBill'
							'RECHARGE'
							)
						AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
						AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
						AND ServiceID not in (Select ServiceID from @tempRechargeServiceId)
					) AS sales
				,ISNULL(ISNULL((
							SELECT ISNULL(sum(isnull(DiscountAmount, 0)), 0)
							FROM TransactionDetails
							WHERE UserID = UI.UserID
								AND [DOCType] IN (
									--'ElectricityBill'
									'RECHARGE'
									,'DAILYRENTAL'
									)
								AND DOCType != 'REFUND'
								AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
								AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
								AND ServiceID not in (Select ServiceID from @tempRechargeServiceId)
							), 0) - (
						ISNULL((
								SELECT ISNULL(sum(isnull(DiscountAmount, 0)), 0)
								FROM TransactionDetails
								WHERE UserID = UI.UserID
									AND DOCType = 'REFUND'
									AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
									AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
									AND ServiceID not in (Select ServiceID from @tempRechargeServiceId)
								), 0)
						), 0) AS commissionValue
				,ISNULL(ISNULL((
						SELECT ISNULL(sum(isnull(GST, 0)), 0)
						FROM TransactionDetails
						WHERE UserID = UI.UserID
							AND Upper(DOCType) IN (
								Upper('RECHARGE')
								,Upper('Balance Transfer')
								,Upper('Balance Receive')
								,upper('Balance Reverse')
								--,UPPER('ElectricityBill')
								,upper('REFUND')
								)
							AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
							AND ServiceID not in (Select ServiceID from @tempRechargeServiceId)
						), 0)- (
						ISNULL((
								SELECT ISNULL(sum(isnull(GST, 0)), 0)
								FROM TransactionDetails
								WHERE UserID = UI.UserID
									AND upper(DOCType) in (upper('REFUND'))
									AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
									AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
									AND ServiceID not in (Select ServiceID from @tempRechargeServiceId)
								), 0)
						), 0) AS GST
				,ISNULL(ISNULL((
						SELECT ISNULL(sum(isnull(TDS, 0)), 0)
						FROM TransactionDetails
						WHERE UserID = UI.UserID
							AND Upper(DOCType) IN (
								Upper('RECHARGE')
								,Upper('Balance Transfer')
								,Upper('Balance Receive')
								,upper('Balance Reverse')
								--,UPPER('ElectricityBill')
								,upper('REFUND')
								)
							--AND upper(DOCType) != upper('REFUND DMT')
							AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
							AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
							AND ServiceID not in (Select ServiceID from @tempRechargeServiceId)
						), 0)-
						(
						ISNULL((
								SELECT ISNULL(sum(isnull(TDS, 0)), 0)
								FROM TransactionDetails
								WHERE UserID = UI.UserID
									AND upper(DOCType) in (upper('REFUND'))
									AND Convert(DATE, DOC, 103) >= Convert(DATE, @FromDate, 103)
									AND Convert(DATE, DOC, 103) <= Convert(DATE, @Todate, 103)
									AND ServiceID not in (Select ServiceID from @tempRechargeServiceId)
								), 0)
						), 0) AS TDS
			FROM UserInformation UI(NOLOCK)
			INNER JOIN UserInformation UIP ON UIP.UserID = UI.ParentID
			INNER JOIN UserTypeMaster UIM ON UIM.UserTypeID = UI.UserTypeID
			INNER JOIN UserTypeMaster UIC ON UIC.UserTypeID = UIP.UserTypeID
			INNER JOIN #tblUserID TU ON TU.UserID = UI.UserID
			WHERE (
					@Type = @Distributor
					AND UI.UserTypeID = @Distributor
					)
				OR (@Type = @All)
				OR (
					@Type = @Retailer
					AND UI.UserTypeID = @Retailer
					)
				OR (
					@Type = @SuperDistributor
					AND UI.UserTypeID = @SuperDistributor
					)
			) AS Summary
		ORDER BY ExactSale DESC
	END
END


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectStatus]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure for resend recharge 
CREATE PROCEDURE[dbo].[RechargeSelectStatus] @RechargeID INT
AS
BEGIN
	SELECT [Status]
	FROM [dbo].[Recharge](NOLOCK)
	WHERE [RechargeID] = @RechargeID
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectStatusCount]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectStatusCount]  
AS  
BEGIN  
 SELECT IsNull(Sum(Success), 0) AS Success  
  ,IsNull(Sum(SuccessAmount), 0) AS SuccessAmount  
  ,IsNull(Sum(Fail), 0) AS Fail  
  ,IsNull(Sum(FailAmount), 0) AS FailAmount  
  ,IsNull(Sum(Process), 0) AS Process  
  ,IsNull(Sum(Autorefund), 0) AS Autorefund  
  ,IsNull(Sum(ProcessAmount), 0) AS ProcessAmount  
  ,IsNull(Sum(AutorefundAmount), 0) AS AutorefundAmount  
  ,Count(1) AS Total  
  ,Sum(FLOOR(TotalAmount)) AS TotalAmount  
 FROM (  
  SELECT IsNull(Sum(CASE   
      WHEN STATUS = 'SUCCESS'  
       THEN 1  
      ELSE 0  
      END), 0) AS Success  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'SUCCESS'  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS SuccessAmount  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'FAILURE'  
       OR STATUS = 'REFUND'  
       THEN 1  
      ELSE 0  
      END), 0) AS Fail  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'FAILURE'  
       OR STATUS = 'REFUND'  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS FailAmount  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'PROCESS'  
       THEN 1  
      ELSE 0  
      END), 0) AS Process  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'AUTO REFUND'  
       THEN 1  
      ELSE 0  
      END), 0) AS Autorefund  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'PROCESS'  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS ProcessAmount  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'AUTO REFUND'  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS AutorefundAmount  
   ,Count(1) AS Total  
   ,Sum(FLOOR(Amount)) AS TotalAmount  
  FROM Recharge(NOLOCK)  
  WHERE CONVERT(VARCHAR, [DOC], 101) = CONVERT(VARCHAR, GETDATE(), 101)  
    
  UNION ALL  
    
  SELECT IsNull(Sum(CASE   
      WHEN STATUS = 'SUCCESS'  
       THEN 1  
      ELSE 0  
      END), 0) AS Success  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'SUCCESS'  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS SuccessAmount  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'FAILURE'  
       THEN 1  
      ELSE 0  
      END), 0) AS Fail  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'FAILURE'  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS FailAmount  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'PROCESS'  
       THEN 1  
      ELSE 0  
      END), 0) AS Process  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'AUTO REFUND'  
       THEN 1  
      ELSE 0  
      END), 0) AS Autorefund  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'PROCESS'  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS ProcessAmount  
   ,IsNull(Sum(CASE   
      WHEN STATUS = 'AUTO REFUND'  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS AutorefundAmount  
   ,Count(1) AS Total  
   ,Sum(FLOOR(Amount)) AS TotalAmount  
  FROM [dbo].[MoneyTransfer](NOLOCK)  
  WHERE CONVERT(VARCHAR, [DOC], 101) = CONVERT(VARCHAR, GETDATE(), 101)  
  ) AS MainTable  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectStatusCountofRetailer]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectStatusCountofRetailer] @UserID INT  
AS  
BEGIN  
 SELECT IsNull(Sum(Success), 0) AS Success  
  ,IsNull(Sum(SuccessAmount), 0) AS SuccessAmount  
  ,IsNull(Sum(Fail), 0) AS Fail  
  ,IsNull(Sum(FailAmount), 0) AS FailAmount  
  ,IsNull(Sum(Process), 0) AS Process  
  ,0 AS Autorefund  
  ,IsNull(Sum(ProcessAmount), 0) AS ProcessAmount  
  ,0 AS AutorefundAmount  
  ,Count(1) AS Total  
  ,IsNull(Sum(FLOOR(TotalAmount)), 0) AS TotalAmount  
 FROM (  
  SELECT IsNull(Sum(CASE   
      WHEN upper(STATUS) = 'SUCCESS'  
       THEN 1  
      ELSE 0  
      END), 0) AS Success  
   ,IsNull(Sum(CASE   
      WHEN upper(STATUS) = 'SUCCESS'  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS SuccessAmount  
   ,IsNull(Sum(CASE   
      WHEN (  
        upper(STATUS) = 'FAILURE'  
        OR upper(STATUS) = 'REFUND'  
        )  
       THEN 1  
      ELSE 0  
      END), 0) AS Fail  
   ,IsNull(Sum(CASE   
      WHEN (  
        upper(STATUS) = 'FAILURE'  
        OR upper(STATUS) = 'REFUND'  
        )  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS FailAmount  
   ,IsNull(Sum(CASE   
      WHEN (  
        upper(STATUS) = 'PROCESS'  
        OR upper(STATUS) = 'PENDING'  
        )  
       THEN 1  
      ELSE 0  
      END), 0) AS Process  
   ,IsNull(Sum(CASE   
      WHEN (  
        upper(STATUS) = 'PROCESS'  
        OR upper(STATUS) = 'PENDING'  
        )  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS ProcessAmount  
   ,Count(1) AS Total  
   ,IsNull(Sum(FLOOR(Amount)), 0) AS TotalAmount  
  FROM Recharge(NOLOCK)  
  WHERE CONVERT(VARCHAR, [DOC], 101) = CONVERT(VARCHAR, GETDATE(), 101)  
   AND UserID = @UserID  
 
  UNION ALL  
    
  SELECT IsNull(Sum(CASE   
      WHEN upper(STATUS) = 'SUCCESS'  
       THEN 1  
      ELSE 0  
      END), 0) AS Success  
   ,IsNull(Sum(CASE   
      WHEN upper(STATUS) = 'SUCCESS'  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS SuccessAmount  
   ,IsNull(Sum(CASE   
      WHEN (  
        upper(STATUS) = 'FAILURE'  
        OR upper(STATUS) = 'REFUND'  
        )  
       THEN 1  
      ELSE 0  
      END), 0) AS Fail  
   ,IsNull(Sum(CASE   
      WHEN (  
        upper(STATUS) = 'FAILURE'  
        OR upper(STATUS) = 'REFUND'  
        )  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS FailAmount  
   ,IsNull(Sum(CASE   
      WHEN (  
        upper(STATUS) = 'PROCESS'  
        OR upper(STATUS) = 'PENDING'  
        )  
       THEN 1  
      ELSE 0  
      END), 0) AS Process  
   ,IsNull(Sum(CASE   
      WHEN (  
        upper(STATUS) = 'PROCESS'  
        OR upper(STATUS) = 'PENDING'  
        )  
       THEN FLOOR(Amount)  
      ELSE 0  
      END), 0) AS ProcessAmount  
   ,Count(1) AS Total  
   ,IsNull(Sum(FLOOR(Amount)), 0) AS TotalAmount  
  FROM [dbo].[MoneyTransfer](NOLOCK)  
  WHERE CONVERT(VARCHAR, [DOC], 101) = CONVERT(VARCHAR, GETDATE(), 101)  
   AND UserID = @UserID  
  ) AS MainTable  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectTodaysFailRecharge]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectTodaysFailRecharge] @date DATE = NULL  
AS  
BEGIN  
 IF (@date IS NULL)  
 BEGIN  
  SET @date = (switchoffset(sysdatetimeoffset(), '+05:30'))  
 END (  
    SELECT R.OperatorID  
     ,OM.OperatorName  
     ,sum(R.Amount) AS Amount  
     ,Count(R.RechargeID) AS Count  
    FROM OperatorMaster OM  
    INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID  
    INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
    INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID  
    WHERE (  
      R.STATUS LIKE '%Fail%'  
      OR R.STATUS LIKE '%REFUND%'  
      )  
     AND convert(VARCHAR(50), R.DOC, 103) = convert(VARCHAR(50), @date, 103)  
    GROUP BY SM.ServiceName  
     ,RPM.RechargeProviderName  
     ,OM.OperatorName  
     ,R.RechargeProviderID  
     ,R.OperatorID  
     ,R.ServiceID  
   
    UNION ALL  
      
    SELECT M.[DMROperatorID]  
     ,OM.OperatorName  
     ,sum(M.Amount) AS Amount  
     ,Count(M.[MoneyTransferID]) AS Count  
    FROM OperatorMaster OM  
    INNER JOIN [dbo].[MoneyTransfer] M ON OM.OperatorID = M.[DMROperatorID]  
    INNER JOIN ServiceMaster SM ON M.[DMRServiceID] = SM.ServiceID  
    INNER JOIN RechargeProviderMaster RPM ON M.[DMRProviderID] = RPM.RechargeProviderID  
    WHERE (  
      M.STATUS LIKE '%Fail%'  
      OR M.STATUS LIKE '%REFUND%'  
      )  
     AND convert(VARCHAR(50), M.DOC, 103) = convert(VARCHAR(50), @date, 103)  
    GROUP BY SM.ServiceName  
     ,RPM.RechargeProviderName  
     ,OM.OperatorName  
     ,M.[DMRProviderID]  
     ,M.[DMROperatorID]  
     ,M.[DMRServiceID]  
    )  
  ORDER BY count DESC  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectTodaysPendingRech]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectTodaysPendingRech] @date DATE = NULL  
AS  
BEGIN  
 IF (@date IS NULL)  
 BEGIN  
  SET @date = (switchoffset(sysdatetimeoffset(), '+05:30'))  
 END (  
    SELECT R.OperatorID  
     ,OM.OperatorName  
     ,sum(R.Amount) AS Amount  
     ,Count(R.RechargeID) AS Count  
    FROM OperatorMaster OM  
    INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID  
    INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
    INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID  
    WHERE R.STATUS = Upper('Process')  
     AND convert(VARCHAR(50), R.DOC, 103) = convert(VARCHAR(50), @date, 103)  
    GROUP BY SM.ServiceName  
     ,RPM.RechargeProviderName  
     ,OM.OperatorName  
     ,R.RechargeProviderID  
     ,R.OperatorID  
     ,R.ServiceID  
    )  
  ORDER BY Count DESC  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectTodaysPendingRecharge]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectTodaysPendingRecharge]  
AS  
BEGIN  
 SELECT *  
 FROM (  
  SELECT R.RechargeID  
   ,R.ServiceID  
   ,R.TransactionID  
   ,R.OperatorID  
   ,R.ConsumerNumber  
   ,R.Amount  
   ,R.STATUS  
   ,R.IsDispute  
   ,RPM.RechargeProviderName  
   ,RPM.HostName AS RechargeProviderUrl  
   ,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC  
   ,SM.ServiceName  
   ,OM.OperatorName  
   ,R.OpeningBalance  
   ,TD.DiscountAmount  
   ,R.UserID  
   ,UI.UserName AS Uname  
   ,UI.UserTypeID  
   ,R.RechargeProviderID  
  FROM Recharge R  
  INNER JOIN UserInformation UI ON R.UserID = UI.UserID  
  INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
  INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID  
  INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID  
  INNER JOIN TransactionDetails TD ON R.RechargeID = TD.RechargeID
  AND R.ServiceID = TD.ServiceID  
   AND R.UserID = TD.UserID  
  WHERE   
   UPPER(R.STATUS) = UPPER('PROCESS')   
  ) AS UnionBoth  
 ORDER BY convert(DATETIME, DOC, 103) DESC  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectTodaysPendingRechargeForResend]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    

CREATE PROCEDURE [dbo].[RechargeSelectTodaysPendingRechargeForResend]    

AS    

BEGIN    

 DECLARE @CurTime DATETIME;    

 Declare @GasServiceID int = 19;  

 SET @CurTime = CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30'), (0))    

    

 --print @CurTime          

 SELECT R.RechargeID    

  ,R.ServiceID    

  ,R.TransactionID    

  ,R.RechargeProviderId    

  ,R.OperatorID    

  ,R.ConsumerNumber    

  ,R.Amount    

  ,R.STATUS    

  ,R.IsDispute    

  ,RPM.RechargeProviderName    

  ,RPM.RechargeProviderUrl    
   ,RPM.HostName    

  ,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC    

  ,SM.ServiceName    

  ,OM.OperatorName    

  ,R.OpeningBalance    

  ,TD.DiscountAmount    

  ,R.UserID    

  ,UI.UserName AS Uname    

  ,UI.UserTypeID    

 FROM Recharge R    

 INNER JOIN UserInformation UI ON R.UserID = UI.UserID    

 INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID    

 INNER JOIN OperatorMaster OM ON R.OperatorID = OM.OperatorID    

 INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID    

 INNER JOIN TransactionDetails TD ON R.RechargeID = TD.RechargeID    

  AND R.ServiceID = TD.ServiceID  

  AND R.UserID = TD.UserID    

 WHERE convert(DATE, R.DOC, 103) = convert(DATE, getdate(), 103)    

  AND UPPER(R.STATUS) = UPPER('PROCESS')    

  AND R.ServiceID != @GasServiceID  

  ORder By R.DOC Desc

END 


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectTodaysRetailerFailRecharge]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[RechargeSelectTodaysRetailerFailRecharge] @UserID INT  
AS  
BEGIN  
 SELECT R.OperatorID  
  ,OM.OperatorName  
  ,sum(R.Amount) AS Amount  
  ,Count(R.RechargeID) AS Count  
 FROM OperatorMaster OM  
 INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID  
 INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
 INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID  
 WHERE (  
   (R.STATUS LIKE '%Fail%')  
   OR (R.STATUS LIKE '%Refund%')  
   )  
  AND R.UserID = @UserID  
  AND convert(DATE, R.DOC, 103) = convert(DATE, getdate(), 103)  
 GROUP BY SM.ServiceName  
  ,RPM.RechargeProviderName  
  ,OM.OperatorName  
  ,R.RechargeProviderID  
  ,R.OperatorID  
  ,R.ServiceID  
 
 UNION ALL  
   
 SELECT E.[DMROperatorID]  
  ,OM.OperatorName  
  ,sum(E.Amount) AS Amount  
  ,Count(E.[MoneyTransferID]) AS Count  
 FROM OperatorMaster OM  
 INNER JOIN [dbo].[MoneyTransfer] E ON OM.OperatorID = E.[DMROperatorID]  
 INNER JOIN ServiceMaster SM ON E.[DMRServiceID] = SM.ServiceID  
 INNER JOIN RechargeProviderMaster RPM ON E.[DMRProviderID] = RPM.RechargeProviderID  
 WHERE (  
   (E.STATUS LIKE '%Fail%')  
   OR (E.STATUS LIKE '%Refund%')  
   )  
  AND E.UserID = @UserID  
  AND convert(DATE, E.DOC, 103) = convert(DATE, getdate(), 103)  
 GROUP BY SM.ServiceName  
  ,RPM.RechargeProviderName  
  ,OM.OperatorName  
  ,E.[DMRProviderID]  
  ,E.[DMROperatorID]  
  ,E.[DMRServiceID]  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectTodaysRetailerPendingRech]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectTodaysRetailerPendingRech] @UserID INT  
AS  
BEGIN  
 DECLARE @userType VARCHAR(20);  
 DECLARE @retailer VARCHAR(20) = 'RETAILER';  
 DECLARE @apiUser VARCHAR(20) = 'APIUSER';  
  
 SELECT @userType = UTM.UserType  
 FROM [dbo].UserInformation(NOLOCK) UI  
 INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID  
 WHERE UI.UserID = @UserID  
  
 IF (  
   Upper(@userType) = Upper(@retailer)  
   OR Upper(@userType) = Upper(@apiUser)  
   )  
 BEGIN  
  SELECT R.OperatorID  
   ,OM.OperatorName  
   ,sum(R.Amount) AS Amount  
   ,Count(R.RechargeID) AS Count  
  FROM OperatorMaster OM  
  INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID  
  INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
  INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID  
  WHERE Upper(R.STATUS) = Upper('Process')  
   AND R.UserID = @UserID  
   AND convert(DATE, R.DOC, 103) = convert(DATE, getdate(), 103)  
  GROUP BY SM.ServiceName  
   ,RPM.RechargeProviderName  
   ,OM.OperatorName  
   ,R.RechargeProviderID  
   ,R.OperatorID  
   ,R.ServiceID  
  
  UNION ALL  
    
  SELECT E.[DMROperatorID]  
   ,OM.OperatorName  
   ,sum(E.Amount) AS Amount  
   ,Count(E.[MoneyTransferID]) AS Count  
  FROM OperatorMaster OM  
  INNER JOIN [dbo].[MoneyTransfer] E ON OM.OperatorID = E.[DMROperatorID]  
  INNER JOIN ServiceMaster SM ON E.[DMRServiceID] = SM.ServiceID  
  INNER JOIN RechargeProviderMaster RPM ON E.[DMRProviderID] = RPM.RechargeProviderID  
  WHERE E.STATUS = Upper('Process')  
   AND E.UserID = @UserID  
   AND convert(DATE, E.DOC, 103) = convert(DATE, getdate(), 103)  
  GROUP BY SM.ServiceName  
   ,RPM.RechargeProviderName  
   ,OM.OperatorName  
   ,E.[DMRProviderID]  
   ,E.[DMROperatorID]  
   ,E.[DMRServiceID]  
 END  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectTodaysRetailerSuccessRecharge]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[RechargeSelectTodaysRetailerSuccessRecharge] @UserID INT  
AS  
BEGIN  
 DECLARE @userType VARCHAR(20);  
 DECLARE @retailer VARCHAR(20) = 'RETAILER';  
 DECLARE @apiUser VARCHAR(20) = 'APIUSER';  
  
 SELECT @userType = UTM.UserType  
 FROM [dbo].UserInformation(NOLOCK) UI  
 INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID  
 WHERE UI.UserID = @UserID  
  
 IF (  
   Upper(@userType) = Upper(@retailer)  
   OR Upper(@userType) = Upper(@apiUser)  
   )  
 BEGIN  
  SELECT R.OperatorID  
   ,OM.OperatorName  
   ,sum(R.Amount) AS Amount  
   ,Count(R.RechargeID) AS Count  
  FROM OperatorMaster OM  
  INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID  
  INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
  INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID  
  WHERE R.STATUS = Upper('Success')  
   AND R.UserID = @UserID  
   AND convert(DATE, R.DOC, 103) = convert(DATE, getdate(), 103)  
  GROUP BY SM.ServiceName  
   ,RPM.RechargeProviderName  
   ,OM.OperatorName  
   ,R.RechargeProviderID  
   ,R.OperatorID  
   ,R.ServiceID  
 
  UNION ALL  
    
  SELECT MT.[DMROperatorID]  
   ,OM.OperatorName  
   ,sum(MT.Amount) AS Amount  
   ,Count(MT.[MoneyTransferID]) AS Count  
  FROM OperatorMaster OM  
  INNER JOIN [MoneyTransfer] MT ON OM.OperatorID = MT.[DMROperatorID]  
  INNER JOIN ServiceMaster SM ON MT.[DMRServiceID] = SM.ServiceID  
  INNER JOIN RechargeProviderMaster RPM ON MT.[DMRProviderID] = RPM.RechargeProviderID  
  WHERE MT.STATUS = Upper('Success')  
   AND MT.UserID = @UserID  
   AND convert(DATE, MT.DOC, 103) = convert(DATE, getdate(), 103)  
  GROUP BY SM.ServiceName  
   ,RPM.RechargeProviderName  
   ,OM.OperatorName  
   ,MT.[DMRProviderID]  
   ,MT.[DMROperatorID]  
   ,MT.[DMRServiceID]  
 END        
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectTodaysRetailerTotalRecharge]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectTodaysRetailerTotalRecharge] @UserID INT
AS
BEGIN
	DECLARE @userType VARCHAR(20);
	DECLARE @retailer VARCHAR(20) = 'RETAILER';
	DECLARE @apiUser VARCHAR(20) = 'APIUSER';

	SELECT @userType = UTM.UserType
	FROM [dbo].UserInformation(NOLOCK) UI
	INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @UserID

	IF (
			Upper(@userType) = Upper(@retailer)
			OR Upper(@userType) = Upper(@apiUser)
			)
	BEGIN
		SELECT R.OperatorID
			,OM.OperatorName
			,R.STATUS
			,sum(R.Amount) AS Amount
			,Count(R.RechargeID) AS Count
		FROM OperatorMaster OM
		INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID
		INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID
		INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID
		WHERE R.UserID = @UserID
			AND convert(DATE, R.DOC, 103) = convert(DATE, getdate(), 103)
		GROUP BY SM.ServiceName
			,RPM.RechargeProviderName
			,OM.OperatorName
			,R.RechargeProviderID
			,R.OperatorID
			,R.ServiceID
			,R.STATUS
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectTodaysSuccessRecharge]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RechargeSelectTodaysSuccessRecharge] @date DATE = NULL  
AS  
BEGIN  
 IF (@date IS NULL)  
 BEGIN  
  SET @date = (switchoffset(sysdatetimeoffset(), '+05:30'))  
 END (  
    SELECT R.OperatorID  
     ,OM.OperatorName  
     ,sum(R.Amount) AS Amount  
     ,Count(R.RechargeID) AS Count  
     ,RPM.RechargeProviderName  
    FROM OperatorMaster OM  
    INNER JOIN Recharge R ON OM.OperatorID = R.OperatorID  
    INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
    INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID  
    WHERE R.STATUS = Upper('Success')  
     AND convert(VARCHAR(50), R.DOC, 103) = convert(VARCHAR(50), @date, 103)  
    GROUP BY SM.ServiceName  
     ,RPM.RechargeProviderName  
     ,OM.OperatorName  
     ,R.RechargeProviderID  
     ,R.OperatorID  
     ,R.ServiceID  
  
    UNION ALL  
      
    SELECT M.[DMROperatorID]  
     ,OM.OperatorName  
     ,sum(M.Amount) AS Amount  
     ,Count(M.[MoneyTransferID]) AS Count  
     ,RPM.RechargeProviderName  
    FROM OperatorMaster OM  
    INNER JOIN [dbo].[MoneyTransfer] M ON OM.OperatorID = M.[DMROperatorID]  
    INNER JOIN ServiceMaster SM ON M.[DMRServiceID] = SM.ServiceID  
    INNER JOIN RechargeProviderMaster RPM ON M.[DMRProviderID] = RPM.RechargeProviderID  
    WHERE M.STATUS = Upper('Success')  
     AND convert(VARCHAR(50), M.DOC, 103) = convert(VARCHAR(50), @date, 103)  
    GROUP BY SM.ServiceName  
     ,RPM.RechargeProviderName  
     ,OM.OperatorName  
     ,M.[DMRProviderID]  
     ,M.[DMROperatorID]  
     ,M.[DMRServiceID]  
    )  
  ORDER BY Count DESC  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSelectUserOperatorDateWise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [RechargeSelectUserOperatorDateWise] '02/10/2018','02/10/2018',1    
CREATE PROCEDURE [dbo].[RechargeSelectUserOperatorDateWise] @FromDate VARCHAR(100)  
 ,@Todate VARCHAR(100)  
 ,@UserID INT  
 ,@LoginUserID INT = 0  
AS  
BEGIN  
 SELECT DISTINCT OperatorID  
  ,OperatorName  
  ,ServiceName  
  ,RechargeProviderID  
  ,RechargeProviderName  
  ,Sum(DiscountAmount) AS DiscountAmount  
  ,ServiceID  
  ,Count(Amount) AS Count  
  ,Sum(Amount) AS Amount  
 FROM (  
  SELECT R.OperatorID AS OperatorID  
   ,OM.OperatorName  
   ,SM.ServiceName  
   ,R.RechargeProviderID AS RechargeProviderID  
   ,RPM.RechargeProviderName  
   ,TD.DiscountAmount AS DiscountAmount  
   ,R.ServiceID  
   ,R.Amount AS Amount  
   ,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC  
  FROM Recharge R  
  INNER JOIN OperatorMaster OM ON OM.OperatorID = R.OperatorID  
  INNER JOIN TransactionDetails TD ON TD.RechargeID = R.RechargeID  
  AND R.ServiceID = TD.ServiceID
  INNER JOIN UserInformation UM ON UM.UserID = TD.UserID  
  INNER JOIN ServiceMaster SM ON R.ServiceID = SM.ServiceID  
  INNER JOIN RechargeProviderMaster RPM ON R.RechargeProviderID = RPM.RechargeProviderID  
  WHERE R.STATUS IN ('SUCCESS')  
   AND TD.DOCType IN ('RECHARGE', 'ElectricityBill')
   AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)  
   AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)  
   --and TD.UserID != 1          
   AND TD.UserID NOT IN (  
    1  
    ,60  
    )  
   AND UM.UserTypeID NOT IN (  
    2  
    ,8  
    )  
    
  UNION  
    
  SELECT R.[DMROperatorID] AS OperatorID  
   ,OM.OperatorName  
   ,SM.ServiceName  
   ,R.[DMRProviderID] AS RechargeProviderID  
   ,RPM.RechargeProviderName  
   ,TD.DiscountAmount AS DiscountAmount  
   ,R.[DMRServiceID] AS ServiceID  
   ,R.Amount AS Amount  
   ,convert(VARCHAR(12), R.DOC, 113) + right(convert(VARCHAR(39), R.DOC, 22), 11) AS DOC  
  FROM MoneyTransfer R  
  INNER JOIN OperatorMaster OM ON OM.OperatorID = R.[DMROperatorID]  
  INNER JOIN TransactionDetails TD ON TD.RechargeID = R.[MoneyTransferID] 
  AND TD.ServiceID = R.DMRServiceID 
  INNER JOIN UserInformation UM ON UM.UserID = TD.UserID  
  INNER JOIN ServiceMaster SM ON R.[DMRServiceID] = SM.ServiceID  
  INNER JOIN RechargeProviderMaster RPM ON R.[DMRProviderID] = RPM.RechargeProviderID  
  WHERE R.STATUS IN ('SUCCESS')  
   AND TD.DOCType = ('MoneyTransfer')  
   AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)  
   AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)  
   --and TD.UserID != 1          
   AND TD.UserID NOT IN (  
    1  
    ,60  
    )  
   AND UM.UserTypeID NOT IN (  
    2  
    ,8  
    )  
  ) t  
 GROUP BY ServiceName  
  ,RechargeProviderName  
  ,OperatorName  
  ,RechargeProviderID  
  ,OperatorID  
  ,ServiceID  
 ORDER BY Count DESC  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSummarySelectDatewise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [RechargeSummarySelectDatewise] 1048,'07/10/2018','08/10/2018'  
CREATE PROCEDURE [dbo].[RechargeSummarySelectDatewise] @UserID INT  
 ,@FromDate VARCHAR(50)  
 ,@ToDate VARCHAR(50)  
AS  
BEGIN  
 DECLARE @UserType INT;  
 DECLARE @BalanceTransferService INT = 18;  
 DECLARE @SystemUserID INT = 60;  
 DECLARE @RetailerType INT = 3;  
 DECLARE @FOSType INT = 7;  
 DECLARE @AdminType INT = 1;  
 DECLARE @DistType INT = 2;  
 DECLARE @SuperDistType INT = 8;  
 DECLARE @APIUserType INT = 4;  
  
 SET @UserType = (  
   SELECT UserTypeID  
   FROM UserInformation  
   WHERE UserID = @UserID  
   )  
  
 PRINT @UserType  
  
 IF (  
   @UserType = @RetailerType  
   OR @UserType = @FOSType  
   OR @UserType = @APIUserType  
   )  
 BEGIN  
  SELECT COUNT(RechargeCount) AS RechargeCount  
   ,sum(Amount) AS Amount  
   ,sum(Commission) AS Commission  
   ,ServiceName  
  FROM (  
   SELECT TD.RechargeID AS RechargeCount  
    ,R.Amount AS Amount  
    ,TD.DiscountAmount AS Commission  
    ,S.ServiceName  
    ,row_number() OVER (  
     PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC  
     ) AS row_num  
   FROM Recharge R  
   LEFT JOIN TransactionDetails TD ON TD.RechargeID = R.RechargeID  
   AND TD.ServiceID = R.ServiceID
   LEFT JOIN ServiceMaster S ON S.ServiceID = R.ServiceID  
   WHERE R.STATUS = 'SUCCESS'  
    AND TD.AmountCreditDebit = 'DEBIT'  
    AND R.UserID = @UserID  
    AND TD.ServiceID != @BalanceTransferService  
    AND DOCType in ('RECHARGE', 'ElectricityBill')
    AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)  
    AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)  
   GROUP BY R.ServiceID  
    ,S.ServiceName  
    ,TD.RechargeID  
    ,TD.DOC  
    ,R.Amount  
    ,TD.DiscountAmount  
   ) AS tbl  
  WHERE row_num = 1  
  GROUP BY ServiceName  
    
  UNION ALL  
    
  SELECT COUNT(RechargeCount) AS RechargeCount  
   ,sum(Amount) AS Amount  
   ,sum(Commission) AS Commission  
   ,ServiceName  
  FROM (  
   SELECT TD.RechargeID AS RechargeCount  
    ,R.Amount AS Amount  
    ,TD.DiscountAmount AS Commission  
    ,S.ServiceName  
    ,row_number() OVER (  
     PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC  
     ) AS row_num  
   FROM dbo.MoneyTransfer R  
   LEFT JOIN TransactionDetails TD ON TD.RechargeID = R.[MoneyTransferID]  
   AND TD.ServiceID = R.DMRServiceID
   LEFT JOIN ServiceMaster S ON S.ServiceID = R.[DMRServiceID]  
   WHERE R.STATUS = 'SUCCESS'  
    AND TD.AmountCreditDebit = 'DEBIT'  
    AND R.UserID = @UserID  
    AND TD.ServiceID != @BalanceTransferService  
    AND DOCType = 'MoneyTransfer'  
    AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)  
    AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)  
   GROUP BY R.[DMRServiceID]  
    ,S.ServiceName  
    ,TD.RechargeID  
    ,TD.DOC  
    ,R.Amount  
    ,TD.DiscountAmount  
   ) AS tbl   
  WHERE row_num = 1  
  GROUP BY ServiceName  
 END  
  
 IF (@UserType = @AdminType)  
 BEGIN  
  SELECT COUNT(RechargeCount) AS RechargeCount  
   ,sum(Amount) AS Amount  
   ,sum(Commission) AS Commission  
   ,ServiceName  
  FROM (  
   SELECT TD.RechargeID AS RechargeCount  
    ,R.Amount AS Amount  
    ,TD.DiscountAmount AS Commission  
    ,S.ServiceName  
    ,row_number() OVER (  
     PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC  
     ) AS row_num  
   FROM Recharge R  
   LEFT JOIN TransactionDetails TD ON TD.RechargeID = R.RechargeID  
   AND R.ServiceID = TD.ServiceID
   LEFT JOIN ServiceMaster S ON S.ServiceID = R.ServiceID  
   WHERE R.STATUS = 'SUCCESS'  
    AND TD.[CommissionCreditDebit] = 'CREDIT'  
    AND TD.[AmountCreditDebit] = ''  
    AND TD.UserID = @SystemUserID  
    AND TD.ServiceID != @BalanceTransferService  
     AND DOCType in ('RECHARGE', 'ElectricityBill')
    AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)  
    AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)  
   GROUP BY R.ServiceID  
    ,S.ServiceName  
    ,TD.RechargeID  
    ,TD.DOC  
    ,R.Amount  
    ,TD.DiscountAmount  
   ) AS tbl  
  WHERE row_num = 1  
  GROUP BY ServiceName  
    
  UNION ALL  
    
  SELECT COUNT(RechargeCount) AS RechargeCount  
   ,sum(Amount) AS Amount  
   ,sum(Commission) AS Commission  
   ,ServiceName  
  FROM (  
   SELECT TD.RechargeID AS RechargeCount  
    ,R.Amount AS Amount  
    ,TD.DiscountAmount AS Commission  
    ,S.ServiceName  
    ,row_number() OVER (  
     PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC  
     ) AS row_num  
   FROM dbo.MoneyTransfer R  
   LEFT JOIN TransactionDetails TD ON TD.RechargeID = R.[MoneyTransferID] 
   AND R.DMRServiceID = TD.ServiceID
   LEFT JOIN ServiceMaster S ON S.ServiceID = R.[DMRServiceID]  
   WHERE R.STATUS = 'SUCCESS'  
    AND TD.[CommissionCreditDebit] = 'CREDIT'  
    AND TD.[AmountCreditDebit] = ''  
    AND TD.UserID = @SystemUserID  
    AND TD.ServiceID != @BalanceTransferService  
    AND DOCType = 'MoneyTransfer'  
    AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)  
    AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)  
   GROUP BY R.[DMRServiceID]  
    ,S.ServiceName  
    ,TD.RechargeID  
    ,TD.DOC  
    ,R.Amount  
    ,TD.DiscountAmount  
   ) AS tbl  
  WHERE row_num = 1  
  GROUP BY ServiceName  
 END  
  
 IF (  
   @UserType = @DistType  
   OR @UserType = @SuperDistType  
   )  
 BEGIN  
  SELECT COUNT(RechargeCount) AS RechargeCount  
   ,sum(Amount) AS Amount  
   ,sum(Commission) AS Commission  
   ,ServiceName  
  FROM (  
   SELECT TD.RechargeID AS RechargeCount  
    ,R.Amount AS Amount  
    ,TD.DiscountAmount AS Commission  
    ,S.ServiceName  
    ,row_number() OVER (  
     PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC  
     ) AS row_num  
   FROM Recharge R  
   LEFT JOIN TransactionDetails TD ON TD.RechargeID = R.RechargeID 
   AND R.ServiceID = TD.ServiceID 
   LEFT JOIN ServiceMaster S ON S.ServiceID = R.ServiceID  
   WHERE R.STATUS = 'SUCCESS'  
    AND TD.CommissionCreditDebit = 'CREDIT'  
    AND TD.AmountCreditDebit = ''  
    AND TD.ServiceID != @BalanceTransferService  
    AND DOCType in ('RECHARGE', 'ElectricityBill')
    AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)  
    AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)  
    AND TD.UserID = @UserID  
   GROUP BY R.ServiceID  
    ,S.ServiceName  
    ,TD.RechargeID  
    ,TD.DOC  
    ,R.Amount  
    ,TD.DiscountAmount  
   ) AS tbl  
  WHERE row_num = 1  
  GROUP BY ServiceName  
    
  UNION ALL  
    
  SELECT COUNT(RechargeCount) AS RechargeCount  
   ,sum(Amount) AS Amount  
   ,sum(Commission) AS Commission  
   ,ServiceName  
  FROM (  
   SELECT TD.RechargeID AS RechargeCount  
    ,R.Amount AS Amount  
    ,TD.DiscountAmount AS Commission  
    ,S.ServiceName  
    ,row_number() OVER (  
     PARTITION BY TD.RechargeID ORDER BY TD.DOC DESC  
     ) AS row_num  
   FROM dbo.MoneyTransfer R  
   LEFT JOIN TransactionDetails TD ON TD.RechargeID = R.[MoneyTransferID]  
    AND TD.ServiceID = R.DMRServiceID
   LEFT JOIN ServiceMaster S ON S.ServiceID = R.[DMRServiceID]  
   WHERE R.STATUS = 'SUCCESS'  
    AND TD.CommissionCreditDebit = 'CREDIT'  
    AND TD.AmountCreditDebit = ''  
    AND TD.ServiceID != @BalanceTransferService  
    AND DOCType = 'MoneyTransfer'  
    AND convert(DATE, R.DOC, 103) >= convert(DATE, @FromDate, 103)  
    AND convert(DATE, R.DOC, 103) <= convert(DATE, @ToDate, 103)  
    AND TD.UserID = @UserID  
   GROUP BY R.[DMRServiceID]  
    ,S.ServiceName  
    ,TD.RechargeID  
    ,TD.DOC  
    ,R.Amount  
    ,TD.DiscountAmount  
   ) AS tbl  
  WHERE row_num = 1  
  GROUP BY ServiceName  
 END  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeSummarySelectUserWise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  [RechargeSummarySelectUserWise] 3    
CREATE PROCEDURE [dbo].[RechargeSummarySelectUserWise] --3      
 @UserID INT  
AS  
BEGIN  
 DECLARE @ShopeName VARCHAR(500);  
 DECLARE @date DATE = (switchoffset(sysdatetimeoffset(), '+05:30'))  
 DECLARE @salesAmount DECIMAL(18, 5);  
 DECLARE @targerAmount DECIMAL(18, 5);  
 DECLARE @UserIDRank INT;  
 DECLARE @CurrentSale DECIMAL(18, 5);  
 DECLARE @behindBy DECIMAL(18, 5);  
  
 ---select top 1 sum(Amount) AS Amount,UserID from Recharge where Status ='SUCCESS' and Convert(date, DOC, 103) = Convert(date, @date, 103)  order by amount desc       
 create  TABLE #tblSalesSummary (  
  Amount DECIMAL(18, 5)  
  ,UserID INT  
  );  
  
 ---drop table #tblSalesSummary      
 INSERT INTO #tblSalesSummary  
 SELECT sum(Amount) AS Amount  
  ,UserID AS UserID  
 FROM (  
  SELECT IsNull((R.Amount), 0) AS Amount  
   ,UserID  
  FROM Recharge R  
  WHERE R.STATUS = 'SUCCESS'  
   AND Convert(DATE, DOC, 103) = Convert(DATE, @date, 103)  
    
  UNION ALL  
    
  SELECT IsNull((R.Amount), 0) AS Amount  
   ,UserID  
  FROM dbo.MoneyTransfer R  
  WHERE R.STATUS = 'SUCCESS'  
   AND Convert(DATE, DOC, 103) = Convert(DATE, @date, 103)  
    
  ) t  
 GROUP BY UserID  
  
 SELECT TOP 1 @salesAmount = Amount  
  ,@UserIDRank = UserID  
 FROM #tblSalesSummary  
 ORDER BY Amount DESC  
  
 ----select @salesAmount as salesAmount,@UserIDRank as UserIDRank      
 SET @targerAmount = @salesAmount + 100.00;  
 SET @ShopeName = (  
   SELECT TOP 1 UserName  
   FROM UserInformation  
   WHERE UserID = @UserIDRank  
   )  
  
 SELECT @CurrentSale = IsNull(sum(Amount), 0)  
 FROM (  
  SELECT IsNull((R.Amount), 0) AS Amount  
   ,UserID  
  FROM Recharge R  
  WHERE R.STATUS = 'SUCCESS'  
   AND R.UserID = @UserID  
   AND Convert(DATE, DOC, 103) = Convert(DATE, @date, 103)  
    
  UNION ALL  
    
  SELECT IsNull((R.Amount), 0) AS Amount  
   ,UserID  
  FROM dbo.MoneyTransfer R  
  WHERE R.STATUS = 'SUCCESS'  
   AND R.UserID = @UserID  
   AND Convert(DATE, DOC, 103) = Convert(DATE, @date, 103)  
    
  ) p  
 GROUP BY UserID  
  
 SET @UserIDRank = (  
   SELECT ROW_NUMBER() OVER (  
     ORDER BY Amount ASC  
     ) AS Row#  
   FROM #tblSalesSummary  
   WHERE UserID = @UserID  
   )  
 SET @behindBy = @targerAmount - IsNull(@CurrentSale, 0);  
  
 SELECT @salesAmount AS SalesAmount  
  ,@ShopeName AS [UserName]  
  ,@ShopeName AS [ShopeName]  
  ,IsNull(@CurrentSale, 0) AS CurrentRechargeSale  
  ,@targerAmount AS TargetAmount  
  ,IsNull(@UserIDRank, 0) AS UserRank  
  ,@behindBy AS BehindBy  
END  


GO
/****** Object:  StoredProcedure [dbo].[RechargeUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                                            
-- Author:  <Sainath Bhujbal>                                            
-- Create date: <10 March 2017 ,>                                            
-- Description: <Description,,>                                            
-- RechargeUpdate 209,'7745875075','SUCCESS','Recharge successful',2,6,1,10,'MH0720173722000232',0,3435             
-- =============================================                                   
CREATE PROCEDURE [dbo].[RechargeUpdate] @RechargeId INT
	,@ConsumerNo VARCHAR(20)
	,@Status VARCHAR(20)
	,@RechargeDesc VARCHAR(20)
	,@RechargeProviderID INT
	,@OperatorId INT
	,@ServiceID INT
	,@Amount NUMERIC(18, 5) = 0
	,@TransID VARCHAR(50)
	,@RechargeProviderOpeningBalance DECIMAL(18, 5) = 0
	,@RechargeProviderClosingBalance DECIMAL(18, 5) = 0
AS
BEGIN
	DECLARE @providerComm NUMERIC(18, 5) = 0;
	DECLARE @providerCommType VARCHAR(10) = 'P';
	DECLARE @providerOpBal NUMERIC(18, 5) = 0;
	DECLARE @AfterTransAmt NUMERIC(18, 5)
	DECLARE @BeforeTransAmt NUMERIC(18, 5) = 0;
	DECLARE @TRANS_DESC VARCHAR(max)
	DECLARE @UserID INT
	DECLARE @PrevClosingBal DECIMAL(18, 5) = 0;
	DECLARE @Success VARCHAR(20) = 'SUCCESS';
	DECLARE @PROCESS VARCHAR(20) = 'PROCESS';
	DECLARE @SystemAccount INT = 60;
	DECLARE @OriginalStatus VARCHAR(50)
	DECLARE @ElectricityServiceID int = 14;

	SELECT @UserID = UserID
	FROM dbo.Recharge(NOLOCK)
	WHERE RechargeID = @RechargeId

	SELECT @OriginalStatus = STATUS
	FROM [dbo].Recharge(NOLOCK)
	WHERE RechargeID = @RechargeId

	IF (
			@OriginalStatus IS NULL
			OR upper(@OriginalStatus) != Upper(@PROCESS)
			)
	BEGIN
		PRINT 'Recharge Status is not in PROCESS'

		RETURN
	END

	SELECT TOP (1) @PrevClosingBal = isnull(RechargeProviderClosingBalance, 0)
	FROM dbo.Recharge(NOLOCK)
	WHERE RechargeProviderID = @RechargeProviderID
		AND STATUS != 'PROCESS'
	ORDER BY RechargeID DESC

	IF (
			@RechargeProviderClosingBalance IS NULL
			OR @RechargeProviderClosingBalance = 0
			)
	BEGIN
		PRINT '@PrevClosingBal12' + convert(VARCHAR(30), @PrevClosingBal)

		SET @RechargeProviderClosingBalance = @PrevClosingBal
	END

	UPDATE RechargeProviderMaster
	SET CurrentBalance = ISNULL(@RechargeProviderClosingBalance, 0)
	WHERE RechargeProviderID = @RechargeProviderID

	-----------------------------End------------------------------------                         
	SELECT @providerComm = isnull(CommissionAdmin, 0)
		,@providerCommType = Upper(MarginType)
	FROM dbo.RechargeProviderOperatorMaster
	WHERE RechargeProviderID = @RechargeProviderID
		AND OperatorID = @OperatorId

	PRINT '@providerComm' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType

	IF @providerCommType = 'P'
	BEGIN
		SET @providerComm = (@providerComm * @Amount) / 100;

		PRINT '@providerComm1' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType
	END
	ELSE
	BEGIN
		SET @providerComm = @providerComm;
	END

	IF (upper(@Status) = upper(@Success))
	BEGIN
		UPDATE dbo.Recharge
		SET STATUS = @Status
			--, Request_Id = @RequestId                                
			,RechargeDescription = @RechargeDesc
			,RechargeProviderID = @RechargeProviderID
			,RechargeProviderOpeningBalance = @PrevClosingBal
			,[RechargeProviderClosingBalance] = @RechargeProviderClosingBalance
			,RechargeProviderCommission = @providerComm
			,UpdateDoc = (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))
			,TransactionID = @TransID
		WHERE RechargeID = @RechargeId

		BEGIN TRY
			IF (@providerComm IS NOT NULL)
			BEGIN
				IF (@ServiceID = @ElectricityServiceID)
				BEGIN
					SELECT @BeforeTransAmt = DMRBalance
					FROM DBO.UserBalance(NOLOCK)
					WHERE UserID = @SystemAccount
				END
				ELSE
				BEGIN
					SELECT @BeforeTransAmt = CurrentBalance
					FROM DBO.UserBalance(NOLOCK)
					WHERE UserID = @SystemAccount
				END

				--Adde For System AccountBal=(ProviderCommission-(RetailerCommission + distributerCommission)                
				DECLARE @RetailerCommission NUMERIC(18, 5) = 0;
				DECLARE @DistributerCommission NUMERIC(18, 5) = 0;

				SELECT @RetailerCommission = isnull(DiscountAmount, 0)
				FROM TransactionDetails
				WHERE RechargeID = @RechargeId
					AND Upper(AmountCreditDebit) = Upper('DEBIT')
					AND Upper(DOCType) = Upper('RECHARGE')

				PRINT @RetailerCommission

				SELECT @DistributerCommission = isnull(SUM(DiscountAmount), 0)
				FROM TransactionDetails
				WHERE RechargeID = @RechargeId
					AND AmountCreditDebit = ''
					AND Upper(DOCType) = Upper('RECHARGE')

				DECLARE @benefitCommission NUMERIC(18, 5) = 0;

				SET @benefitCommission = ISNULL( @providerComm - (@RetailerCommission + @DistributerCommission), 0)
				SET @AfterTransAmt = @BeforeTransAmt + @benefitCommission;

				IF (@ServiceID = @ElectricityServiceID)
				BEGIN
					UPDATE dbo.UserBalance
					SET DMRBalance = @AfterTransAmt
					WHERE UserID = @SystemAccount
				END
				ELSE
				BEGIN
					UPDATE dbo.UserBalance
					SET CurrentBalance = @AfterTransAmt
					WHERE UserID = @SystemAccount
				END

				DECLARE @Uname VARCHAR(50)

				SELECT @Uname = name
				FROM DBO.UserInformation(NOLOCK)
				WHERE UserID = @UserID;

				DECLARE @TransDesc VARCHAR(200)

				SET @TransDesc = 'Provider Commision Amount:' + CONVERT(VARCHAR, @benefitCommission) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;

				EXEC dbo.TransactionDetailsInsertByUser @SystemAccount
					,-- @UserID                        
					@RechargeID
					,--@RechargeID                   
					@ServiceID
					,--@serviceID                  
					@UserID
					,--@AssignedUserID                  
					@ConsumerNo
					,--@ConsumerNumber                   
					'RECHARGE'
					,--@DOCType                  
					@BeforeTransAmt
					,--@OpeningBalance                       
					@AfterTransAmt
					,--@ClosingBalance                      
					0 --@TransactionAmount                                                   
					,@benefitCommission
					,--@DiscountAmount                      
					''
					,--@AmountCreditDebit                  
					'CREDIT'
					,--@CommissionCreditDebit                  
					@TransDesc --@TransactionDescription                      
			END
		END TRY

		BEGIN CATCH
			RETURN
		END CATCH
	END
	ELSE --FOR PENDING RECHARGE                                                  
	BEGIN
		UPDATE dbo.Recharge
		SET RechargeDescription = 'Request Accepted Successfully'
			,RechargeProviderID = @RechargeProviderID
			,RechargeProviderOpeningBalance = @PrevClosingBal
			,RechargeProviderCommission = @providerComm
			,UpdateDoc = (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))
		WHERE RechargeID = @RechargeId
	END
END

GO
/****** Object:  StoredProcedure [dbo].[RechargeUpdateForHitBack]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[RechargeUpdateForHitBack] 1, 1, '', ''

CREATE PROCEDURE [dbo].[RechargeUpdateForHitBack] @Rechargeid INT
	,@ServiceID INT = NULL
	,@Request VARCHAR(max)
	,@Response VARCHAR(max)
AS
BEGIN
	Declare @MoneyTransferServiceID int = 21;

	DECLARE @RegID INT
	DECLARE @APIID INT

	SET @RegID = 0
	SET @APIID = 0

	IF (@ServiceID IS NULL)
	BEGIN
		SELECT TOP 1 @RegID = UserID
			,@APIID = RechargeProviderID
		FROM dbo.RECHARGE(NOLOCK)
		WHERE RechargeID = @Rechargeid
	END
	ELSE
	BEGIN
		SELECT TOP (1) @RegID = UserID
			,@APIID = RechargeProviderID
		FROM (
			SELECT UserID
				,RechargeProviderID
			FROM dbo.RECHARGE(NOLOCK)
			WHERE RechargeID = @Rechargeid
			And ServiceID = @ServiceID
			
			UNION ALL
			
			SELECT UserID
				,DMRProviderID AS RechargeProviderID
			FROM dbo.MoneyTransfer(NOLOCK)
			WHERE MoneytransferID = @Rechargeid
			And DMRServiceID = @ServiceID
			) AS A
	END

	IF(@ServiceID is Null
	OR @ServiceID != @MoneyTransferServiceID)
	BEGIN
		UPDATE dbo.Recharge
		SET HitBack = 1
		WHERE RechargeID = @Rechargeid
	END
	ELSE IF(@ServiceID != @MoneyTransferServiceID)
	BEGIN
		UPDATE dbo.Recharge
		SET HitBack = 1
		WHERE RechargeID = @Rechargeid
	END
	ELSE IF(@ServiceID = @MoneyTransferServiceID)
	BEGIN
		UPDATE dbo.MoneyTransfer
		SET HitBack = 1
		WHERE MoneyTransferID = @Rechargeid
	END

	IF(@ServiceID is null)
	BEGIN
		SET @ServiceID = 100
	END

	INSERT INTO dbo.UserRequestResponseLog (
		Regid
		,APIID
		,Request
		,Response
		,Rechargeid
		,ServiceID
		)
	VALUES (
		@Regid
		,@APIID
		,@Request
		,@Response
		,@Rechargeid
		,@ServiceID
		)
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeUpdateForTesting]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                              
-- Author:  <Sainath Bhujbal>                              
-- ALTER  date: <7 Jul 2017 ,>                              
-- Description: <Description,,>                              
-- =============================================                     
CREATE PROCEDURE [dbo].[RechargeUpdateForTesting] @RechargeId INT
	,@ConsumerNo VARCHAR(20)
	,@Status VARCHAR(20)
	,@RechargeDesc VARCHAR(20)
	,@RechargeProviderID INT
	,@OperatorId INT
	,@ServiceID INT
	,@Amount NUMERIC(18, 5) = 0
	,@TransID VARCHAR(50)
AS
BEGIN
	DECLARE @providerComm NUMERIC(18, 5) = 0;
	DECLARE @providerCommType VARCHAR(10) = 'P';
	DECLARE @providerOpBal NUMERIC(18, 5) = 0;
	DECLARE @AfterTransAmt NUMERIC(18, 5)
	DECLARE @BeforeTransAmt NUMERIC(18, 5) = 0;
	DECLARE @TRANS_DESC VARCHAR(max)
	DECLARE @UserID INT
	DECLARE @PrevClosingBal DECIMAL(18, 5) = 0;
	DECLARE @Success VARCHAR(20) = 'SUCCESS';
	DECLARE @SystemAccount INT = 60;

	SELECT @UserID = UserID
	FROM dbo.Recharge(NOLOCK)
	WHERE RechargeID = @RechargeId

	SET @PrevClosingBal = (
			SELECT TOP (1) RechargeProviderClosingBalance
			FROM dbo.Recharge(NOLOCK)
			WHERE RechargeProviderID = @RechargeProviderID
				AND STATUS != 'PROCESS'
			ORDER BY RechargeID DESC
			)

	IF @PrevClosingBal IS NULL
	BEGIN
		SELECT @PrevClosingBal = CurrentBalance
		FROM dbo.RechargeProviderMaster(NOLOCK)
		WHERE RechargeProviderID = @RechargeProviderID
	END

	-----------------------------End------------------------------------           
	SELECT @providerComm = isnull(CommissionAdmin, 0)
		,@providerCommType = Upper(MarginType)
	FROM dbo.RechargeProviderOperatorMaster
	WHERE RechargeProviderID = @RechargeProviderID
		AND OperatorID = @OperatorId

	PRINT '@providerComm' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType

	IF @providerCommType = 'P'
	BEGIN
		SET @providerComm = (@providerComm * @Amount) / 100;

		PRINT '@providerComm1' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType
	END
	ELSE
	BEGIN
		SET @providerComm = @providerComm;
	END

	IF (upper(@Status) = upper(@Success))
	BEGIN
		UPDATE dbo.Recharge
		SET STATUS = @Status
			--, Request_Id = @RequestId                  
			,RechargeDescription = @RechargeDesc
			,RechargeProviderID = @RechargeProviderID
			,RechargeProviderOpeningBalance = @PrevClosingBal
			,RechargeProviderCommission = @providerComm
			,UpdateDoc = (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))
			,TransactionID = @TransID
		WHERE RechargeID = @RechargeId

		BEGIN TRY
			IF (@providerComm IS NOT NULL)
			BEGIN
				SELECT @BeforeTransAmt = CurrentBalance
				FROM DBO.UserBalance(NOLOCK)
				WHERE UserID = @SystemAccount

				--Adde For System AccountBal=(ProviderCommission-(RetailerCommission + distributerCommission)
				DECLARE @RetailerCommission NUMERIC(18, 5) = 0;
				DECLARE @DistributerCommission NUMERIC(18, 5) = 0;

				SELECT @RetailerCommission = isnull(DiscountAmount, 0)
				FROM TransactionDetails
				WHERE RechargeID = @RechargeId
					AND AmountCreditDebit = 'DEBIT'
					AND DOCType = 'RECHARGE'

				SELECT @DistributerCommission = isnull(DiscountAmount, 0)
				FROM TransactionDetails
				WHERE RechargeID = @RechargeId
					AND AmountCreditDebit = ''
					AND DOCType = 'RECHARGE'

				SET @AfterTransAmt = @BeforeTransAmt + (@providerComm - (@RetailerCommission + @DistributerCommission));

				-- set @AfterTransAmt = @BeforeTransAmt + @providerComm;          
				-----------------         
				UPDATE dbo.UserBalance
				SET CurrentBalance = @AfterTransAmt
				WHERE UserID = @SystemAccount

				DECLARE @Uname VARCHAR(50)

				SELECT @Uname = name
				FROM DBO.UserInformation(NOLOCK)
				WHERE UserID = @UserID;

				DECLARE @TransDesc VARCHAR(200)

				SET @TransDesc = 'Provider Commision Amount:' + CONVERT(VARCHAR, @providerComm) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;

				EXEC dbo.TransactionDetailsInsertByUser @SystemAccount
					,-- @UserID          
					@RechargeID
					,--@RechargeID     
					@ServiceID
					,--@serviceID    
					@UserID
					,--@AssignedUserID    
					@ConsumerNo
					,--@ConsumerNumber     
					'RECHARGE'
					,--@DOCType    
					@BeforeTransAmt
					,--@OpeningBalance         
					@AfterTransAmt
					,--@ClosingBalance        
					0 --@TransactionAmount                                     
					,@providerComm
					,--@DiscountAmount        
					''
					,--@AmountCreditDebit    
					'CREDIT'
					,--@CommissionCreditDebit    
					@TransDesc --@TransactionDescription        
			END
		END TRY

		BEGIN CATCH
			RETURN
		END CATCH
	END
	ELSE --FOR PENDING RECHARGE                                    
	BEGIN
		UPDATE dbo.Recharge
		SET RechargeDescription = 'Request Accepted Successfully'
			,RechargeProviderID = @RechargeProviderID
			,RechargeProviderOpeningBalance = @PrevClosingBal
			,RechargeProviderCommission = @providerComm
			,UpdateDoc = (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))
		WHERE RechargeID = @RechargeId
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeUpdateMannual]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                                    
-- Author:  <Sainath Bhujbal>                                    
-- Create date: <15 March 2017 ,>                                    
-- Description: <Mannual  Recharge success,fail,,>                                    
-- =============================================                        
-- RechargeUpdateMannual 2270,'REFUND','FAILURE','Recharge fail-manual' ,''                  
-- RechargeUpdateMannual 8,'DEDUCTION','SUCCESS','Recharge successful-JAIJUI COMMUNICATION' ,'8','470.0000'                
-- RechargeUpdateMannual 8,'DEDUCTION','SUCCESS','Recharge successful-JAIJUI COMMUNICATION' ,'8'           
-- [RechargeUpdateMannual] 81764, 'DEDUCTION', 'REFUND','Recharge Refund', dbnull , '' , 1      
CREATE PROCEDURE [dbo].[RechargeUpdateMannual] @RechargeID INT
	,@TransType VARCHAR(20)
	,@Status VARCHAR(20)
	,@RechargeDescription VARCHAR(100)
	,@TransactionID VARCHAR(50) = NULL
	,@RechargeProviderClosingBalance NUMERIC(18, 5) = NULL
	,@RechargeProviderID INT = 1
AS
BEGIN
	DECLARE @UserID INT
	DECLARE @OperatorID INT
	DECLARE @ServiceID INT
	DECLARE @Amount NUMERIC(18, 5)
	DECLARE @ConsumerNo VARCHAR(20)
	---declare @RechargeProviderID int        
	DECLARE @Output INT
	DECLARE @Result INT
	DECLARE @success VARCHAR(20) = 'SUCCESS';
	DECLARE @failure VARCHAR(20) = 'FAILURE';
	DECLARE @REFUND VARCHAR(20) = 'REFUND';
	DECLARE @SystemAccount INT = 60;
	DECLARE @OriginalStatus VARCHAR(50)
	DECLARE @ElectricityServiceID INT = 14;

	SELECT @UserID = UserID
		,@OperatorID = OperatorID
		,@ServiceID = ServiceID
		,@Amount = amount
		,@ConsumerNo = ConsumerNumber
		,@OriginalStatus = STATUS
		,@RechargeProviderID = RechargeProviderID
	FROM [dbo].Recharge(NOLOCK)
	WHERE RechargeID = @RechargeID

	IF (@RechargeProviderClosingBalance IS NOT NULL)
	BEGIN
		UPDATE [dbo].[RechargeProviderMaster]
		SET [CurrentBalance] = @RechargeProviderClosingBalance
		WHERE [RechargeProviderID] = @RechargeProviderID
	END

	IF (@RechargeProviderClosingBalance IS NOT NULL)
	BEGIN
		UPDATE [DBO].Recharge
		SET RechargeProviderClosingBalance = @RechargeProviderClosingBalance
		WHERE RechargeID = @RechargeID
	END

	IF (
			upper(@OriginalStatus) = upper(@failure)
			OR upper(@OriginalStatus) = Upper(@REFUND)
			)
	BEGIN
		SET @Output = 1

		PRINT 'Already Failed Recharge'
		PRINT @Output

		RETURN @Output;
	END

	IF (upper(@Status) = upper(@success))
	BEGIN
		SET @TransType = 'DEDUCTION'

		DECLARE @afterTransAmt NUMERIC(18, 5);
		DECLARE @beforeTransAmt NUMERIC(18, 5);

		-- Select current balance before transaction...     
		IF (@ServiceID = @ElectricityServiceID)
		BEGIN
			SELECT @beforeTransAmt = DMRBalance
			FROM dbo.UserBalance
			WHERE UserID = @UserID
		END
		ELSE
		BEGIN
			SELECT @beforeTransAmt = CurrentBalance
			FROM dbo.UserBalance
			WHERE UserID = @UserID
		END

		--Update Recharge status and RechargeDescription,OpeningBalance                            
		UPDATE [DBO].Recharge
		SET TransactionType = @TransType
			,OpeningBalance = @beforeTransAmt
			,STATUS = @Status
			,RechargeDescription = @RechargeDescription
			,TransactionID = @TransactionID
			,RechargeProviderID = @RechargeProviderID
		WHERE RechargeID = @RechargeID

		SELECT @Result = @@IDENTITY

		SET @Output = 1

		--------------Transactiondetails Insert-------------------------------------                              
		DECLARE @PrevClosingBal DECIMAL(18, 5) = 0;

		SET @PrevClosingBal = (
				SELECT TOP (1) RechargeProviderClosingBalance
				FROM dbo.Recharge(NOLOCK)
				WHERE RechargeProviderID = @RechargeProviderID
					AND STATUS != 'PROCESS'
				ORDER BY RechargeID DESC
				)

		IF @PrevClosingBal IS NULL
		BEGIN
			SELECT @PrevClosingBal = CurrentBalance
			FROM dbo.RechargeProviderMaster(NOLOCK)
			WHERE RechargeProviderID = @RechargeProviderID
		END

		PRINT '@PrevClosingBal'
		PRINT @PrevClosingBal

		DECLARE @providerComm NUMERIC(18, 5);
		DECLARE @providerCommType VARCHAR(10) = 'P';

		SELECT @providerComm = isnull(CommissionAdmin, 0)
			,@providerCommType = Upper(MarginType)
		FROM dbo.RechargeProviderOperatorMaster
		WHERE RechargeProviderID = @RechargeProviderID
			AND OperatorID = @OperatorId

		PRINT '@providerComm' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType

		-- print '@providerComm2' + convert(varchar(30),@RechargeProviderID) + ':' +   @OperatorId                   
		IF @providerCommType = 'P'
		BEGIN
			SET @providerComm = (@providerComm * @Amount) / 100;

			PRINT '@providerComm1' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType
		END
		ELSE
		BEGIN
			SET @providerComm = @providerComm;
		END

		-- begin try                              
		IF (@providerComm IS NOT NULL)
		BEGIN
			--Adde For System AccountBal=(ProviderCommission-(RetailerCommission + distributerCommission)                
			DECLARE @RetailerCommission NUMERIC(18, 5) = 0;
			DECLARE @UplineCommission NUMERIC(18, 5) = 0;

			SELECT @RetailerCommission = isnull(DiscountAmount, 0)
			FROM TransactionDetails
			WHERE RechargeID = @RechargeId
				AND Upper(AmountCreditDebit) = Upper('DEBIT')
				AND Upper(DOCType) = Upper('RECHARGE')

			PRINT @RetailerCommission

			SELECT @UplineCommission = ISNULL(SUM(isnull(DiscountAmount, 0)), 0)
			FROM TransactionDetails
			WHERE RechargeID = @RechargeId
				AND AmountCreditDebit = ''
				AND Upper(DOCType) = Upper('RECHARGE')

			DECLARE @benefitCommission NUMERIC(18, 5) = 0;
			SET @benefitCommission = @providerComm - (@RetailerCommission + @UplineCommission)
			--SET @AfterTransAmt = @BeforeTransAmt + @benefitCommission;

			-- set @AfterTransAmt = @BeforeTransAmt + @providerComm;    
			IF (@ServiceID = @ElectricityServiceID)
			BEGIN
				SELECT @beforeTransAmt = DMRBalance
				FROM dbo.UserBalance
				WHERE UserID = @SystemAccount

				UPDATE [DBO].UserBalance
				SET DMRBalance = DMRBalance + @benefitCommission
				WHERE UserID = @SystemAccount

				SELECT @afterTransAmt = DMRBalance
				FROM dbo.UserBalance
				WHERE UserID = @SystemAccount
			END
			ELSE
			BEGIN
				SELECT @beforeTransAmt = CurrentBalance
				FROM dbo.UserBalance
				WHERE UserID = @SystemAccount

				UPDATE [DBO].UserBalance
				SET CurrentBalance = CurrentBalance + @benefitCommission
				WHERE UserID = @SystemAccount

				SELECT @afterTransAmt = CurrentBalance
				FROM dbo.UserBalance
				WHERE UserID = @SystemAccount
			END

			DECLARE @Uname VARCHAR(50)

			SELECT @Uname = name
			FROM DBO.UserInformation(NOLOCK)
			WHERE UserID = @UserID;

			DECLARE @TransDesc VARCHAR(200)

			SET @TransDesc = 'Provider Commision Amount:' + CONVERT(VARCHAR, @benefitCommission) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;

			EXEC dbo.TransactionDetailsInsertByUser @SystemAccount
				,@RechargeID
				,@ServiceID
				,@UserID
				,@ConsumerNo
				,'RECHARGE'
				,@BeforeTransAmt
				,@AfterTransAmt
				,0 --@TransactionAmount                                                 
				,@benefitCommission
				,''
				,'CREDIT'
				,@TransDesc
		END
	END
	ELSE IF (
			Upper(@STATUS) = Upper(@failure)
			OR Upper(@STATUS) = Upper(@REFUND)
			)
	BEGIN
		UPDATE [DBO].Recharge
		SET STATUS = @Status
			,RechargeDescription = @RechargeDescription
			,TransactionType = @TransType
			,TransactionID = @TransactionID
			,RechargeProviderID = @RechargeProviderID
		WHERE RechargeID = @RechargeID

		SELECT @Result = @@IDENTITY

		SET @Output = 1

		--------------Transactiondetails Refund Insert -------------------------------------                     
		EXEC dbo.[TransactionDetailsRefundInsert] @UserID
			,@RechargeID
			,@ServiceID
			,@ConsumerNo
			,@Amount
			,@OperatorID
			,@RechargeProviderID
	END

	RETURN @Output
END

GO
/****** Object:  StoredProcedure [dbo].[RechargeUpdateMannualTesting]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                        
-- Author:  <Sainath Bhujbal>                        
-- ALTER  date: <15 March 2017 ,>                        
-- Description: <Mannual  Recharge success,fail,,>                        
-- =============================================            
-- RechargeUpdateMannual 2270,'REFUND','FAILURE','Recharge fail-manual' ,''      
CREATE PROCEDURE [dbo].[RechargeUpdateMannualTesting] @RechargeID INT
	,@TransType VARCHAR(20)
	,@Status VARCHAR(20)
	,@RechargeDescription VARCHAR(100)
	,@TransactionID VARCHAR(50) = NULL
AS
BEGIN
	DECLARE @UserID INT
	DECLARE @OperatorID INT
	DECLARE @ServiceID INT
	DECLARE @Amount NUMERIC(18, 5)
	DECLARE @ConsumerNo VARCHAR(20)
	DECLARE @RechargeProviderID INT
	DECLARE @Output INT
	DECLARE @Result INT
	DECLARE @success VARCHAR(20) = 'SUCCESS';
	DECLARE @failure VARCHAR(20) = 'FAILURE';
	DECLARE @SystemAccount INT = 60;
	DECLARE @OriginalStatus VARCHAR(50)

	SELECT @UserID = UserID
		,@OperatorID = OperatorID
		,@ServiceID = ServiceID
		,@Amount = amount
		,@ConsumerNo = ConsumerNumber
		,@RechargeProviderID = RechargeProviderID
		,@OriginalStatus = STATUS
	FROM [dbo].Recharge(NOLOCK)
	WHERE RechargeID = @RechargeID

	IF (upper(@OriginalStatus) = upper(@failure))
	BEGIN
		SET @Output = 1

		PRINT @Output
	END
	ELSE
	BEGIN
		IF (upper(@Status) = upper(@success))
		BEGIN
			SET @TransType = 'DEDUCTION'

			DECLARE @afterTransAmt NUMERIC(18, 5);
			DECLARE @beforeTransAmt NUMERIC(18, 5);

			-- Select current balance before transaction...                      
			SELECT @beforeTransAmt = CurrentBalance
			FROM dbo.UserBalance
			WHERE UserID = @UserID

			--Update Recharge status and RechargeDescription,OpeningBalance                
			UPDATE [DBO].Recharge
			SET TransactionType = @TransType
				,OpeningBalance = @beforeTransAmt
				,STATUS = @Status
				,RechargeDescription = @RechargeDescription
				,TransactionID = @TransactionID
			WHERE RechargeID = @RechargeID

			SELECT @Result = @@IDENTITY

			SET @Output = 1

			--------------Transactiondetails Insert-------------------------------------                  
			DECLARE @PrevClosingBal DECIMAL(18, 5) = 0;

			SET @PrevClosingBal = (
					SELECT TOP (1) RechargeProviderClosingBalance
					FROM dbo.Recharge(NOLOCK)
					WHERE RechargeProviderID = @RechargeProviderID
						AND STATUS != 'PROCESS'
					ORDER BY RechargeID DESC
					)

			IF @PrevClosingBal IS NULL
			BEGIN
				SELECT @PrevClosingBal = CurrentBalance
				FROM dbo.RechargeProviderMaster(NOLOCK)
				WHERE RechargeProviderID = @RechargeProviderID
			END

			DECLARE @providerComm NUMERIC(18, 5);
			DECLARE @providerCommType VARCHAR(10) = 'P';

			SELECT @providerComm = isnull(CommissionAdmin, 0)
				,@providerCommType = Upper(MarginType)
			FROM dbo.RechargeProviderOperatorMaster
			WHERE RechargeProviderID = @RechargeProviderID
				AND OperatorID = @OperatorId

			PRINT '@providerComm' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType

			-- print '@providerComm2' + convert(varchar(30),@RechargeProviderID) + ':' +   @OperatorId       
			IF @providerCommType = 'P'
			BEGIN
				SET @providerComm = (@providerComm * @Amount) / 100;

				PRINT '@providerComm1' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType
			END
			ELSE
			BEGIN
				SET @providerComm = @providerComm;
			END

			-- begin try                  
			IF (@providerComm IS NOT NULL)
			BEGIN
				SELECT @BeforeTransAmt = CurrentBalance
				FROM DBO.UserBalance(NOLOCK)
				WHERE UserID = @SystemAccount

				--Adde For System AccountBal=(ProviderCommission-(RetailerCommission + distributerCommission)  
				DECLARE @RetailerCommission NUMERIC(18, 5) = 0;
				DECLARE @DistributerCommission NUMERIC(18, 5) = 0;

				SELECT @RetailerCommission = isnull(DiscountAmount, 0)
				FROM TransactionDetails
				WHERE RechargeID = @RechargeId
					AND AmountCreditDebit = 'DEBIT'
					AND DOCType = 'RECHARGE'

				PRINT @RetailerCommission

				SELECT @DistributerCommission = isnull(DiscountAmount, 0)
				FROM TransactionDetails
				WHERE RechargeID = @RechargeId
					AND AmountCreditDebit = ''
					AND DOCType = 'RECHARGE'

				PRINT @DistributerCommission

				SET @AfterTransAmt = @BeforeTransAmt + (@providerComm - (@RetailerCommission + @DistributerCommission));

				-- set @AfterTransAmt = @BeforeTransAmt + @providerComm;            
				PRINT @AfterTransAmt

				-------------  
				UPDATE dbo.UserBalance
				SET CurrentBalance = @AfterTransAmt
				WHERE UserID = @SystemAccount

				DECLARE @Uname VARCHAR(50)

				SELECT @Uname = name
				FROM DBO.UserInformation(NOLOCK)
				WHERE UserID = @UserID;

				DECLARE @TransDesc VARCHAR(200)

				SET @TransDesc = 'Provider Commision Amount:' + CONVERT(VARCHAR, @providerComm) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;

				EXEC dbo.TransactionDetailsInsertByUser @SystemAccount
					,@RechargeID
					,@ServiceID
					,@UserID
					,@ConsumerNo
					,'RECHARGE'
					,@BeforeTransAmt
					,@AfterTransAmt
					,0 --@TransactionAmount                                     
					,@providerComm
					,''
					,'CREDIT'
					,@TransDesc
			END
		END
		ELSE -- IF (@STATUS = 'FAILURE')          
		BEGIN
			UPDATE [DBO].Recharge
			SET STATUS = @Status
				,RechargeDescription = @RechargeDescription
				,TransactionType = @TransType
				,TransactionID = @TransactionID
			WHERE RechargeID = @RechargeID

			SELECT @Result = @@IDENTITY

			SET @Output = 1

			--------------Transactiondetails Refund Insert -------------------------------------         
			EXEC dbo.[TransactionDetailsRefundInsert] @UserID
				,@RechargeID
				,@ServiceID
				,@ConsumerNo
				,@Amount
				,@OperatorID
				,@RechargeProviderID
		END
	END

	RETURN @Output
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeUpdateRechargeProviderIDByRechrageID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[RechargeUpdateRechargeProviderIDByRechrageID] @RechargeId INT
	,@RechargeProviderId INT
AS
BEGIN
	UPDATE Recharge
	SET [RechargeProviderID] = @RechargeProviderId
	WHERE [RechargeID] = @RechargeId
END



GO
/****** Object:  StoredProcedure [dbo].[RechargeUpdateWithOpeningClosing]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                            
-- Author:  <Sainath Bhujbal>                            
-- ALTER  date: <09 Jun 2017 ,>                            
-- Description: <Recharge Update with TransID,Status,providerOpening,ProviderClosing,,>                            
-- =============================================                   
CREATE PROCEDURE [dbo].[RechargeUpdateWithOpeningClosing] @RechargeId INT
	,@ConsumerNo VARCHAR(20)
	,@Status VARCHAR(20)
	,@RechargeDesc VARCHAR(20)
	,@RechargeProviderID INT
	,@RechargeProviderOpeningBalance NUMERIC(18, 5) = NULL
	,@RechargeProviderClosingBalance NUMERIC(18, 5) = NULL
	,@OperatorId INT
	,@ServiceID INT
	,@Amount NUMERIC(18, 5) = 0
	,@TransID VARCHAR(50)
AS
BEGIN
	DECLARE @providerComm NUMERIC(18, 5) = 0;
	DECLARE @providerCommType VARCHAR(10) = 'P';
	DECLARE @providerOpBal NUMERIC(18, 5) = 0;
	DECLARE @AfterTransAmt NUMERIC(18, 5)
	DECLARE @BeforeTransAmt NUMERIC(18, 5) = 0;
	DECLARE @TRANS_DESC VARCHAR(max)
	DECLARE @UserID INT
	DECLARE @ProviderOpeningBalance DECIMAL(18, 5) = 0;
	DECLARE @ProviderClosingBalance DECIMAL(18, 5) = 0;
	--declare @PrevClosingBal decimal(18, 5)= 0;                
	DECLARE @Success VARCHAR(20) = 'SUCCESS';
	DECLARE @SystemAccount INT = 60;

	SELECT @UserID = UserID
	FROM dbo.Recharge(NOLOCK)
	WHERE RechargeID = @RechargeId

	IF (
			@RechargeProviderOpeningBalance IS NULL
			OR @RechargeProviderOpeningBalance = 0
			)
	BEGIN
		SET @ProviderOpeningBalance = (
				SELECT TOP (1) RechargeProviderClosingBalance
				FROM dbo.Recharge(NOLOCK)
				WHERE RechargeProviderID = @RechargeProviderID
					AND STATUS != 'PROCESS'
				ORDER BY RechargeID DESC
				)
	END
	ELSE
	BEGIN
		SET @ProviderOpeningBalance = @RechargeProviderOpeningBalance;
	END

	IF (
			@RechargeProviderClosingBalance IS NULL
			OR @RechargeProviderClosingBalance = 0
			)
	BEGIN
		SET @ProviderClosingBalance = NULL;
	END
	ELSE
	BEGIN
		SET @ProviderClosingBalance = @RechargeProviderClosingBalance;
	END

	IF @ProviderOpeningBalance IS NULL
	BEGIN
		SELECT @ProviderClosingBalance = CurrentBalance
		FROM dbo.RechargeProviderMaster(NOLOCK)
		WHERE RechargeProviderID = @RechargeProviderID
	END

	-----------------------------End------------------------------------         
	SELECT @providerComm = isnull(CommissionAdmin, 0)
		,@providerCommType = Upper(MarginType)
	FROM dbo.RechargeProviderOperatorMaster
	WHERE RechargeProviderID = @RechargeProviderID
		AND OperatorID = @OperatorId

	PRINT '@providerComm' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType

	IF @providerCommType = 'P'
	BEGIN
		SET @providerComm = (@providerComm * @Amount) / 100;

		PRINT '@providerComm1' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType
	END
	ELSE
	BEGIN
		SET @providerComm = @providerComm;
	END

	IF (upper(@Status) = upper(@Success))
	BEGIN
		UPDATE dbo.Recharge
		SET STATUS = @Status
			--, Request_Id = @RequestId                
			,RechargeDescription = @RechargeDesc
			,RechargeProviderID = @RechargeProviderID
			,RechargeProviderOpeningBalance = @ProviderOpeningBalance
			,RechargeProviderClosingBalance = @ProviderClosingBalance
			,RechargeProviderCommission = @providerComm
			,UpdateDoc = (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))
			,TransactionID = @TransID
		WHERE RechargeID = @RechargeId

		BEGIN TRY
			IF (@providerComm IS NOT NULL)
			BEGIN
				SELECT @BeforeTransAmt = CurrentBalance
				FROM DBO.UserBalance(NOLOCK)
				WHERE UserID = @SystemAccount

				SET @AfterTransAmt = @BeforeTransAmt + @providerComm;

				UPDATE dbo.UserBalance
				SET CurrentBalance = @AfterTransAmt
				WHERE UserID = @SystemAccount

				DECLARE @Uname VARCHAR(50)

				SELECT @Uname = name
				FROM DBO.UserInformation(NOLOCK)
				WHERE UserID = @UserID;

				DECLARE @TransDesc VARCHAR(200)

				SET @TransDesc = 'Provider Commision Amount:' + CONVERT(VARCHAR, @providerComm) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;

				EXEC dbo.TransactionDetailsInsertByUser @SystemAccount
					,-- @UserID        
					@RechargeID
					,--@RechargeID   
					@ServiceID
					,--@serviceID  
					@UserID
					,--@AssignedUserID  
					@ConsumerNo
					,--@ConsumerNumber   
					'RECHARGE'
					,--@DOCType  
					@BeforeTransAmt
					,--@OpeningBalance       
					@AfterTransAmt
					,--@ClosingBalance      
					0 --@TransactionAmount                                   
					,@providerComm
					,--@DiscountAmount      
					''
					,--@AmountCreditDebit  
					'CREDIT'
					,--@CommissionCreditDebit  
					@TransDesc --@TransactionDescription      
			END
		END TRY

		BEGIN CATCH
			RETURN
		END CATCH
	END
	ELSE --FOR PENDING RECHARGE                                  
	BEGIN
		UPDATE dbo.Recharge
		SET RechargeDescription = 'Request Accepted Successfully'
			,RechargeProviderID = @RechargeProviderID
			,RechargeProviderOpeningBalance = @ProviderOpeningBalance
			,RechargeProviderClosingBalance = @ProviderClosingBalance
			,RechargeProviderCommission = @providerComm
			,UpdateDoc = (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))
		WHERE RechargeID = @RechargeId
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RefundAccept]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RefundAccept] @RechargeID INT
	,@TransType VARCHAR(20)
	,@Status VARCHAR(20)
	,@RechargeDescription VARCHAR(100)
	,@RefundID INT
	,@UserID INT
	,@Remark VARCHAR(1000)
AS
BEGIN
	UPDATE RefundMaster
	SET STATUS = 'Solved'
		,[Remarks] = @Remark
	WHERE RechargeID = @RechargeID
		AND RefundID = @RefundID

	EXEC RechargeUpdateMannual @RechargeID
		,@TransType
		,@Status
		,@RechargeDescription
END



GO
/****** Object:  StoredProcedure [dbo].[RefundAcceptManually]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RefundAcceptManually] @RechargeID INT
	,@TransType VARCHAR(20)
	,@Status VARCHAR(20)
	,@RechargeDescription VARCHAR(100)
	,@RefundID INT
	,@UserID INT
	,@Amount DECIMAL(18, 5)
	,@CustomerNumber VARCHAR(200)
	,@Remark VARCHAR(1000)
AS
BEGIN
	UPDATE RefundMaster
	SET STATUS = 'Solved'
		,[Remarks] = @Remark
	WHERE RechargeID = @RechargeID
		AND RefundID = @RefundID

	EXEC RefundMasterAcceptManually @TransType
		,@Status
		,@RechargeDescription
		,@UserID
		,@Amount
		,@CustomerNumber
END



GO
/****** Object:  StoredProcedure [dbo].[RefundInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :23-2-17
--Purpose Refund Insert
--==========================================
--RefundInsert 3,10,1234,'Prepaid','Test',7083741815,1
--RefundInsert 3,10,0,'Prepaid','Test','1234567890',0,'Manual'
CREATE PROCEDURE [dbo].[RefundInsert] @UserID INT
	,@Amount INT
	,@RechargeID INT
	,@Subject VARCHAR(50)
	,@Description VARCHAR(90)
	,@CustomerNumber VARCHAR(50)
	,@OperatorID INT
	,@ComplaintType VARCHAR(450)
AS
BEGIN
	DECLARE @UserTypeID INT;

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
				AND IsActive = 1
			);

	IF (
			@UserTypeID = 3
			OR @UserTypeID = 7
			OR @UserTypeID = 4
			)
	BEGIN
		IF (@ComplaintType = 'Auto')
			PRINT '1';

		BEGIN
			DECLARE @Amount1 INT
				,@DOR DATETIME;

			SET @Amount1 = (
					SELECT TOP 1 Amount
					FROM Recharge
					WHERE RechargeID = @RechargeID
						AND OperatorID = @OperatorID
						AND ConsumerNumber = @CustomerNumber
					);
			SET @DOR = (
					SELECT DOC
					FROM Recharge
					WHERE RechargeID = @RechargeID
						AND OperatorID = @OperatorID
						AND ConsumerNumber = @CustomerNumber
					);

			IF (@Amount = @Amount1)
			BEGIN
				IF NOT EXISTS (
						SELECT RechargeID
						FROM RefundMaster
						WHERE RechargeID = @RechargeID
							AND OperatorID = @OperatorID
						)
				BEGIN
					INSERT INTO RefundMaster (
						UserID
						,RechargeID
						,Remarks
						,DOR
						,OperatorID
						,ComplaintType
						,Type
						,Amount
						,ConsumerNumber
						)
					VALUES (
						@UserID
						,@RechargeID
						,@Description
						,@DOR
						,@OperatorID
						,@Subject
						,@ComplaintType
						,@Amount
						,@CustomerNumber
						)

					UPDATE Recharge
					SET IsDispute = 1
					WHERE RechargeID = @RechargeID;
				END
			END
			ELSE
			BEGIN
				IF (@Subject != 'Other')
				BEGIN
					INSERT INTO RefundMaster (
						UserID
						,RechargeID
						,Remarks
						,DOR
						,OperatorID
						,ComplaintType
						,Type
						,Amount
						,ConsumerNumber
						)
					VALUES (
						@UserID
						,@RechargeID
						,@Description
						,@DOR
						,@OperatorID
						,@Subject
						,@ComplaintType
						,@Amount
						,@CustomerNumber
						)
				END
				ELSE
				BEGIN
					SET @DOR = getdate();

					INSERT INTO RefundMaster (
						UserID
						,RechargeID
						,Remarks
						,DOR
						,OperatorID
						,ComplaintType
						,Type
						,ConsumerNumber
						)
					VALUES (
						@UserID
						,@RechargeID
						,@Description
						,@DOR
						,@OperatorID
						,@Subject
						,@ComplaintType
						,@CustomerNumber
						)
				END
			END
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[RefundMasterAcceptManually]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                            
-- Author:  <Sainath Bhujbal>                            
-- ALTER  date: <15 March 2017 ,>                            
-- Description: <Mannual  Recharge success,fail,,>                            
-- =============================================           
-- RechargeUpdateMannual 2270,'REFUND','FAILURE','Recharge fail-manual' ,''          
-- RechargeUpdateMannual 8,'DEDUCTION','SUCCESS','Recharge successful-JAIJUI COMMUNICATION' ,'8','470.0000'        
-- RechargeUpdateMannual 8,'DEDUCTION','SUCCESS','Recharge successful-JAIJUI COMMUNICATION' ,'8'       
CREATE PROCEDURE [dbo].[RefundMasterAcceptManually] @TransType VARCHAR(20)
	,@Status VARCHAR(20)
	,@RechargeDescription VARCHAR(100)
	,@UserID INT
	,@Amount DECIMAL(18, 5)
	,@CustomerNumber VARCHAR(100)
AS
BEGIN
	DECLARE @BeforeAmount DECIMAL(18, 5);
	DECLARE @AfterTransAmt DECIMAL(18, 5);

	SELECT @BeforeAmount = CurrentBalance
	FROM UserBalance
	WHERE UserID = @UserID

	SET @AfterTransAmt = (@BeforeAmount + @Amount);

	UPDATE dbo.UserBalance
	SET CurrentBalance = @AfterTransAmt
	WHERE UserID = @UserID

	DECLARE @Uname VARCHAR(50)

	SELECT @Uname = name
	FROM DBO.UserInformation(NOLOCK)
	WHERE UserID = @UserID;

	DECLARE @TransDesc VARCHAR(200)

	SET @TransDesc = 'TRANSACTION MANUAL REFUND:' + CONVERT(VARCHAR, (@BeforeAmount + @Amount)) + ' TO' + @Uname;

	EXEC dbo.TransactionDetailsInsertByUserRefundManually @UserID
		,@BeforeAmount
		,@AfterTransAmt
		,@TransDesc
		,@CustomerNumber
		,@Amount
END



GO
/****** Object:  StoredProcedure [dbo].[RefundReject]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RefundReject] @RechargeID INT
	,@Status VARCHAR(20)
	,@RefundID INT
	,@Remark VARCHAR(1000)
AS
BEGIN
	UPDATE RefundMaster
	SET STATUS = @Status
		,[Remarks] = @Remark
	WHERE RechargeID = @RechargeID
		AND RefundID = @RefundID
END



GO
/****** Object:  StoredProcedure [dbo].[RefundSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :28-2-17
--Purpose Refund Select
--==========================================
--RefundSelectByStatus 1,'Unresolved','12/3/2017','10/3/2018'
CREATE PROCEDURE [dbo].[RefundSelect] @UserID INT
	,@Status VARCHAR(10)
	,@FromDate VARCHAR(250) = NULL
	,@ToDate VARCHAR(250) = NULL
AS
BEGIN
	DECLARE @UserTypeID INT;
if(upper(@Status)=upper('Pending'))
Begin
set @Status = 'Unresolved'
End

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	IF (@UserTypeID = 1)
	BEGIN
		IF (@Status = 'Unresolved')
		BEGIN
			SELECT *
			FROM (
				SELECT TOP (1000) RM.RefundID
					,RM.RechargeID
					,convert(VARCHAR(12), RM.DOR, 113) + right(convert(VARCHAR(39), RM.DOR, 22), 11) AS DOR
					,RM.Description
					,convert(VARCHAR(12), RM.DOC, 113) + right(convert(VARCHAR(39), RM.DOC, 22), 11) AS DOC
					,R.ConsumerNumber AS ConsumerNumber
					,R.Amount AS Amount
					,R.STATUS
					,RM.STATUS AS RMStatus
					,RM.UserID
					,UI.name AS Sender
					,RM.Type
					,UI.UserTypeID
					,RM.Remarks
				FROM dbo.Recharge R(NOLOCK)
				INNER JOIN dbo.RefundMaster RM(NOLOCK) ON R.RechargeID = RM.RechargeID
				INNER JOIN dbo.UserInformation UI(NOLOCK) ON RM.UserID = UI.UserID
				WHERE Type = 'Auto'
					AND RM.STATUS = @Status
					AND (
						convert(DATE, RM.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, RM.DOC, 103) <= convert(DATE, @ToDate, 103)
						)
				
				UNION
				
				SELECT TOP (1000) RM.RefundID
					,RM.RechargeID
					,convert(VARCHAR(12), RM.DOR, 113) + right(convert(VARCHAR(39), RM.DOR, 22), 11) AS DOR
					,RM.Description
					,convert(VARCHAR(12), RM.DOC, 113) + right(convert(VARCHAR(39), RM.DOC, 22), 11) AS DOC
					,RM.ConsumerNumber AS ConsumerNumber
					,RM.Amount AS Amount
					,'' AS STATUS
					,RM.STATUS AS RMStatus
					,RM.UserID
					,UI.name AS Sender
					,RM.Type
					,UI.UserTypeID
					,RM.Remarks
				FROM dbo.RefundMaster RM(NOLOCK)
				INNER JOIN dbo.UserInformation UI(NOLOCK) ON RM.UserID = UI.UserID
				WHERE Type = 'Manual'
					AND RM.STATUS = @Status
					AND (
						convert(DATE, RM.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, RM.DOC, 103) <= convert(DATE, @ToDate, 103)
						)
				) t
			ORDER BY convert(DATETIME, DOC, 103) DESC;
		END
		ELSE
		BEGIN
			SELECT *
			FROM (
				SELECT TOP (1000) RM.RefundID
					,RM.RechargeID
					,convert(VARCHAR(12), RM.DOR, 113) + right(convert(VARCHAR(39), RM.DOR, 22), 11) AS DOR
					,RM.Description
					,convert(VARCHAR(12), RM.DOC, 113) + right(convert(VARCHAR(39), RM.DOC, 22), 11) AS DOC
					,R.ConsumerNumber AS ConsumerNumber
					,R.Amount AS Amount
					,R.STATUS
					,RM.STATUS AS RMStatus
					,RM.UserID
					,UI.name AS Sender
					,RM.Type
					,UI.UserTypeID
					,RM.Remarks
				FROM dbo.Recharge R(NOLOCK)
				INNER JOIN dbo.RefundMaster RM(NOLOCK) ON R.RechargeID = RM.RechargeID
				INNER JOIN dbo.UserInformation UI(NOLOCK) ON RM.UserID = UI.UserID
				WHERE Type = 'Auto'
					AND RM.STATUS = @Status
					AND (
						convert(DATE, RM.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, RM.DOC, 103) <= convert(DATE, @ToDate, 103)
						)
				
				UNION
				
				SELECT TOP (1000) RM.RefundID
					,RM.RechargeID
					,convert(VARCHAR(12), RM.DOR, 113) + right(convert(VARCHAR(39), RM.DOR, 22), 11) AS DOR
					,RM.Description
					,convert(VARCHAR(12), RM.DOC, 113) + right(convert(VARCHAR(39), RM.DOC, 22), 11) AS DOC
					,RM.ConsumerNumber AS ConsumerNumber
					,RM.Amount AS Amount
					,'' AS STATUS
					,RM.STATUS AS RMStatus
					,RM.UserID
					,UI.name AS Sender
					,RM.Type
					,UI.UserTypeID
					,RM.Remarks
				FROM dbo.RefundMaster RM(NOLOCK)
				INNER JOIN dbo.UserInformation UI(NOLOCK) ON RM.UserID = UI.UserID
				WHERE Type = 'Manual'
					AND RM.STATUS = @Status
					AND (
						convert(DATE, RM.DOC, 103) >= convert(DATE, @FromDate, 103)
						AND convert(DATE, RM.DOC, 103) <= convert(DATE, @ToDate, 103)
						)
				) t
			ORDER BY convert(DATETIME, DOC, 103) DESC;
		END
	END

	IF (@UserTypeID = 3)
	BEGIN
		SELECT *
		FROM (
			SELECT TOP (1000) RM.RefundID
				,RM.RechargeID
				,convert(VARCHAR(12), RM.DOR, 113) + right(convert(VARCHAR(39), RM.DOR, 22), 11) AS DOR
				,RM.Description
				,convert(VARCHAR(12), RM.DOC, 113) + right(convert(VARCHAR(39), RM.DOC, 22), 11) AS DOC
				,R.ConsumerNumber AS ConsumerNumber
				,R.Amount AS Amount
				,R.STATUS
				,RM.STATUS AS RMStatus
				,RM.UserID
				,UI.name AS Sender
				,RM.Type
				,UI.UserTypeID
				,RM.Remarks
			FROM dbo.Recharge R(NOLOCK)
			INNER JOIN dbo.RefundMaster RM(NOLOCK) ON R.RechargeID = RM.RechargeID
			INNER JOIN dbo.UserInformation UI(NOLOCK) ON RM.UserID = UI.UserID
			WHERE Type = 'Auto'
				AND RM.UserID = @UserID
				AND RM.STATUS = @Status
				AND (
					convert(DATE, RM.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, RM.DOC, 103) <= convert(DATE, @ToDate, 103)
					)
			
			UNION
			
			SELECT TOP (1000) RM.RefundID
				,RM.RechargeID
				,convert(VARCHAR(12), RM.DOR, 113) + right(convert(VARCHAR(39), RM.DOR, 22), 11) AS DOR
				,RM.Description
				,convert(VARCHAR(12), RM.DOC, 113) + right(convert(VARCHAR(39), RM.DOC, 22), 11) AS DOC
				,RM.ConsumerNumber AS ConsumerNumber
				,RM.Amount AS Amount
				,'' AS STATUS
				,RM.STATUS AS RMStatus
				,RM.UserID
				,UI.name AS Sender
				,RM.Type
				,UI.UserTypeID
				,RM.Remarks
			FROM dbo.RefundMaster RM(NOLOCK)
			INNER JOIN dbo.UserInformation UI(NOLOCK) ON RM.UserID = UI.UserID
			WHERE Type = 'Manual'
				AND RM.STATUS = @Status
				AND RM.UserID = @UserID
				AND (
					convert(DATE, RM.DOC, 103) >= convert(DATE, @FromDate, 103)
					AND convert(DATE, RM.DOC, 103) <= convert(DATE, @ToDate, 103)
					)
			) t
		ORDER BY convert(DATETIME, DOC, 103) DESC;
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RefundSelectByStatus]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :28-2-17
--Purpose Refund Select By Status
--==========================================
--RefundSelectByStatus 1,'Solved','12/3/2017','10/5/2017'
CREATE PROCEDURE [dbo].[RefundSelectByStatus] @UserID INT
	,@FromDate VARCHAR(250)
	,@ToDate VARCHAR(250)
	,@Status VARCHAR(250)
AS
BEGIN
	DECLARE @UserTypeID INT;

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	IF (@UserTypeID = 1)
	BEGIN
		SELECT TOP (1000) RM.RefundID
			,RM.RechargeID
			,convert(VARCHAR(12), RM.DOR, 113) + right(convert(VARCHAR(39), RM.DOR, 22), 11) AS DOR
			,convert(VARCHAR(12), RM.DOC, 113) + right(convert(VARCHAR(39), RM.DOC, 22), 11) AS DOC
			,R.ConsumerNumber
			,R.Amount
			,R.STATUS
			,RM.STATUS AS RMStatus
			,RM.UserID
			,UI.name AS Sender
			,UI.UserTypeID
			,RM.Remarks
		FROM dbo.Recharge R(NOLOCK)
		INNER JOIN dbo.RefundMaster RM(NOLOCK) ON R.RechargeID = RM.RechargeID
		INNER JOIN dbo.UserInformation UI(NOLOCK) ON RM.UserID = UI.UserID
		WHERE RM.STATUS = @Status
			AND (
				convert(DATE, RM.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, RM.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, RM.DOC, 103) DESC;
	END

	IF (@UserTypeID = 3)
	BEGIN
		SELECT TOP (1000) RM.RefundID
			,RM.RechargeID
			,convert(VARCHAR(12), RM.DOR, 113) + right(convert(VARCHAR(39), RM.DOR, 22), 11) AS DOR
			,convert(VARCHAR(12), RM.DOC, 113) + right(convert(VARCHAR(39), RM.DOC, 22), 11) AS DOC
			,R.ConsumerNumber
			,R.Amount
			,R.STATUS
			,RM.STATUS AS RMStatus
			,RM.UserID
			,UI.name AS Sender
			,UI.UserTypeID
			,RM.Remarks
		FROM dbo.Recharge R(NOLOCK)
		INNER JOIN dbo.RefundMaster RM(NOLOCK) ON R.RechargeID = RM.RechargeID
		INNER JOIN dbo.UserInformation UI(NOLOCK) ON RM.UserID = UI.UserID
		WHERE RM.UserID = @UserID
			AND RM.STATUS = @Status
			AND (
				convert(DATE, RM.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, RM.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
		ORDER BY convert(DATETIME, RM.DOC, 103) DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[RefundValidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :21-2-17
--Purpose Refund Validation 
--==========================================
CREATE PROCEDURE [dbo].[RefundValidation] @RechargeID INT
AS
BEGIN
	SELECT *
	FROM RefundMaster
	WHERE RechargeID = @RechargeID
		AND Type = 'Auto'
END



GO
/****** Object:  StoredProcedure [dbo].[ResetPin]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ResetPin] @UserID INT
	,@MobileNumber VARCHAR(100)
	,@RechargePin VARCHAR(50)
AS
BEGIN
	UPDATE UserInformation
	SET RechargePin = @RechargePin
	WHERE UserID = @UserID
		AND MobileNumber = @MobileNumber;

	SELECT UserID
		,EmailID
		,MobileNumber
		,RechargePin
	FROM UserInformation
	WHERE UserID = @UserID
		AND MobileNumber = @MobileNumber;
END



GO
/****** Object:  StoredProcedure [dbo].[ResponseGroupInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================  
--Auther :Priyanka  
--Date :10-2-17  
--Purpose ServiceMaster Insert  
--==========================================  
--ServiceInsert Electricity  
CREATE PROCEDURE [dbo].[ResponseGroupInsert] --GroupCommissionInsert 'GOld'  
 @RechargeProviderID INT  
 ,@TextMustExist VARCHAR(100)  
 ,@TextExpression VARCHAR(1000)  
 ,@Type VARCHAR(50)  
 ,@TrnStatus VARCHAR(10)  
 ,@IsDelete INT  
 ,@IsRefCodePresent Int = 0
AS  
BEGIN  
 INSERT INTO [dbo].[ResponseGroup] (  
  RechargeProviderID  
  ,[TextMustExist]  
  ,[RegularExpression]  
  ,[Type]  
  ,[TrxStatus]  
  ,[IsDelete]  
  ,[IsRefcodePresent]
  )  
 VALUES (  
  @RechargeProviderID  
  ,@TextMustExist  
  ,@TextExpression  
  ,@Type  
  ,@TrnStatus  
  ,@IsDelete  
  ,@IsRefCodePresent
  )  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[ResponseGroupSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================  
--Auther :Priyanka  
--Date :10-7-17  
--Purpose GroupCommission Select  
--==========================================  
CREATE PROCEDURE [dbo].[ResponseGroupSelect] @RechargeProviderID INT  
AS  
BEGIN  
 SELECT [ID]  
  ,[RechargeProviderID]  
  ,[TextMustExist]  
  ,[Type]  
  ,[TrxStatus]  
  ,[RegularExpression]  
  ,[IsDelete]  
  ,[IsRefCodePresent]
 FROM [dbo].[ResponseGroup](NOLOCK)  
 WHERE [RechargeProviderID] = @RechargeProviderID  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[ResponseGroupUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================  
--Auther :Priyanka  
--Date :10-2-17  
--Purpose ServiceMaster Insert  
--==========================================  
--ServiceInsert Electricity  
CREATE PROCEDURE [dbo].[ResponseGroupUpdate] --GroupCommissionInsert 'GOld'  
 @ID INT  
 ,@RechargeProviderID INT  
 ,@TextMustExist VARCHAR(100)  
 ,@TextExpression VARCHAR(1000)  
 ,@Type VARCHAR(50)  
 ,@TrnStatus VARCHAR(10)  
 ,@IsDelete INT  
 ,@IsRefCodePresent Int = 0
AS  
BEGIN  
 UPDATE [dbo].[ResponseGroup]  
 SET [RechargeProviderID] = @RechargeProviderID  
  ,[TextMustExist] = @TextMustExist  
  ,[RegularExpression] = @TextExpression  
  ,[Type] = @Type  
  ,[TrxStatus] = @TrnStatus  
  ,[IsDelete] = @IsDelete  
  ,[IsRefCodePresent] = @IsRefCodePresent
 WHERE [ID] = @ID  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[ResponseKeySelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[ResponseKeySelect]
AS
BEGIN
	SELECT *
	FROM ResponseKeyValue
END



GO
/****** Object:  StoredProcedure [dbo].[ResponseKeyValueSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[ResponseKeyValueSelect] --ResponseParametersSelect 1
	@key VARCHAR(50)
AS
BEGIN
	SELECT [Value]
	FROM [dbo].[ResponseKeyValue]
	WHERE [Response] = @key
END



GO
/****** Object:  StoredProcedure [dbo].[ResponseParametersSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--ResponseParametersSelect 3,'callback'
CREATE PROC [dbo].[ResponseParametersSelect] @RechargeProviderId INT
,@Type VARCHAR(20)
AS
BEGIN
SELECT [RegularExpression] AS SuccessMessage
 ,[TextMustExist]
FROM [dbo].[ResponseGroup](NOLOCK)
WHERE [RechargeProviderID] = @RechargeProviderId
 AND [TrxStatus] = 'SUCCESS'
 AND Type = @Type
ORDER BY LEN([TextMustExist]) DESC ,LEN([RegularExpression]) DESC

SELECT [RegularExpression] AS FailureMessage
 ,[TextMustExist]
FROM [dbo].[ResponseGroup](NOLOCK)
WHERE [RechargeProviderID] = @RechargeProviderId
 AND [TrxStatus] = 'FAILURE'
 AND Type = @Type
ORDER BY LEN([TextMustExist]) DESC  ,LEN([RegularExpression]) DESC

SELECT [RegularExpression] AS PendingMessage
 ,[TextMustExist]
FROM [dbo].[ResponseGroup](NOLOCK)
WHERE [RechargeProviderID] = @RechargeProviderId
 AND [TrxStatus] = 'PENDING'
 AND Type = @Type
ORDER BY LEN([TextMustExist]) DESC  ,LEN([RegularExpression]) DESC
END
GO
/****** Object:  StoredProcedure [dbo].[ReverseUplineCommission]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                                      
-- Author:  <Sainath Bhujbal>                                      
-- Create date: <29 Dec 2017 ,>                                      
-- Description: <For Reverse upline commision  of distributor and super distributor,,>             
-- =============================================                                 
            
CREATE PROCedure [dbo].[ReverseUplineCommission]         
@ServiceID int,     
@RechargeId int,     
@ParentID int,         
@RetailerName varchar(50),    
@UserID int,    
@ConsumerNo varchar(20)          
as          
begin          
 --Added For Electricity Bill            
 declare @dcomAmt numeric(18, 5)          
 declare @electricityServiceID int = 14;          
 declare @afterTransAmt numeric(18, 5);          
 declare @beforeTransAmt numeric(18, 5);          
 declare @TransDesc varchar(200);          
 declare @DMRServiceID int = 21;        
          
 if (@ServiceID = @electricityServiceID)          
 begin          
  select @dcomAmt = (          
    select top (1) DiscountAmount          
    from dbo.TransactionDetails(nolock)          
    where RechargeID = @RechargeId          
     and DOCType = 'ElectricityBill'          
     and AmountCreditDebit = ''          
     and CommissionCreditDebit = 'CREDIT'          
     and UserID = @ParentID          
    )          
 end          
 else if (@ServiceID = @DMRServiceID)          
 begin          
  select @dcomAmt = (          
    select top (1) DiscountAmount          
    from dbo.TransactionDetails(nolock)          
    where RechargeID = @RechargeId          
     and DOCType = 'MoneyTransfer'          
     and AmountCreditDebit = ''          
     and CommissionCreditDebit = 'CREDIT'          
     and UserID = @ParentID          
    )          
 end          
 else         
         
 begin          
  select @dcomAmt = (          
    select top (1) DiscountAmount          
    from dbo.TransactionDetails(nolock)          
    where RechargeID = @RechargeId          
     and DOCType = 'RECHARGE'          
     and AmountCreditDebit = ''          
     and CommissionCreditDebit = 'CREDIT'          
     and UserID = @ParentID          
    )          
 end          
          
 print '@dcomAmt' + convert(varchar(30), @dcomAmt) + ':'          
          
 -- IF (@comAmt != 0)                        
 begin          
  set @beforeTransAmt = 0;          
          
  select @beforeTransAmt = DMRBalance          
  from dbo.UserBalance          
  where UserID = @ParentID          
          
  update [DBO].UserBalance          
  set CurrentBalance = (DMRBalance - @dcomAmt)          
  where UserID = @ParentID          

  set @afterTransAmt = 0;
  select @afterTransAmt = (@beforeTransAmt - @dcomAmt)          
     
  set @TransDesc = 'DEBIT Reversed Commision Of Rs. ' + CONVERT(varchar, CONVERT(decimal(18, 5), @dcomAmt)) + 'For ServiceID Of ' + Convert(varchar, @ServiceID) + ' By ' + @RetailerName;          
          
  --UPLINE COMM AND BALANCE                            
  exec dbo.TransactionDetailsInsertByUser @ParentID, @RechargeID, @ServiceID, @UserID, @ConsumerNo, 'REVERSE', @beforeTransAmt, @afterTransAmt, 0 --@TransactionAmount                                               
   , @dcomAmt, 'DEBIT', 'DEBIT', @TransDesc          
 end          
end 


GO
/****** Object:  StoredProcedure [dbo].[RPCDeleteSequence]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RPCDeleteSequence] @OperatorID INT
AS
BEGIN
	DELETE
	FROM RechargeProviderConfiguration
	WHERE OperatorID = @OperatorID
END



GO
/****** Object:  StoredProcedure [dbo].[RPCDeleteSequenceUserwise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RPCDeleteSequenceUserwise] @OperatorID INT
	,@UserID INT
AS
BEGIN
	DELETE
	FROM RechargeProviderConfigurationUserwise
	WHERE OperatorID = @OperatorID
		AND UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[RPCInsertSequence]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :10-2-17
--Purpose Recharge Provider configuration Insert Sequence
--==========================================
--RPCInsertSequence 1,24,1
CREATE PROCEDURE [dbo].[RPCInsertSequence] @OperatorID INT
	,@ProviderID INT
	,@Sequence INT
AS
BEGIN
	INSERT INTO RechargeProviderConfiguration (
		OperatorID
		,ProviderID
		,Sequence
		)
	VALUES (
		@OperatorID
		,@ProviderID
		,@Sequence
		);
END



GO
/****** Object:  StoredProcedure [dbo].[RPCInsertSequenceUserwise]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================    
--Auther : Sushant Yelpale    
--Date : 02-07-2018    
--Purpose: Recharge Provider configuration Insert Sequence    
--==========================================    
--[RPCInsertSequenceUserwise] 1,24,1    
CREATE PROCEDURE [dbo].[RPCInsertSequenceUserwise] @UserID INT
	,@OperatorID INT
	,@ProviderID INT
	,@Sequence INT
AS
BEGIN
	INSERT INTO RechargeProviderConfigurationUserwise (
		UserID
		,OperatorID
		,ProviderID
		,Sequence
		)
	VALUES (
		@UserID
		,@OperatorID
		,@ProviderID
		,@Sequence
		);
END



GO
/****** Object:  StoredProcedure [dbo].[selectAPIBalance]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selectAPIBalance]
AS
BEGIN
	SELECT RechargeProviderID
		,RechargeProviderName
		,IsNull(CurrentBalance, 0) AS CurrentBalance
	FROM RechargeProviderMaster
	WHERE RechargeProviderID NOT IN (8) --not in (1,8)
END



GO
/****** Object:  StoredProcedure [dbo].[selectAPIBalanceCount]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--selectAPIBalance

--==================
--ALTER d by :Rahul Hembade
--Reson:API Count with balance
--=======================
CREATE PROCEDURE [dbo].[selectAPIBalanceCount]
AS
BEGIN
	SELECT count(RechargeProviderID) AS APICount
		,Sum(IsNull(CurrentBalance, 0)) AS APIBalance
	FROM [RechargeProviderMaster]
END



GO
/****** Object:  StoredProcedure [dbo].[SelectBeneficiaryID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SelectBeneficiaryID] @Mobile VARCHAR(50)
	,@AccountNo VARCHAR(100)
	,@IFSC VARCHAR(100)
AS
BEGIN
	SELECT isnull([BeneficiaryID], 0) AS [BeneficiaryID]
	FROM [Beneficiary]
	WHERE [AccountNo] = @AccountNo
		AND [Mobile] = @Mobile
		AND [IFSC] = @IFSC
END



GO
/****** Object:  StoredProcedure [dbo].[SelectCommissionWallet]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[SelectCommissionWallet] @UserID INT
	,@FromDate VARCHAR(100)
	,@ToDate VARCHAR(100)
	,@BalanceType VARCHAR(100) = 'all'
	 
AS
BEGIN
	DECLARE @UserIDDB INT = @UserID
	DECLARE @FromDateDB VARCHAR(100) = @FromDate
	DECLARE @ToDateDB VARCHAR(100) = @ToDate
	DECLARE @userType VARCHAR(20);
	DECLARE @admin VARCHAR(20) = 'ADMIN';
	DECLARE @SuperDistributor VARCHAR(20) = 'SuperDistributor';
	DECLARE @SystemAccount INT = 60

	

	if(@BalanceType !='All')

	begin 

	SELECT @userType = UTM.UserType
	FROM [dbo].UserInformation(NOLOCK) UI
	INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @UserIDDB

	IF (Upper(@userType) = Upper(@admin))
	BEGIN
		SELECT TOP 10000 *
		FROM (
			SELECT DISTINCT TOP 10000 RE.DOC AS MainDOC
				,convert(VARCHAR(12), RE.DOC, 113) + right(convert(VARCHAR(39), RE.DOC, 22), 11) AS DOC
				,RE.ConsumerNumber
				,OM.OperatorName
				,RPM.RechargeProviderName
				,RE.Amount
				,CONCAT (
					UI.UserName
					,'-'
					,UTM.UserType
					) AS RetailerUserName
				,CONCAT (
					UIN.UserName
					,'-'
					,UINP.UserType
					) AS DistributerUserName
				,isnull(RE.RechargeProviderCommission, 0) AS ProviderCommissionReceivedInRs
				,isnull(TD.DiscountAmount, 0) AS CommissionGivenToRet
				,isnull(TDDist.DiscountAmount, 0) AS CommissionGivenToDist
				,isnull(TDSDist.DiscountAmount, 0) AS CommissionGivenToSuperDist
				,isnull(ISNULL(RE.RechargeProviderCommission, 0) - (ISNULL(TD.DiscountAmount, 0) + ISNULL(TDDist.DiscountAmount, 0) + ISNULL(TDSDist.DiscountAmount, 0)), 0) AS AdminCommission
			FROM dbo.RECHARGE(NOLOCK) AS RE
			INNER JOIN dbo.OperatorMaster(NOLOCK) AS OM ON RE.OperatorID = OM.OperatorID
			INNER JOIN dbo.RechargeProviderMaster(NOLOCK) AS RPM ON RPM.RechargeProviderID = RE.RechargeProviderID
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN [dbo].[UserTypeMaster] AS UTM ON UTM.[UserTypeID] = UI.[UserTypeID]
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN [dbo].[UserTypeMaster](NOLOCK) AS UINP ON UINP.[UserTypeID] = UIN.[UserTypeID]
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
				AND TD.ServiceID = RE.ServiceID
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDDist ON TDDist.RechargeID = RE.RechargeID
				AND TDDist.ServiceID = RE.ServiceID
				AND TDDist.AmountCreditDebit = ''
				AND TDDist.UserID = UIN.UserID
				AND upper(TDDist.DOCType) IN (
					'RECHARGE'
					,'ElectricityBill'
					)
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDSDist ON TDSDist.RechargeID = RE.RechargeID
				AND TDSDist.ServiceID = RE.ServiceID
				AND TDSDist.UserID NOT IN (
					UIN.UserID
					,@SystemAccount
					)
				AND TDSDist.AmountCreditDebit = ''
				AND TDSDist.CommissionCreditDebit IN (
					'CREDIT'
					,''
					)
				AND upper(TDSDist.DOCType) IN (
					'RECHARGE'
					,'ElectricityBill'
					)
			WHERE upper(TD.DOCType) IN (
					'RECHARGE'
					,'ElectricityBill'
					)
				AND TD.CommissionCreditDebit IN (
					'CREDIT'
					,'DEBIT'
					,''
					)
				AND TD.AmountCreditDebit = 'DEBIT'
				AND upper(RE.STATUS) IN ('SUCCESS')
				AND TD.UserID != @SystemAccount
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDateDB, 103)
					AND Convert(DATE, @ToDateDB, 103)
						and
					( @BalanceType ='Recharge' or @BalanceType =  NULL)
			ORDER BY RE.DOC DESC
			
			UNION
			
			SELECT DISTINCT TOP 10000 RE.DOC AS MainDOC
				,convert(VARCHAR(12), RE.DOC, 113) + right(convert(VARCHAR(39), RE.DOC, 22), 11) AS DOC
				,RE.BeneficiaryAccountNo
				,OM.OperatorName
				,RPM.RechargeProviderName
				,RE.Amount
				,CONCAT (
					UI.UserName
					,'-'
					,UTM.UserType
					) AS RetailerUserName
				,CONCAT (
					UIN.UserName
					,'-'
					,UINP.UserType
					) AS DistributerUserName
				,isnull(RE.[DMRCommissionn], 0) AS ProviderCommissionReceivedInRs
				,isnull(TD.DiscountAmount, 0) AS CommissionGivenToRet
				,isnull(TDDist.DiscountAmount, 0) AS CommissionGivenToDist
				,isnull(TDSDist.DiscountAmount, 0) AS CommissionGivenToSuperDist
				,isnull(ISNULL(RE.[DMRCommissionn], 0) - (ISNULL(TD.DiscountAmount, 0) + ISNULL(TDDist.DiscountAmount, 0) + ISNULL(TDSDist.DiscountAmount, 0)), 0) AS AdminCommission
			FROM dbo.MoneyTransfer(NOLOCK) AS RE
			INNER JOIN dbo.OperatorMaster(NOLOCK) AS OM ON RE.[DMROperatorID] = OM.OperatorID
			INNER JOIN dbo.RechargeProviderMaster(NOLOCK) AS RPM ON RPM.RechargeProviderID = RE.DMRProviderID
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN [dbo].[UserTypeMaster] AS UTM ON UTM.[UserTypeID] = UI.[UserTypeID]
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN [dbo].[UserTypeMaster](NOLOCK) AS UINP ON UINP.[UserTypeID] = UIN.[UserTypeID]
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.[MoneyTransferID]
				AND TD.ServiceID = RE.DMRServiceID
				AND TD.AmountCreditDebit IN ('DEBIT')
				AND TD.CommissionCreditDebit IN (
					'CREDIT'
					,'DEBIT'
					)
				AND TD.UserID != @SystemAccount
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDDist ON TDDist.RechargeID = RE.[MoneyTransferID]
				AND TDDist.ServiceID = RE.DMRServiceID
				AND TDDist.AmountCreditDebit = ''
				AND TDDist.CommissionCreditDebit IN ('CREDIT')
				AND TDDist.UserID = UIN.UserID
				AND upper(TDDist.DOCType) = upper('MoneyTransfer')
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDSDist ON TDSDist.RechargeID = RE.[MoneyTransferID]
				AND TDSDist.ServiceID = RE.DMRServiceID
				AND TDSDist.UserID NOT IN (
					UIN.UserID
					,@SystemAccount
					)
				AND TDSDist.AmountCreditDebit = ''
				AND TDSDist.CommissionCreditDebit IN (
					'CREDIT'
					,''
					)
				AND upper(TDSDist.DOCType) = upper('MoneyTransfer')
			WHERE upper(TD.DOCType) = upper('MoneyTransfer')
				AND TD.CommissionCreditDebit IN (
					'DEBIT'
					,'CREDIT'
					)
				AND TD.AmountCreditDebit IN (
					'DEBIT'
					,''
					)
				AND upper(RE.STATUS) IN ('SUCCESS')
				AND TD.UserID != @SystemAccount
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDateDB, 103)
					AND Convert(DATE, @ToDateDB, 103)
						and
					( @BalanceType ='DMT' or @BalanceType =  NULL)
			ORDER BY RE.DOC DESC
			) result
		ORDER BY MainDOC DESC
	END
	ELSE
	BEGIN
		DECLARE @TempTbl TABLE (UserID VARCHAR(200));

		IF (Upper(@userType) = Upper(@SuperDistributor))
		BEGIN --------- For SD show transactions on all its down tree.                
			WITH cte
			AS (
				SELECT *
				FROM UserInformation
				WHERE ParentID = @UserIDDB
				
				UNION ALL
				
				SELECT UserInformation.*
				FROM UserInformation
				INNER JOIN cte ON cte.UserID = UserInformation.ParentID
				)
			INSERT INTO @TempTbl (UserID)
			SELECT UserID
			FROM cte
		END
		ELSE --- For all Users show transactions from its downlines                
		BEGIN
			INSERT INTO @TempTbl (UserID)
			SELECT UserID
			FROM UserInformation
			WHERE ParentID = @UserIDDB
		END

		----- insert self ID in to temp table for Client------                
		INSERT INTO @TempTbl
		VALUES (@UserIDDB);
			
		SELECT TOP 10000 *
		FROM (
	
			SELECT DISTINCT TOP 10000 RE.DOC AS MainDOC
				,convert(VARCHAR(12), RE.DOC, 113) + right(convert(VARCHAR(39), RE.DOC, 22), 11) AS DOC
				,RE.ConsumerNumber
				,OM.OperatorName
				,RPM.RechargeProviderName
				,RE.Amount
				,CONCAT (
					UI.UserName
					,'-'
					,UTM.UserType
					) AS RetailerUserName
				,CONCAT (
					UIN.UserName
					,'-'
					,UINP.UserType
					) AS DistributerUserName
				,isnull(RE.RechargeProviderCommission, 0) AS ProviderCommissionReceivedInRs
				,isnull(TD.DiscountAmount, 0) AS CommissionGivenToRet
				,isnull(TDDist.DiscountAmount, 0) AS CommissionGivenToDist
				,isnull(TDSDist.DiscountAmount, 0) AS CommissionGivenToSuperDist
				,isnull(ISNULL(RE.RechargeProviderCommission, 0) - (isnull(TD.DiscountAmount, 0) + isnull(TDDist.DiscountAmount, 0) + isnull(TDSDist.DiscountAmount, 0)), 0) AS AdminCommission
			FROM dbo.RECHARGE(NOLOCK) AS RE
			INNER JOIN dbo.OperatorMaster(NOLOCK) AS OM ON RE.OperatorID = OM.OperatorID
			INNER JOIN dbo.RechargeProviderMaster(NOLOCK) AS RPM ON RPM.RechargeProviderID = RE.RechargeProviderID
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN [dbo].[UserTypeMaster] AS UTM ON UTM.[UserTypeID] = UI.[UserTypeID]
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN [dbo].[UserTypeMaster](NOLOCK) AS UINP ON UINP.[UserTypeID] = UIN.[UserTypeID]
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
				AND TD.ServiceID = RE.ServiceID
				AND upper(TD.AmountCreditDebit) = 'DEBIT'
				AND TD.UserID != @SystemAccount
			INNER JOIN @TempTbl AS TP ON TP.UserID = TD.UserID
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDDist ON TDDist.RechargeID = RE.RechargeID
				AND TDDist.ServiceID = RE.ServiceID
				AND TDDist.AmountCreditDebit = ''
				AND TDDist.UserID = UIN.UserID
				AND upper(TDDist.DOCType) IN (
					'RECHARGE'
					,'ElectricityBill'
					)
				AND TDDist.UserID NOT IN (
					UI.UserID
					,@SystemAccount
					)
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDSDist ON TDSDist.RechargeID = RE.RechargeID
				AND TDSDist.ServiceID = RE.ServiceID
				AND TDSDist.UserID NOT IN (
					UI.UserID
					,UIN.UserID
					,@SystemAccount
					)
				AND TDSDist.AmountCreditDebit = ''
				AND TDSDist.CommissionCreditDebit IN (
					'CREDIT'
					,''
					)
				AND TDSDist.UserID != @SystemAccount
				AND upper(TDSDist.DOCType) IN (
					'RECHARGE'
					,'ElectricityBill'
					)
			WHERE upper(TD.DOCType) IN (
					'RECHARGE'
					,'ElectricityBill'
					)
				AND upper(TD.CommissionCreditDebit) IN (
					'CREDIT'
					,'DEBIT'
					,''
					)
				AND upper(RE.STATUS) IN ('SUCCESS')
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDateDB, 103)
					AND Convert(DATE, @ToDateDB, 103)

					and
					( @BalanceType ='Recharge' or @BalanceType =  NULL)
		
			ORDER BY RE.DOC DESC
		
			UNION
			
			SELECT DISTINCT TOP 10000 RE.DOC AS MainDOC
				,convert(VARCHAR(12), RE.DOC, 113) + right(convert(VARCHAR(39), RE.DOC, 22), 11) AS DOC
				,RE.BeneficiaryAccountNo
				,OM.OperatorName
				,RPM.RechargeProviderName
				,RE.Amount
				,CONCAT (
					UI.UserName
					,'-'
					,UTM.UserType
					) AS RetailerUserName
				,CONCAT (
					UIN.UserName
					,'-'
					,UINP.UserType
					) AS DistributerUserName
				,isnull(RE.[DMRCommissionn], 0) AS ProviderCommissionReceivedInRs
				,isnull(TD.DiscountAmount, 0) AS CommissionGivenToRet
				,isnull(TDDist.DiscountAmount, 0) AS CommissionGivenToDist
				,isnull(TDSDist.DiscountAmount, 0) AS CommissionGivenToSuperDist
				,isnull(ISNULL(RE.[DMRCommissionn], 0) - (isnull(TD.DiscountAmount, 0) + isnull(TDDist.DiscountAmount, 0) + isnull(TDSDist.DiscountAmount, 0)), 0) AS AdminCommission
			FROM dbo.MoneyTransfer(NOLOCK) AS RE
			INNER JOIN dbo.OperatorMaster(NOLOCK) AS OM ON RE.[DMROperatorID] = OM.OperatorID
			INNER JOIN dbo.RechargeProviderMaster(NOLOCK) AS RPM ON RPM.RechargeProviderID = RE.[DMRProviderID]
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN [dbo].[UserTypeMaster] AS UTM ON UTM.[UserTypeID] = UI.[UserTypeID]
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN [dbo].[UserTypeMaster](NOLOCK) AS UINP ON UINP.[UserTypeID] = UIN.[UserTypeID]
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.[MoneyTransferID]
				AND TD.ServiceID = RE.DMRServiceID
				AND TD.AmountCreditDebit IN ('DEBIT')
				AND TD.CommissionCreditDebit IN (
					'CREDIT'
					,'DEBIT'
					)
				AND TD.UserID != @SystemAccount
				AND TD.AmountCreditDebit IN ('DEBIT')
			INNER JOIN @TempTbl AS TP ON TP.UserID = TD.UserID
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDDist ON TDDist.RechargeID = RE.[MoneyTransferID]
				AND TDDist.ServiceID = RE.DMRServiceID
				AND TDDist.AmountCreditDebit = ''
				AND TDDist.CommissionCreditDebit IN ('CREDIT')
				AND TDDist.UserID = UIN.UserID
				AND upper(TDDist.DOCType) = upper('MoneyTransfer')
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDSDist ON TDSDist.RechargeID = RE.[MoneyTransferID]
				AND TDSDist.ServiceID = RE.DMRServiceID
				AND TDSDist.UserID NOT IN (
					UI.UserID
					,UIN.UserID
					,@SystemAccount
					)
				AND TDSDist.AmountCreditDebit = ''
				AND TDSDist.CommissionCreditDebit IN (
					'CREDIT'
					,''
					)
				AND TDSDist.UserID != @SystemAccount
				AND upper(TDSDist.DOCType) = upper('MoneyTransfer')
			WHERE upper(TD.DOCType) = upper('MoneyTransfer')
				AND TD.CommissionCreditDebit IN (
					'DEBIT'
					,'CREDIT'
					,''
					)
				AND upper(RE.STATUS) IN (upper('SUCCESS'))
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDateDB, 103)
					AND Convert(DATE, @ToDateDB, 103)
						and
					( @BalanceType ='DMT' or @BalanceType =  NULL)
			ORDER BY RE.DOC DESC
			) result
		ORDER BY MainDOC DESC

	END
	end



	 if(@BalanceType ='All' or @BalanceType=null )

	begin 

	SELECT @userType = UTM.UserType
	FROM [dbo].UserInformation(NOLOCK) UI
	INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @UserIDDB

	IF (Upper(@userType) = Upper(@admin))
	BEGIN
		SELECT TOP 10000 *
		FROM (
			SELECT DISTINCT TOP 10000 RE.DOC AS MainDOC
				,convert(VARCHAR(12), RE.DOC, 113) + right(convert(VARCHAR(39), RE.DOC, 22), 11) AS DOC
				,RE.ConsumerNumber
				,OM.OperatorName
				,RPM.RechargeProviderName
				,RE.Amount
				,CONCAT (
					UI.UserName
					,'-'
					,UTM.UserType
					) AS RetailerUserName
				,CONCAT (
					UIN.UserName
					,'-'
					,UINP.UserType
					) AS DistributerUserName
				,isnull(RE.RechargeProviderCommission, 0) AS ProviderCommissionReceivedInRs
				,isnull(TD.DiscountAmount, 0) AS CommissionGivenToRet
				,isnull(TDDist.DiscountAmount, 0) AS CommissionGivenToDist
				,isnull(TDSDist.DiscountAmount, 0) AS CommissionGivenToSuperDist
				,isnull(ISNULL(RE.RechargeProviderCommission, 0) - (ISNULL(TD.DiscountAmount, 0) + ISNULL(TDDist.DiscountAmount, 0) + ISNULL(TDSDist.DiscountAmount, 0)), 0) AS AdminCommission
			FROM dbo.RECHARGE(NOLOCK) AS RE
			INNER JOIN dbo.OperatorMaster(NOLOCK) AS OM ON RE.OperatorID = OM.OperatorID
			INNER JOIN dbo.RechargeProviderMaster(NOLOCK) AS RPM ON RPM.RechargeProviderID = RE.RechargeProviderID
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN [dbo].[UserTypeMaster] AS UTM ON UTM.[UserTypeID] = UI.[UserTypeID]
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN [dbo].[UserTypeMaster](NOLOCK) AS UINP ON UINP.[UserTypeID] = UIN.[UserTypeID]
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
				AND TD.ServiceID = RE.ServiceID
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDDist ON TDDist.RechargeID = RE.RechargeID
				AND TDDist.ServiceID = RE.ServiceID
				AND TDDist.AmountCreditDebit = ''
				AND TDDist.UserID = UIN.UserID
				AND upper(TDDist.DOCType) IN (
					'RECHARGE'
					,'ElectricityBill'
					)
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDSDist ON TDSDist.RechargeID = RE.RechargeID
				AND TDSDist.ServiceID = RE.ServiceID
				AND TDSDist.UserID NOT IN (
					UIN.UserID
					,@SystemAccount
					)
				AND TDSDist.AmountCreditDebit = ''
				AND TDSDist.CommissionCreditDebit IN (
					'CREDIT'
					,''
					)
				AND upper(TDSDist.DOCType) IN (
					'RECHARGE'
					,'ElectricityBill'
					)
			WHERE upper(TD.DOCType) IN (
					'RECHARGE'
					,'ElectricityBill'
					)
				AND TD.CommissionCreditDebit IN (
					'CREDIT'
					,'DEBIT'
					,''
					)
				AND TD.AmountCreditDebit = 'DEBIT'
				AND upper(RE.STATUS) IN ('SUCCESS')
				AND TD.UserID != @SystemAccount
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDateDB, 103)
					AND Convert(DATE, @ToDateDB, 103)
					
			ORDER BY RE.DOC DESC
			
			UNION
			
			SELECT DISTINCT TOP 10000 RE.DOC AS MainDOC
				,convert(VARCHAR(12), RE.DOC, 113) + right(convert(VARCHAR(39), RE.DOC, 22), 11) AS DOC
				,RE.BeneficiaryAccountNo
				,OM.OperatorName
				,RPM.RechargeProviderName
				,RE.Amount
				,CONCAT (
					UI.UserName
					,'-'
					,UTM.UserType
					) AS RetailerUserName
				,CONCAT (
					UIN.UserName
					,'-'
					,UINP.UserType
					) AS DistributerUserName
				,isnull(RE.[DMRCommissionn], 0) AS ProviderCommissionReceivedInRs
				,isnull(TD.DiscountAmount, 0) AS CommissionGivenToRet
				,isnull(TDDist.DiscountAmount, 0) AS CommissionGivenToDist
				,isnull(TDSDist.DiscountAmount, 0) AS CommissionGivenToSuperDist
				,isnull(ISNULL(RE.[DMRCommissionn], 0) - (ISNULL(TD.DiscountAmount, 0) + ISNULL(TDDist.DiscountAmount, 0) + ISNULL(TDSDist.DiscountAmount, 0)), 0) AS AdminCommission
			FROM dbo.MoneyTransfer(NOLOCK) AS RE
			INNER JOIN dbo.OperatorMaster(NOLOCK) AS OM ON RE.[DMROperatorID] = OM.OperatorID
			INNER JOIN dbo.RechargeProviderMaster(NOLOCK) AS RPM ON RPM.RechargeProviderID = RE.DMRProviderID
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN [dbo].[UserTypeMaster] AS UTM ON UTM.[UserTypeID] = UI.[UserTypeID]
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN [dbo].[UserTypeMaster](NOLOCK) AS UINP ON UINP.[UserTypeID] = UIN.[UserTypeID]
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.[MoneyTransferID]
				AND TD.ServiceID = RE.DMRServiceID
				AND TD.AmountCreditDebit IN ('DEBIT')
				AND TD.CommissionCreditDebit IN (
					'CREDIT'
					,'DEBIT'
					)
				AND TD.UserID != @SystemAccount
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDDist ON TDDist.RechargeID = RE.[MoneyTransferID]
				AND TDDist.ServiceID = RE.DMRServiceID
				AND TDDist.AmountCreditDebit = ''
				AND TDDist.CommissionCreditDebit IN ('CREDIT')
				AND TDDist.UserID = UIN.UserID
				AND upper(TDDist.DOCType) = upper('MoneyTransfer')
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDSDist ON TDSDist.RechargeID = RE.[MoneyTransferID]
				AND TDSDist.ServiceID = RE.DMRServiceID
				AND TDSDist.UserID NOT IN (
					UIN.UserID
					,@SystemAccount
					)
				AND TDSDist.AmountCreditDebit = ''
				AND TDSDist.CommissionCreditDebit IN (
					'CREDIT'
					,''
					)
				AND upper(TDSDist.DOCType) = upper('MoneyTransfer')
			WHERE upper(TD.DOCType) = upper('MoneyTransfer')
				AND TD.CommissionCreditDebit IN (
					'DEBIT'
					,'CREDIT'
					)
				AND TD.AmountCreditDebit IN (
					'DEBIT'
					,''
					)
				AND upper(RE.STATUS) IN ('SUCCESS')
				AND TD.UserID != @SystemAccount
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDateDB, 103)
					AND Convert(DATE, @ToDateDB, 103)
					
			ORDER BY RE.DOC DESC
			) result
		ORDER BY MainDOC DESC
	END
	ELSE
	BEGIN
		--DECLARE @TempTbl TABLE (UserID VARCHAR(200));

		IF (Upper(@userType) = Upper(@SuperDistributor))
		BEGIN --------- For SD show transactions on all its down tree.                
			WITH cte
			AS (
				SELECT *
				FROM UserInformation
				WHERE ParentID = @UserIDDB
				
				UNION ALL
				
				SELECT UserInformation.*
				FROM UserInformation
				INNER JOIN cte ON cte.UserID = UserInformation.ParentID
				)
			INSERT INTO @TempTbl (UserID)
			SELECT UserID
			FROM cte
		END
		ELSE --- For all Users show transactions from its downlines                
		BEGIN
			INSERT INTO @TempTbl (UserID)
			SELECT UserID
			FROM UserInformation
			WHERE ParentID = @UserIDDB
		END

		----- insert self ID in to temp table for Client------                
		INSERT INTO @TempTbl
		VALUES (@UserIDDB);
			
		SELECT TOP 10000 *
		FROM (
	
			SELECT DISTINCT TOP 10000 RE.DOC AS MainDOC
				,convert(VARCHAR(12), RE.DOC, 113) + right(convert(VARCHAR(39), RE.DOC, 22), 11) AS DOC
				,RE.ConsumerNumber
				,OM.OperatorName
				,RPM.RechargeProviderName
				,RE.Amount
				,CONCAT (
					UI.UserName
					,'-'
					,UTM.UserType
					) AS RetailerUserName
				,CONCAT (
					UIN.UserName
					,'-'
					,UINP.UserType
					) AS DistributerUserName
				,isnull(RE.RechargeProviderCommission, 0) AS ProviderCommissionReceivedInRs
				,isnull(TD.DiscountAmount, 0) AS CommissionGivenToRet
				,isnull(TDDist.DiscountAmount, 0) AS CommissionGivenToDist
				,isnull(TDSDist.DiscountAmount, 0) AS CommissionGivenToSuperDist
				,isnull(ISNULL(RE.RechargeProviderCommission, 0) - (isnull(TD.DiscountAmount, 0) + isnull(TDDist.DiscountAmount, 0) + isnull(TDSDist.DiscountAmount, 0)), 0) AS AdminCommission
			FROM dbo.RECHARGE(NOLOCK) AS RE
			INNER JOIN dbo.OperatorMaster(NOLOCK) AS OM ON RE.OperatorID = OM.OperatorID
			INNER JOIN dbo.RechargeProviderMaster(NOLOCK) AS RPM ON RPM.RechargeProviderID = RE.RechargeProviderID
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN [dbo].[UserTypeMaster] AS UTM ON UTM.[UserTypeID] = UI.[UserTypeID]
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN [dbo].[UserTypeMaster](NOLOCK) AS UINP ON UINP.[UserTypeID] = UIN.[UserTypeID]
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
				AND TD.ServiceID = RE.ServiceID
				AND upper(TD.AmountCreditDebit) = 'DEBIT'
				AND TD.UserID != @SystemAccount
			INNER JOIN @TempTbl AS TP ON TP.UserID = TD.UserID
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDDist ON TDDist.RechargeID = RE.RechargeID
				AND TDDist.ServiceID = RE.ServiceID
				AND TDDist.AmountCreditDebit = ''
				AND TDDist.UserID = UIN.UserID
				AND upper(TDDist.DOCType) IN (
					'RECHARGE'
					,'ElectricityBill'
					)
				AND TDDist.UserID NOT IN (
					UI.UserID
					,@SystemAccount
					)
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDSDist ON TDSDist.RechargeID = RE.RechargeID
				AND TDSDist.ServiceID = RE.ServiceID
				AND TDSDist.UserID NOT IN (
					UI.UserID
					,UIN.UserID
					,@SystemAccount
					)
				AND TDSDist.AmountCreditDebit = ''
				AND TDSDist.CommissionCreditDebit IN (
					'CREDIT'
					,''
					)
				AND TDSDist.UserID != @SystemAccount
				AND upper(TDSDist.DOCType) IN (
					'RECHARGE'
					,'ElectricityBill'
					)
			WHERE upper(TD.DOCType) IN (
					'RECHARGE'
					,'ElectricityBill'
					)
				AND upper(TD.CommissionCreditDebit) IN (
					'CREDIT'
					,'DEBIT'
					,''
					)
				AND upper(RE.STATUS) IN ('SUCCESS')
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDateDB, 103)
					AND Convert(DATE, @ToDateDB, 103)

					
		
			ORDER BY RE.DOC DESC
		
			UNION
			
			SELECT DISTINCT TOP 10000 RE.DOC AS MainDOC
				,convert(VARCHAR(12), RE.DOC, 113) + right(convert(VARCHAR(39), RE.DOC, 22), 11) AS DOC
				,RE.BeneficiaryAccountNo
				,OM.OperatorName
				,RPM.RechargeProviderName
				,RE.Amount
				,CONCAT (
					UI.UserName
					,'-'
					,UTM.UserType
					) AS RetailerUserName
				,CONCAT (
					UIN.UserName
					,'-'
					,UINP.UserType
					) AS DistributerUserName
				,isnull(RE.[DMRCommissionn], 0) AS ProviderCommissionReceivedInRs
				,isnull(TD.DiscountAmount, 0) AS CommissionGivenToRet
				,isnull(TDDist.DiscountAmount, 0) AS CommissionGivenToDist
				,isnull(TDSDist.DiscountAmount, 0) AS CommissionGivenToSuperDist
				,isnull(ISNULL(RE.[DMRCommissionn], 0) - (isnull(TD.DiscountAmount, 0) + isnull(TDDist.DiscountAmount, 0) + isnull(TDSDist.DiscountAmount, 0)), 0) AS AdminCommission
			FROM dbo.MoneyTransfer(NOLOCK) AS RE
			INNER JOIN dbo.OperatorMaster(NOLOCK) AS OM ON RE.[DMROperatorID] = OM.OperatorID
			INNER JOIN dbo.RechargeProviderMaster(NOLOCK) AS RPM ON RPM.RechargeProviderID = RE.[DMRProviderID]
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN [dbo].[UserTypeMaster] AS UTM ON UTM.[UserTypeID] = UI.[UserTypeID]
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN [dbo].[UserTypeMaster](NOLOCK) AS UINP ON UINP.[UserTypeID] = UIN.[UserTypeID]
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.[MoneyTransferID]
				AND TD.ServiceID = RE.DMRServiceID
				AND TD.AmountCreditDebit IN ('DEBIT')
				AND TD.CommissionCreditDebit IN (
					'CREDIT'
					,'DEBIT'
					)
				AND TD.UserID != @SystemAccount
				AND TD.AmountCreditDebit IN ('DEBIT')
			INNER JOIN @TempTbl AS TP ON TP.UserID = TD.UserID
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDDist ON TDDist.RechargeID = RE.[MoneyTransferID]
				AND TDDist.ServiceID = RE.DMRServiceID
				AND TDDist.AmountCreditDebit = ''
				AND TDDist.CommissionCreditDebit IN ('CREDIT')
				AND TDDist.UserID = UIN.UserID
				AND upper(TDDist.DOCType) = upper('MoneyTransfer')
			LEFT JOIN dbo.TransactionDetails(NOLOCK) AS TDSDist ON TDSDist.RechargeID = RE.[MoneyTransferID]
				AND TDSDist.ServiceID = RE.DMRServiceID
				AND TDSDist.UserID NOT IN (
					UI.UserID
					,UIN.UserID
					,@SystemAccount
					)
				AND TDSDist.AmountCreditDebit = ''
				AND TDSDist.CommissionCreditDebit IN (
					'CREDIT'
					,''
					)
				AND TDSDist.UserID != @SystemAccount
				AND upper(TDSDist.DOCType) = upper('MoneyTransfer')
			WHERE upper(TD.DOCType) = upper('MoneyTransfer')
				AND TD.CommissionCreditDebit IN (
					'DEBIT'
					,'CREDIT'
					,''
					)
				AND upper(RE.STATUS) IN (upper('SUCCESS'))
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDateDB, 103)
					AND Convert(DATE, @ToDateDB, 103)
					
			ORDER BY RE.DOC DESC
			) result
		ORDER BY MainDOC DESC

	END
	end
END

GO
/****** Object:  StoredProcedure [dbo].[SelectDuplicateRecharge]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Sainath Bhujbal>      
-- ALTER  date: <27 Feb 2017 ,>      
-- Description: <Description,,>      
-- =============================================   
CREATE PROCEDURE[dbo].[SelectDuplicateRecharge] @CONSUMERNO VARCHAR(20)
	,@AMOUNT DECIMAL(18, 5)
	,@OPERATORID INT
AS
BEGIN
	DECLARE @CurTime DATETIME;
	DECLARE @RepeatTime INT

	SET @RepeatTime = 0
	SET @CurTime = CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30'), (0))

	SELECT @RepeatTime = isnull(RepeatRecharge, 0)
	FROM dbo.OperatorMaster(NOLOCK)
	WHERE OperatorID = @OPERATORID

	IF @RepeatTime >= 0
	BEGIN
		SELECT RC.UserID
			,isnull(OM.RepeatRecharge, 0) AS RepeatRecharge
		FROM dbo.RECHARGE(NOLOCK) AS RC
		INNER JOIN dbo.OperatorMaster(NOLOCK) AS OM ON RC.OperatorID = OM.OperatorID
		WHERE RC.ConsumerNumber = @CONSUMERNO
			AND RC.Amount = @AMOUNT
			AND RC.STATUS IN (
				'PROCESS'
				,'SUCCESS'
				)
			AND datediff(minute, RC.DOC, @CurTime) < @RepeatTime
	END
			-- ELSE
			-- BEGIN
			-- SELECT RC.UserID
			-- ,isnull(OM.RepeatRecharge, 0) AS RepeatRecharge
			-- FROM dbo.RECHARGE(NOLOCK) AS RC
			-- INNER JOIN dbo.OperatorMaster(NOLOCK) AS OM ON RC.OperatorID = OM.OperatorID
			-- WHERE RC.ConsumerNumber = @CONSUMERNO
			-- AND RC.Amount = @AMOUNT
			-- AND RC.STATUS IN (
			-- 'PROCESS'
			-- ,'SUCCESS'
			-- )
			-- AND datediff(minute, RC.DOC, @CurTime) < @RepeatTime
			-- END
END



GO
/****** Object:  StoredProcedure [dbo].[selectMarketBalance]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[selectMarketBalance]
AS
BEGIN
	SELECT count(UI.[UserID]) AS UserCount
		,Sum(UB.CurrentBalance) AS MarketBalance
		,Sum(UB.DMRBalance) AS DMRBalance
	FROM [UserInformation] AS UI
	INNER JOIN [UserBalance] AS UB ON UI.UserID = UB.UserID
	WHERE UI.UserTypeID NOT IN (
			1
			,5
			)
END



GO
/****** Object:  StoredProcedure [dbo].[SelectNotificationCount]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SelectNotificationCount]
AS
BEGIN
	DECLARE @BalRequestCount INT
		,@BalRevereseCount INT
		,@EnquiryCount INT
		,@complaintPendingCount INT

	SET @BalRequestCount = (
			SELECT COUNT(RequestID)
			FROM dbo.BalanceRequest
			WHERE upper(Type) = upper('Transfer')
				AND upper(STATUS) = upper('Pending')
				AND IsDeleted = 0
			);
	SET @BalRevereseCount = (
			SELECT COUNT(RequestID)
			FROM dbo.BalanceRequest
			WHERE upper(STATUS) = upper('Pending')
				AND IsDeleted = 0
				AND upper(Type) = upper('Reverse')
			);
	SET @EnquiryCount = (
			SELECT COUNT(ContactUsID)
			FROM Enquiry
			WHERE IsDeleted = 0
				AND Convert(VARCHAR(20), DOC, 103) = Convert(VARCHAR(20), switchoffset(sysdatetimeoffset(), '+05:30'), 103)
			);
	SET @complaintPendingCount = (
			SELECT COUNT(RefundID)
			FROM RefundMaster
			WHERE upper(STATUS) = upper('Pending')
			);

	SELECT @BalRequestCount AS BalRequestCount
		,@BalRevereseCount AS BalRevereseCount
		,@EnquiryCount AS EnquiryCount
		,@complaintPendingCount AS complaintPendingCount
		,@BalRequestCount + @BalRevereseCount + @EnquiryCount + @complaintPendingCount AS Total
		,@BalRequestCount + @BalRevereseCount AS BalanceTotal
END



GO
/****** Object:  StoredProcedure [dbo].[SelectOpcodebyOperatorAPIID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[SelectOpcodebyOperatorAPIID] --[SelectOpcodebyOperatorAPIID] 3 , 48  
	@RechargeProviderID INT
	,@OperatorID INT
AS
BEGIN
	SELECT TOP 1 Opcode
	FROM [dbo].[RechargeProviderOperatorMaster](NOLOCK)
	WHERE [RechargeProviderID] = @RechargeProviderID
		AND [OperatorID] = @OperatorID
END



GO
/****** Object:  StoredProcedure [dbo].[SelectPaymentGatwayReply]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================              
--Author :Sainath Bhujbal              
--Date :03-04-17              
--Purpose :Select PaymentGatway Reply    
--==========================================         
--SelectPaymentGatwayReply '01/04/2017','13/06/2017'  
CREATE PROCEDURE[dbo].[SelectPaymentGatwayReply] @Fromdate VARCHAR(20)
	,@Todate VARCHAR(20)
AS
BEGIN
	SELECT UI.name
		,PR.[ConsumerNumber]
		,PR.[Amount]
		,upper(PR.[Status]) AS STATUS
		,PR.[TransactionID]
		,PR.[DOC]
		,OM.OperatorName
		,STP.ServiceName AS Service
	FROM DBO.[PaymentGatewayReply] PR(NOLOCK)
	LEFT JOIN [dbo].UserInformation UI(NOLOCK) ON PR.[UserID] = UI.UserID
	LEFT JOIN [dbo].OperatorMaster OM(NOLOCK) ON PR.[OperatorID] = OM.[OperatorID]
	LEFT JOIN [dbo].ServiceMaster STP(NOLOCK) ON PR.[ServiceID] = STP.ServiceID
	WHERE PR.[DOC] >= convert(DATETIME, @Fromdate, 103)
		AND PR.[DOC] <= convert(DATETIME, @Todate, 103)
	ORDER BY DOC DESC
END



GO
/****** Object:  StoredProcedure [dbo].[SelectSMSDetail]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SelectSMSDetail]
AS
BEGIN
	SELECT TOP 1 [SMSString]
	FROM [dbo].[SMSMaster]
	WHERE IsActive = 1
	ORDER BY doc DESC
END



GO
/****** Object:  StoredProcedure [dbo].[selectSMSRechargeRequestLogbydate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selectSMSRechargeRequestLogbydate] @UserID INT
	,@FromDate VARCHAR(100)
	,@ToDate VARCHAR(100)
AS
BEGIN
	DECLARE @UserTypeID INT;
	DECLARE @retailer VARCHAR(20) = 'RETAILER';
	DECLARE @Customer VARCHAR(20) = 'Customer';
	DECLARE @distributor VARCHAR(20) = 'DISTRIBUTOR';
	DECLARE @admin VARCHAR(20) = 'ADMIN';
	DECLARE @apiUser VARCHAR(20) = 'APIUSER';
	DECLARE @userType VARCHAR(20);

	SELECT @userType = UTM.UserType
	FROM [dbo].UserInformation(NOLOCK) UI
	INNER JOIN dbo.UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @UserID

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM dbo.UserInformation
			WHERE UserID = @UserID
			)

	IF (Upper(@userType) = Upper(@admin))
	BEGIN
		SELECT S.*
			,convert(VARCHAR(12), S.DOC, 113) + right(convert(VARCHAR(39), S.DOC, 22), 11) AS RechargeDate
			,UI.ShopeName
			,UI.UserName
		FROM SMSRechargeRequestLog S
		INNER JOIN dbo.UserInformation(NOLOCK) UI ON S.UserID = UI.UserID
		WHERE (
				convert(DATE, S.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, S.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
	END

	IF (Upper(@userType) = Upper(@distributor))
	BEGIN
		SELECT S.*
			,convert(VARCHAR(12), S.DOC, 113) + right(convert(VARCHAR(39), S.DOC, 22), 11) AS RechargeDate
			,UI.ShopeName
			,UI.UserName
		FROM SMSRechargeRequestLog S
		INNER JOIN dbo.UserInformation(NOLOCK) UI ON S.UserID = UI.UserID
		WHERE (
				convert(DATE, S.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, S.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
			AND @UserID IN (UI.ParentID)
	END

	IF (
			Upper(@userType) = Upper(@retailer)
			OR Upper(@userType) = Upper(@Customer)
			OR Upper(@userType) = Upper(@apiUser)
			)
	BEGIN
		SELECT S.*
			,convert(VARCHAR(12), S.DOC, 113) + right(convert(VARCHAR(39), S.DOC, 22), 11) AS RechargeDate
			,UI.ShopeName
			,UI.UserName
		FROM SMSRechargeRequestLog S
		INNER JOIN dbo.UserInformation(NOLOCK) UI ON S.UserID = UI.UserID
		WHERE (
				convert(DATE, S.DOC, 103) >= convert(DATE, @FromDate, 103)
				AND convert(DATE, S.DOC, 103) <= convert(DATE, @ToDate, 103)
				)
			AND S.UserID = @UserID
	END
END


GO
/****** Object:  StoredProcedure [dbo].[SelectUplineCommission]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                                          
-- Author:  <Sainath Bhujbal>                                          
-- ALTER  date: <23 March 2018,>                                          
-- Description: <Procedure for Select Upline ID commision of retailers >                                          
-- =============================================                                     
CREATE PROCEDURE [dbo].[SelectUplineCommission] @UserID INT
	,@RechargeID INT
	,@ServiceID INT
	,@ConsumerNo VARCHAR(20)
	,@TransactionAmount NUMERIC(18, 5)
	,@OperatorID INT
	,@ParentID INT
	,@RetailerName VARCHAR(50)
	,@docType VARCHAR(30)
AS
DECLARE @dcomAmt NUMERIC(18, 5)
DECLARE @SDcomAmt NUMERIC(18, 5)
DECLARE @distributor VARCHAR(20) = 'DISTRIBUTOR';
DECLARE @Superdistributor VARCHAR(20) = 'SUPERDISTRIBUTOR';
DECLARE @admin VARCHAR(20) = 'ADMIN';
DECLARE @userType VARCHAR(20)
DECLARE @TransDesc VARCHAR(200);
DECLARE @mType VARCHAR(50)
DECLARE @electricityServiceID INT = 14;

IF @ParentID != 0
BEGIN
	SELECT @userType = UTM.UserType
	FROM [dbo].UserInformation(NOLOCK) UI
	INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @ParentID

	IF (
			upper(@userType) = upper(@distributor)
			OR upper(@userType) = upper(@admin)
			OR upper(@userType) = upper(@Superdistributor)
			)
	BEGIN TRY
		--User Comm                        
		--declare @comAmt numeric(18, 5)                      
		--declare @mType varchar(10)         
		DECLARE @UserGroupId INT

		SELECT @UserGroupId = GroupID
		FROM UserGroupMapping
		WHERE UserId = @UserID

		PRINT @UserGroupId

		IF (@UserGroupId IS NOT NULL)
		BEGIN
			SELECT @dcomAmt = DistributorCommissionAmount
				,@SDcomAmt = ISNULL(SuperDistributorCommissionAmount, 0)
				,@mType = Upper(MarginType)
			FROM dbo.RechargeGroupCommission(NOLOCK)
			WHERE OperatorID = @OperatorID
				AND GroupID = @UserGroupId

			PRINT '@dcomAmt' + convert(VARCHAR(30), @dcomAmt) + ':' + @mType
			PRINT '@SDcomAmt' + convert(VARCHAR(30), @SDcomAmt) + ':' + @mType
		END

		IF (@dcomAmt IS NULL) --Default Comm                        
		BEGIN
			SELECT @dcomAmt = ISNULL(DistributorCommissionAmount, 0)
				,@SDcomAmt = ISNULL(SuperDistributorCommissionAmount, 0)
				,@mType = Upper(MarginType)
			FROM DBO.OperatorMaster(NOLOCK)
			WHERE OperatorID = @OperatorID

			PRINT '@dcomAmt' + convert(VARCHAR(30), @dcomAmt) + ':' + @mType
			PRINT '@SDcomAmt' + convert(VARCHAR(30), @SDcomAmt) + ':' + @mType
		END

		IF @mType = 'P'
		BEGIN
			SET @dcomAmt = (@dcomAmt * @TransactionAmount) / 100;
			SET @SDcomAmt = (@SDcomAmt * @TransactionAmount) / 100;
		END
		ELSE
		BEGIN
			SET @dcomAmt = @dcomAmt;
			SET @SDcomAmt = @SDcomAmt;
		END
	END TRY

	BEGIN CATCH
		RETURN
	END CATCH

	PRINT '@dcomAmt:' + convert(VARCHAR(30), @dcomAmt) + ':' + @mType
	PRINT '@SDcomAmt' + convert(VARCHAR(30), @SDcomAmt) + ':' + @mType

	DECLARE @afterTransAmt NUMERIC(18, 5);
	DECLARE @beforeTransAmt NUMERIC(18, 5);

	IF (upper(@userType) = upper(@distributor))
	BEGIN
		SET @dcomAmt = @dcomAmt;
	END
	ELSE IF (upper(@userType) = upper(@Superdistributor))
	BEGIN
		SET @dcomAmt = @SDcomAmt;
	END

	BEGIN
		SET @beforeTransAmt = 0;

		IF (@ServiceID = @electricityServiceID)
		BEGIN
			SELECT @beforeTransAmt = DMRBalance
			FROM dbo.UserBalance
			WHERE UserID = @ParentID

			UPDATE [DBO].UserBalance
			SET DMRBalance = DMRBalance + @dcomAmt
			WHERE UserID = @ParentID

			SELECT @afterTransAmt = DMRBalance
			FROM dbo.UserBalance
			WHERE UserID = @ParentID
		END
		ELSE
		BEGIN
			SELECT @beforeTransAmt = CurrentBalance
			FROM dbo.UserBalance
			WHERE UserID = @ParentID

			UPDATE [DBO].UserBalance
			SET CurrentBalance = CurrentBalance + @dcomAmt
			WHERE UserID = @ParentID

			SELECT @afterTransAmt = CurrentBalance
			FROM dbo.UserBalance
			WHERE UserID = @ParentID
		END

		SET @TransDesc = 'Commision Amount:' + CONVERT(VARCHAR, @dcomAmt) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @RetailerName;

		PRINT @TransDesc

		--UPLINE COMM AND BALANCE                                
		EXEC dbo.TransactionDetailsInsertByUser @ParentID
			,@RechargeID
			,@ServiceID
			,@UserID
			,@ConsumerNo
			,@docType
			,--'RECHARGE',                 
			@beforeTransAmt
			,@afterTransAmt
			,0 --@TransactionAmount                                                   
			,@dcomAmt
			,''
			,'CREDIT'
			,@TransDesc
	END
END

GO
/****** Object:  StoredProcedure [dbo].[SelectUserBalance]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SelectUserBalance] @UserID INT                
AS                
BEGIN                
 SELECT CurrentBalance,      
 DMRBalance           
 FROM UserBalance                
 WHERE UserID = @UserID                
END 


GO
/****** Object:  StoredProcedure [dbo].[SelectUserSales]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--WARNING! ERRORS ENCOUNTERED DURING SQL PARSING!
--Author:Sainath Bhujbal
--Date:13 Sep 2017
--[SelectUserSales] 85,'30/11/2017','30/11/2017'
CREATE PROCEDURE [dbo].[SelectUserSales] @UserID INT
	,@FromDate VARCHAR(100)
	,@ToDate VARCHAR(100)
	,@Type VARCHAR(1000)
AS
BEGIN
	DECLARE @userType VARCHAR(20);
	DECLARE @admin VARCHAR(20) = 'ADMIN';
	DECLARE @distributor VARCHAR(20) = 'Distributor';
	DECLARE @retailer VARCHAR(20) = 'Retailer';
	DECLARE @apiUser VARCHAR(20) = 'APIUSER';

	SELECT @userType = UTM.UserType
	FROM [dbo].UserInformation(NOLOCK) UI
	INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @UserID

	IF (@Type = 'Summary')
	BEGIN
		IF (Upper(@userType) = Upper(@admin))
		BEGIN
			SELECT TOP (1000) --RE.DOC
				Convert(VARCHAR(20), RE.Doc, 103) AS DOC
				,SM.ServiceName
				,Sum(RE.Amount) AS Amount
				,Count(RE.Amount) AS RechargeCount
				,UI.UserName
				-- ,RE.UserID
				,UIN.UserName AS DistributerUserName
				,Sum(TD.DiscountAmount) AS CommissionGivenToRet
			FROM dbo.RECHARGE(NOLOCK) AS RE
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
			--INNER JOIN dbo.TransactionDetails(NOLOCK) AS TDAdmin ON TDAdmin.RechargeID = RE.RechargeID
			INNER JOIN ServiceMaster SM ON SM.ServiceID = RE.ServiceID
			WHERE TD.DOCType = 'RECHARGE'
				AND TD.CommissionCreditDebit = 'CREDIT'
				AND TD.AmountCreditDebit = 'DEBIT'
				AND RE.STATUS IN (
					'PROCESS'
					,'SUCCESS'
					)
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDate, 103)
					AND Convert(DATE, @ToDate, 103)
			GROUP BY ServiceName
				,UI.UserName
				,UIN.UserName
				,Convert(VARCHAR(20), RE.Doc, 103)
		END
		ELSE IF (Upper(@userType) = Upper(@distributor))
		BEGIN
			SELECT TOP (1000) --RE.DOC
				Convert(VARCHAR(20), RE.Doc, 103) AS DOC
				,SM.ServiceName
				,Sum(RE.Amount) AS Amount
				,Count(RE.Amount) AS RechargeCount
				,UI.UserName
				-- ,RE.UserID
				,UIN.UserName AS DistributerUserName
				,Sum(TD.DiscountAmount) AS CommissionGivenToRet
			FROM dbo.RECHARGE(NOLOCK) AS RE
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
			--INNER JOIN dbo.TransactionDetails(NOLOCK) AS TDAdmin ON TDAdmin.RechargeID = RE.RechargeID
			INNER JOIN ServiceMaster SM ON SM.ServiceID = RE.ServiceID
			WHERE TD.DOCType = 'RECHARGE'
				AND TD.CommissionCreditDebit = 'CREDIT'
				AND TD.AmountCreditDebit = 'DEBIT'
				AND RE.STATUS IN (
					'PROCESS'
					,'SUCCESS'
					)
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDate, 103)
					AND Convert(DATE, @ToDate, 103)
				AND @UserID IN (UI.ParentID)
			GROUP BY ServiceName
				,UI.UserName
				,UIN.UserName
				,Convert(VARCHAR(20), RE.Doc, 103)
		END
		ELSE IF (
				Upper(@userType) = Upper(@retailer)
				OR Upper(@userType) = Upper(@apiUser)
				)
		BEGIN
			SELECT TOP (1000) --RE.DOC
				Convert(VARCHAR(20), RE.Doc, 103) AS DOC
				,SM.ServiceName
				,Sum(RE.Amount) AS Amount
				,Count(RE.Amount) AS RechargeCount
				,UI.UserName
				-- ,RE.UserID
				,UIN.UserName AS DistributerUserName
				,Sum(TD.DiscountAmount) AS CommissionGivenToRet
			FROM dbo.RECHARGE(NOLOCK) AS RE
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
			--INNER JOIN dbo.TransactionDetails(NOLOCK) AS TDAdmin ON TDAdmin.RechargeID = RE.RechargeID
			INNER JOIN ServiceMaster SM ON SM.ServiceID = RE.ServiceID
			WHERE TD.DOCType = 'RECHARGE'
				AND TD.CommissionCreditDebit = 'CREDIT'
				AND TD.AmountCreditDebit = 'DEBIT'
				AND RE.STATUS IN (
					'PROCESS'
					,'SUCCESS'
					)
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDate, 103)
					AND Convert(DATE, @ToDate, 103)
				AND @UserID IN (UI.UserID)
			GROUP BY ServiceName
				,UI.UserName
				,UIN.UserName
				,Convert(VARCHAR(20), RE.Doc, 103)
		END
	END
	ELSE IF (@Type = 'Detail')
	BEGIN
		IF (Upper(@userType) = Upper(@admin))
		BEGIN
			SELECT TOP (1000) --RE.DOC
				Convert(VARCHAR(20), RE.Doc, 103) AS DOC
				,SM.ServiceName
				,Sum(RE.Amount) AS Amount
				,Count(RE.Amount) AS RechargeCount
				,UI.UserName
				-- ,RE.UserID
				,UIN.UserName AS DistributerUserName
				,Sum(TD.DiscountAmount) AS CommissionGivenToRet
			FROM dbo.RECHARGE(NOLOCK) AS RE
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
			--INNER JOIN dbo.TransactionDetails(NOLOCK) AS TDAdmin ON TDAdmin.RechargeID = RE.RechargeID
			INNER JOIN ServiceMaster SM ON SM.ServiceID = RE.ServiceID
			WHERE TD.DOCType = 'RECHARGE'
				AND TD.CommissionCreditDebit = 'CREDIT'
				AND TD.AmountCreditDebit = 'DEBIT'
				AND RE.STATUS IN (
					'PROCESS'
					,'SUCCESS'
					)
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDate, 103)
					AND Convert(DATE, @ToDate, 103)
			GROUP BY ServiceName
				,UI.UserName
				,UIN.UserName
				,Convert(VARCHAR(20), RE.Doc, 103)
		END
		ELSE IF (Upper(@userType) = Upper(@distributor))
		BEGIN
			SELECT TOP (1000) --RE.DOC
				Convert(VARCHAR(20), RE.Doc, 103) AS DOC
				,SM.ServiceName
				,Sum(RE.Amount) AS Amount
				,Count(RE.Amount) AS RechargeCount
				,UI.UserName
				-- ,RE.UserID
				,UIN.UserName AS DistributerUserName
				,Sum(TD.DiscountAmount) AS CommissionGivenToRet
			FROM dbo.RECHARGE(NOLOCK) AS RE
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
			--INNER JOIN dbo.TransactionDetails(NOLOCK) AS TDAdmin ON TDAdmin.RechargeID = RE.RechargeID
			INNER JOIN ServiceMaster SM ON SM.ServiceID = RE.ServiceID
			WHERE TD.DOCType = 'RECHARGE'
				AND TD.CommissionCreditDebit = 'CREDIT'
				AND TD.AmountCreditDebit = 'DEBIT'
				AND RE.STATUS IN (
					'PROCESS'
					,'SUCCESS'
					)
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDate, 103)
					AND Convert(DATE, @ToDate, 103)
				AND @UserID IN (UI.ParentID)
			GROUP BY ServiceName
				,UI.UserName
				,UIN.UserName
				,Convert(VARCHAR(20), RE.Doc, 103)
		END
		ELSE IF (
				Upper(@userType) = Upper(@retailer)
				OR Upper(@userType) = Upper(@apiUser)
				)
		BEGIN
			SELECT TOP (1000) --RE.DOC
				Convert(VARCHAR(20), RE.Doc, 103) AS DOC
				,SM.ServiceName
				,Sum(RE.Amount) AS Amount
				,Count(RE.Amount) AS RechargeCount
				,UI.UserName
				-- ,RE.UserID
				,UIN.UserName AS DistributerUserName
				,Sum(TD.DiscountAmount) AS CommissionGivenToRet
			FROM dbo.RECHARGE(NOLOCK) AS RE
			INNER JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
			INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
			INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
			--INNER JOIN dbo.TransactionDetails(NOLOCK) AS TDAdmin ON TDAdmin.RechargeID = RE.RechargeID
			INNER JOIN ServiceMaster SM ON SM.ServiceID = RE.ServiceID
			WHERE TD.DOCType = 'RECHARGE'
				AND TD.CommissionCreditDebit = 'CREDIT'
				AND TD.AmountCreditDebit = 'DEBIT'
				AND RE.STATUS IN (
					'PROCESS'
					,'SUCCESS'
					)
				AND Convert(DATE, RE.DOC, 103) BETWEEN Convert(DATE, @FromDate, 103)
					AND Convert(DATE, @ToDate, 103)
				AND @UserID IN (UI.UserID)
			GROUP BY ServiceName
				,UI.UserName
				,UIN.UserName
				,Convert(VARCHAR(20), RE.Doc, 103)
		END
	END
END



GO
/****** Object:  StoredProcedure [dbo].[SelectvalidUsersForBalanceTransfer]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SelectvalidUsersForBalanceTransfer] @SenderUserID INT
	,@ReceiverUserID INT
AS
BEGIN
	SELECT UI.UserTypeID AS SenderUserType
		,UI.ParentID AS SenderParentID
		,UF.UserTypeID AS ReceiverUserType
		,UF.ParentID AS ReceiverParentID
	FROM UserInformation UI
	INNER JOIN UserInformation UF ON UI.UserID = @SenderUserID
		AND UF.UserID = @ReceiverUserID
END



GO
/****** Object:  StoredProcedure [dbo].[SelecVersion]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SelecVersion] --[SelecVersion] '1.0'      

	@newVersion VARCHAR(50)

AS

BEGIN

	DECLARE @previesVersion VARCHAR(20);

	SELECT @previesVersion = [Version]

	FROM [dbo].[Version]

	IF (@newVersion IS NULL)

	BEGIN

		RETURN 0;

	END

	IF (@previesVersion < @newVersion)

	BEGIN

		UPDATE [dbo].[Version]

		SET Version = @newVersion

		SELECT *

		FROM [dbo].[Version]

	END

	ELSE

		SELECT *

		FROM [dbo].[Version]

END

GO
/****** Object:  StoredProcedure [dbo].[SenderBeneficiaryMappingDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		Sushant Yelpale
-- Create date: 9 Feb 2019
-- Description:	Soft remove beneficiary against Sender
-- =============================================
CREATE PROCEDURE [dbo].[SenderBeneficiaryMappingDelete]
	@SenderId varchar(50)
	,@BeneficiaryId Varchar(50)
	,@Output Int = 0 OUT
AS
BEGIN
	Update SenderBeneficiaryMapping
	SET IsActive = 0
	where SenderId = @SenderId
	AND BeneficiaryId = @BeneficiaryId

	SET @Output  = @@ROWCOUNT
END

GO
/****** Object:  StoredProcedure [dbo].[SenderBeneficiarySelectByMobileNo]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SenderBeneficiarySelectByMobileNo '8007141498'
CREATE PROCEDURE[dbo].[SenderBeneficiarySelectByMobileNo] @MobileNo VARCHAR(10)
AS
BEGIN
	SELECT isnull([BeneficiaryID], 0) AS BeneficiaryID
		,isnull(b.[Name], '') AS BeneficiaryName
		,isnull(b.[Mobile], '') AS Mobile
		,isnull([AccountNo], '') AS AccountNo
		,isnull([BankName], '') AS BankName
		,isnull([BranchName], '') AS BranchName
		,isnull([IFSC], '') AS IFSC
		,s.[SenderID]
		,s.[Name] AS SenderName
		,s.[Mobile] AS SenderMobile
		,isnull(b.Isverified, 0) AS Isverified
	FROM [dbo].sender s
	LEFT JOIN [dbo].[Beneficiary] b ON s.SenderID = b.SenderID
		AND b.Isdelete = 0
		AND b.IsVerifyOTP = 1
	WHERE s.[Mobile] = @MobileNo
		AND s.IsDelete = 0
		AND s.IsVerifyOTP = 1
	ORDER BY BeneficiaryID DESC
END



GO
/****** Object:  StoredProcedure [dbo].[SenderBeneficiarySelectCodeByID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SenderBeneficiarySelectCodeByID] @SenderID INT
	,@BeneficiaryID INT
AS
BEGIN
	DECLARE @SenderCode INT
	DECLARE @BeneficiaryCode INT

	SET @SenderCode = 0
	SET @BeneficiaryCode = 0
	SET @SenderCode = (
			SELECT SenderCode
			FROM Sender
			WHERE SenderID = @SenderID
			)
	SET @BeneficiaryCode = (
			SELECT BeneficiaryCode
			FROM [dbo].[Beneficiary]
			WHERE BeneficiaryID = @BeneficiaryID
			)

	SELECT isnull(@SenderCode, 0) AS SenderCode
		,isnull(@BeneficiaryCode, 0) AS BeneficiaryCode
END



GO
/****** Object:  StoredProcedure [dbo].[SenderBeneficiarySelectIDByBeneficiaryCode]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SenderBeneficiarySelectIDByBeneficiaryCode] @BeneficiaryCode VARCHAR(50)
AS
BEGIN
	SELECT BeneficiaryID
		,SenderID
	FROM [dbo].[Beneficiary]
	WHERE BeneficiaryCode = @BeneficiaryCode
		AND IsVerifyOTP = 1
		AND Isdelete = 0
END



GO
/****** Object:  StoredProcedure [dbo].[SenderBeneficiaryUpdateCodeByID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SenderBeneficiarySelectIDByBeneficiaryCode '1363'

CREATE PROCEDURE[dbo].[SenderBeneficiaryUpdateCodeByID] @SenderID INT
	,@BeneficiaryID INT
	,@SenderCode INT
	,@BeneficiaryCode INT
AS
BEGIN
	UPDATE [dbo].[Sender]
	SET SenderCode = @SenderCode
	WHERE SenderID = @SenderID

	UPDATE [dbo].[Beneficiary]
	SET BeneficiaryCode = @BeneficiaryCode
	WHERE BeneficiaryID = @BeneficiaryID
END



GO
/****** Object:  StoredProcedure [dbo].[SenderBeneficiaryUpdateIsVerifyOTP]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SenderBeneficiaryUpdateIsVerifyOTP] @SenderID INT
	,@BeneficiaryID INT
AS
BEGIN
	UPDATE [dbo].[Sender]
	SET IsVerifyOTP = 1
	WHERE SenderID = @SenderID

	UPDATE [dbo].[Beneficiary]
	SET IsVerifyOTP = 1
	WHERE BeneficiaryID = @BeneficiaryID
END



GO
/****** Object:  StoredProcedure [dbo].[SenderIDByMobile]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SenderIDByMobile] @Mobile VARCHAR(10)
AS
BEGIN
	SELECT isnull(SenderID, 0) AS SenderID
	FROM Sender
	WHERE Mobile = @Mobile
END



GO
/****** Object:  StoredProcedure [dbo].[SenderInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:  Sushant Yelpale       
-- Create date: 9 Feb 2019      
-- Description: insert Sender Details      
-- =============================================      
CREATE PROCEDURE [dbo].[SenderInsert]   
 @MobileNumber VARCHAR(50)  
 ,@FirstName VARCHAR(50)  
 ,@LastName VARCHAR(50)  
 ,@UserId INT  
AS  
BEGIN  
 IF NOT EXISTS (  
   SELECT TOP 1 MobileNumber  
   FROM Sender  
   WHERE MobileNumber = @MobileNumber  
   )  
 BEGIN  
  INSERT INTO [dbo].[Sender] (  
   [MobileNumber]  
   ,[UserId]  
   ,[FirstName]  
   ,[LastName]  
   )  
  VALUES (  
   @MobileNumber  
   ,@UserId  
   ,@FirstName  
   ,@LastName  
   )  
 END  
 ELSE  
 BEGIN  
  Update [Sender]  
  SET [FirstName] = @FirstName,  
  [LastName] = @LastName,  
  [UserId] = @UserId  
  WHERE MobileNumber = @MobileNumber  
 END  
END  
GO
/****** Object:  StoredProcedure [dbo].[SenderKYCByMobile]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SenderKYCByMobile] --SenderKYCByMobile '9766676093'
	@SenderNumber VARCHAR(10)
AS
BEGIN
	DECLARE @SenderID INT

	SET @SenderID = 0;

	SELECT @SenderID = SenderID
	FROM [dbo].[Sender]
	WHERE [Mobile] = @SenderNumber
		AND IsDelete = 0

	SELECT [SenderID]
		,[Status]
	FROM [dbo].[SenderKYC]
	WHERE [SenderID] = @SenderID
		AND [Isdeleted] = 0
END



GO
/****** Object:  StoredProcedure [dbo].[SenderKYCInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SenderKYCInsert] @SenderID INT
	,@KYCImage VARCHAR(1000)
	,@Type VARCHAR(50)
AS
BEGIN
	IF EXISTS (
			SELECT *
			FROM [dbo].[SenderKYC]
			WHERE [SenderID] = @SenderID
				AND [Type] = @Type
			)
	BEGIN
		UPDATE [dbo].[SenderKYC]
		SET [SenderID] = @SenderID
			,[ImagePath] = @KYCImage
			,[Status] = 'Pending'
		WHERE [SenderID] = @SenderID
			AND [Type] = @Type
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[SenderKYC] (
			[SenderID]
			,[Type]
			,[ImagePath]
			,[Status]
			)
		VALUES (
			@SenderID
			,@Type
			,@KYCImage
			,'Pending'
			)
	END
END



GO
/****** Object:  StoredProcedure [dbo].[SenderOTPInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		Sushant Yelpale
-- Create date: 9 Feb 2019
-- Description:	insert otp to senderOTP table 
-- =============================================
CREATE PROCEDURE [dbo].[SenderOTPInsert]
	@SenderId Varchar(50)
	,@OTP Varchar(50)
AS
BEGIN
	INSERT INTO [dbo].[SenderOTP]
           ([SenderId]
           ,[OTP])
     VALUES
           (@SenderId
           ,@OTP
		   )
END

GO
/****** Object:  StoredProcedure [dbo].[SenderSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:  Sushant Yelpale      
-- Create date: 9 Feb 2019      
-- Description: Select Sender Details by MobileNumber      
-- SenderSelect '9975688036'      
-- =============================================      
CREATE PROCEDURE [dbo].[SenderSelect]      
 @MobileNumber Varchar(20)      
AS      
BEGIN      
 Declare @UtilisedFund Decimal(18,5);      
      
 Select @UtilisedFund = ISNULL(SUM(Amount),0)      
 from MoneyTransfer MT      
 Inner Join Sender S ON CONVERT(Varchar(50),S.SenderId) = MT.SenderID      
 where S.MobileNumber = @MobileNumber      
 And Month(MT.DOC) = Month(Getdate())      
 And Year(MT.DOC) = Year(Getdate())      
 AND MT.Status in ('SUCCESS','PROCESS')    
      
 SELECT [SenderId]      
      ,[UserId]      
      ,[FirstName]      
      ,[LastName]      
      ,[IsVerified]      
      ,[DOC]      
      ,[IsActive]      
      ,[IsKycApproved]      
      ,[PAN]      
      ,[PANDocPath]      
      ,[KycDoc1]      
      ,[KycDoc1Path]      
      ,[KycUploadedUserId]      
      ,[Aadhar]      
      ,[AadharDocPath]      
      ,@UtilisedFund As UtilisedFund      
  FROM [dbo].[Sender] S      
  WHERE @MobileNumber = [MobileNumber]     
  AND  [IsActive] = 1
END 
GO
/****** Object:  StoredProcedure [dbo].[SenderSelectBySenderMobileNumber]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:  Sushant Yelpale     
-- Create date: 11 Feb 2019    
-- Description: Selects All beneficiary by sender mobile Number    
-- SenderSelectBySenderMobileNumber '9975688036'
-- =============================================    
CREATE PROCEDURE [dbo].[SenderSelectBySenderMobileNumber]    
 @MobileNumber Varchar(20)    
AS    
BEGIN    
 Declare @SenderId Varchar(50);    
    
 SELECT @SenderID = SenderID    
 from Sender    
 where MobileNumber = @MobileNumber    
    
 if @SenderID is NULL    
 BEGIN    
  return;    
 END    
    
 SELECT B.[BeneficiaryId]    
	  ,@SenderId As SenderId    
      ,@MobileNumber As SenderMobileNumber
	  ,B.BeneficiaryId
      ,B.[FirstName]    
      ,B.[LastName]    
      ,B.[AccountNumber]    
      ,B.[IFSC]    
      ,B.[BankName]
      ,SBM.[DOC]    
      ,B.[IsVerified]    
   ,@MobileNumber As SenderMobileNumber  
 FROM [dbo].[Beneficiary] B    
 INNER JOIN SenderBeneficiaryMapping SBM On B.BeneficiaryId = SBM.BeneficiaryId    
 AND SBM.SenderId = @SenderId    
END 




GO
/****** Object:  StoredProcedure [dbo].[SenderUpdateSubmitOtp]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:  Sushant Yelpale  
-- Create date: 9 Feb 2019  
-- Description: submit OTP for sender registration  
-- 
-- =============================================  
CREATE PROCEDURE [dbo].[SenderUpdateSubmitOtp] @MobileNumber VARCHAR(50)  
 ,@OTP VARCHAR(50)  
 ,@STATUS VARCHAR(20) OUT  
 ,@MESSAGE VARCHAR(20)  OUT  
AS  
BEGIN  
 DECLARE @SenderId VARCHAR(50);  
 DECLARE @OTPvalidTime INT = 15;  
 SET @STATUS = 'FAILURE'
 SET @MESSAGE = ''

 IF EXISTS (  
   SELECT TOP 1 SenderId  
   FROM Sender  
   WHERE @MobileNumber = MobileNumber  
   AND IsVerified = 0
   )  
 BEGIN  
  SELECT TOP 1 @SenderId = CONVERT(VARCHAR(50), SenderId)  
  FROM Sender  
  WHERE @MobileNumber = MobileNumber  
 END  
 ELSE  
 BEGIN  
  SET @MESSAGE = 'Sender not found'  
  RETURN;  
 END  
 
 PRINT  @SenderId
 
 DECLARE @SenderOTPId VARCHAR(50);  
 DECLARE @SenderOTP VARCHAR(50);  
 DECLARE @OTPDateTime DATETIME;  
  
 SELECT TOP 1 @SenderOTPId = SenderOTPId  
  ,@SenderOTP = [OTP]  
  ,@OTPDateTime = DOC  
 FROM [dbo].[SenderOTP]  
 WHERE [SenderId] = @SenderId  
  AND [IsActive] = 1  
  AND [OTP] = @OTP  
 ORDER BY doc DESC  
  
 IF (@OTPDateTime IS NULL)  
 BEGIN  
  SET @MESSAGE = 'OTP invalid'  
  RETURN;  
 END  
  
 PRINT (DATEADD(mi, @OTPvalidTime, @OTPDateTime))

 IF (getdate()  > DATEADD(mi, @OTPvalidTime, @OTPDateTime))  
 BEGIN  
  SET @MESSAGE = 'OTP expired'  
  RETURN;  
 END  
 SET @MESSAGE = ''  

 UPDATE [SenderOTP]  
 SET [IsActive] = 0  
 WHERE SenderId = @SenderId  
  
 UPDATE SENDER  
 SET IsVerified = 1  
 WHERE SENDERID = @SenderId  
  
 SET @STATUS = 'SUCCESS'  
  
 RETURN;  
END  
GO
/****** Object:  StoredProcedure [dbo].[SentSMSInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SentSMSInsert] @UserID INT
	,@MobNo VARCHAR(10)
	,@MessageType VARCHAR(50)
	,@Message VARCHAR(500)
	,@SenderId varchar(50) = null
AS
BEGIN
	INSERT INTO [dbo].[SentSMS] (
		[UserID]
		,[MobileNumber]
		,[MessageType]
		,[Message]
		,[SenderId]
		)
	VALUES (
		@UserID
		,@MobNo
		,@MessageType
		,@Message
		,@SenderId
		);
END



GO
/****** Object:  StoredProcedure [dbo].[SentSMSSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Name : Abhiraj Shinde
--Objective :To show SMS Report
-----------------------------------
--SentSMSSelect 0,'0', 20181123,20181123
CREATE PROC [dbo].[SentSMSSelect] @UserID INT
	,@MessageType VARCHAR(50)
	,@FromDate VARCHAR(50)
	,@ToDate VARCHAR(50)
AS
BEGIN
	CREATE TABLE #TotalReport (
		UserID INT
		,UserName VARCHAR(100)
		,MobileNumber VARCHAR(50)
		,Message VARCHAR(500)
		,DOC DATETIME
		,MessageType VARCHAR(50)
		,Response varchar(max)

		);

	WITH CTETotalReport (
		UserName
		,UserID
		,MobileNumber
		,Message
		,DOC
		,MessageType
		,Response
		)
	AS (
		SELECT ui.[UserName]
			,ss.[UserID]
			,ss.[MobileNumber]
			,ss.[Message]
		   
			,convert(VARCHAR(12), ss.[SendingDate], 113) + right(convert(VARCHAR(39), ss.[SendingDate], 22), 11) AS DOC
			,ss.[MessageType]
			 ,ss.Response 
		FROM [dbo].[SentSMS] ss
		INNER JOIN [dbo].[UserInformation] ui ON ss.[UserID] = ui.[UserID]
		WHERE [DOCInt] >= @FromDate
			AND [DOCInt] <= @ToDate
		)
	INSERT INTO #TotalReport (
		UserName
		,UserID
		,MobileNumber
		,Message
		,DOC
		,MessageType
		 ,Response 
		)
	SELECT UserName
		,UserID
		,MobileNumber
		,Message
		,DOC
		,MessageType
		, isnull(Response ,'')as Response
	FROM CTETotalReport

	IF (@UserID = 0)
	BEGIN
		IF (@MessageType = '0')
		BEGIN
			SELECT UserName
				,UserID
				,MobileNumber
				,Message
				--,DOC
				,convert(VARCHAR(12), DOC, 113) + right(convert(VARCHAR(39), DOC, 22), 11) AS DOC
				,MessageType
				, Response 
			FROM #TotalReport
			ORDER BY DOC DESC
		END
		ELSE
		BEGIN
			SELECT UserName
				,UserID
				,MobileNumber
				,Message
				--,DOC
				,convert(VARCHAR(12), DOC, 113) + right(convert(VARCHAR(39), DOC, 22), 11) AS DOC
				,MessageType
				 ,Response 
			FROM #TotalReport
			WHERE [MessageType] = @MessageType
			ORDER BY DOC DESC
		END
	END
	ELSE
	BEGIN
		IF (@MessageType = '0')
		BEGIN
			SELECT UserName
				,UserID
				,MobileNumber
				,Message
				--,DOC
				,convert(VARCHAR(12), DOC, 113) + right(convert(VARCHAR(39), DOC, 22), 11) AS DOC
				,MessageType
				, Response 
			FROM #TotalReport
			WHERE [UserID] = @UserID
			ORDER BY DOC DESC
		END
		ELSE
		BEGIN
			SELECT UserName
				,UserID
				,MobileNumber
				,Message
				--,DOC
				,convert(VARCHAR(12), DOC, 113) + right(convert(VARCHAR(39), DOC, 22), 11) AS DOC
				,MessageType
				 ,Response 
			FROM #TotalReport
			WHERE [UserID] = @UserID
				AND [MessageType] = @MessageType
			ORDER BY DOC DESC
		END
	END
END



GO
/****** Object:  StoredProcedure [dbo].[SentSMSSelectTop1]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sushant yelpale	
-- Create date: 6 Feb 2019
-- Description:	Selects top 1 sms from, to date
-- =============================================
CREATE PROCEDURE [dbo].[SentSMSSelectTop1]
  @UserID INT  
 ,@MessageType VARCHAR(50)  
 ,@FromDate VARCHAR(50)  
 ,@ToDate VARCHAR(50)  
AS
BEGIN
	Select Top 1 SMSID
	from SentSMS 
	where UserID = @UserID
	and convert(DATE, SendingDate, 103) >= convert(DATE, @FromDate, 103)
	and convert(DATE, SendingDate, 103) <= convert(DATE, @ToDate, 103)
	and upper(MessageType)= upper(@MessageType) 
END

GO
/****** Object:  StoredProcedure [dbo].[ServiceInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :10-2-17
--Purpose ServiceMaster Insert
--==========================================
--ServiceInsert Electricity
CREATE PROCEDURE [dbo].[ServiceInsert] @ServiceName VARCHAR(50)
AS
BEGIN
	INSERT INTO ServiceMaster (ServiceName)
	VALUES (@ServiceName)
END



GO
/****** Object:  StoredProcedure [dbo].[ServiceMasterSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :10-7-17
--Purpose ServiceMaster Select
--==========================================
CREATE PROCEDURE [dbo].[ServiceMasterSelect]
AS
BEGIN
	SELECT 0 AS ServiceID
		,'All' AS ServiceName
		,0.0 AS OneTime
		,0.0 AS Daily
		,0.0 AS Monthly
		,0.0 AS MinimumBalanace
		,0.0 AS PerTransactionCharge
		,'0.00' AS DailyDebitTime
		,'00' AS MonthlyDebitDate
		,0 AS IsDeleted
	FROM dbo.ServiceMaster
	
	UNION
	
	SELECT ServiceID
		,ServiceName
		,OneTime
		,Daily
		,Monthly
		,MinimumBalanace
		,PerTransactionCharge
		,REPLACE(REPLACE(DailyDebitTime, ' ', ''), ':', '')
		,MonthlyDebitDate
		,IsDeleted
	FROM dbo.ServiceMaster(NOLOCK)
		--where IsDeleted=0
END



GO
/****** Object:  StoredProcedure [dbo].[ServiceMasterSelectServices]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================  
--Auther :Priyanka  
--Date :10-7-17  
--Purpose ServiceMaster Select  
--==========================================  
CREATE PROCEDURE [dbo].[ServiceMasterSelectServices]
AS
BEGIN
	SELECT 0 AS ServiceID
		,'All' AS ServiceName
	FROM dbo.ServiceMaster(NOLOCK)
	
	UNION
	
	SELECT ServiceID
		,ServiceName
	FROM [dbo].ServiceMaster(NOLOCK)
	WHERE IsDeleted = 0
END



GO
/****** Object:  StoredProcedure [dbo].[ServiceMasterUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :10-7-17
--Purpose ServiceMaster update
--==========================================
CREATE PROCEDURE [dbo].[ServiceMasterUpdate] @ServiceID INT
	,@MinimumBalanace INT
	,@IsDeleted BIT
	--@OneTime decimal,@Daily decimal, @Monthly decimal,@PerTransactionCharge int, @DailyDebitTime varchar(50), @MonthlyDebitDate varchar(50),
AS
BEGIN
	UPDATE ServiceMaster
	SET
		-- OneTime = @OneTime, Daily = @Daily, Monthly = @Monthly,PerTransactionCharge =  @PerTransactionCharge, DailyDebitTime=@DailyDebitTime, MonthlyDebitDate =@MonthlyDebitDate,
		IsDeleted = @IsDeleted
		,MinimumBalanace = @MinimumBalanace
	WHERE ServiceID = @ServiceID
END



GO
/****** Object:  StoredProcedure [dbo].[ServiceMasterValidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :9-2-17
--Purpose ServiceMaster Validation
--==========================================
--ServiceMasterValidation Mobile
CREATE PROCEDURE [dbo].[ServiceMasterValidation] @ServiceName VARCHAR(50)
AS
BEGIN
	SELECT ServiceID
	FROM ServiceMaster
	WHERE ServiceName = @ServiceName
END



GO
/****** Object:  StoredProcedure [dbo].[SetCommissionInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  <Sainath Bhujbal>            
-- ALTER  date: <25 Feb 2017 ,>            
-- Description: Set commission for balance transfer    
-- =============================================  
--SetCommissionInsert 4
CREATE PROCEDURE [dbo].[SetCommissionInsert] @CommissionAmount INT
AS
BEGIN
	DELETE
	FROM SetCommission

	INSERT INTO SetCommission (CommissionAmount)
	VALUES (@CommissionAmount)
END



GO
/****** Object:  StoredProcedure [dbo].[SetCommissionSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================                                                                
-- AUTHOR:Priyanka Deshmukh                                                                 
-- ALTER  DATE: 03 April 2017                                                                  
-- DESCRIPTION: Select set commission value                                                              
-- ========================================================================    
CREATE PROCEDURE [dbo].[SetCommissionSelect]
AS
BEGIN
	SELECT CommissionAmount
	FROM SetCommission
END



GO
/****** Object:  StoredProcedure [dbo].[SlabCommissionDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sushant Yelpale
-- Create date: 15 Oct 2018
-- Description:	delete Default Slab
-- =============================================
CREATE PROCEDURE [dbo].[SlabCommissionDelete] @SlabID INT
	,@OUTPUT VARCHAR(500) OUT
AS
BEGIN
	DECLARE @TotalSlabs INT

	SELECT @TotalSlabs = ISNULL(COUNT(1), 0)
	FROM [SlabCommission]

	IF (@TotalSlabs <= 1)
	BEGIN
		SET @OUTPUT = 'Can Not Delete Last Slab'

		RETURN 0;
	END

	DELETE
	FROM [SlabCommission]
	WHERE ID = @SlabID

	SET @OUTPUT = 'Slab Deleted Successfully'
END



GO
/****** Object:  StoredProcedure [dbo].[SlabCommissionInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modifay:Rahul Hembade 
--Reson: duplicate amount check  
CREATE PROCEDURE[dbo].[SlabCommissionInsert] @FromAmount DECIMAL(18, 5)
	,@ToAmount DECIMAL(18, 5)
	,@RetailerFixRs DECIMAL(18, 5)
	,@RetailerFlexi DECIMAL(18, 5)
	,@DistributorFixRs DECIMAL(18, 5)
	,@DistributorFlexi DECIMAL(18, 5)
	,@SuperDistributorFixRs DECIMAL(18, 5)
	,@SuperDistributorFlexi DECIMAL(18, 5)
AS
BEGIN
	IF EXISTS (
			SELECT *
			FROM [dbo].[SlabCommission]
			WHERE (
					@ToAmount >= [FromAmount]
					AND @ToAmount <= [ToAmount]
					)
				OR (
					@FromAmount >= [FromAmount]
					AND @ToAmount <= [ToAmount]
					)
				OR (
					@FromAmount >= [FromAmount]
					AND @FromAmount <= [ToAmount]
					)
			)
		--@FromAmount>=[FromAmount] and @ToAmount<=[ToAmount])
	BEGIN
		RETURN;
	END

	INSERT INTO [dbo].[SlabCommission] (
		[FromAmount]
		,[ToAmount]
		,[RetailerFixRs]
		,[RetailerFlexi]
		,[DistributorFixRs]
		,[DistributorFlexi]
		,[SuperDistributorFixRs]
		,[SuperDistributorFlexi]
		)
	VALUES (
		@FromAmount
		,@ToAmount
		,@RetailerFixRs
		,@RetailerFlexi
		,@DistributorFixRs
		,@DistributorFlexi
		,@SuperDistributorFixRs
		,@SuperDistributorFlexi
		)
END



GO
/****** Object:  StoredProcedure [dbo].[SlabCommissionSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE[dbo].[SlabCommissionSelect]
AS
BEGIN
	SELECT ID
		,[FromAmount]
		,[ToAmount]
		,[RetailerFixRs]
		,[RetailerFlexi]
		,[DistributorFixRs]
		,[DistributorFlexi]
		,[SuperDistributorFixRs]
		,[SuperDistributorFlexi]
	FROM [dbo].[SlabCommission]
END



GO
/****** Object:  StoredProcedure [dbo].[SlabCommissionSelectByUser]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[SlabCommissionSelectByUser] @USERID INT
	,@TransactionAmount DECIMAL(18, 5)
	,@RetailerCommAmt DECIMAL(18, 5) OUTPUT
	,@DistributorCommAmt DECIMAL(18, 5) OUTPUT
	,@SuperDistributorCommAmt DECIMAL(18, 5) OUTPUT
AS
BEGIN
	SET @RetailerCommAmt = 0;
	SET @DistributorCommAmt = 0;
	SET @SuperDistributorCommAmt = 0;

	DECLARE @userTypeID INT

	SELECT @userTypeID = UI.UserTypeID
	FROM [dbo].UserInformation(NOLOCK) UI
	WHERE UI.UserID = @USERID

	DECLARE @RetailerFixRs DECIMAL(18, 5)
	DECLARE @RetailerFlexi DECIMAL(18, 5)
	DECLARE @RetailerFlexiValue DECIMAL(18, 5)

	SET @RetailerFlexiValue = 0;

	DECLARE @DistributorFixRs DECIMAL(18, 5)
	DECLARE @DistributorFlexi DECIMAL(18, 5)
	DECLARE @DistributorFlexiValue DECIMAL(18, 5)

	SET @DistributorFlexiValue = 0;

	DECLARE @SuperDistributorFixRs DECIMAL(18, 5)
	DECLARE @SuperDistributorFlexi DECIMAL(18, 5)
	DECLARE @SuperDistributorFlexiValue DECIMAL(18, 5)

	SET @SuperDistributorFlexiValue = 0;

	DECLARE @GroupID INT

	SET @GroupID = 0

	SELECT @GroupID = GroupID
	FROM [dbo].[UserwiseSlabCommission]
	WHERE UserID = @USERID

	SELECT @RetailerFixRs = isnull(RetailerFixRs, 0)
		,@RetailerFlexi = isnull(RetailerFlexi, 0)
		,@DistributorFixRs = isnull(DistributorFixRs, 0)
		,@DistributorFlexi = isnull(DistributorFlexi, 0)
		,@SuperDistributorFixRs = isnull(SuperDistributorFixRs, 0)
		,@SuperDistributorFlexi = isnull(SuperDistributorFlexi, 0)
	FROM [dbo].[GroupDetail]
	WHERE [GroupID] = @GroupID
		AND @TransactionAmount BETWEEN FromAmount
			AND ToAmount

	IF (
			@RetailerFixRs IS NULL
			OR @RetailerFlexi IS NULL
			)
	BEGIN
		SELECT @RetailerFixRs = isnull(RetailerFixRs, 0)
			,@RetailerFlexi = isnull(RetailerFlexi, 0)
			,@DistributorFixRs = isnull(DistributorFixRs, 0)
			,@DistributorFlexi = isnull(DistributorFlexi, 0)
			,@SuperDistributorFixRs = isnull(SuperDistributorFixRs, 0)
			,@SuperDistributorFlexi = isnull(SuperDistributorFlexi, 0)
		FROM [dbo].[SlabCommission]
		WHERE @TransactionAmount BETWEEN FromAmount
				AND ToAmount
	END

	---------------------------Retailer------------------------------------------------    
	SET @RetailerFlexiValue = @RetailerFlexi * @TransactionAmount / 100
	SET @RetailerCommAmt = @RetailerFixRs

	IF (@RetailerFlexiValue < @RetailerFixRs)
	BEGIN
		SET @RetailerCommAmt = @RetailerFlexiValue
	END

	-------------------------------------distributor---------------------------------------------------------    
	SET @DistributorFlexiValue = @DistributorFlexi * @TransactionAmount / 100
	SET @DistributorCommAmt = @DistributorFixRs

	IF (@DistributorFlexiValue > @DistributorFixRs)
	BEGIN
		SET @DistributorCommAmt = @DistributorFlexiValue
	END

	----------------------------------------------------------------------------------------------------------    
	-------------------------------------SuperDistributor---------------------------------------------------------    
	SET @SuperDistributorFlexiValue = @SuperDistributorFlexi * @TransactionAmount / 100
	SET @SuperDistributorCommAmt = @SuperDistributorFixRs

	IF (@SuperDistributorFlexiValue > @SuperDistributorFixRs)
	BEGIN
		SET @SuperDistributorCommAmt = @SuperDistributorFlexiValue
	END
			--------------------------------------------------------------------------------------------------------------------------------------    
END



GO
/****** Object:  StoredProcedure [dbo].[SlabCommissionSelectForUser]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SlabCommissionSelectForUser 3,4000        
CREATE PROC [dbo].[SlabCommissionSelectForUser] @USERID INT        
 ,@TransactionAmount DECIMAL        
AS        
BEGIN        
 DECLARE @RetailerCommAmt DECIMAL(18, 5)=0;        
 DECLARE @DistributorCommAmt DECIMAL(18, 5)=0;        
 DECLARE @SuperDistributorCommAmt DECIMAL(18, 5)=0;        
 DECLARE @GroupID INT        
        
 SET @GroupID = 0        
        
 SELECT @GroupID = GroupID        
 FROM [dbo].[UserwiseSlabCommission]        
 WHERE UserID = @USERID        
        
 PRINT @GroupID        
        
 IF ( @GroupID is NULL or @GroupID = 0 )        
 BEGIN        
  SELECT @RetailerCommAmt = RetailerFixRs        
  FROM [dbo].[SlabCommission]        
  WHERE @TransactionAmount >= FromAmount        
   AND @TransactionAmount <= ToAmount        
 END        
 ELSE        
 BEGIN        
  EXECUTE [dbo].SlabCommissionSelectByUser @USERID = @USERID        
   ,@TransactionAmount = @TransactionAmount        
   ,@RetailerCommAmt = @RetailerCommAmt OUTPUT        
   ,@DistributorCommAmt = @DistributorCommAmt OUTPUT        
   ,@SuperDistributorCommAmt = @SuperDistributorCommAmt OUTPUT        
 END        
        
 PRINT @RetailerCommAmt        
        
 if(@RetailerCommAmt !=0)        
 begin        
 SELECT @RetailerCommAmt AS RetailerCommAmt        
 end        
 else        
 begin        
  SELECT -100 AS RetailerCommAmt        
 end        
END 
GO
/****** Object:  StoredProcedure [dbo].[SlabCommissionUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SlabCommissionUpdate] @FromAmount DECIMAL(18, 5)
	,@ToAmount DECIMAL(18, 5)
	,@RetailerFixRs DECIMAL(18, 5)
	,@RetailerFlexi DECIMAL(18, 5)
	,@DistributorFixRs DECIMAL(18, 5)
	,@DistributorFlexi DECIMAL(18, 5)
	,@SuperDistributorFixRs DECIMAL(18, 5)
	,@SuperDistributorFlexi DECIMAL(18, 5)
	,@ID INT
AS
BEGIN
	UPDATE [dbo].[SlabCommission]
	SET [RetailerFixRs] = @RetailerFixRs
		,[RetailerFlexi] = @RetailerFlexi
		,[DistributorFixRs] = @DistributorFixRs
		,[DistributorFlexi] = @DistributorFlexi
		,[SuperDistributorFixRs] = @SuperDistributorFixRs
		,[SuperDistributorFlexi] = @SuperDistributorFlexi
	WHERE ID = @ID
END



GO
/****** Object:  StoredProcedure [dbo].[SMSMasterUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SMSMasterUpdate] @SMSUrl VARCHAR(3000)
	,@UserID INT
	,@SMSID INT
	,@BalanceCheckUrl VARCHAR(max)
AS
BEGIN
	UPDATE SMSMaster
	SET SMSString = @SMSUrl
		,BalanceCheckUrl = @BalanceCheckUrl
		,DOC = (CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30')))
	WHERE SMSID = @SMSID
END

GO
/****** Object:  StoredProcedure [dbo].[SMSRechargeRequestLogInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SMSRechargeRequestLogInsert] @absurl VARCHAR(max)
	,@SenderNumber VARCHAR(20) = NULL
	,@MobileNumber VARCHAR(20) = NULL
	,@Operator VARCHAR(20) = NULL
	,@Amount VARCHAR(20) = NULL
	,@UserID VARCHAR(20) = NULL
AS
BEGIN
	INSERT INTO [dbo].[SMSRechargeRequestLog] (
		[Request]
		,SenderNumber
		,MobileNumber
		,Operator
		,Amount
		,UserID
		)
	VALUES (
		@absurl
		,@SenderNumber
		,@MobileNumber
		,@Operator
		,@Amount
		,@UserID
		)
END






GO
/****** Object:  StoredProcedure [dbo].[SMSRequestInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SMSRequestInsert] @URL VARCHAR(max)
AS
BEGIN
	INSERT INTO [dbo].[SMSRechargeRequestLog] ([Request])
	VALUES (@URL)
END



GO
/****** Object:  StoredProcedure [dbo].[SMSURLActDeact]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SMSURLActDeact] @SMSID INT
	,@IsActive INT
AS
BEGIN
	UPDATE [SMSMaster]
	SET [IsActive] = @IsActive
	WHERE SMSID = @SMSID
END



GO
/****** Object:  StoredProcedure [dbo].[SMSURLInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[SMSURLInsert] @SMSURL VARCHAR(max),
@BalanceCheckURL varchar(max) =null

AS

BEGIN

	UPDATE [SMSMaster]

	SET [IsActive] = 0

	WHERE [IsActive] = 1



	INSERT INTO [SMSMaster] (

		SMSString
,BalanceCheckUrl
		,[IsActive]

		)

	VALUES (

		@SMSURL
		,@BalanceCheckURL
		,1

		)

END





GO
/****** Object:  StoredProcedure [dbo].[SMSURLSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE[dbo].[SMSURLSelect]

AS

BEGIN

	SELECT SMSID

		,SMSString

		,[IsActive]
		,isNull([BalanceCheckUrl],'')as [BalanceCheckUrl]

	FROM [SMSMaster]

END



GO
/****** Object:  StoredProcedure [dbo].[SpecialAmountInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :10-2-17
--Purpose SpecialAmount Insert
--==========================================
--
CREATE PROCEDURE [dbo].[SpecialAmountInsert] @OperatorID INT
	,@CorelatedOperator INT
	,@Amount INT
AS
BEGIN
	INSERT INTO SpecialAmount (
		OperatorID
		,CorelatedOperatorID
		,Amount
		)
	VALUES (
		@OperatorID
		,@CorelatedOperator
		,@Amount
		)
END



GO
/****** Object:  StoredProcedure [dbo].[SpecialAmountSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :10-2-17
--Purpose SpecialAmount Select
--==========================================
CREATE PROCEDURE [dbo].[SpecialAmountSelect]
AS
BEGIN
	SELECT OM.OperatorName
		,OM.OperatorID
		,SM.Amount
		,SM.CorelatedOperatorID
		,OMS.OperatorName AS CorelatedOperatorName
		,SM.IsActive
	FROM dbo.OperatorMaster OM(NOLOCK)
	INNER JOIN dbo.SpecialAmount SM(NOLOCK) ON OM.OperatorID = SM.OperatorID
	INNER JOIN dbo.OperatorMaster OMS(NOLOCK) ON OMS.OperatorID = SM.CorelatedOperatorID
END



GO
/****** Object:  StoredProcedure [dbo].[SpecialAmountUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :10-2-17
--Purpose SpecialAmount Validation
--==========================================
CREATE PROCEDURE [dbo].[SpecialAmountUpdate] @OperatorID INT
	,@CorelatedOperator INT
	,@Amount INT
	,@IsActive BIT
AS
BEGIN
	UPDATE SpecialAmount
	SET Amount = @Amount
		,IsActive = @IsActive
	WHERE OperatorID = @OperatorID
		AND CorelatedOperatorID = @CorelatedOperator
END



GO
/****** Object:  StoredProcedure [dbo].[SpecialAmountValidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :9-2-17
--Purpose SpecialAmount Validation
--==========================================
--SpecialAmountValidation 1,1,100
CREATE PROCEDURE [dbo].[SpecialAmountValidation] @CorelatedOperator INT
	,@OperatorID INT
	,@Amount INT
AS
BEGIN
	SELECT SpecialAmountID
	FROM SpecialAmount
	WHERE CorelatedOperatorID = @CorelatedOperator
		AND Amount = @Amount
		AND OperatorID = @OperatorID
END



GO
/****** Object:  StoredProcedure [dbo].[StateSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :8-2-2017
--Purpose :Select state
--==========================================
CREATE PROCEDURE [dbo].[StateSelect]
AS
BEGIN
	SELECT 'ALL' AS StateName
		,0 AS StateID
		,'0' AS StateVal
	FROM dbo.STATE
	
	UNION
	
	SELECT StateName
		,StateID
		,StateVal
	FROM dbo.STATE
END



GO
/****** Object:  StoredProcedure [dbo].[SubscriptionSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[SubscriptionSelect]
AS
BEGIN
	DECLARE @EndDate VARCHAR(100)

	SELECT @EndDate = [EndDate]
	FROM [dbo].[Subscription]

	SELECT DATEDIFF(day, @EndDate, getdate()) AS ENDSUBSCRIPTION
END



GO
/****** Object:  StoredProcedure [dbo].[SupportUserAccessControlDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================  
--Auther :Priyanka  
--Date :10-2-17  
--Purpose UserCommission Delete   
--==========================================  
CREATE PROCEDURE [dbo].[SupportUserAccessControlDelete] @UserId INT
AS
BEGIN
	DELETE
	FROM SupportUserAccessControl
	WHERE UserID = @UserId
END



GO
/****** Object:  StoredProcedure [dbo].[SupportUserAccessControlUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================    
--Auther :Priyanka    
--Date :24-2-17    
--Purpose User Commission Update    
--==========================================    
CREATE PROCEDURE[dbo].[SupportUserAccessControlUpdate] @UserID INT
	,@PageID INT
	,@PageName VARCHAR(250)
	,@IsOn INT
AS
BEGIN
	IF NOT EXISTS (
			SELECT [PageID]
			FROM dbo.SupportUserAccessControl
			WHERE [PageID] = @PageID
				AND [UserID] = @UserID
			)
	BEGIN
		INSERT INTO dbo.SupportUserAccessControl (
			[PageID]
			,[PageName]
			,[IsOn]
			,[UserID]
			)
		VALUES (
			@PageID
			,@PageName
			,@IsOn
			,@UserID
			)
	END
END



GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                                          
-- Author:  <Sainath Bhujbal>                                          
-- ALTER  date: <11 Feb 2017 ,>                                          
-- Description: <Procedure for Mobile Recharge as well as ElectricityBill Insert in Transaction detail,,>                                          
-- =============================================                                     
-- [dbo].TransactionDetailsInsert 3, 81, 1,'7588215033',10,7,1                          
CREATE PROCEDURE [dbo].[TransactionDetailsInsert] @UserID INT
	,@RechargeID INT
	,@ServiceID INT
	,@ConsumerNo VARCHAR(20)
	,@TransactionAmount NUMERIC(18, 5)
	,@OperatorID INT
	,@RechargeProviderID INT
AS
BEGIN
	DECLARE @afterTransAmt NUMERIC(18, 5);
	DECLARE @beforeTransAmt NUMERIC(18, 5);
	DECLARE @currentBalance NUMERIC(18, 5);
	DECLARE @TransDesc VARCHAR(200);
	--------------------------------FOR USER Account-------------------------------------------------------                            
	DECLARE @comAmt NUMERIC(18, 5)
	DECLARE @userType VARCHAR(20)
	DECLARE @retailer VARCHAR(20) = 'RETAILER';
	DECLARE @freeUser VARCHAR(20) = 'CUSTOMER';--'FREE USER';                        
	DECLARE @distributor VARCHAR(20) = 'DISTRIBUTOR';
	DECLARE @superdistributor VARCHAR(20) = 'SUPERDISTRIBUTOR';
	DECLARE @admin VARCHAR(20) = 'ADMIN';
	DECLARE @apiUser VARCHAR(20) = 'APIUSER';
	DECLARE @electricityServiceID INT = 14;
	DECLARE @DMRServiceID INT = 21;

	SELECT @userType = UTM.UserType
	FROM [dbo].UserInformation(NOLOCK) UI
	INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @USERID

	IF (
			Upper(@userType) = Upper(@retailer)
			OR Upper(@userType) = Upper(@apiUser)
			)
	BEGIN
		EXECUTE [dbo].RechargeSelectRetailerCommission @UserID = @UserID
			,@OperatorID = @OperatorID
			,@TransactionAmount = @TransactionAmount
			,@comAmt = @comAmt OUTPUT
	END
	ELSE IF (Upper(@userType) = Upper(@freeUser))
	BEGIN
		SET @comAmt = 0;
	END
	ELSE
	BEGIN
		RETURN;
	END

	PRINT 'if Default commission Amt:' + convert(VARCHAR(30), @comAmt)

	DECLARE @comCreditDebit VARCHAR(20)

	IF @comAmt = 0
	BEGIN
		SET @comCreditDebit = ''
	END
	ELSE
	BEGIN
		SET @comCreditDebit = 'CREDIT'
	END

	IF (@ServiceID = @electricityServiceID)
	BEGIN
		SELECT @beforeTransAmt = DMRBalance
		FROM dbo.UserBalance
		WHERE UserID = @USERID

		UPDATE [DBO].UserBalance
		SET DMRBalance = ((DMRBalance - @TransactionAmount) + @comAmt)
		WHERE UserID = @USERID

		SELECT @afterTransAmt = DMRBalance
		FROM dbo.UserBalance
		WHERE UserID = @USERID
	END
	ELSE
	BEGIN
		SELECT @beforeTransAmt = CurrentBalance
		FROM dbo.UserBalance
		WHERE UserID = @USERID

		UPDATE [DBO].UserBalance
		SET CurrentBalance = ((CurrentBalance - @TransactionAmount) + @comAmt)
		WHERE UserID = @USERID

		SELECT @afterTransAmt = CurrentBalance
		FROM dbo.UserBalance
		WHERE UserID = @USERID
	END

	PRINT '@CurrentBalance:' + convert(VARCHAR(30), @beforeTransAmt)
	PRINT '@AfterBalance:' + convert(VARCHAR(30), @afterTransAmt)

	-- For ElectricityBill Doctype            
	DECLARE @docType VARCHAR(20);

	IF (@ServiceID = @electricityServiceID)
	BEGIN
		SET @docType = 'ElectricityBill'
	END
	ELSE IF (@ServiceID = @DMRServiceID)
	BEGIN
		SET @docType = 'MoneyTransfer'
	END
	ELSE
	BEGIN
		SET @docType = 'RECHARGE'
	END

	SET @TransDesc = 'DEBIT ' + @docType + ' Of Rs. ' + CONVERT(VARCHAR, CONVERT(DECIMAL(18, 2), @TransactionAmount)) + '  For Cust. No. ' + @ConsumerNo

	PRINT '@TransDesc:' + convert(VARCHAR(30), @TransDesc)

	EXEC dbo.TransactionDetailsInsertByUser @UserID --userID                          
		,@RechargeID --@RechargeID             
		,@ServiceID
		,'0' --                      
		,@ConsumerNo
		,@docType
		,
		--'RECHARGE',                         
		@beforeTransAmt
		,@afterTransAmt
		,@TransactionAmount
		,@comAmt
		,'DEBIT'
		,@comCreditDebit
		,@TransDesc

	--------------------------------FOR Upline Account-------------------------------------------------------                       
	DECLARE @dcomAmt NUMERIC(18, 5)
	--set @dcomAmt=0                       
	DECLARE @RetailerName VARCHAR(50);
	DECLARE @ParentID INT = 0;

	BEGIN TRY
		SELECT @ParentID = ISNULL(ParentID, 0)
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @UserID;

		PRINT '@ParentID:' + convert(VARCHAR(30), @ParentID)

		IF (
				@ParentID = 1
				OR @ParentID = 0
				)
		BEGIN
			RETURN;
		END

		SELECT @RetailerName = name
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @UserID;

		PRINT '@RetailerName:' + convert(VARCHAR(30), @RetailerName)
	END TRY

	BEGIN CATCH
		RETURN
	END CATCH

	-- distributor          
	IF (@ParentID != 0)
	BEGIN
		EXEC SelectUplineCommission @UserID
			,@RechargeID
			,@ServiceID
			,@ConsumerNo
			,@TransactionAmount
			,@OperatorID
			,@ParentID
			,@RetailerName
			,@docType
	END

	-- super dist               
	DECLARE @ParentOfParent_id INT = 0

	SELECT @ParentOfParent_id = ISNULL(ParentID, 0)
	FROM DBO.UserInformation(NOLOCK)
	WHERE UserID = @ParentID;

	IF (
			@ParentOfParent_id = 1
			OR @ParentOfParent_id = 0
			)
	BEGIN
		RETURN;
	END

	IF (
			@ParentOfParent_id != 0
			AND @ParentOfParent_id != 1
			)
	BEGIN
		-- For Super Distributor Upline Commission              
		EXEC SelectUplineCommission @UserID
			,@RechargeID
			,@ServiceID
			,@ConsumerNo
			,@TransactionAmount
			,@OperatorID
			,@ParentOfParent_id
			,@RetailerName
			,@docType
	END

	DECLARE @ROWCNT INT;

	SET @ROWCNT = @@ROWCOUNT

	SELECT @ROWCNT
END

GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsInsertByUser]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                        
-- Author:  <Sainath Bhujbal>                        
-- ALTER  date: <11 Feb 2017 ,>                        
-- Description: <Description,,>                        
-- =============================================                   
CREATE PROCEDURE [dbo].[TransactionDetailsInsertByUser] (  
 @UserID INT  
 ,@RechargeID INT  
 ,@serviceID INT  
 ,@AssignedUserID INT = 0  
 ,@ConsumerNumber VARCHAR(20)  
 ,@DOCType VARCHAR(50)  
 ,@OpeningBalance NUMERIC(18, 5)  
 ,@ClosingBalance NUMERIC(18, 5)  
 ,@TransactionAmount NUMERIC(18, 5)  
 ,@DiscountAmount NUMERIC(18, 5)  
 ,@AmountCreditDebit VARCHAR(10)  
 ,@CommissionCreditDebit VARCHAR(10)  
 ,@TransactionDescription VARCHAR(200)  
 ,@GST Decimal(18,5) = 0
 ,@TDS Decimal(18,5) = 0
 )  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 DECLARE @ROWCNT INT;  
  
 INSERT INTO [dbo].TransactionDetails (  
  UserID  
  ,RechargeID  
  ,serviceID  
  ,AssignedUserID  
  ,ConsumerNumber  
  ,DOCType  
  ,OpeningBalance  
  ,ClosingBalance  
  ,TransactionAmount  
  ,DiscountAmount  
  ,AmountCreditDebit  
  ,CommissionCreditDebit  
  ,TransactionDescription 
  ,GST
  ,TDS 
  )  
 VALUES (  
  @UserID  
  ,@RechargeID  
  ,@serviceID  
  ,@AssignedUserID  
  ,@ConsumerNumber  
  ,@DOCType  
  ,@OpeningBalance  
  ,@ClosingBalance  
  ,@TransactionAmount  
  ,@DiscountAmount  
  ,@AmountCreditDebit  
  ,@CommissionCreditDebit  
  ,@TransactionDescription  
  ,@GST
  ,@TDS
  )  
  
 SET @ROWCNT = @@ROWCOUNT;  
  
 SELECT @ROWCNT  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsInsertByUserRefundManually]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                      
-- Author:  <Sainath Bhujbal>                      
-- ALTER  date: <11 Feb 2017 ,>                      
-- Description: <Description,,>                      
-- =============================================       
---select * from UserInformation
---@UserID,@BeforeAmount, @AfterTransAmt,@TransDesc, @CustomerNumber,@Amount          
CREATE PROCEDURE [dbo].[TransactionDetailsInsertByUserRefundManually] @UserID INT
	,@BeforeTransAmt NUMERIC(18, 5)
	,@AfterTransAmt NUMERIC(18, 5)
	,@TransDesc VARCHAR(200)
	,@CustomerNumber VARCHAR(200)
	,@Amount DECIMAL(18, 5)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ROWCNT INT;
	DECLARE @RechargeID INT = 0;
	DECLARE @ServiceID INT = 0;
	DECLARE @AssignedUserID INT = 0;

	BEGIN
		INSERT INTO [dbo].TransactionDetails (
			UserID
			,RechargeID
			,serviceID
			,AssignedUserID
			,ConsumerNumber
			,DOCType
			,OpeningBalance
			,ClosingBalance
			,TransactionAmount
			,DiscountAmount
			,AmountCreditDebit
			,CommissionCreditDebit
			,TransactionDescription
			)
		VALUES (
			@UserID
			,0
			,1
			,0
			,@CustomerNumber
			,'REFUNDED'
			,@BeforeTransAmt
			,@AfterTransAmt
			,@Amount
			,0
			,'CREDIT'
			,''
			,@TransDesc
			)

		SET @ROWCNT = @@ROWCOUNT;
	END

	SELECT @ROWCNT
END



GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsInsertForSystemAccount]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                  
-- Author:  <Sainath Bhujbal>                  
-- ALTER  date: <10 MARCH 2017 ,>                  
-- Description: <Description,,>                  
-- =============================================             
CREATE PROCEDURE [dbo].[TransactionDetailsInsertForSystemAccount] (
	@UserID INT
	,@RechargeID INT
	,@ServiceID INT
	,@ConsumerNo VARCHAR(20)
	,@TransactionAmount NUMERIC(18, 5)
	,@OperatorID INT
	,@RechargeProviderID INT
	)
AS
BEGIN
	DECLARE @afterTransAmt NUMERIC(18, 5);
	DECLARE @beforeTransAmt NUMERIC(18, 5);
	DECLARE @currentBalance NUMERIC(18, 5);
	DECLARE @TransDesc VARCHAR(200)
	--------------------------------FOR System Account-------------------------------------------------------    
	----For System Account    
	DECLARE @SystemAccount INT = 60;
	DECLARE @providerComm NUMERIC(18, 5) = 0;
	DECLARE @providerCommType VARCHAR(10) = 'P';
	DECLARE @Uname VARCHAR(50)

	SELECT @Uname = name
	FROM DBO.UserInformation(NOLOCK)
	WHERE UserID = @UserID;

	SELECT @providerComm = ISNULL(CommissionAdmin, 0)
		,@providerCommType = Upper(MarginType)
	FROM dbo.RechargeProviderOperatorMaster
	WHERE RechargeProviderID = @RechargeProviderID
		AND OperatorID = @OperatorId

	IF @providerCommType = 'P'
	BEGIN
		SET @providerComm = (@providerComm * @TransactionAmount) / 100;
	END
	ELSE
	BEGIN
		SET @providerComm = @providerComm;
	END

	SELECT @beforeTransAmt = CurrentBalance
	FROM DBO.UserBalance(NOLOCK)
	WHERE UserID = @SystemAccount

	UPDATE dbo.UserBalance
	SET CurrentBalance = (CurrentBalance + @providerComm)
	WHERE UserID = @SystemAccount

	SELECT @afterTransAmt = @beforeTransAmt + @providerComm;

	SET @TransDesc = 'Provider Commision Amount:' + CONVERT(VARCHAR, @providerComm) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;

	EXEC dbo.TransactionDetailsInsertByUser @SystemAccount
		,@RechargeID
		,@ServiceID
		,@UserID
		,@ConsumerNo
		,'RECHARGE'
		,@beforeTransAmt
		,@afterTransAmt
		,0 --@TransactionAmount                           
		,@providerComm
		,'CREDIT'
		,'CREDIT'
		,@TransDesc
END



GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsInsertMoneyTransfer]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
--modifay:Rahul Hembade              
--Date:21 may 2018              
--Reson:use DMR Balance for many transfer              
--============================================        
--     
CREATE PROCEDURE [dbo].[TransactionDetailsInsertMoneyTransfer] @UserID INT    
 ,@RechargeID INT    
 ,@ServiceID INT    
 ,@ConsumerNo VARCHAR(20)    
 ,@TransactionAmount NUMERIC(18, 5)    
 ,@OperatorID INT    
 ,@RechargeProviderID INT    
AS    
BEGIN    
 DECLARE @afterTransAmt NUMERIC(18, 5);    
 DECLARE @beforeTransAmt NUMERIC(18, 5);    
 DECLARE @currentBalance NUMERIC(18, 5);    
 DECLARE @TransDesc VARCHAR(200);    
 --------------------------------FOR USER Account-------------------------------------------------------                              
 DECLARE @mType VARCHAR(50)    
 DECLARE @comAmt NUMERIC(18, 5)    
 DECLARE @userTypeID INT    
 DECLARE @distributorTypeID INT = 2;    
 DECLARE @retailerTypeID INT = 3;    
 DECLARE @apiUserTypeID INT = 4;    
 DECLARE @customerTypeID INT = 6;    
 DECLARE @superdistributorTypeID INT = 8;    
 DECLARE @electricityServiceID INT = 14;    
 DECLARE @DMRServiceID INT = 21;    
 DECLARE @BenificryVerificationOperatorID INT = 45;    
 DECLARE @MoneyTransferOperatorID INT = 44;    
    
 SELECT @userTypeID = UI.UserTypeID    
 FROM [dbo].UserInformation(NOLOCK) UI    
 WHERE UI.UserID = @USERID    
    
 DECLARE @RetailerCommAmt DECIMAL(18, 5)    
 DECLARE @DistributorCommAmt DECIMAL(18, 5)    
 DECLARE @SuperDistributorCommAmt DECIMAL(18, 5)    
    
 EXECUTE [dbo].SlabCommissionSelectByUser @USERID = @USERID    
  ,@TransactionAmount = @TransactionAmount    
  ,@RetailerCommAmt = @RetailerCommAmt OUTPUT    
  ,@DistributorCommAmt = @DistributorCommAmt OUTPUT    
  ,@SuperDistributorCommAmt = @SuperDistributorCommAmt OUTPUT    
    
 SET @comAmt = @RetailerCommAmt;    
    
 DECLARE @comCreditDebit VARCHAR(20)    
    
 IF @comAmt = 0    
 BEGIN    
  SET @comCreditDebit = ''    
 END    
 ELSE    
 BEGIN    
  SET @comCreditDebit = 'DEBIT'    
 END    
    
  Declare @GST decimal(18,5);  
  Declare @TDS decimal(18,5);  
  EXECUTE [dbo].[GSTTDSSelect] @Amount = @TransactionAmount    
  ,@Commission = @comAmt    
  ,@ServiceID = @ServiceID  
  ,@GST = @GST OUTPUT    
  ,@TDS = @TDS OUTPUT    
  
  IF(@GST is NULL)  
  BEGIN  
 SET @GST = 0  
  END  
  
   IF(@TDS is NULL)  
  BEGIN  
 SET @TDS = 0  
  END  
  
  IF(@ServiceID = @DMRServiceID )  
  BEGIN  
 update MoneyTransfer  
 Set GST = @GST,  
 TDS = @TDS  
 Where MoneyTransferID = @RechargeID  
  END  
  
 --print '@comAmt' + convert(varchar(30),@comAmt) + ':' +   @mType                              
 SELECT @beforeTransAmt = DMRBalance    
 FROM dbo.UserBalance    
 WHERE UserID = @USERID    
    
 SELECT @afterTransAmt = (@beforeTransAmt - @TransactionAmount) + @comAmt - (@GST + @TDS)   
    
 --print '@@beforeTransAmt' + convert(varchar(30),@beforeTransAmt)                                 
 --print '@afterTransAmt' + convert(varchar(30),@afterTransAmt)                         
 UPDATE [DBO].UserBalance    
 SET DMRBalance = @afterTransAmt    
 WHERE UserID = @USERID    
    
 -- For ElectricityBill Doctype              
 DECLARE @docType VARCHAR(20);    
    
 SET @docType = 'MoneyTransfer'    
 SET @TransDesc = 'DEBIT ' + @docType + ' Of Rs. ' + CONVERT(VARCHAR, CONVERT(DECIMAL(18, 2), @TransactionAmount)) + ' For Acc. No. ' + @ConsumerNo    
    
 EXEC dbo.TransactionDetailsInsertByUser @UserID --userID                            
  ,@RechargeID --@RechargeID                           
  ,@ServiceID    
  ,'0' --                        
  ,@ConsumerNo    
  ,@docType    
  ,    
  --'RECHARGE',                           
  @beforeTransAmt    
  ,@afterTransAmt    
  ,@TransactionAmount    
  ,@comAmt    
  ,'DEBIT'    
  ,@comCreditDebit    
  ,@TransDesc    
  ,@GST  
  ,@TDS  
    
  SET @GST = 0  
  SET @TDS = 0  
  
 --------------------------------FOR Upline Account -------------------------------------------------------                
 ---- 1     DISTRIBUTOR ----------------------------------                   
 DECLARE @RetailerName VARCHAR(50);    
    
 SELECT @RetailerName = name    
 FROM DBO.UserInformation(NOLOCK)    
 WHERE UserID = @UserID;    
    
 DECLARE @ParentID INT = 0;    
    
 SELECT @ParentID = ISNULL(ParentID, 0)    
 FROM DBO.UserInformation(NOLOCK)    
 WHERE UserID = @UserID;    
    
 EXEC dbo.[TransactionDetailsInsertUplineMoneyTransfer] @UserID --  @RetailerID               
  ,@RetailerName    
  ,@ParentID --@UserID                
  ,@RechargeID    
  ,@ServiceID    
  ,@ConsumerNo    
  ,@TransactionAmount    
  ,@OperatorID    
  ,@RechargeProviderID    
  ,@docType    
    
 ---- 2    SUPER DISTRIBUTOR ----------------------------------                 
 DECLARE @SuperParentID INT = 0;    
 DECLARE @SuperParentTypeID INT = 0;    
    
 SELECT @SuperParentID = ISNULL(ParentID, 0)    
  ,@SuperParentTypeID = ISNULL([UserTypeID], 0)    
 FROM DBO.UserInformation(NOLOCK)    
 WHERE UserID = @ParentID;    
    
 DECLARE @AdminTypeID INT = 1;    
    
 IF (@SuperParentID != @AdminTypeID)    
 BEGIN    
  EXEC dbo.[TransactionDetailsInsertUplineMoneyTransfer] @UserID --  @RetailerID               
   ,@RetailerName    
   ,@SuperParentID --@UserID                
   ,@RechargeID    
   ,@ServiceID    
   ,@ConsumerNo    
   ,@TransactionAmount    
   ,@OperatorID    
   ,@RechargeProviderID    
   ,@docType    
 END    
    
 DECLARE @ROWCNT INT;    
    
 SET @ROWCNT = @@ROWCOUNT    
    
 SELECT @ROWCNT    
END    
    
GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsInsertUpline]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================                              
-- Author:  <Priyanka>                              
-- ALTER  date: <02 Sept 2017 ,>                              
-- Description: <Procedure for Insert data for Upline in transction_dtl >                              
-- =============================================   
-- [dbo].[TransactionDetailsInsertUpline] 3, 81, 1,'7588215033',10,7,1              
CREATE PROCEDURE [dbo].[TransactionDetailsInsertUpline] @RetailerID INT
	,@RetailerName VARCHAR(100)
	,@UserID INT
	,@RechargeID INT
	,@ServiceID INT
	,@ConsumerNo VARCHAR(20)
	,@TransactionAmount NUMERIC(18, 5)
	,@OperatorID INT
	,@RechargeProviderID INT
	,@docType VARCHAR(20)
AS
BEGIN
	--declare @distributor varchar(20) = 'DISTRIBUTOR';
	--declare @superdistributor varchar(20) = 'SuperDistributor';
	--declare @admin varchar(20) = 'ADMIN';
	DECLARE @adminTypeID INT = 1;
	DECLARE @distributorTypeID INT = 2;
	DECLARE @superdistributorTypeID INT = 7;
	DECLARE @ParentID INT = 0;
	DECLARE @userTypeID INT;
	DECLARE @dcomAmt NUMERIC(18, 5);
	DECLARE @mType VARCHAR(50);
	DECLARE @TransDesc VARCHAR(200);

	SET @ParentID = @UserID

	IF @ParentID != 0
	BEGIN
		SELECT @userTypeID = UI.UserTypeID
		FROM [dbo].UserInformation(NOLOCK) UI
		WHERE UI.UserID = @ParentID

		IF (
				@userTypeID = @distributorTypeID
				OR @userTypeID = @adminTypeID
				)
		BEGIN
			BEGIN TRY
				SELECT @dcomAmt = DistributorCommissionAmount
					,@mType = Upper(MarginType)
				FROM dbo.UserCommission(NOLOCK)
				WHERE OperatorID = @OperatorID
					AND UserID = @RetailerID

				PRINT '@dcomAmt' + convert(VARCHAR(30), @dcomAmt) + ':' + @mType

				IF (@dcomAmt IS NULL) --Default Comm            
				BEGIN
					SELECT @dcomAmt = ISNULL(DistributorCommissionAmount, 0)
						,@mType = Upper(MarginType)
					FROM DBO.OperatorMaster(NOLOCK)
					WHERE OperatorID = @OperatorID

					PRINT '@dcomAmt' + convert(VARCHAR(30), @dcomAmt) + ':' + @mType
				END

				SET @TransDesc = 'Commision Amount:' + CONVERT(VARCHAR, @dcomAmt) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @RetailerName;
			END TRY

			BEGIN CATCH
				--return
			END CATCH
		END
		ELSE IF (@userTypeID = @superdistributorTypeID)
		BEGIN
			BEGIN TRY
				SELECT @dcomAmt = SuperDistributorCommissionAmount
					,@mType = Upper(MarginType)
				FROM dbo.UserCommission(NOLOCK)
				WHERE OperatorID = @OperatorID
					AND UserID = @RetailerID

				PRINT '@dcomAmt' + convert(VARCHAR(30), @dcomAmt) + ':' + @mType

				IF (@dcomAmt IS NULL) --Default Comm            
				BEGIN
					SELECT @dcomAmt = ISNULL(SuperDistributorCommissionAmount, 0)
						,@mType = Upper(MarginType)
					FROM DBO.OperatorMaster(NOLOCK)
					WHERE OperatorID = @OperatorID

					PRINT '@dcomAmt' + convert(VARCHAR(30), @dcomAmt) + ':' + @mType
				END

				DECLARE @DistributorName VARCHAR(50);

				SELECT @DistributorName = name
				FROM DBO.UserInformation(NOLOCK)
				WHERE UserID = @UserID;

				SET @TransDesc = 'Commision Amount:' + CONVERT(VARCHAR, @dcomAmt) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @RetailerName + ', Through ' + @DistributorName;
			END TRY

			BEGIN CATCH
				--	return
			END CATCH
		END

		DECLARE @afterTransAmt NUMERIC(18, 5);
		DECLARE @beforeTransAmt NUMERIC(18, 5);
		DECLARE @currentBalance NUMERIC(18, 5);

		IF @mType = 'P'
		BEGIN
			SET @dcomAmt = (@dcomAmt * @TransactionAmount) / 100;
		END
		ELSE
		BEGIN
			SET @dcomAmt = @dcomAmt;
		END

		SET @beforeTransAmt = 0;

		SELECT @beforeTransAmt = CurrentBalance
		FROM dbo.UserBalance
		WHERE UserID = @ParentID

		SET @afterTransAmt = 0;

		SELECT @afterTransAmt = (@beforeTransAmt + @dcomAmt)

		UPDATE [DBO].UserBalance
		SET CurrentBalance = @afterTransAmt
		WHERE UserID = @ParentID

		--UPLINE COMM AND BALANCE                    
		EXEC dbo.TransactionDetailsInsertByUser @ParentID
			,@RechargeID
			,@ServiceID
			,@UserID
			,@ConsumerNo
			,@docType
			,
			--'RECHARGE',             
			@beforeTransAmt
			,@afterTransAmt
			,0 --@TransactionAmount                                       
			,@dcomAmt
			,''
			,'CREDIT'
			,@TransDesc
	END
END



GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsInsertUplineMoneyTransfer]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modifay:Rahul Hembade        
--Date:21 may 2018        
--Reson:use DMR Balance for many transfer        
--============================================               
CREATE PROCEDURE [dbo].[TransactionDetailsInsertUplineMoneyTransfer] @RetailerID INT    
 ,@RetailerName VARCHAR(100)    
 ,@UserID INT    
 ,@RechargeID INT    
 ,@ServiceID INT    
 ,@ConsumerNo VARCHAR(20)    
 ,@TransactionAmount NUMERIC(18, 5)    
 ,@OperatorID INT    
 ,@RechargeProviderID INT    
 ,@docType VARCHAR(20)    
AS    
BEGIN    
 DECLARE @adminTypeID INT = 1;    
 DECLARE @distributorTypeID INT = 2;    
 DECLARE @superdistributorTypeID INT = 8;    
 DECLARE @ParentID INT = 0;    
 DECLARE @userTypeID INT;    
 DECLARE @dcomAmt NUMERIC(18, 5);    
 DECLARE @mType VARCHAR(50);    
 DECLARE @TransDesc VARCHAR(200);    
 DECLARE @RetailerCommAmt DECIMAL(18, 5);    
 DECLARE @DistributorCommAmt DECIMAL(18, 5);    
 DECLARE @SuperDistributorCommAmt DECIMAL(18, 5);    
    
 SET @ParentID = @UserID    
    
 IF @ParentID != 0    
 BEGIN    
  SELECT @userTypeID = UI.UserTypeID    
  FROM [dbo].UserInformation(NOLOCK) UI    
  WHERE UI.UserID = @ParentID    
    
  IF (    
    @userTypeID = @distributorTypeID    
    OR @userTypeID = @adminTypeID    
    )    
  BEGIN    
   BEGIN TRY    
    EXECUTE [dbo].SlabCommissionSelectByUser @USERID = @RetailerID    
     ,@TransactionAmount = @TransactionAmount    
     ,@RetailerCommAmt = @RetailerCommAmt OUTPUT    
     ,@DistributorCommAmt = @DistributorCommAmt OUTPUT    
     ,@SuperDistributorCommAmt = @SuperDistributorCommAmt OUTPUT    
    
    SET @dcomAmt = @DistributorCommAmt    
    SET @TransDesc = 'Commision Amount:' + CONVERT(VARCHAR, @dcomAmt) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @RetailerName;    
   END TRY    
    
   BEGIN CATCH    
    --return        
   END CATCH    
  END    
  ELSE IF (@userTypeID = @superdistributorTypeID)    
  BEGIN    
   BEGIN TRY    
    EXECUTE [dbo].SlabCommissionSelectByUser @USERID = @RetailerID    
     ,@TransactionAmount = @TransactionAmount    
     ,@RetailerCommAmt = @RetailerCommAmt OUTPUT    
     ,@DistributorCommAmt = @DistributorCommAmt OUTPUT    
     ,@SuperDistributorCommAmt = @SuperDistributorCommAmt OUTPUT    
    
    SET @dcomAmt = @SuperDistributorCommAmt    
    
    DECLARE @DistributorName VARCHAR(50);    
    
    SELECT @DistributorName = name    
    FROM DBO.UserInformation(NOLOCK)    
    WHERE UserID = @UserID;    
    
    SET @TransDesc = 'Commision Amount:' + CONVERT(VARCHAR, @dcomAmt) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @RetailerName + ', Through ' + @DistributorName;    
   END TRY    
    
   BEGIN CATCH    
    -- return        
   END CATCH    
  END    
    
  DECLARE @afterTransAmt NUMERIC(18, 5);    
  DECLARE @beforeTransAmt NUMERIC(18, 5);    
  DECLARE @currentBalance NUMERIC(18, 5);    
    
  SET @dcomAmt = @dcomAmt;    
  SET @beforeTransAmt = 0;    
    
  SELECT @beforeTransAmt = DMRBalance    
  FROM dbo.UserBalance    
  WHERE UserID = @ParentID    
    
  SET @afterTransAmt = 0;    
    
  SELECT @afterTransAmt = (@beforeTransAmt + @dcomAmt)    
    
  UPDATE [DBO].UserBalance    
  SET DMRBalance = @afterTransAmt    
  WHERE UserID = @ParentID    
    
  --UPLINE COMM AND BALANCE                            
  EXEC dbo.TransactionDetailsInsertByUser @ParentID    
   ,@RechargeID    
   ,@ServiceID    
   ,@UserID    
   ,@ConsumerNo    
   ,@docType    
   ,    
   --'RECHARGE',                     
   @beforeTransAmt    
   ,@afterTransAmt    
   ,0 --@TransactionAmount                                               
   ,@dcomAmt    
   ,''    
   ,'CREDIT'    
   ,@TransDesc    
 END    
END    
  
GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsMoneyTranferRefundInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by:Rahul Hembade        
--Date: 21 May 2018        
--Reson: Money Transfer Refund for User         
--=============================================        
--[TransactionDetailsRefundInsert] 3,97,21,'20356390959',3.00000,44,8        
CREATE PROCEDURE [dbo].[TransactionDetailsMoneyTranferRefundInsert] @UserID INT
	,@RechargeID INT
	,@ServiceID INT
	,@ConsumerNo VARCHAR(20)
	,@TransactionAmount NUMERIC(18, 5)
	,@OperatorID INT
	,@RechargeProviderID INT
AS
BEGIN
	DECLARE @beforeTransAmt NUMERIC(18, 5);
	DECLARE @currentBalance NUMERIC(18, 5);
	DECLARE @TransDesc VARCHAR(200);
	--------------------------------FOR USER Account-------------------------------------------------------                    
	DECLARE @mType VARCHAR(50)
	DECLARE @comAmt NUMERIC(18, 5)
	DECLARE @GST NUMERIC(18, 5)
	DECLARE @TDS NUMERIC(18, 5)
	DECLARE @userTypeID INT
	DECLARE @retailerTypeID INT = 3;
	DECLARE @apiUserTypeID INT = 4;
	DECLARE @Uname VARCHAR(50)
	DECLARE @electricityServiceID INT = 14;
	DECLARE @MoneyTransfer INT = 21;
	DECLARE @DOCType VARCHAR(50)
	DECLARE @ServiceType VARCHAR(50)

	SET @DOCType = 'REFUND'

	IF (@ServiceID = @electricityServiceID)
	BEGIN
		SELECT @ServiceType = 'ElectricityBill'
	END
	ELSE IF (@ServiceID = @MoneyTransfer)
	BEGIN
		SELECT @ServiceType = 'MoneyTransfer'
	END
	ELSE
	BEGIN
		SELECT @ServiceType = 'RECHARGE'
	END

	SELECT @comAmt = DiscountAmount
		,@GST = GST
		,@TDS = TDS
	FROM dbo.TransactionDetails(NOLOCK)
	WHERE RechargeID = @RechargeId
		AND DOCType = @ServiceType
		AND AmountCreditDebit = 'DEBIT'
		AND UserID = @UserID

	--PRINT '@@comAmt' + convert(VARCHAR(30), @beforeTransAmt)        
	SELECT @beforeTransAmt = DMRBalance
	FROM dbo.UserBalance
	WHERE UserID = @UserID

	PRINT '@@beforeTransAmt' + convert(VARCHAR(30), @beforeTransAmt)
	PRINT '@@beforeTransAmt' + convert(VARCHAR(30), @TransactionAmount)
	PRINT @comAmt
	PRINT @TransactionAmount

	DECLARE @afterTransAmt NUMERIC(18, 5);

	SET @afterTransAmt = (@beforeTransAmt + @TransactionAmount) - (@comAmt) + (@GST + @TDS);

	UPDATE [DBO].UserBalance
	SET DMRBalance = @afterTransAmt
	WHERE UserID = @UserID

	PRINT '@@afterTransAmt' + convert(VARCHAR(30), @afterTransAmt)

	SELECT @Uname = name
	FROM DBO.UserInformation(NOLOCK)
	WHERE UserID = @UserID;

	SET @TransDesc = 'CREDIT ' + @ServiceType + ' REFUNDED Of Rs.' + CONVERT(VARCHAR, CONVERT(DECIMAL(18, 2), @TransactionAmount)) + ' For Consumer No. ' + @ConsumerNo + ' SERVICE ID.' + Convert(VARCHAR, @ServiceID)

	IF (@ServiceID = @MoneyTransfer)
	BEGIN
		SET @DOCType = 'REFUND DMT'
	END

	EXEC dbo.TransactionDetailsInsertByUser @UserID
		,@RechargeID
		,@ServiceID
		,'0'
		,@ConsumerNo
		,@DOCType
		,@beforeTransAmt
		,@afterTransAmt
		,@TransactionAmount
		,@comAmt
		,'CREDIT'
		,'CREDIT'
		,@TransDesc
		,@GST
		,@TDS

	--------------------------------FOR Upline Account-------------------------------------------------------                    
	BEGIN TRY
		DECLARE @dcomAmt NUMERIC(18, 5)
		DECLARE @CurrentParentID INT = 0;

		SELECT @CurrentParentID = ISNULL(ParentID, 0)
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @UserID;

		SELECT @Uname = name
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @UserID;
	END TRY

	BEGIN CATCH
		RETURN
	END CATCH

	EXEC dbo.[TransactionDetailsMoneyTransferRefundUplineInsert] @CurrentParentID
		,@Uname
		,@ServiceID
		,@RechargeId
		,@dcomAmt
		,@electricityServiceID
		,@UserID
		,@ConsumerNo

	----------------SuperDistributor-----        
	BEGIN TRY
		DECLARE @CurrentSDParentID INT = 0;
		DECLARE @CurrentSDParentTypeID INT = 0;
		DECLARE @SDUname VARCHAR(300);

		SELECT @CurrentSDParentID = ISNULL(ParentID, 0)
			,@CurrentSDParentTypeID = ISNULL([UserTypeID], 0)
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @CurrentParentID;

		SELECT @Uname = name
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @UserID;
	END TRY

	BEGIN CATCH
		RETURN
	END CATCH

	DECLARE @AdminTypeID INT = 1;

	IF (@CurrentSDParentID != @AdminTypeID)
	BEGIN
		EXEC dbo.[TransactionDetailsRefundUplineInsert] @CurrentSDParentID
			,---SD        
			@Uname
			,@ServiceID
			,@RechargeId
			,@dcomAmt
			,@electricityServiceID
			,@UserID
			,---Retailer        
			@ConsumerNo
	END

	----------------------------------SYSTEM ACCOUNT---------------------------------------------                  
	DECLARE @SystemAccount INT = 60;
	DECLARE @providerComm NUMERIC(18, 5) = 0;

	SELECT @providerComm = isnull(DiscountAmount, 0)
	FROM [dbo].TransactionDetails
	WHERE RechargeID = @RechargeId
		AND UserID = @SystemAccount
		AND DOCType = @ServiceType

	PRINT '@dcomAmt' + convert(VARCHAR(30), @providerComm) + ':'

	IF (@providerComm != 0)
	BEGIN
		SELECT @beforeTransAmt = DMRBalance
		FROM DBO.UserBalance(NOLOCK)
		WHERE UserID = @SystemAccount

		UPDATE dbo.UserBalance
		SET DMRBalance = (CurrentBalance - @providerComm)
		WHERE UserID = @SystemAccount

		SELECT @afterTransAmt = @beforeTransAmt - @providerComm;

		SET @TransDesc = 'DEBIT Reversed Provider Commision Of Rs.' + CONVERT(VARCHAR, CONVERT(DECIMAL(18, 5), @providerComm)) + 'For ServiceID ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;
		SET @DOCType = 'REVERSE'

		IF (@ServiceID = @MoneyTransfer)
		BEGIN
			SET @DOCType = 'REVERSE DMT'
		END

		EXEC dbo.TransactionDetailsInsertByUser @SystemAccount
			,@RechargeID
			,@ServiceID
			,@UserID
			,@ConsumerNo
			,@DOCType
			,@beforeTransAmt
			,@afterTransAmt
			,0 --@TransactionAmount                                           
			,@providerComm
			,'DEBIT'
			,'DEBIT'
			,@TransDesc
	END

	DECLARE @ROWCNT INT;

	SET @ROWCNT = @@ROWCOUNT

	SELECT @ROWCNT
END

GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsMoneyTransferRefundUplineInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
--Created by:Rahul Hembade  
--Date: 21 May 2018  
--Reson: Money Transfer Refund for Upline   
--=============================================  
CREATE procedure [dbo].[TransactionDetailsMoneyTransferRefundUplineInsert] @CurrentParentID int  
 ,@Uname varchar(100)  
 ,@ServiceID int  
 ,@RechargeId int  
 ,@dcomAmt int  
 ,@electricityServiceID int  
 ,@UserID int  
 ,@ConsumerNo varchar(20)  
as  
begin  
 declare @TransDesc varchar(300);  
 declare @afterTransAmt numeric(18, 5);  
 declare @beforeTransAmt numeric(18, 5);  
 declare @currentBalance numeric(18, 5);  
 declare @MoneyTransfer int = 21;  
 declare @DOCType varchar(50)  
  
 set @DOCType = 'REVERSE'  
  
 if @CurrentParentID != 0  
 begin  
  --Added For Electricity Bill  
  if (@ServiceID = @electricityServiceID)  
  begin  
   select @dcomAmt = (  
     select top (1) DiscountAmount  
     from dbo.TransactionDetails(nolock)  
     where RechargeID = @RechargeId  
      and DOCType = 'ElectricityBill'  
      and AmountCreditDebit = ''  
      and CommissionCreditDebit = 'CREDIT'  
      and UserID = @CurrentParentID  
     )  
  end  
  else if (@ServiceID = @MoneyTransfer)  
  begin  
   select @dcomAmt = (  
     select top (1) DiscountAmount  
     from dbo.TransactionDetails(nolock)  
     where RechargeID = @RechargeId  
      and DOCType = 'MoneyTransfer'  
      and AmountCreditDebit = ''  
      and CommissionCreditDebit = 'CREDIT'  
      and UserID = @CurrentParentID  
     )  
  end  
  else  
  begin  
   select @dcomAmt = (  
     select top (1) DiscountAmount  
     from dbo.TransactionDetails(nolock)  
     where RechargeID = @RechargeId  
      and DOCType = 'RECHARGE'  
      and AmountCreditDebit = ''  
      and CommissionCreditDebit = 'CREDIT'  
      and UserID = @CurrentParentID  
     )  
  end  
  
  print '@dcomAmt' + convert(varchar(30), @dcomAmt) + ':'  
  
  -- IF (@comAmt != 0)              
  begin  
   set @beforeTransAmt = 0;  
  
   select @beforeTransAmt = DMRBalance  
   from dbo.UserBalance  
   where UserID = @CurrentParentID  
  
   update [DBO].UserBalance  
   set DMRBalance = (DMRBalance - @dcomAmt)  
   where UserID = @CurrentParentID  
  
   set @afterTransAmt = 0;  
  
   select @afterTransAmt = (@beforeTransAmt - @dcomAmt)  
  
   set @TransDesc = 'DEBIT Reversed Commision Of Rs. ' + CONVERT(varchar, CONVERT(decimal(18, 5), @dcomAmt)) + 'For ServiceID Of ' + Convert(varchar, @ServiceID) + ' By ' + @Uname;  
  
   if (@ServiceID = @MoneyTransfer)  
   begin  
    set @DOCType = 'REVERSE DMT'  
   end  
  
   --UPLINE COMM AND BALANCE                  
   exec dbo.TransactionDetailsInsertByUser @CurrentParentID  
    ,@RechargeID  
    ,@ServiceID  
    ,@UserID  
    ,@ConsumerNo  
    ,@DOCType  
    ,@beforeTransAmt  
    ,@afterTransAmt  
    ,0 --@TransactionAmount                                     
    ,@dcomAmt  
    ,'DEBIT'  
    ,'DEBIT'  
    ,@TransDesc  
  end  
 end  
end  
  
GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsPurchaseSale]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---created by Abhiraj------------------  
-----purpose shows last and current month sale and purchase------  
---- TransactionDetailsPurchaseSale 2  
CREATE PROC [dbo].[TransactionDetailsPurchaseSale] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT;

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM userinformation
			WHERE UserID = @UserID
			)

	DECLARE @SystemAccountID INT = 60;

	IF (@UserTypeID IN (1))
	BEGIN
		SELECT TOP (2) YearMonth
			,[Balance Receive]
			,isNull([Balance Reverse], 0) AS [Balance Reverse]
			,([Balance Receive] - isNull([Balance Reverse], 0)) AS [purchase]
		FROM (
			SELECT sum(t.TransactionAmount) AS purchase
				,t.Doctype
				,0 AS Sale
				,DATENAME(month, t.DOC) AS YearMonth
			FROM [dbo].[TransactionDetails] t
			INNER JOIN userinformation u ON t.UserID = u.UserID
			WHERE t.UserID = @UserID
				AND (
					t.DocType = 'Balance Receive'
					OR t.Doctype = 'Balance Reverse'
					)
			GROUP BY t.Doctype
				,DATENAME(month, t.DOC)
			
			UNION
			
			SELECT sum(Amount) AS purchase
				,'Balance Receive' AS Doctype
				,0 AS Sale
				,DATENAME(month, DOC) AS YearMonth
			FROM virtualbalance
			GROUP BY DATENAME(month, DOC)
			) MainTable
		PIVOT(SUM(purchase) FOR DocType IN (
					[Balance Receive]
					,[Balance Reverse]
					)) AS PIVOTtable
		ORDER BY cast(YearMonth + ' 1, ' + cast(datepart(year, getdate()) AS CHAR(4)) AS DATE) DESC
	END
	ELSE
	BEGIN
		SELECT TOP (2) YearMonth
			,[Balance Receive]
			,isNull([Balance Reverse], 0) AS [Balance Reverse]
			,([Balance Receive] - isNull([Balance Reverse], 0)) AS [purchase]
		FROM (
			SELECT sum(t.TransactionAmount) AS purchase
				,t.Doctype
				,DATENAME(month, t.DOC) AS YearMonth
			FROM [dbo].[TransactionDetails] t
			INNER JOIN userinformation u ON t.UserID = u.UserID
			WHERE t.UserID = @UserID
				AND (
					t.DocType = 'Balance Receive'
					OR t.Doctype = 'Balance Reverse'
					)
			GROUP BY t.Doctype
				,DATENAME(month, t.DOC)
			) MainTable
		PIVOT(SUM(purchase) FOR DocType IN (
					[Balance Receive]
					,[Balance Reverse]
					)) AS PIVOTtable
		ORDER BY cast(YearMonth + ' 1, ' + cast(datepart(year, getdate()) AS CHAR(4)) AS DATE) DESC
	END

	IF (
			@UserTypeID IN (
				1
				,2
				,8
				)
			)
	BEGIN
		SELECT sum(t.TransactionAmount) AS sale
			,t.Doctype
			,DATENAME(month, t.DOC) AS YearMonth
		FROM [dbo].[TransactionDetails] t
		INNER JOIN userinformation u ON t.UserID = u.UserID
		WHERE t.UserID = @UserID
			AND t.DocType = 'Balance Transfer'
		GROUP BY t.Doctype
			,DATENAME(month, t.DOC)
		ORDER BY DATENAME(month, t.DOC)
	END
	ELSE
	BEGIN
		SELECT TOP (2) YearMonth
			,[RECHARGE]
			,isNull([REFUND], 0) AS [REFUND]
			,([RECHARGE] - isNull([REFUND], 0)) AS [sale]
		FROM (
			SELECT sum(t.TransactionAmount) AS sale
				,t.Doctype
				,DATENAME(month, t.DOC) AS YearMonth
			FROM [dbo].[TransactionDetails] t
			INNER JOIN userinformation u ON t.UserID = u.UserID
			WHERE t.UserID = @UserID
				AND (
					t.DocType = 'RECHARGE'
					OR t.Doctype = 'REFUND'
					)
			GROUP BY t.Doctype
				,DATENAME(month, t.DOC)
			) MainTable
		PIVOT(SUM(sale) FOR DocType IN (
					[RECHARGE]
					,[REFUND]
					)) AS PIVOTtable
		ORDER BY cast(YearMonth + ' 1, ' + cast(datepart(year, getdate()) AS CHAR(4)) AS DATE) DESC
	END

	IF (@UserTypeID IN (1))
	BEGIN
		PRINT 'Admin comm'

		SELECT sum(t.DiscountAmount) AS Comm
			,DATENAME(month, t.DOC) AS YearMonth
		FROM [dbo].[TransactionDetails] t
		INNER JOIN userinformation u ON t.UserID = u.UserID
		WHERE t.UserID = @SystemAccountID
			AND (upper(t.CommissionCreditDebit) = upper('CREDIT'))
		GROUP BY DATENAME(month, t.DOC)
		ORDER BY DATENAME(month, t.DOC)
	END
	ELSE
	BEGIN
		SELECT sum(t.DiscountAmount) AS Comm
			,DATENAME(month, t.DOC) AS YearMonth
		FROM [dbo].[TransactionDetails] t
		WHERE t.UserID = @UserID
			AND (upper(t.CommissionCreditDebit) = upper('CREDIT'))
		GROUP BY DATENAME(month, t.DOC)
		ORDER BY DATENAME(month, t.DOC)
	END
END



GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsRefundInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[TransactionDetailsRefundInsert] 3,97,21,'20356390959',3.00000,44,8      
CREATE PROCEDURE [dbo].[TransactionDetailsRefundInsert] @UserID INT
	,@RechargeID INT
	,@ServiceID INT
	,@ConsumerNo VARCHAR(20)
	,@TransactionAmount NUMERIC(18, 5)
	,@OperatorID INT
	,@RechargeProviderID INT
AS
BEGIN
	DECLARE @afterTransAmt NUMERIC(18, 5);
	DECLARE @beforeTransAmt NUMERIC(18, 5);
	DECLARE @TransDesc VARCHAR(200);
	--------------------------------FOR USER Account-------------------------------------------------------                  
	DECLARE @mType VARCHAR(50)
	DECLARE @comAmt NUMERIC(18, 5)
	DECLARE @userTypeID INT
	DECLARE @Uname VARCHAR(50)
	DECLARE @electricityServiceID INT = 14;
	DECLARE @MoneyTransfer INT = 21;
	DECLARE @DOCType VARCHAR(50)

	SET @DOCType = 'REFUND'

	IF (@ServiceID = @electricityServiceID)
	BEGIN
		SELECT @comAmt = DiscountAmount
		FROM dbo.TransactionDetails(NOLOCK)
		WHERE RechargeID = @RechargeId
			AND DOCType = 'ElectricityBill'
			AND AmountCreditDebit = 'DEBIT'
			AND UserID = @UserID
	END
	ELSE IF (@ServiceID = @MoneyTransfer)
	BEGIN
		SELECT @comAmt = DiscountAmount
		FROM dbo.TransactionDetails(NOLOCK)
		WHERE RechargeID = @RechargeId
			AND DOCType = 'MoneyTransfer'
			AND AmountCreditDebit = 'DEBIT'
			AND UserID = @UserID
	END
	ELSE
	BEGIN
		SELECT @comAmt = DiscountAmount
		FROM dbo.TransactionDetails(NOLOCK)
		WHERE RechargeID = @RechargeId
			AND DOCType = 'RECHARGE'
			AND AmountCreditDebit = 'DEBIT'
			AND UserID = @UserID
	END

	SET @comAmt = isNull(@comAmt, 0);

	IF (
		@ServiceID = @electricityServiceID
		OR @ServiceID = @MoneyTransfer
		)
	BEGIN
		SELECT @beforeTransAmt = DMRBalance
		FROM dbo.UserBalance
		WHERE UserID = @UserID

		UPDATE [DBO].UserBalance
		SET DMRBalance = (DMRBalance + @TransactionAmount) - @comAmt
		WHERE UserID = @UserID

		SELECT @afterTransAmt = DMRBalance
		FROM dbo.UserBalance
		WHERE UserID = @UserID
	END
	ELSE
	BEGIN
		SELECT @beforeTransAmt = CurrentBalance
		FROM dbo.UserBalance
		WHERE UserID = @UserID

		UPDATE [DBO].UserBalance
		SET CurrentBalance = (CurrentBalance + @TransactionAmount) - @comAmt
		WHERE UserID = @UserID

		SELECT @afterTransAmt = CurrentBalance
		FROM dbo.UserBalance
		WHERE UserID = @UserID

	END

	SELECT @Uname = name
	FROM DBO.UserInformation(NOLOCK)
	WHERE UserID = @UserID;

	SET @TransDesc = 'CREDIT RECHARGE REFUNDED Of Rs.' + CONVERT(VARCHAR, CONVERT(DECIMAL(18, 2), @TransactionAmount)) + ' For Consumer No. ' + @ConsumerNo + ' SERVICE ID.' + Convert(VARCHAR, @ServiceID)

	IF (@ServiceID = @MoneyTransfer)
	BEGIN
		SET @TransDesc = 'CREDIT MoneyTransfer REFUNDED Of Rs.' + CONVERT(VARCHAR, CONVERT(DECIMAL(18, 2), @TransactionAmount)) + ' For Consumer No. ' + @ConsumerNo + ' SERVICE ID.' + Convert(VARCHAR, @ServiceID)
		SET @DOCType = 'REFUND-MT'
	END

	EXEC dbo.TransactionDetailsInsertByUser @UserID
		,@RechargeID
		,@ServiceID
		,'0'
		,@ConsumerNo
		,@DOCType
		,@beforeTransAmt
		,@afterTransAmt
		,@TransactionAmount
		,@comAmt
		,'CREDIT'
		,'DEBIT'
		,@TransDesc

	--------------------------------FOR Upline Account-------------------------------------------------------                  
	BEGIN TRY
		DECLARE @dcomAmt NUMERIC(18, 5)
		DECLARE @CurrentParentID INT = 0;

		SELECT @CurrentParentID = ISNULL(ParentID, 0)
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @UserID;

		SELECT @Uname = name
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @UserID;
	END TRY

	BEGIN CATCH
		RETURN
	END CATCH

	EXEC dbo.[TransactionDetailsRefundUplineInsert] @CurrentParentID
		,@Uname
		,@ServiceID
		,@RechargeId
		,@dcomAmt
		,@electricityServiceID
		,@UserID
		,@ConsumerNo

	----------------SuperDistributor-----      
	BEGIN TRY
		DECLARE @CurrentSDParentID INT = 0;
		DECLARE @CurrentSDParentTypeID INT = 0;
		DECLARE @SDUname VARCHAR(300);

		SELECT @CurrentSDParentID = ISNULL(ParentID, 0)
			,@CurrentSDParentTypeID = ISNULL([UserTypeID], 0)
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @CurrentParentID;

		SELECT @Uname = name
		FROM DBO.UserInformation(NOLOCK)
		WHERE UserID = @UserID;
	END TRY

	BEGIN CATCH
		RETURN
	END CATCH

	DECLARE @AdminTypeID INT = 1;

	IF (@CurrentSDParentID != @AdminTypeID)
	BEGIN
		EXEC dbo.[TransactionDetailsRefundUplineInsert] @CurrentSDParentID
			,---SD      
			@Uname
			,@ServiceID
			,@RechargeId
			,@dcomAmt
			,@electricityServiceID
			,@UserID
			,---Retailer      
			@ConsumerNo
	END

	----------------------------------SYSTEM ACCOUNT---------------------------------------------                
	DECLARE @SystemAccount INT = 60;
	DECLARE @providerComm NUMERIC(18, 5) = 0;

	--Added For Electricity Bill      
	IF (@ServiceID = @electricityServiceID)
	BEGIN
		SELECT @providerComm = isnull(DiscountAmount, 0)
		FROM [dbo].TransactionDetails
		WHERE RechargeID = @RechargeId
			AND UserID = @SystemAccount
			AND DOCType = 'ElectricityBill'
	END
	ELSE IF (@ServiceID = @MoneyTransfer)
	BEGIN
		SELECT @providerComm = isnull(DiscountAmount, 0)
		FROM [dbo].TransactionDetails
		WHERE RechargeID = @RechargeId
			AND UserID = @SystemAccount
			AND DOCType = 'MoneyTransfer'
	END
	ELSE
	BEGIN
		SELECT @providerComm = isnull(DiscountAmount, 0)
		FROM [dbo].TransactionDetails
		WHERE RechargeID = @RechargeId
			AND UserID = @SystemAccount
			AND DOCType = 'RECHARGE'
	END

	PRINT '@dcomAmt' + convert(VARCHAR(30), @providerComm) + ':'

	IF (@providerComm != 0)
	BEGIN
		IF (
				@ServiceID = @electricityServiceID
				OR @ServiceID = @MoneyTransfer
				)
		BEGIN
			SELECT @beforeTransAmt = DMRBalance
			FROM DBO.UserBalance(NOLOCK)
			WHERE UserID = @SystemAccount

			UPDATE dbo.UserBalance
			SET DMRBalance = DMRBalance - @providerComm
			WHERE UserID = @SystemAccount

			SELECT @afterTransAmt = DMRBalance
			FROM DBO.UserBalance(NOLOCK)
			WHERE UserID = @SystemAccount
		END
		ELSE
		BEGIN
			SELECT @beforeTransAmt = CurrentBalance
			FROM DBO.UserBalance(NOLOCK)
			WHERE UserID = @SystemAccount

			UPDATE dbo.UserBalance
			SET CurrentBalance = CurrentBalance - @providerComm
			WHERE UserID = @SystemAccount

			SELECT @afterTransAmt = CurrentBalance
			FROM DBO.UserBalance(NOLOCK)
			WHERE UserID = @SystemAccount
		END

		SET @TransDesc = 'DEBIT Reversed Provider Commision Of Rs.' + CONVERT(VARCHAR, CONVERT(DECIMAL(18, 5), @providerComm)) + 'For ServiceID ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;
		SET @DOCType = 'REVERSE'

		IF (@ServiceID = @MoneyTransfer)
		BEGIN
			SET @DOCType = 'REVERSE-MT'
		END

		EXEC dbo.TransactionDetailsInsertByUser @SystemAccount
			,@RechargeID
			,@ServiceID
			,@UserID
			,@ConsumerNo
			,@DOCType
			,@beforeTransAmt
			,@afterTransAmt
			,0 --@TransactionAmount                                         
			,@providerComm
			,'DEBIT'
			,'DEBIT'
			,@TransDesc
	END

	DECLARE @ROWCNT INT;

	SET @ROWCNT = @@ROWCOUNT

	SELECT @ROWCNT
END

GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsRefundInsertDMR]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                                              
-- Author:  <Sainath Bhujbal>                                              
-- Create date: <29 Feb 2017 ,>                                              
-- Description: <Description,,>                     
-- Modified : Modified 30May2017 For Selecting CommAmt Of ElectricityBill DocType                   
-- Modified : Modified 29 Dec 2017 For Selecting upline Reverese commision                                            
-- =============================================                                         
--[dbo].[TransactionDetailsRefundInsert] 3, 86, 1,'7588215033',10,7,1                                
CREATE PROCedure [dbo].[TransactionDetailsRefundInsertDMR] @UserID int            
 ,@RechargeID int            
 ,@ServiceID int            
 ,@ConsumerNo varchar(20)            
 ,@TransactionAmount numeric(18, 5)            
 ,@OperatorID int            
 ,@RechargeProviderID int            
as            
begin            
 declare @afterTransAmt numeric(18, 5);            
 declare @beforeTransAmt numeric(18, 5);                       
 declare @TransDesc varchar(200);            
 --------------------------------FOR USER Account-------------------------------------------------------                                
 declare @mType varchar(50)            
 declare @comAmt numeric(18, 5)            
 declare @userType varchar(20)            
 declare @retailer varchar(20) = 'RETAILER';            
 declare @apiUser varchar(20) = 'API USER';            
 declare @Uname varchar(50)            
 declare @electricityServiceID int = 14;            
 declare @DMRServiceID int = 21;        
 declare @DMRBeneVerifyOperatorID int = 45;        
 declare @CREDITDEBIT Varchar(20);     
    
 if (@ServiceID = @electricityServiceID)            
 begin            
  select @comAmt = DiscountAmount            
  from dbo.TransactionDetails(nolock)            
  where RechargeID = @RechargeId            
   and DOCType = 'ElectricityBill'            
   and AmountCreditDebit = 'DEBIT'            
   and UserID = @UserID            
 end            
             
 else if (@ServiceID = @DMRServiceID)            
 begin            
  select @comAmt = DiscountAmount            
  from dbo.TransactionDetails(nolock)            
  where RechargeID = @RechargeId            
   and (DOCType = 'MoneyTransfer'  or DOCType = 'Beneficiary Verification'  )          
   and AmountCreditDebit = 'DEBIT'            
   and UserID = @UserID            
 end            
 else                   
 begin            
  select @comAmt = DiscountAmount            
  from dbo.TransactionDetails(nolock)            
  where RechargeID = @RechargeId            
   and DOCType = 'RECHARGE'            
   and AmountCreditDebit = 'DEBIT'            
   and UserID = @UserID            
 end            
        
SET @CREDITDEBIT = 'CREDIT';    
    
if (@OperatorID = @DMRBeneVerifyOperatorID)  -- Beneficiery Verification    
 begin            
  SET @comAmt = 0;    
  SET @CREDITDEBIT = '';    
 end         
            
 print '@@comAmt' + convert(varchar(30), @beforeTransAmt)            
            
 select @beforeTransAmt = DMRBalance            
 from dbo.UserBalance            
 where UserID = @UserID            
            
 print '@@beforeTransAmt' + convert(varchar(30), @beforeTransAmt)            
            
 update [DBO].UserBalance            
 set DMRBalance = (@beforeTransAmt + @TransactionAmount) - (@comAmt)            
 where UserID = @UserID            
            
 select @afterTransAmt = (@beforeTransAmt + @TransactionAmount) - @comAmt            
            
 print '@@afterTransAmt' + convert(varchar(30), @afterTransAmt)            
            
 select @Uname = name            
 from DBO.UserInformation(nolock)            
 where UserID = @UserID;            
     
    
 set @TransDesc = @CREDITDEBIT  + ' RECHARGE' + ' REFUNDED Of Rs.' + CONVERT(varchar, CONVERT(decimal(18, 2), @TransactionAmount)) + ' For Consumer No. ' + @ConsumerNo + ' SERVICE ID.' + Convert(varchar, @ServiceID)            
            
 exec dbo.TransactionDetailsInsertByUser @UserID            
  ,@RechargeID            
  ,@ServiceID            
  ,'0'            
  ,@ConsumerNo            
  ,'REFUND'            
  ,@beforeTransAmt            
  ,@afterTransAmt            
  ,@TransactionAmount            
  ,@comAmt            
  ,'CREDIT'            
  ,@CREDITDEBIT           
  ,@TransDesc            
            
 --------------------------------FOR Upline Account-------------------------------------------------------                                
 declare @ParentID int = 0;          
 declare @RetailerName varchar(50);            
            
 --DECLARE @CURRENTID INT=0;                                
 begin try            
  select @ParentID = ISNULL(ParentID, 0)            
  from DBO.UserInformation(nolock)            
  where UserID = @UserID;            
            
  select @RetailerName = name            
  from DBO.UserInformation(nolock)            
  where UserID = @UserID;            
 end try            
            
 begin catch            
  return            
 end catch            
            
 if (@ParentID != 0)            
 begin            
  exec ReverseUplineCommission @ServiceID            
   ,@RechargeID            
   ,@ParentID            
   ,@RetailerName            
   ,@UserID            
   ,@ConsumerNo            
 end            
            
 -- super dist                     
 declare @ParentOfParent_id int = 0            
            
 select @ParentOfParent_id = ISNULL(ParentID, 0)            
 from DBO.UserInformation(nolock)            
 where UserID = @ParentID;            
            
 if (            
   @ParentOfParent_id != 0            
   and @ParentOfParent_id != 1            
   )            
 begin            
  exec ReverseUplineCommission @ServiceID            
   ,@RechargeID            
   ,@ParentOfParent_id            
   ,@RetailerName            
   ,@UserID            
   ,@ConsumerNo            
 end            
            
 ----------------------------------SYSTEM ACCOUNT---------------------------------------------                              
 declare @SystemAccount int = 60;            
 declare @providerComm numeric(18, 5) = 0;            
            
 --Added For Electricity Bill                    
 if (@ServiceID = @electricityServiceID)            
 begin            
  select @providerComm = isnull(DiscountAmount, 0)            
  from [dbo].TransactionDetails            
  where RechargeID = @RechargeId            
   and UserID = @SystemAccount            
   and DOCType = 'ElectricityBill'            
 end            
 else if (@ServiceID = @DMRServiceID)            
 begin            
  select @providerComm = isnull(DiscountAmount, 0)            
  from [dbo].TransactionDetails            
  where RechargeID = @RechargeId            
   and UserID = @SystemAccount            
   and DOCType = 'MoneyTransfer'            
 end            
 else            
             
 begin            
  select @providerComm = isnull(DiscountAmount, 0)            
  from [dbo].TransactionDetails            
  where RechargeID = @RechargeId            
   and UserID = @SystemAccount            
   and DOCType = 'RECHARGE'            
 end            
            
 -- print '@dcomAmt' + convert(varchar(30),@providerComm) + ':'                           
 if (@providerComm != 0)            
 begin            
  select @beforeTransAmt = DMRBalance            
  from DBO.UserBalance(nolock)            
  where UserID = @SystemAccount            
            
  update dbo.UserBalance            
  set DMRBalance = DMRBalance --- @providerComm)            
  where UserID = @SystemAccount            
            
  select @afterTransAmt = @beforeTransAmt -- - @providerComm;            
            
  set @TransDesc = 'DEBIT Reversed Provider Commision Of Rs.' + CONVERT(varchar, CONVERT(decimal(18, 5), @providerComm)) + 'For ServiceID ' + Convert(varchar, @ServiceID) + ' By ' + @Uname;            
            
  exec dbo.TransactionDetailsInsertByUser @SystemAccount            
   ,@RechargeID            
   ,@ServiceID            
   ,@UserID            
   ,@ConsumerNo            
   ,'REVERSE'            
   ,@beforeTransAmt            
   ,@afterTransAmt            
   ,0 --@TransactionAmount                                                       
   ,@providerComm            
   ,'DEBIT'          
   ,'DEBIT'            
   ,@TransDesc            
 end            
            
 declare @ROWCNT int;            
            
 set @ROWCNT = @@ROWCOUNT            
            
 select @ROWCNT            
end 


GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsRefundUplineInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionDetailsRefundUplineInsert] @CurrentParentID INT
	,@Uname VARCHAR(100)
	,@ServiceID INT
	,@RechargeId INT
	,@dcomAmt NUMERIC(18, 5)
	,@electricityServiceID INT
	,@UserID INT
	,@ConsumerNo VARCHAR(20)
AS
BEGIN
	PRINT 'Inside TransactionDetailsRefundUplineInsert Proc'

	DECLARE @TransDesc VARCHAR(300);
	DECLARE @afterTransAmt NUMERIC(18, 5);
	DECLARE @beforeTransAmt NUMERIC(18, 5);
	DECLARE @currentBalance NUMERIC(18, 5);
	DECLARE @MoneyTransfer INT = 21;
	DECLARE @DOCType VARCHAR(50)

	SET @DOCType = 'REVERSE'

	IF @CurrentParentID != 0
	BEGIN
		--Added For Electricity Bill            
		IF (@ServiceID = @electricityServiceID)
		BEGIN
			SELECT @dcomAmt = (
					SELECT TOP (1) DiscountAmount
					FROM dbo.TransactionDetails(NOLOCK)
					WHERE RechargeID = @RechargeId
						AND DOCType = 'ElectricityBill'
						AND AmountCreditDebit = ''
						AND CommissionCreditDebit = 'CREDIT'
						AND UserID = @CurrentParentID
					)
		END
		ELSE IF (@ServiceID = @MoneyTransfer)
		BEGIN
			SELECT @dcomAmt = (
					SELECT TOP (1) DiscountAmount
					FROM dbo.TransactionDetails(NOLOCK)
					WHERE RechargeID = @RechargeId
						AND DOCType = 'MoneyTransfer'
						AND AmountCreditDebit = ''
						AND CommissionCreditDebit = 'CREDIT'
						AND UserID = @CurrentParentID
					)
		END
		ELSE
		BEGIN
			SELECT @dcomAmt = (
					SELECT TOP (1) DiscountAmount
					FROM dbo.TransactionDetails(NOLOCK)
					WHERE RechargeID = @RechargeId
						AND DOCType = 'RECHARGE'
						AND AmountCreditDebit = ''
						AND CommissionCreditDebit = 'CREDIT'
						AND UserID = @CurrentParentID
					)
		END

		PRINT '@dcomAmt' + convert(VARCHAR(30), @dcomAmt) + ':'

		IF (@dcomAmt IS NULL)
		BEGIN
			PRINT '@dcomAmt was NULL, Set it to 0'

			SET @dcomAmt = 0
		END

		BEGIN
			SET @beforeTransAmt = 0;

			IF (
				@ServiceID = @electricityServiceID
				OR @ServiceID = @MoneyTransfer
				)
			BEGIN
				SELECT @beforeTransAmt = DMRBalance
				FROM dbo.UserBalance
				WHERE UserID = @CurrentParentID

				UPDATE [DBO].UserBalance
				SET DMRBalance = DMRBalance - @dcomAmt
				WHERE UserID = @CurrentParentID

				SELECT @afterTransAmt = DMRBalance
				FROM dbo.UserBalance
				WHERE UserID = @CurrentParentID
			END
			ELSE
			BEGIN
				SELECT @beforeTransAmt = CurrentBalance
				FROM dbo.UserBalance
				WHERE UserID = @CurrentParentID

				UPDATE [DBO].UserBalance
				SET CurrentBalance = CurrentBalance - @dcomAmt
				WHERE UserID = @CurrentParentID

				SELECT @afterTransAmt = CurrentBalance
				FROM dbo.UserBalance
				WHERE UserID = @CurrentParentID
			END

			SET @TransDesc = 'DEBIT Reversed Commision Of Rs. ' + CONVERT(VARCHAR, CONVERT(DECIMAL(18, 5), @dcomAmt)) + 'For ServiceID Of ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;

			IF (@ServiceID = @MoneyTransfer)
			BEGIN
				SET @DOCType = 'REVERSE DMT'
			END

			--UPLINE COMM AND BALANCE                            
			EXEC dbo.TransactionDetailsInsertByUser @CurrentParentID
				,@RechargeID
				,@ServiceID
				,@UserID
				,@ConsumerNo
				,@DOCType
				,@beforeTransAmt
				,@afterTransAmt
				,0 --@TransactionAmount                                               
				,@dcomAmt
				,'DEBIT'
				,'DEBIT'
				,@TransDesc
		END
	END
END

GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================                                                                    
-- AUTHOR:Priyanka Deshmukh                                                                     
-- ALTER  DATE: 16-8-2017                                                                     
-- DESCRIPTION: THIS PRODCEDURE MADE FOR SELECT data FROM TRANSECTION                                                                     
-- ========================================================================        
--[TransactionDetailsSelect] 1,'25/09/2017','4/10/2017'    
CREATE PROCEDURE [dbo].[TransactionDetailsSelect] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	IF (@UserTypeID = 1)
	BEGIN
		SELECT UI.UserName
			,UI.UserTypeID
			,T.RechargeID
			,T.UserID
			,T.TransactionNumberID
			,T.OpeningBalance
			,T.ClosingBalance
			,T.TransactionAmount
			,T.DiscountAmount
			,T.AmountCreditDebit
			,T.CommissionCreditDebit
			,T.TransactionDescription
			,convert(VARCHAR(12), T.DOC, 113) + right(convert(VARCHAR(39), T.DOC, 22), 11) AS DOC
			,T.DOCType
			,T.ConsumerNumber
		FROM [dbo].UserInformation UI(NOLOCK)
		INNER JOIN [dbo].TransactionDetails T(NOLOCK) ON UI.UserID = T.UserID
		WHERE T.DOCType NOT IN ('Virtual Balance')
		ORDER BY convert(VARCHAR(12), T.DOC, 113) + right(convert(VARCHAR(39), T.DOC, 22), 11) DESC
	END

	--Distributor    
	IF (@UserTypeID = 2)
	BEGIN
		SELECT UI.UserName
			,UI.UserTypeID
			,T.RechargeID
			,T.UserID
			,T.TransactionNumberID
			,T.OpeningBalance
			,T.ClosingBalance
			,T.TransactionAmount
			,T.DiscountAmount
			,T.AmountCreditDebit
			,T.CommissionCreditDebit
			,T.TransactionDescription
			,convert(VARCHAR(12), T.DOC, 113) + right(convert(VARCHAR(39), T.DOC, 22), 11) AS DOC
			,T.DOCType
			,T.ConsumerNumber
		FROM [dbo].UserInformation UI(NOLOCK)
		INNER JOIN [dbo].TransactionDetails T(NOLOCK) ON UI.UserID = T.UserID
		WHERE @UserID IN (
				UI.ParentID
				,T.UserID
				)
			AND T.DOCType NOT IN ('Virtual Balance')
		ORDER BY convert(VARCHAR(12), T.DOC, 113) + right(convert(VARCHAR(39), T.DOC, 22), 11) DESC
	END

	--Retailer    
	IF (
			@UserTypeID = 3
			OR @UserTypeID = 4
			)
	BEGIN
		SELECT UI.UserName
			,UI.UserTypeID
			,T.UserID
			,T.RechargeID
			,T.TransactionNumberID
			,T.OpeningBalance
			,T.ClosingBalance
			,T.TransactionAmount
			,T.DiscountAmount
			,T.AmountCreditDebit
			,T.CommissionCreditDebit
			,T.TransactionDescription
			,convert(VARCHAR(12), T.DOC, 113) + right(convert(VARCHAR(39), T.DOC, 22), 11) AS DOC
			,T.DOCType
			,T.ConsumerNumber
		FROM [dbo].UserInformation UI(NOLOCK)
		INNER JOIN [dbo].TransactionDetails T(NOLOCK) ON UI.UserID = T.UserID
		WHERE UI.UserID = @UserID
			AND T.DOCType NOT IN ('Virtual Balance')
		ORDER BY convert(VARCHAR(12), T.DOC, 113) + right(convert(VARCHAR(39), T.DOC, 22), 11) DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsSelectForBackUp]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Author :Rahul Hembade   
--Date : 15 Feb 2018    
CREATE PROCEDURE [dbo].[TransactionDetailsSelectForBackUp]
AS
BEGIN
	SELECT TOP (10000) UI.UserName
		,UI.UserTypeID
		,T.RechargeID
		,T.UserID
		,T.TransactionNumberID
		,T.OpeningBalance
		,T.ClosingBalance
		,T.TransactionAmount
		,T.DiscountAmount
		,T.AmountCreditDebit
		,T.CommissionCreditDebit
		,T.TransactionDescription
		,convert(VARCHAR(12), T.DOC, 113) + right(convert(VARCHAR(39), T.DOC, 22), 11) AS DOC
		,T.DOCType
		,T.ConsumerNumber
	FROM UserInformation UI
	INNER JOIN TransactionDetails T ON UI.UserID = T.UserID
	ORDER BY convert(DATETIME, T.DOC, 103) DESC
END



GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsSelectSearch]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- ========================================================================                                                                          
-- AUTHOR:Priyanka Deshmukh                                                                           
-- CREATE DATE: 22 MARCH 2017                                                                            
-- DESCRIPTION: THIS PRODCEDURE MADE FOR SELECT data FROM TRANSECTION                                                                           
-- ========================================================================             
-- TransactionDetailsSelectSearch '1',1,'25/7/2018','25/7/2018',100           
-- TransactionDetailsSelectSearch 1431,0,'10/10/2018','13/10/2018',500000        
CREATE PROCEDURE [dbo].[TransactionDetailsSelectSearch] @UserID VARCHAR(50) = NULL  
 ,@IsDownline INT  
 ,@FromDate VARCHAR(100)  
 ,@ToDate VARCHAR(100)  
 ,@Records INT  
AS  
BEGIN  
 DECLARE @UserTypeID INT;  
 DECLARE @retailer VARCHAR(20) = 'RETAILER';  
 DECLARE @Customer VARCHAR(20) = 'Customer';  
 DECLARE @distributor VARCHAR(20) = 'DISTRIBUTOR';  
 DECLARE @superdistributor VARCHAR(20) = 'SuperDistributor';  
 DECLARE @admin VARCHAR(20) = 'ADMIN';  
 DECLARE @apiUser VARCHAR(20) = 'APIUSER';  
 DECLARE @client VARCHAR(20) = 'CLIENT';  
 DECLARE @userType VARCHAR(20);  
  
 SELECT @userType = UTM.UserType  
 FROM [dbo].UserInformation(NOLOCK) UI  
 INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID  
 WHERE UI.UserID = @UserID  
  
 SET @UserTypeID = (  
   SELECT UserTypeID  
   FROM UserInformation  
   WHERE UserID = @UserID  
   )  
  
 DECLARE @TempTbl TABLE (UserID VARCHAR(200))  
  
 IF (  
   Upper(@userType) = Upper(@admin)  
   AND @IsDownline = 1  
   )  
 BEGIN  
  INSERT INTO @TempTbl  
  SELECT userID  
  FROM UserInformation  
 END  
 ELSE IF (@IsDownline = 0)  
 BEGIN  
  INSERT INTO @TempTbl  
  SELECT userID  
  FROM UserInformation  
  WHERE UserID = @UserID  
 END  
 ELSE  
 BEGIN  
  INSERT INTO @TempTbl  
  SELECT UserID  
  FROM UserInformation  
  WHERE ParentID = @UserID  
  
  INSERT INTO @TempTbl  
  VALUES (@UserID)  
 END  
  
 SELECT TOP (@Records) UI.UserName  
  ,UI.ParentID  
  ,T.RechargeID  
  ,UI.UserTypeID  
  ,T.UserID  
  ,UI.MobileNumber  
  ,T.TransactionNumberID  
  ,T.OpeningBalance  
  ,T.ClosingBalance  
  ,T.TransactionAmount  
  ,T.DiscountAmount  
  ,T.AmountCreditDebit  
  ,T.CommissionCreditDebit  
  ,T.TransactionDescription  
  --, IsnULL(OM.OperatorName, 'Transfer') as OperatorName          
  ,convert(VARCHAR(12), T.DOC, 113) + right(convert(VARCHAR(39), T.DOC, 22), 11) AS DOC  
  ,T.DOCType  
  ,T.ConsumerNumber  
  ,COnvert(Decimal(18,2),T.GST) As GST
  ,COnvert(Decimal(18,2),T.TDS) As TDS
 FROM [dbo].TransactionDetails T(NOLOCK)  
 INNER JOIN [dbo].UserInformation UI(NOLOCK) ON UI.UserID = T.UserID  
 LEFT JOIN [dbo].Recharge R(NOLOCK) ON R.RechargeID = T.RechargeID  
 LEFT JOIN OperatorMaster OM(NOLOCK) ON OM.OperatorID = R.OperatorID  
 WHERE (  
   convert(DATE, T.DOC, 103) >= convert(DATE, @FromDate, 103)  
   AND convert(DATE, T.DOC, 103) <= convert(DATE, @ToDate, 103)  
   )  
  AND UI.UserID NOT IN (60)  
  AND UI.UserID IN (  
   SELECT UserID  
   FROM @TempTbl  
   )  
 ORDER BY T.DOC DESC  
  ,T.TransactionNumberID DESC  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsSelectTodaysTotalCommission]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- TransactionDetailsSelectTodaysTotalCommission 1058  
CREATE PROCEDURE [dbo].[TransactionDetailsSelectTodaysTotalCommission] @UserID INT
	,@date DATE = NULL
AS
BEGIN
	IF (@date IS NULL)
	BEGIN
		SET @date = (switchoffset(sysdatetimeoffset(), '+05:30'))
	END

	DECLARE @UserType INT;
	DECLARE @NewUserID INT

	SET @UserType = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	IF (@UserType = 1)
	BEGIN
		SET @NewUserID = 60;
	END
	ELSE
	BEGIN
		SET @NewUserID = @UserID;
	END

	PRINT @NewUserID

	SELECT IsNull(sum(Commission), 0) AS DiscountAmount
		,count(RechargeCount) AS DiscountRechargeCount
	FROM (
		SELECT COUNT(R.RechargeID) AS RechargeCount
			,sum(R.Amount) AS Amount
			,sum(TD.DiscountAmount) AS Commission
			,S.ServiceName
		FROM Recharge R
		LEFT JOIN TransactionDetails TD ON TD.RechargeID = R.RechargeID
		AND R.ServiceID = TD.ServiceID
		LEFT JOIN ServiceMaster S ON S.ServiceID = R.ServiceID
		WHERE R.STATUS = 'SUCCESS'
			AND TD.CommissionCreditDebit = 'CREDIT'
			AND TD.AmountCreditDebit IN (
				'DEBIT'
				,''
				)
			AND TD.UserID = @NewUserID
			AND DOCType in ('RECHARGE','ElectricityBill')
			AND Convert(DATE, R.DOC, 103) BETWEEN Convert(DATE, @date, 103)
				AND Convert(DATE, @date, 103)
		GROUP BY R.ServiceID
			,S.ServiceName
		
		UNION ALL
		
		SELECT COUNT(R.MoneyTransferID) AS RechargeCount
			,sum(R.Amount) AS Amount
			,sum(TD.DiscountAmount) AS Commission
			,S.ServiceName
		FROM dbo.MoneyTransfer R
		LEFT JOIN TransactionDetails TD ON TD.RechargeID = R.MoneyTransferID
		LEFT JOIN ServiceMaster S ON S.ServiceID = R.DMRServiceID
				WHERE R.STATUS = 'SUCCESS'
			AND TD.CommissionCreditDebit = 'CREDIT'
			AND TD.AmountCreditDebit IN (
				'DEBIT'
				,''
				)
			AND TD.UserID = @NewUserID
			AND DOCType = 'MoneyTransfer'
			AND Convert(DATE, R.DOC, 103) BETWEEN Convert(DATE, @date, 103)
				AND Convert(DATE, @date, 103)
		GROUP BY R.DMRServiceID
			,S.ServiceName
		) t
END



GO
/****** Object:  StoredProcedure [dbo].[TransactionDetailsSelectUserSales]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select convert(VARCHAR, RE.Doc, 101) AS DOC from RECHARGE
--WARNING! ERRORS ENCOUNTERED DURING SQL PARSING!
--Author:Sainath Bhujbal
--Date:13 Sep 2017
--TransactionDetailsSelectUserSales 1,'13/08/2018','13/08/2018',100
CREATE PROCEDURE [dbo].[TransactionDetailsSelectUserSales] @UserID INT
	,@FromDate VARCHAR(100)
	,@ToDate VARCHAR(100)
	,@Records INT
AS
BEGIN
	DECLARE @userType VARCHAR(20);
	DECLARE @admin VARCHAR(20) = 'ADMIN';
	DECLARE @distributor VARCHAR(20) = 'Distributor';
	DECLARE @SuperDistributor VARCHAR(20) = 'SuperDistributor';
	DECLARE @retailer VARCHAR(20) = 'Retailer';
	DECLARE @apiUser VARCHAR(20) = 'APIUSER';

	SELECT @userType = UTM.UserType
	FROM [dbo].UserInformation(NOLOCK) UI
	INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @UserID

	IF (Upper(@userType) = Upper(@admin))
	BEGIN
		SELECT TOP (@Records) --RE.DOC
			--convert(VARCHAR(12), RE.Doc, 113) + right(convert(VARCHAR(39), RE.Doc, 22), 11) AS DOC
			convert(VARCHAR, RE.Doc, 101) AS DOC
			,SM.ServiceName
			,Sum(RE.Amount) AS Amount
			,Count(RE.Amount) AS RechargeCount
			,UI.UserName
			,RE.UserID
			,UIN.UserName AS DistributerUserName
			,Sum(TD.DiscountAmount) AS CommissionGivenToRet
		FROM dbo.RECHARGE(NOLOCK) AS RE
		LEFT JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
		LEFT JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
		INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
		--INNER JOIN dbo.TransactionDetails(NOLOCK) AS TDAdmin ON TDAdmin.RechargeID = RE.RechargeID
		INNER JOIN ServiceMaster SM ON SM.ServiceID = RE.ServiceID
		WHERE TD.DOCType = 'RECHARGE'
			AND (
				--TD.CommissionCreditDebit = 'CREDIT'
				--OR
				TD.AmountCreditDebit = 'DEBIT'
				)
			AND RE.STATUS IN (
				--'PROCESS'
				'SUCCESS'
				--,'Pending'
				--,'PENDING'
				,'Success'
				)
			AND convert(DATE, RE.DOC, 103) >= convert(DATE, @FromDate, 103)
			AND convert(DATE, RE.DOC, 103) <= convert(DATE, @ToDate, 103)
			AND TD.UserID NOT IN (
				1
				,60
				)
		GROUP BY ServiceName
			,UI.UserName
			,UIN.UserName
			,RE.UserID
			--,convert(VARCHAR(12), RE.Doc, 113) + right(convert(VARCHAR(39), RE.Doc, 22), 11)
			,convert(VARCHAR, RE.Doc, 101)
	END
	ELSE IF (
			Upper(@userType) = Upper(@distributor)
			OR Upper(@userType) = Upper(@SuperDistributor)
			)
	BEGIN
		SELECT TOP (@Records) --RE.DOC
			--convert(VARCHAR(12), RE.Doc, 113) + right(convert(VARCHAR(39), RE.Doc, 22), 11) AS DOC
			convert(VARCHAR, RE.Doc, 101) AS DOC
			,SM.ServiceName
			,Sum(RE.Amount) AS Amount
			,Count(RE.Amount) AS RechargeCount
			,UI.UserName
			,RE.UserID
			,UIN.UserName AS DistributerUserName
			,Sum(TD.DiscountAmount) AS CommissionGivenToRet
		FROM dbo.RECHARGE(NOLOCK) AS RE
		LEFT JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
		INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
		INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
		--INNER JOIN dbo.TransactionDetails(NOLOCK) AS TDAdmin ON TDAdmin.RechargeID = RE.RechargeID
		INNER JOIN ServiceMaster SM ON SM.ServiceID = RE.ServiceID
		WHERE TD.DOCType = 'RECHARGE'
			AND TD.CommissionCreditDebit = 'CREDIT'
			AND TD.AmountCreditDebit = 'DEBIT'
			AND RE.STATUS IN (
				--'PROCESS'
				'SUCCESS'
				--,'Pending'
				--,'PENDING'
				,'Success'
				)
			AND convert(DATE, RE.DOC, 103) >= convert(DATE, @FromDate, 103)
			AND convert(DATE, RE.DOC, 103) <= convert(DATE, @ToDate, 103)
			AND UI.ParentID IN (@UserID)
		GROUP BY ServiceName
			,UI.UserName
			,UIN.UserName
			,RE.UserID
			--,convert(VARCHAR(12), RE.Doc, 113) + right(convert(VARCHAR(39), RE.Doc, 22), 11)
			,convert(VARCHAR, RE.Doc, 101)
	END
	ELSE IF (
			Upper(@userType) = Upper(@retailer)
			OR Upper(@userType) = Upper(@apiUser)
			)
	BEGIN
		SELECT TOP (@Records) --RE.DOC
			--convert(VARCHAR(12), RE.Doc, 113) + right(convert(VARCHAR(39), RE.Doc, 22), 11) AS DOC
			convert(VARCHAR, RE.Doc, 101) AS DOC
			,SM.ServiceName
			,Sum(RE.Amount) AS Amount
			,Count(RE.Amount) AS RechargeCount
			,UI.UserName
			,RE.UserID
			,UIN.UserName AS DistributerUserName
			,Sum(TD.DiscountAmount) AS CommissionGivenToRet
		FROM dbo.RECHARGE(NOLOCK) AS RE
		LEFT JOIN dbo.UserInformation(NOLOCK) AS UI ON RE.UserID = UI.UserID
		INNER JOIN dbo.UserInformation(NOLOCK) AS UIN ON UI.ParentID = UIN.UserID
		INNER JOIN dbo.TransactionDetails(NOLOCK) AS TD ON TD.RechargeID = RE.RechargeID
		--INNER JOIN dbo.TransactionDetails(NOLOCK) AS TDAdmin ON TDAdmin.RechargeID = RE.RechargeID
		INNER JOIN ServiceMaster SM ON SM.ServiceID = RE.ServiceID
		WHERE TD.DOCType = 'RECHARGE'
			AND TD.CommissionCreditDebit = 'CREDIT'
			AND TD.AmountCreditDebit = 'DEBIT'
			AND RE.STATUS IN (
				--'PROCESS'
				'SUCCESS'
				--,'Pending'
				--,'PENDING'
				,'Success'
				)
			AND convert(DATE, RE.DOC, 103) >= convert(DATE, @FromDate, 103)
			AND convert(DATE, RE.DOC, 103) <= convert(DATE, @ToDate, 103)
			AND UI.UserID IN (@UserID)
		GROUP BY ServiceName
			,UI.UserName
			,RE.UserID
			,UIN.UserName
			--,convert(VARCHAR(12), RE.Doc, 113) + right(convert(VARCHAR(39), RE.Doc, 22), 11)
			,convert(VARCHAR, RE.Doc, 101)
	END
END



GO
/****** Object:  StoredProcedure [dbo].[TrustableDeviceInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================  
--Auther :Rahul Hembade  
--Date :9-7-17  
--Purpose OperatorMaster Insert  
--==========================================  
--OperatorMasterInsert 1,'Idea',1.1,1.1,'%'  
CREATE PROCEDURE [dbo].[TrustableDeviceInsert] @IsTrustable INT
	,@UserID INT
	,@FingerPrint VARCHAR(100)
AS
BEGIN
	INSERT INTO [dbo].[TrustableDevice] (
		[IPAddress]
		,[UserID]
		,[Verify]
		)
	VALUES (
		@FingerPrint
		,@UserID
		,@IsTrustable
		);
END



GO
/****** Object:  StoredProcedure [dbo].[TrustableDeviceInsertApp]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================    
--Auther :Rahul Hembade    
--Purpose : Insert    
--==========================================    
--   
CREATE PROCEDURE [dbo].[TrustableDeviceInsertApp] @IsTrustable INT
	,@UserID INT
	,@FingerPrint VARCHAR(100)
	,@Browser VARCHAR(100)
AS
BEGIN
	INSERT INTO [dbo].[TrustableDevice] (
		[IPAddress]
		,[UserID]
		,[Browser]
		,[Verify]
		)
	VALUES (
		@FingerPrint
		,@UserID
		,@Browser
		,@IsTrustable
		);
END



GO
/****** Object:  StoredProcedure [dbo].[TrustableDeviceSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TrustableDeviceSelect] @UserID INT
	,@IpAddress VARCHAR(100)
AS
BEGIN
	SELECT [Id]
	FROM [dbo].[TrustableDevice](NOLOCK)
	WHERE [IPAddress] = @IpAddress
		AND [UserID] = @UserID
		AND [Verify] = 1
END



GO
/****** Object:  StoredProcedure [dbo].[TrustableDeviceSelectApp]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------    
--ALTER d By : Rahul Hembade   
--Reason : OTP    
--------------------------------------------    
----Select * from LoginHistory order by doc desc    
--[TrustableDeviceSelect] 1,'213.23'    
CREATE PROCEDURE [dbo].[TrustableDeviceSelectApp] @UserID INT
	,@IpAddress VARCHAR(100)
	,@Browser VARCHAR(100)
AS
BEGIN
	SELECT 1 AS [Id]
		--select [Id]
		--from [dbo].[TrustableDevice](nolock)
		--where [IPAddress] = @IpAddress
		--	and [UserID] = @UserID
		--	and [Browser] = @Browser
		--	and [Verify] = 1
END



GO
/****** Object:  StoredProcedure [dbo].[UILoginOTPInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UILoginOTPInsert] @UserID INT
	,@OTP VARCHAR(20)
AS
BEGIN
	UPDATE UserInformation
	SET OTP = @OTP
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[UpdateFreeUserBalance]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================          
--Auther :Sainath Bhujbal          
--Date :01-04-17          
--Purpose :Update FreeUserBalance at the time of  paymentGatewayreply  success
--==========================================     
--UpdateFreeUserBalance 77,0
CREATE PROCEDURE[dbo].[UpdateFreeUserBalance] @UserID INT
	,@Amount INT
	,@TransactionID VARCHAR(50)
	,@ConsumerNumber VARCHAR(20)
	,@ServiceID INT
	,@PaymentGatewayReplyID INT
AS
BEGIN
	DECLARE @openingBalance INT
	DECLARE @afterTransAmt INT
	DECLARE @TransDesc VARCHAR(500)

	SELECT @openingBalance = ISNULL(CurrentBalance, 0)
	FROM DBO.UserBalance(NOLOCK)
	WHERE UserID = @UserID

	SET @afterTransAmt = @openingBalance + @Amount

	UPDATE UserBalance
	SET CurrentBalance = @afterTransAmt
	WHERE UserID = @UserID

	SET @TransDesc = 'Payment gateway amount:' + CONVERT(VARCHAR, @Amount) + ', TransID: ' + Convert(VARCHAR, @TransactionID) + ' By Payment gateway';

	--TransactionDetails insert of payment gateway amount to the user              
	EXEC dbo.TransactionDetailsInsertByUser @UserID
		,@PaymentGatewayReplyID
		,@ServiceID
		,0
		,@ConsumerNumber
		,'PaymentGatewayTransfer'
		,@openingBalance
		,@afterTransAmt
		,@Amount --@TransactionAmount                                 
		,0
		,'CREDIT'
		,''
		,@TransDesc
END



GO
/****** Object:  StoredProcedure [dbo].[UpdateInitialPass]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[UpdateInitialPass] @userId INT
AS
BEGIN
	UPDATE [dbo].UserInformation
	SET IsInitialPassword = 1
	WHERE UserID = @userId
END



GO
/****** Object:  StoredProcedure [dbo].[UpdateMoneyTransfer]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[UpdateMoneyTransfer] @UserID INT
	,@APIID VARCHAR(50) = NULL
	,@APIRefNo VARCHAR(50) = NULL
	,@APIOprID VARCHAR(50) = NULL
	,@APIRequestID VARCHAR(50) = NULL
	,@APIClosingBal VARCHAR(50) = NULL
	,@Status VARCHAR(150) = NULL
	,@StatusCode VARCHAR(50) = NULL
	,@Response VARCHAR(max) = NULL
	,@MoneyTransferID INT
	,@SessionID VARCHAR(1000) = NULL
	,@Bank VARCHAR(1000) = NULL
	,@BeneficiaryName VARCHAR(1000) = NULL
	,@BeneficiaryAccountNo VARCHAR(1000) = NULL
AS
BEGIN
	DECLARE @Success VARCHAR(20) = 'SUCCESS';
	DECLARE @BeforeTransAmt NUMERIC(18, 5) = 0;
	DECLARE @providerComm NUMERIC(18, 5) = 0;
	DECLARE @providerCommType VARCHAR(10) = 'P';
	DECLARE @providerOpBal NUMERIC(18, 5) = 0;
	DECLARE @providerClBal NUMERIC(18, 5) = 0;
	DECLARE @transactionAmt NUMERIC(18, 5) = 0;
	DECLARE @RechargeID INT;
	DECLARE @SystemAccount INT = 60;
	DECLARE @AfterTransAmt NUMERIC(18, 5) = 0;
	DECLARE @ServiceID INT;
	DECLARE @ConsumerNo VARCHAR(50);
	DECLARE @RechargeProviderID INT = 10;
	DECLARE @DMROperatorID INT;

	SELECT @RechargeId = MoneyTransferID
		,@providerOpBal = DMRProviderOpeningBalance
		,@providerClBal = DMRProviderClosingBalance
		,@transactionAmt = Amount
		,@ServiceID = DMRServiceID
		,@ConsumerNo = SenderNumber
		,@DMROperatorID = DMROperatorID
	FROM dbo.MoneyTransfer(NOLOCK)
	WHERE MoneyTransferID = @MoneyTransferID

	PRINT '@DMT' + convert(VARCHAR(30), @MoneyTransferID)

	SELECT @providerComm = isnull(CommissionAdmin, 0)
		,@providerCommType = Upper(MarginType)
	FROM dbo.RechargeProviderOperatorMaster
	WHERE RechargeProviderID = @RechargeProviderID
		AND OperatorID = @DMROperatorID

	PRINT '@providerComm' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType

	IF @providerCommType = 'P'
	BEGIN
		SET @providerComm = (@providerComm * @transactionAmt) / 100;

		PRINT '@providerComm1' + convert(VARCHAR(30), @providerComm) + ':' + @providerCommType
	END
	ELSE
	BEGIN
		SET @providerComm = @providerComm;
	END

	PRINT @providerComm

	IF (upper(@Status) = upper(@Success))
	BEGIN
		UPDATE [MoneyTransfer]
		SET  [ProviderDOCNo]= @APIID
			,[ProviderRefNo] = @APIRefNo
			,[ProviderOprID] = @APIOprID
			,DMRProviderClosingBalance = @APIClosingBal
			,STATUS = @Status
			,StatusCode = @StatusCode
			,[Description] = @Response
			,SessionID = @SessionID
			,[Bank] = @Bank
			,[BeneficiaryName] = @BeneficiaryName
			,[BeneficiaryAccountNo] = @BeneficiaryAccountNo
		WHERE MoneyTransferID = @MoneyTransferID

		--AND STATUS = 'PROCESS'          
		PRINT '@update done' + convert(VARCHAR(30), @MoneyTransferID)

		BEGIN TRY
			IF (@providerComm IS NOT NULL)
			BEGIN
				SELECT @BeforeTransAmt = CurrentBalance
				FROM DBO.UserBalance(NOLOCK)
				WHERE UserID = @SystemAccount

				--Adde For System AccountBal=(ProviderCommission-(RetailerCommission + distributerCommission)                        
				DECLARE @RetailerCommission NUMERIC(18, 5) = 0;

				SELECT @RetailerCommission = isnull(DiscountAmount, 0)
				FROM dbo.TransactionDetails(NOLOCK)
				WHERE RechargeID = @MoneyTransferID
					AND Upper(AmountCreditDebit) = Upper('DEBIT')
					AND Upper(DOCType) = Upper('MoneyTransfer')
					AND UserId != @SystemAccount

				PRINT @RetailerCommission

				DECLARE @DistributerCommission NUMERIC(18, 5) = 0;

				SELECT @DistributerCommission = isnull(DiscountAmount, 0)
				FROM dbo.TransactionDetails(NOLOCK)
				WHERE RechargeID = @MoneyTransferID
					AND AmountCreditDebit = ''
					AND Upper(DOCType) = Upper('MoneyTransfer')
					AND UserId != @SystemAccount

				PRINT @DistributerCommission

				DECLARE @benefitCommission NUMERIC(18, 5) = 0;

				SET @benefitCommission = @providerComm - (@RetailerCommission + @DistributerCommission)

				PRINT @benefitCommission

				SET @AfterTransAmt = @BeforeTransAmt + @benefitCommission;

				UPDATE dbo.UserBalance
				SET CurrentBalance = @AfterTransAmt
				WHERE UserID = @SystemAccount

				DECLARE @Uname VARCHAR(50)

				SELECT @Uname = name
				FROM DBO.UserInformation(NOLOCK)
				WHERE UserID = @UserID;

				DECLARE @TransDesc VARCHAR(200)

				SET @TransDesc = 'Provider Commision Amount:' + CONVERT(VARCHAR, @benefitCommission) + ',ServicesID: ' + Convert(VARCHAR, @ServiceID) + ' By ' + @Uname;

				EXEC dbo.TransactionDetailsInsertByUser @SystemAccount
					,-- @UserID                                
					@RechargeID
					,--@RechargeID                           
					@ServiceID
					,--@serviceID                          
					@UserID
					,--@AssignedUserID                          
					@ConsumerNo
					,--@ConsumerNumber                           
					'MoneyTransfer'
					,--@DOCType                          
					@BeforeTransAmt
					,--@OpeningBalance                               
					@AfterTransAmt
					,--@ClosingBalance                              
					0 --@TransactionAmount                                                           
					,@benefitCommission
					,--@DiscountAmount                              
					''
					,--@AmountCreditDebit                          
					'CREDIT'
					,--@CommissionCreditDebit                          
					@TransDesc --@TransactionDescription                              
			END
		END TRY

		BEGIN CATCH
			RETURN
		END CATCH
	END
END


GO
/****** Object:  StoredProcedure [dbo].[UpdateRequestID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[UpdateRequestID] @RequestID VARCHAR(100)
	,@ResponseRequestID VARCHAR(100)
AS
BEGIN
	UPDATE Recharge
	SET RequestID = @ResponseRequestID
	WHERE RequestID = @RequestID
END



GO
/****** Object:  StoredProcedure [dbo].[UpdateValidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateValidation] ---3,'E'
	@userID INT
	,@Type VARCHAR(10)
AS
BEGIN
	DECLARE @DOC DATETIME;
	DECLARE @NewDate DATETIME;
	DECLARE @UpDOC DATETIME;
	DECLARE @DATE VARCHAR(350);

	SET @DOC = (
			SELECT switchoffset(sysdatetimeoffset(), '+05:30')
			);
	SET @UpDOC = (
			SELECT TOP (1) ModifiedDOC
			FROM UpdateUserInformation
			WHERE UserID = @userID
				AND Type = @Type
			)
	SET @NewDate = (
			SELECT datediff(D, (@DOC), (@UpDOC))
			)

	IF (
			(
				SELECT datediff(D, (@UpDOC), (@DOC))
				) < 30
			)
	BEGIN
		SET @DATE = (
				SELECT DATEADD(day, 30, @UpDOC)
				);

		SELECT @DATE AS UpdateDate
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserAccessControlSelectValidAccess]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserAccessControlSelectValidAccess] @PageName VARCHAR(50)
	,@UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT

	SELECT @UserTypeID = UserTypeID
	FROM UserInformation
	WHERE UserID = @UserID

	SELECT UserAccessID
	FROM UserAccessControl
	WHERE PageName = @PageName
		AND UserTypeID = @UserTypeID
		AND IsAccess = 1
END



GO
/****** Object:  StoredProcedure [dbo].[UserAccessControlSupportSelectValidAccess]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserAccessControlSupportSelectValidAccess] @PageName VARCHAR(50)
	,@UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT
	DECLARE @SupportUserTypeID INT = 9
	SELECT @UserTypeID = UserTypeID
	FROM UserInformation
	WHERE UserID = @UserID
	PRINT @UserTypeID
	IF @UserTypeID = @SupportUserTypeID
	BEGIN
		DECLARE @Pageount INT
		SELECT @Pageount = PageID
		FROM [dbo].[SupportUserAccessControlMaster]
		WHERE PageName = @PageName
			AND IsOn = 1
		PRINT @Pageount
		IF (@Pageount IS NULL)
		BEGIN
			DECLARE @PageAdminAccessCount INT
			SELECT @PageAdminAccessCount = [UserAccessID]
			FROM [UserAccessControl]
			WHERE [PageName] = @PageName
				AND [UserTypeID] = 1
				AND [IsAccess] = 1
			IF (@PageAdminAccessCount IS NULL)
			BEGIN
				RETURN
			END
			ELSE
			BEGIN
				SELECT 1 AS ID
				RETURN
			END
		END
		SELECT ID
		FROM [SupportUserAccessControl]
		WHERE PageName = @PageName
			AND [UserID] = @UserID
			AND [IsOn] = 1
	END
END

GO
/****** Object:  StoredProcedure [dbo].[UserAccessSupportPagesSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--UserAccessSupportPagesSelect 2325    
CREATE PROCEDURE[dbo].[UserAccessSupportPagesSelect] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT;

	SELECT @UserTypeID = UserTypeID
	FROM dbo.UserInformation
	WHERE UserID = @UserID
		AND IsActive = 1

	SELECT *
	FROM (
		SELECT *
			,ROW_NUMBER() OVER (
				PARTITION BY [PageID] ORDER BY tag DESC
				) AS SNN
		FROM (
			SELECT [PageID]
				,[PageName]
				,0 AS [IsOn]
				,0 AS tag
			FROM [dbo].[SupportUserAccessControlMaster](NOLOCK)
			
			UNION
			
			SELECT [PageID]
				,[PageName]
				,[IsOn]
				,1 AS tag
			FROM [dbo].[SupportUserAccessControl](NOLOCK)
			WHERE UserID = @UserID
			) AS SubTab
		) AS TabMain
	WHERE SNN = 1
END



GO
/****** Object:  StoredProcedure [dbo].[UserBalanceSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UserBalanceSelect] @UserID INT
AS
BEGIN
	SELECT isnull(UB.CurrentBalance, 0) AS CurrentBalance
		,isnull(UB.DMRBalance, 0) AS DMRBalance
		,isnull(UB.MinimumBalance, 0) AS MinimumBalance
		,isnull(UB.[Outstanding], 0) [Outstanding]
		,UI.EmailID
		,UI.IsActive
	FROM UserBalance UB(NOLOCK)
	INNER JOIN [dbo].UserInformation UI(NOLOCK) ON UI.UserID = UB.UserID
	WHERE UB.UserID = @UserID
		AND UI.IsActive = 1
END



GO
/****** Object:  StoredProcedure [dbo].[UserBalanceSelectReceiverID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[UserBalanceSelectReceiverID] @ReceiverID INT  
AS  
BEGIN  
 SELECT [CurrentBalance] AS RechargeBalance  
  ,[DMRBalance] AS DMRBalance  
  ,[Outstanding] As Outstanding
 FROM UserBalance  
 WHERE [UserID] = @ReceiverID  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[UserBalanceUpdateMinimumBalance]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================  
--Author: Priynaka Deshmukh
--Date :19-4-2017     
--Description:User Balance Update Minimum Balance
--========================== 
--UserBalanceUpdateMinimumBalance 71,123
CREATE PROCEDURE [dbo].[UserBalanceUpdateMinimumBalance] @UserId INT
	,@MinimumBalance DECIMAL
AS
BEGIN
	UPDATE UserBalance
	SET MinimumBalance = @MinimumBalance
	WHERE UserId = @UserId
END



GO
/****** Object:  StoredProcedure [dbo].[UserCommissionDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================
--Auther :Priyanka
--Date :10-2-17
--Purpose UserCommission Delete 
--==========================================
CREATE PROCEDURE [dbo].[UserCommissionDelete] @UserId INT
AS
BEGIN
	DELETE UserCommission
	WHERE UserID = @UserId
END



GO
/****** Object:  StoredProcedure [dbo].[UserCommissionDeleteDownline]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================  
--Auther :Priyanka  
--Date :10-2-17  
--Purpose UserCommission Delete Downline  
--==========================================  
CREATE PROCEDURE [dbo].[UserCommissionDeleteDownline] @UserId INT
AS
BEGIN
	DELETE UserCommission
	WHERE UserID IN (
			SELECT UserID
			FROM UserInformation
			WHERE ParentID = @UserId
			)
END



GO
/****** Object:  StoredProcedure [dbo].[UserCommissionSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================        
--Auther :Sainath Bhujbal        
--Date :24-2-17        
--Purpose User Commission Select First Else union of  default commission       
--==========================================        
--[UserCommissionSelect] 2  
CREATE PROCEDURE[dbo].[UserCommissionSelect] @userID INT
AS
BEGIN
	SELECT *
	FROM (
		SELECT *
			,ROW_NUMBER() OVER (
				PARTITION BY OperatorID ORDER BY OperatorID ASC
				) AS SNN
		FROM (
			SELECT OM.OperatorID
				,OM.OperatorName
				,UC.RetailerCommissionAmount
				,UC.DistributorCommissionAmount
				,UC.SuperDistributorCommissionAmount
				,UC.IsOn
				,---(IsNULL(UC.SuperDistributorCommissionAmount, 0)+IsNULL(UC.DistributorCommissionAmount, 0) + IsNULL(UC.RetailerCommissionAmount, 0)) as 
				UC.TotalCommissionAmount AS TotalCommission
				,UC.MarginType
				,SM.ServiceName
			FROM [dbo].UserCommission UC(NOLOCK)
			INNER JOIN [dbo].OperatorMaster OM(NOLOCK) ON UC.OperatorID = OM.OperatorID
			INNER JOIN [dbo].ServiceMaster SM(NOLOCK) ON SM.ServiceID = OM.ServiceID
			WHERE UC.UserID = @userID
			
			UNION
			
			SELECT OM.OperatorID
				,OM.OperatorName
				,OM.RetailerCommissionAmount
				,OM.DistributorCommissionAmount
				,OM.SuperDistributorCommissionAmount
				,OM.IsOn
				,(IsNULL(OM.SuperDistributorCommissionAmount, 0) + IsNULL(OM.DistributorCommissionAmount, 0) + IsNULL(OM.RetailerCommissionAmount, 0)) AS TotalCommission
				,OM.MarginType
				,SM.ServiceName
			FROM dbo.OperatorMaster OM(NOLOCK)
			INNER JOIN dbo.ServiceMaster SM(NOLOCK) ON SM.ServiceID = OM.ServiceID
			WHERE OM.OperatorID NOT IN (
					SELECT UC.OperatorID
					FROM UserCommission UC
					WHERE UC.UserID = @userID
					)
			) AS tabMain
		) AS outerTab
	WHERE SNN = 1
	ORDER BY OperatorID
		--  select OM.OperatorID ,OM.OperatorName, UC.RetailerCommissionAmount, UC.DistributorCommissionAmount,
		--  UC.IsOn, (IsNULL(UC.DistributorCommissionAmount, 0) + IsNULL(UC.RetailerCommissionAmount, 0)) as TotalCommission, UC.MarginType,   SM.      
		--ServiceName     
		--  from UserCommission UC        
		--  inner join OperatorMaster OM on UC.OperatorID = OM.OperatorID        
		--  inner join ServiceMaster SM on SM.ServiceID = OM.ServiceID        
		--  where UC.UserID =@userID 
		--UNION     
		-- select OM.OperatorID, OM.OperatorName, OM.RetailerCommissionAmount, OM.DistributorCommissionAmount, 
		-- OM.IsOn, (IsNULL(OM.DistributorCommissionAmount, 0) + IsNULL(OM.RetailerCommissionAmount, 0)) as TotalCommission, OM.MarginType, SM.ServiceName        
		--  from dbo.OperatorMaster OM        
		--  inner join dbo.ServiceMaster SM on SM.ServiceID = OM.ServiceID     
		-- except    
		--  select OM.OperatorID ,OM.OperatorName, UC.RetailerCommissionAmount, UC.DistributorCommissionAmount,
		--  UC.IsOn, (IsNULL(UC.DistributorCommissionAmount, 0) + IsNULL(UC.RetailerCommissionAmount, 0)) as TotalCommission, UC.MarginType,   SM.      
		--ServiceName     
		--  from UserCommission UC        
		--  inner join OperatorMaster OM on UC.OperatorID = OM.OperatorID        
		--  inner join ServiceMaster SM on SM.ServiceID = OM.ServiceID        
		--  where UC.UserID =@userID   )  
		--   order by  OperatorID      
END



GO
/****** Object:  StoredProcedure [dbo].[UserCommissionSelectByOperatorIDAndUserID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[UserCommissionSelectByOperatorIDAndUserID] @UserID INT
	,@OperatorID INT
AS
BEGIN
	SELECT DistributorCommissionAmount AS DistributorCommission
	FROM UserCommission
	WHERE UserID = @UserID
		AND OperatorID = @OperatorID
END



GO
/****** Object:  StoredProcedure [dbo].[UserCommissionSelectByService]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================              
--Auther :Sainath Bhujbal              
--Date :16-6-17              
--Purpose User Commission Structure Select By Service Id             
--==========================================              
--[UserCommissionSelectByService] 1065 ,0
CREATE PROCEDURE[dbo].[UserCommissionSelectByService] @userID INT
	,@ServiceID INT
AS
BEGIN
	DECLARE @DMRServiceID INT = 21

	IF (@ServiceID = 0)
	BEGIN
		SELECT *
		FROM (
			SELECT *
				,ROW_NUMBER() OVER (
					PARTITION BY OperatorID ORDER BY OperatorID ASC
					) AS SNN
			FROM (
				SELECT OM.OperatorID
					,OM.OperatorName
					,RGC.RetailerCommissionAmount
					,RGC.DistributorCommissionAmount
					,RGC.IsOn
					,(IsNULL(RGC.DistributorCommissionAmount, 0) + IsNULL(RGC.RetailerCommissionAmount, 0)) AS TotalCommission
					,RGC.MarginType
					,SM.ServiceName
					,SM.ServiceID
				FROM [dbo].[RechargeGroupMaster] RGM
				INNER JOIN [dbo].[RechargeGroupCommission] RGC ON RGC.[GroupID] = RGM.[GroupID]
				INNER JOIN OperatorMaster OM ON RGC.[OperatorID] = OM.OperatorID
				INNER JOIN ServiceMaster SM ON SM.ServiceID = OM.ServiceID
				INNER JOIN [dbo].[UserGroupMapping] UGM ON UGM.[GroupID] = RGC.[GroupID]
				WHERE UGM.UserID = @userID
					AND SM.ServiceID != @DMRServiceID
					AND OM.IsDelete = 0
				
				UNION
				
				SELECT OM.OperatorID
					,OM.OperatorName
					,OM.RetailerCommissionAmount
					,OM.DistributorCommissionAmount
					,OM.IsOn
					,(IsNULL(OM.DistributorCommissionAmount, 0) + IsNULL(OM.RetailerCommissionAmount, 0)) AS TotalCommission
					,OM.MarginType
					,SM.ServiceName
					,SM.ServiceID
				FROM dbo.OperatorMaster OM
				INNER JOIN dbo.ServiceMaster SM ON SM.ServiceID = OM.ServiceID
					AND SM.ServiceID != @DMRServiceID
					AND OM.IsDelete = 0
				) AS tabMain
			) AS outerTab
		WHERE SNN = 1
		ORDER BY ServiceID
			,OperatorID
	END
	ELSE
	BEGIN
		SELECT *
		FROM (
			SELECT *
				,ROW_NUMBER() OVER (
					PARTITION BY OperatorID ORDER BY OperatorID ASC
					) AS SNN
			FROM (
				SELECT OM.OperatorID
					,OM.OperatorName
					,RGC.RetailerCommissionAmount
					,RGC.DistributorCommissionAmount
					,RGC.IsOn
					,(IsNULL(RGC.DistributorCommissionAmount, 0) + IsNULL(RGC.RetailerCommissionAmount, 0)) AS TotalCommission
					,RGC.MarginType
					,SM.ServiceName
					,SM.ServiceID
				FROM [dbo].[RechargeGroupMaster] RGM
				INNER JOIN [dbo].[RechargeGroupCommission] RGC ON RGC.[GroupID] = RGM.[GroupID]
				INNER JOIN OperatorMaster OM ON RGC.[OperatorID] = OM.OperatorID
				INNER JOIN ServiceMaster SM ON SM.ServiceID = OM.ServiceID
				INNER JOIN [dbo].[UserGroupMapping] UGM ON UGM.[GroupID] = RGC.[GroupID]
				WHERE UGM.UserID = @userID
					AND OM.IsDelete = 0
					AND OM.ServiceID = @ServiceID
					AND SM.ServiceID != @DMRServiceID
				
				UNION
				
				SELECT OM.OperatorID
					,OM.OperatorName
					,OM.RetailerCommissionAmount
					,OM.DistributorCommissionAmount
					,OM.IsOn
					,(IsNULL(OM.DistributorCommissionAmount, 0) + IsNULL(OM.RetailerCommissionAmount, 0)) AS TotalCommission
					,OM.MarginType
					,SM.ServiceName
					,SM.ServiceID
				FROM dbo.OperatorMaster OM
				INNER JOIN dbo.ServiceMaster SM ON SM.ServiceID = OM.ServiceID
				WHERE OM.ServiceID = @ServiceID
					AND SM.ServiceID != @DMRServiceID
					AND OM.IsDelete = 0
				) AS tabMain
			) AS outerTab
		WHERE SNN = 1
		ORDER BY ServiceID
			,OperatorID
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserCommissionUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================    
--Auther :Priyanka    
--Date :24-2-17    
--Purpose User Commission Update    
--==========================================    
CREATE PROCEDURE[dbo].[UserCommissionUpdate] @RetailerComm DECIMAL(18, 5)
	,@TotalComm DECIMAL(18, 5)
	,@OperatorId INT
	,@Type VARCHAR(20)
	,@UserId INT
	,@IsActive INT
	,@DistComm DECIMAL(18, 5)
AS
BEGIN
	IF NOT EXISTS (
			SELECT UserID
				,OperatorID
			FROM dbo.UserCommission
			WHERE UserID = @UserId
				AND OperatorID = @OperatorId
			)
	BEGIN
		INSERT INTO dbo.UserCommission (
			UserID
			,OperatorID
			,RetailerCommissionAmount
			,DistributorCommissionAmount
			,SuperDistributorCommissionAmount
			,TotalCommissionAmount
			,MarginType
			,IsOn
			)
		VALUES (
			@UserId
			,@OperatorId
			,@RetailerComm
			,@DistComm
			,0.00
			,@TotalComm
			,@Type
			,@IsActive
			);
	END
	ELSE
	BEGIN
		UPDATE UserCommission
		SET RetailerCommissionAmount = @RetailerComm
			,DistributorCommissionAmount = @DistComm
			,SuperDistributorCommissionAmount = 0.00
			,MarginType = @Type
			,IsOn = @IsActive
		WHERE OperatorID = @OperatorId
			AND UserID = @UserId
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserCommissionUpdateComm]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE[dbo].[UserCommissionUpdateComm] @RetailerComm NUMERIC(18, 5)
	,@TotalComm NUMERIC(18, 5)
	,@OperatorId INT
	,@Type VARCHAR(10)
	,@UserId INT
	,@IsActive INT
	,@DistComm NUMERIC(18, 5)
AS
BEGIN
	IF EXISTS (
			SELECT [UserID]
			FROM [UserCommission]
			WHERE [UserID] = @UserId
				AND [OperatorID] = @OperatorId
			)
	BEGIN
		UPDATE [UserCommission]
		SET [MarginType] = @Type
			,[RetailerCommissionAmount] = @RetailerComm
			,[DistributorCommissionAmount] = @DistComm
			,[TotalCommissionAmount] = @TotalComm
			,[IsOn] = @IsActive
		WHERE [UserID] = @UserId
			AND [OperatorID] = @OperatorId
	END
	ELSE
	BEGIN
		INSERT INTO [UserCommission] (
			[UserID]
			,[OperatorID]
			,[MarginType]
			,[RetailerCommissionAmount]
			,[DistributorCommissionAmount]
			,[TotalCommissionAmount]
			,[IsOn]
			)
		VALUES (
			@UserId
			,@OperatorId
			,@Type
			,@RetailerComm
			,@DistComm
			,@TotalComm
			,@IsActive
			);
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserGroupMappingDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--======================================    
--Author : Rahul Hembade    
--Date : 16 may 2018    
--Purpose :delete User group    
--======================================    
CREATE PROCEDURE[dbo].[UserGroupMappingDelete] @GroupId INT
	,@UserId INT
AS
BEGIN
	DELETE
	FROM UserGroupMapping
	WHERE UserId = @UserId
		AND GroupID = @GroupId
END



GO
/****** Object:  StoredProcedure [dbo].[UserGroupMappingInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--======================================
--Author : Rahul Hembade
--Date : 16 May 2018
--Purpose :Map User group if exists update else insert
--======================================
-- [UserGroupMappingInsert] 1, 3519
CREATE PROC [dbo].[UserGroupMappingInsert] @GroupId INT
	,@UserId INT
AS
BEGIN
	DECLARE @SuperDistID INT = 8;
	DECLARE @DistID INT = 2;
	DECLARE @RetID INT = 1;

	IF (
			@GroupId = NULL
			OR @GroupId = 0
			)
	BEGIN
		RETURN
	END

	-- get usertypeID
	DECLARE @UserTypeID INT;

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserId
			)

	PRINT @UserTypeID

	CREATE TABLE #tblRetailer (UserID INT)

	CREATE TABLE #tblDist (UserID INT)

	-- if SD then get Dist and its retailer
	IF (@UserTypeID = @SuperDistID)
	BEGIN
		INSERT INTO #tblDist (UserID)
		SELECT UserID
		FROM UserInformation
		WHERE ParentID = @UserId

		INSERT INTO #tblRetailer (UserID)
		SELECT UserID
		FROM #tblDist

		INSERT INTO #tblRetailer (UserID)
		SELECT UI.UserID
		FROM #tblDist Dist
		INNER JOIN UserInformation AS UI ON UI.ParentID = Dist.UserID
		WHERE UI.ParentID = Dist.UserID
	END

	--if Dist then get its retailer
	IF (@UserTypeID = @DistID)
	BEGIN
		INSERT INTO #tblRetailer (UserID)
		SELECT UserID
		FROM UserInformation
		WHERE ParentID = @UserId
	END

	--Insert Own ID in tbl retailer
	INSERT INTO #tblRetailer (UserID)
	SELECT @UserID

	DELETE
	FROM UserGroupMapping
	WHERE UserID IN (
			SELECT UserID
			FROM #tblRetailer
			WHERE UserID NOT IN (
					SELECT UserID
					FROM UserGroupMapping
					WHERE [ExplicitGroup] = 1
					)
			)

	PRINT 'delete user'

	INSERT INTO [dbo].UserGroupMapping (
		UserId
		,GroupID
		)
	SELECT UserID
		,@GroupId
	FROM #tblRetailer
	WHERE UserID NOT IN (
			SELECT UserID
			FROM UserGroupMapping
			WHERE [ExplicitGroup] = 1
			)
END



GO
/****** Object:  StoredProcedure [dbo].[UserGroupMappingInsertWithDownline]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[UserGroupMappingInsertWithDownline] @UserId INT
	,@GroupId INT
AS
BEGIN
	create  TABLE #tempID (TempUserID INT)

	--,TempGroupID int
	INSERT INTO #tempID (TempUserID)
	SELECT UserID AS TempUserID
	FROM userinformation
	WHERE parentid = @UserID
		OR userid = @UserID

	INSERT INTO #tempID (TempUserID)
	SELECT UserID AS TempUserID
	FROM userinformation
	WHERE parentid IN (
			SELECT TempUserID
			FROM #tempID
			)

	DECLARE @tttt INT;

	SELECT @tttt = TempUserID
	FROM #tempID

	INSERT INTO UserGroupMapping (
		UserID
		,GroupID
		)
	VALUES (
		@tttt
		,@GroupId
		)
END



GO
/****** Object:  StoredProcedure [dbo].[UserGroupMappingSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--======================================  
--Author : Rahul Hembade  
--Date : 16 may 2018  
--Purpose :Select Users from  group   
--======================================  
CREATE PROCEDURE[dbo].[UserGroupMappingSelect]
AS
BEGIN
	SELECT UG.UserId
		,UG.GroupID
		,UI.UserName
		,RGM.GroupName
	FROM UserGroupMapping UG
	INNER JOIN UserInformation UI ON UG.UserID = UI.UserID
	INNER JOIN RechargeGroupMaster RGM ON UG.GroupID = RGM.GroupID
END



GO
/****** Object:  StoredProcedure [dbo].[UserGroupMappingSelectUserGroupID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--ALTER d: Rahul Hembade
--Date:13May2018
CREATE PROCEDURE[dbo].[UserGroupMappingSelectUserGroupID] @UserId INT
AS
BEGIN
	SELECT GroupID
	FROM UserGroupMapping
	WHERE UserId = @UserId
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationActDeactMember]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================
--Author: Priyanka Deshmukh
--Date :11-2-2017   
--Description:UserInformation ActDeact Member
--==========================
CREATE PROCEDURE [dbo].[UserInformationActDeactMember] @UserID INT
	,@IsActive INT
	,@IsWithDownline INT
AS
BEGIN
	IF @IsWithDownline = 1
	BEGIN
		UPDATE dbo.UserInformation
		SET Isactive = @IsActive
		WHERE UserID = @UserID

		UPDATE dbo.UserInformation
		SET Isactive = @IsActive
		WHERE ParentId = @UserID
	END
	ELSE
	BEGIN
		UPDATE dbo.UserInformation
		SET Isactive = @IsActive
		WHERE UserID = @UserID
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationAddAPIToken]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[UserInformationAddAPIToken]  
@UserID int,  
@APIToken varchar(1000)  
as   
begin  
 Update UserInformation  
 set Token = @APIToken  
 where UserID = @UserID  
end
GO
/****** Object:  StoredProcedure [dbo].[UserInformationCustomerLogin]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================        
--Auther :Priyanka        
--Date :9-2-17        
--Purpose UserInformation Validation        
--==========================================        
--UserInformationLogin '8888888888','sai'        
CREATE PROCEDURE [dbo].[UserInformationCustomerLogin] @UserID INT
	,@OTP VARCHAR(50)
AS
BEGIN
	SELECT UI.UserName
		,UI.UserID
		,UI.ParentID
		,UI.MobileNumber
		,UI.EmailID
		,UB.CurrentBalance
		,UT.UserType
		,UT.UserTypeID
		,UI.name
		,UI.RechargePin
	FROM dbo.UserInformation UI
	INNER JOIN dbo.UserBalance UB ON UI.UserID = UB.UserID
	INNER JOIN dbo.UserTypeMaster UT ON UI.UserTypeID = UT.UserTypeID
	WHERE (
			UI.UserID = @UserID
			AND OTP = @OTP
			)
		AND UI.UserTypeID IN (6)
		AND ISACTIVE = 1
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[UserInformationDelete] @MobileNo VARCHAR(100)  
 ,@EmailId VARCHAR(500)  
AS  
BEGIN  
 DECLARE @UserID INT;  

 SET @UserID = (  
   SELECT UserID  
   FROM UserInformation  
   WHERE MobileNumber = @MobileNo  
    OR EmailId = @EmailId  
    AND IsActive = 0  
    AND UserTypeID = 6  
   );  
  
 DELETE  
 FROM UserBalance  
 WHERE UserID = @UserID;  
  
 DELETE  
 FROM UserInformation  
 WHERE UserID = @UserID;  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[UserInformationDeleteMember]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[UserInformationDeleteMember] @UserID INT  
AS  
BEGIN  
 return;

 DELETE  
 FROM UserBalance  
 WHERE userid = @UserID  
  
 DELETE  
 FROM UserGroupMapping  
 WHERE userid = @UserID  
  
 DELETE  
 FROM userinformation  
 WHERE userid = @UserID  
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[UserInformationInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserInformationInsert] @Name VARCHAR(50)    
 ,@ShopeName VARCHAR(100)    
 ,@State Int = null  
 ,@UserName VARCHAR(50)    
 ,@Password VARCHAR(50)    
 ,@Address VARCHAR(max)    
 ,@MobileNo VARCHAR(50)    
 ,@EmailId VARCHAR(50)    
 ,@PAN VARCHAR(20)    
 ,@UserTypeId INT    
 ,@ParentID INT    
 ,@DateOfBirth VARCHAR(20)    
 ,@RechargePin INT    
 ,@Gender VARCHAR(20)    
 ,@OTP VARCHAR(50)    
 ,@IsActive INT    
 ,@Adhar VARCHAR(25)    
 ,@Gst VARCHAR(25)    
 ,@GroupId INT = NULL    
 --,@OtoMaxID varchar(500)                
 ,@CallBackUrl VARCHAR(1000)    
 ,@IP VARCHAR(30)    
 ,@DMRGroupId INT = NULL    
 ,@ParentFOS INT = NULL    
AS    
BEGIN    
 DECLARE @checkParentId INT = 0;    
 SELECT TOP 1 @checkParentId = userid    
 FROM UserInformation    
 WHERE UserID = @ParentId    
 IF (@UserTypeId = 1)    
 BEGIN    
  RETURN;    
 END    
 IF (    
   @checkParentId IS NULL    
   OR @checkParentId <= 0    
   )    
 BEGIN    
  RETURN;    
 END    
 DECLARE @currentUserId INT    
 DECLARE @pid NUMERIC(4, 0)    
 SET @currentUserId = 0;    
 DECLARE @AdminUserTypeId INT = 1    
 IF (@UserTypeId = @AdminUserTypeId)    
 BEGIN    
  RETURN - 1;    
 END    
 INSERT INTO dbo.UserInformation (    
  [Name]    
  ,[ShopeName]    
  ,[UserName]    
  ,[Address]    
  ,[StateID]  
  ,[Password]    
  ,[Emailid]    
  ,MobileNumber    
  ,UserTypeID    
  ,ParentID    
  ,PAN    
  ,DateOfBirth    
  ,RechargePin    
  ,Gender    
  ,OTP    
  ,IsActive    
  ,Adhar    
  ,GST    
  ,CallBackUrl    
  ,IP    
  ,ParentFOS    
  )    
 --,OtoMaxID                
 VALUES (    
  @Name    
  ,@ShopeName    
  ,@UserName    
  ,@Address   
  ,@State   
  ,@Password    
  ,@EmailId    
  ,@MobileNo    
  ,@UserTypeId    
  ,@ParentID    
  ,@PAN    
  ,convert(DATE, @DateOfBirth, 103)    
  ,@RechargePin    
  ,@Gender    
  ,@OTP    
  ,@IsActive    
  ,@Adhar    
  ,@Gst    
  ,@CallBackUrl    
  ,@IP    
  ,@ParentFOS    
  )    
 --,@OtoMaxID                
 SET @currentUserId = (    
   SELECT @@identity    
   )    
 PRINT 'User info working : userid '    
 IF (@currentUserId IS NULL)    
 BEGIN    
  RETURN - 1    
 END    
 IF (@currentUserId <= 0)    
 BEGIN    
  RETURN - 1    
 END    
 PRINT @currentUserId    
 INSERT [dbo].UserBalance (    
  UserID    
  ,CurrentBalance    
  ,DMRBalance    
  )    
 VALUES (    
  @currentUserId    
  ,0    
  ,0    
  )    
 PRINT 'UserBalance working'    
 DECLARE @ParentsUserTypeID INT    
 DECLARE @ParentGroup INT    
 SELECT @ParentsUserTypeID = usertypeid    
 FROM UserInformation    
 WHERE UserID = @ParentID    
 SELECT @ParentGroup = GroupID    
 FROM UserGroupMapping    
 WHERE UserID = @ParentID    
 DECLARE @isGroupExists INT    
 SELECT @isGroupExists = GroupID    
 FROM RechargeGroupMaster    
 WHERE GroupID = @GroupId    
 --- Recharge Grroup                  
 IF (    
   @GroupId IS NOT NULL    
   AND @GroupId != 0    
   AND @isGroupExists IS NOT NULL    
   )    
 BEGIN    
  PRINT '@GroupId :' + convert(VARCHAR(40), @GroupId)    
  INSERT INTO UserGroupMapping (    
   UserId    
   ,GroupID    
   )    
  VALUES (    
   @currentUserId    
   ,@GroupId    
   )    
  PRINT 'UserGroupMapping working'    
 END    
 ELSE IF (    
   @ParentGroup IS NOT NULL    
   AND @ParentGroup != 0    
   )    
 BEGIN    
  INSERT INTO UserGroupMapping (    
   UserId    
   ,GroupID    
   )    
  VALUES (    
   @currentUserId    
   ,@ParentGroup    
   )    
  PRINT 'UserGroupMapping working : else part'    
 END    
 DECLARE @isDMRGroupExists INT    
 SELECT @isDMRGroupExists = ID    
 FROM [dbo].[GroupMaster]    
 WHERE ID = @DMRGroupId    
 DECLARE @ParentDMRGroup INT    
 SELECT @ParentDMRGroup = GroupID    
 FROM [UserwiseSlabCommission]    
 WHERE UserID = @ParentID    
 ---- For DMR Group              
 IF (    
   @DMRGroupId IS NOT NULL    
   AND @DMRGroupId != 0    
   AND @isDMRGroupExists IS NOT NULL    
   )    
 BEGIN    
  INSERT INTO [UserwiseSlabCommission] (    
   UserId    
   ,GroupID    
   )    
  VALUES (    
   @currentUserId    
   ,@DMRGroupId    
   )    
  PRINT '@DMRGroupId working : if part'     END    
 ELSE IF (    
   @ParentDMRGroup IS NOT NULL    
   AND @ParentDMRGroup != 0    
   )    
 BEGIN    
  INSERT INTO [dbo].[UserwiseSlabCommission] (    
   [UserID]    
   ,[GroupID]    
   )    
  VALUES (    
   @currentUserId    
   ,@ParentDMRGroup    
   )    
  PRINT '@DMRGroupId working : else part'    
 END    
 DECLARE @Output INT;    
 SET @Output = @currentUserId    
 RETURN @Output;    
END 

GO
/****** Object:  StoredProcedure [dbo].[UserinformationInsertKycFlag]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[UserinformationInsertKycFlag] @UserID INT
	,@ApprovedBy INT
AS
BEGIN
	UPDATE [UserInformation]
	SET [IsKYCApproved] = 1
		,[KYCApprovedBy] = @ApprovedBy
		,[KYCDate] = switchoffset(sysdatetimeoffset(), '+05:30')
	WHERE [UserID] = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationLogin]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================                    
--Modification :Rahul Hembade                   
--Date :9-2-17                    
--Purpose: added DMR Balance for only Partner user               
--==========================================                    
--UserInformationLogin 'shREEtelecomlsg@gmail.com','1234',''              
--UserInformationLogin '9067397680','757671','Partner'                    
CREATE  PROCEDURE [dbo].[UserInformationLogin] @userName VARCHAR(50)      
 ,@Password VARCHAR(50)      
 ,@userType VARCHAR(50) = 'Partner'      
AS      
BEGIN      
 SET @userType = 'Partner';      
      
 DECLARE @freeUserType INT = 6;      
 DECLARE @EndDate VARCHAR(100);      
      
 SELECT @EndDate = [EndDate]      
 FROM [dbo].[Subscription]      
      
 IF (Upper(@userType) = upper('Partner'))      
 BEGIN      
  SELECT DATEDIFF(day, @EndDate, getdate()) AS ENDSUBSCRIPTION      
   ,UI.UserName      
   ,UI.UserID      
   ,UI.ParentID      
   ,UI.MobileNumber      
   ,UI.EmailID      
   ,UB.CurrentBalance      
   ,UB.Outstanding      
   ,dbo.GetOutStandingBand(UI.UserID) As Band  
   ,UB.DMRBalance      
   ,UT.UserType      
   ,UT.UserTypeID      
   ,UI.name      
   ,UP.UserName AS ParentName      
   ,UI.IP      
   ,ISNULL(UI.IPActive, 0) AS IPActive      
  FROM dbo.UserInformation UI      
  INNER JOIN dbo.UserBalance UB ON UI.UserID = UB.UserID      
  INNER JOIN UserInformation UP ON UP.UserID = UI.ParentID      
  INNER JOIN dbo.UserTypeMaster UT ON UI.UserTypeID = UT.UserTypeID      
  WHERE (      
    UI.EmailID = @userName COLLATE SQL_Latin1_General_CP1_CS_AS      
    OR UI.MobileNumber = @userName COLLATE SQL_Latin1_General_CP1_CS_AS      
    OR UI.UserName = @userName COLLATE SQL_Latin1_General_CP1_CS_AS      
    )      
   AND UI.UserTypeID NOT IN (@freeUserType)      
   AND UI.Password = @Password COLLATE SQL_Latin1_General_CP1_CS_AS      
   AND UI.ISACTIVE = 1      
 END      
 ELSE      
 BEGIN      
  SELECT UI.UserName      
   ,UI.UserID      
   ,UI.ParentID      
   ,UI.MobileNumber      
   ,UI.EmailID      
   ,UB.CurrentBalance      
   ,UB.Outstanding      
   ,UT.UserType      
   ,UT.UserTypeID      
   ,UI.name      
   ,UI.RechargePin      
   ,UP.UserName AS ParentName      
   ,UI.IP      
   ,ISNULL(UI.IPActive, 0) AS IPActive      
  FROM dbo.UserInformation UI      
  INNER JOIN dbo.UserBalance UB ON UI.UserID = UB.UserID      
  INNER JOIN UserInformation UP ON UP.UserID = UI.ParentID      
  INNER JOIN dbo.UserTypeMaster UT ON UI.UserTypeID = UT.UserTypeID      
  WHERE (      
    UI.EmailID = @userName COLLATE SQL_Latin1_General_CP1_CS_AS      
    OR UI.MobileNumber = @userName COLLATE SQL_Latin1_General_CP1_CS_AS      
    OR UI.UserName = @userName COLLATE SQL_Latin1_General_CP1_CS_AS      
    )      
   AND UI.UserTypeID IN (@freeUserType)      
   AND (UI.Password = @Password COLLATE SQL_Latin1_General_CP1_CS_AS)      
   AND UI.ISACTIVE = 1      
 END      
END 
GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================    
--Auther :Priyanka    
--Date :27-2-17    
--Purpose UserInformation Select    
--Modifay:Rahul hembade    
--Reson:add type gor DMRBalance    
--==========================================    
---UserInformationSelect  1,'Recharge'    
CREATE PROCEDURE [dbo].[UserInformationSelect] @UserID INT
	,@BalanceType VARCHAR(20)
AS
BEGIN
	IF (upper(@BalanceType) = upper('Recharge'))
	BEGIN
		SELECT UI.UserID
			,UI.UserName
			,UI.MobileNumber
			,isnull(ub.CurrentBalance, 0) AS CurrentBalance
			,isnull(ub.Outstanding, 0) AS Outstanding
			,UI.ParentID AS ParentID
			,UP.UserName AS ParentName
			,UI.name
			,UIT.UserType
			,UPT.UserType AS DUserType
			,UP.MobileNumber AS PMobileNumber
		FROM [dbo].UserInformation(NOLOCK) AS UI
		INNER JOIN [dbo].UserInformation(NOLOCK) AS UP ON UI.ParentID = UP.UserID
		LEFT JOIN [dbo].UserBalance(NOLOCK) AS ub ON ub.UserID = UI.UserID
		INNER JOIN [dbo].UserTypeMaster(NOLOCK) AS UIT ON UI.UserTypeID = UIT.UserTypeID
		INNER JOIN [dbo].UserTypeMaster(NOLOCK) AS UPT ON UP.UserTypeID = UPT.UserTypeID
		WHERE UI.UserID = @UserID
			AND UI.IsActive = 1
	END

	IF (upper(@BalanceType) = upper('DMT'))
	BEGIN
		SELECT UI.UserID
			,UI.UserName
			,UI.MobileNumber
			-- for DMRBalance     
			,isnull(ub.DMRBalance, 0) AS CurrentBalance
			,isnull(ub.Outstanding, 0) AS Outstanding
			,UI.ParentID AS ParentID
			,UP.UserName AS ParentName
			,UI.name
			,UIT.UserType
			,UPT.UserType AS DUserType
			,UP.MobileNumber AS PMobileNumber
		FROM [dbo].UserInformation(NOLOCK) AS UI
		INNER JOIN [dbo].UserInformation(NOLOCK) AS UP ON UI.ParentID = UP.UserID
		LEFT JOIN [dbo].UserBalance(NOLOCK) AS ub ON ub.UserID = UI.UserID
		INNER JOIN [dbo].UserTypeMaster(NOLOCK) AS UIT ON UI.UserTypeID = UIT.UserTypeID
		INNER JOIN [dbo].UserTypeMaster(NOLOCK) AS UPT ON UP.UserTypeID = UPT.UserTypeID
		WHERE UI.UserID = @UserID
			AND UI.IsActive = 1
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectAllUser]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Created: Rahul Hembade  
--Date:16/05/2018  
--[UserInformationSelectAllUser] 1      
CREATE PROC [dbo].[UserInformationSelectAllUser] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT;
	DECLARE @adminTypeId INT = 1;

	SELECT @UserTypeID = UserTypeID
	FROM dbo.UserInformation
	WHERE UserID = @UserID
		AND IsActive = 1

	IF (@UserTypeID = @adminTypeId)
	BEGIN
		SELECT UI.UserID
			,UI.UserName
			,UI.MobileNumber
			,UTM.UserType
			,UTM.UserTypeID AS UserTypeID
		FROM [dbo].UserInformation AS UI
		INNER JOIN UserTypeMaster UTM(NOLOCK) ON UTM.UserTypeID = UI.UserTypeID
		WHERE UI.UserTypeID IN (
				2
				,3
				,4
				,8
				)
			AND UI.IsActive = 1
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectAndroidDownlineUser]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UserInformationSelectAndroidDownlineUser] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT;
	DECLARE @SuperDistTypeID INT = 8
	DECLARE @FOSTypeID INT = 7
	DECLARE @AdminTypeID INT = 1
	DECLARE @DistTypeID INT = 2
	DECLARE @SupportTypeID INT = 9
	DECLARE @APITypeID INT = 4
	SELECT @UserTypeID = UserTypeID
	FROM dbo.UserInformation
	WHERE UserID = @UserID
		AND IsActive = 1
	IF (
			@UserTypeID = @SupportTypeID
			OR @UserTypeID = @AdminTypeID
			)
	BEGIN
		SELECT UI.UserID
			,UI.UserName
			,UI.Emailid
			,UI.MobileNumber
			,UTM.UserType
			,UTM.UserTypeID AS RUserTypeID
			,UP.UserName AS ParentName
			,UTP.UserType AS ParentRole
			,CONVERT(DECIMAL(18, 2), isnull(ub.CurrentBalance, 0)) AS CurrentBalance
			,CONVERT(DECIMAL(18, 2), isnull(ub.[DMRBalance], 0)) AS DMRBalance
			,dbo.GetOutStandingBand(UI.UserID) AS Band
			,CONVERT(DECIMAL(18, 2), isnull(ub.[Outstanding], 0)) AS Outstanding
			,CONVERT(DECIMAL(18, 2), isnull(ub.MinimumBalance, 0)) AS MinimumBalance
			,UI.ParentID
			,UP.UserTypeID AS DUserTypeID
		FROM [dbo].UserInformation AS UI
		LEFT JOIN [dbo].UserBalance AS ub ON ub.UserID = UI.UserID
		INNER JOIN UserTypeMaster UTM ON UTM.UserTypeID = UI.UserTypeID
		INNER JOIN [dbo].UserInformation AS UP ON UI.ParentID = UP.UserID
		INNER JOIN UserTypeMaster UTP ON UTP.UserTypeID = UP.UserTypeID
		WHERE UI.UserTypeID NOT IN (
				1
				,5
				,7
				)
	END
	ELSE IF (@UserTypeID = @FOSTypeID)
	BEGIN
		BEGIN
			SELECT UI.UserID
				,UI.UserName
				,UI.Emailid
				,UI.MobileNumber
				,UTM.UserType
				,UTM.UserTypeID AS RUserTypeID
				,UP.UserName AS ParentName
				,UTP.UserType AS ParentRole
				,CONVERT(DECIMAL(18, 2), isnull(ub.CurrentBalance, 0)) AS CurrentBalance
				,CONVERT(DECIMAL(18, 2), isnull(ub.[DMRBalance], 0)) AS DMRBalance
				,CONVERT(DECIMAL(18, 2), isnull(ub.[Outstanding], 0)) AS Outstanding
				,CONVERT(DECIMAL(18, 2), isnull(ub.MinimumBalance, 0)) AS MinimumBalance
				,UI.ParentID
				,UP.UserTypeID AS DUserTypeID
			FROM [dbo].UserInformation AS UI
			LEFT JOIN [dbo].UserBalance AS ub ON ub.UserID = UI.UserID
			INNER JOIN UserTypeMaster UTM ON UTM.UserTypeID = UI.UserTypeID
			INNER JOIN [dbo].UserInformation AS UP ON UI.ParentID = UP.UserID
			INNER JOIN UserTypeMaster UTP ON UTP.UserTypeID = UP.UserTypeID
			WHERE UI.ParentID = (
					SELECT TOP (1) ParentID
					FROM UserInformation
					WHERE UserID = @UserID
					)
				AND UI.UserTypeID != (7)
		END
	END
	ELSE IF (
			@UserTypeID = @SuperDistTypeID
			OR @UserTypeID = @DistTypeID
			OR @UserTypeID = @APITypeID
			)
	BEGIN
		SELECT UI.UserID
			,UI.UserName
			,UI.Emailid
			,UI.MobileNumber
			,UTM.UserType
			,UTM.UserTypeID AS RUserTypeID
			,UP.UserName AS ParentName
			,UTP.UserType AS ParentRole
			,CONVERT(DECIMAL(18, 2), isnull(ub.CurrentBalance, 0)) AS CurrentBalance
			,CONVERT(DECIMAL(18, 2), isnull(ub.[DMRBalance], 0)) AS DMRBalance
			,CONVERT(DECIMAL(18, 2), isnull(ub.[Outstanding], 0)) AS Outstanding
			,CONVERT(DECIMAL(18, 2), isnull(ub.MinimumBalance, 0)) AS MinimumBalance
			,UI.ParentID
			,UP.UserTypeID AS DUserTypeID
		FROM [dbo].UserInformation AS UI
		LEFT JOIN [dbo].UserBalance AS ub ON ub.UserID = UI.UserID
		INNER JOIN UserTypeMaster UTM ON UTM.UserTypeID = UI.UserTypeID
		INNER JOIN [dbo].UserInformation AS UP ON UI.ParentID = UP.UserID
		INNER JOIN UserTypeMaster UTP ON UTP.UserTypeID = UP.UserTypeID
		WHERE UI.UserID IN (
				SELECT UserID
				FROM UserInformation
				WHERE ParentId = @UserID
				)
	END
END

GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectAPIToken]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UserInformationSelectAPIToken] @UserID INT    
AS    
BEGIN    
 SELECT UserID    
  ,MobileNumber    
  ,isnull(Token,'') AS APIToken    
 FROM UserInformation    
 WHERE UserID = @UserID    
END 
GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectAPIUserWithIDAndIP]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:  Sushant Yelpale
-- ALTER  date: 2/10/2018
-- Description: Selects UserID from Userinformation by Token and Mobilenumber and IP
-- [UserInformationSelectAPIUserWithIDAndIP] '9462500007','oD43af270j1OIpo3fz41'
-- =============================================
CREATE PROCEDURE [dbo].[UserInformationSelectAPIUserWithIDAndIP] @UserName VARCHAR(50)
	,@Token VARCHAR(50)
	,@IPAddress VARCHAR(50)
	,@OUTPUT INT OUT
AS
BEGIN
	DECLARE @IPAddressDB VARCHAR(20);

	SELECT @IPAddressDB = IP
	FROM dbo.UserInformation(NOLOCK)
	WHERE MobileNumber = @UserName
		AND Token = @Token

	IF @IPAddressDB IS NULL
	BEGIN
		SELECT TOP (1) @OUTPUT = UserID
		FROM dbo.UserInformation(NOLOCK)
		WHERE MobileNumber = @UserName
			AND Token = @Token
	END
	ELSE
	BEGIN
		SELECT TOP (1) @OUTPUT = UserID
		FROM dbo.UserInformation(NOLOCK)
		WHERE MobileNumber = @UserName
			AND Token = @Token
			AND IP = @IPAddress
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectBackUp]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Author :Rahul Hembade  
--Date : 15 Feb 2018    
CREATE PROCEDURE [dbo].[UserInformationSelectBackUp]
AS
BEGIN
	SELECT UI.[UserID]
		,[Name]
		,[UserName]
		,UTM.UserType
		,UB.CurrentBalance
		,UB.MinimumBalance
		,UB.ServiceCharge
		,[EmailID]
		,[Address]
		,[MobileNumber]
		,UI.[UserTypeID]
		,[ParentID]
		,UI.[IsActive]
		,UI.[DOC]
	FROM [dbo].[UserInformation] UI
	INNER JOIN UserBalance UB ON UI.UserID = UB.UserID
	INNER JOIN UserTypeMaster UTM ON UI.[UserTypeID] = UTM.[UserTypeID]
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectDetails]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================        
--Auther :Priyanka        
--Date :27-2-17        
--Purpose UserInformation Select        
--Modifay:Rahul hembade        
--Reson:add type gor DMRBalance        
--==========================================        
---UserInformationSelect  1,'Recharge'        
CREATE PROCEDURE [dbo].[UserInformationSelectDetails] @UserID INT    
AS    
BEGIN    
 BEGIN    
  SELECT UI.UserID    
   ,UI.UserName   
   ,UI.UserTypeID 
   ,UI.MobileNumber    
   ,isnull(ub.CurrentBalance, 0) AS CurrentBalance    
   ,isnull(ub.DMRBalance, 0) AS DMRBalance  
   ,isnull(ub.Outstanding, 0) AS Outstanding    
   ,UI.ParentID AS ParentID    
   ,UP.UserName AS ParentName    
  FROM [dbo].UserInformation(NOLOCK) AS UI    
  INNER JOIN [dbo].UserInformation(NOLOCK) AS UP ON UI.ParentID = UP.UserID    
  LEFT JOIN [dbo].UserBalance(NOLOCK) AS ub ON ub.UserID = UI.UserID    
  INNER JOIN [dbo].UserTypeMaster(NOLOCK) AS UIT ON UI.UserTypeID = UIT.UserTypeID    
  WHERE UI.UserID = @UserID    
   AND UI.IsActive = 1    
 END    
END    
    
GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectDistributor]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Author :Sainath Bhujbal
--Date: 14 Feb 2018
--UserInformationSelectSuperDistributor 64  
CREATE PROC [dbo].[UserInformationSelectDistributor] @UserId INT
AS
BEGIN
	DECLARE @UserType INT;
	DECLARE @DistributorUserType INT = 2;

	SELECT @UserType = UserTypeID
	FROM UserInformation
	WHERE UserID = @UserId

	IF (@UserType = @DistributorUserType)
	BEGIN
		SELECT 0 AS UserID
			,'Select User' AS UserName
			,'' AS MobileNumber
			,0.0 AS CurrentBalance
			,0 AS ParentID
			,'' AS ParentName
			,'' AS ParentMobileNumber
			,0 AS UserTypeID
			,'' AS UserType
		FROM UserInformation
		
		UNION
		
		SELECT UI.UserID
			,UI.UserName
			,UI.MobileNumber
			,isnull(ub.CurrentBalance, 0) AS CurrentBalance
			,UI.ParentID AS ParentID
			,UP.name AS ParentName
			,UP.MobileNumber AS ParentMobileNumber
			,UI.UserTypeID
			,UIT.UserType
		FROM [dbo].UserInformation AS UI
		INNER JOIN [dbo].UserInformation AS UP ON UI.ParentID = UP.UserID
		LEFT JOIN [dbo].UserBalance AS ub ON ub.UserID = UI.UserID
		INNER JOIN [dbo].UserTypeMaster AS UIT ON UI.UserTypeID = UIT.UserTypeID
		WHERE (UI.UserTypeID = @DistributorUserType)
			AND UI.IsActive = 1
			AND UI.UserID NOT IN (@UserId)
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectDownline]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--==========================
--Author: Priyanka Deshmukh
--Date :11-2-2017   
--Description: UserInformation Select Downline
--==========================
--UserInformationSelectDownline 2
CREATE PROCEDURE[dbo].[UserInformationSelectDownline] @UserId INT
AS
BEGIN
	DECLARE @role VARCHAR(20);

	SELECT @role = UM.UserType
	FROM UserInformation UI
	INNER JOIN UserTypeMaster UM ON UI.UserTypeID = UM.UserTypeID
	WHERE UserID = @UserId

	IF (@role = 'Admin')
	BEGIN
		SELECT '0' AS UserID
			,'All users' AS name
		
		UNION
		
		SELECT UserID
			,name
		FROM [dbo].UserInformation(NOLOCK)
		WHERE UserTypeID IN (
				3
				,2
				)
	END
	ELSE
	BEGIN
		SELECT '0' AS UserID
			,'All users' AS name
		
		UNION
		
		SELECT UserID
			,name
		FROM [dbo].UserInformation(NOLOCK)
		WHERE ParentId = @UserId
			AND UserTypeID IN (
				2
				,3
				)
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectDownlineByUserID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--UserInformationSelectDownlineByUserID 2622

CREATE PROCedure [dbo].[UserInformationSelectDownlineByUserID] 

@UserId int

As

Begin

select Count(UserId) as DownlineUserCount from UserInformation where ParentId=@UserId

End









GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectDownlineTree]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:  Sushant Yelpale  
-- Create date: 12 Nov 2018  
-- Description: Selects All Downline tree members  
-- UserInformationSelectDownlineTree 1  
-- =============================================  
CREATE PROCEDURE [dbo].[UserInformationSelectDownlineTree] @UserID INT  
AS  
BEGIN  
 DECLARE @UserIDDB INT = @UserID  
 DECLARE @TempTbl TABLE (UserID VARCHAR(200));  
  
 IF (@UserIDDB = 1)  
 BEGIN  
  SELECT UI.UserID  
	,UI.Name 
	,UI.UserName
	,UI.ShopeName
   ,UI.MobileNumber  
   ,UTM.UserType
   ,UI.UserTypeID  
  FROM UserInformation  UI
  Inner Join UserTypeMaster UTM on UTM.UsertypeID = UI.UsertypeID
  WHERE UI.usertypeid NOT IN (5)  
 END  
 ELSE  
 BEGIN  
  WITH cte  
  AS (  
   SELECT *  
   FROM UserInformation  
   WHERE ParentID = @UserIDDB  
     
   UNION ALL  
     
   SELECT UserInformation.*  
   FROM UserInformation  
   INNER JOIN cte ON cte.UserID = UserInformation.ParentID  
   )  
  INSERT INTO @TempTbl (UserID)  
  SELECT UserID  
  FROM cte  
  
  SELECT  UI.UserID  
	,UI.Name 
	,UI.UserName
	,UI.ShopeName
   ,UI.MobileNumber  
   ,UTM.UserType
   ,UI.UserTypeID  
  FROM UserInformation UI  
  INNER JOIN @TempTbl TT ON TT.UserID = UI.UserID 
   Inner Join UserTypeMaster UTM on UTM.UsertypeID = UI.UsertypeID
   WHERE UI.usertypeid NOT IN (5)  
 END  
END  


GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectDownlineUser]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [UserInformationSelectDownlineUser] 1
CREATE PROC [dbo].[UserInformationSelectDownlineUser] @UserID INT  
AS  
BEGIN  
 DECLARE @UserTypeID INT;  
 SELECT @UserTypeID = UserTypeID  
 FROM dbo.UserInformation  
 WHERE UserID = @UserID  
  AND IsActive = 1  
 DECLARE @SuperDistTypeID INT = 8  
 DECLARE @DFOSTypeID INT = 7  
 DECLARE @SDFOSTypeID INT = 10  
 DECLARE @AdminTypeID INT = 1  
 DECLARE @DistTypeID INT = 2  
 DECLARE @SupportTypeID INT = 9  
 IF (  
   @UserTypeID = @AdminTypeID  
   OR @UserTypeID = @SupportTypeID  
   )  
 BEGIN  
  SELECT UI.UserID  
   ,UI.name  
   ,UI.UserName  
   ,UI.Password  
   ,UI.RechargePin  
   ,UI.Emailid  
   ,UI.MobileNumber  
   ,UTM.UserType  
   ,UTM.UserTypeID AS RUserTypeID  
   ,UI.Address  
   ,CASE   
    WHEN len(UI.ShopeName) <= 0  
     THEN UI.name  
    ELSE UI.ShopeName  
    END AS ShopeName  
   ,UI.IsActive  
   ,UP.UserName AS ParentName  
   ,UTP.UserType AS ParentRole  
   ,CONVERT(DECIMAL(18, 2), isnull(ub.CurrentBalance, 0)) AS CurrentBalance  
   ,CONVERT(DECIMAL(18, 2), isnull(ub.OutStanding, 0)) AS OutStanding  
   ,dbo.GetOutStandingBand(UI.UserID) AS Band  
   ,CONVERT(DECIMAL(18, 2), isnull(ub.DMRBalance, 0)) AS DMRBalance  
   ,CONVERT(DECIMAL(18, 2), isnull(ub.MinimumBalance, 0)) AS MinimumBalance  
   ,UI.ParentID  
   ,UP.UserTypeID AS DUserTypeID  
   ,UI.Adhar AS Adhar  
   ,UI.Gst AS Gst  
  FROM [dbo].UserInformation AS UI  
  LEFT JOIN [dbo].UserBalance AS ub(NOLOCK) ON ub.UserID = UI.UserID  
  INNER JOIN UserTypeMaster UTM(NOLOCK) ON UTM.UserTypeID = UI.UserTypeID  
  INNER JOIN [dbo].UserInformation AS UP(NOLOCK) ON UI.ParentID = UP.UserID  
  INNER JOIN [dbo].UserTypeMaster UTP(NOLOCK) ON UTP.UserTypeID = UP.UserTypeID  
  WHERE UI.UserTypeID NOT IN (  
    1  
    ,5  
    )  
 END  
 ELSE IF (  
   @UserTypeID = @DFOSTypeID  
   OR @UserTypeID = @SDFOSTypeID  
   )  
 BEGIN  
  SELECT UI.UserID  
   ,UI.name  
   ,UI.UserName  
   ,UI.Password  
   ,UI.RechargePin  
   ,UI.Emailid  
   ,UI.MobileNumber  
   ,UTM.UserType  
   ,UTM.UserTypeID AS RUserTypeID  
   ,UI.Address  
   ,CASE   
    WHEN len(UI.ShopeName) <= 0  
     THEN UI.name  
    ELSE UI.ShopeName  
    END AS ShopeName  
   ,UI.IsActive  
   ,UP.UserName AS ParentName  
   ,UTP.UserType AS ParentRole  
   ,CONVERT(DECIMAL(18, 2), isnull(ub.CurrentBalance, 0)) AS CurrentBalance  
   ,CONVERT(DECIMAL(18, 2), isnull(ub.OutStanding, 0)) AS OutStanding  
   ,CONVERT(DECIMAL(18, 2), isnull(ub.DMRBalance, 0)) AS DMRBalance  
   ,CONVERT(DECIMAL(18, 2), isnull(ub.MinimumBalance, 0)) AS MinimumBalance  
   ,UI.ParentID  
   ,UP.UserTypeID AS DUserTypeID  
   ,UI.Adhar AS Adhar  
   ,UI.Gst AS Gst  
  FROM [dbo].UserInformation AS UI  
  LEFT JOIN [dbo].UserBalance AS ub(NOLOCK) ON ub.UserID = UI.UserID  
  INNER JOIN UserTypeMaster UTM(NOLOCK) ON UTM.UserTypeID = UI.UserTypeID  
  INNER JOIN [dbo].UserInformation AS UP(NOLOCK) ON UI.ParentID = UP.UserID  
  INNER JOIN [dbo].UserTypeMaster UTP(NOLOCK) ON UTP.UserTypeID = UP.UserTypeID  
  WHERE UI.ParentFOS = @UserID  
   OR (  
    UI.ParentID = (  
     SELECT ParentID  
     FROM UserInformation  
     WHERE UserID = @UserID  
     )  
    )  
   OR UI.UserID = @UserID  
 END  
 ELSE IF (  
   @UserTypeID = @SuperDistTypeID  
   OR @UserTypeID = @DistTypeID  
   )  
 BEGIN  
  SELECT UI.UserID  
   ,UI.name  
   ,UI.UserName  
   ,UI.Password  
   ,UI.RechargePin  
   ,UI.Emailid  
   ,UI.MobileNumber  
   ,UTM.UserType  
   ,UTM.UserTypeID AS RUserTypeID  
   ,UI.Address  
   ,CASE   
    WHEN len(UI.ShopeName) <= 0  
     THEN UI.name  
    ELSE UI.ShopeName  
    END AS ShopeName  
   ,UI.IsActive  
   ,UP.UserName AS ParentName  
   ,UTP.UserType AS ParentRole  
   ,CONVERT(DECIMAL(18, 2), isnull(ub.CurrentBalance, 0)) AS CurrentBalance  
   ,CONVERT(DECIMAL(18, 2), isnull(ub.OutStanding, 0)) AS OutStanding  
   ,CONVERT(DECIMAL(18, 2), isnull(ub.DMRBalance, 0)) AS DMRBalance  
   ,CONVERT(DECIMAL(18, 2), isnull(ub.MinimumBalance, 0)) AS MinimumBalance  
   ,UI.ParentID  
   ,UP.UserTypeID AS DUserTypeID  
   ,UI.Adhar AS Adhar  
   ,UI.Gst AS Gst  
  FROM [dbo].UserInformation AS UI  
  LEFT JOIN [dbo].UserBalance AS ub(NOLOCK) ON ub.UserID = UI.UserID  
  INNER JOIN UserTypeMaster UTM(NOLOCK) ON UTM.UserTypeID = UI.UserTypeID  
  INNER JOIN [dbo].UserInformation AS UP(NOLOCK) ON UI.ParentID = UP.UserID  
  INNER JOIN [dbo].UserTypeMaster UTP(NOLOCK) ON UTP.UserTypeID = UP.UserTypeID  
  WHERE UI.UserID IN (  
    SELECT UserID  
    FROM UserInformation  
    WHERE ParentId = @UserID  
    )  
   OR UI.UserID = @UserID  
   -- AND UI.IsActive = 1                              
 END  
END  
GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectForTransfer]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Author :Sainath Bhujbal
--Date: 21 Nov 2018
--[UserInformationSelectForTransfer] 1             
CREATE PROCEDURE [dbo].[UserInformationSelectForTransfer] @ParentID INT
AS
BEGIN
	DECLARE @UserTypeID INT
	DECLARE @distributorIdType INT = 2
	DECLARE @AdminIdType INT = 1
	DECLARE @SuperdistributorIdType INT = 8

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @ParentID
			)

	IF (@UserTypeID = @AdminIdType)
	BEGIN
		SELECT *
		FROM (
			SELECT 0 AS UserID
				,'Select User' AS UserName
				,'' AS MobileNumber
				,0.0 AS CurrentBalance
				,0 AS ParentID
				,'' AS ParentName
				,'' AS ParentMobileNumber
				,0 AS UserTypeID
				,'' AS UserType
				,'' AS Name
			FROM UserInformation
			
			UNION
			
			SELECT UI.UserID
				,UI.UserName
				,UI.MobileNumber
				,isnull(ub.CurrentBalance, 0) AS CurrentBalance
				,UI.ParentID AS ParentID
				,UP.name AS ParentName
				,UP.MobileNumber AS ParentMobileNumber
				,UI.UserTypeID
				,UIT.UserType
				,UI.Name
			FROM [dbo].UserInformation AS UI
			INNER JOIN [dbo].UserInformation AS UP ON UI.ParentID = UP.UserID
			LEFT JOIN [dbo].UserBalance AS ub ON ub.UserID = UI.UserID
			INNER JOIN [dbo].UserTypeMaster AS UIT ON UI.UserTypeID = UIT.UserTypeID
			WHERE UI.UserTypeID = @distributorIdType
				OR UI.UserTypeID = @SuperdistributorIdType
				AND UI.IsActive = 1
			) AS Main
		ORDER BY UserID
	END
			
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectGroups]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[UserInformationSelectGroups] @UserID INT
AS
BEGIN
	DECLARE @RechargeGroupID INT
	DECLARE @MoneyGroupID INT
	DECLARE @UserTypeID INT

	SELECT @UserTypeID = UserTypeID
	FROM userinformation
	WHERE UserID = @UserID

	IF (@UserTypeID != 1)
	BEGIN
		SELECT @MoneyGroupID = GroupID
		FROM UserwiseSlabCommission
		WHERE UserId = @UserID

		SELECT @RechargeGroupID = GroupID
		FROM UserGroupMapping
		WHERE UserId = @UserID

		SELECT isNull(@RechargeGroupID, 0) AS RechargeGroupID
			,isNull(@MoneyGroupID, 0) AS MoneyGroupID
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserinformationSelectKycFlag]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[UserinformationSelectKycFlag] @UserID INT
AS
BEGIN
	SELECT [IsKYCApproved]
	FROM [dbo].[UserInformation]
	WHERE [UserID] = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[userInformationSelectOwnDownline]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[userInformationSelectOwnDownline] @UserID INT
AS
BEGIN
	DECLARE @SDFosuser VARCHAR(50) = 'SDFOS';
	DECLARE @DFosuser VARCHAR(50) = 'DFOS';
	DECLARE @UserType VARCHAR(50)
	SELECT @UserType = UTM.UserType
	FROM UserInformation UI
	INNER JOIN UserTypeMaster UTM ON UI.UserTypeID = UTM.UserTypeID
	WHERE UI.UserID = @UserID;
	PRINT @UserType
	IF (
			@UserType != @DFosuser
			AND @UserType != @SDFosuser
			)
	BEGIN
		SELECT 0 AS UserID
			,'Select User' AS UserName
			,'' AS MobileNumber
			,'' AS ShopeName
			,'' AS UserType
		FROM UserInformation
		UNION
		SELECT UI.UserID
			,UI.UserName
			,UI.MobileNumber
			,UI.ShopeName
			,UTM.[UserType]
		FROM UserInformation UI
		INNER JOIN UserTypeMaster UTM ON UTM.UserTypeID = UI.UserTypeID
		WHERE UI.ParentID = @UserID
			AND UI.UserTypeID NOT IN (
				1
				,5
				)
	END
	ELSE IF (
			@UserType = @DFosuser
			OR @UserType = @SDFosuser
			)
	BEGIN
		DECLARE @FOSParent INT;
		SELECT @FOSParent = ParentID
		FROM UserInformation
		WHERE UserID = @UserID
		PRINT @FOSParent
		SELECT UserID
		INTO #temp
		FROM UserInformation
		WHERE ParentID = @FOSParent
		SELECT 0 AS UserID
			,'Select User' AS UserName
			,'' AS MobileNumber
			,'' AS ShopeName
			,'' AS UserType
		FROM UserInformation
		UNION
		SELECT UI.UserID
			,UI.UserName
			,UI.MobileNumber
			,UI.ShopeName
			,UTM.[UserType]
		FROM UserInformation UI
		INNER JOIN UserTypeMaster UTM ON UTM.UserTypeID = UI.UserTypeID
		INNER JOIN #temp AS T ON T.UserID = UI.UserID
		WHERE --UI.ParentID = @UserID      
			UI.UserTypeID NOT IN (
				1
				,5
				,10
				,7
				)
	END
END

GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectOwnUser]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- UserInformationSelectOwnUser 1405                  
-- UserInformationSelectOwnUser 2629            
CREATE PROCEDURE [dbo].[UserInformationSelectOwnUser] @ParentID INT
AS
BEGIN
	DECLARE @UserTypeID INT
	DECLARE @SuperDistTypeID INT = 8
	DECLARE @DFOSTypeID INT = 7
	DECLARE @SDFOSTypeID INT = 10
	DECLARE @AdminTypeID INT = 1
	DECLARE @DistTypeID INT = 2
	DECLARE @SupportTypeID INT = 9
	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @ParentID
				AND IsActive = 1
			)
	PRINT @UserTypeID
	IF (
			@UserTypeID = @SuperDistTypeID
			OR @UserTypeID = @DistTypeID
			)
	BEGIN
		SELECT 0 AS UserID
			,'Select User' AS UserName
			,'' AS MobileNumber
			,0.0 AS CurrentBalance
			,0 AS ParentID
			,'' AS ParentName
			,'' AS ParentMobileNumber
			,0 AS UserTypeID
			,'' AS UserType
		FROM UserInformation
		UNION
		SELECT UI.UserID
			,UI.UserName
			,UI.MobileNumber
			,isnull(ub.CurrentBalance, 0) AS CurrentBalance
			,UI.ParentID AS ParentID
			,UP.name AS ParentName
			,UP.MobileNumber AS ParentMobileNumber
			,UI.UserTypeID
			,UIT.UserType
		FROM [dbo].UserInformation AS UI
		INNER JOIN [dbo].UserInformation AS UP ON UI.ParentID = UP.UserID
		INNER JOIN [dbo].UserBalance AS ub ON ub.UserID = UI.UserID
		INNER JOIN [dbo].UserTypeMaster AS UIT ON UI.UserTypeID = UIT.UserTypeID
		WHERE UI.ParentID = @ParentID
	END
	IF (
			@UserTypeID = @AdminTypeID
			OR @UserTypeID = @SupportTypeID
			)
	BEGIN
		SELECT 0 AS UserID
			,'Select User' AS UserName
			,'' AS MobileNumber
			,0.0 AS CurrentBalance
			,0 AS ParentID
			,'' AS ParentName
			,'' AS ParentMobileNumber
			,0 AS UserTypeID
			,'' AS UserType
		FROM UserInformation
		UNION
		SELECT UI.UserID
			,UI.UserName
			,UI.MobileNumber
			,isnull(ub.CurrentBalance, 0) AS CurrentBalance
			,UI.ParentID AS ParentID
			,UP.name AS ParentName
			,UP.MobileNumber AS ParentMobileNumber
			,UI.UserTypeID
			,UIT.UserType
		FROM [dbo].UserInformation AS UI
		INNER JOIN [dbo].UserInformation AS UP ON UI.ParentID = UP.UserID
		INNER JOIN [dbo].UserBalance AS ub ON ub.UserID = UI.UserID
		INNER JOIN [dbo].UserTypeMaster AS UIT ON UI.UserTypeID = UIT.UserTypeID
		WHERE UI.UserTypeID IN (
				2
				,4
				,8
				,9
				)
	END
	IF (
			@UserTypeID = @DFOSTypeID
			OR @UserTypeID = @SDFOSTypeID
			)
	BEGIN
		PRINT 'In FOS'
		DECLARE @FOSParent INT
		SELECT @FOSParent = ParentID
		FROM UserInformation
		WHERE UserId = @ParentID
		SELECT 0 AS UserID
			,'Select User' AS UserName
			,'' AS MobileNumber
			,0.0 AS CurrentBalance
			,0 AS ParentID
			,'' AS ParentName
			,'' AS ParentMobileNumber
			,0 AS UserTypeID
			,'' AS UserType
		FROM UserInformation
		UNION
		SELECT UI.UserID
			,UI.UserName
			,UI.MobileNumber
			,isnull(ub.CurrentBalance, 0) AS CurrentBalance
			,UI.ParentID AS ParentID
			,UP.name AS ParentName
			,UP.MobileNumber AS ParentMobileNumber
			,UI.UserTypeID
			,UIT.UserType
		FROM [dbo].UserInformation AS UI
		INNER JOIN [dbo].UserInformation AS UP ON UI.ParentID = UP.UserID
		INNER JOIN [dbo].UserBalance AS ub ON ub.UserID = UI.UserID
		INNER JOIN [dbo].UserTypeMaster AS UIT ON UI.UserTypeID = UIT.UserTypeID
		WHERE UI.ParentId = @FOSParent
			AND UI.UserTypeId NOT IN (7)
			AND UI.ParentId = @FOSParent
			AND UI.UserTypeId NOT IN (10)
			--WHERE UI.ParentFOS = @ParentID    
	END
END

GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectRechargePin]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UserInformationSelectRechargePin] @UserID INT
	,@RechargePin VARCHAR(100)
AS
BEGIN
	SELECT UserID
	FROM UserInformation
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectStatus]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Rahul Hembade  
-- [UserInformationSelectStatus] 1
CREATE PROCEDURE [dbo].[UserInformationSelectStatus] ---- UserInformationSelectStatus 1  
	@UserID INT
AS
BEGIN
	DECLARE @TotalUser INT;
	DECLARE @LoginUser INT;
	DECLARE @OfflineUser INT;
	DECLARE @date DATE = (switchoffset(sysdatetimeoffset(), '+05:30'));

	SET @TotalUser = (
			SELECT count(UserID)
			FROM UserInformation
			WHERE UserTypeID NOT IN (
					1
					,5
					)
			);
	SET @LoginUser = (
			SELECT count(DISTINCT UserID)
			FROM LoginHistory
			WHERE Convert(DATE, DOC, 103) = Convert(DATE, @date, 103)
			);
	SET @OfflineUser = @TotalUser - @LoginUser;

	SELECT @TotalUser AS TotalUser
		,@LoginUser AS LoginUser
		,@OfflineUser AS OfflineUser
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectSuperDistributor]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Author :Sainath Bhujbal

--Date: 14 Feb 2018

--UserInformationSelectSuperDistributor 74  

CREATE PROC [dbo].[UserInformationSelectSuperDistributor] @UserId INT

AS

BEGIN

	DECLARE @UserType INT;

	DECLARE @SuperDistributorUserType INT = 8;



	SELECT @UserType = UserTypeID

	FROM UserInformation

	WHERE UserID = @UserId



	IF (@UserType = @SuperDistributorUserType)

	BEGIN

	Print 'In-SuperDist'

		SELECT 0 AS UserID

			,'Select User' AS UserName

			,'' AS MobileNumber

			,0.0 AS CurrentBalance

			,0 AS ParentID

			,'' AS ParentName

			,'' AS ParentMobileNumber

			,0 AS UserTypeID

			,'' AS UserType

		FROM UserInformation

		

		UNION

		

		SELECT UI.UserID

			,UI.UserName

			,UI.MobileNumber

			,isnull(ub.CurrentBalance, 0) AS CurrentBalance

			,UI.ParentID AS ParentID

			,UP.name AS ParentName

			,UP.MobileNumber AS ParentMobileNumber

			,UI.UserTypeID

			,UIT.UserType

		FROM [dbo].UserInformation AS UI

		INNER JOIN [dbo].UserInformation AS UP ON UI.ParentID = UP.UserID

		LEFT JOIN [dbo].UserBalance AS ub ON ub.UserID = UI.UserID

		INNER JOIN [dbo].UserTypeMaster AS UIT ON UI.UserTypeID = UIT.UserTypeID

		WHERE (UI.UserTypeID = @SuperDistributorUserType)

			AND UI.IsActive = 1

			AND UI.UserID NOT IN (@UserId)

	END

END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectSupportUser]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--UserInformationSelectDownlineUser 1      
--UserInformationSelectDownlineUser 2      
CREATE PROCEDURE[dbo].[UserInformationSelectSupportUser] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT;

	SELECT @UserTypeID = UserTypeID
	FROM dbo.UserInformation
	WHERE UserID = @UserID
		AND IsActive = 1

	IF (@UserTypeID = '1')
	BEGIN
		SELECT UI.UserID
			,UI.name
			,UI.UserName
			,UI.MobileNumber
			,UTM.UserType
			,UTM.UserTypeID AS RUserTypeID
		FROM [dbo].UserInformation AS UI
		INNER JOIN UserTypeMaster UTM(NOLOCK) ON UTM.UserTypeID = UI.UserTypeID
		WHERE UI.UserTypeID = 9
			--  AND UI.IsActive = 1          
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectToken]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================                
--Author :Sainath Bhujbal                
--Date :06-04-17                
--Purpose :Select Recharge Pin from UserInformation     
--==========================================           
--UserInformationSelectRechargePinAndToken 1   
CREATE PROCEDURE[dbo].[UserInformationSelectToken] @UserID INT
AS
BEGIN
	SELECT Token
	FROM UserInformation
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectUpline]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserInformationSelectUpline] @Role INT  
 ,@userId INT  
AS  
BEGIN  
 DECLARE @UserTypeID INT;  
 SELECT @UserTypeID = UserTypeID  
 FROM dbo.UserInformation  
 WHERE UserID = @UserID  
 IF (  
   @UserTypeID = 1  
   AND (  
    @Role IN (  
     4  
     ,8  
     ,9 ,2
     )  
    )  
   )  
 BEGIN  
  SELECT UserID  
   ,UserName  
   ,MobileNumber  
  FROM dbo.UserInformation(NOLOCK)  
  WHERE IsActive = 1  
   AND UserTypeID IN (1)  
 END  
 IF (  
   @UserTypeID = 1  
   AND (@Role IN (7))  
   )  
 BEGIN  
  SELECT UserID  
   ,UserName  
   ,MobileNumber  
  FROM dbo.UserInformation(NOLOCK)  
  WHERE IsActive = 1  
   AND UserTypeID IN (2)  
 END  
 IF (  
   @UserTypeID = 1  
   AND (  
    @Role IN (  
     2  
     ,10  
     )  
    )  
   )  
 BEGIN  
  SELECT UserID  
   ,UserName  
   ,MobileNumber  
  FROM dbo.UserInformation(NOLOCK)  
  WHERE IsActive = 1  
   AND UserTypeID IN (8)  
 END  
 IF (  
   @UserTypeID = 1  
   AND @Role IN (3)  
   )  
 BEGIN  
  SELECT '0' AS UserID  
   ,'Select User' AS UserName  
   ,'' AS MobileNumber  
  UNION  
  SELECT UserID  
   ,UserName  
   ,MobileNumber  
  FROM dbo.UserInformation(NOLOCK)  
  WHERE IsActive = 1  
   AND UserTypeID IN (2)  
 END  
 IF (  
   @UserTypeID = 2  
   AND @Role = 2  
   )  
 BEGIN  
  SELECT UserID  
   ,UserName  
   ,MobileNumber  
  FROM dbo.UserInformation(NOLOCK)  
  WHERE IsActive = 1  
   AND UserTypeID IN (1)  
 END  
 IF (  
   @UserTypeID = 2  
   AND @Role IN (  
    3  
    ,7  
    )  
   )  
 BEGIN  
  SELECT UserID  
   ,UserName  
   ,MobileNumber  
  FROM dbo.UserInformation(NOLOCK)  
  WHERE UserID = @userId  
   AND IsActive = 1  
 END  
 IF (  
   @UserTypeID = 7  
   AND @Role = 3  
   )  
 BEGIN  
  SELECT UIF.UserID  
   ,UIF.UserName  
   ,UIF.MobileNumber  
  FROM dbo.UserInformation(NOLOCK) UI  
  INNER JOIN UserInformation(NOLOCK) UIF ON UI.ParentID = UIF.UserID  
  WHERE UI.UserID = @userId  
   AND UIF.IsActive = 1  
 END  
 IF (  
   @UserTypeID = 8  
   AND @Role IN (  
    2  
    ,10  
    )  
   )  
 BEGIN  
  SELECT UI.UserID  
   ,UI.UserName  
   ,UI.MobileNumber  
  FROM dbo.UserInformation(NOLOCK) UI  
  WHERE UI.UserID = @userId  
   AND UI.IsActive = 1  
 END  
 IF (  
   @UserTypeID = 10  
   AND @Role = 2  
   )  
 BEGIN  
  SELECT UIF.UserID  
   ,UIF.UserName  
   ,UIF.MobileNumber  
  FROM dbo.UserInformation(NOLOCK) UI  
  INNER JOIN UserInformation(NOLOCK) UIF ON UI.ParentID = UIF.UserID  
  WHERE UI.UserID = @userId  
   AND UI.IsActive = 1  
 END  
END  
GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectUplineByUser]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserInformationSelectUplineByUser] @UserID INT
AS
BEGIN
	DECLARE @ParentID INT;

	SET @ParentID = (
			SELECT ParentID
			FROM UserInformation
			WHERE UserID = @UserID
			);

	SELECT UserName
		,MobileNumber
		,UserID
	FROM UserInformation
	WHERE UserID = @ParentID
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectUserBalanceReverse]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================    
--Auther :Priyanka    
--Date :5-4-17    
--Purpose Recharge report    
--==========================================    
--UserInformationSelectUserBalanceReverse 2 
CREATE PROCEDURE [dbo].[UserInformationSelectUserBalanceReverse] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT;

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	--Admin Sender    
	IF (@UserTypeID = 1)
	BEGIN
		SELECT 0 AS UserID
			,'SelectUser' AS UserName
			,'' AS MobileNumber
		FROM UserInformation
		
		UNION
		
		SELECT UserID
			,UserName
			,MobileNumber
		FROM UserInformation
		WHERE UserTypeID NOT IN (
				1
				,6
				)
			AND IsActive = 1
	END

	IF (@UserTypeID = 2)
	BEGIN
		SELECT 0 AS UserID
			,'SelectUser' AS UserName
			,'' AS MobileNumber
		FROM UserInformation
		
		UNION
		
		SELECT UserID
			,UserName
			,MobileNumber
		FROM UserInformation
		WHERE ParentID = @UserID
			AND UserTypeID NOT IN (
				1
				,6
				)
			AND IsActive = 1
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectUserByContactNumber]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================  
--Author: Priyanka Deshmukh  
--Date :11-2-2017     
--Description:UserInformation Select User By Contact Number
--==========================  
--UserInformationSelectUserByContactNumber 8484992183
CREATE PROCEDURE [dbo].[UserInformationSelectUserByContactNumber] @ContactNo VARCHAR(100)
	,@UserID INT
AS
BEGIN
	SELECT DISTINCT UI.UserID
		,UI.name
		,UI.UserName
		,UI.MobileNumber
		,UTM.UserType
		,UTM.UserTypeID
	FROM [dbo].UserInformation AS UI
	INNER JOIN [dbo].UserInformation AS UP ON UI.ParentID = UP.UserID
	LEFT JOIN [dbo].UserBalance AS ub ON ub.UserID = UI.UserID
	LEFT JOIN UserTypeMaster UTM ON UTM.UserTypeID = UI.UserTypeID
	WHERE (
			UI.MobileNumber = @ContactNo
			OR UI.EmailID = @ContactNo
			)
		AND UTM.UserTypeID = 3
		AND UI.IsActive = 1
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectUserByEmailMobile]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--UserInformationSelectUserByEmailMobile 147,'53698'
CREATE PROCEDURE [dbo].[UserInformationSelectUserByEmailMobile] @MobileNumber VARCHAR(100)
	,@Password VARCHAR(50)
AS
BEGIN
	DECLARE @IsActive INT;

	SET @IsActive = (
			SELECT IsActive
			FROM UserInformation
			WHERE MobileNumber = @MobileNumber
			)

	IF (@IsActive = 1)
	BEGIN
		UPDATE UserInformation
		SET Password = @Password
		WHERE MobileNumber = @MobileNumber

		SELECT UserId
			,EmailID
			,MobileNumber
		FROM UserInformation
		WHERE MobileNumber = @MobileNumber
			AND Password = @Password
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectUserByRole]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserInformationSelectUserByRole] @UserTypeID INT
AS
BEGIN
	SELECT MobileNumber
		,UserName
		,UserID
	FROM UserInformation
	WHERE UserTypeID = @UserTypeID
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectUserByUserID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  [UserInformationSelectUserByUserID] 113
CREATE PROC [dbo].[UserInformationSelectUserByUserID] @UserID INT    
AS    
BEGIN    
 DECLARE @RechargeGroupID INT    
 DECLARE @MoneyGroupID INT    
 DECLARE @UserTypeID INT    
 SELECT @MoneyGroupID = GroupID    
 FROM UserwiseSlabCommission    
 WHERE UserId = @UserID    
 SELECT @RechargeGroupID = GroupID    
 FROM UserGroupMapping    
 WHERE UserId = @UserID    
 SELECT @UserTypeID = UserTypeID    
 FROM UserInformation    
 WHERE UserId = @UserID    
 IF (@UserTypeID = 1)    
 BEGIN    
  SELECT DISTINCT UI.UserID    
   ,UI.name    
   ,UI.UserName    
   ,UI.Emailid    
   ,UI.MobileNumber    
   ,UTM.UserType    
   ,UTM.UserTypeID AS RUserTypeID    
   ,UI.Address    
   ,UI.[StateID]  
   ,SM.Name As [State]  
   ,UI.ShopeName    
   ,UI.PAN    
   ,UI.ParentID AS ParentID    
   ,UI.IsActive    
   ,isnull(ub.CurrentBalance, 0) AS CurrentBalance    
   ,isnull(ub.DMRBalance, 0) AS DMRBalance    
   ,isnull(ub.OutStanding, 0) AS OutStanding    
   ,isnull(UI.RechargePin, 0) AS RechargePin    
   ,UI.Password    
   ,convert(VARCHAR(12), UI.DateOfBirth, 113) DateOfBirth    
   ,UI.RechargePin    
   ,UP.MobileNumber AS PMobileNumber    
   ,UP.UserName AS ParentName    
   ,UTP.UserType AS ParentRole    
   ,UP.UserTypeID AS DUserTypeID    
   ,UI.GST AS Gst    
   ,UI.Adhar AS Adhar    
   ,isNull(@RechargeGroupID, 0) AS RechargeGroupID    
   ,isNull(@MoneyGroupID, 0) AS MoneyGroupID    
  FROM [dbo].UserInformation AS UI    
  INNER JOIN [dbo].UserInformation AS UP(NOLOCK) ON UI.ParentID = UP.UserID    
  LEFT JOIN [dbo].UserBalance AS ub(NOLOCK) ON ub.UserID = UI.UserID    
  LEFT JOIN [dbo].UserTypeMaster UTM(NOLOCK) ON UTM.UserTypeID = UI.UserTypeID    
  INNER JOIN [dbo].UserTypeMaster UTP(NOLOCK) ON UTP.UserTypeID = UP.UserTypeID    
  Left Join StateMaster SM on UI.StateID = SM.StateID  
  FULL JOIN [dbo].UserServiceMapping(NOLOCK) ON UserServiceMapping.UserID = UI.UserID    
  WHERE UI.UserID = @UserID    
   AND UI.UserTypeID NOT IN (5)    
  SELECT sm.[ServiceID]    
   ,sm.ServiceName    
   ,isnull(USM.IsActive, 0) AS Active    
  FROM [dbo].ServiceMaster AS SM(NOLOCK)    
  LEFT JOIN [dbo].[UserServiceMapping] AS USM(NOLOCK) ON SM.[ServiceID] = USM.[ServiceID]    
   AND [UserID] = @UserID    
  WHERE SM.IsDeleted = 0    
   AND sm.[ServiceID] NOT IN ('18')    
 END    
 ELSE    
 BEGIN    
  SELECT DISTINCT UI.UserID    
   ,UI.name    
   ,UI.UserName    
   ,UI.Emailid    
   ,UI.MobileNumber    
   ,UTM.UserType    
   ,UTM.UserTypeID AS RUserTypeID    
   ,UI.Address    
   ,UI.[StateID]  
   ,SM.Name As [State]  
   ,UI.ShopeName    
   ,UI.PAN    
   ,UI.ParentID AS ParentID    
   ,UI.IsActive    
   ,isnull(ub.CurrentBalance, 0) AS CurrentBalance    
   ,isnull(ub.DMRBalance, 0) AS DMRBalance    
   ,isnull(ub.OutStanding, 0) AS OutStanding    
   ,dbo.GetOutStandingBand(UI.UserID) AS Band    
   ,isnull(UI.RechargePin, 0) AS RechargePin    
   ,UI.Password    
   ,convert(VARCHAR(12), UI.DateOfBirth, 113) DateOfBirth    
   ,UI.RechargePin    
   ,UP.MobileNumber AS PMobileNumber    
   ,UP.UserName AS ParentName    
   ,UTP.UserType AS ParentRole    
   ,UP.UserTypeID AS DUserTypeID    
   ,UI.GST AS Gst    
   ,UI.Adhar AS Adhar    
   ,isNull(@RechargeGroupID, 0) AS RechargeGroupID    
   ,isNull(@MoneyGroupID, 0) AS MoneyGroupID    
  FROM [dbo].UserInformation AS UI    
  INNER JOIN [dbo].UserInformation AS UP(NOLOCK) ON UI.ParentID = UP.UserID    
  LEFT JOIN [dbo].UserBalance AS ub(NOLOCK) ON ub.UserID = UI.UserID    
  LEFT JOIN [dbo].UserTypeMaster UTM(NOLOCK) ON UTM.UserTypeID = UI.UserTypeID    
  INNER JOIN [dbo].UserTypeMaster UTP(NOLOCK) ON UTP.UserTypeID = UP.UserTypeID    
   Left Join StateMaster SM on UI.StateID = SM.StateID  
  FULL JOIN [dbo].UserServiceMapping(NOLOCK) ON UserServiceMapping.UserID = UI.UserID    
  WHERE UI.UserID = @UserID    
   AND UI.UserTypeID NOT IN (    
    1    
    ,5    
    )    
  SELECT sm.[ServiceID]    
   ,sm.ServiceName    
   ,isnull(USM.IsActive, 0) AS Active    
  FROM [dbo].ServiceMaster AS SM(NOLOCK)    
  LEFT JOIN [dbo].[UserServiceMapping] AS USM(NOLOCK) ON SM.[ServiceID] = USM.[ServiceID]    
   AND [UserID] = @UserID    
  WHERE SM.IsDeleted = 0    
   AND sm.[ServiceID] NOT IN ('18')    
 END    
END    
GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectUserDetail]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[UserInformationSelectUserDetail] @UserID INT
AS
BEGIN
	DECLARE @UserTypeID INT;

	SELECT @UserTypeID = UserTypeID
	FROM dbo.UserInformation
	WHERE UserID = @UserID
		AND IsActive = 1

	IF (@UserTypeID = '1')
	BEGIN
		SELECT UI.UserID
			,UI.name
			,UI.UserName
			,UI.Password
			,UI.Emailid
			,UI.MobileNumber
			,UTM.UserType
			,UTM.UserTypeID
			,UI.Address
			,UI.ShopeName
			,UI.IsActive
			,CONVERT(DECIMAL(18, 2), isnull(ub.CurrentBalance, 0)) AS CURRENT_BALANCE
			,CONVERT(DECIMAL(18, 2), isnull(ub.MinimumBalance, 0)) AS MinimumBalance
		FROM [dbo].UserInformation AS UI(NOLOCK)
		LEFT JOIN [dbo].UserBalance AS ub(NOLOCK) ON ub.UserID = UI.UserID
		INNER JOIN [dbo].UserTypeMaster UTM(NOLOCK) ON UTM.UserTypeID = UI.UserTypeID
		WHERE UI.UserTypeID NOT IN (
				1
				,4
				)
			--  AND UI.IsActive = 1    
	END
	ELSE
	BEGIN
		SELECT UI.UserID
			,UI.name
			,UI.UserName
			,UI.Password
			,UI.Emailid
			,UI.MobileNumber
			,UTM.UserType
			,UTM.UserTypeID
			,UI.Address
			,UI.ShopeName
			,UI.IsActive
			,CONVERT(DECIMAL(18, 2), isnull(ub.CurrentBalance, 0)) AS CURRENT_BALANCE
			,CONVERT(DECIMAL(18, 2), isnull(ub.MinimumBalance, 0)) AS MinimumBalance
		FROM [dbo].UserInformation AS UI(NOLOCK)
		LEFT JOIN [dbo].UserBalance AS ub(NOLOCK) ON ub.UserID = UI.UserID
		INNER JOIN [dbo].UserTypeMaster UTM(NOLOCK) ON UTM.UserTypeID = UI.UserTypeID
		WHERE UI.UserID IN (
				SELECT UserID
				FROM UserInformation
				WHERE ParentId = @UserID
				)
			-- AND UI.IsActive = 1      
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationSelectUserTypeID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--UserInformationSelectUserTypeID 2
CREATE PROCEDURE [dbo].[UserInformationSelectUserTypeID] @UserID INT
AS
BEGIN
	SELECT UserTypeID
		,EmailID
		,UserName
	FROM [dbo].UserInformation(NOLOCK)
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationServiceDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UserInformationServiceDelete] @UserID INT
AS
BEGIN
	DELETE
	FROM UserServiceMapping
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationServiceOnOff]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================
--Author: Priyanka Deshmukh
--Date :11-2-2017   
--Description:UserInformation Service On-Off
--==========================
CREATE PROCEDURE [dbo].[UserInformationServiceOnOff] @UserID INT
	,@Service INT
	,@IsActive INT
AS
BEGIN
	IF NOT EXISTS (
			SELECT UserID
				,ServiceID
			FROM UserServiceMapping
			WHERE UserID = @UserID
				AND ServiceID = @Service
			)
	BEGIN
		INSERT INTO dbo.UserServiceMapping (
			UserID
			,ServiceID
			,IsActive
			,IsBlock
			)
		VALUES (
			@UserID
			,@Service
			,1
			,0
			)
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================    
--Author: Priyanka Deshmukh    
--Date :11-2-2017       
--Description:UserInformation Update     
--==========================    
--UserInformationUpdate ,1'Sainath','ABCD','India','9999999999'    
CREATE PROCEDURE [dbo].[UserInformationUpdate] @UserId INT
	,@Name VARCHAR(50)
	,@ShopeName VARCHAR(50)
	,@UserName VARCHAR(50)
	,@Address VARCHAR(100)
	,@StateID Int 
	,@UserTypeId VARCHAR(50)
	,@PAN VARCHAR(20)
	,@ParentId INT
	,@DateOfBirth VARCHAR(50)
	,@RechargePin INT = NULL
	,@Gst VARCHAR(25)
	,@Adhar VARCHAR(20)
	,@OtoMaxID VARCHAR(500)
	,@MobileNo VARCHAR(20) = NULL
AS
BEGIN
	DECLARE @checkParentId INT = 0;

	SELECT TOP 1 @checkParentId = userid
	FROM UserInformation
	WHERE UserID = @ParentId

	IF (@UserTypeId = 1)
	BEGIN
		RETURN;
	END

	IF (
			@checkParentId IS NULL
			OR @checkParentId <= 0
			)
	BEGIN
		RETURN;
	END

	IF(@UserId=@ParentId)
	Begin
	    RETURN;
	End


	UPDATE [dbo].UserInformation
	SET [Name] = @Name
		,[Address] = @Address
		,[ShopeName] = @ShopeName
		,[UserTypeID] = @UserTypeId
		,[PAN] = @PAN
		,[StateID] = @StateID
		,ParentId = @ParentId
		,DateOfBirth = @DateOfBirth
		,GST = @Gst
		,Adhar = @Adhar
		--,RechargePin = @RechargePin  
		,OtoMaxID = @OtoMaxID
	WHERE UserId = @UserId
END

GO
/****** Object:  StoredProcedure [dbo].[UserInformationUpdateAdhar]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================
--Author: Priyanka Deshmukh
--Date :3-3-2017   
--Description:UserInformation Update Mobile
--==========================
--UserInformationUpdateMobile 98,'1200000'
CREATE PROCEDURE [dbo].[UserInformationUpdateAdhar] @UserId INT
	,@AdharNo VARCHAR(20)
AS
BEGIN
	UPDATE UserInformation
	SET [PAN] = @AdharNo
	WHERE UserID = @UserId
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationUpdateEmailID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================
--Author: Priyanka Deshmukh
--Date :3-3-2017   
--Description:UserInformation Update Email
--==========================
CREATE PROCEDURE [dbo].[UserInformationUpdateEmailID] @UserId INT
	,@EmailId VARCHAR(200)
AS
BEGIN
	IF NOT EXISTS (
			SELECT *
			FROM UserInformation
			WHERE EmailID = @EmailId
			)
	BEGIN
		UPDATE UserInformation
		SET EmailID = @EmailId
		WHERE UserID = @UserId

		INSERT INTO UpdateUserInformation (
			UserID
			,Type
			,ModifiedDOC
			)
		VALUES (
			@UserId
			,'E'
			,(switchoffset(sysdatetimeoffset(), '+05:30'))
			)
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationUpdateGST]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserInformationUpdateGST] @UserID INT
	,@GSTNo VARCHAR(500)
AS
BEGIN
	UPDATE UserInformation
	SET GST = @GSTNo
	WHERE UserID = @UserId
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationUpdateMobile]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================
--Author: Priyanka Deshmukh
--Date :3-3-2017   
--Description:UserInformation Update Mobile
--==========================
--UserInformationUpdateMobile 98,'1200000'
CREATE PROCEDURE [dbo].[UserInformationUpdateMobile] @UserId INT
	,@MobileNo VARCHAR(20)
AS
BEGIN
	IF NOT EXISTS (
			SELECT *
			FROM UserInformation
			WHERE MobileNumber = @MobileNo
			)
	BEGIN
		UPDATE UserInformation
		SET MobileNumber = @MobileNo
		WHERE UserID = @UserId

		INSERT INTO UpdateUserInformation (
			UserID
			,Type
			,ModifiedDOC
			)
		VALUES (
			@UserId
			,'M'
			,(switchoffset(sysdatetimeoffset(), '+05:30'))
			)
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationUpdateOTP]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserInformationUpdateOTP] @UserID INT
	,@OTP INT
AS
BEGIN
	UPDATE UserInformation
	SET OTP = @OTP
	WHERE UserID = @UserID

	SELECT UserId
		,EmailID
		,MobileNumber
	FROM UserInformation
	WHERE UserId = @UserID
		AND OTP = @OTP
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationUpdateParent]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserInformationUpdateParent] @UserID INT
	,@ParentID INT
AS
BEGIN
	UPDATE UserInformation
	SET ParentID = @ParentID
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationUpdatePassword]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================
--Auther :Priyanka
--Date :2-3-17
--Purpose Password update
--==========================================
--UserInformationUpdatePassword 1,'1234' 
CREATE PROCEDURE [dbo].[UserInformationUpdatePassword] @UserID INT
	,@OldPassword VARCHAR(100)
	,@NewPassword VARCHAR(100)
AS
BEGIN
	DECLARE @Password1 VARCHAR(100);

	SET @Password1 = (
			SELECT Password
			FROM UserInformation
			WHERE UserID = @UserID
			);

	IF (@Password1 = @OldPassword)
	BEGIN
		UPDATE UserInformation
		SET Password = @NewPassword
		WHERE UserID = @UserID
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationUpdateRechargePin]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================
--Author: Priyanka Deshmukh
--Date :3-3-2017   
--Description:UserInformation Update RechargePin
--==========================
CREATE PROCEDURE [dbo].[UserInformationUpdateRechargePin] @UserID INT
	,@RechargePin VARCHAR(20)
	,@OldRechargePin VARCHAR(20)
AS
BEGIN
	DECLARE @OldPin VARCHAR(20);

	SET @OldPin = (
			SELECT RechargePin
			FROM UserInformation
			WHERE UserID = @UserID
			);

	IF (@OldPin = @OldRechargePin)
	BEGIN
		UPDATE UserInformation
		SET RechargePin = @RechargePin
		WHERE UserID = @UserID
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationUpdateToken]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author:  <Sainath Bhujbal>              
-- ALTER  date: <7 April 2017 ,>              
-- Description: < Update Token in  UserInformation at time of android login.>              
-- =============================================           
--UserInformationUpdateToken 'sai','aqweplokfjiiejjdm'   
CREATE PROCEDURE [dbo].[UserInformationUpdateToken] @UserID INT
	,@Token VARCHAR(50)
AS
BEGIN
	UPDATE UserInformation
	SET Token = @Token
	WHERE UserID = @UserID

	UPDATE LoginHistory
	SET Token = @Token
	WHERE UserID = @UserID
END
	--select * from UserInformation



GO
/****** Object:  StoredProcedure [dbo].[UserInformationUserDetails]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================
--Author: Priyanka Deshmukh
--Date :11-2-2017   
--Description:  UserInformation User Details for User comission page
--==========================
--UserInformationUserDetails 4
CREATE PROCEDURE[dbo].[UserInformationUserDetails] @UserId INT
AS
BEGIN
	SELECT UI.UserID
		,UI.[Name]
		,UI.[UserName]
		,UI.MobileNumber
		,UM.[UserType]
		,UI.[ParentId]
		,UI.[Isactive]
		,UI.[UserTypeID]
		,CONVERT(DECIMAL(18, 2), isnull(UB.CurrentBalance, 0)) AS CURRENTBALANCE
	FROM [dbo].UserInformation AS UI
	LEFT JOIN [dbo].UserBalance AS UB ON UB.UserID = UI.UserID
	INNER JOIN UserTypeMaster UM ON UM.UserTypeID = UI.UserTypeID
	WHERE UI.UserID = @UserId
		AND UI.IsActive = 1
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationVBalance]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--UserInformationVerifyOTP 147,'53698'
CREATE PROCEDURE [dbo].[UserInformationVBalance] @UserID INT
	,@OTP VARCHAR(50)
AS
BEGIN
	SELECT UI.UserName
		,UI.UserID
		,UI.ParentID
		,UI.MobileNumber
		,UI.EmailID
		,UB.CurrentBalance
		,UT.UserType
		,UT.UserTypeID
		,UI.name
	FROM dbo.UserInformation UI
	INNER JOIN dbo.UserBalance UB ON UI.UserID = UB.UserID
	INNER JOIN dbo.UserTypeMaster UT ON UI.UserTypeID = UT.UserTypeID
	WHERE UI.IsActive = 1
		AND UI.UserID = @UserID
		AND OTP = @OTP
END



GO
/****** Object:  StoredProcedure [dbo].[UserInformationVerifyOTP]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--UserInformationVerifyOTP 147,'53698'
CREATE PROCEDURE [dbo].[UserInformationVerifyOTP] @UserID INT
	,@OTP VARCHAR(50)
AS
BEGIN
	UPDATE UserInformation
	SET IsActive = 1
	WHERE UserId = @UserID
		AND OTP = @OTP

	SELECT UserId
		,EmailID
		,Password
		,MobileNumber
		,RechargePin
	FROM UserInformation
	WHERE UserId = @UserID
		AND OTP = @OTP
END



GO
/****** Object:  StoredProcedure [dbo].[UserInfromationIsKYCApproved]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[UserInfromationIsKYCApproved] @UserID INT
AS
BEGIN
	SELECT isnull([IsKYCApproved], 0) AS [IsKYCApproved]
		,isnull([KYCApprovedBy], 0) AS [KYCApprovedBy]
	FROM UserInformation
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[UserInfromationSelectFCMToken]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[UserInfromationSelectFCMToken] @UserID INT
AS
BEGIN
	SELECT isnull(FCMToken, 0) AS FCMToken
	FROM UserInformation
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[UserInfromationSelectPasswordbyMobile]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--UserInfromationSelectPasswordbyMobile '7588215033'



CREATE PROC [dbo].[UserInfromationSelectPasswordbyMobile]



@MobileNo varchar(10)



As



Begin 



select Password,UserId from UserInformation where MobileNumber=@MobileNo



End


GO
/****** Object:  StoredProcedure [dbo].[UserInfromationSelectUserTypeID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[UserInfromationSelectUserTypeID] @UserID INT
AS
BEGIN
	SELECT UserTypeID
		,ParentID
	FROM UserInformation
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[UserInfromationUpdateFCMToken]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[UserInfromationUpdateFCMToken] @AndroidToken VARCHAR(500)
	,@UserID INT
AS
BEGIN
	UPDATE UserInformation
	SET FCMToken = @AndroidToken
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[Usernamevalidation]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:Priyanka Deshmukh
-- ALTER  date:5-4-2017
-- Description:Username validation
-- =============================================  
-----Usernamevalidation '5252525252',6
CREATE PROCEDURE [dbo].[Usernamevalidation] @UserName VARCHAR(100)
	,@UserTypeID INT
AS
BEGIN
	DECLARE @Customer INT = 6;

	IF (@UserTypeID = @Customer)
	BEGIN
		SELECT UserID
		FROM [dbo].UserInformation
		WHERE UserName = @UserName
			AND IsActive = 1
	END

	IF (@UserTypeID <> @Customer)
	BEGIN
		SELECT UserID
		FROM [dbo].UserInformation
		WHERE UserName = @UserName
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserRequestResponseLogSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--ALTER d by:Rahul Hembade
--Reson:Responce to Api user
--===================================
-- UserRequestResponseLogSelect '9405898057',1
CREATE PROCEDURE [dbo].[UserRequestResponseLogSelect] @ConsumerNumber VARCHAR(20)
	,@UserId INT
AS
BEGIN
	DECLARE @UserType INT;
	DECLARE @APIUserType INT = 4;
	DECLARE @AdminUserType INT = 1;

	SELECT @UserType = UserTypeID
	FROM UserInformation
	WHERE UserID = @UserID;

	IF (@UserType = @APIUserType)
	BEGIN
		SELECT U.Regid
			,U.Request
			,U.Response
			,
			--U.DOC,  
			convert(VARCHAR(12), U.DOC, 113) + right(convert(VARCHAR(39), U.DOC, 22), 11) AS DOC
			,R.ConsumerNumber
			,RPM.[RechargeProviderName]
			,R.STATUS
		FROM dbo.UserRequestResponseLog(NOLOCK) U
		INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeId = U.RechargeId
		INNER JOIN [dbo].[RechargeProviderMaster] RPM ON RPM.[RechargeProviderID] = U.APIID
		WHERE R.ConsumerNumber = @ConsumerNumber
			AND U.Regid = @UserId
		ORDER BY ID DESC
	END
	ELSE IF (@UserType = @AdminUserType)
	BEGIN
		SELECT U.Regid
			,U.Request
			,U.Response
			,
			--U.DOC,  
			convert(VARCHAR(12), U.DOC, 113) + right(convert(VARCHAR(39), U.DOC, 22), 11) AS DOC
			,R.ConsumerNumber
			,RPM.[RechargeProviderName]
			,R.STATUS
		FROM dbo.UserRequestResponseLog(NOLOCK) U
		INNER JOIN dbo.Recharge(NOLOCK) R ON R.RechargeId = U.RechargeId
		INNER JOIN [dbo].[RechargeProviderMaster] RPM ON RPM.[RechargeProviderID] = U.APIID
		WHERE R.ConsumerNumber = @ConsumerNumber
		ORDER BY ID DESC
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserServiceMappingSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserServiceMappingSelect] @UserID INT
AS
BEGIN
	SELECT [ServiceID]
	FROM [dbo].[UserServiceMapping](NOLOCK)
	WHERE [UserID] = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[UserTypeMasterSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================    
--Author: Priyanka Deshmukh    
--Date :11-2-2017       
--Description:  UserTypeMaster Select    
--==========================    
-- UserTypeMasterSelect 2    
-- UserTypeMasterSelect 3    
CREATE PROCEDURE [dbo].[UserTypeMasterSelect] @UserID INT
AS
BEGIN
	DECLARE @select AS VARCHAR(20) = '--Select--';
	DECLARE @role AS INT

	SELECT @role = [UserTypeID]
	FROM dbo.UserInformation(NOLOCK)
	WHERE UserID = @UserID

	IF (@role = 2) -- Distributor    
	BEGIN
		SELECT 0 AS [UserTypeID]
			,@select AS UserType
		
		UNION
		
		SELECT [UserTypeID]
			,UserType
		FROM dbo.[UserTypeMaster](NOLOCK)
		WHERE IsDeleted = 0
			AND UserTypeID IN (
				3
				,7
				)
	END

	IF (@role = 1)
	BEGIN
		SELECT 0 AS [UserTypeID]
			,@select AS UserType
		
		UNION
		
		SELECT [UserTypeID]
			,UserType
		FROM dbo.[UserTypeMaster](NOLOCK)
		WHERE IsDeleted = 0
			AND UserTypeID NOT IN (
				1
				,5
				,6
				,7
				)
	END

	IF (@role = 7)
	BEGIN
		SELECT 0 AS [UserTypeID]
			,@select AS UserType
		
		UNION
		
		SELECT [UserTypeID]
			,UserType
		FROM dbo.[UserTypeMaster](NOLOCK)
		WHERE IsDeleted = 0
			AND UserTypeID IN (3)
	END

	IF (@role = 8)
	BEGIN
		SELECT 0 AS [UserTypeID]
			,@select AS UserType
		
		UNION
		
		SELECT [UserTypeID]
			,UserType
		FROM dbo.[UserTypeMaster](NOLOCK)
		WHERE IsDeleted = 0
			AND UserTypeID IN (
				2
				,7
				)
	END

	IF (@role = 4)
	BEGIN
		SELECT [UserTypeID]
			,UserType
		FROM dbo.[UserTypeMaster](NOLOCK)
		WHERE IsDeleted = 0
			AND UserTypeID IN (4)
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserTypeMasterSelectType]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================
--Author: sujata nalavade
--Date :10-2-2018   
--Description:  UserTypeMaster SelectType by UserId
--==========================
-- [UserTypeMasterSelectType] 2
-- UserTypeMasterSelect 3
CREATE PROCEDURE [dbo].[UserTypeMasterSelectType] @UserID INT
AS
BEGIN
	SELECT [UserTypeID]
	FROM dbo.UserInformation(NOLOCK)
	WHERE UserID = @UserID
END



GO
/****** Object:  StoredProcedure [dbo].[UserwiseSlabCommissionDelete]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Sushant Yelpale  
-- Create date: 3 Nov 2018  
-- Description: Delete DMR Users Group  
-- =============================================  
CREATE PROCEDURE [dbo].[UserwiseSlabCommissionDelete]  
 @UserID int  
AS  
BEGIN  
 Declare @DistID int = 2;  
 Declare @SuperDistID int = 8;  
  
 Declare @UserTypeID int  
 Select @UserTypeID = UserTypeID  
 from UserInformation  
 where Userid = @UserID  
  
 PRINT @UserTypeID        
        
    CREATE TABLE #tblRetailer (UserID INT)        
    CREATE TABLE #tblDist (UserID INT)        
          
    --Insert Own ID in tbl retailer        
    INSERT INTO #tblRetailer (UserID)        
    SELECT @UserID        
        
    -- if SD then get Dist and its retailer        
    IF (@UserTypeID = @SuperDistID)        
    BEGIN        
        INSERT INTO #tblDist (UserID)        
        SELECT UserID        
        FROM UserInformation        
        WHERE ParentID = @UserId        
        
        INSERT INTO #tblRetailer (UserID)        
        SELECT UserID        
        FROM #tblDist        
        
        INSERT INTO #tblRetailer (UserID)        
        SELECT UI.UserID        
        FROM #tblDist Dist        
        INNER JOIN UserInformation AS UI ON UI.ParentID = Dist.UserID        
        WHERE UI.ParentID = Dist.UserID        
    END        
        
    --if Dist then get its retailer        
    IF (@UserTypeID = @DistID)        
    BEGIN        
        INSERT INTO #tblRetailer (UserID)        
        SELECT UserID        
        FROM UserInformation        
        WHERE ParentID = @UserId        
    END        
        
      
    DELETE        
    FROM UserwiseSlabCommission        
    WHERE UserID IN (        
            SELECT UserID        
            FROM #tblRetailer        
            WHERE UserID NOT IN (        
                    SELECT UserID        
                    FROM UserwiseSlabCommission        
                    WHERE [ExplicitGroup] = 1        
                    )        
            )        
    
	-- Set Explicit group to users not in Dist and Super Dist----
	IF(Not(@UserTypeID = @SuperDistID OR @UserTypeID = @DistID))
	BEGIN
		Delete from UserwiseSlabCommission
		where UserID = @UserId
	END

    PRINT 'delete user'    
END  


GO
/****** Object:  StoredProcedure [dbo].[UserwiseSlabCommissionInsertByGroupID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Sushant Yelpale  
-- Create date: 3 Nov 2018  
-- Description: Add DMR Group to Users  
-- =============================================  
CREATE PROC [dbo].[UserwiseSlabCommissionInsertByGroupID] @GroupId INT
	,@UserId INT
AS
BEGIN
	DECLARE @DistID INT = 2;
	DECLARE @RetID INT = 1;
	DECLARE @FOSTypeID INT = 7;
	DECLARE @SuperDistID INT = 8;
	DECLARE @SupportTypeID INT = 9;

	IF (
			(
				@GroupId = NULL
				OR @GroupId = 0
				)
			OR (
				@UserId = NULL
				OR @UserId <= 1
				)
			)
	BEGIN
		RETURN
	END

	-- get usertypeID        
	DECLARE @UserTypeID INT;

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserId
			)

	PRINT @UserTypeID

	CREATE TABLE #tblRetailer (UserID INT)

	CREATE TABLE #tblDist (UserID INT)

	--Insert Own ID in tbl retailer        
	INSERT INTO #tblRetailer (UserID)
	SELECT @UserID

	-- if SD then get Dist and its retailer        
	IF (@UserTypeID = @SuperDistID)
	BEGIN
		INSERT INTO #tblDist (UserID)
		SELECT UserID
		FROM UserInformation
		WHERE ParentID = @UserId

		INSERT INTO #tblRetailer (UserID)
		SELECT UserID
		FROM #tblDist

		INSERT INTO #tblRetailer (UserID)
		SELECT UI.UserID
		FROM #tblDist Dist
		INNER JOIN UserInformation AS UI ON UI.ParentID = Dist.UserID
		WHERE UI.ParentID = Dist.UserID
	END

	--if Dist then get its retailer        
	IF (@UserTypeID = @DistID)
	BEGIN
		INSERT INTO #tblRetailer (UserID)
		SELECT UserID
		FROM UserInformation
		WHERE ParentID = @UserId
	END

	DELETE
	FROM UserwiseSlabCommission
	WHERE UserID IN (
			SELECT UserID
			FROM #tblRetailer
			WHERE UserID NOT IN (
					SELECT UserID
					FROM UserwiseSlabCommission
					WHERE [ExplicitGroup] = 1
					)
			)

	PRINT 'delete user'

	------ Add Users with downline In Group-------  
	INSERT INTO [dbo].UserwiseSlabCommission (
		UserId
		,GroupID
		,Doc
		)
	SELECT TR.UserID
		,@GroupId
		,GETDATE()
	FROM #tblRetailer TR
	INNER JOIN UserInformation UI ON UI.UserID = TR.UserID
	WHERE TR.UserID NOT IN (
			SELECT UserID
			FROM UserwiseSlabCommission
			WHERE [ExplicitGroup] = 1
			)
		AND UI.UserTypeID NOT IN (
			@FOSTypeID
			,@SupportTypeID
			)

	-- Set Explicit group to users not in Dist and Super Dist----  
	IF (
			NOT (
				@UserTypeID = @SuperDistID
				OR @UserTypeID = @DistID
				)
			)
	BEGIN
		UPDATE UserwiseSlabCommission
		SET [ExplicitGroup] = 1
			,[GroupID] = @GroupId
			,Doc = GETDATE()
		WHERE UserID = @UserId
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UserwiseSlabCommissionSelectByUserID]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- [UserwiseSlabCommissionSelectByUserID] 1      
CREATE PROC [dbo].[UserwiseSlabCommissionSelectByUserID] @UserID INT  
AS  
BEGIN  
 DECLARE @UserTypeID INT;  
  
 SELECT @UserTypeID = UserTypeID  
 FROM UserInformation  
 WHERE UserID = @UserID  
  
 IF (@UserTypeID = 1)  
 BEGIN  
  SELECT USC.[ID]  
   ,USC.GroupID
   ,USC.[UserID]  
   ,UTM.UserType
   ,UI.MobileNumber
   ,USC.ExplicitGroup
   ,UserName  
   ,GM.[GroupName]  
  FROM [dbo].[UserwiseSlabCommission] USC  
  INNER JOIN [dbo].[UserInformation] UI ON UI.[UserID] = USC.[UserID]  
  INNER JOIN [dbo].[GroupMaster] GM ON GM.[ID] = USC.[GroupID]  
  INNER JOIN [dbo].[UserTypeMaster] UTM ON UTM.[UserTypeID] = UI.[UserTypeID]  
  ORDER BY USC.Doc DESC  
 END  
END  


GO
/****** Object:  StoredProcedure [dbo].[VersionSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VersionSelect] --[SelecVersion] '1.0'      
	@newVersion VARCHAR(50)
AS
BEGIN
	DECLARE @previesVersion VARCHAR(20);
	SELECT @previesVersion = [Version]
	FROM [dbo].[Version]
	IF (@newVersion IS NULL)
	BEGIN
		RETURN 0;
	END
	IF (@previesVersion < @newVersion)
	BEGIN
		UPDATE [dbo].[Version]
		SET Version = @newVersion
		SELECT *
		FROM [dbo].[Version]
	END
	ELSE
		SELECT *
		FROM [dbo].[Version]
END

GO
/****** Object:  StoredProcedure [dbo].[VirtualBalanceSelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--ALTER d by:Rahul Hembade
--Reson: two types of virtualbalance
--=======================================
CREATE PROCEDURE [dbo].[VirtualBalanceSelect] @FromDate VARCHAR(100)
	,@ToDate VARCHAR(100)
AS
BEGIN
	SELECT [OpBal]
		,[ClBal]
		,Amount
		,[BalanceType]
		,convert(VARCHAR(12), DOC, 113) + right(convert(VARCHAR(39), DOC, 22), 11) AS DOC
	FROM VirtualBalance
	WHERE convert(DATE, DOC, 103) >= convert(DATE, @FromDate, 103)
		AND convert(DATE, DOC, 103) <= convert(DATE, @ToDate, 103)
	ORDER BY doc DESC
END



GO
/****** Object:  StoredProcedure [dbo].[VirtualBalanceUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--====  
--Author : Rahul Hembade
--Purpose :Update Virtual Recharge balance and    
--===================================  
CREATE PROCEDURE[dbo].[VirtualBalanceUpdate] @UserID INT
	,@Amount INT
	,@BalanceType VARCHAR(50)
AS
BEGIN
	DECLARE @UserTypeID INT
		,@CurrentBalance DECIMAL
		,@DMRBalance DECIMAL
		,@NewAmount DECIMAL;
	DECLARE @RESULT INT;
	DECLARE @TransDesc VARCHAR(200);
	DECLARE @Uname VARCHAR(50);

	SET @UserTypeID = (
			SELECT UserTypeID
			FROM UserInformation
			WHERE UserID = @UserID
			)

	IF (@BalanceType = 'Recharge')
	BEGIN
		IF (@UserTypeID = 1)
		BEGIN
			SET @CurrentBalance = (
					SELECT CurrentBalance
					FROM UserBalance
					WHERE UserID = @UserID
					)
			SET @NewAmount = @Amount + @CurrentBalance;

			INSERT INTO VirtualBalance (
				[OpBal]
				,Amount
				,[ClBal]
				,[BalanceType]
				)
			VALUES (
				@CurrentBalance
				,@Amount
				,@NewAmount
				,@BalanceType
				)

			SELECT @RESULT = @@IDENTITY

			UPDATE UserBalance
			SET CurrentBalance = @NewAmount
			WHERE UserID = @UserID

			SELECT @Uname = name
			FROM DBO.UserInformation(NOLOCK)
			WHERE UserID = @UserID;

			--      
			SET @TransDesc = 'Virtual Recharge Balance  Amount: ' + CONVERT(VARCHAR, @Amount) + ' To ' + @Uname + ' From ' + @Uname;

			EXEC dbo.TransactionDetailsInsertByUser @UserID
				,-- @UserID          
				@RESULT
				,--@RechargeID     
				18
				,--@serviceID    
				1
				,--@AssignedUserID    
				'7575011773'
				,--@ConsumerNumber     
				'Virtual Balance'
				,--@DOCType    
				@CurrentBalance
				,--@OpeningBalance         
				@NewAmount
				,--@ClosingBalance        
				@Amount --@TransactionAmount                                     
				,0
				,--@DiscountAmount        
				'CREDIT'
				,--@AmountCreditDebit    
				''
				,--@CommissionCreditDebit    
				@TransDesc --@TransactionDescription  
		END
	END

	---------DMT----------
	IF (@BalanceType = 'DMT')
	BEGIN
		IF (@UserTypeID = 1)
		BEGIN
			SET @DMRBalance = (
					SELECT DMRBalance
					FROM UserBalance
					WHERE UserID = @UserID
					)
			SET @NewAmount = @Amount + @DMRBalance;

			INSERT INTO VirtualBalance (
				[OpBal]
				,Amount
				,[ClBal]
				,[BalanceType]
				)
			VALUES (
				@DMRBalance
				,@Amount
				,@NewAmount
				,@BalanceType
				)

			SELECT @RESULT = @@IDENTITY

			UPDATE UserBalance
			SET DMRBalance = @NewAmount
			WHERE UserID = @UserID

			SELECT @Uname = name
			FROM DBO.UserInformation(NOLOCK)
			WHERE UserID = @UserID;

			--      
			SET @TransDesc = 'Virtual DMT Balance Amount: ' + CONVERT(VARCHAR, @Amount) + ' To ' + @Uname + ' From ' + @Uname;

			EXEC dbo.TransactionDetailsInsertByUser @UserID
				,-- @UserID          
				@RESULT
				,--@RechargeID     
				18
				,--@serviceID    
				1
				,--@AssignedUserID    
				'7575011773'
				,--@ConsumerNumber     
				'Virtual Balance'
				,--@DOCType    
				@DMRBalance
				,--@OpeningBalance         
				@NewAmount
				,--@ClosingBalance        
				@Amount --@TransactionAmount                                     
				,0
				,--@DiscountAmount        
				'CREDIT'
				,--@AmountCreditDebit    
				''
				,--@CommissionCreditDebit    
				@TransDesc --@TransactionDescription  
		END
	END
END



GO
/****** Object:  StoredProcedure [dbo].[WebScrappingReplyInsert]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================      
--Auther :Sainath Bhujbal      
--Date :25-3-17      
--Purpose :    
--========================================== 
--WebScrappingReplyInsert 'http://localhost:14520/ResponseSAPF.aspx?Status=FAIL&RequestId=122&Number=9866464646&Amount=10&OperatorTxnID=TimeOut&ClBal=0',1,'oto'  
CREATE PROCEDURE[dbo].[WebScrappingReplyInsert] @replyString VARCHAR(500)
	,@RechargeProviderId INT
	,@Type VARCHAR(20)
AS
BEGIN
	INSERT INTO dbo.WebScrappingReply (
		RechargeProviderID
		,RechargeProviderReply
		,Type
		)
	VALUES (
		@RechargeProviderId
		,@replyString
		,@Type
		)
END



GO
/****** Object:  StoredProcedure [dbo].[WebScrappingReplyInsertnew]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--==========================================        
--Auther :Sainath Bhujbal        
--Date :25-3-17        
--Purpose :      
--==========================================   
--WebScrappingReplyInsertnew 'http://jaijuirecharge.in/CallBackBigShop.aspx?Status=FAILURE&MobileNumber=7208431570&Amount=10&TransID=1619010&RequestId=15&ClBal=20.86000',1,'15','oto'    
CREATE PROCEDURE[dbo].[WebScrappingReplyInsertnew] @replyString VARCHAR(500)
	,@RechargeProviderId INT
	,@RequestId VARCHAR(50)
	,@Type VARCHAR(20)
AS
BEGIN
	DECLARE @CurTime DATETIME;

	SET @CurTime = CONVERT([datetime], switchoffset(sysdatetimeoffset(), '+05:30'), (0))

	IF NOT EXISTS (
			SELECT RechargeProviderID
			FROM dbo.WebScrappingReply(NOLOCK)
			WHERE RechargeProviderReply = @replyString
				AND datediff(minute, DOC, @CurTime) < 1
			)
	BEGIN
		INSERT INTO dbo.WebScrappingReply (
			RechargeProviderID
			,RechargeProviderReply
			,Type
			)
		VALUES (
			@RechargeProviderId
			,@replyString
			,@Type
			)

		SELECT TOP (1) RechargeID
		FROM dbo.Recharge(NOLOCK)
		WHERE InboxID = @RequestId
	END
END



GO
/****** Object:  StoredProcedure [dbo].[WebScrappingReplySelect]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[WebScrappingReplySelect] @ConsumerNo VARCHAR(50)
AS
BEGIN
	SELECT TOP 1000 convert(VARCHAR(12), W.DOC, 113) + right(convert(VARCHAR(39), W.DOC, 22), 11) AS DOC
		,isnull(W.RechargeProviderReply, 0) AS Response
		,R.RechargeProviderName AS RechargeProviderName
	FROM [dbo].[WebScrappingReply](NOLOCK) AS W
	INNER JOIN dbo.RechargeProviderMaster(NOLOCK) AS R ON R.RechargeProviderID = W.RechargeProviderID
	WHERE RechargeProviderReply LIKE '%' + @ConsumerNo + '%'
	ORDER BY W.DOC DESC
END



GO
/****** Object:  StoredProcedure [dbo].[WebScrappingReplyUpdate]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[WebScrappingReplyUpdate] @RechargeProviderReply VARCHAR(500)
	,@ReplyId INT
AS
BEGIN
	UPDATE WebScrappingReply
	SET RechargeProviderReply = @RechargeProviderReply
	WHERE ReplyId = @ReplyId
END

GO
/****** Object:  StoredProcedure [dbo].[WebScrappingReplyUpdateForIsMismatch]    Script Date: 02/13/2019 11:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--==========================================        
--Auther :Somnath Kadam      
--Date :05-Dec-18        
--Purpose :  TO update ismismatch is reposne not found in list
--==========================================   
--[WebScrappingReplyUpdateForIsMismatch] 1
CREATE PROC [dbo].[WebScrappingReplyUpdateForIsMismatch] @ReplyId INT
AS
BEGIN
	UPDATE WebScrappingReply
	SET isMismatch = 1
	WHERE ReplyId = @ReplyId
END
GO
