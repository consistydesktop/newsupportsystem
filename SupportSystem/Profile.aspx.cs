﻿using Bal;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class Profile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        public static ProfileModel SelectProfile()
        {
            ProfileModel ObjModel = new ProfileModel();
            ProfileBal ObjBal = new ProfileBal();
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            try
            {
                ObjModel = ObjBal.SelectProfile(UserID);
            }
            catch (Exception ex)
            {

            }
            return ObjModel;
        }


        [WebMethod]
        public static MessageWithIcon UpdateEmail(int UserID, string OldEmailID, string NewEmailID)
        {
            MessageWithIcon msgIcon = new MessageWithIcon();
            ProfileModel ObjModel = new ProfileModel();

            int CurrentUser = -1;
            try
            {
                CurrentUser = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                msgIcon.Message = "User Not found";
                return msgIcon;
            }
            if (CurrentUser != UserID)
            {
                msgIcon.Message = "Unautherized user";
                return msgIcon;
            }

            ObjModel.UserID = CurrentUser;
            ObjModel.EmailID = OldEmailID;
            ObjModel.NewEmailID = NewEmailID;
            ProfileBal ObjBal = new ProfileBal();
            int result = -1;
            try
            {
                result = ObjBal.UpdateEmail(ObjModel);
            }
            catch (Exception ex) { new Logger().write(ex); }

            if (result > 0)
                msgIcon.Message = "Email id updated";

            else
                msgIcon.Message = "Email id not updated, Please try with new emailid";
            return msgIcon;
        }


        [WebMethod]
        public static MessageWithIcon UpdatePassword(int UserID, string OldPassword, string NewPassword)
        {
            MessageWithIcon msgIcon = new MessageWithIcon();
            ProfileModel ObjModel = new ProfileModel();

            int CurrentUser = -1;
            try
            {
                CurrentUser = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                msgIcon.Message = "User Not found";
                return msgIcon;
            }
            if (CurrentUser != UserID)
            {
                msgIcon.Message = "Unautherized user";
                return msgIcon;
            }

            ObjModel.UserID = CurrentUser;
            ObjModel.Password = OldPassword;
            ObjModel.NewPassword = NewPassword;
            ProfileBal ObjBal = new ProfileBal();
            int result = -1;
            try
            {
                result = ObjBal.UpdatePassword(ObjModel);
            }
            catch (Exception ex) { new Logger().write(ex); }

            if (result > 0)
                msgIcon.Message = "Password updated";

            else
                msgIcon.Message = "Password not updated";
            return msgIcon;


        }

    }
}