﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using SupportSystem.StaticClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class AddTicketClient : System.Web.UI.Page
    {
        private static string pageName = "AddTicketClient.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static ServiceModel[] SelectServiceName()
        {
            AccessController.checkAccess(pageName);
            List<ServiceModel> details = new List<ServiceModel>();
            AddTicketClientBal objBAL = new AddTicketClientBal();
            try
            {
                details = objBAL.SelectServiceName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static List<AttachmentModel> SelectTicketAttachmentByID(int TicketID)
        {
            DataTable dt = new DataTable();
            List<AttachmentModel> report = new List<AttachmentModel>();

            AddTicketClientBal objBal = new AddTicketClientBal();
            try
            {
                string UploadType = UploadTypeMaster.UploadTicketClient;
                dt = objBal.SelectTicketAttachmentByID(TicketID, UploadType);

                if (dt.Rows.Count <= 0)
                    return report;
                foreach (DataRow dtrow in dt.Rows)
                {
                    AttachmentModel detail = new AttachmentModel();
                    detail.FileID = Convert.ToInt32(dtrow["FileID"].ToString());
                    detail.FileName = dtrow["OriginalFileName"].ToString();
                    detail.FilePath = dtrow["Attachment"].ToString();
                    report.Add(detail);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return report;
        }

        [WebMethod]
        public static AddTicketClientModel SelectByID(int TicketID)
        {
            DataTable dt = new DataTable();
            AddTicketClientModel report = new AddTicketClientModel();

            AddTicketClientBal objBal = new AddTicketClientBal();
            try
            {
                dt = objBal.SelectByID(TicketID);

                if (dt.Rows.Count <= 0)
                    return report;

                report.TicketID = Convert.ToInt32(dt.Rows[0]["TicketID"].ToString());
                report.TicketNo = dt.Rows[0]["TicketNo"].ToString();
                report.Title = dt.Rows[0]["Title"].ToString();
                report.DOC = Convert.ToDateTime(dt.Rows[0]["TicketGeneratedDate"].ToString().Trim());
                report.ServiceName = dt.Rows[0]["ServiceName"].ToString().Trim();
                report.ServiceID = Convert.ToInt32(dt.Rows[0]["ServiceID"].ToString().Trim());
                report.Description = dt.Rows[0]["Description"].ToString().Trim();
                report.TicketStatus = dt.Rows[0]["TicketStatus"].ToString().Trim();
                report.EmployeeRemark = dt.Rows[0]["EmployeeRemark"].ToString().Trim();
                report.AdminRemark = dt.Rows[0]["AdminRemark"].ToString().Trim();
                report.RequestType = dt.Rows[0]["RequestType"].ToString().Trim();
                report.Priority = dt.Rows[0]["Priority"].ToString().Trim();
                report.Attachment = dt.Rows[0]["Attachment"].ToString().Trim();
                report.OriginalFileName = dt.Rows[0]["OriginalFileName"].ToString().Trim();
                report.RemoteAccessType = dt.Rows[0]["RemoteAccessType"].ToString().Trim();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return report;
        }

        #region Insert

        public static bool ValidUserValidation(int ServiceID, string Priority)
        {
            Validation validation = new Validation();

            if (!validation.IsValidInt(ServiceID))
                return false;
            if (!validation.IsValidString(Priority))
                return false;
            return true;
        }

        [WebMethod]
        public static MessageWithIcon Insert(string Title, int ServiceID, string RequestType, string Description, string TeamAny, string FileIDs, string Priority, string TicketGenerationDate, string RemoteAccessType)
        {
            AccessController.checkAccess(pageName);

            AddTicketClientModel objModel = new AddTicketClientModel();
            AddTicketClientBal objBAL = new AddTicketClientBal();

            MessageWithIcon msgIcon = new MessageWithIcon();
            int k = -1;
            int i = -1;
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            if (!ValidUserValidation(ServiceID, Priority))
                msgIcon.Message = "Missing paramter";

            TicketGenerator Obj = new TicketGenerator();
            string TicketNo = Obj.GenerateTicketNo();
            DataTable dt = new DataTable();

            objModel.Title = Title;
            objModel.UserID = UserID;
            objModel.ServiceID = ServiceID;
            objModel.Description = Description;
            objModel.TeamAny = TeamAny;
            objModel.Priority = Priority.ToUpper();
            objModel.TicketNo = TicketNo;
            objModel.RequestType = RequestType.ToUpper();
            objModel.RemoteAccessType = RemoteAccessType;
            try
            {
                i = objBAL.Insert(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (i < 0)
                msgIcon.Message = "Ticket not added";

            msgIcon.Message = " Ticket added successfully \n Ticket Number is " + TicketNo;
            if (FileIDs != "0")
            {
                int j = 0;
                String Files = FileIDs;
                string[] fileID = Files.Split('#');

                for (j = 1; j < fileID.Length; j++)
                {
                    var currentFileID = fileID[j];
                    objModel.FileID = Convert.ToInt32(currentFileID);
                    objModel.TicketID = i;
                    objModel.UploadType = UploadTypeMaster.UploadTicketClient;
                    AddTicketClientBal ObjBal = new AddTicketClientBal();
                    try
                    {
                        k = ObjBal.FileMappingInsert(objModel);
                    }
                    catch (Exception ex) { new Logger().write(ex); }
                    if (k < 0)
                        msgIcon.Message += " File not added";
                }
            }

            string returnEmail = AllMessages.TicketAddedByClientEmail;
            returnEmail = returnEmail.Replace("[TicketNo]", TicketNo);
            returnEmail = returnEmail.Replace("[Title]", Title);
            SendEmail(returnEmail, "Consisty System || Ticket Accepted", UserID);

            return msgIcon;
        }

        #endregion Insert

        public static void SendEmail(string Message, string Subject, int UserID)
        {
            UserInformationModel ObjModel = new UserInformationModel();
            UserInformationBal ObjBal = new UserInformationBal();
            string EmailID = string.Empty;
            string UserName = string.Empty;
            string MobileNo = string.Empty;
            try
            {
                DataTable dtAdmin = ObjBal.GetEmailByUserID(UserRole.AdminUserID);
                EmailID = dtAdmin.Rows[0]["EmailID"].ToString();
                UserName = dtAdmin.Rows[0]["Name"].ToString();
                MobileNo = dtAdmin.Rows[0]["MobileNumber"].ToString();

                Message = Message.Replace("[Username]", UserName);
                MessageProcessing.SendEmail(EmailID, Message, Subject);

                DataTable dt = ObjBal.GetEmailByUserID(UserID);
                EmailID = dt.Rows[0]["EmailID"].ToString();
                UserName = dt.Rows[0]["Name"].ToString();
                MobileNo = dt.Rows[0]["MobileNumber"].ToString();

                Message = Message.Replace("[Username]", UserName);
                MessageProcessing.SendEmail(EmailID, Message, Subject);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
        }

        [WebMethod]
        public static MessageWithIcon Update(int TicketID, string Title, int ServiceID, string RequestType, string Description, string TeamAny, string FileIDs, string Priority, string TicketGenerationDate, string RemoteAccessType)
        {
            AccessController.checkAccess(pageName);

            AddTicketClientModel objModel = new AddTicketClientModel();
            AddTicketClientBal objBAL = new AddTicketClientBal();

            MessageWithIcon msgIcon = new MessageWithIcon();
            int k = -1;
            int i = -1;
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            if (!ValidUserValidation(ServiceID, Priority))
                msgIcon.Message = "Missing paramter";

            objModel.TicketID = TicketID;
            objModel.Title = Title;
            objModel.UserID = UserID;
            objModel.ServiceID = ServiceID;
            objModel.Description = Description;
            objModel.TeamAny = TeamAny;
            objModel.Priority = Priority.ToUpper();
            objModel.TicketNo = "";
            objModel.RequestType = RequestType.ToUpper();
            objModel.RemoteAccessType = RemoteAccessType;
            try
            {
                i = objBAL.Update(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (i < 0)
                msgIcon.Message = "Ticket not Updated";

            msgIcon.Message = " Ticket updated succesfully.";
            if (FileIDs != "0")
            {
                int j = 0;
                String Files = FileIDs;
                string[] fileID = Files.Split('#');

                for (j = 1; j < fileID.Length; j++)
                {
                    var currentFileID = fileID[j];
                    objModel.FileID = Convert.ToInt32(currentFileID);
                    objModel.TicketID = i;
                    objModel.UploadType = UploadTypeMaster.UploadTicketClient;
                    AddTicketClientBal ObjBal = new AddTicketClientBal();
                    try
                    {
                        k = ObjBal.FileMappingInsert(objModel);
                    }
                    catch (Exception ex) { new Logger().write(ex); }
                    if (k < 0)
                        msgIcon.Message += " File not added";
                }
            }

            return msgIcon;
        }
    }
}