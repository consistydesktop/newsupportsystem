﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TimeSheetReport.aspx.cs" Inherits="SupportSystem.TimeSheetReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Time Sheet Report
                                <button class="btn btn-info buttons float-right" type="button" onclick="location.href='/TimeSheet.aspx'" title="Fill TimeSheet">Fill TimeSheet</button>
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputFromDate">From Date </label>
                        <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="inputToDate">To Date</label>
                        <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()'></input>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick=" Search();" id="SearchButton" title="Search">Search</button>
                            </div>
                        </div>
                        <label id="ResponseMsgLbl"></label>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table id="TimeSheetDetails" class="table table-bordered nowrap table-hover">
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </section>
    </div>

    <div class="wrapper">
        <section>
            <div class="container-fluid mt-4">
                <div class="row">
                    <div class="col-12">
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="modal_TimeSheetReport"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="min-width:100%">
            <div class="modal-content" >
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Today's TimeSheet</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table id="TimeSheetReport" class="table table-bordered nowrap">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="/JS/TimeSheetReport.js"></script>
    <script>
        $("#inputFromDate").datepicker({
            dateFormat: 'd MM, yy',
            changeMonth: true,
            changeYear: true,
        });
        $("#inputToDate").datepicker({
            dateFormat: 'd MM, yy',
            changeMonth: true,
            changeYear: true,

        });
        TimeSheetMasterSelectForReport();
    </script>
</asp:Content>
