﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TransactionDetailReport.aspx.cs" Inherits="SupportSystem.TransactionDetailReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Ledeger Reprot</h6>
                            <button class="btn btn-info buttons float-right" type="button" onclick="location.href='/Account.aspx'">Add</button>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputFromDate">From Date </label>
                        <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputToDate">To Date</label>
                        <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()'></input>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputCRDR">Transaction Type</label>
                        <select class="form-control loginput" id="inputCRDR" title="Select Type">
                            <option value="0">Select Type</option>
                            <option value="CR">CR </option>
                            <option value="DR">DR </option>

                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputPayment">Payment Type</label>
                        <select class="form-control loginput" id="inputPayment" title="Select Payment Type">
                            <option value="0">Select Payment Type</option>
                            <option value="Website Payment">Website Payment</option>
                            <option value="Desktop Payment">Desktop Payment</option>
                            <option value="Website Subscription">Website subscription</option>
                            <option value="Desktop subscription">Desktop subscription</option>
                            <option value="Salary">Salary </option>
                            <option value="Bill Tea">Bill Tea</option>
                            <option value="SMS ">SMS </option>
                            <option value="LighBill">LightBill</option>
                            <option value="Net Bill">Net Bill</option>
                            <option value="Rent">Rent</option>
                            <option value="Other">Other</option>

                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputBank">Bank Name</label>
                        <select class="form-control loginput" id="inputBank" title="Select Bank">
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputIsGST">
                            <input type="checkbox" checked="checked" class="form-control loginput" id="inputIsGST" title="Is GST" aria-describedby="emailHelp" />Is GST
                        </label>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return TransactionMasterSelectByDate();" id="ShowButton" title="Show">Show</button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </section>
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="projects" role="tabpanel" aria-labelledby="projects-tab">
                            <div class="table-responsive">
                                <table id="TransactionDetails" class="table table-bordered nowrap table-hover">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="JS/TransactionMaster.js"></script>
    <script>

        $(document).ready(function () {

            $("#inputFromDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });
            $("#inputToDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });

            TransactionMasterSelect();
           
            SelectBankName();

        });
    </script>
</asp:Content>
