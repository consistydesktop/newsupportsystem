﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace SupportSystem
{
    /// <summary>
    /// Summary description for FileUploadHandlerForAndroidData
    /// </summary>
    public class FileUploadHandlerForAndroidData : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            List<AndroidSupportModel> details = new List<AndroidSupportModel>();
            string serverFileName = string.Empty;
            string fileName = string.Empty;
            HttpPostedFile file;

            serverFileName = "NoImage";
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;

                for (int i = 0; i < files.Count; i++)
                {
                    file = files[i];
                    string OriginalFileName = file.FileName;
                    string Extension = Path.GetExtension(file.FileName);
                    string docType = files.AllKeys[i];


                    if (!Validation.IsValidAndroidFileType(Extension))
                    {

                        context.Response.ContentType = "text/plain";
                        context.Response.Write("Only .keystore and .jks files are allowed");
                        return;
                    }


                    if (!Validation.IsValidAndroidSize(file.ContentLength))
                    {

                        context.Response.ContentType = "text/plain";
                        context.Response.Write("File size must under 1MB!");
                        return;
                    }


                    fileName = Guid.NewGuid().ToString() + Extension;
                    string key = files.AllKeys[i];

                    serverFileName = context.Server.MapPath("./AndroidDataAttachment/" + fileName);
                    file.SaveAs(serverFileName);

                    DataTable dt = new DataTable();
                    dt = FileInsert(fileName, docType, OriginalFileName);
                    foreach (DataRow dtrow in dt.Rows)
                    {
                        AndroidSupportModel report = new AndroidSupportModel();
                        report.FileID = Convert.ToInt32(dtrow["FileID"].ToString());
                        details.Add(report);
                    }
                }
                context.Response.ContentType = "text/plain";
                string fileIDs = "";

                foreach (AndroidSupportModel data in details)
                {
                    fileIDs = fileIDs + "#" + data.FileID;
                }

                context.Response.Write(fileIDs);
            }
        }

        private DataTable FileInsert(string path, string docType, string OriginalFileName)
        {
            AndroidSupportModel objModel = new AndroidSupportModel();
            AndroidSupportBal objBal = new AndroidSupportBal();

            DataTable dt = new DataTable();

            objModel.Attachment = "/AndroidDataAttachment/" + path;
            objModel.DocType = docType;
            objModel.OriginalFileName = OriginalFileName;
            dt = objBal.FileInsert(objModel);

            return dt;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}