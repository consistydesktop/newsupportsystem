﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewProjectDetailsForEmployee.aspx.cs" Inherits="SupportSystem.ViewProjectDetailsForEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>View Project                     
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
             <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputFromDate">From Date </label>
                        <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date"  />
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputToDate">To Date</label>
                        <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()'></input>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="ProjectDetailSelectByDate();" id="SearchButton" title="Search">Search</button>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table id="ProjectDetailMasterDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                    <thead></thead>
                    <tbody></tbody>

                </table>
            </div>
        </section>

        <div class="modal fade" id="modal_attachment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="H3">Attachment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid alltrans">
                            <div class="table-responsive">
                                <table id="Attachments" class="table table-bordered nowrap table-hover" cellspacing="0" width="100%">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


    </div>




    <script src="/JS/ViewProjectDetailsForEmployee.js"></script>
    <script>
       
        $(document).ready(function () {

            $("#inputFromDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });


         
                var Todate = $("#inputFromDate").val();
                $("#inputToDate").datepicker({
                    dateFormat: 'd MM, yy',
                    changeMonth: true,
                    changeYear: true,
                    mindate: Todate,
                });
                   
           
           
            

            if (window.location.href.indexOf("ProjectDetailMasterID") > -1) {
                var url = window.location.href;
                var id = url.split('=')
                ProjectDetailMasterSelectForEmployee(id[1]);
            }
            else {
                ProjectDetailMasterSelectAllForEmployee();
            }
        });
    </script>
</asp:Content>
