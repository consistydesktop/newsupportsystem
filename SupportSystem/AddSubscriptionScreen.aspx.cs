﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Web.Services;

namespace SupportSystem
{
    public partial class AddSubscriptionScreen : System.Web.UI.Page
    {
        private static string pageName = "AddSubscriptionScreen.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static AddSubscriptionScreenModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);

            List<AddSubscriptionScreenModel> details = new List<AddSubscriptionScreenModel>();
            AddSubscriptionScreenBal objBAL = new AddSubscriptionScreenBal();
            try
            {
                details = objBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static AddSubscriptionScreenModel[] SelectDesktop()
        {
            AccessController.checkAccess(pageName);

            List<AddSubscriptionScreenModel> details = new List<AddSubscriptionScreenModel>();
            AddSubscriptionScreenBal objBAL = new AddSubscriptionScreenBal();
            try
            {
                details = objBAL.SelectDesktop();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Insert(int SystemID, int DesktopID, string Description, string DemoYear, string PaymentStatus)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            AddSubscriptionScreenModel objModel = new AddSubscriptionScreenModel();

            int i = -1;

            objModel.SystemID = SystemID;
            objModel.DesktopID = DesktopID;
            objModel.Description = Description;
            objModel.DemoYear = DemoYear;
            objModel.PaymentStatus = PaymentStatus;

            AddSubscriptionScreenBal objBAL = new AddSubscriptionScreenBal();
            try
            {
                i = objBAL.Insert(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "Already exist system and desktop type.";
            else
                msgIcon.Message = "Subscription added successfully.";
            return msgIcon;
        }

        [WebMethod]
        public static AddSubscriptionScreenModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<AddSubscriptionScreenModel> details = new List<AddSubscriptionScreenModel>();

            AddSubscriptionScreenBal objBAL = new AddSubscriptionScreenBal();
            try
            {
                details = objBAL.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon Delete(int SubscriptionID)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            AddSubscriptionScreenBal objBal = new AddSubscriptionScreenBal();

            try
            {
                i = objBal.Delete(SubscriptionID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i > 0)
                msgIcon.Message = "Subscription  record not deleted";
            else
                msgIcon.Message = "Subscription record deleted Successfully";

            return msgIcon;
        }

        [WebMethod]
        public static AddSubscriptionScreenModel SelectByID(int SubscriptionID)
        {
            AccessController.checkAccess(pageName);
            AddSubscriptionScreenModel Register = new AddSubscriptionScreenModel();

            AddSubscriptionScreenBal objBal = new AddSubscriptionScreenBal();

            try
            {
                Register = objBal.SelectByID(SubscriptionID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Register;
        }

        [WebMethod]
        public static MessageWithIcon Update(int SystemID, int DesktopID, string Description, string DemoYear, string PaymentStatus, int SubscriptionID)
        {
            AccessController.checkAccess(pageName);
            AddSubscriptionScreenModel objModel = new AddSubscriptionScreenModel();
            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            objModel.SubscriptionID = SubscriptionID;
            objModel.SystemID = SystemID;
            objModel.DesktopID = DesktopID;
            objModel.Description = Description;
            objModel.DemoYear = DemoYear;
            objModel.PaymentStatus = PaymentStatus;

            AddSubscriptionScreenBal objBAL = new AddSubscriptionScreenBal();
            try
            {
                i = objBAL.Update(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i < 0)
                msgIcon.Message = "Subscription not updated";
            else
                msgIcon.Message = "Subscription updated successfully";

            return msgIcon;
        }
    }
}