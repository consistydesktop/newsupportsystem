﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class TimeSheetReport : System.Web.UI.Page
    {
        private static string pageName = "TimeSheetReport.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {

            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static TimeSheetModel[] Select()
        {

            AccessController.checkAccess(pageName);

            List<TimeSheetModel> details = new List<TimeSheetModel>();
            TimeSheetReportBal ObjBAL = new TimeSheetReportBal();
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
         
            try
            {
                details = ObjBAL.Select(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static TimeSheetModel[] Search(string FromDate, string ToDate)
        {
              
            AccessController.checkAccess(pageName);

            List<TimeSheetModel> details = new List<TimeSheetModel>();
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            TimeSheetReportBal ObjBAL = new TimeSheetReportBal();

            TimeSheetModel ObjModel = new TimeSheetModel();

            ObjModel.FormDate = FromDate;
            ObjModel.Todate = ToDate;
            ObjModel.UserID = UserID;

            try
            {
                details = ObjBAL.Search(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static TimeSheetModel[] SelectByID(string Date)
        {
              
            AccessController.checkAccess(pageName);

            List<TimeSheetModel> ObjModel = new List<TimeSheetModel>();
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
           // int UserID = 45;
            TimeSheetReportBal objBal = new TimeSheetReportBal();
            try
            {
                ObjModel = objBal.SelectByID(Date, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return ObjModel.ToArray();
        }
    }
}