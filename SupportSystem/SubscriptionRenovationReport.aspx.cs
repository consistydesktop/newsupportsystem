﻿using Bal;
using Model;
using Newtonsoft.Json;
using SupportProjectEmployee.Controller;
using SupportSystem.Controller;
using SupportSystem.PG;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class SubscriptionRenovationReport : System.Web.UI.Page
    {
        private static string pageName = "SubscriptionRenovationReport.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static PGModel[] PGSelectDatewise(string FromDate, string ToDate, string Status)
        {
            List<PGModel> details = new List<PGModel>();
            try
            {

                string UserID = HttpContext.Current.Session["UserID"].ToString();
                PGModel objModel = new PGModel();
                objModel.UserID = Convert.ToInt32(UserID);
                objModel.Date = FromDate;
                objModel.ToDate = ToDate;

                objModel.status = Status;

                SubscritpionRenovationBal objPgbal = new SubscritpionRenovationBal();
                DataTable dt = objPgbal.PGReportDateWiseSelect(objModel);
                foreach (DataRow dtrow in dt.Rows)
                {
                    PGModel report = new PGModel();
                    report.DisplayDate = dtrow["DisplayDOC"].ToString();
                    report.UserName = dtrow["UserName"].ToString();
                    report.SystemName = dtrow["SystemName"].ToString();
                    report.MobileNumber = dtrow["MobileNumber"].ToString();
                    report.Amount = Convert.ToDecimal(dtrow["Amount"].ToString());
                    report.status = dtrow["STATUS"].ToString();
                    report.OrderId = dtrow["OrderID"].ToString();
                    report.TransactionID = dtrow["TransactionID"].ToString();
                    report.gatewayName = dtrow["GateWay"].ToString();
                    report.PayMode = dtrow["Paymode"].ToString();
                    report.response = dtrow["Response"].ToString();
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static string StatusCheck(string OrderID)
        {
            string Message = string.Empty;
            // string pageName = "PGReport.aspx";
            // AccessControl.checkAccess(pageName);

            CheckStatusOrderId(OrderID);
            Message = string.Format(" Status Checked for OrderID: {0}", OrderID);
            return Message;


        }


        [WebMethod]
        public static void CheckStatusOrderId(string OrderID)
        {
            Message msg = new Message();
            string json = new Message().failure;
            try
            {
                SubscritpionRenovationBal objPGBal = new SubscritpionRenovationBal();

                DataTable dt = objPGBal.Select(OrderID);

                if (dt == null)
                {
                    return;
                }

                if (dt.Rows.Count <= 0)
                {
                    return;
                }

                if (!dt.Rows[0]["status"].ToString().Equals(new Message().pending, StringComparison.InvariantCultureIgnoreCase))
                {
                    return;
                }

                int DBUserId = Convert.ToInt32(dt.Rows[0]["UserID"].ToString());
                decimal DBAmount = Convert.ToDecimal(dt.Rows[0]["Amount"].ToString());


                HttpProcessing processing = new HttpProcessing();

                string Response = processing.GetMethodForPayOut(OrderID);

                RazorpayStatusCheckResponse obj = new RazorpayStatusCheckResponse();
                obj = JsonConvert.DeserializeObject<RazorpayStatusCheckResponse>(Response);

                int result = 0;
                PGModel objModel = new PGModel();

                if (obj.items.Count <= 0)
                {
                    objModel.TransactionID = "";
                    objModel.status = new Message().failure;
                    objModel.OrderId = OrderID;
                    objModel.Amount = DBAmount;
                    objModel.PaymentGatewayID = 1;
                    objModel.PayMode = "";
                    objModel.PaymentId = "";
                    objModel.response = Response;
                    objModel.SystemID = Convert.ToInt32(dt.Rows[0]["SystemID"].ToString());
                    result = objPGBal.Update(objModel);
                    return;
                }


                string pgStatus = obj.items[0].status;
                int amt = obj.items[0].amount / 100;
                objModel.TransactionID = obj.items[0].acquirer_data.rrn;
                if (objModel.TransactionID == null)
                {
                    objModel.TransactionID = obj.items[0].acquirer_data.auth_code;
                }
                if (objModel.TransactionID == null)
                {
                    objModel.TransactionID = obj.items[0].acquirer_data.bank_transaction_id;
                }
                if (pgStatus.Equals("captured", StringComparison.InvariantCultureIgnoreCase) || pgStatus.Equals("authorized", StringComparison.InvariantCultureIgnoreCase))
                {
                    objModel.status = new Message().success;
                }
                else if (pgStatus.Equals("failed", StringComparison.InvariantCultureIgnoreCase))
                {
                    objModel.TransactionID = "";
                    objModel.status = new Message().failure;
                }
                else
                {
                    objModel.status = new Message().pending;
                }

                if (objModel.status.Equals(new Message().pending, StringComparison.InvariantCultureIgnoreCase))
                {
                    return;
                }

                objModel.OrderId = OrderID;
                objModel.Amount = Convert.ToDecimal(amt);
                objModel.PaymentGatewayID = 1;
                objModel.PayMode = obj.items[0].method;
                objModel.TransactionID = objModel.TransactionID;
                objModel.PaymentId = obj.items[0].id;
                objModel.response = Response;

                result = objPGBal.Update(objModel);

                if (result <= 0)
                {
                    return;
                }

                if (objModel.status.Equals(new Message().failure, StringComparison.InvariantCultureIgnoreCase))
                {
                    return;
                }

                decimal statusAmount = Convert.ToDecimal(amt);

                if (DBAmount != statusAmount)
                {
                    return;
                }

                LoadMoneyModel objLoadMoneyUPI = new LoadMoneyModel();
                objLoadMoneyUPI.Amount = objModel.Amount;
                objLoadMoneyUPI.Month = dt.Rows[0]["Month"].ToString();
                objLoadMoneyUPI.UserID = DBUserId;
                objLoadMoneyUPI.PaytmTransactionID = objModel.TransactionID;
                objLoadMoneyUPI.Remark = "Payment received from Consisty System PG";
                objLoadMoneyUPI.Paymode = objModel.PayMode;
                objLoadMoneyUPI.SystemID = Convert.ToInt32(dt.Rows[0]["SystemID"].ToString());
                string response = "OK";
                if (objModel.status.Contains(new Message().success))
                {
                    PGProcessing objPaytmProcessing = new PGProcessing();
                    objPaytmProcessing.ExtendSubscriptionProcessing(objLoadMoneyUPI);
                }

                return;
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
                json = JsonConvert.SerializeObject(msg.ExceptionOccurred);
                return;
            }
        }



    }
}