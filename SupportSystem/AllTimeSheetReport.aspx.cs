﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class AllTimeSheetReport : System.Web.UI.Page
    {
        private static string pageName = "AllTimeSheetReport.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static AllTimeSheetReportModel[] SelectEmployeeName()
        {
            AccessController.checkAccess(pageName);

            List<AllTimeSheetReportModel> details = new List<AllTimeSheetReportModel>();
            AllTimeSheetReportBal ObjBAL = new AllTimeSheetReportBal();
            try
            {
                details = ObjBAL.SelectEmployeeName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static AllTimeSheetReportModel[] Select()
        {
            AccessController.checkAccess(pageName);

            List<AllTimeSheetReportModel> details = new List<AllTimeSheetReportModel>();
            AllTimeSheetReportBal ObjBAL = new AllTimeSheetReportBal();
            try
            {
                details = ObjBAL.Select();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static AllTimeSheetReportModel[] Search(int UserID,string FromDate, string ToDate)
        {

            AccessController.checkAccess(pageName);

            List<AllTimeSheetReportModel> details = new List<AllTimeSheetReportModel>();
            AllTimeSheetReportBal ObjBAL = new AllTimeSheetReportBal();

            AllTimeSheetReportModel ObjModel = new AllTimeSheetReportModel();

            ObjModel.FromDate = FromDate;
            ObjModel.ToDate = ToDate;
            ObjModel.UserID = UserID;

            try
            {
                details = ObjBAL.Search(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static AllTimeSheetReportModel[] SelectTimeSheetReportByEmployeeName(string Date, int UserID)
        {
            AccessController.checkAccess(pageName);

           
            List<AllTimeSheetReportModel> details = new List<AllTimeSheetReportModel>();
            AllTimeSheetReportBal ObjBAL = new AllTimeSheetReportBal();
            try
            {
                details = ObjBAL.SelectTimeSheetReportByEmployeeName(Date, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }
        
    }
}