﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class ViewTicketEmployee : System.Web.UI.Page
    {
        private static string pageName = "ViewTicketEmployee.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
              
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static ViewTicketEmployeeModel[] Select()
        {
              
            AccessController.checkAccess(pageName);
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            List<ViewTicketEmployeeModel> details = new List<ViewTicketEmployeeModel>();
            ViewTicketEmployeeBal objBal = new ViewTicketEmployeeBal();
            try
            {
                details = objBal.Select(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static ViewTicketEmployeeModel[] Search(string FromDate, string ToDate)
        {
              
            AccessController.checkAccess(pageName);
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            List<ViewTicketEmployeeModel> details = new List<ViewTicketEmployeeModel>();

            ViewTicketEmployeeBal objBal = new ViewTicketEmployeeBal();
            try
            {
                details = objBal.Search(FromDate, ToDate, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static ViewTicketEmployeeModel[] SelectByTicketID(int TicketID)
        {
              
            AccessController.checkAccess(pageName);
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            List<ViewTicketEmployeeModel> details = new List<ViewTicketEmployeeModel>();
            ViewTicketEmployeeBal objBAL = new ViewTicketEmployeeBal();
            try
            {
                details = objBAL.SelectByTicketID(TicketID, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

        [WebMethod]
        public static ViewTicketEmployeeModel[] SelectAttachment(int TicketID)
        {
              
            AccessController.checkAccess(pageName);
            ViewTicketEmployeeModel objModel = new ViewTicketEmployeeModel();
            List<ViewTicketEmployeeModel> details = new List<ViewTicketEmployeeModel>();
            objModel.TicketID = TicketID;
            ViewTicketEmployeeBal objBAL = new ViewTicketEmployeeBal();

            try
            {
                details = objBAL.SelectAttachment(objModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }

    }
}