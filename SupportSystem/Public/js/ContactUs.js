﻿
function EmailValidation(Email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(Email)) {
        return false;
    } else {
        return true;
    }
}

function ContactDetails() {
    var Name = $("#txtname").val();
    var Email = $("#txtemail").val();
    var MobileNumber = $("#txtmob").val();
    var Message = $("#txtmessage").val();

    if (Name == "") {
        $.alert.open({
            content: String.format("Please enter customer name"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtname").focus();
            }
        });
        return false;
    }

    if (EmailID == "") {
        $.alert.open({
            content: String.format("Please enter a email address"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtemail").focus();
            }
        });

        return false;
    }
    if (EmailID != "") {
        if (!EmailValidation(EmailID)) {
            $.alert.open({
                content: String.format("Please enter a valid email address"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#txtemail").focus();
                }
            });
            return false;
        }

        if (MobileNumber == "") {
            $.alert.open({
                content: String.format("Please enter mobile number"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#txtmob").focus();
                }
            });
            return false;
        }

        if (MobileNumber.length != 10) {
            $.alert.open({
                content: String.format("Please enter valid mobile number"), icon: 'warning', title: ClientName,
                callback: function () {
                    $("#txtmob").focus();
                }
            });
            return false;
        }
    }
    if (Message == "") {
        $.alert.open({
            content: String.format("Please enter customer name"), icon: 'warning', title: ClientName,
            callback: function () {
                $("#txtmessage").focus();
            }
        });
        return false;
    }
    var jsonObj = {
        Name: Name,
        EmailID: EmailID,
        MobileNumber: MobileNumber,
        Message: Message
    };
    var jData = JSON.stringify(jsonObj)

    $.alert.open({
        type: 'confirm',
        content: 'Are you want to submit',
        icon: 'warning',
        title: ClientName,
        callback: function (button) {
            if (button == null) {
                return false;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ContactUs.aspx/Insert",
                data: jData,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    Clear();
                },
                error: function (result) {
                    $.alert.open({
                        content: String.format("Somthing Worng"), icon: 'error', title: ClientName,
                    });
                }
            });
            return false;
        }
    });
}

function Clear() {
    $("#txtname").val("");
    $("#txtemail").val("");
    $("#txtmob").val("");
    $("#txtmessage").val("");
}
