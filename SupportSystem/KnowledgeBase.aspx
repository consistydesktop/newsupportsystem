﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="KnowledgeBase.aspx.cs" Inherits="SupportSystem.KnowledgeBase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">

        <section class="formhead">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <h6>Knowledge Base </h6>
                        <button class="btn btn-info buttons float-right" type="button" onclick="location.href='/KnowledgeBaseReport.aspx'">View</button>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <form class="formbox mt-4">

                    <div class="form-group row mt-3">
                        <div class="col-12">
                            <label for="inputSystemName">System Name</label>
                            <select class="form-control loginput" id="inputSystemName">
                            </select>
                        </div>
                    </div>




                    <div class="form-group row mt-3">
                        <div class="col-12">
                            <label for="inputknowledgebase">Issue</label>
                            <input type="text" class="form-control loginput" id="inputIssue" title="Knowledge Base" aria-describedby="emailHelp" placeholder="Issue" />
                        </div>
                    </div>

                    <div class="form-group row mt-3">
                        <div class="col-12">
                            <label for="inputResolution">Resolution</label>
                            <textarea class="form-control loginput"  id="inputResolution" aria-describedby="emailHelp" placeholder="Enter Resolution" title="Enter Resolution" maxlength="3900" " onkeypress ="CheckLength('inputResolution','countchar');"></textarea></br>
                            <h6>you have entered<span id="countchar" style="color:red", "font-size : 10px;">0</span>characters  !</h6>

                        </div>
                    </div>


                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return KnowledgeBaseInsert();" id="SaveButton" title="Save">Save</button>
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return KnowledgeBaseUpdate();" id="UpdateButton" title="Update Knowledge Base" style="display: none">Update</button>
                                <input type="hidden" id="hdnKnowledgeBaseID">
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton buttons" onclick="return KnowledgeBaseResetAll();">Reset</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-4"></div>
        </div>

    </div>
    <script src="JS/KnowledgeBase.js"></script>

    <script>
        $(document).ready(function () {
            $("#inputSystemName").focus();
            SystemMasterSelect();
        });

        $(document).ready(function () {
            $("inputSystemName").focus();
            $("#inputIssue").focus();
            $("#inputResolution").focus();

            $(document).ready(function () {
                $('#inputResolution').on('input propertychange', function () {
                    CharLimit(this, 3900);
                });
            });

            function CharLimit(input, maxChar) {
                var len = $(input).val().length;
                $('#countchar').text(maxChar - len + ' characters remaining');

                if (len > maxChar) {
                    $(input).val($(input).val().substring(0, maxChar));
                    $('#countchar').text(0 + ' characters remaining');
                }
            }

            SystemMasterSelect();

            if (window.location.href.indexOf("KnowledgeBaseID") > -1) {
                var url = window.location.href;
                var id = url.split('=');
                $("#hdnKnowledgeBaseID").val(id[1]);
                KnowledgeBaseSelectByID(id[1]);
                $("#SaveButton").css('display', 'none');
                $("#UpdateButton").show();

            }
        });
    </script>
</asp:Content>
