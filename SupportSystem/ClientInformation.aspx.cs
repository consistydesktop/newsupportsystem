﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class ClientInformation : System.Web.UI.Page
    {

        private static string pageName = "ClientInformation.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static ClientModel[] SelectClient()
        {
            AccessController.checkAccess(pageName);
            List<ClientModel> report = new List<ClientModel>();


            ClientInformationBal objBal = new ClientInformationBal();
            DataTable dt = new DataTable();
            try
            {
                dt = objBal.SelectClient();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }


            foreach (DataRow dtrow in dt.Rows)
            {
                ClientModel Register = new ClientModel();
                Register.UserID = Convert.ToInt32(dtrow["UserID"].ToString().Trim());
                Register.Name = dtrow["Name"].ToString().Trim();
                Register.Username = dtrow["Username"].ToString().Trim();
                Register.EmailID = dtrow["EmailID"].ToString().Trim();
                Register.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
                Register.GSTNo = dtrow["GSTNo"].ToString().Trim();
                Register.PANNo = dtrow["PANNo"].ToString().Trim();
                Register.Address = dtrow["Address"].ToString().Trim();
                Register.State = dtrow["State"].ToString().Trim();
                Register.IsActive = Convert.ToInt32(dtrow["IsActive"]);
                report.Add(Register);
            }

            return report.ToArray();

        }





        [WebMethod]
        public static string UpdateAciveUser(int UserID, int IsActive)
        {

            AccessController.checkAccess(pageName);

            string ReturnMessage = "User not updated.";
            if (UserID == 0)
            {
                return ReturnMessage;
            }
            int Result = -1;
            ClientInformationBal objBal = new ClientInformationBal();
            try
            {
                Result = objBal.UpdateAciveUser(UserID, IsActive);
            }
            catch (Exception ex)
            {

            }

            if (Result > 0)
                ReturnMessage = "User updated successfully..";



            return ReturnMessage;
        }



        [WebMethod]
        public static ClientModel UserInformationSelectClientByID(int UserID)
        {
            AccessController.checkAccess(pageName);


            ClientInformationBal objBal = new ClientInformationBal();
            DataTable dt = new DataTable();
            try
            {
                dt = objBal.UserInformationSelectClientByID(UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }



            ClientModel Register = new ClientModel();

            if (dt.Rows.Count <= 0)
                return Register;

            Register.UserID = Convert.ToInt32(dt.Rows[0]["UserID"].ToString().Trim());
            Register.Name = dt.Rows[0]["Name"].ToString().Trim();
            Register.Username = dt.Rows[0]["Username"].ToString().Trim();
            Register.EmailID = dt.Rows[0]["EmailID"].ToString().Trim();
            Register.MobileNumber = dt.Rows[0]["MobileNumber"].ToString().Trim();
            Register.GSTNo = dt.Rows[0]["GSTNo"].ToString().Trim();
            Register.PANNo = dt.Rows[0]["PANNo"].ToString().Trim();
            Register.Address = dt.Rows[0]["Address"].ToString().Trim();
            Register.State = dt.Rows[0]["State"].ToString().Trim();
            Register.IsActive = Convert.ToInt32(dt.Rows[0]["IsActive"]);
            return Register;

        }
    }
}