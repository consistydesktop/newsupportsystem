﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddHoliday.aspx.cs" Inherits="SupportSystem.AddHoliday" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Add Holidays
                               
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">
                <div class="form-group row mt-4">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputDate"><b>Date</b></label>
                        <input type="text" autocomplete="off" class="form-control loginput" id="inputDate" title="Select Date" aria-describedby="emailHelp" placeholder="Add Date" onchange='$("#inputDescription").focus()' />
                    </div>

                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                        <label for="inputDescription"><b>Description (Optional)</b></label>
                        <textarea class="form-control loginput" id="inputDescription" title="Enter Description" aria-describedby="emailHelp" placeholder="Enter Description" onchange='$("#SubmitButton").focus()'></textarea>
                    </div>



                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return HolidayMasterInsert();" id="SubmitButton" title="Add Holyday">ADD</button>
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="return HolidayMasterUpdate();" id="UpdateButton" title="Update HoliDay" style="display: none;">Update</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-danger w-100 mt-4 logbutton cancelbtn" onclick=" Reset();" title="Cancel Reminder">Cancel</button>
                            </div>
                        </div>
                        <label id="ResponseMsgLbl"></label>
                    </div>
                </div>
                <label id="HolidayID" style="display: none"></label>


            </form>
            <div class="table-responsive">
                <table id="HolidayDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                    <thead></thead>
                    <tbody></tbody>
                </table>
            </div>
        </section>
    </div>

    <script src="/JS/AddHoliday.js"></script>

    <script>

        $("#inputDate").datepicker({
            dateFormat: 'd MM, yy',
            changeMonth: true,
            changeYear: true
        });


        HolidayMasterSelect();
    </script>

</asp:Content>
