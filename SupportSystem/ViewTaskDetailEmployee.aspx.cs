﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using SupportSystem.Controller;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class ViewTaskDetailEmployee : System.Web.UI.Page
    {
        private static string pageName = "ViewTaskDetailEmployee.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static AssignTaskModel[] SelectEmployeeName()
        {
            AccessController.checkAccess(pageName);

            List<AssignTaskModel> details = new List<AssignTaskModel>();
            ViewTaskDetailEmployeeBal ObjBAL = new ViewTaskDetailEmployeeBal();
            try
            {
                details = ObjBAL.SelectEmployeeName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static List<List<AssignTaskModel>> SelectByID(int TaskID)
        {
            AccessController.checkAccess(pageName);

            List<List<AssignTaskModel>> details = new List<List<AssignTaskModel>>();
            ViewTaskDetailEmployeeBal ObjBAL = new ViewTaskDetailEmployeeBal();
            try
            {
                details = ObjBAL.SelectByID(TaskID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }

        [WebMethod]
        public static AssignTaskModel[] SelectAttachment(int TaskID)
        {
            AccessController.checkAccess(pageName);

            List<AssignTaskModel> details = new List<AssignTaskModel>();
            ViewTaskDetailEmployeeBal ObjBAL = new ViewTaskDetailEmployeeBal();
            try
            {
                details = ObjBAL.SelectAttachment(TaskID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static MessageWithIcon InsertComment(int TaskID, string Comment)
        {
            MessageWithIcon msgIcon = new MessageWithIcon();
            int Result = -1;
            ViewTaskDetailEmployeeBal ObjBal = new ViewTaskDetailEmployeeBal();

            AssignTaskModel ObjModel = new AssignTaskModel();
            ObjModel.TaskID = TaskID;
            ObjModel.AssignedUserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            ObjModel.Comment = Comment;
            ObjModel.CommentType = CommentTypeMaster.TaskCommentType;
            try
            {
                Result = ObjBal.InsertComment(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (Result > -1)
                msgIcon.Message = "Comment added successfully";
            else
                msgIcon.Message = "Comment not added";

            return msgIcon;
        }

        [WebMethod]
        public static MessageWithIcon UpdateSubTaskStatus(int SubTaskID, int SubTaskStatus)
        {
            MessageWithIcon msgIcon = new MessageWithIcon();
            int Result = -1;
            ViewTaskDetailEmployeeBal ObjBal = new ViewTaskDetailEmployeeBal();

            AssignTaskModel ObjModel = new AssignTaskModel();
            ObjModel.SubTaskID = SubTaskID;
            ObjModel.SubTaskStatus = SubTaskStatus;
            try
            {
                Result = ObjBal.UpdateSubTaskStatus(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (Result > -1)
                msgIcon.Message = "Status updated successfully";
            else
                msgIcon.Message = "Status not added";

            return msgIcon;
        }

        [WebMethod]
        public static MessageWithIcon UpdateTaskStatus(int TaskID, string Status)
        {
            MessageWithIcon msgIcon = new MessageWithIcon();
            int Result = -1;
            ViewTaskDetailEmployeeBal ObjBal = new ViewTaskDetailEmployeeBal();

            AssignTaskModel ObjModel = new AssignTaskModel();
            ObjModel.TaskID = TaskID;
            ObjModel.TaskStatus = Status;
            try
            {
                Result = ObjBal.UpdateTaskStatus(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (Result > -1)
                msgIcon.Message = "Status updated successfully";
            else
                msgIcon.Message = "Status not added";

            return msgIcon;
        }
    }
}