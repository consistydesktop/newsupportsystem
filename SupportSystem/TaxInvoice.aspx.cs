﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Linq;
namespace SupportSystem
{

    public partial class TaxInvoice : System.Web.UI.Page
    {
        private static string pageName = "TaxInvoice.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static TaxInvoiceModel[] SelectInvoiceReport(string FromDate, string ToDate, int UserID, int SystemID)
        {
            List<TaxInvoiceModel> details = new List<TaxInvoiceModel>();

            try
            {
                AccessController.checkAccess(pageName);


                InvoiceModel request = new InvoiceModel();
                request.UserID = UserID;
                request.FromDate = FromDate;
                request.ToDate = ToDate;
                request.SystemID = SystemID;


                if (UserID != 0)
                {
                    request.ReceiverID = UserID;
                }
                else
                {
                    request.ReceiverID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                }

                TaxInvoiceBal objBAL = new TaxInvoiceBal();
                DataTable dt = objBAL.SelectSearch(request);

                if (dt.Rows.Count <= 0)
                {
                    return details.ToArray();
                }

                foreach (DataRow dtrow in dt.Rows)
                {
                    TaxInvoiceModel report = new TaxInvoiceModel();


                    report.Amount = Convert.ToDecimal(dtrow["PurchaseAmount"]);
                    report.Charge = Convert.ToDecimal(dtrow["Charge"]);
                    report.GST = Convert.ToDecimal(dtrow["GST"]);
                    report.ComOnGST = Convert.ToDecimal(dtrow["ComOnGST"]);
                    report.TDS = Convert.ToDecimal(dtrow["TDS"]);
                    report.Address = dtrow["Address"].ToString();
                    report.UserName = dtrow["UserName"].ToString();
                    report.Receiver = dtrow["Receiver"].ToString();
                    report.GSTNumber = dtrow["GSTNo"].ToString();
                    report.SystemName = dtrow["SystemName"].ToString();
                    report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                    report.State = dtrow["State"].ToString();


                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }



        [WebMethod]
        public static TaxInvoiceViewModel[] SelectInvoiceReportForPrint(string FromDate, string ToDate, int UserID, int SystemID)
        {
            List<TaxInvoiceViewModel> details = new List<TaxInvoiceViewModel>();

            try
            {
                AccessController.checkAccess(pageName);


                InvoiceModel request = new InvoiceModel();
                request.UserID = UserID;
                request.FromDate = FromDate;
                request.ToDate = ToDate;
                request.SystemID = SystemID;


                if (UserID != 0)
                {
                    request.ReceiverID = UserID;
                }
                else
                {
                    request.ReceiverID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                }

                TaxInvoiceBal objBAL = new TaxInvoiceBal();
                DataTable dt = objBAL.SelectInvoiceReportForPrint(request);

                if (dt.Rows.Count <= 0)
                {
                    return details.ToArray();
                }

                foreach (DataRow dtrow in dt.Rows)
                {
                    TaxInvoiceViewModel report = new TaxInvoiceViewModel();
                    report.State = dt.Rows[0]["State"].ToString();
                    report.Amount = Convert.ToDecimal(dt.Rows[0]["PurchaseAmount"]);

                    report.AmountWithGST = 0;


                    if (!report.State.Equals("Maharashtra", StringComparison.InvariantCultureIgnoreCase))
                        report.AmountWithGST = (report.Amount * 18) / 100;


                    report.AmountTotal = report.Amount + report.AmountWithGST;
                    report.Address = dt.Rows[0]["Address"].ToString();
                    report.UserName = dt.Rows[0]["UserName"].ToString();
                    report.Receiver = dt.Rows[0]["Receiver"].ToString();
                    report.GSTNumber = dt.Rows[0]["GSTNo"].ToString();
                    report.SystemName = dt.Rows[0]["SystemName"].ToString();
                    string ProductName = dt.Rows[0]["ProductName"].ToString();
                    string Date = DateTime.Now.ToString("yyyyMMdd");
                    string RandomNumber = GetRandomNumber().ToString();
                    report.InvoiceNumber = ProductName + "_" + Date + "_" + RandomNumber;

                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }
        public static string GetRandomNumber()
        {
            var getNumbers = (from t in Guid.NewGuid().ToString()
                              where char.IsDigit(t)
                              select t).ToArray();

            string sttNumber = string.Join("", getNumbers);

            sttNumber = sttNumber.TrimStart(new Char[] { '0' });

            return (sttNumber.Length >= 6) ? sttNumber.Substring(0, 5) : sttNumber;
        }
    }
}