﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class KnowledgeBase : System.Web.UI.Page
    {
        private static string pageName = "KnowledgeBase.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }
        [WebMethod]
        public static KnowledgeBaseModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);
            List<KnowledgeBaseModel> details = new List<KnowledgeBaseModel>();
            KnowledgeBaseBal objBAL = new KnowledgeBaseBal();
            try
            {
                details = objBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();

        }

        static string  IsValidInsert(string Issue, string Resolution, int SystemID)
        {
            string msg ="";
            if (Issue == null)
            {
                return msg="invalid Data";
            }
            if (Resolution == null)
            {
                return msg="Invalid Data";
            }
            if (SystemID == null)
            {
                return msg = "Invalid Data"; 
            }
           
            if (Resolution.Length >=3900)
            {
                return msg="Resolution must contain less than 3900 character";
            }

            return msg="ok";
        }

        [WebMethod]
        public static MessageWithIcon Insert(string Issue, string Resolution, int SystemID)
        {
            AccessController.checkAccess(pageName);
            string msg = IsValidInsert(Issue, Resolution, SystemID);
            MessageWithIcon msgIcon = new MessageWithIcon();
            if (msg!="ok")
            {
                msgIcon.Message = msg ;
            }
            msgIcon.Message = "Issue not submitted";
            int result = -1;
            int UserID = -1;
            try
            {
                UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());

                KnowledgeBaseModel ObjModel = new KnowledgeBaseModel();
                ObjModel.UserID = UserID;
                ObjModel.Issue = Issue;
                ObjModel.Resolution = Resolution;
                ObjModel.SystemID = SystemID;

                KnowledgeBaseBal ObjBAL = new KnowledgeBaseBal();
                result = ObjBAL.Insert(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result > 0)
            {
                msgIcon.Message = "Issue submitted successfully";
            }
            return msgIcon;
        }

        [WebMethod]
        public static List<List<KnowledgeBaseModel>> SelectByID(int KnowledgeBaseID)
        {
            AccessController.checkAccess(pageName);

            List<List<KnowledgeBaseModel>> details = new List<List<KnowledgeBaseModel>>();
            KnowledgeBaseBal ObjBAL = new KnowledgeBaseBal();

            try
            {
                details = ObjBAL.SelectByID(KnowledgeBaseID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }


        [WebMethod]
        public static MessageWithIcon UpdateByID(int KnowledgeBaseID, string Issue, string Resolution, int SystemID)
        {
            KnowledgeBaseBal ObjBAL = new KnowledgeBaseBal();
            MessageWithIcon msgIcon = new MessageWithIcon();

            int Result = -1;
            try
            {
                KnowledgeBaseModel ObjModel = new KnowledgeBaseModel();
                ObjModel.KnowledgeBaseID = KnowledgeBaseID;
                ObjModel.Issue = Issue;
                ObjModel.Resolution = Resolution;
                ObjModel.SystemID = SystemID;

                Result = ObjBAL.UpdateByID(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (Result > 0)
                msgIcon.Message = " Updated successufully.";
            else
                msgIcon.Message = "Not updated";
            return msgIcon;

        }
    }
}