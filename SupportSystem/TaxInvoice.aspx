﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TaxInvoice.aspx.cs" Inherits="SupportSystem.TaxInvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style type="text/css">
        p {
            margin-bottom: 0;
            text-align: left;
        }

        .innertable {
            margin-bottom: 0px;
        }

        .table > thead > tr > th {
            vertical-align: middle;
        }

        table.tablelist tr td {
            vertical-align: middle;
        }

            table.tablelist tr td h4 {
                margin: 0px;
                font-weight: bold;
                font-size: 16px;
            }

        table.tablelist tr th h4 {
            margin: 0px;
            font-weight: bold;
            font-size: 16px;
        }

        .ui-widget.ui-widget-content {
            width: 1000px;
            top: 124px;
            left: 447.5px;
            z-index: 101;
        }
    </style>
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Subscritpion Tax Invoice 
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <div class="form-group row mt-3">
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                    <label for="inputFromDate">From Date </label>
                    <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                </div>

                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                    <label for="inputToDate">To Date</label>
                    <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()' />
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12" id="divCustomer" style="display: none;">
                    <label for="inputClientName">Customer</label>
                    <select class="form-control loginput" id="inputClientName" onchange="return  SystemMasterSelect();" title="Client Name"></select>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <label for="inputSystemName">System Name</label>
                    <select class="form-control loginput" id="inputSystemName" title="Select System">
                    </select>
                </div>

                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="SelectSearch();" id="SearchButton" title="Search">Search</button>
                        </div>
                    </div>
                    <label id="ResponseMsgLbl"></label>
                </div>
            </div>


            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content" id="Div1">
                            <div class="tab-pane fade show active" id="Div2" role="tabpanel" aria-labelledby="projects-tab">
                                <div class="table-responsive">
                                    <table id="tblTaxInvoiceReport" class="table table-bordered nowrap table-hover" style="width: 100%">
                                        <thead>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="loader" id="loader" style="display: none"></div>


        <label style="display: none" id="lblUserID"></label>
    </div>





    <div class="container Invoice" id="DivGSTPrint" style="display: none;">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 class="text-center">Tax Invoice</h3>
                <table class="table-bordered table table-responsive innertable">
                    <tr>
                        <td width="50%" valign="bottom">

                            <table class="table-bordered table" style="margin-bottom: 0px;">
                                <tr>
                                    <td>
                                        <div style="float: left">

                                            <p><strong>Consisty System Pvt Ltd</strong></p>
                                            <p></p>
                                            <p>
                                                Speciality Business Center,Office No-310
3re Floor,Opposite SKP Campus,
Above SBI,Balewadi,Pine-411045
GSTIN/UIN: 27AAFCC6979C1ZF
State Name : Maharashtra, Code : 27
E-Mail : consistyhelp@gmail.com
                                            </p>
                                            <p><strong>CIN:</strong> N/A</p>
                                            <p><strong>PAN:AAFCC6979C</strong> </p>
                                            <p><strong>GSTIN:27AAFCC6979C1ZF </strong> </p>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <div style="float: left">
                                            <p>Buyer</p>
                                            <p><strong id="txtBuyerName"></strong></p>
                                            <p></p>
                                            <p id="txtBuyerAddress">
                                            </p>
                                            <p>
                                                <strong>GSTIN/UIN:</strong>
                                                <strong id="txtGSTIN"></strong>
                                            </p>

                                            <p>
                                                <strong>State Name :</strong>
                                                <strong id="txtStateName"></strong>

                                            </p>


                                        </div>
                                    </td>
                                </tr>

                            </table>

                        </td>
                        <td width="50%">
                            <table class="table-bordered table" style="margin-bottom: 0px;">
                                <tr>
                                    <td>
                                        <p>Invoice No:</p>
                                        <p><strong id="InvoiceNumber"></strong></p>
                                    </td>
                                    <td>
                                        <p>Dated</p>
                                        <p><strong id="Date"></strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>Delivery Note</p>
                                        <p><strong id="DealerCode"></strong></p>
                                    </td>
                                    <td>
                                        <p>Mode/Terms of Payment</p>
                                        <p><strong id="ModePayment"></strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Supplier's Ref.
                                        </p>
                                        <p><strong id="Supplier"></strong></p>
                                    </td>
                                    <td>
                                        <p>
                                            Other Reference(s)
                                        </p>
                                        <p><strong id="OtherReference"></strong></p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <p>
                                            Buyer's Order No.
                                        </p>
                                        <p><strong id="BuyersOrderNo"></strong></p>
                                    </td>
                                    <td>
                                        <p>
                                            Dated
                                        </p>
                                        <p><strong id="Dated"></strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>Despatch Document No.</p>
                                        <p><strong id="DespatchDocument"></strong></p>
                                    </td>
                                    <td>
                                        <p>Delivery Note Date</p>
                                        <p><strong id="DeliveryNoteDate"></strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>Despatched through</p>
                                        <p><strong id="Despatchedthrough"></strong></p>
                                    </td>
                                    <td>
                                        <p>Destination</p>
                                        <p><strong id="Destination"></strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <p>Terms of Delivery</p>


                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="table-bordered table table-responsive innertable">
                    <tr>
                        <th>SI No.</th>
                        <th width="50%">Description of Services</th>
                        <th>HSN/SAC</th>
                        <th>Quantity</th>
                        <th>Rate</th>
                        <th>per</th>
                        <th width="50%">Amount</th>

                    </tr>
                    <tr>
                        <td>
                            <p>1</p>
                        </td>
                        <td width="50%">
                            <p><strong>Software Developement and Support@IGST 18%</strong></p>
                        </td>
                        <td>
                            <p id="HSNSAC">9983</p>
                        </td>
                        <td>
                            <p></p>
                        </td>
                        <td>
                            <p></p>
                        </td>
                        <td>
                            <p></p>
                        </td>
                        <td width="50%">
                            <p id="Developementcost">3,813.56</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p></p>
                        </td>
                        <td>
                            <p><strong>IGST @ 18%</strong></p>
                        </td>
                        <td>
                            <p></p>
                        </td>
                        <td>
                            <p></p>
                        </td>
                        <td>
                            <p>18</p>
                        </td>
                        <td>
                            <p>%</p>
                        </td>
                        <td>
                            <p id="DevelopementcostWithIGST">686.44</p>
                        </td>
                    </tr>



                    <tfoot>
                        <tr>
                            <td></td>
                            <td><strong>Total</strong></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><strong>₹ </strong><strong id="TotalAmountWithIGST">4,500.00 </strong></td>
                        </tr>
                    </tfoot>


                </table>
                <table class="table table-bordered table-responsive innertable">
                    <tr>
                        <td width="100%">
                            <p><strong>Amount Chargeable (in words) : </strong><strong id="AmountChargeableWords"></strong><span class="text-right">E & O.E</span></p>
                            <p></p>
                        </td>
                    </tr>
                </table>



                <table class="table-bordered table table-responsive innertable">
                    <tr>
                        <th width="50%">HSN/SAC</th>
                        <th>Taxable Value</th>
                        <th width="50%" colspan="2">Integrated Tax
                        </th>
                        <th>Total Tax Amount</th>


                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Rate
                        </td>

                        <td>Amount
                        </td>
                        <td></td>
                    </tr>


                    <tr>
                        <td>
                            <p>9983</p>
                        </td>
                        <td>
                            <p id="Servicecost">3,813.56</p>
                        </td>
                        <td>
                            <p>18%</p>
                        </td>
                        <td>
                            <p id="TaxwithPercentage">686.44</p>
                        </td>

                        <td>
                            <p id="TaxwithPercentage1">686.44</p>
                        </td>
                    </tr>



                    <tfoot>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong id="Servicecost1">3,813.56</strong></td>
                            <td></td>
                            <td><strong id="TotalTaxwithPercentage">686.44</strong></td>
                            <td><strong id="TotalTaxwithPercentage1">686.44</strong></td>
                        </tr>
                    </tfoot>
                </table>

                <table class="table table-bordered table-responsive tablelist innertable" id="InvoicePrint">
                </table>

                <table class="table table-bordered table-responsive innertable">
                    <tr>
                        <td width="100%">
                            <p><strong>Tax Amount (in words) :</strong><strong id="TaxAmountChargeableWords"></strong></p>
                            <p></p>
                        </td>
                    </tr>
                </table>
                <table class="table table-bordered table-responsive innertable">
                    <tr>
                        <td>

                            <p class="text-left">
                                <strong>Company's PAN : AAFCC6979C</strong>

                            </p>

                            <table class="table-bordered table">
                                <tr>
                                    <td width="50%">
                                        <p>
                                            <strong>Declaration:</strong> We declare that this invoice shows the actual price of the 
goods described and that all particulars are true and 
correct.
                                        </p>
                                    </td>

                                    <td width="50%">

                                        <p class="text-right">
                                            <strong>For Consisty System Pvt Ltd:</strong>
                                        </p>


                                        <div style="margin-top: 50px;">
                                            <p class="text-right">
                                                Authorised Signatory
                                            </p>
                                        </div>
                                    </td>

                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>
                <p class="text-center">This is a Computer Generated Invoice.</p>
            </div>
        </div>
        <input type="button" value="Print" style="margin-left: 50%;" onclick="printDiv('InvoicePrint')" />
    </div>
    <input type="hidden" id="ID" />

    <input type="hidden" id="uid" />
    <script src="JS/TaxInvoice.js"></script>

    <script src="JS/Validation.js"></script>
    <script>
        $(document).ready(function () {



            var id = ('<%= Session["UserTypeID"] %>');
            $("#ID").text(id);

            var lblUser = ('<%= Session["UserID"] %>');
            $("#lblUserID").text(lblUser);

            var uid = ('<%= Session["UserID"] %>');
            $("#uid").text(uid);


            if (id == 1) {

                $("#divCustomer").css("display", "block");
                UserInformationSelectCustomerName();
            }
            $("#inputFromDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });
            $("#inputToDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true
            });

            var d = new Date();
            var currMonth = d.getMonth();
            var currYear = d.getFullYear();

            var startDate = new Date(currYear, currMonth, 1);
            $("#inputFromDate").datepicker("setDate", startDate);

            $("#inputToDate").datepicker('option', 'minDate', startDate);
            $("#inputToDate").datepicker("setDate", d);


            SystemMasterSelect();
        });
    </script>
</asp:Content>
