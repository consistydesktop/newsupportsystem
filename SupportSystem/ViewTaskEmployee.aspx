﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewTaskEmployee.aspx.cs" Inherits="SupportSystem.ViewTaskEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>View Task</h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputFromDate">From Date </label>
                        <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputToDate">To Date</label>
                        <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" onchange='$("#inputStatus").focus()'></input>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="TaskMasterSelectByDate();" id="SearchButton" title="Search">Search</button>
                            </div>
                        </div>
                        <label id="ResponseMsgLbl"></label>
                    </div>
                </div>
            </form>
        </section>

        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-12">
                    <div class="tab-content" id="Div1">
                        <div class="tab-pane fade show active" id="Div2" role="tabpanel" aria-labelledby="projects-tab">
                            <div class="table-responsive">
                                <table id="TaskDetails" class="table table-bordered nowrap table-hover" style="width: 100%">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal_attachment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="H3">Attachment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid alltrans">
                            <div class="table-responsive">
                                <table id="Attachments" class="table table-bordered nowrap">
                                    <thead>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal_status" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="H2">Update Status</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <select name="Status" id="ddlStatus" class="form-control" data-toggle="tooltip" data-placement="top" title="Ticket Status" style="height: 36px;">
                            <option value="0">Select Status</option>
                            <option value="Inprogress">Inprogress</option>
                            <option value="Completed">Resolved</option>
                        </select>
                        <br />
                        <input type="file" id="inputTaskAttachment" title="Attach file" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" onclick="return TaskMasterMasterUpdateStatus();" title="Update status" data-dismiss="modal">Update Status</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal_Sender" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="h1">Description</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span id="txtDescription" style="height: 100%; width: 100%;"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <label id="TaskID" style="display: none"></label>

        <div class="modal fade" id="Div3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="H4">Update Status</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <select name="Status" id="Select1" class="form-control" data-toggle="tooltip" data-placement="top" title="Ticket Status" style="height: 36px;">
                            <option value="0">Select Status</option>
                            <option value="Inprogress">Inprogress</option>
                            <option value="Completed">Resolved</option>
                        </select>
                        <br />
                        <input type="file" id="File1" title="Attach file" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" onclick="return TaskMasterMasterUpdateStatus();" title="Update status" data-dismiss="modal">Update Status</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/JS/ViewTaskEmployee.js"></script>
    <script>
        $(document).ready(function () {
            $("#inputFromDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });
            $("#inputToDate").datepicker({
                dateFormat: 'd MM, yy',
                changeMonth: true,
                changeYear: true,

            });

            TaskMasterSelectForEmployee();

        });
    </script>
</asp:Content>
