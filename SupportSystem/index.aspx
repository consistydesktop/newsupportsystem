﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="SupportSystem.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="wrapper">

        <section>

            <section class="formhead mt-3">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link projects active" id="projects-tab" data-toggle="tab" href="#projects" role="tab" aria-controls="projects" aria-selected="true">Projects</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tickets" id="tickets-tab" data-toggle="tab" href="#tickets" role="tab" aria-controls="tickets" aria-selected="false">Tickets</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link calls" id="calls-tab" data-toggle="tab" href="#calls" role="tab" aria-controls="calls" aria-selected="false">Calls</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tasks" id="tasks-tab" data-toggle="tab" href="#tasks" role="tab" aria-controls="tasks" aria-selected="false">Tasks</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-fluid mt-5">
                <div class="row">
                    <div class="col-12">

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="projects" role="tabpanel" aria-labelledby="projects-tab">

                                <div class="table-responsive">
                                    <table id="myorganiations" class="table" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>Project Name</th>
                                                <th class="text-center">Tickets</th>
                                                <th class="text-center">Details</th>
                                                <th class="text-center">More</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <h4>Digital Global Services</h4>
                                                    <p>
                                                        <i class="fa fa-circle red" aria-hidden="true"></i>
                                                        Status : Close :: <i class="fa fa-clock-o red" aria-hidden="true"></i>Total Time : 1 Day 2 hr
                                                    </p>
                                                </td>
                                                <td class="align-middle text-center"><span class="purple">100</span></td>
                                                <td class="align-middle text-center"><span class="green"><a href="#" class="explore">Explore</a></span></td>
                                                <td class="align-middle text-center"><span class="purple">More</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="tickets" role="tabpanel" aria-labelledby="tickets-tab">2</div>
                            <div class="tab-pane fade" id="calls" role="tabpanel" aria-labelledby="calls-tab">3</div>
                            <div class="tab-pane fade" id="tasks" role="tabpanel" aria-labelledby="tasks-tab">4</div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        </div>
</asp:Content>
