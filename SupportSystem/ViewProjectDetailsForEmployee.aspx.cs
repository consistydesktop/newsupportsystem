﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SupportSystem
{
    public partial class ViewProjectDetailsForEmployee : System.Web.UI.Page
    {
        private static string pageName = "ViewProjectDetailsForEmployee.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {

            AccessController.checkAccess(pageName);
        }

        [WebMethod]
        public static ViewProjectDetailsForEmployeeModel[] Select()
        {

            AccessController.checkAccess(pageName);
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            List<ViewProjectDetailsForEmployeeModel> details = new List<ViewProjectDetailsForEmployeeModel>();
            ViewProjectDetailsForEmployeeBal objBAL = new ViewProjectDetailsForEmployeeBal();
            try
            {
                details = objBAL.Select(UserID);
            }
            catch (Exception ex) { new Logger().write(ex); }
            return details.ToArray();
        }
        
        
        
        [WebMethod]
        public static ViewProjectDetailsForEmployeeModel[] SelectByID(int ProjectDetailMasterID)
        {

            AccessController.checkAccess(pageName);
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            List<ViewProjectDetailsForEmployeeModel> details = new List<ViewProjectDetailsForEmployeeModel>();
            ViewProjectDetailsForEmployeeBal objBAL = new ViewProjectDetailsForEmployeeBal();
            try
            {
                details = objBAL.SelectByID(UserID, ProjectDetailMasterID);
            }
            catch (Exception ex) { new Logger().write(ex); }
            return details.ToArray();
        }



        [WebMethod]
        public static ViewProjectDetailsForEmployeeModel[] Search(string FromDate, string ToDate)
        {

            AccessController.checkAccess(pageName);
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            List<ViewProjectDetailsForEmployeeModel> details = new List<ViewProjectDetailsForEmployeeModel>();

            ViewProjectDetailsForEmployeeBal objBal = new ViewProjectDetailsForEmployeeBal();
            try
            {
                details = objBal.Search(FromDate, ToDate, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details.ToArray();
        }


        [WebMethod]
        public static ViewProjectDetailsForEmployeeModel[] SelectAttachment(int ProjectDetailMasterID)
        {

            AccessController.checkAccess(pageName);

            List<ViewProjectDetailsForEmployeeModel> details = new List<ViewProjectDetailsForEmployeeModel>();
            ViewProjectDetailsForEmployeeModel objModel = new ViewProjectDetailsForEmployeeModel();

            objModel.UploadType = UploadTypeMaster.UploadProject;
            objModel.ProjectDetailMasterID = ProjectDetailMasterID;
            ViewProjectDetailsForEmployeeBal objBAL = new ViewProjectDetailsForEmployeeBal();
            try
            {
                details = objBAL.SelectAttachment(objModel);
            }
            catch (Exception ex) { new Logger().write(ex); }
            return details.ToArray();
        }
    }
}