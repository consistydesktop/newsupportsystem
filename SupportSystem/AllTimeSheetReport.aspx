﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AllTimeSheetReport.aspx.cs" Inherits="SupportSystem.AllTimeSheetReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <section>
            <section class="formhead">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h6>Time Sheet Report                            
                            </h6>
                        </div>
                    </div>
                </div>
            </section>
            <form class="formbox mt-4">

                <div class="form-group row mt-3">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputFromDate">FromDate </label>
                        <input type="text" class="form-control loginput" id="inputFromDate" title="From Date" aria-describedby="emailHelp" placeholder="From Date" />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputToDate">ToDate </label>
                        <input type="text" class="form-control loginput" id="inputToDate" title="To Date" aria-describedby="emailHelp" placeholder="To Date" />
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <label for="inputEmployee" style ="margin-bottom:21px">Employee Name</label>
                        <select id="inputEmployee" class="form-control loginput select"></select>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-info w-100 mt-4 logbutton buttons" onclick="Search();" id="SearchButton" title="Search">Search</button>
                            </div>
                        </div>
                        <label id="ResponseMsgLbl"></label>
                    </div>

                </div>
            </form>
            <div class="loader" id="loader" style="display: none"></div>
            <div class="table-responsive">
                <table id="TimeSheetDetails" class="table table-bordered nowrap table-hover">
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
    <label id="UserID" style="display: none"></label>
    <div class="modal fade" id="modal_TimeSheetReport" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" style="min-width: 80%" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Today's TimeSheet</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <h5 style="float: left"><b>
                                    <label id="LabelTimeSheetEmployeeName"></label>
                                </b></h5>
                            </div>
                            <div class="col-md-4">
                                <div class="peginew">
                                    <span>
                                        <label id="LabelOpenTimeSheetDate" style="display: none"></label>
                                        <a href="#" onclick="TimeSheetMasterSelectByIDForPreviousDayReport()"><< Previous Day</a>
                                    </span>
                                    <span class="midspan">
                                       <%-- <label class="mb-0" id="LabelTimeSheetDate"></label>--%>
                                        TimeSheet</span>
                                    <label id="LabelTimeSheetNextDate" style="display: none"></label>
                                    <span><a href="#" onclick="TimeSheetMasterSelectByIDForNextDayReport()">Next Day >></a></span>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <h5 style="float: right"><b>
                                    <label id="LabelTotalTime"></label>
                                </b></h5>
                            </div>

                        </div>
                    </div>


                    <div class="table-responsive">
                        <table id="TimeSheetReport" class="table table-bordered nowrap">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="/JS/AllTimeSheetReport.js"></script>

    <script>
        $("#inputFromDate").datepicker({
            dateFormat: 'd MM, yy',
            changeMonth: true,
            changeYear: true,

        });
        $("#inputFromDate").datepicker().datepicker("setDate", new Date());
        $("#inputToDate").datepicker({
            dateFormat: 'd MM, yy',
            changeMonth: true,
            changeYear: true,

        });
        $("#inputToDate").datepicker().datepicker("setDate", new Date());
        UserInformationSelectEmployeeName();
        TimeSheetMasterSelectForAllReport();
    </script>
</asp:Content>
