﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

namespace SupportSystem
{
    public partial class APIProvider : System.Web.UI.Page
    {
        private static string pageName = "APIProvider.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }



        static bool IsValidInsert(string ProviderName, string URL)
        {

            if (string.IsNullOrEmpty(URL))
            {
                return false;
            }
            if (string.IsNullOrEmpty(ProviderName))
            {
                return false;
            }


            return true;
        }

        [WebMethod]
        public static MessageWithIcon Insert(string ProviderName, string URL)
        {
            AccessController.checkAccess(pageName);
            MessageWithIcon msgIcon = new MessageWithIcon();
            if (!IsValidInsert(ProviderName, URL))
            {
                msgIcon.Message = Message.InvalidData;
                return msgIcon;
            }

            msgIcon.Message = "URL details not added";
            int result = -1;

            try
            {

                APIProviderModel ObjModel = new APIProviderModel();
                ObjModel.ProviderName = ProviderName;
                ObjModel.URL = URL;


                APIProviderBal ObjBAL = new APIProviderBal();
                result = ObjBAL.Insert(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result < 1)
                return msgIcon;


            msgIcon.Message = " Provider details added successfully";
            return msgIcon;
        }



        [WebMethod]
        public static APIProviderModel[] Select()
        {
            AccessController.checkAccess(pageName);
            List<APIProviderModel> details = new List<APIProviderModel>();
            DataTable dt = new DataTable();

            try
            {

                APIProviderBal objBAL = new APIProviderBal();

                dt = objBAL.SelectAPIProviderReport();
                if (dt.Rows.Count <= 0)
                {
                    return details.ToArray();
                }
                foreach (DataRow dtrow in dt.Rows)
                {

                    Model.APIProviderModel report = new Model.APIProviderModel();
                    report.ProviderID = Convert.ToInt32(dtrow["ProviderID"].ToString());
                    report.ProviderName = (dtrow["ProviderName"].ToString());
                    report.URL = (dtrow["URL"].ToString());
                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Model.Logger().write(ex);
            }
            return details.ToArray();
        }


        [WebMethod]
        public static APIProviderModel SelectByID(int ProviderID)
        {
            AccessController.checkAccess(pageName);
            APIProviderModel report = new APIProviderModel();

            APIProviderBal objBal = new APIProviderBal();
            try
            {
                report = objBal.SelectByID(ProviderID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return report;
        }

        [WebMethod]
        public static MessageWithIcon Update(int ProviderID, string ProviderName, string URL)
        {
            AccessController.checkAccess(pageName);



            MessageWithIcon msgIcon = new MessageWithIcon();
            if (!IsValidInsert(ProviderName, URL))
            {
                msgIcon.Message = Message.InvalidData;
                return msgIcon;
            }

            msgIcon.Message = "Provider not updated";
            int result = -1;

            try
            {

                APIProviderModel ObjModel = new APIProviderModel();
                ObjModel.ProviderID = ProviderID;
                ObjModel.ProviderName = ProviderName;
                ObjModel.URL = URL;

                APIProviderBal ObjBAL = new APIProviderBal();
                result = ObjBAL.Update(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (result < 1)
                return msgIcon;


            msgIcon.Message = " Provider details updated successfully";
            return msgIcon;
        }



        [WebMethod]
        public static MessageWithIcon Delete(int ProviderID)
        {
            AccessController.checkAccess(pageName);

            MessageWithIcon msgIcon = new MessageWithIcon();
            int i = 0;

            APIProviderBal objBal = new APIProviderBal();
            try
            {
                i = objBal.Delete(ProviderID);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (i > 0)
                msgIcon.Message = "Provider not deleted";
            else
                msgIcon.Message = "Provider deleted Successfully";
            return msgIcon;
        }





    }
}