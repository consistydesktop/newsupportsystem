﻿using Bal;
using Model;
using SupportProjectEmployee.Controller;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace SupportSystem
{
    public partial class Reminder : System.Web.UI.Page
    {
        private static string pageName = "Reminder.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessController.checkAccess(pageName);
        }

        static bool IsValidInsert(string CustomerName, string ReminderType, string ReminderDate, double Amount)
        {
            if (CustomerName == "")
                return false;

            if (ReminderType == "")
                return false;

            if (ReminderDate == "")
                return false;
            try
            {
                DateTime d = Convert.ToDateTime(ReminderDate);
                double a = Convert.ToDouble(Amount);
            }
            catch (Exception e)
            {
                return false;
            }


            return true;
        }


        [WebMethod]
        public static MessageWithIcon Insert(string CustomerName, string ReminderType, string ReminderDate, double Amount, string Description, string Status)
        {
            AccessController.checkAccess(pageName);

            bool IsVallid = IsValidInsert(CustomerName, ReminderType, ReminderDate, Amount);



            MessageWithIcon msgIcon = new MessageWithIcon();
            msgIcon.Message = "Reminder not submitted";
            if (!IsVallid)
            {
                return msgIcon;
            }
            int result = -1;

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            //int UserID = 45;
            ReminderModel ObjModel = new ReminderModel();

            ObjModel.CustomerName = CustomerName;
            ObjModel.ReminderType = ReminderType;
            ObjModel.Description = Description;
            ObjModel.ReminderDate = ReminderDate;
            ObjModel.Amount = Amount;
            ObjModel.UserID = UserID;
            ObjModel.Status = Status;

            ReminderBal ObjBAL = new ReminderBal();
            try
            {
                result = ObjBAL.Insert(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (result > 0)
                msgIcon.Message = "Reminder Submitted successfully";


            return msgIcon;
        }

        [WebMethod]
        public static ReminderModel[] SelectByID(int ReminderMasterID)
        {
            AccessController.checkAccess(pageName);
            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            // int UserID = 45;
            List<ReminderModel> details = new List<ReminderModel>();
            ReminderBal ObjBAL = new ReminderBal();

            try
            {
                details = ObjBAL.SelectByID(ReminderMasterID, UserID);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

       

        [WebMethod]
        public static MessageWithIcon Update(int ReminderMasterID, string CustomerName, string ReminderType, string ReminderDate, double Amount, string Description, string Status)
        {
            AccessController.checkAccess(pageName);

            bool IsVallid = IsValidInsert(CustomerName, ReminderType, ReminderDate, Amount);
            MessageWithIcon msgIcon = new MessageWithIcon();
            msgIcon.Message = "Reminder not updated";
            if (!IsVallid)
            {
                return msgIcon;
            }

            int result = -1;

            int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString());
            //int UserID = 45;
            ReminderModel ObjModel = new ReminderModel();
            ObjModel.ReminderMasterID = ReminderMasterID;
            ObjModel.CustomerName = CustomerName;
            ObjModel.ReminderType = ReminderType;
            ObjModel.Description = Description;
            ObjModel.ReminderDate = ReminderDate;
            ObjModel.Amount = Amount;
            ObjModel.UserID = UserID;
            ObjModel.Status = Status;

            ReminderBal ObjBAL = new ReminderBal();
            try
            {
                result = ObjBAL.Update(ObjModel);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (result > 0)
                msgIcon.Message = "Reminder updated successfully";



            return msgIcon;
        }

        [WebMethod]
        public static ReminderModel[] SelectCustomerName()
        {
            AccessController.checkAccess(pageName);

            List<ReminderModel> details = new List<ReminderModel>();
            ReminderBal ObjBAL = new ReminderBal();

            try
            {
                details = ObjBAL.SelectCustomerName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }

        [WebMethod]
        public static ReminderModel[] SelectSystemName()
        {
            AccessController.checkAccess(pageName);

            List<ReminderModel> details = new List<ReminderModel>();
            ReminderBal ObjBAL = new ReminderBal();

            try
            {
                details = ObjBAL.SelectSystemName();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }



        [WebMethod]
        public static ReminderModel[] SelectReminderType()
        {
            AccessController.checkAccess(pageName);

            List<ReminderModel> details = new List<ReminderModel>();
            ReminderBal ObjBAL = new ReminderBal();

            try
            {
                details = ObjBAL.SelectReminderType();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details.ToArray();
        }
    }
}