﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ViewDetailsByProjectDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<ViewDetailsByProjectModel> Select(int ProjectDetailMasterID)
        {
            DataTable dt = new DataTable();
            List<ViewDetailsByProjectModel> details = new List<ViewDetailsByProjectModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.ProjectEmployeeMappingSelect");
                cmd.Parameters.AddWithValue("@ProjectDetailMasterID", ProjectDetailMasterID);
                dt = db.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    ViewDetailsByProjectModel report = new ViewDetailsByProjectModel();
                    report.ProjectDetailMasterID = Convert.ToInt32(dtrow["ProjectDetailMasterID"].ToString());
                    report.Name = dtrow["Name"].ToString().Trim();

                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }

        public List<ViewDetailsByProjectModel> SelectByProjectDetailMasterID(int ProjectDetailMasterID)
        {
            DataTable dt = new DataTable();
            List<ViewDetailsByProjectModel> details = new List<ViewDetailsByProjectModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.FileMasterSelect");
                cmd.Parameters.AddWithValue("@ProjectDetailMasterID", ProjectDetailMasterID);
                dt = db.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    ViewDetailsByProjectModel report = new ViewDetailsByProjectModel();
                    report.ProjectDetailMasterID = Convert.ToInt32(dtrow["ProjectDetailMasterID"].ToString());
                    report.Attachment = dtrow["Attachment"].ToString().Trim();

                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }
    }
}