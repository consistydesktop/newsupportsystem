﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ProjectAssignScreenDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<ProjectAssignScreenModel> SelectServiceName()
        {
            DataTable dt = new DataTable();
            List<ProjectAssignScreenModel> details = new List<ProjectAssignScreenModel>();

            SqlCommand cmd = new SqlCommand("dbo.ServiceMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            foreach (DataRow dtrow in dt.Rows)
            {
                ProjectAssignScreenModel report = new ProjectAssignScreenModel();
                report.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString());
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ProjectAssignScreenModel> SelectEmployeeName()
        {
            DataTable dt = new DataTable();
            List<ProjectAssignScreenModel> details = new List<ProjectAssignScreenModel>();

            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByEmployeeID");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            foreach (DataRow dtrow in dt.Rows)
            {
                ProjectAssignScreenModel report = new ProjectAssignScreenModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ProjectAssignScreenModel> SelectProductName()
        {
            DataTable dt = new DataTable();
            List<ProjectAssignScreenModel> details = new List<ProjectAssignScreenModel>();
            SqlCommand cmd = new SqlCommand("dbo.ProductMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            foreach (DataRow dtrow in dt.Rows)
            {
                ProjectAssignScreenModel report = new ProjectAssignScreenModel();

                report.ProductID = Convert.ToInt32(dtrow["ProductID"].ToString());
                report.ProductName = dtrow["ProductName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Insert(List<ProjectAssignScreenModel> ObjModel)
        {
            int Result = -1;

            foreach (var item in ObjModel)
            {
                SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterInsert");

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ProjectName", item.ProjectName);
                cmd.Parameters.AddWithValue("@UserID", item.UserID);
                cmd.Parameters.AddWithValue("@ServiceID", item.ServiceID);
                cmd.Parameters.AddWithValue("@Description", item.Description);
                cmd.Parameters.AddWithValue("@ProductName", item.ProductName);
                cmd.Parameters.AddWithValue("@ClientName", item.ClientName);
                cmd.Parameters.AddWithValue("@EmailID", item.EmailID);
                cmd.Parameters.AddWithValue("@MobileNumber", item.MobileNumber);
                cmd.Parameters.AddWithValue("@ExpectedDelivery", item.ExpectedDelivery);
                cmd.Parameters.AddWithValue("@Status", "NEW".ToUpper());
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(Output);
                try
                {
                    Result = db.getOutputParam(cmd, Output);
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }
                cmd.Dispose();
            }

            return Result;
        }

        public DataTable FileInsert(ProjectAssignScreenModel ObjModel)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Attachment", ObjModel.Attachment);
            cmd.Parameters.AddWithValue("@OriginalFileName", ObjModel.OriginalFileName);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public DataTable FileMappingInsert(ProjectAssignScreenModel ObjModel)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.FileMappingMasterInsertByProject");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ProjectDetailMasterID", ObjModel.ProjectDetailMasterID);
            cmd.Parameters.AddWithValue("@UploadType", ObjModel.UploadType);
            cmd.Parameters.AddWithValue("@FileID", ObjModel.FileID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public List<ProjectAssignScreenModel> SelectByID(int ProjectDetailMasterID)
        {
            DataTable dt = new DataTable();
            List<ProjectAssignScreenModel> details = new List<ProjectAssignScreenModel>();

            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterSelectByID");
            cmd.Parameters.Add("@ProjectDetailMasterID", SqlDbType.Int).Value = ProjectDetailMasterID;

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            foreach (DataRow dtrow in dt.Rows)
            {
                ProjectAssignScreenModel Register = new ProjectAssignScreenModel();
                Register.ProjectDetailMasterID = Convert.ToInt32(dtrow["ProjectDetailMasterID"].ToString());
                Register.ProjectName = dtrow["ProjectName"].ToString().Trim();
                Register.ProductName = dtrow["ProductName"].ToString().Trim();
                Register.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString().Trim());
                Register.UserID = Convert.ToInt32(dtrow["UserID"].ToString().Trim());
                Register.ClientName = dtrow["ClientName"].ToString().Trim();
                Register.EmailID = dtrow["EmailID"].ToString().Trim();
                Register.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
                Register.ExpectedDelivery = dtrow["ExpectedDelivery"].ToString().Trim();
                Register.Description = dtrow["Description"].ToString().Trim();
                details.Add(Register);
            }
            return details;
        }

        public int Update(List<ProjectAssignScreenModel> ObjModel)
        {
            int Result = -1;
            foreach (var item in ObjModel)
            {
                SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterUpdate");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ProjectDetailMasterID", item.ProjectDetailMasterID);
                cmd.Parameters.AddWithValue("@ProjectName", item.ProjectName);
                cmd.Parameters.AddWithValue("@ServiceID", item.ServiceID);
                cmd.Parameters.AddWithValue("@Description", item.Description);
                cmd.Parameters.AddWithValue("@ProductName", item.ProductName);
                cmd.Parameters.AddWithValue("@ClientName", item.ClientName);
                cmd.Parameters.AddWithValue("@EmailID", item.EmailID);
                cmd.Parameters.AddWithValue("@MobileNumber", item.MobileNumber);
                cmd.Parameters.AddWithValue("@ExpectedDelivery", item.ExpectedDelivery);
               
                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(Output);

                try
                {
                    Result = db.getOutputParam(cmd, Output);
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }
                cmd.Dispose();
                if (Result < 1)
                    return Result;

                break;
            }

            {
                int result = 0;
                SqlCommand cmd = new SqlCommand("dbo.ProjectEmployeeMappingDelete");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ProjectDetailMasterID", Result);
                try
                {
                    result = db.executeStoredProcedure(cmd);
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }

                if (result < 0)
                    return result;
            }

            foreach (var item in ObjModel)
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand("dbo.ProjectEmployeeMappingUpdate");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ProjectDetailMasterID", Result);
                cmd.Parameters.AddWithValue("@UserID", item.UserID);

                try
                {
                    result = db.executeStoredProcedure(cmd);
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }
                cmd.Dispose();

                if (result < 1)
                    return result;
            }

            return Result;
        }

        public List<ProjectAssignScreenModel> SelectAttachment(int ProjectDetailMasterID)
        {



            DataTable dt = new DataTable();
            List<ProjectAssignScreenModel> details = new List<ProjectAssignScreenModel>();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterSelectForProject");

            cmd.Parameters.AddWithValue("@UploadType", "Project");
            cmd.Parameters.AddWithValue("@ProjectDetailMasterID", ProjectDetailMasterID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ProjectAssignScreenModel report = new ProjectAssignScreenModel();
                report.FileID = Convert.ToInt32(dtrow["FileID"].ToString());
                report.Attachment = dtrow["Attachment"].ToString().Trim();
                report.OriginalFileName = dtrow["OriginalFileName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }
    }
}