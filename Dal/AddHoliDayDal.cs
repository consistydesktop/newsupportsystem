﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class AddHoliDayDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();


        public int Insert(Model.HoliDayModel objmodel)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.HolidayMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Date", objmodel.Date);
            cmd.Parameters.AddWithValue("@Description", objmodel.Description);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }

            return Result;
        }

        public int Update(Model.HoliDayModel objmodel)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.HolidayMasterUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DayID", objmodel.DayID);
            cmd.Parameters.AddWithValue("@Date", objmodel.Date);
            cmd.Parameters.AddWithValue("@Description", objmodel.Description);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }

            return Result;
        }

        public int Delete(int DayID)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.HolidayMasterDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DayID", DayID);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }

            return Result;
        }

        public DataTable Select()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.HolidayMasterSelect");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }

            return dt;
        }

        public DataTable SelectByID(int DayID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.HolidayMasterSelectByID");
            cmd.Parameters.AddWithValue("@DayID", DayID);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }

            return dt;
        }
    }
}
