﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class SystemReportDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public List<SystemReportModel> SelectSystemName()
        {
            DataTable dt = new DataTable();
            List<SystemReportModel> details = new List<SystemReportModel>();

            try
            {
                SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectSystemName");
                cmd.CommandType = CommandType.StoredProcedure;
                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    SystemReportModel report = new SystemReportModel();

                    report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                    report.SystemName = dtrow["SystemName"].ToString().Trim();
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }

        public List<List<SystemReportModel>> Search(string FromDate, string ToDate, int SystemID)
        {

            List<List<SystemReportModel>> AllDetails = new List<List<SystemReportModel>>();

            List<SystemReportModel> TimeSheetDetails = new List<SystemReportModel>();
            TimeSheetDetails = SelectTimeSheetReport(FromDate, ToDate, SystemID);
            AllDetails.Add(TimeSheetDetails);

            List<SystemReportModel> TaskDetails = new List<SystemReportModel>();
            TaskDetails = SelectTaskReport(FromDate, ToDate, SystemID);
            AllDetails.Add(TaskDetails);

            List<SystemReportModel> TicketDetails = new List<SystemReportModel>();
            TicketDetails = SelectTicketReport(FromDate, ToDate, SystemID);
            AllDetails.Add(TicketDetails);

            return AllDetails;
        }

        private List<SystemReportModel> SelectTimeSheetReport(string FromDate, string ToDate, int SystemID)
        {
            DataTable dt = new DataTable();
            List<SystemReportModel> details = new List<SystemReportModel>();

            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterSelectBySystemIDForReport");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", FromDate);
                cmd.Parameters.AddWithValue("@ToDate", ToDate);
                cmd.Parameters.AddWithValue("@SystemID", SystemID);
                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    SystemReportModel report = new SystemReportModel();

                    report.TimeSheetDetailID = Convert.ToInt32(dtrow["TimeSheetDetailID"].ToString());
                    report.Task = dtrow["Task"].ToString();
                    report.SystemName = dtrow["SystemName"].ToString();
                    report.Date = dtrow["Date"].ToString();
                    report.Time = Convert.ToInt32(dtrow["Time"].ToString());
                    report.Name = dtrow["Name"].ToString().Trim();
                    report.Status = Convert.ToInt32(dtrow["Status"].ToString());
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }


        private List<SystemReportModel> SelectTaskReport(string FromDate, string ToDate, int SystemID)
        {
            List<SystemReportModel> TaskDetails = new List<SystemReportModel>();
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TaskMasterSelectBySystemIDForReport");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", FromDate);
                cmd.Parameters.AddWithValue("@ToDate", ToDate);
                cmd.Parameters.AddWithValue("@SystemID", SystemID);
                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    SystemReportModel report = new SystemReportModel();
                    report.TaskID = Convert.ToInt32(dtrow["TaskID"].ToString());
                    report.Task = dtrow["Task"].ToString().Trim();
                    report.Deadline = dtrow["Deadline"].ToString().Trim();
                    report.TaskStatus = (dtrow["Status"].ToString().Trim());
                    report.EmployeeNames = getEmployeeNamesByTaskID(report.TaskID);

                    
                    TaskDetails.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return TaskDetails;
        }
        private string getEmployeeNamesByTaskID(int TaskID)
        {
            string EmployeeNames = "";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.TaskEmployeeMappingSelectNameByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TaskID", TaskID);
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            foreach (DataRow dtrow in dt.Rows)
            {
                EmployeeNames += " " + dtrow["Name"].ToString();
            }

            return EmployeeNames;
        }

        private List<SystemReportModel> SelectTicketReport(string FromDate, string ToDate, int SystemID)
        {
            List<SystemReportModel> TicketDetails = new List<SystemReportModel>();
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectBySystemIDForReport");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", FromDate);
                cmd.Parameters.AddWithValue("@ToDate", ToDate);
                cmd.Parameters.AddWithValue("@SystemID", SystemID);
                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    SystemReportModel report = new SystemReportModel();


                    report.TicketGeneratedDate = dtrow["TicketGeneratedDate"].ToString();
                    report.TicketNo = dtrow["TicketNo"].ToString().Trim();
                    report.TicketStatus = dtrow["TicketStatus"].ToString();
                    report.AssignedDate = (dtrow["AssignDate"].ToString().Trim());
                    report.AdminRemark = dtrow["AdminRemark"].ToString();

                   
                    TicketDetails.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return TicketDetails;
        }
        
    }
}
