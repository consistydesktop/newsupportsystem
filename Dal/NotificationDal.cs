﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class NotificationDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public int Insert(NotificationModel objModel)
        {
            int Result = 0;

            SqlCommand cmd = new SqlCommand("dbo.NotificationMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Notification", objModel.Notification);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return Result;
        }

        public List<NotificationModel> Select()
        {
            List<NotificationModel> details = new List<NotificationModel>();
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.NotificationMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                NotificationModel report = new NotificationModel();
                report.NotificationID = Convert.ToInt32(dtrow["NotificationID"].ToString());
                report.Notification = dtrow["Notification"].ToString().Trim();
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().Trim());

                details.Add(report);
            }

            return details;
        }

        public List<NotificationModel> SelectNotification()
        {
            List<NotificationModel> details = new List<NotificationModel>();
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.NotificationMasterSelectNotification");

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                NotificationModel report = new NotificationModel();
                report.Notification = dtrow["Notification"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public NotificationModel SelectByID(int NotificationID)
        {
            DataTable dt = new DataTable();
            NotificationModel Register = new NotificationModel();

            SqlCommand cmd = new SqlCommand("dbo.NotificationMasterSelectByID");
            cmd.Parameters.Add("@NotificationID", SqlDbType.Int).Value = NotificationID;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                Register.Notification = dtrow["Notification"].ToString().Trim();
                Register.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().Trim());
            }

            return Register;
        }

        public int Update(NotificationModel ObjModel)
        {
            int Result = -1;

            SqlCommand cmd = new SqlCommand("dbo.NotificationMasterUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@NotificationID", ObjModel.NotificationID);
            cmd.Parameters.AddWithValue("@Notification", ObjModel.Notification);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int Delete(int NotificationID)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.NotificationMasterDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@NotificationID", NotificationID);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }
    }
}