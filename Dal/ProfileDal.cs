﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class ProfileDal
    {

        private DatabaseProcessing db = new DatabaseProcessing();

        public ProfileModel SelectProfile(int UserID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectForProfile");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            ProfileModel ObjModel = new ProfileModel();

            DataRow dtrow = dt.Rows[0];
            ObjModel.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
            ObjModel.Name = dtrow["Name"].ToString();
            ObjModel.UserName = dtrow["Username"].ToString();
            ObjModel.EmailID = dtrow["EmailID"].ToString();
            return ObjModel;
        }

        public int UpdateEmail(ProfileModel ObjModel)
        {
            int Result = -1;

            SqlCommand cmd = new SqlCommand("dbo.UserInformationUpdateEmail");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", ObjModel.UserID);

            cmd.Parameters.AddWithValue("@NewEmailID", ObjModel.NewEmailID);

            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int UpdatePassword(ProfileModel ObjModel)
        {
            int Result = -1;
            string OldPassword = GetPassword(ObjModel.UserID);
            string GivenOldPassword = ObjModel.Password;

            if (!String.Equals(OldPassword, GivenOldPassword, StringComparison.OrdinalIgnoreCase))
                return Result;

            SqlCommand cmd = new SqlCommand("dbo.UserInformationUpdatePassword");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", ObjModel.UserID);
            cmd.Parameters.AddWithValue("@NewPassword", ObjModel.NewPassword);
            cmd.Parameters.AddWithValue("@OldPassword", ObjModel.Password);

            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return Result;
        }

        private string GetPassword(int UserID)
        {
            string OldPassword = "";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectPassword");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            DataRow dtrow = dt.Rows[0];
            OldPassword = dtrow["Password"].ToString();

            return OldPassword;
        }
    }
}
