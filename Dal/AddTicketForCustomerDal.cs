﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class AddTicketForCustomerDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<AddTicketForCustomerModel> SelectServiceName()
        {
            DataTable dt = new DataTable();
            List<AddTicketForCustomerModel> details = new List<AddTicketForCustomerModel>();

            SqlCommand cmd = new SqlCommand("dbo.ServiceMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddTicketForCustomerModel report = new AddTicketForCustomerModel();
                report.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString());
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<AddTicketForCustomerModel> SelectSystemName(int UserID)
        {
            DataTable dt = new DataTable();
            List<AddTicketForCustomerModel> details = new List<AddTicketForCustomerModel>();

            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectByUserID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddTicketForCustomerModel report = new AddTicketForCustomerModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Insert(AddTicketForCustomerModel objModel)
        {
            int result = 0;

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationInsertTicket");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SystemID", objModel.SystemID);
            cmd.Parameters.AddWithValue("@UserID", objModel.UserID);
            cmd.Parameters.AddWithValue("@ServiceID", objModel.ServiceID);
            cmd.Parameters.AddWithValue("@DeveloperID", objModel.DeveloperID);
            cmd.Parameters.AddWithValue("@RequestType", objModel.RequestType);
            cmd.Parameters.AddWithValue("@Description", objModel.Description);
            cmd.Parameters.AddWithValue("@TeamAny", objModel.TeamAny);
            cmd.Parameters.AddWithValue("@Priority", objModel.Priority);
            cmd.Parameters.AddWithValue("@TicketNo", objModel.TicketNo);
            try
            {
                result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public AddTicketForCustomerModel SelectByTicketID(int TicketID, int UserID)
        {
            DataTable dt = new DataTable();
            AddTicketForCustomerModel result = new AddTicketForCustomerModel();
            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByTicketIDAndUserID");
            cmd.Parameters.Add("@TicketID", SqlDbType.Int).Value = TicketID;
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                result.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());
                result.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString());
                result.RequestType = dtrow["RequestType"].ToString().Trim();
                result.ServiceName = dtrow["ServiceName"].ToString().Trim();
                result.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                result.SystemName = dtrow["SystemName"].ToString().Trim();
                result.Description = dtrow["Description"].ToString().Trim();
                result.TeamAny = dtrow["TeamAny"].ToString().Trim();
                result.Priority = dtrow["Priority"].ToString().Trim();
            }

            return result;
        }

        public int FileMappingInsert(AddTicketForCustomerModel ObjModel)
        {
            int result = 0;
            SqlCommand cmd = new SqlCommand("dbo.FileMappingMasterInsertByTicket");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TicketID", ObjModel.TicketID);
            cmd.Parameters.AddWithValue("@FileID", ObjModel.FileID);
            cmd.Parameters.AddWithValue("@UploadType", ObjModel.UploadType);
            try
            {
                result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }
    }
}