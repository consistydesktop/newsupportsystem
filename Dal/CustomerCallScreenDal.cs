﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class CustomerCallScreenDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<CustomerCallScreenModel> SelectSystemName()
        {
            DataTable dt = new DataTable();
            List<CustomerCallScreenModel> details = new List<CustomerCallScreenModel>();

            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectSystemName");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                CustomerCallScreenModel report = new CustomerCallScreenModel();

                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<CustomerCallScreenModel> SelectEmployeeName()
        {
            DataTable dt = new DataTable();
            List<CustomerCallScreenModel> details = new List<CustomerCallScreenModel>();

            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByEmployeeID");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            } foreach (DataRow dtrow in dt.Rows)
            {
                CustomerCallScreenModel report = new CustomerCallScreenModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<CustomerCallScreenModel> SelectIssue()
        {
            DataTable dt = new DataTable();
            List<CustomerCallScreenModel> details = new List<CustomerCallScreenModel>();
            SqlCommand cmd = new SqlCommand("dbo.CallIssueMasterSelect");

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            } foreach (DataRow dtrow in dt.Rows)
            {
                CustomerCallScreenModel report = new CustomerCallScreenModel();
                report.CallIssueID = Convert.ToInt32(dtrow["CallIssueID"].ToString());
                report.IssueName = dtrow["IssueName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Insert(CustomerCallScreenModel objModel)
        {
            int Result = 0;
            SqlCommand cmd = new SqlCommand("dbo.CallMasterInsert");

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SystemID", objModel.SystemID);
            cmd.Parameters.AddWithValue("@UserID", objModel.UserID);
            cmd.Parameters.AddWithValue("@CallIssueID", objModel.CallIssueID);
            cmd.Parameters.AddWithValue("@MobileNumber", objModel.MobileNumber);
            cmd.Parameters.AddWithValue("@Description", objModel.Description);
            cmd.Parameters.AddWithValue("@TeamAny", objModel.TeamAny);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return Result;
        }

        public List<CustomerCallScreenModel> Select()
        {
            List<CustomerCallScreenModel> details = new List<CustomerCallScreenModel>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.CallMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                CustomerCallScreenModel report = new CustomerCallScreenModel();
                report.CallID = Convert.ToInt32(dtrow["CallID"].ToString());
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.CallIssueID = Convert.ToInt32(dtrow["CallIssueID"].ToString());
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().Trim());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.Name = dtrow["Name"].ToString().Trim();
                report.IssueName = dtrow["IssueName"].ToString().Trim();
                report.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();

                details.Add(report);
            }

            return details;
        }

        public CustomerCallScreenModel SelectByID(int CallID)
        {
            DataTable dt = new DataTable();
            CustomerCallScreenModel report = new CustomerCallScreenModel();
            SqlCommand cmd = new SqlCommand("dbo.CallMasterSelectByID");
            cmd.Parameters.Add("@CallID", SqlDbType.Int).Value = CallID;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                report.CallID = Convert.ToInt32(dtrow["CallID"].ToString());
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.CallIssueID = Convert.ToInt32(dtrow["CallIssueID"].ToString());
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().ToUpper().Trim());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.Name = dtrow["Name"].ToString().Trim();
                report.IssueName = dtrow["IssueName"].ToString().Trim();
                report.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.TeamAny = dtrow["TeamAny"].ToString().Trim();
            }

            return report;
        }

        public int Update(CustomerCallScreenModel ObjModel)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.CallMasterUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CallID", ObjModel.CallID);
            cmd.Parameters.AddWithValue("@SystemID", ObjModel.SystemID);
            cmd.Parameters.AddWithValue("@UserID", ObjModel.UserID);
            cmd.Parameters.AddWithValue("@CallIssueID", ObjModel.CallIssueID);
            cmd.Parameters.AddWithValue("@MobileNumber", ObjModel.MobileNumber);
            cmd.Parameters.AddWithValue("@Description", ObjModel.Description);
            cmd.Parameters.AddWithValue("@TeamAny", ObjModel.TeamAny);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int Delete(int CallID)
        {
            int Result = -1;

            SqlCommand cmd = new SqlCommand("dbo.CallMasterDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CallID", CallID);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }
    }
}