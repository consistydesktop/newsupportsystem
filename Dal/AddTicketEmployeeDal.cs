﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class AddTicketEmployeeDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<AddTicketEmployeeModel> SelectServiceName()
        {
            DataTable dt = new DataTable();
            List<AddTicketEmployeeModel> details = new List<AddTicketEmployeeModel>();

            SqlCommand cmd = new SqlCommand("dbo.ServiceMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddTicketEmployeeModel report = new AddTicketEmployeeModel();
                report.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString());
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Insert(AddTicketEmployeeModel objModel)
        {
            int result = 0;

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationInsertTicket");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SystemID", objModel.SystemID);
            cmd.Parameters.AddWithValue("@UserID", objModel.UserID);
            cmd.Parameters.AddWithValue("@ServiceID", objModel.ServiceID);
            cmd.Parameters.AddWithValue("@RequestType", objModel.RequestType);
            cmd.Parameters.AddWithValue("@Description", objModel.Description);
            cmd.Parameters.AddWithValue("@TeamAny", objModel.TeamAny);
            cmd.Parameters.AddWithValue("@Priority", objModel.Priority);
            cmd.Parameters.AddWithValue("@DeveloperID", 0);
            cmd.Parameters.AddWithValue("@TicketNo", objModel.TicketNo);
            try
            {
                result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public List<AddTicketEmployeeModel> SelectSystemName()
        {
            DataTable dt = new DataTable();
            List<AddTicketEmployeeModel> details = new List<AddTicketEmployeeModel>();

            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectSystemName");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddTicketEmployeeModel report = new AddTicketEmployeeModel();
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int FileMappingInsert(AddTicketEmployeeModel ObjModel)
        {
            int result = 0;
            SqlCommand cmd = new SqlCommand("dbo.FileMappingMasterInsertByTicket");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TicketID", ObjModel.TicketID);
            cmd.Parameters.AddWithValue("@FileID", ObjModel.FileID);
            cmd.Parameters.AddWithValue("@UploadType", ObjModel.UploadType);
            try
            {
                result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }
    }
}