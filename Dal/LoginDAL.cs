﻿using DAL;
using Model;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class LoginDAL
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public DataTable Select(LoginModel objModel)
        {
            LoginModel ObjModel = new LoginModel();

            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectForLogin");
                cmd.Parameters.Add("@MobileNumber", SqlDbType.VarChar).Value = objModel.MobileNumber;
                cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = objModel.Password;
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return dt;
        }
        public DataTable SelectByToken(string Token)
        {
            LoginModel ObjModel = new LoginModel();

            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectForLoginByToken");
                cmd.Parameters.Add("@Token", SqlDbType.VarChar).Value = Token;
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return dt;
        }

        public DataTable SelectToken(LoginModel objModel)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("UserInformationSelectToken");
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = objModel.UserID;
                cmd.Parameters.Add("@Token", SqlDbType.VarChar).Value = objModel.Token;
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return dt;
        }

        public int UpdateToken(LoginModel objModel)
        {
            int Result = -1;

            try
            {
                SqlCommand cmd = new SqlCommand("UserInformationUpdateToken");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MobileNumber", SqlDbType.VarChar).Value = objModel.MobileNumber;
                cmd.Parameters.Add("@Token", SqlDbType.VarChar).Value = objModel.Token;
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int UpdateOTP(LoginModel objModel)
        {
            int i = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("UserInformationUpdateOTP");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MobileNumber", SqlDbType.VarChar).Value = objModel.MobileNumber;
                cmd.Parameters.Add("@OTP", SqlDbType.VarChar).Value = objModel.OTP;

                i = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return i;
        }

        public DataTable SelectOTP(LoginModel objModel)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("UserInformationSelectOTP");
                cmd.Parameters.Add("@MobileNumber", SqlDbType.VarChar).Value = objModel.MobileNumber;
                cmd.Parameters.Add("@OTP", SqlDbType.VarChar).Value = objModel.OTP;

                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return dt;
        }
        public DataTable SelectTokenForGateway(LoginModel objModel)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("UserInformationSelectTokenForGateway");
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = objModel.UserID;
                              dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return dt;
        }


    }
}