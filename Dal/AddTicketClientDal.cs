﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class AddTicketClientDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();
        public List<ServiceModel> SelectServiceName()
        {
            DataTable dt = new DataTable();
            List<ServiceModel> details = new List<ServiceModel>();

            SqlCommand cmd = new SqlCommand("dbo.ServiceMasterSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ServiceModel report = new ServiceModel();
                report.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString());
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Insert(AddTicketClientModel objModel)
        {
            int result = 0;

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationInsertTicketByClient");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Title", objModel.Title);
            cmd.Parameters.AddWithValue("@UserID", objModel.UserID);
            cmd.Parameters.AddWithValue("@ServiceID", objModel.ServiceID);
            cmd.Parameters.AddWithValue("@RequestType", objModel.RequestType);
            cmd.Parameters.AddWithValue("@Description", objModel.Description);
            cmd.Parameters.AddWithValue("@TeamAny", objModel.TeamAny);
            cmd.Parameters.AddWithValue("@Priority", objModel.Priority);
            cmd.Parameters.AddWithValue("@TicketNo", objModel.TicketNo);
            cmd.Parameters.AddWithValue("@RemoteAccessType", objModel.RemoteAccessType);
            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public int FileMappingInsert(AddTicketClientModel objModel)
        {
            int result = 0;

            SqlCommand cmd = new SqlCommand("dbo.FileMappingMasterInsertByTicket");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TicketID", objModel.TicketID);
            cmd.Parameters.AddWithValue("@FileID", objModel.FileID);
            cmd.Parameters.AddWithValue("@UploadType", objModel.UploadType);
            try
            {
                result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public int UpdateStatusByClient(string TicketNo, string Status)
        {
            int result = 0;

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationUpdateStatusByClient");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TicketNo", TicketNo);
            cmd.Parameters.AddWithValue("@Status", Status.ToUpper());
            try
            {
                result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public int Delete(int TicketID)
        {
            int result = 0;

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TicketID", TicketID);
            try
            {
                result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public DataTable SelectByID(int TicketID)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByTicketID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TicketID", TicketID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public DataTable SelectTicketAttachmentByID(int TicketID, string UploadType)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterSelectForTicket");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UploadType", UploadType);
            cmd.Parameters.AddWithValue("@TicketID", TicketID);


            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public int Update(AddTicketClientModel objModel)
        {
            int result = 0;

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationUpdateTicketByClient");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TicketID", objModel.TicketID);
            cmd.Parameters.AddWithValue("@Title", objModel.Title);
            cmd.Parameters.AddWithValue("@UserID", objModel.UserID);
            cmd.Parameters.AddWithValue("@ServiceID", objModel.ServiceID);
            cmd.Parameters.AddWithValue("@RequestType", objModel.RequestType);
            cmd.Parameters.AddWithValue("@Description", objModel.Description);
            cmd.Parameters.AddWithValue("@TeamAny", objModel.TeamAny);
            cmd.Parameters.AddWithValue("@Priority", objModel.Priority);
            cmd.Parameters.AddWithValue("@TicketNo", objModel.TicketNo);
            cmd.Parameters.AddWithValue("@RemoteAccessType", objModel.RemoteAccessType);
            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }
    }
}
