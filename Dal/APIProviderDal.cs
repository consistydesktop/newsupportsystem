﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class APIProviderDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();
        public DataTable SelectAPIProviderReport()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.SelectAPIProviderReport");
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }


            return dt;
        }

        public int Delete(int ProviderID)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.ProviderMasterDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ProviderID", ProviderID);
            try
            {
                Result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int Update(APIProviderModel ObjModel)
        {

            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.ProviderMasterUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ProviderID", ObjModel.ProviderID);
            cmd.Parameters.AddWithValue("@ProviderName", ObjModel.ProviderName);
            cmd.Parameters.AddWithValue("@URL", ObjModel.URL);


            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public APIProviderModel SelectByID(int ProviderID)
        {
            DataTable dt = new DataTable();
            Model.APIProviderModel report = new Model.APIProviderModel();

            SqlCommand cmd = new SqlCommand("dbo.ProviderSelectByID");
            cmd.Parameters.Add("@ProviderID", SqlDbType.Int).Value = ProviderID;
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {

                report.ProviderID = Convert.ToInt32(dtrow["ProviderID"].ToString());
                report.ProviderName = (dtrow["ProviderName"].ToString());
                report.URL = (dtrow["URL"].ToString());
             
            }

            return report;
        }

        public int Insert(APIProviderModel ObjModel)
        {

            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.ProviderMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ProviderName", ObjModel.ProviderName);
            cmd.Parameters.AddWithValue("@URL", ObjModel.URL);


            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }
    }
}
