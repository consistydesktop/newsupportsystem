﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class FinovativeVersionHistoryDal
    {


        private DatabaseProcessing dp = new DatabaseProcessing();
        public int InsertFinovativeVersionHistory(FinovativeVersionHistoryModel ObjModel)
        {
            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.FinovativeVersionHistoryInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Title", ObjModel.Title);
            cmd.Parameters.AddWithValue("@Description", ObjModel.Description);
            cmd.Parameters.AddWithValue("@Version", ObjModel.Version);
            cmd.Parameters.AddWithValue("@Date", ObjModel.Date);

            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public DataTable selectVersionHistoryReport()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.FinovativeVersionHistorySelect");
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }


            return dt;
        }
        public int Update(FinovativeVersionHistoryModel ObjModel)
        {

            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.VersionHistoryUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@FinnovativeVersionHistoryID", SqlDbType.Int).Value = ObjModel.FinnovativeVersionHistoryID;
            cmd.Parameters.AddWithValue("@Title", ObjModel.Title);
            cmd.Parameters.AddWithValue("@Description", ObjModel.Description);
            cmd.Parameters.AddWithValue("@Version", ObjModel.Version);
            cmd.Parameters.AddWithValue("@Date", ObjModel.Date);
            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public VersionHistoryReport SelectByID(int FinnovativeVersionHistoryID)
        {
            DataTable dt = new DataTable();
            Model.VersionHistoryReport report = new Model.VersionHistoryReport();

            SqlCommand cmd = new SqlCommand("dbo.VersionHistorySelectByID");
            cmd.Parameters.Add("@FinnovativeVersionHistoryID", SqlDbType.Int).Value = FinnovativeVersionHistoryID;
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                report.FinnovativeVersionHistoryID = Convert.ToInt32(dtrow["FinnovativeVersionHistoryID"]);
                report.Version = dtrow["Version"].ToString().ToUpper().Trim();
                report.Title = (dtrow["Title"].ToString());
                report.Description = (dtrow["Description"].ToString());
                report.DOC = (dtrow["DOC"].ToString());

            }

            return report;
        }

        public int Delete(int FinnovativeVersionHistoryID)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.VersionHistoryDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FinnovativeVersionHistoryID", FinnovativeVersionHistoryID);
            try
            {
                Result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }
    }
}
