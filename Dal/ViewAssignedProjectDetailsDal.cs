﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ViewAssignedProjectDetailsDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<ViewAssignedProjectDetailsModel> Select()
        {
            DataTable dt = new DataTable();
            List<ViewAssignedProjectDetailsModel> details = new List<ViewAssignedProjectDetailsModel>();

            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }

            foreach (DataRow dtrow in dt.Rows)
            {
                ViewAssignedProjectDetailsModel report = new ViewAssignedProjectDetailsModel();
                report.ProjectDetailMasterID = Convert.ToInt32(dtrow["ProjectDetailMasterID"].ToString());
                report.DOC = dtrow["DOC"].ToString().Trim();
                report.ProjectName = dtrow["ProjectName"].ToString().Trim();
                report.Status = dtrow["Status"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.ExpectedDelivery = dtrow["ExpectedDelivery"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewAssignedProjectDetailsModel> Search(ViewAssignedProjectDetailsModel ObjModel)
        {
            DataTable dt = new DataTable();
            List<ViewAssignedProjectDetailsModel> details = new List<ViewAssignedProjectDetailsModel>();

            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterSelectForSearch");
            cmd.Parameters.AddWithValue("@FromDate", ObjModel.FromDate);
            cmd.Parameters.AddWithValue("@Todate", ObjModel.ToDate);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewAssignedProjectDetailsModel report = new ViewAssignedProjectDetailsModel();

                report.ProjectDetailMasterID = Convert.ToInt32(dtrow["ProjectDetailMasterID"].ToString());
                report.DOC = dtrow["DOC"].ToString().Trim();
                report.ProjectName = dtrow["ProjectName"].ToString().Trim();
                report.Status = dtrow["Status"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.ExpectedDelivery = dtrow["ExpectedDelivery"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewAssignedProjectDetailsModel> SelectEmployeeName()
        {
            DataTable dt = new DataTable();
            List<ViewAssignedProjectDetailsModel> details = new List<ViewAssignedProjectDetailsModel>();
            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByEmployeeID");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewAssignedProjectDetailsModel report = new ViewAssignedProjectDetailsModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().ToUpper().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewAssignedProjectDetailsModel> SelectEmployeeByUserID(int UserID)
        {
            DataTable dt = new DataTable();
            List<ViewAssignedProjectDetailsModel> details = new List<ViewAssignedProjectDetailsModel>();

            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterSelectByEmployeeID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewAssignedProjectDetailsModel report = new ViewAssignedProjectDetailsModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().ToUpper().Trim();
                report.DOC = dtrow["DOC"].ToString().ToUpper().Trim();
                report.ProjectName = dtrow["ProjectName"].ToString().ToUpper().Trim();
                report.Description = dtrow["Description"].ToString().ToUpper().Trim();
                report.Attachment = dtrow["Attachment"].ToString().ToUpper().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewAssignedProjectDetailsModel> SelectByDate(string FromDate, string ToDate)
        {
            DataTable dt = new DataTable();
            List<ViewAssignedProjectDetailsModel> details = new List<ViewAssignedProjectDetailsModel>();

            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterSelectByDate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewAssignedProjectDetailsModel report = new ViewAssignedProjectDetailsModel();
                report.ProjectDetailMasterID = Convert.ToInt32(dtrow["ProjectDetailMasterID"].ToString());
                report.DOC = dtrow["DOC"].ToString().Trim();
                report.ProjectName = dtrow["ProjectName"].ToString().Trim();
                report.Status = dtrow["Status"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.ExpectedDelivery = dtrow["ExpectedDelivery"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Delete(int ProjectDetailMasterID)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterDeleteForProject");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ProjectDetailMasterID", ProjectDetailMasterID);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            return Result;
        }

        public int Update(ViewAssignedProjectDetailsModel ObjModel)
        {
            int Result = -1;

            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterUpdateStatus");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Status", ObjModel.Status);
            cmd.Parameters.AddWithValue("@ProjectDetailMasterID", ObjModel.ProjectDetailMasterID);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            return Result;
        }

        public List<ViewAssignedProjectDetailsModel> SelectEmployee(int ProjectDetailMasterID)
        {
            DataTable dt = new DataTable();
            List<ViewAssignedProjectDetailsModel> details = new List<ViewAssignedProjectDetailsModel>();

            SqlCommand cmd = new SqlCommand("dbo.ProjectEmployeeMappingSelect");
            cmd.Parameters.AddWithValue("@ProjectDetailMasterID", ProjectDetailMasterID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewAssignedProjectDetailsModel report = new ViewAssignedProjectDetailsModel();
                report.ProjectDetailMasterID = Convert.ToInt32(dtrow["ProjectDetailMasterID"].ToString());
                report.Name = dtrow["Name"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewAssignedProjectDetailsModel> SelectByProjectDetailMasterID(ViewAssignedProjectDetailsModel ObjModel)
        {
            DataTable dt = new DataTable();
            List<ViewAssignedProjectDetailsModel> details = new List<ViewAssignedProjectDetailsModel>();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterSelectForProject");
            cmd.Parameters.AddWithValue("@ProjectDetailMasterID", ObjModel.ProjectDetailMasterID);
            cmd.Parameters.AddWithValue("@UploadType", ObjModel.UploadType);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewAssignedProjectDetailsModel report = new ViewAssignedProjectDetailsModel();
                report.FileID = Convert.ToInt32(dtrow["FileID"].ToString());
                report.Attachment = dtrow["Attachment"].ToString().Trim();
                report.OriginalFileName = dtrow["OriginalFileName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int DeleteFile(int FileID)
        {
            int Result = 0;

            SqlCommand cmd = new SqlCommand("dbo.FileMasterDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FileID", FileID);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            return Result;
        }
    }
}