﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class MemberDetailsDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<MemberDetailsModel> Select()
        {
            DataTable dt = new DataTable();
            List<MemberDetailsModel> details = new List<MemberDetailsModel>();

            SqlCommand cmd = new SqlCommand("dbo.CustomerRegistrationSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                MemberDetailsModel report = new MemberDetailsModel();
                report.RegistrationID = Convert.ToInt32(dtrow["RegistrationID"].ToString());
                report.CustomerName = dtrow["CustomerName"].ToString().Trim();
                report.UserName = dtrow["UserName"].ToString().Trim();
                report.DateAndTime = dtrow["DOC"].ToString().Trim();
                report.GSTNo = dtrow["GSTNo"].ToString().Trim();
                report.PANNo = dtrow["PANNo"].ToString().Trim();
                report.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
                report.IsApproved = Convert.ToInt32(dtrow["IsApproved"]);

                details.Add(report);
            }

            return details;
        }

        public int AcceptRejectCustomer(int RegistrationID, int IsAprroved, string Password)
        {
            int Result = 0;

            if (IsAprroved != 0)
            {
                SqlCommand cmd = new SqlCommand("dbo.UserInformationInsertForApprovedCustomer");

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RegistrationID", RegistrationID);
                cmd.Parameters.AddWithValue("@IsAprroved", IsAprroved);
                cmd.Parameters.AddWithValue("@Password", Password);
                try
                {
                    Result = db.executeStoredProcedure(cmd);
                }
                catch (Exception ex)
                {
                    new Logger().write(ex);
                }
            }




            return Result;
        }

        public List<MemberDetailsModel> SelectCustomerName()
        {
            DataTable dt = new DataTable();
            List<MemberDetailsModel> details = new List<MemberDetailsModel>();

            SqlCommand cmd = new SqlCommand("dbo.CustomerRegistrationSelect");

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                MemberDetailsModel report = new MemberDetailsModel();
                report.RegistrationID = Convert.ToInt32(dtrow["RegistrationID"].ToString());
                report.CustomerName = dtrow["CustomerName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public MemberDetailsModel SelectByRegistrationID(int RegistrationID)
        {
            DataTable dt = new DataTable();


            SqlCommand cmd = new SqlCommand("dbo.CustomerRegistrationSelectByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RegistrationID", RegistrationID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            MemberDetailsModel report = new MemberDetailsModel();
            report.RegistrationID = Convert.ToInt32(dt.Rows[0]["RegistrationID"].ToString());
            report.CustomerName = dt.Rows[0]["CustomerName"].ToString().Trim();
            report.UserName = dt.Rows[0]["UserName"].ToString().Trim();
            report.DateAndTime = dt.Rows[0]["DOC"].ToString().Trim();
            report.GSTNo = dt.Rows[0]["GSTNo"].ToString().Trim();
            report.PANNo = dt.Rows[0]["PANNo"].ToString().Trim();
            report.IsApproved = Convert.ToInt32(dt.Rows[0]["IsApproved"]);
            report.EmailID = dt.Rows[0]["EmailID"].ToString().Trim();
            report.MobileNumber = dt.Rows[0]["MobileNumber"].ToString().Trim();

            return report;
        }
    }
}