﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class KnowledgeBaseDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public List<KnowledgeBaseModel> SelectSystemName()
        {
            DataTable dt = new DataTable();
            List<KnowledgeBaseModel> details = new List<KnowledgeBaseModel>();
            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectSystemName");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    KnowledgeBaseModel report = new KnowledgeBaseModel();

                    report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                    report.SystemName = dtrow["SystemName"].ToString().Trim();
                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }

        public int Insert(KnowledgeBaseModel ObjModel)
        {
            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.KnowledgeBaseMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Issue", ObjModel.Issue);
            cmd.Parameters.AddWithValue("@Resolution", ObjModel.Resolution);
            cmd.Parameters.AddWithValue("@SystemID", ObjModel.SystemID);
            cmd.Parameters.AddWithValue("@UserID",ObjModel.UserID);
           

            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public List<List<KnowledgeBaseModel>> SelectByID(int KnowledgeBaseID)
        {
            List<List<KnowledgeBaseModel>> AllKnowledgeBaseDetails = new List<List<KnowledgeBaseModel>>();

            List<KnowledgeBaseModel> KnowledgeBaseDetails = new List<KnowledgeBaseModel>();


            KnowledgeBaseDetails = KnowledgeBaseSelectByID(KnowledgeBaseID);

            AllKnowledgeBaseDetails.Add(KnowledgeBaseDetails);


            return AllKnowledgeBaseDetails;
        }

        private List<KnowledgeBaseModel> KnowledgeBaseSelectByID(int KnowledgeBaseID)
        {
            DataTable dt = new DataTable();
            List<KnowledgeBaseModel> KnowledgeBaseDetails = new List<KnowledgeBaseModel>();

            SqlCommand cmd = new SqlCommand("dbo.KnowledgeBaseSelectByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@KnowledgeBaseID", KnowledgeBaseID);

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                KnowledgeBaseModel report = new KnowledgeBaseModel();
                report.KnowledgeBaseID = Convert.ToInt32(dtrow["KnowledgeBaseID"].ToString().Trim());
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString().Trim());
                report.Issue = dtrow["Issue"].ToString().Trim();
                report.Resolution = dtrow["Resolution"].ToString().Trim();

                KnowledgeBaseDetails.Add(report);
            }

            return KnowledgeBaseDetails;
        }

        public int UpdateByID(KnowledgeBaseModel ObjModel)
        {
            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.KnowledgeBaseMasterUpdateByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@KnowledgeBaseID", ObjModel.KnowledgeBaseID);
            cmd.Parameters.AddWithValue("@Issue", ObjModel.Issue);
            cmd.Parameters.AddWithValue("@Resolution", ObjModel.Resolution);
            cmd.Parameters.AddWithValue("@SystemID",ObjModel.SystemID);
            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
           
        }

         
    }
}
