﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ViewTimeSheetForEmployeeDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public List<ViewTimeSheetForEmployeeModel> Select()
        {
            DataTable dt = new DataTable();
            List<ViewTimeSheetForEmployeeModel> details = new List<ViewTimeSheetForEmployeeModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterSelectAllReport");

                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    ViewTimeSheetForEmployeeModel report = new ViewTimeSheetForEmployeeModel();

                    report.Name = dtrow["Name"].ToString();
                    report.Date = dtrow["Date"].ToString();
                    report.Time = Convert.ToInt32(dtrow["TotalTime"].ToString());

                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }

        public List<ViewTimeSheetForEmployeeModel> SelectEmployeeName()
        {
            DataTable dt = new DataTable();
            List<ViewTimeSheetForEmployeeModel> details = new List<ViewTimeSheetForEmployeeModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByEmployeeID");

                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    ViewTimeSheetForEmployeeModel report = new ViewTimeSheetForEmployeeModel();
                    report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                    report.Name = dtrow["Name"].ToString().ToUpper().Trim();
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }

        public List<ViewTimeSheetForEmployeeModel> Search(ViewTimeSheetForEmployeeModel ObjModel)
        {
            DataTable dt = new DataTable();
            List<ViewTimeSheetForEmployeeModel> details = new List<ViewTimeSheetForEmployeeModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterSelectAllReportByID");
                cmd.Parameters.AddWithValue("@FromDate", ObjModel.FormDate);
                cmd.Parameters.AddWithValue("@Todate", ObjModel.Todate);
                cmd.Parameters.AddWithValue("@DeveloperName", ObjModel.DeveloperName);

                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    ViewTimeSheetForEmployeeModel report = new ViewTimeSheetForEmployeeModel();

                    report.Name = dtrow["Name"].ToString();
                    report.Date = dtrow["Date"].ToString();
                    report.Time = Convert.ToInt32(dtrow["TotalTime"].ToString());

                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }
    }
}