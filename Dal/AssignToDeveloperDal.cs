﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class AssignToDeveloperDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<AssignToDeveloperModel> SelectEmployeeName()
        {
            DataTable dt = new DataTable();
            List<AssignToDeveloperModel> details = new List<AssignToDeveloperModel>();

            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByEmployeeID");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignToDeveloperModel report = new AssignToDeveloperModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Insert(AssignToDeveloperModel ObjModel)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.TicketInformationInsertByAssignEmployeeID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DeveloperID", ObjModel.DeveloperID);
            cmd.Parameters.AddWithValue("@AssignDate", ObjModel.AssignDate);
            cmd.Parameters.AddWithValue("@AdminRemark", ObjModel.AdminRemark);
            cmd.Parameters.AddWithValue("@TicketID", ObjModel.TicketID);
            cmd.Parameters.AddWithValue("@Description", ObjModel.Description);
            cmd.Parameters.AddWithValue("@SystemID", ObjModel.SystemID);
            SqlParameter Output = new SqlParameter("@Output", SqlDbType.VarChar);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public AssignToDeveloperModel SelectByTicketID(int TicketID)
        {
            DataTable dt = new DataTable();
            AssignToDeveloperModel report = new AssignToDeveloperModel();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByTicketID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TicketID", TicketID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                report.DeveloperID = Convert.ToInt32(dtrow["DeveloperID"].ToString().Trim());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.TeamAny = dtrow["TeamAny"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.TicketNo = dtrow["TicketNo"].ToString().Trim();
                report.Title = dtrow["Title"].ToString().Trim();
                report.AdminRemark = dtrow["AdminRemark"].ToString().Trim();
            }
            return report;
        }

        public List<AssignToDeveloperModel> SelectSystem()
        {
            DataTable dt = new DataTable();
            List<AssignToDeveloperModel> details = new List<AssignToDeveloperModel>();

            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelect");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignToDeveloperModel report = new AssignToDeveloperModel();
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int getUserIDByTicketID(int TicketID)
        {
            DataTable dt = new DataTable();


            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectUserID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TicketID", TicketID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            int UserID = -1;
            if (dt.Rows.Count <= 0)
                return UserID;

            UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
            return UserID;

        }

        public string GetMobileNoByUserID(int AssignEmployeeID)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.UserinformationSelectMobileNoByUserID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", AssignEmployeeID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            string MobileNo = string.Empty;
            if (dt.Rows.Count <= 0)
                return MobileNo;

            MobileNo = (dt.Rows[0]["MobileNumber"]).ToString().Trim();
            return MobileNo;
        }
    }
}