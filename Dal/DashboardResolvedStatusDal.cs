﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class DashboardResolvedStatusDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<DashboardModel> Select(int SystemID)
        {
            DataTable dt = new DataTable();
            List<DashboardModel> details = new List<DashboardModel>();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectResolved");
            cmd.Parameters.AddWithValue("@SystemID", SystemID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                DashboardModel report = new DashboardModel();
                report.TicketID = Convert.ToInt32(dtrow["Ticket"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                report.ResolvedTickets = dtrow["Resolved"].ToString().Trim();

                details.Add(report);
            }

            return details;
        }

        public List<DashboardModel> SelectForEmployee(int SystemID, int UserID)
        {
            DataTable dt = new DataTable();
            List<DashboardModel> details = new List<DashboardModel>();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectResolvedByUserID");
            cmd.Parameters.AddWithValue("@SystemID", SystemID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                DashboardModel report = new DashboardModel();
                report.TicketID = Convert.ToInt32(dtrow["Ticket"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                report.ResolvedTickets = dtrow["Resolved"].ToString().Trim();

                details.Add(report);
            }

            return details;
        }
    }
}