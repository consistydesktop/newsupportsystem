﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class FinovativeSampleURLDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();
        public DataTable selectSampleURLReport()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.SampleURLSelectReport");
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }


            return dt;
        }

        public int Delete(int SampleURLID)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.SampleURLDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SampleURLID", SampleURLID);
            try
            {
                Result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public DataTable SelectBYProviderID(int ProviderID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.SampleURLSelectByProviderID");

            cmd.Parameters.AddWithValue("@ProviderID", ProviderID);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }


            return dt;
        }

        public int Update(SampleURLViewModel ObjModel)
        {

            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.SampleURLUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SampleURLID", ObjModel.SampleURLID);
            cmd.Parameters.AddWithValue("@ProviderID", ObjModel.ProviderID);
            cmd.Parameters.AddWithValue("@ServiceID", ObjModel.ServiceID);
            cmd.Parameters.AddWithValue("@ServiceName", ObjModel.ServiceName);
            cmd.Parameters.AddWithValue("@URL", ObjModel.URL);
            cmd.Parameters.AddWithValue("@DateFormat", ObjModel.DateFormat);
            cmd.Parameters.AddWithValue("@URLType", ObjModel.URLType);
            cmd.Parameters.AddWithValue("@MethodType", ObjModel.MethodType);
            cmd.Parameters.AddWithValue("@HeaderJSON", ObjModel.HeaderJSON);

            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public SampleURLViewModel SelectByID(int SampleURLID)
        {
            DataTable dt = new DataTable();
            Model.SampleURLViewModel report = new Model.SampleURLViewModel();

            SqlCommand cmd = new SqlCommand("dbo.SampleURLSelectByID");
            cmd.Parameters.Add("@SampleURLID", SqlDbType.Int).Value = SampleURLID;
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
               
                report.SampleURLID = Convert.ToInt32(dtrow["SampleURLID"].ToString());
                report.ProviderID = Convert.ToInt32(dtrow["ProviderID"].ToString());
                report.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString());
                report.DeveloperName = (dtrow["DeveloperName"].ToString());
                report.ServiceName = (dtrow["ServiceName"].ToString());
                report.URL = (dtrow["URL"].ToString());
                report.URLType = (dtrow["URLType"].ToString());
                report.MethodType = (dtrow["MethodType"].ToString());
                report.HeaderJSON = (dtrow["HeaderJSON"].ToString());
                report.DateFormat = (dtrow["DateFormat"].ToString());
                
            }

            return report;
        }

        public int Insert(SampleURLViewModel ObjModel)
        {
        
            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.SampleURLInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ProviderID", ObjModel.ProviderID);
            cmd.Parameters.AddWithValue("@ServiceID", ObjModel.ServiceID);
            cmd.Parameters.AddWithValue("@ServiceName", ObjModel.ServiceName);
            cmd.Parameters.AddWithValue("@URL", ObjModel.URL);
            cmd.Parameters.AddWithValue("@DateFormat", ObjModel.DateFormat);
            cmd.Parameters.AddWithValue("@URLType", ObjModel.URLType);
            cmd.Parameters.AddWithValue("@MethodType", ObjModel.MethodType);
            cmd.Parameters.AddWithValue("@HeaderJSON", ObjModel.HeaderJSON);

            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }
    }
}
