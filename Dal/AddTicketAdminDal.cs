﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class AddTicketAdminDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<AddTicketAdminModel> SelectServiceName()
        {
            DataTable dt = new DataTable();
            List<AddTicketAdminModel> details = new List<AddTicketAdminModel>();

            SqlCommand cmd = new SqlCommand("dbo.ServiceMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddTicketAdminModel report = new AddTicketAdminModel();
                report.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString());
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Insert(AddTicketAdminModel objModel)
        {
            int result = 0;

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationInsertTicket");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SystemID", objModel.SystemID);
            cmd.Parameters.AddWithValue("@DeveloperID", objModel.DeveloperID);
            cmd.Parameters.AddWithValue("@UserID", objModel.UserID);
            cmd.Parameters.AddWithValue("@ServiceID", objModel.ServiceID);
            cmd.Parameters.AddWithValue("@RequestType", objModel.RequestType);
            cmd.Parameters.AddWithValue("@Description", objModel.Description);
            cmd.Parameters.AddWithValue("@TeamAny", objModel.TeamAny);
            cmd.Parameters.AddWithValue("@Priority", objModel.Priority);
            cmd.Parameters.AddWithValue("@TicketNo", objModel.TicketNo);

            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public List<AddTicketAdminModel> SelectSystemName()
        {
            DataTable dt = new DataTable();
            List<AddTicketAdminModel> details = new List<AddTicketAdminModel>();

            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectSystemName");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddTicketAdminModel report = new AddTicketAdminModel();

                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public DataTable FileInsert(AddTicketAdminModel ObjModel)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Attachment", ObjModel.Attachment);
            cmd.Parameters.AddWithValue("@OriginalFileName", ObjModel.OriginalFileName);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public int FileMappingInsert(AddTicketAdminModel ObjModel)
        {
            int result = 0;

            SqlCommand cmd = new SqlCommand("dbo.FileMappingMasterInsertByTicket");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TicketID", ObjModel.TicketID);
            cmd.Parameters.AddWithValue("@FileID", ObjModel.FileID);
            cmd.Parameters.AddWithValue("@UploadType", ObjModel.UploadType);
            try
            {
                result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public List<AddTicketAdminModel> SelectEmployeeName()
        {
            DataTable dt = new DataTable();
            List<AddTicketAdminModel> details = new List<AddTicketAdminModel>();

            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByEmployeeID");

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddTicketAdminModel report = new AddTicketAdminModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().ToUpper().Trim();
                details.Add(report);
            }

            return details;
        }
    }
}