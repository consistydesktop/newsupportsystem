﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class AttendanceDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public List<Employee> Select(int Month, int Year)
        {


            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.TimeSheetDetailsSelectForAttendenceTest");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Month", Month);
            cmd.Parameters.AddWithValue("@Year", Year);

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            int NoOfDaysInMonth = DateTime.DaysInMonth(Year, Month);

            int HoliDayCount = GetHolidayCount(Month, Year);

            NoOfDaysInMonth = NoOfDaysInMonth - HoliDayCount;


            List<DateTime> AllDays = GetAllDays(Month, Year);

            List<Employee> EmployeeList = new List<Employee>();

            int PresentDays = 0;

            foreach (DataRow dtrow in dt.Rows)
            {

                Employee e = new Employee();
                int UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                string Name = dtrow["Name"].ToString().ToUpper().Trim();
                string date = dtrow["Date"].ToString().ToUpper().Trim();
                int Time = Convert.ToInt32(dtrow["Time"]);

                date = ((Convert.ToDateTime(date)).Date).ToString("dd/MMMM/yyyy").Trim();

                if (IsHoliDay(date, Month, Year))
                    continue;


                if (!IsPresent(EmployeeList, UserID))
                {
                    PresentDays = 0;

                    e.UserID = UserID;
                    e.Name = Name;

                    if (Time < 301)
                    {
                        e.CountOfLessThan5Hours++;
                        e.DatesOfLessThan5Hour.Add(date);
                    }

                    PresentDays++;

                    EmployeeList.Add(e);
                }
                if (EmployeeList.Count > 0)
                {
                    if (Time < 301)
                    {
                        EmployeeList[EmployeeList.Count - 1].CountOfLessThan5Hours++;
                        EmployeeList[EmployeeList.Count - 1].DatesOfLessThan5Hour.Add(date);
                    }
                    PresentDays++;

                    EmployeeList[EmployeeList.Count - 1].CountOfAbsentDays = NoOfDaysInMonth - PresentDays;
                }


            }

            return EmployeeList;
        }

        int GetHolidayCount(int Month, int Year)
        {

            int HoliDayCount = -1;

            SqlCommand cmd = new SqlCommand("dbo.HolidayMasterSelectByMonthForCount");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Month", Month);
            cmd.Parameters.AddWithValue("@Year", Year);

            DataTable dt = new DataTable();
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (dt.Rows.Count > 0)
                HoliDayCount = Convert.ToInt32(dt.Rows[0]["HoliDayCount"].ToString());


            return HoliDayCount;

        }



        List<DateTime> GetAllDays(int Month, int Year)
        {
            DateTime FirstDate = new DateTime(Year, Month, 1);
            DateTime LastDate = FirstDate.AddMonths(1).AddDays(-1);


            List<DateTime> Alldays = new List<DateTime>();
            Alldays.Add(FirstDate);


            for (int i = 0; i < (DateTime.DaysInMonth(Year, Month)); i++)
                Alldays.Add(FirstDate.AddDays(i));

            return Alldays;
        }



        bool IsPresent(List<Employee> EmployeeList, int UserID)
        {

            if (EmployeeList == null || UserID == 0)
                return false;


            foreach (Employee e in EmployeeList)
            {
                if (e.UserID == UserID)
                    return true;
            }
            return false;
        }


        bool IsHoliDay(string Date, int Month, int Year)
        {
            List<HoliDayModel> Holidays = new List<HoliDayModel>();

            Holidays = getHolidays(Month, Year);

            if (Holidays == null)
                return false;

            foreach (HoliDayModel Day in Holidays)
            {
                DateTime date1 = Convert.ToDateTime(Date);
                DateTime date2 = Convert.ToDateTime(Day.Date);

                if (DateTime.Compare(date1, date2) == 0)
                    return true;
            }

            return false;
        }


        List<HoliDayModel> getHolidays(int Month, int Year)
        {

            List<HoliDayModel> report = new List<HoliDayModel>();

            SqlCommand cmd = new SqlCommand("dbo.HolidayMasterSelectByMonth");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Month", Month);
            cmd.Parameters.AddWithValue("@Year", Year);

            DataTable dt = new DataTable();
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            foreach (DataRow dtrow in dt.Rows)
            {
                HoliDayModel Day = new HoliDayModel();
                Day.DayID = Convert.ToInt32(dtrow["DayID"].ToString());
                Day.Date = Convert.ToDateTime(dtrow["Date"].ToString().ToUpper().Trim());
                report.Add(Day);
            }

            return report;
        }
    }
}
