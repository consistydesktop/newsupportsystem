﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class TicketReportForCustomerDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<TicketReportForCustomerModel> Select(int UserID)
        {
            DataTable dt = new DataTable();
            List<TicketReportForCustomerModel> details = new List<TicketReportForCustomerModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectForCustomer");
                cmd.Parameters.AddWithValue("@UserID", UserID);
                dt = db.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    TicketReportForCustomerModel report = new TicketReportForCustomerModel();
                    report.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());
                    report.TicketNo = dtrow["TicketNo"].ToString().Trim();
                    report.GeneratedDate = dtrow["TicketGeneratedDate"].ToString().Trim();
                    report.ResolvedDate = dtrow["ResolvedDate"].ToString().Trim();
                    report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                    report.RequestType = dtrow["RequestType"].ToString().Trim();
                    report.Priority = dtrow["Priority"].ToString().Trim();
                    report.Description = dtrow["Description"].ToString().Trim();

                    report.TicketStatus = dtrow["TicketStatus"].ToString().Trim();
                    report.EmployeeRemark = dtrow["EmployeeRemark"].ToString().Trim();
                    report.AdminRemark = dtrow["AdminRemark"].ToString().Trim();
                    report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }

        public List<TicketReportForCustomerModel> Search(string FromDate, string ToDate, int UserID)
        {
            DataTable dt = new DataTable();
            List<TicketReportForCustomerModel> details = new List<TicketReportForCustomerModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByDateForCustomer");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", Convert.ToDateTime(FromDate));
                cmd.Parameters.AddWithValue("@ToDate", Convert.ToDateTime(ToDate));
                cmd.Parameters.AddWithValue("@UserID", UserID);
                dt = db.getDataTable(cmd);

                foreach (DataRow dtrow in dt.Rows)
                {
                    TicketReportForCustomerModel report = new TicketReportForCustomerModel();
                    report.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());
                    report.TicketNo = dtrow["TicketNo"].ToString().Trim();
                    report.GeneratedDate = dtrow["TicketGeneratedDate"].ToString().Trim();
                    report.ResolvedDate = dtrow["ResolvedDate"].ToString().Trim();
                    report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                    report.RequestType = dtrow["RequestType"].ToString().Trim();
                    report.Priority = dtrow["Priority"].ToString().Trim();
                    report.Description = dtrow["Description"].ToString().Trim();

                    report.TicketStatus = dtrow["TicketStatus"].ToString().Trim();
                    report.EmployeeRemark = dtrow["EmployeeRemark"].ToString().Trim();
                    report.AdminRemark = dtrow["AdminRemark"].ToString().Trim();
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }

        public List<TicketReportForCustomerModel> SelectBySystemID(int SystemID, int UserID)
        {
            DataTable dt = new DataTable();
            List<TicketReportForCustomerModel> details = new List<TicketReportForCustomerModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectBySystemID");
                cmd.Parameters.AddWithValue("@SystemID", SystemID);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                dt = db.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    TicketReportForCustomerModel report = new TicketReportForCustomerModel();
                    report.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());
                    report.TicketNo = dtrow["TicketNo"].ToString().Trim();
                    report.GeneratedDate = dtrow["TicketGeneratedDate"].ToString().Trim();
                    report.ResolvedDate = dtrow["ResolvedDate"].ToString().Trim();
                    report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                    report.RequestType = dtrow["RequestType"].ToString().Trim();
                    report.Priority = dtrow["Priority"].ToString().Trim();
                    report.Description = dtrow["Description"].ToString().Trim();

                    report.TicketStatus = dtrow["TicketStatus"].ToString().Trim();
                    report.EmployeeRemark = dtrow["EmployeeRemark"].ToString().Trim();
                    report.AdminRemark = dtrow["AdminRemark"].ToString().Trim();
                    report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }
    }
}