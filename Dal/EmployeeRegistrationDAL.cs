﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class EmployeeRegistrationDAL
    {
        private DatabaseProcessing db = new DatabaseProcessing();
        public int Insert(EmployeeRegistrationModel objModel)
        {
            int result = 0;
            SqlCommand cmd = new SqlCommand("dbo.UserInformationInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FirstName", objModel.FirstName);
            cmd.Parameters.AddWithValue("@LastName", objModel.LastName);
            cmd.Parameters.AddWithValue("@Username", objModel.Username);
            cmd.Parameters.AddWithValue("@EmailID", objModel.EmailID);
            cmd.Parameters.AddWithValue("@Password", objModel.Password);
            cmd.Parameters.AddWithValue("@UserTypeID", objModel.UserTypeID);
            cmd.Parameters.AddWithValue("@MobileNumber", objModel.MobileNumber);
            cmd.Parameters.AddWithValue("@DateOfJoining", objModel.DateOfJoining);
            cmd.Parameters.AddWithValue("@EmployeeMentStatus", objModel.EmployeeMentStatus);
            try
            {
                result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public List<EmployeeRegistrationModel> Select()
        {
            List<EmployeeRegistrationModel> details = new List<EmployeeRegistrationModel>();

            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelect");
            DataTable dt = new DataTable();
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                EmployeeRegistrationModel report = new EmployeeRegistrationModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.FirstName = dtrow["FirstName"].ToString();
                report.LastName = dtrow["LastName"].ToString();
                report.Username = dtrow["Username"].ToString().Trim();
                report.EmailID = dtrow["EmailID"].ToString().Trim();
                report.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public EmployeeRegistrationModel SelectByID(int UserID)
        {
            DataTable dt = new DataTable();
            EmployeeRegistrationModel Register = new EmployeeRegistrationModel();
            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByID");
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                Register.FirstName = dtrow["FirstName"].ToString().Trim();
                Register.LastName = dtrow["LastName"].ToString().Trim();
                Register.Username = dtrow["Username"].ToString().Trim();
                Register.EmailID = dtrow["EmailID"].ToString().Trim();
                Register.MobileNumber = dtrow["MobileNumber"].ToString().Trim();

                //Register.JoiningDate = dtrow["DateOfJoining"].ToString();
                Register.DateOfJoining =Convert.ToDateTime(dtrow["DateOfJoining"].ToString());
                Register.DateOfReliving = Convert.ToDateTime(dtrow["DateOfReliving"].ToString());
                Register.EmployeeMentStatus = dtrow["EmployeeMentStatus"].ToString().Trim();
            }
            return Register;
        }

        public int Update(EmployeeRegistrationModel ObjModel)
        {
            int Result = -1;


            SqlCommand cmd = new SqlCommand("dbo.UserInformationEmployeeUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", ObjModel.UserID);
            cmd.Parameters.AddWithValue("@FirstName", ObjModel.FirstName);
            cmd.Parameters.AddWithValue("@LastName", ObjModel.LastName);
            cmd.Parameters.AddWithValue("@Username", ObjModel.Username);
            cmd.Parameters.AddWithValue("@EmailID", ObjModel.EmailID);
            cmd.Parameters.AddWithValue("@MobileNumber", ObjModel.MobileNumber);
            cmd.Parameters.AddWithValue("@DateOfJoining", ObjModel.DateOfJoining);
            cmd.Parameters.AddWithValue("@DateOfReliving", ObjModel.DateOfReliving);
            cmd.Parameters.AddWithValue("@EmployeeMentStatus", ObjModel.EmployeeMentStatus);
       
    
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int Delete(int UserID)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.UserInformationDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }
    }
}