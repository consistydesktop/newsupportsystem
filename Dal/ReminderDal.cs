﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ReminderDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public int Insert(ReminderModel ObjModel)
        {
            int result = -1;

            SqlCommand cmd = new SqlCommand("dbo.ReminderMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CustomerName", ObjModel.CustomerName);
            cmd.Parameters.AddWithValue("@ReminderType", ObjModel.ReminderType);
            cmd.Parameters.AddWithValue("@ReminderDate", ObjModel.ReminderDate);
            cmd.Parameters.AddWithValue("@Amount", (ObjModel.Amount));
            cmd.Parameters.AddWithValue("@Description", (ObjModel.Description));
            cmd.Parameters.AddWithValue("@UserID", (ObjModel.UserID));
            cmd.Parameters.AddWithValue("@Status", (ObjModel.Status));
            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public List<ReminderModel> Select()
        {
            DataTable dt = new DataTable();
            List<ReminderModel> details = new List<ReminderModel>();

            SqlCommand cmd = new SqlCommand("dbo.ReminderMasterSelect");

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ReminderModel report = new ReminderModel();
                report.ReminderMasterID = Convert.ToInt32(dtrow["ReminderMasterID"].ToString());
                report.CustomerName = dtrow["CustomerName"].ToString().Trim();
                report.TypeName = dtrow["TypeName"].ToString().Trim();
                report.Amount = Convert.ToDouble(dtrow["Amount"].ToString().Trim());
                report.ReminderDate = dtrow["ReminderDate"].ToString().Trim();
                report.Status = dtrow["status"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                details.Add(report);
            }


            return details;
        }

        public List<ReminderModel> SelectByID(int ReminderMasterID, int UserID)
        {
            DataTable dt = new DataTable();
            List<ReminderModel> details = new List<ReminderModel>();
            SqlCommand cmd = new SqlCommand("dbo.ReminderMasterSelectByID");
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@ReminderMasterID", ReminderMasterID);
            try
            {

                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            foreach (DataRow dtrow in dt.Rows)
            {
                ReminderModel report = new ReminderModel();
                report.ReminderMasterID = Convert.ToInt32(dtrow["ReminderMasterID"].ToString());
                report.CustomerName = dtrow["CustomerName"].ToString().Trim();
                report.ReminderType = dtrow["ReminderType"].ToString().Trim();
                report.Amount = Convert.ToDouble(dtrow["Amount"].ToString().Trim());
                report.ReminderDate = dtrow["ReminderDate"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Delete(int ReminderMasterID)
        {
            int result = -1;
            List<ReminderModel> details = new List<ReminderModel>();
            SqlCommand cmd = new SqlCommand("dbo.ReminderMasterDelete");
            cmd.Parameters.AddWithValue("@ReminderMasterID", ReminderMasterID);
            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public int Update(ReminderModel ObjModel)
        {
            int result = -1;

            SqlCommand cmd = new SqlCommand("dbo.ReminderMasterUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReminderMasterID", ObjModel.ReminderMasterID);
            cmd.Parameters.AddWithValue("@CustomerName", ObjModel.CustomerName);
            cmd.Parameters.AddWithValue("@ReminderType", ObjModel.ReminderType);
            cmd.Parameters.AddWithValue("@ReminderDate", ObjModel.ReminderDate);
            cmd.Parameters.AddWithValue("@Amount", (ObjModel.Amount));
            cmd.Parameters.AddWithValue("@Description", (ObjModel.Description));

            cmd.Parameters.AddWithValue("@Status", (ObjModel.Status));
            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public List<ReminderModel> SelectCustomerName()
        {
            DataTable dt = new DataTable();
            List<ReminderModel> details = new List<ReminderModel>();
            SqlCommand cmd = new SqlCommand("dbo.ReminderMasterSelectCustomerName");
            try
            {

                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            foreach (DataRow dtrow in dt.Rows)
            {
                ReminderModel report = new ReminderModel();
                report.CustomerName = dtrow["CustomerName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ReminderModel> SelectReminderType()
        {
            DataTable dt = new DataTable();
            List<ReminderModel> details = new List<ReminderModel>();

            SqlCommand cmd = new SqlCommand("dbo.ReminderTypeMasterSelectReminderType");
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            foreach (DataRow dtrow in dt.Rows)
            {
                ReminderModel report = new ReminderModel();
                report.ReminderTypeID = Convert.ToInt32(dtrow["ReminderTypeID"].ToString().Trim());
                report.TypeName = dtrow["TypeName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ReminderModel> Search(ReminderModel ObjModel)
        {
            DataTable dt = new DataTable();
            List<ReminderModel> details = new List<ReminderModel>();

            SqlCommand cmd = new SqlCommand("dbo.ReminderMasterSelectForSearch");
            cmd.Parameters.AddWithValue("@FromDate", ObjModel.FormDate);
            cmd.Parameters.AddWithValue("@Todate", ObjModel.Todate);
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            var filterTerms = new List<string>();

            if (ObjModel.Status != "0")
                filterTerms.Add(string.Format("Status like '%{0}%'", ObjModel.Status));

            dt.DefaultView.RowFilter = string.Join(" AND ", filterTerms);

            dt = (dt.DefaultView).ToTable();

            foreach (DataRow dtrow in dt.Rows)
            {
                ReminderModel report = new ReminderModel();

                report.ReminderMasterID = Convert.ToInt32(dtrow["ReminderMasterID"].ToString());
                report.CustomerName = dtrow["CustomerName"].ToString().Trim();
                report.TypeName = dtrow["TypeName"].ToString().Trim();
                report.Amount = Convert.ToDouble(dtrow["Amount"].ToString().Trim());
                report.Status = dtrow["Status"].ToString().Trim();
                report.ReminderDate = dtrow["ReminderDate"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();

                details.Add(report);
            }


            return details;
        }

        public List<ReminderModel> SelectSystemName()
        {
            DataTable dt = new DataTable();
            List<ReminderModel> details = new List<ReminderModel>();
            SqlCommand cmd = new SqlCommand("dbo.ReminderMasterSelectCustomerName");
            try
            {

                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            foreach (DataRow dtrow in dt.Rows)
            {
                ReminderModel report = new ReminderModel();
                report.CustomerName = dtrow["CustomerName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }
    }
}