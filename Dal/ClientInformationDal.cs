﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class ClientInformationDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();


        public DataTable SelectClient()
        {
            DataTable dt = new DataTable();


            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectClient");
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;


        }

        public int UpdateAciveUser(int UserID, int IsActive)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.UserInformationUpdateForClientActivation");

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@IsActive", IsActive);

            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public DataTable UserInformationSelectClientByID(int UserID)
        {
            DataTable dt = new DataTable();


            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectClientByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }
    }
}
