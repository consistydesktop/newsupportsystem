﻿using DAL;
using Model;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ActionDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public int Insert(ActionModel ObjModel)
        {
            int Result = 0;

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationInsertAction");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ResolvedDate", ObjModel.ResolvedDate);
            cmd.Parameters.AddWithValue("@TicketStatus", ObjModel.TicketStatus);
            cmd.Parameters.AddWithValue("@EmployeeRemark", ObjModel.EmployeeRemark);
            cmd.Parameters.AddWithValue("@TicketID", ObjModel.TicketID);

            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                Result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return Result;
        }

        public ActionModel SelectByTicketID(int TicketID, int UserID)
        {
            DataTable dt = new DataTable();
            ActionModel report = new ActionModel();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByTicketIDForAction");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TicketID", TicketID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                report.Description = dtrow["Description"].ToString().Trim();
                report.Priority = dtrow["Priority"].ToString().Trim();
                report.TicketID = Convert.ToInt32(dtrow["TicketID"]);
                report.TicketStatus = dtrow["TicketStatus"].ToString();
                report.EmployeeRemark = dtrow["EmployeeRemark"].ToString();
                report.TicketNo = dtrow["TicketNo"].ToString();
                report.UserID =  Convert.ToInt32(dtrow["UserID"]);
            }

            return report;
        }
    }
}