﻿using DAL;
using Model;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class LeaveRequestDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public int Insert(LeaveRequestModel ObjModel)
        {
            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.LeaveMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", ObjModel.FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ObjModel.ToDate);
            cmd.Parameters.AddWithValue("@Description", ObjModel.Description);
            cmd.Parameters.AddWithValue("@UserID", ObjModel.UserID);
            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;

        }
    }
}
