﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class TimeSheetReportDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public List<TimeSheetModel> Select(int UserID)
        {
            DataTable dt = new DataTable();
            List<TimeSheetModel> details = new List<TimeSheetModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterSelectForReort");
                cmd.Parameters.AddWithValue("@UserID", UserID);

                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    TimeSheetModel report = new TimeSheetModel();
                    report.TotalTime = Convert.ToInt32(dtrow["TotalTime"].ToString());
                    report.Date = dtrow["Date"].ToString().Trim();
      
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }

        public List<TimeSheetModel> Search(TimeSheetModel ObjModel)
        {
            DataTable dt = new DataTable();
            List<TimeSheetModel> details = new List<TimeSheetModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterSelectForSearch");
                cmd.Parameters.AddWithValue("@FromDate", ObjModel.FormDate);
                cmd.Parameters.AddWithValue("@Todate", ObjModel.Todate);
                cmd.Parameters.AddWithValue("@UserID", ObjModel.UserID);
                dt = dp.getDataTable(cmd);

                foreach (DataRow dtrow in dt.Rows)
                {
                    TimeSheetModel report = new TimeSheetModel();
                    report.TotalTime = Convert.ToInt32(dtrow["TotalTime"].ToString());
                    report.Date = dtrow["Date"].ToString().Trim();
          
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }

        public List<TimeSheetModel> SelectByID(string Date, int UserID)
        {
            DataTable dt = new DataTable();
            List<TimeSheetModel> details = new List<TimeSheetModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterSelectByIDForReport");
                cmd.Parameters.AddWithValue("@Date", Date);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    TimeSheetModel report = new TimeSheetModel();

                    report.TimeSheetDetailID = Convert.ToInt32(dtrow["TimeSheetDetailID"].ToString());
                    report.Task = dtrow["Task"].ToString();
                    report.SystemName = (dtrow["SystemName"].ToString());
                    report.Date = dtrow["Date"].ToString();
                    report.Time = Convert.ToInt32(dtrow["Time"].ToString());
                    report.Status = Convert.ToInt32(dtrow["Status"].ToString());
                 
                 
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }
    }
}