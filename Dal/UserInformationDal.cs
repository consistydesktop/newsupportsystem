﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class UserInformationDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<UserInformationModel> Select(int UserTypeID,int LoginUserID)
        {
            List<UserInformationModel> details = new List<UserInformationModel>();

            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectForAll");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserTypeID", UserTypeID);
            cmd.Parameters.AddWithValue("@LoginUserID", LoginUserID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (UserTypeID != 3)
            {
                foreach (DataRow dtrow in dt.Rows)
                {
                    UserInformationModel report = new UserInformationModel();
                    report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                    report.UserTypeID = Convert.ToInt32(dtrow["UserTypeID"].ToString());
                    report.Name = dtrow["Name"].ToString();
                    report.Username = dtrow["Username"].ToString().Trim();
                    report.EmailID = dtrow["EmailID"].ToString().Trim();
                    report.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
                   

                    details.Add(report);
                }
            }
            else
            {
                foreach (DataRow dtrow in dt.Rows)
                {
                    UserInformationModel report = new UserInformationModel();

                    report.UserID = Convert.ToInt32(dtrow["UserID"].ToString().Trim());
                    report.Name = dtrow["Name"].ToString().Trim();
                    report.Username = dtrow["Username"].ToString().Trim();
                    report.EmailID = dtrow["EmailID"].ToString().Trim();
                    report.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
                    report.GSTNo = dtrow["GSTNo"].ToString().Trim();
                    report.PANNo = dtrow["PANNo"].ToString().Trim();
                    report.Address = dtrow["Address"].ToString().Trim();
                    report.State = dtrow["State"].ToString().Trim();
                    report.IsActive = Convert.ToInt32(dtrow["IsActive"]);
                    report.UserTypeID = Convert.ToInt32(dtrow["UserTypeID"]);
                    report.LoginToken = dtrow["LoginToken"].ToString().Trim();
                    details.Add(report);
                }
            }

            return details;
        }

        public UserInformationModel SelectByID(int UserID)
        {
            UserInformationModel Register = new UserInformationModel();

            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByIDForAll");
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
            DataTable dt = new DataTable();
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                Register.Name = dtrow["Name"].ToString().Trim();
                Register.Username = dtrow["Username"].ToString().Trim();
                Register.EmailID = dtrow["EmailID"].ToString().Trim();
                Register.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
            }

            return Register;
        }

        public int Update(UserInformationModel ObjModel)
        {
            int Result = -1;

            SqlCommand cmd = new SqlCommand("dbo.UserInformationUpdateForAll");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", ObjModel.UserID);
            cmd.Parameters.AddWithValue("@Name", ObjModel.Name);
            cmd.Parameters.AddWithValue("@Username", ObjModel.Username);
            cmd.Parameters.AddWithValue("@EmailID", ObjModel.EmailID);
            cmd.Parameters.AddWithValue("@MobileNumber", ObjModel.MobileNumber);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int Delete(int UserID)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.UserInformationDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int UpdateAciveUser(int UserID, int IsActive)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.UserInformationUpdateForClientActivation");

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@IsActive", IsActive);

            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public DataTable GetEmailByUserID(int UserID)
        {

            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectForEmail");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);

            DataTable dt = new DataTable();
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
          

            return dt;
        }

        public DataTable UserinformationSelectForIncompleteTimeSheet(string date)
        {
           
            SqlCommand cmd = new SqlCommand("dbo.UserinformationSelectForIncompleteTimeSheet");
            cmd.Parameters.Add("@date", SqlDbType.Date).Value = date;
            DataTable dt = new DataTable();
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }
    }
}