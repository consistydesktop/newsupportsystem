﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ViewTicketsAdminDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<ViewTicketsAdminModel> SelectEmployeeName()
        {
            DataTable dt = new DataTable();
            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();
            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByEmployeeID");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTicketsAdminModel report = new ViewTicketsAdminModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewTicketsAdminModel> SelectServiceName()
        {
            DataTable dt = new DataTable();
            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();

            SqlCommand cmd = new SqlCommand("dbo.ServiceMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTicketsAdminModel report = new ViewTicketsAdminModel();
                report.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString());
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewTicketsAdminModel> SelectSystemName()
        {
            DataTable dt = new DataTable();
            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();
            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectSystemName");
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTicketsAdminModel report = new ViewTicketsAdminModel();

                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewTicketsAdminModel> Select()
        {
            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTicketsAdminModel report = new ViewTicketsAdminModel();
                report.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());
                report.TicketNo = dtrow["TicketNo"].ToString().Trim();
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.DOC = Convert.ToDateTime(dtrow["TicketGeneratedDate"].ToString().Trim());
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.TicketStatus = dtrow["TicketStatus"].ToString().Trim();
                report.EmployeeRemark = dtrow["EmployeeRemark"].ToString().Trim();
                report.AdminRemark = dtrow["AdminRemark"].ToString().Trim();
                report.RaisedBy = dtrow["RaisedBy"].ToString().Trim();
                report.AssignedTo = dtrow["AssignedTo"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewTicketsAdminModel> Search(string FromDate, string ToDate)
        {
            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByDate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTicketsAdminModel report = new ViewTicketsAdminModel();
                report.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.DOC = Convert.ToDateTime(dtrow["TicketGeneratedDate"].ToString().Trim());
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.TicketStatus = dtrow["TicketStatus"].ToString().Trim();
                report.EmployeeRemark = dtrow["EmployeeRemark"].ToString().Trim();
                report.AdminRemark = dtrow["AdminRemark"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewTicketsAdminModel> SelectByTicketID(int TicketID)
        {
            DataTable dt = new DataTable();
            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByTicketID");
            cmd.Parameters.AddWithValue("@TicketID", TicketID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTicketsAdminModel report = new ViewTicketsAdminModel();
                report.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.DOC = Convert.ToDateTime(dtrow["TicketGeneratedDate"].ToString().Trim());
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.TicketStatus = dtrow["TicketStatus"].ToString().Trim();
                report.EmployeeRemark = dtrow["EmployeeRemark"].ToString().Trim();
                report.AdminRemark = dtrow["AdminRemark"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }


        public List<ViewTicketsAdminModel> SearchByFilter(string FromDate, string ToDate, int DeveloperID, int SystemID, int ServiceID, string TicketNo)
        {
            List<ViewTicketsAdminModel> details = new List<ViewTicketsAdminModel>();
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByDate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", Convert.ToDateTime(FromDate));
            cmd.Parameters.AddWithValue("@ToDate", Convert.ToDateTime(ToDate));
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            var filterTerms = new List<string>();

            if (DeveloperID != 0)
                filterTerms.Add(string.Format("DeveloperID = {0}", DeveloperID));

            if (!TicketNo.Equals(""))
                filterTerms.Add(string.Format("TicketNo like '%{0}%'", TicketNo));

            if (SystemID != 0)
                filterTerms.Add(string.Format("SystemID = {0}", SystemID));

            if (ServiceID != 0)
                filterTerms.Add(string.Format("ServiceID = {0}", ServiceID));

            dt.DefaultView.RowFilter = string.Join(" AND ", filterTerms);

            dt = (dt.DefaultView).ToTable();

            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTicketsAdminModel report = new ViewTicketsAdminModel();
                report.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString().Trim());
                report.TicketNo = dtrow["TicketNo"].ToString().Trim();
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.DOC = Convert.ToDateTime(dtrow["TicketGeneratedDate"].ToString().Trim());
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.TicketStatus = dtrow["TicketStatus"].ToString().Trim();
                report.EmployeeRemark = dtrow["EmployeeRemark"].ToString().Trim();
                report.AdminRemark = dtrow["AdminRemark"].ToString().Trim();
                report.RaisedBy = dtrow["RaisedBy"].ToString().Trim();
                report.AssignedTo = dtrow["AssignedTo"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }
    }
}