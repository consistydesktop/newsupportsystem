﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class AddSubscriptionScreenDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<AddSubscriptionScreenModel> SelectSystemName()
        {
            DataTable dt = new DataTable();
            List<AddSubscriptionScreenModel> details = new List<AddSubscriptionScreenModel>();

            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectSystemName");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddSubscriptionScreenModel report = new AddSubscriptionScreenModel();

                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<AddSubscriptionScreenModel> SelectDesktop()
        {
            DataTable dt = new DataTable();
            List<AddSubscriptionScreenModel> details = new List<AddSubscriptionScreenModel>();
            SqlCommand cmd = new SqlCommand("dbo.DesktopMasterSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddSubscriptionScreenModel report = new AddSubscriptionScreenModel();

                report.DesktopID = Convert.ToInt32(dtrow["DesktopID"].ToString());
                report.Desktop = dtrow["Desktop"].ToString().ToUpper().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Insert(AddSubscriptionScreenModel objModel)
        {
            int Result = 0;

            SqlCommand cmd = new SqlCommand("dbo.SubscriptionMasterInsert");

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SystemID", objModel.SystemID);
            cmd.Parameters.AddWithValue("@DesktopID", objModel.DesktopID);
            cmd.Parameters.AddWithValue("@Description", objModel.Description);
            cmd.Parameters.AddWithValue("@DemoYear", objModel.DemoYear);
            cmd.Parameters.AddWithValue("@PaymentStatus", objModel.PaymentStatus);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch
            {
                throw;
            }

            return Result;
        }

        public List<AddSubscriptionScreenModel> Select()
        {
            List<AddSubscriptionScreenModel> details = new List<AddSubscriptionScreenModel>();

            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.SubscriptionMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddSubscriptionScreenModel report = new AddSubscriptionScreenModel();
                report.SubscriptionID = Convert.ToInt32(dtrow["SubscriptionID"].ToString());
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.DesktopID = Convert.ToInt32(dtrow["DesktopID"].ToString());
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().Trim());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.Desktop = dtrow["Desktop"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.DemoYear = dtrow["DemoYear"].ToString().Trim();
                report.PaymentStatus = dtrow["PaymentStatus"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public AddSubscriptionScreenModel SelectByID(int SubscriptionID)
        {
            DataTable dt = new DataTable();
            AddSubscriptionScreenModel report = new AddSubscriptionScreenModel();

            SqlCommand cmd = new SqlCommand("dbo.SubscriptionMasterSelectByID");
            cmd.Parameters.Add("@SubscriptionID", SqlDbType.Int).Value = SubscriptionID;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                report.SubscriptionID = Convert.ToInt32(dtrow["SubscriptionID"].ToString());
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.DesktopID = Convert.ToInt32(dtrow["DesktopID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.Desktop = dtrow["Desktop"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.DemoYear = dtrow["DemoYear"].ToString().Trim();
                report.PaymentStatus = dtrow["PaymentStatus"].ToString().Trim();
            }

            return report;
        }

        public int Update(AddSubscriptionScreenModel ObjModel)
        {
            int Result = -1;

            SqlCommand cmd = new SqlCommand("dbo.SubscriptionMasterUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubscriptionID", ObjModel.SubscriptionID);
         
            cmd.Parameters.AddWithValue("@DesktopID", ObjModel.DesktopID);
            cmd.Parameters.AddWithValue("@Description", ObjModel.Description);
            cmd.Parameters.AddWithValue("@DemoYear", ObjModel.DemoYear);
            cmd.Parameters.AddWithValue("@PaymentStatus", ObjModel.PaymentStatus);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int Delete(int SubscriptionID)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.SubscriptionMasterDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubscriptionID", SubscriptionID);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }
    }
}