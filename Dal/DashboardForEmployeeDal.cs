﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class DashboardForEmployeeDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<DashboardForEmployeeModel> SelectTicketDetails(int UserID)
        {
            DataTable dt = new DataTable();
            List<DashboardForEmployeeModel> details = new List<DashboardForEmployeeModel>();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectPendingAndResolvedTicketsByUserID");
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                DashboardForEmployeeModel report = new DashboardForEmployeeModel();
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.PendingTickets = dtrow["Pending"].ToString().Trim();
                report.ResolvedTickets = dtrow["Resolved"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<DashboardForEmployeeModel> SelectProjectDetails(int UserID)
        {
            DataTable dt = new DataTable();
            List<DashboardForEmployeeModel> details = new List<DashboardForEmployeeModel>();

            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterSelectByUserID");
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                DashboardForEmployeeModel report = new DashboardForEmployeeModel();

                report.ProjectDetailMasterID = Convert.ToInt32(dtrow["ProjectDetailMasterID"].ToString());
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().Trim());
                report.ProjectName = dtrow["ProjectName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int SelectPendingProjects(int UserID)
        {
            int result = -1;

            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterSelectByUserIDForPending");
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public int SelectPendingCalls(int UserID)
        {
            int result = -1;

            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterSelectByUserIDForPending");
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public int SelectPendingTickets(int UserID)
        {
            int result = -1;

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByUserIDForPending");
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public int SelectPendingTasks(int UserID)
        {
            int result = -1;

            SqlCommand cmd = new SqlCommand("dbo.TaskMasterSelectByUserIDForPending");
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public int SelectPendingBugs(int UserID)
        {
            int result = -1;

            SqlCommand cmd = new SqlCommand("dbo.BugMasterSelectByUserIDForPending");
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public List<DashboardForEmployeeModel> SelectCallDetails(int UserID)
        {
            DataTable dt = new DataTable();
            List<DashboardForEmployeeModel> details = new List<DashboardForEmployeeModel>();
            SqlCommand cmd = new SqlCommand("dbo.CallMasterSelectForEmployee");
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                DashboardForEmployeeModel report = new DashboardForEmployeeModel();
                report.CallID = Convert.ToInt32(dtrow["CallID"].ToString());
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.TeamAny = dtrow["TeamAny"].ToString().Trim();
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().Trim());
                report.CallIssueID = Convert.ToInt32(dtrow["CallIssueID"].ToString());
                report.IssueName = dtrow["IssueName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<DashboardForEmployeeModel> SelectBugDetails(int UserID)
        {
            DataTable dt = new DataTable();
            List<DashboardForEmployeeModel> details = new List<DashboardForEmployeeModel>();

            SqlCommand cmd = new SqlCommand("dbo.BugMasterSelectForEmployee");
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                DashboardForEmployeeModel report = new DashboardForEmployeeModel();

                report.BugID = Convert.ToInt32(dtrow["BugID"].ToString());
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().Trim());
                report.BugTitle = dtrow["BugTitle"].ToString().Trim();
                report.TestingIssueID = Convert.ToInt32(dtrow["TestingIssueID"].ToString());
                report.IssueName = dtrow["IssueName"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.ExpectedResult = dtrow["ExpectedResult"].ToString().Trim();
                report.PageURL = dtrow["PageURL"].ToString().Trim();
                report.Priority = dtrow["Priority"].ToString().Trim();
                report.Attachment = dtrow["Attachment"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<DashboardForEmployeeModel> SelectTaskDetails(int UserID)
        {
            DataTable dt = new DataTable();
            List<DashboardForEmployeeModel> details = new List<DashboardForEmployeeModel>();

            SqlCommand cmd = new SqlCommand("dbo.TaskMasterSelectForEmployee");
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                DashboardForEmployeeModel report = new DashboardForEmployeeModel();

                report.TaskID = Convert.ToInt32(dtrow["TaskID"].ToString());
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().Trim());
                report.Description = dtrow["Description"].ToString().Trim();
                report.Task = dtrow["Task"].ToString().Trim();
                report.Status = (dtrow["Status"].ToString().Trim());
                report.Deadline = dtrow["Deadline"].ToString().Trim();
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int UpdateStatus(DashboardForEmployeeModel ObjModel)
        {
            int Result = -1;

            SqlCommand cmd = new SqlCommand("dbo.TaskMasterUpdateStatus");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Status", ObjModel.Status);
            cmd.Parameters.AddWithValue("@TaskID", ObjModel.TaskID);
            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                Result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public List<DashboardForEmployeeModel> SelectByTaskID(DashboardForEmployeeModel ObjModel)
        {
            DataTable dt = new DataTable();
            List<DashboardForEmployeeModel> details = new List<DashboardForEmployeeModel>();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterSelectForTask");
            cmd.Parameters.AddWithValue("@TaskID", ObjModel.TaskID);
            cmd.Parameters.AddWithValue("@UploadType", ObjModel.UploadType);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                DashboardForEmployeeModel report = new DashboardForEmployeeModel();
                report.FileID = Convert.ToInt32(dtrow["FileID"].ToString());
                report.Attachment = dtrow["Attachment"].ToString().Trim();
                report.OriginalFileName = dtrow["OriginalFileName"].ToString().Trim();

                details.Add(report);
            }

            return details;
        }

        public DataTable FileMappingInsert(DashboardForEmployeeModel ObjModel)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.FileMappingMasterInsertByTask");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TaskID", ObjModel.TaskID);
            cmd.Parameters.AddWithValue("@UploadType", ObjModel.UploadType);
            cmd.Parameters.AddWithValue("@FileID", ObjModel.FileID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public string SelectDescriptionByTaskID(DashboardForEmployeeModel ObjModel)
        {
            DataTable dt = new DataTable();

            string description = "";
            SqlCommand cmd = new SqlCommand("dbo.SelectDescriptionByTaskID");
            cmd.Parameters.AddWithValue("@TaskID", ObjModel.TaskID);
            
            try
            {
                dt = db.getDataTable(cmd);
                description = dt.Rows[0]["Description"].ToString().Trim();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return description;
        }
        
    }
}