﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class AndroidSupportDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public List<AndroidSupportModel> SelectSystemName()
        {
            DataTable dt = new DataTable();
            List<AndroidSupportModel> details = new List<AndroidSupportModel>();
            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectSystemName");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    AndroidSupportModel report = new AndroidSupportModel();

                    report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                    report.SystemName = dtrow["SystemName"].ToString().Trim();
                    details.Add(report);
                }

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;


        }

        public List<AndroidSupportViewModel> Search(AndroidSupportViewModel objModel)
        {
            DataTable dt = new DataTable();
            List<AndroidSupportViewModel> details = new List<AndroidSupportViewModel>();

            SqlCommand cmd = new SqlCommand("dbo.AndroidSupportSearch");
            cmd.Parameters.AddWithValue("@FromDate", objModel.FormDate);
            cmd.Parameters.AddWithValue("@Todate", objModel.Todate);
            cmd.Parameters.AddWithValue("@SystemID", objModel.SystemID);
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            foreach (DataRow dtrow in dt.Rows)
            {
                AndroidSupportViewModel report = new AndroidSupportViewModel();

                report.AndroidPlayStoreID = Convert.ToInt32(dtrow["AndroidPlayStoreID"].ToString());
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.PlayStoreURL = dtrow["PlayStoreURL"].ToString().Trim();
                report.GoogleConsolePassword = dtrow["GoogleConsolePassword"].ToString().Trim();
                report.GoogleConsoleUserName = dtrow["GoogleConsoleUserName"].ToString().Trim();
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.DOC = dtrow["DOC"].ToString().Trim();

                List<AndroidFileModel> doclist = new List<AndroidFileModel>();
                doclist = SelectAttachment(report.AndroidPlayStoreID);
                foreach (AndroidFileModel doc in doclist)
                {
                    if (doc.DocType.Equals("KeyStoreDoc", StringComparison.InvariantCultureIgnoreCase))
                    {
                        report.KeyStoreFileName = doc.OriginalFileName;
                        report.KeyStoreFilePath = doc.Attachment;

                    }

                    if (doc.DocType.Equals("JKSDoc", StringComparison.InvariantCultureIgnoreCase))
                    {
                        report.JKSFileName = doc.OriginalFileName;
                        report.JKSFileFilePath = doc.Attachment;
                    }

                }
                details.Add(report);
            }

            return details;
        }


        public List<AndroidFileModel> SelectAttachment(int AndroidPlayStoreID)
        {
            DataTable dt = new DataTable();
            List<AndroidFileModel> details = new List<AndroidFileModel>();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterSelectForAndroidData");
            cmd.Parameters.AddWithValue("@AndroidPlayStoreID", AndroidPlayStoreID);
            cmd.Parameters.AddWithValue("@UploadType", "AndroidData");
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AndroidFileModel report = new AndroidFileModel();
                report.Attachment = dtrow["Attachment"].ToString().Trim();
                report.OriginalFileName = dtrow["OriginalFileName"].ToString().Trim();
                report.DocType = dtrow["DocType"].ToString().Trim();

                details.Add(report);
            }

            return details;
        }

        public int Insert(AndroidSupportModel ObjModel)
        {
            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.AndroidSupportInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SystemID", ObjModel.SystemID);
            cmd.Parameters.AddWithValue("@PlayStoreURL", ObjModel.PlayStoreURL);
            cmd.Parameters.AddWithValue("@GoogleConsoleUserName", ObjModel.GoogleConsoleUserName);
            cmd.Parameters.AddWithValue("@GoogleConsolePassword", ObjModel.GoogleConsolePassword);
            cmd.Parameters.AddWithValue("@Description", ObjModel.Description);
            cmd.Parameters.AddWithValue("@CodeRepository", ObjModel.CodeRepository);

            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                result = dp.getOutputParam(cmd, Output);
            }

            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }


        public DataTable FileInsert(AndroidSupportModel objModel)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Attachment", objModel.Attachment);
            cmd.Parameters.AddWithValue("@OriginalFileName", objModel.OriginalFileName);
            cmd.Parameters.AddWithValue("@DocType", objModel.DocType);
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public int FileMappingInsert(AndroidSupportModel objModel)
        {
            int result = 0;

            SqlCommand cmd = new SqlCommand("dbo.FileMappingMasterInsertByAndroidData");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AndroidPlayStoreID", objModel.AndroidPlayStoreID);
            cmd.Parameters.AddWithValue("@FileID", objModel.FileID);
            cmd.Parameters.AddWithValue("@UploadType", objModel.UploadType);
            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }
    }
}
