﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class SendMessageDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();
       
        public int insertMessage(MessageMasterModel objModel)
        {
            int i;
            try
            {
                SqlCommand cmd = new SqlCommand("MessageInsert");
                cmd.Parameters.Add("@Message", SqlDbType.VarChar).Value = objModel.Message;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = objModel.UserID;
                cmd.Parameters.Add("@Usertype", SqlDbType.VarChar).Value = objModel.Usertype;

                i = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                return 0;
            }
            return i;
        }

        public DataTable getMobileNumberfromUserID(int UserID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("UserInformationSelectMobileNumberFromUserID");
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                dt = db.getDataTable(cmd);

            }
            catch (Exception ex)
            {
                return dt;
            }
            return dt;
        }

        
    }
}
