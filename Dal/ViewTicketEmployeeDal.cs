﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ViewTicketEmployeeDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<ViewTicketEmployeeModel> Select(int UserID)
        {
            List<ViewTicketEmployeeModel> details = new List<ViewTicketEmployeeModel>();
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByUserID");
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTicketEmployeeModel report = new ViewTicketEmployeeModel();
                report.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());
                report.TicketNo = dtrow["TicketNo"].ToString();
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.DOC = Convert.ToDateTime(dtrow["TicketGeneratedDate"].ToString().Trim());
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();

                report.TicketStatus = dtrow["TicketStatus"].ToString().Trim();
                report.EmployeeRemark = dtrow["EmployeeRemark"].ToString().Trim();
                report.AdminRemark = dtrow["AdminRemark"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewTicketEmployeeModel> Search(string FromDate, string ToDate, int UserID)
        {
            List<ViewTicketEmployeeModel> details = new List<ViewTicketEmployeeModel>();
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByDateForEmployee");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTicketEmployeeModel report = new ViewTicketEmployeeModel();
                report.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().ToUpper().Trim();
                report.DOC = Convert.ToDateTime(dtrow["TicketGeneratedDate"].ToString().ToUpper().Trim());
                report.ServiceName = dtrow["ServiceName"].ToString().ToUpper().Trim();
                report.Description = dtrow["Description"].ToString().ToUpper().Trim();

                report.TicketStatus = dtrow["TicketStatus"].ToString().ToUpper().Trim();
                report.EmployeeRemark = dtrow["EmployeeRemark"].ToString().ToUpper().Trim();
                report.AdminRemark = dtrow["AdminRemark"].ToString().ToUpper().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewTicketEmployeeModel> SelectByTicketID(int TicketID, int UserID)
        {
            DataTable dt = new DataTable();
            List<ViewTicketEmployeeModel> details = new List<ViewTicketEmployeeModel>();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByTicketIDForEmployee");
            cmd.Parameters.AddWithValue("@TicketID", TicketID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTicketEmployeeModel report = new ViewTicketEmployeeModel();
                report.TicketID = Convert.ToInt32(dtrow["TicketID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.DOC = Convert.ToDateTime(dtrow["TicketGeneratedDate"].ToString().Trim());
                report.ServiceName = dtrow["ServiceName"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();

                report.TicketStatus = dtrow["TicketStatus"].ToString().Trim();
                report.EmployeeRemark = dtrow["EmployeeRemark"].ToString().Trim();
                report.AdminRemark = dtrow["AdminRemark"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<ViewTicketEmployeeModel> SelectAttachment(ViewTicketEmployeeModel ObjModel)
        {
            DataTable dt = new DataTable();
            List<ViewTicketEmployeeModel> details = new List<ViewTicketEmployeeModel>();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterSelectAttachmentForEmployee");
            cmd.Parameters.AddWithValue("@TicketID", ObjModel.TicketID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewTicketEmployeeModel report = new ViewTicketEmployeeModel();
                report.FileID = Convert.ToInt32(dtrow["FileID"].ToString());
                report.Attachment = dtrow["Attachment"].ToString().Trim();
                report.OriginalFileName = dtrow["OriginalFileName"].ToString().Trim();

                details.Add(report);
            }

            return details;
        }
    }
}