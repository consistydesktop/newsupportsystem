﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class NewMessageDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<NewMessageModel> SelectName(string Usertype)
        {
            DataTable dt = new DataTable();
            List<NewMessageModel> details = new List<NewMessageModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByUsertype");
                cmd.Parameters.AddWithValue("@Usertype", Usertype);
                dt = db.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    NewMessageModel report = new NewMessageModel();
                    report.UserTypeID = Convert.ToInt32(dtrow["UserTypeID"].ToString());
                    report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                    report.Name = dtrow["Name"].ToString().Trim();
                    report.UserType = dtrow["UserType"].ToString().Trim();
                    report.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }

        public int Insert(NewMessageModel objModel)
        {
            int Result = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.MessageMasterInsert");

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Message", objModel.Message);
                cmd.Parameters.AddWithValue("@UserID", objModel.UserID);
                cmd.Parameters.AddWithValue("@Usertype", objModel.Usertype);
                Result = db.executeStoredProcedure(cmd);
            }
            catch
            {
                throw;
            }

            return Result;
        }

        public List<NewMessageModel> Select()
        {
            List<NewMessageModel> details = new List<NewMessageModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.MessageMasterSelect");
                DataTable dt = db.getDataTable(cmd);

                foreach (DataRow dtrow in dt.Rows)
                {
                    NewMessageModel report = new NewMessageModel();

                    report.MessageID = Convert.ToInt32(dtrow["MessageID"].ToString());
                    report.Usertype = dtrow["Usertype"].ToString().Trim();
                    report.Message = dtrow["Message"].ToString().Trim();
                    report.DOC = dtrow["DOC"].ToString().Trim();
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }
    }
}