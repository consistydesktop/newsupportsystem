﻿using DAL;
using Model;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class UserAccessDAL
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public bool SelectValidAccess(UserAccessModel objModel)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.UserAccessControlSelectValidAccess");
                cmd.Parameters.Add("@Pagename", SqlDbType.VarChar).Value = objModel.Pagename;
                cmd.Parameters.Add("@UserTypeID", SqlDbType.VarChar).Value = objModel.UserTypeID;
                dt = db.getDataTable(cmd);

                if (dt.Rows.Count <= 0)
                    return false;

                if (dt.Rows[0]["isActive"].ToString() == "0" || dt.Rows.Count == 0)
                    return false;
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return true;
        }
    }
}