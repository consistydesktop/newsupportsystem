﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class DashboardDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<DashboardModel> Select()
        {
            DataTable dt = new DataTable();
            List<DashboardModel> details = new List<DashboardModel>();

            SqlCommand cmd = new SqlCommand("TicketInformationSelectPendingAndResolvedTickets");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                DashboardModel report = new DashboardModel();
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.PendingTickets = dtrow["Pending"].ToString().Trim();
                report.ResolvedTickets = dtrow["Resolved"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public DataTable SelectLastSubscriptionDetails(int SystemID, int UserID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "dbo.SelectLastSubscriptionDetails";
                cmd.Parameters.AddWithValue("@SystemID", SystemID);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                dt = db.getDataTable(cmd);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public DataTable SelectTaskCount(int UserID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.TaskMasterSelectCountForAdmin");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {

                new Logger().write(ex);
            }
            return dt;
        }
        public DataTable SelectUnpaidSubscription()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.SelectUnpaidSubscription");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }
        public DataTable SelectpaidSubscriptionDetails()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.SelectpaidSubscriptionDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public DataTable SelectTicketCounts(int UserID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectCountForAdmin");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                dt = db.getDataTable(cmd);
            }

            catch (Exception ex)
            {

                new Logger().write(ex);
            }
            return dt;
        }
    }

}