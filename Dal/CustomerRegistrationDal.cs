﻿using DAL;
using Model;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class CustomerRegistrationDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public int Insert(CustomerRegistrationModel objModel)
        {
            int result = 0;

            SqlCommand cmd = new SqlCommand("dbo.CustomerRegistrationInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Name", objModel.Name);
            cmd.Parameters.AddWithValue("@Username", objModel.Username);
            cmd.Parameters.AddWithValue("@EmailID", objModel.EmailID);
            cmd.Parameters.AddWithValue("@UserTypeID", objModel.UserTypeID);
            cmd.Parameters.AddWithValue("@MobileNumber", objModel.MobileNumber);
            cmd.Parameters.AddWithValue("@PANNo", objModel.PANNo);
            cmd.Parameters.AddWithValue("@GSTNo", objModel.GSTNo);
            cmd.Parameters.AddWithValue("@State", objModel.State);
            cmd.Parameters.AddWithValue("@Address", objModel.Address);


            try
            {
                result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public CustomerRegistrationModel SelectByID(int UserID)
        {
            DataTable dt = new DataTable();
            CustomerRegistrationModel Register = new CustomerRegistrationModel();

            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectClientByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            if (dt.Rows.Count <= 0)
                return Register;

            Register.UserID = Convert.ToInt32(dt.Rows[0]["UserID"].ToString().Trim());
            Register.Name = dt.Rows[0]["Name"].ToString().Trim();
            Register.Username = dt.Rows[0]["Username"].ToString().Trim();
            Register.EmailID = dt.Rows[0]["EmailID"].ToString().Trim();
            Register.MobileNumber = dt.Rows[0]["MobileNumber"].ToString().Trim();
            Register.GSTNo = dt.Rows[0]["GSTNo"].ToString().Trim();
            Register.PANNo = dt.Rows[0]["PANNo"].ToString().Trim();
            Register.Address = dt.Rows[0]["Address"].ToString().Trim();
            Register.State = dt.Rows[0]["State"].ToString().Trim();
            Register.IsActive = Convert.ToInt32(dt.Rows[0]["IsActive"]);

            return Register;
        }

        public int Update(CustomerRegistrationModel ObjModel)
        {
            int Result = -1;

            SqlCommand cmd = new SqlCommand("dbo.UserInformationUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", ObjModel.UserID);
            cmd.Parameters.AddWithValue("@Name", ObjModel.Name);
            cmd.Parameters.AddWithValue("@Username", ObjModel.Username);
            cmd.Parameters.AddWithValue("@EmailID", ObjModel.EmailID);
            cmd.Parameters.AddWithValue("@MobileNumber", ObjModel.MobileNumber);
            cmd.Parameters.AddWithValue("@PANNo", ObjModel.PANNo);
            cmd.Parameters.AddWithValue("@GSTNo", ObjModel.GSTNo);
            cmd.Parameters.AddWithValue("@State", ObjModel.State);
           
            
   
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public CustomerRegistrationModel SelectByRegistrationID(int RegistrationID)
        {
            DataTable dt = new DataTable();
            CustomerRegistrationModel Register = new CustomerRegistrationModel();

            SqlCommand cmd = new SqlCommand("dbo.CustomerRegistrationSelectByID");
            cmd.Parameters.Add("@RegistrationID", SqlDbType.Int).Value = RegistrationID;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                Register.RegistrationID = Convert.ToInt32(dtrow["RegistrationID"].ToString().Trim());
                Register.Name = dtrow["CustomerName"].ToString().Trim();
                Register.Username = dtrow["Username"].ToString().Trim();
                Register.EmailID = dtrow["EmailID"].ToString().Trim();
                Register.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
                Register.GSTNo = dtrow["GSTNo"].ToString().Trim();
                Register.PANNo = dtrow["PANNo"].ToString().Trim();
                Register.Address = dtrow["Address"].ToString().Trim();
                Register.State = dtrow["State"].ToString().Trim();
            }

            return Register;
        }
    }
}