﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{

    public class AccountDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();
        public List<TransactionMasterModel> SelectUser()
        {
            DataTable dt = new DataTable();
            List<TransactionMasterModel> details = new List<TransactionMasterModel>();
            SqlCommand cmd = new SqlCommand("dbo.TransactionMasterSelectUserName");
            try
            {

                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            foreach (DataRow dtrow in dt.Rows)
            {
                TransactionMasterModel report = new TransactionMasterModel();
                report.Name = dtrow["Name"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }
        public int InsertTransactionMaster(TransactionMasterModel ObjModel)
        {
            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.TransactionMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Date", ObjModel.Date);
            cmd.Parameters.AddWithValue("@Name", ObjModel.Name);
            cmd.Parameters.AddWithValue("@ReferenceNo", ObjModel.ReferenceNo);
            cmd.Parameters.AddWithValue("@Amount", ObjModel.Amount);
            cmd.Parameters.AddWithValue("@TransactionType", ObjModel.TransactionType);
            cmd.Parameters.AddWithValue("@PaymentType", ObjModel.PaymentType);
            cmd.Parameters.AddWithValue("@Remark", ObjModel.Remark);
            cmd.Parameters.AddWithValue("@BankMasterID", ObjModel.BankMasterID);
            cmd.Parameters.AddWithValue("@IsGST", ObjModel.IsGST);

            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                result = dp.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }
        
        public List<TransactionMasterModel> SelectByDate(string FromDate, string ToDate, string PaymentType, string TransactionType, int BankMasterID, int IsGST)
        {
            DataTable dt = new DataTable();
            List<TransactionMasterModel> details = new List<TransactionMasterModel>();
            SqlCommand cmd = new SqlCommand("dbo.TransactionMasterSelectByDate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@PaymentType", PaymentType);
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@BankMasterID", BankMasterID);
            cmd.Parameters.AddWithValue("@IsGST", IsGST);

            try
            {
                dt = dp.getDataTable(cmd);




                foreach (DataRow dtrow in dt.Rows)
                {
                    TransactionMasterModel report = new TransactionMasterModel();
                    report.Date = dtrow["Date"].ToString().Trim();
                    report.Name = dtrow["Name"].ToString().Trim();
                    report.ReferenceNo = dtrow["ReferenceNo"].ToString().Trim();
                    report.Amount = Convert.ToDecimal(dtrow["Amount"]);
                    report.TransactionType = dtrow["TransactionType"].ToString().Trim();
                    report.PaymentType = dtrow["PaymentType"].ToString().Trim();
                    report.Remark = dtrow["Remark"].ToString().Trim();
                    report.TransactionMasterID = Convert.ToInt32(dtrow["TransactionMasterID"]);
                    report.BankName = dtrow["BankName"].ToString().Trim();
                    report.IsGST = Convert.ToInt32(dtrow["IsGST"]);


                    details.Add(report);

                }


            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;

        }

        public List<TransactionMasterModel> Select(string FromDate, string ToDate)
        {
            DataTable dt = new DataTable();
            List<TransactionMasterModel> details = new List<TransactionMasterModel>();
            SqlCommand cmd = new SqlCommand("dbo.TransactionMasterSelect");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            try
            {
                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    TransactionMasterModel report = new TransactionMasterModel();
                    report.Date = dtrow["Date"].ToString().Trim();
                    report.Name = dtrow["Name"].ToString().Trim();
                    report.ReferenceNo = dtrow["ReferenceNo"].ToString().Trim();
                    report.Amount = Convert.ToDecimal(dtrow["Amount"]);
                    report.TransactionType = dtrow["TransactionType"].ToString().Trim();
                    report.PaymentType = dtrow["PaymentType"].ToString().Trim();
                    report.Remark = dtrow["Remark"].ToString().Trim();
                    report.TransactionMasterID = Convert.ToInt32(dtrow["TransactionMasterID"]);
                    report.BankName = dtrow["BankName"].ToString().Trim();


                    details.Add(report);

                }

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;

        }

        public List<List<TransactionMasterModel>> SelectByID(int TransactionMasterID)
        {
            List<List<TransactionMasterModel>> AllTransactionMasterDetails = new List<List<TransactionMasterModel>>();

            List<TransactionMasterModel> TransactionMasterDetails = new List<TransactionMasterModel>();


            TransactionMasterDetails = TransactionMasterSelectByID(TransactionMasterID);

            AllTransactionMasterDetails.Add(TransactionMasterDetails);


            return AllTransactionMasterDetails;
        }

        private List<TransactionMasterModel> TransactionMasterSelectByID(int TransactionMasterID)
        {
            DataTable dt = new DataTable();
            List<TransactionMasterModel> TransactionMasterDetails = new List<TransactionMasterModel>();

            SqlCommand cmd = new SqlCommand("dbo.TransactionMasterSelectByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionMasterID", TransactionMasterID);

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                TransactionMasterModel report = new TransactionMasterModel();
                report.TransactionMasterID = Convert.ToInt32(dtrow["TransactionMasterID"].ToString().Trim());
                report.Name = dtrow["Name"].ToString().Trim();
                report.ReferenceNo = dtrow["ReferenceNo"].ToString().Trim();
                report.Amount = Convert.ToDecimal(dtrow["Amount"]);
                report.TransactionType = dtrow["TransactionType"].ToString().Trim();

                report.PaymentType = dtrow["PaymentType"].ToString().Trim();
                report.Remark = dtrow["Remark"].ToString().Trim();
                report.BankMasterID = Convert.ToInt32(dtrow["BankMasterID"].ToString().Trim());



                TransactionMasterDetails.Add(report);
            }

            return TransactionMasterDetails;
        }

        public int UpdateByID(TransactionMasterModel ObjModel)
        {
            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.TransactionMasterUpdateByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionMasterID", ObjModel.TransactionMasterID);
            cmd.Parameters.AddWithValue("@Name", ObjModel.Name);
            cmd.Parameters.AddWithValue("@ReferenceNo", ObjModel.ReferenceNo);
            cmd.Parameters.AddWithValue("@Amount", ObjModel.Amount);
            cmd.Parameters.AddWithValue("@TransactionType", ObjModel.TransactionType);
            cmd.Parameters.AddWithValue("@PaymentType", ObjModel.PaymentType);
            cmd.Parameters.AddWithValue("@Remark", ObjModel.Remark);
            cmd.Parameters.AddWithValue("@BankMasterID", ObjModel.BankMasterID);
           
            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }
    }
}

