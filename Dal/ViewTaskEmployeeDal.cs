﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ViewTaskEmployeeDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<AssignTaskModel> Select(int UserID)
        {
            DataTable dt = new DataTable();
            List<AssignTaskModel> details = new List<AssignTaskModel>();
            SqlCommand cmd = new SqlCommand("dbo.TaskMasterSelectForEmployee");
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignTaskModel report = new AssignTaskModel();
                report.TaskID = Convert.ToInt32(dtrow["TaskID"].ToString());
                report.Task = dtrow["Task"].ToString().Trim();
                report.Deadline = dtrow["Deadline"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.TaskStatus = (dtrow["Status"].ToString().Trim());
                report.SystemName = (dtrow["SystemName"].ToString().Trim());
                details.Add(report);
            }

            return details;
        }

        public List<AssignTaskModel> Search(string FromDate, string ToDate)
        {
            DataTable dt = new DataTable();
            List<AssignTaskModel> details = new List<AssignTaskModel>();
            SqlCommand cmd = new SqlCommand("dbo.TaskMasterSelectByDate");
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignTaskModel report = new AssignTaskModel();
                report.TaskID = Convert.ToInt32(dtrow["TaskID"].ToString());
                report.Task = dtrow["Task"].ToString().Trim();
                report.Deadline = dtrow["Deadline"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.TaskStatus = (dtrow["Status"].ToString().Trim());
                report.SystemName = (dtrow["SystemName"].ToString().Trim());
                details.Add(report);
            }

            return details;
        }
    }
}