﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class KnowledgeBaseReportDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public List<KnowledgeBaseModel> SelectByDate(string FromDate, string ToDate)
        {
            DataTable dt = new DataTable();
            List<KnowledgeBaseModel> details = new List<KnowledgeBaseModel>();
            SqlCommand cmd = new SqlCommand("dbo.KnowledgeBaseMasterSelectByDate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            try
            {
                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    KnowledgeBaseModel report = new KnowledgeBaseModel();
                    report.Issue = dtrow["Issue"].ToString().Trim();
                    report.Resolution = dtrow["Resolution"].ToString().Trim();
                    report.SystemName = dtrow["SystemName"].ToString().Trim();
                    report.Name = dtrow["Name"].ToString().Trim();
                    report.KnowledgeBaseID = Convert.ToInt32(dtrow["KnowledgeBaseID"].ToString());
                    
                    details.Add(report);

                }
              

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;

        }

        
    }
}
