﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class ViewTicketClientDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();


        public System.Data.DataTable Select(int UserID)
        {

            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByUserIDForClient");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }


            return dt;
        }

        public DataTable SelectByDate(int UserID, string FromDate, string ToDate)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectByDateForClient");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }


            return dt;
        }
    }
}
