﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class DashboardForCustomerDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();



        public int SelectTicketCount(int UserID)
        {
            int TicketCount = -1;
            SqlCommand cmd = new SqlCommand("dbo.TicketInformationSelectCountByCustomerID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                DataTable dt = new DataTable();
                dt = db.getDataTable(cmd);

                if (dt.Rows.Count <= 0)
                    return TicketCount;

                TicketCount = Convert.ToInt32(dt.Rows[0]["ToCount"]);

            }

            catch (Exception ex)
            {
                new Logger().write(ex);

            }
            return TicketCount;
        }
    }
}