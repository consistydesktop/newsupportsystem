﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class BankMasterDal
    {

        private DatabaseProcessing dp = new DatabaseProcessing();
        public int InsertBankMaster(BankMasterModel ObjModel)
        {
            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.BankMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BankName", ObjModel.BankName);
            cmd.Parameters.AddWithValue("@AccountNumber", ObjModel.AccountNumber);
            cmd.Parameters.AddWithValue("@BranchName", ObjModel.BranchName);
            cmd.Parameters.AddWithValue("@IFSCCode", ObjModel.IFSCCode);
            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }
        public List<BankMasterModel> SelectBankMaster()
        {
            DataTable dt = new DataTable();
            List<BankMasterModel> details = new List<BankMasterModel>();
            SqlCommand cmd = new SqlCommand("dbo.BankMasterSelectByName");
            try
            {

                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            foreach (DataRow dtrow in dt.Rows)
            {
                BankMasterModel report = new BankMasterModel();
                report.BankName = dtrow["BankName"].ToString().Trim();
                report.AccountNumber = dtrow["AccountNumber"].ToString().Trim();
                report.BranchName = dtrow["BranchName"].ToString().Trim();
                report.IFSCCode = dtrow["IFSCCode"].ToString().Trim();
                report.BankMasterID = Convert.ToInt32(dtrow["BankMasterID"]);
                details.Add(report);
            }

            return details;
        }
    }
}

