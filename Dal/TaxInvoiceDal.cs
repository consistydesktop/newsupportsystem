﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
  public  class TaxInvoiceDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();
        public DataTable SelectSearch(InvoiceModel request)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "dbo.PaymentGatewaySelect";
                cmd.Parameters.AddWithValue("@FromDate", request.FromDate);
                cmd.Parameters.AddWithValue("@ToDate", request.ToDate);
                cmd.Parameters.AddWithValue("@UserID", request.UserID);
                cmd.Parameters.AddWithValue("@SystemID", request.SystemID);
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }
        public DataTable SelectInvoiceReportForPrint(InvoiceModel request)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "dbo.PaymentGatewaySelectInvoiceReport";
                cmd.Parameters.AddWithValue("@FromDate", request.FromDate);
                cmd.Parameters.AddWithValue("@ToDate", request.ToDate);
                cmd.Parameters.AddWithValue("@UserID", request.UserID);
                cmd.Parameters.AddWithValue("@SystemID", request.SystemID);
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }
        
    }
}
