﻿using DAL;
using Model;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ResetPasswordDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public int Update(ResetPasswordModel ObjModel)
        {
            int Result = -1;
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.UserInformationUpdatePassword");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserID", ObjModel.UserID);
                cmd.Parameters.AddWithValue("@NewPassword", ObjModel.NewPassword);
                cmd.Parameters.AddWithValue("@OldPassword", ObjModel.OldPassword);

                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }
    }
}