﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class SystemNameDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<SystemNameModel> SelectCustomerName()
        {
            DataTable dt = new DataTable();
            List<SystemNameModel> details = new List<SystemNameModel>();

            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByCustomerID");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                SystemNameModel report = new SystemNameModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Insert(SystemNameModel objModel)
        {
            int Result = 0;
            SqlCommand cmd = new SqlCommand("dbo.SystemMasterInsertForApprovedCustomer");

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SystemName", objModel.SystemName);
            cmd.Parameters.AddWithValue("@UserID", objModel.UserID);
            cmd.Parameters.AddWithValue("@ServiceID", objModel.ServiceID);
            cmd.Parameters.AddWithValue("@ProductID", objModel.ProductID);
            cmd.Parameters.AddWithValue("@URL", objModel.URL);

            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                Result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return Result;
        }

        public List<SystemNameModel> Select()
        {
            List<SystemNameModel> details = new List<SystemNameModel>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                SystemNameModel report = new SystemNameModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.Name = dtrow["Name"].ToString();
                report.Username = dtrow["Username"].ToString().Trim();
                report.EmailID = dtrow["EmailID"].ToString().Trim();
                report.MobileNumber = dtrow["MobileNumber"].ToString().Trim();
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.ProductName = dtrow["ProductName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<SystemNameModel> SelectService()
        {
            List<SystemNameModel> details = new List<SystemNameModel>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.ServiceMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                SystemNameModel report = new SystemNameModel();
                report.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString());
                report.ServiceName = dtrow["ServiceName"].ToString();
                details.Add(report);
            }

            return details;
        }

        public List<SystemNameModel> SelectProduct()
        {
            List<SystemNameModel> details = new List<SystemNameModel>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.ProductMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                SystemNameModel report = new SystemNameModel();
                report.ProductID = Convert.ToInt32(dtrow["ProductID"].ToString());

                report.ProductName = dtrow["ProductName"].ToString();
                details.Add(report);
            }

            return details;
        }

        public SystemNameModel SelectByID(int SystemID)
        {
            DataTable dt = new DataTable();
            SystemNameModel Register = new SystemNameModel();

            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectByID");
            cmd.Parameters.Add("@SystemID", SqlDbType.Int).Value = SystemID;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                Register.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                Register.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                Register.Name = dtrow["Name"].ToString().Trim();
                Register.SystemName = dtrow["SystemName"].ToString().Trim();
                Register.ServiceID = Convert.ToInt32(dtrow["ServiceID"].ToString().Trim());
                Register.ProductID = Convert.ToInt32(dtrow["ProductID"].ToString().Trim());
                Register.URL=dtrow["URL"].ToString().Trim();

            }

            return Register;
        }

        public int Update(SystemNameModel ObjModel)
        {
            int Result = -1;

            SqlCommand cmd = new SqlCommand("dbo.SystemMasterUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SystemID", ObjModel.SystemID);
            cmd.Parameters.AddWithValue("@UserID", ObjModel.UserID);
            cmd.Parameters.AddWithValue("@SystemName", ObjModel.SystemName);
            cmd.Parameters.AddWithValue("@ServiceID", ObjModel.ServiceID);
            cmd.Parameters.AddWithValue("@ProductID", ObjModel.ProductID);
            cmd.Parameters.AddWithValue("@URL", ObjModel.URL);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int Delete(int SystemID)
        {
            int Result = -1;

            SqlCommand cmd = new SqlCommand("dbo.SystemMasterDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SystemID", SystemID);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }
    }
}