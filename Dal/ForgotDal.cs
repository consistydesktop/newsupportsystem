﻿using DAL;
using Model;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ForgotDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public DataTable ForgotPassword(ForgotModel objModel)
        {
            ForgotModel ObjModel = new ForgotModel();

            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectForForgotPassword");
                cmd.Parameters.Add("@EmailID", SqlDbType.VarChar).Value = objModel.EmailID;
                cmd.Parameters.Add("@MobileNumber", SqlDbType.VarChar).Value = objModel.MobileNumber;
                cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = objModel.Password;

                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return dt;
        }
    }
}