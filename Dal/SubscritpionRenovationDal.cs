﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class SubscritpionRenovationDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();


        public List<Model.AddWebSubscriptionModel> SelectSystemName(int UserID)
        {
            DataTable dt = new DataTable();
            List<AddWebSubscriptionModel> details = new List<AddWebSubscriptionModel>();

            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectSystemNameByUserID");
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddWebSubscriptionModel report = new AddWebSubscriptionModel();
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public DataTable SelectBySystemID(int SystemID, int UserID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "dbo.SelectWebURLBySystemID";
                cmd.Parameters.AddWithValue("@SystemID", SystemID);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                dt = db.getDataTable(cmd);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public DataTable CardTypeSelect()
        {
            DataTable dt = new DataTable();


            SqlCommand cmd = new SqlCommand("dbo.CardTypeMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            //foreach (DataRow dtrow in dt.Rows)
            //{
            //    CardTypeViewModel report = new CardTypeViewModel();
            //    report.CardTypeID = Convert.ToInt32(dtrow["CardTypeID"].ToString());
            //    report.CardType = dtrow["CardType"].ToString().Trim();
            //    details.Add(report);
            //}

            return dt;
        }

        public DataTable SelectCallBackURL(int UserID, int ProductID, int SystemID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "dbo.SelectCallBackURL";
                cmd.Parameters.AddWithValue("@ProductID", ProductID);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                cmd.Parameters.AddWithValue("@SystemID", SystemID);
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public DataTable SelectCost(string daysType, int ProductID)
        {
            DataTable dt = new DataTable();
            AddSubscriptionScreenModel report = new AddSubscriptionScreenModel();
            SqlCommand cmd = new SqlCommand("dbo.ProductCostingSelectCost");
            cmd.Parameters.Add("@DaysMonthType", SqlDbType.VarChar).Value = daysType;
            //cmd.Parameters.Add("@DaysMonth", SqlDbType.Int).Value = Convert.ToInt32(Value);
            cmd.Parameters.Add("@ProductID", SqlDbType.Int).Value = ProductID;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }



            return dt;
        }

        public DataTable SelectSearch(InvoiceModel request)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "dbo.PaymentGatewaySelectPurchase";
                cmd.Parameters.AddWithValue("@FromDate", request.FromDate);
                cmd.Parameters.AddWithValue("@ToDate", request.ToDate);
                cmd.Parameters.AddWithValue("@UserID", request.UserID);
                cmd.Parameters.AddWithValue("@SystemID", request.SystemID);
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public DataTable SelectSlab(string PayMode)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.FinnovativeCostingSelectSlab");
            cmd.Parameters.Add("@PayMode", SqlDbType.VarChar).Value = PayMode;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return dt;
        }

        public int SelectSystemID(string WebURL)
        {
            int SystemID = 0;
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "dbo.SelectSystemID";
                cmd.Parameters.AddWithValue("@WebURL", WebURL);
                dt = db.getDataTable(cmd);
                if (dt.Rows.Count <= 0)
                    return SystemID;

                SystemID = Convert.ToInt32(dt.Rows[0]["SystemID"].ToString());
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return SystemID;
        }

        public int ExtendSubscription(int ProductID, string DaysMonthType, int userID, int SystemID)
        {
            int Result = -1;
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.ExtendSubscription");
                cmd.Parameters.AddWithValue("@UserID", userID);
                cmd.Parameters.AddWithValue("@ProductID", ProductID);
                cmd.Parameters.AddWithValue("@DaysMonthType", DaysMonthType);
                cmd.Parameters.AddWithValue("@SystemID", SystemID);
                Result = Convert.ToInt32(db.executeStoredProcedure(cmd));
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public DataTable SelectUser(int userID)
        {

            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectClientByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", userID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public int Insert(PGModel objPGModel)
        {
            int Result = -1;
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.PaymentGatewayInsert");
                cmd.Parameters.AddWithValue("@UserID", objPGModel.UserID);
                cmd.Parameters.AddWithValue("@OrderID", objPGModel.OrderId);
                cmd.Parameters.AddWithValue("@Amount", objPGModel.Amount);
                cmd.Parameters.AddWithValue("@CommAmount", objPGModel.CommisionAmount);

                cmd.Parameters.AddWithValue("@GatewayID", objPGModel.PaymentGatewayID);
                cmd.Parameters.AddWithValue("@Status", objPGModel.status);
                cmd.Parameters.AddWithValue("@Response", objPGModel.response);
                cmd.Parameters.AddWithValue("@IPAddress", objPGModel.IPAddress);
                cmd.Parameters.AddWithValue("@SystemID", objPGModel.SystemID);
                cmd.Parameters.AddWithValue("@DaysMonthType", objPGModel.DaysMonthType);

                cmd.Parameters.AddWithValue("@ProductID", Convert.ToInt32(objPGModel.ProductID));

                Result = Convert.ToInt32(db.executeStoredProcedure(cmd));
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }
        public int Update(PGModel objPGModel)
        {
            int Result = -1;
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.PaymentGatewayUpdateByOrderID");

                cmd.Parameters.AddWithValue("@OrderID", objPGModel.OrderId);
                cmd.Parameters.AddWithValue("@CommAmount", objPGModel.CommisionAmount);

                cmd.Parameters.AddWithValue("@GatewayID", objPGModel.PaymentGatewayID);
                cmd.Parameters.AddWithValue("@Status", objPGModel.status);
                cmd.Parameters.AddWithValue("@PayMode", objPGModel.PayMode);
                cmd.Parameters.AddWithValue("@TransactionID", objPGModel.TransactionID);
                cmd.Parameters.AddWithValue("@Response", objPGModel.response);

                SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
                Output.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(Output);

                Result = db.getOutputParam(cmd, Output);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }
        public DataTable Select(string OrderId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "dbo.PaymentGatewaySelectByOrderID";
                cmd.Parameters.AddWithValue("@OrderID", OrderId);
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }


        public DataTable PGReportDateWiseSelectDal(PGModel objPGModel)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "dbo.PaymentGatewaySelectDatewiseReport";
                cmd.Parameters.AddWithValue("@UserID", objPGModel.UserID);
                cmd.Parameters.AddWithValue("@FromDate", objPGModel.Date);
                cmd.Parameters.AddWithValue("@ToDate", objPGModel.ToDate);
                cmd.Parameters.AddWithValue("@Status", objPGModel.status);

                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }
        public bool CheckDuplicateOrderID(string orderId)
        {

            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "dbo.CheckDuplicateOrderID";
                cmd.Parameters.AddWithValue("@orderId", orderId);
                dt = db.getDataTable(cmd);

                if (dt.Rows.Count <= 0)
                    return true;

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return false;
        }
    }
}
