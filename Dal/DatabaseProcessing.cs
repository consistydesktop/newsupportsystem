﻿using Model;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DatabaseProcessing
    {
        private string Constr = ConnectionClass.connect();

        #region getDataTable

        public DataTable getDataTable(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    SqlDataAdapter da = new SqlDataAdapter();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    da.SelectCommand = cmd;

                    con.Open();
                    da.Fill(dt);
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        #endregion getDataTable

        #region getDataSet

        public DataSet getDataSet(SqlCommand cmd)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    SqlDataAdapter da = new SqlDataAdapter();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    da.SelectCommand = cmd;
                    con.Open();
                    da.Fill(ds);
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return ds;
        }

        #endregion getDataSet

        #region getOutputParam

        public int getOutputParam(SqlCommand cmd, SqlParameter output)
        {
            int returnValue = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    con.Open();

                    cmd.ExecuteNonQuery();
                    returnValue = Convert.ToInt32(output.Value);

                    con.Close();
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return returnValue;
        }

        #endregion getOutputParam

        #region executeStoredProcedure

        public int executeStoredProcedure(SqlCommand cmd)
        {
            int returnValue = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    con.Open();
                    returnValue = cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return returnValue;
        }

        #endregion executeStoredProcedure

        #region executeStoredProcedureANdFill

        public DataTable executeStoredProcedureANdFill(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    SqlDataAdapter da = new SqlDataAdapter();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    da.SelectCommand = cmd;
                    con.Open();
                    da.Fill(dt);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        #endregion executeStoredProcedureANdFill

        public string getOutputParamString(SqlCommand cmd, SqlParameter output)
        {
            string returnValue = string.Empty; ;
            try
            {
                using (SqlConnection con = new SqlConnection(Constr))
                {
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    con.Open();

                    cmd.ExecuteNonQuery();
                    returnValue = (output.Value).ToString();

                    con.Close();
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return returnValue;
        }
    }
}