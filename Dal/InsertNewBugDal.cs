﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class InsertNewBugDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<InsertNewBugModel> SelectSystemName()
        {
            DataTable dt = new DataTable();
            List<InsertNewBugModel> details = new List<InsertNewBugModel>();

            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectSystemName");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                InsertNewBugModel report = new InsertNewBugModel();

                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<List<InsertNewBugModel>> Search(string fromDate, string toDate, int systemID)
        {
            throw new NotImplementedException();
        }

        public List<InsertNewBugModel> SelectEmployeeName()
        {
            DataTable dt = new DataTable();
            List<InsertNewBugModel> details = new List<InsertNewBugModel>();

            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByEmployeeID");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                InsertNewBugModel report = new InsertNewBugModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<InsertNewBugModel> SelectIssue()
        {
            DataTable dt = new DataTable();
            List<InsertNewBugModel> details = new List<InsertNewBugModel>();

            SqlCommand cmd = new SqlCommand("dbo.TestingIssueMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                InsertNewBugModel report = new InsertNewBugModel();
                report.TestingIssueID = (dtrow["TestingIssueID"].ToString());
                report.IssueName = dtrow["IssueName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Insert(InsertNewBugModel objModel)
        {
            int Result = 0;

            SqlCommand cmd = new SqlCommand("dbo.BugMasterInsert");

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ProjectBugMasterID", objModel.ProjectBugMasterID);

            cmd.Parameters.AddWithValue("@BugTitle", objModel.BugTitle);
            cmd.Parameters.AddWithValue("@PageURL", objModel.PageURL);
            cmd.Parameters.AddWithValue("@TestingIssueID", objModel.TestingIssueID);
            cmd.Parameters.AddWithValue("@Priority", objModel.Priority);
            cmd.Parameters.AddWithValue("@Description", objModel.Description);
            cmd.Parameters.AddWithValue("@ExpectedResult", objModel.ExpectedResult);
            cmd.Parameters.AddWithValue("@Status", objModel.Status.ToUpper());
            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                Result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return Result;
        }

        public List<InsertNewBugModel> Select(int ProjectBugMasterID, string FromDate, string ToDate, string status, string searchtype)
        {
            List<InsertNewBugModel> details = new List<InsertNewBugModel>();
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.BugMasterSelectTest");
            cmd.Parameters.AddWithValue("@ProjectBugMasterID", ProjectBugMasterID);
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@searchtype", searchtype);



            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                InsertNewBugModel report = new InsertNewBugModel();
                report.BugID = Convert.ToInt32(dtrow["BugID"].ToString());
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().Trim());
                report.BugTitle = dtrow["BugTitle"].ToString().Trim();
                report.Priority = dtrow["Priority"].ToString().Trim();
                report.TestingIssueID = (dtrow["TestingIssueID"].ToString());

                report.Description = dtrow["Description"].ToString().Trim();
                report.Developer = dtrow["Developer"].ToString().Trim();
                report.ExpectedResult = dtrow["ExpectedResult"].ToString().Trim();
                report.Status = dtrow["Status"].ToString().Trim();
                report.UpdateDOC = dtrow["UpdateDOC"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public InsertNewBugModel SelectByID(int BugID)
        {
            DataTable dt = new DataTable();
            InsertNewBugModel report = new InsertNewBugModel();

            SqlCommand cmd = new SqlCommand("dbo.BugMasterSelectByBugID");
            cmd.Parameters.Add("@BugID", SqlDbType.Int).Value = BugID;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                report.BugID = Convert.ToInt32(dtrow["BugID"].ToString());
                report.ProjectBugMasterID = Convert.ToInt32(dtrow["ProjectBugMasterID"].ToString());
                report.TestingIssueID = (dtrow["TestingIssueID"].ToString());
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().Trim());
                report.BugTitle = dtrow["BugTitle"].ToString().Trim();
                report.PageURL = dtrow["PageURL"].ToString().Trim();
                report.Priority = dtrow["Priority"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.ExpectedResult = dtrow["ExpectedResult"].ToString().Trim();
                report.Comment = dtrow["Comment"].ToString().Trim();
                report.Status = dtrow["Status"].ToString().Trim();
            }

            return report;
        }

        public int Update(InsertNewBugModel ObjModel)
        {
            int Result = -1;

            SqlCommand cmd = new SqlCommand("dbo.BugMasterUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BugID", ObjModel.BugID);
            cmd.Parameters.AddWithValue("@SystemID", ObjModel.SystemID);
            cmd.Parameters.AddWithValue("@DeveloperID", ObjModel.DeveloperID);
            cmd.Parameters.AddWithValue("@TestingIssueID", ObjModel.TestingIssueID);
            cmd.Parameters.AddWithValue("@BugTitle", ObjModel.BugTitle);
            cmd.Parameters.AddWithValue("@PageURL", ObjModel.PageURL);
            cmd.Parameters.AddWithValue("@Priority", ObjModel.Priority);
            cmd.Parameters.AddWithValue("@Description", ObjModel.Description);
            cmd.Parameters.AddWithValue("@ExpectedResult", ObjModel.ExpectedResult);
            cmd.Parameters.AddWithValue("@Comment", ObjModel.Comment);
            cmd.Parameters.AddWithValue("@Status", ObjModel.Status);
            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                Result = db.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int Delete(int BugID)
        {
            int Result = -1;

            SqlCommand cmd = new SqlCommand("dbo.BugMasterDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BugID", BugID);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public DataTable FileInsert(InsertNewBugModel ObjModel)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Attachment", ObjModel.Attachment);
            cmd.Parameters.AddWithValue("@OriginalFileName", ObjModel.OriginalFileName);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public int FileMappingInsert(InsertNewBugModel ObjModel)
        {
            int result = 0;

            SqlCommand cmd = new SqlCommand("dbo.FileMappingMasterInsertByBug");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BugID", ObjModel.BugID);
            cmd.Parameters.AddWithValue("@FileID", ObjModel.FileID);
            cmd.Parameters.AddWithValue("@UploadType", ObjModel.UploadType);
            try
            {
                result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public List<InsertNewBugModel> SelectAttachment(InsertNewBugModel ObjModel)
        {
            DataTable dt = new DataTable();
            List<InsertNewBugModel> details = new List<InsertNewBugModel>();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterSelectForBug");
            cmd.Parameters.AddWithValue("@BugID", ObjModel.BugID);
            cmd.Parameters.AddWithValue("@UploadType", ObjModel.UploadType);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                InsertNewBugModel report = new InsertNewBugModel();
                report.FileID = Convert.ToInt32(dtrow["FileID"].ToString());
                report.Attachment = dtrow["Attachment"].ToString().Trim();
                report.OriginalFileName = dtrow["OriginalFileName"].ToString().Trim();

                details.Add(report);
            }

            return details;
        }

        public int Exist(string File, int ProjectBugMasterID)
        {
            int Result = 0;

            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.BugMasterCheckForExist");
            cmd.Parameters.AddWithValue("@File", File);
            cmd.Parameters.AddWithValue("@ProjectBugMasterID", ProjectBugMasterID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }

            if (dt.Rows.Count != 0)
            {
                return 1;
            }
            return Result;
        }

        public string SelectProjectName(int ProjectBugMasterID)
        {
            string ProjectName = "";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.ProjectBugMasterSelectProjectName");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ProjectBugMasterID", ProjectBugMasterID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }

            if (dt.Rows.Count < 1)
            {
                ProjectName = "";
                return ProjectName;
            }

            ProjectName = dt.Rows[0]["ProjectName"].ToString();

            return ProjectName;
          
        }
    }
}