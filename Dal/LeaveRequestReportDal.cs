﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class LeaveRequestReportDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public List<LeaveRequestModel> SelectByDate(string FromDate, string ToDate,int UserID)
        {
            DataTable dt = new DataTable();
            List<LeaveRequestModel> details = new List<LeaveRequestModel>();
            SqlCommand cmd = new SqlCommand("dbo.LeaveMasterSelectByDate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = dp.getDataTable(cmd);

                foreach (DataRow dtrow in dt.Rows)
                {
                    LeaveRequestModel report = new LeaveRequestModel();
                    report.LeaveID = Convert.ToInt32(dtrow["LeaveID"].ToString());
                    report.Name = dtrow["Name"].ToString().Trim();
                    report.FromDate = Convert.ToDateTime(dtrow["FromDate"].ToString().Trim());
                    report.ToDate = Convert.ToDateTime(dtrow["ToDate"].ToString().Trim());
                    report.NoOfDays = Convert.ToInt32(dtrow["NoOfDays"].ToString().Trim());
                    if (report.NoOfDays == 0)
                    {
                        report.NoOfDays = 1;
                    }
                    if (report.NoOfDays > 1)
                    {
                        report.NoOfDays = report.NoOfDays +1;
                    }
                    report.Description = dtrow["Description"].ToString().Trim();
                    report.ApprovedByName = (dtrow["ApprovedByName"].ToString().Trim());
                    report.IsApprove = 0;
                    string IsApprove = dtrow["IsApprove"].ToString().Trim();
                    if (IsApprove == "True")
                    {
                        report.IsApprove = 1;
                    }
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }

        public int LeaveRequestApprove(int LeaveID, int IsApprove, int ApprovedBy)
        {
            int Result = 0;



            SqlCommand cmd = new SqlCommand("dbo.LeaveMasterUpdate");

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LeaveID", LeaveID);
            cmd.Parameters.AddWithValue("@IsApprove", IsApprove);
            cmd.Parameters.AddWithValue("@ApprovedBy", ApprovedBy);
           
            try
            {
                Result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }


            return Result;
        }

        public LeaveRequestModel SelectByID(int LeaveID)
        {


            SqlCommand cmd = new SqlCommand("dbo.LeaveMasterSelectByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LeaveID", LeaveID);


            LeaveRequestModel report = new LeaveRequestModel();
            try
            {
                DataTable dt = dp.getDataTable(cmd);



                report.LeaveID = Convert.ToInt32(dt.Rows[0]["LeaveID"].ToString());
                report.Name = dt.Rows[0]["Name"].ToString().Trim();
                report.FromDate = Convert.ToDateTime(dt.Rows[0]["FromDate"].ToString().Trim());
                report.ToDate = Convert.ToDateTime(dt.Rows[0]["ToDate"].ToString().Trim());
                report.NoOfDays = Convert.ToInt32(dt.Rows[0]["NoOfDays"].ToString().Trim());
                if (report.NoOfDays == 0)
                {
                    report.NoOfDays = 1;
                }
                report.Description = dt.Rows[0]["Description"].ToString().Trim();
                report.ApprovedByName = (dt.Rows[0]["ApprovedByName"].ToString().Trim());
                report.EMailID = (dt.Rows[0]["EmailID"].ToString().Trim());
                report.IsApprove = (Convert.ToBoolean(dt.Rows[0]["IsApprove"])) ? 1 : 0;


            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return report;
        }
    }
}
