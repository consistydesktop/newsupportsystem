﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ViewProjectDetailsForEmployeeDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<ViewProjectDetailsForEmployeeModel> Select(int UserID)
        {
            DataTable dt = new DataTable();
            List<ViewProjectDetailsForEmployeeModel> details = new List<ViewProjectDetailsForEmployeeModel>();
            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterSelectAllForEmployee");
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewProjectDetailsForEmployeeModel report = new ViewProjectDetailsForEmployeeModel();
                report.ProjectDetailMasterID = Convert.ToInt32(dtrow["ProjectDetailMasterID"].ToString());
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().ToUpper().Trim());
                report.ProjectName = dtrow["ProjectName"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();

                details.Add(report);
            }

            return details;
        }

        public List<ViewProjectDetailsForEmployeeModel> SelectByID(int UserID, int ProjectDetailMasterID)
        {
            DataTable dt = new DataTable();
            List<ViewProjectDetailsForEmployeeModel> details = new List<ViewProjectDetailsForEmployeeModel>();
            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterSelectForEmployee");
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@ProjectDetailMasterID", ProjectDetailMasterID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewProjectDetailsForEmployeeModel report = new ViewProjectDetailsForEmployeeModel();
                report.ProjectDetailMasterID = Convert.ToInt32(dtrow["ProjectDetailMasterID"].ToString());
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().ToUpper().Trim());
                report.ProjectName = dtrow["ProjectName"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();

                details.Add(report);
            }

            return details;
        }

        public List<ViewProjectDetailsForEmployeeModel> Search(string FromDate, string ToDate, int UserID)
        {
            List<ViewProjectDetailsForEmployeeModel> details = new List<ViewProjectDetailsForEmployeeModel>();
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("dbo.ProjectDetailMasterSelectByDateForEmployee");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewProjectDetailsForEmployeeModel report = new ViewProjectDetailsForEmployeeModel();
                report.ProjectDetailMasterID = Convert.ToInt32(dtrow["ProjectDetailMasterID"].ToString());
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().ToUpper().Trim());
                report.ProjectName = dtrow["ProjectName"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();

                details.Add(report);
            }

            return details;
        }

        public List<ViewProjectDetailsForEmployeeModel> SelectAttachment(ViewProjectDetailsForEmployeeModel ObjModel)
        {
            DataTable dt = new DataTable();
            List<ViewProjectDetailsForEmployeeModel> details = new List<ViewProjectDetailsForEmployeeModel>();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterSelectForProject");
            cmd.Parameters.AddWithValue("@ProjectDetailMasterID", ObjModel.ProjectDetailMasterID);
            cmd.Parameters.AddWithValue("@UploadType", ObjModel.UploadType);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                ViewProjectDetailsForEmployeeModel report = new ViewProjectDetailsForEmployeeModel();
                report.FileID = Convert.ToInt32(dtrow["FileID"].ToString());
                report.Attachment = dtrow["Attachment"].ToString().Trim();
                report.OriginalFileName = dtrow["OriginalFileName"].ToString().Trim();

                details.Add(report);
            }

            return details;
        }
    }
}