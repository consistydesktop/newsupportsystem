﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class FinnoPerformanceDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();


        public int Insert(FinoPerformanceModel ObjModel)
        {
            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.FinnoPerformanceMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ClientName", ObjModel.ClientName);
            cmd.Parameters.AddWithValue("@ServiceName", ObjModel.ServiceName);
            cmd.Parameters.AddWithValue("@SuccessAmount", ObjModel.SuccessAmount);
            cmd.Parameters.AddWithValue("@SuccessCount", ObjModel.SuccessCount);
            cmd.Parameters.AddWithValue("@FailCount", ObjModel.FailCount);
            cmd.Parameters.AddWithValue("@FailAmount", ObjModel.FailAmount);
            cmd.Parameters.AddWithValue("@TxnDoc", ObjModel.TxnDoc);

            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                result = dp.getOutputParam(cmd, Output);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public List<ClientViewModel> ClientSelect()
        {
            List<ClientViewModel> details = new List<ClientViewModel>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.ClientSelect");
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            try
            {
                foreach (DataRow dtrow in dt.Rows)
                {
                    ClientViewModel report = new ClientViewModel();
                    report.ClientName = dtrow["ClientName"].ToString().Trim();
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }

        public DataTable Search(FinoPerformanceViewModel objModel)
        {
            DataTable dt = new DataTable();
            List<FinoPerformanceViewModel> details = new List<FinoPerformanceViewModel>();

            SqlCommand cmd = new SqlCommand("dbo.FinnoPerformanceMasterSearch");
            cmd.Parameters.AddWithValue("@FromDate", objModel.FormDate);
            cmd.Parameters.AddWithValue("@Todate", objModel.Todate);
            cmd.Parameters.AddWithValue("@ClientName", objModel.ClientName);
            cmd.Parameters.AddWithValue("@SalesType", objModel.SalesType);

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
          
            return dt;
        }

    }
}
