﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class TargetMasterDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();
        public int InsertTargetMaster(TargetMasterModel ObjModel)
        {
            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.TargetMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Month", ObjModel.Month);
            cmd.Parameters.AddWithValue("@Year", ObjModel.Year);
            cmd.Parameters.AddWithValue("@TargetAmount", ObjModel.TargetAmount);


            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }
        public List<TargetMasterModel> SelectTargetMaster()
        {
            DataTable dt = new DataTable();
            List<TargetMasterModel> details = new List<TargetMasterModel>();
            SqlCommand cmd = new SqlCommand("dbo.TargetMasterSelectByMonth");
            try
            {

                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            foreach (DataRow dtrow in dt.Rows)
            {
                TargetMasterModel report = new TargetMasterModel();
                report.Month = dtrow["Month"].ToString().Trim();
                report.Year = dtrow["Year"].ToString().Trim();
                report.TargetAmount = Convert.ToDecimal(dtrow["TargetAmount"]);
               
                details.Add(report);
            }

            return details;
        }
    }
}
