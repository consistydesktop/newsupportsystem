﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class TimeSheetDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public List<TimeSheetModel> SelectSystemName()
        {
            DataTable dt = new DataTable();
            List<TimeSheetModel> details = new List<TimeSheetModel>();

            try
            {
                SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectSystemName");
                cmd.CommandType = CommandType.StoredProcedure;
                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    TimeSheetModel report = new TimeSheetModel();

                    report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                    report.SystemName = dtrow["SystemName"].ToString().Trim();
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return details;
        }

        //public int Insert(List<TimeSheetModel> ObjModel, int UserID)
        //{
        //    int result = -1;
        //    int result1 = -1;
        //    SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterInsert");

        //    cmd.CommandType = CommandType.StoredProcedure;

        //    foreach (var item in ObjModel)
        //    {
        //        cmd.Parameters.AddWithValue("@UserID", UserID);
        //        cmd.Parameters.AddWithValue("@Date", item.Date);
        //        cmd.Parameters.AddWithValue("@Reason", item.Reason);

        //        SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
        //        Output.Direction = ParameterDirection.ReturnValue;
        //        cmd.Parameters.Add(Output);

        //        result = dp.getOutputParam(cmd, Output);
        //        break;
        //    }

        //    foreach (var item in ObjModel)
        //    {
        //        try
        //        {
        //            if (result > 0)
        //            {
        //                SqlCommand cmd1 = new SqlCommand("dbo.TimeSheetDetailInsert");
        //                cmd1.CommandType = CommandType.StoredProcedure;

        //                cmd1.Parameters.AddWithValue("@TimeSheetmasterID", result);
        //                cmd1.Parameters.AddWithValue("@SystemID", item.SystemID);
        //                cmd1.Parameters.AddWithValue("@Task", item.Task);
        //                cmd1.Parameters.AddWithValue("@Time", item.Time);

        //                result1 = dp.executeStoredProcedure(cmd1);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            new Logger().write(ex);
        //        }
        //    }

        //    return result1;
        //}

        public int Update(TimeSheetModel ObjModel)
        {
            int result = -1;

            SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterUpdate");

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TimeSheetMasterID", ObjModel.TimeSheetMasterID);

            cmd.Parameters.AddWithValue("@Date", ObjModel.Date);
            cmd.Parameters.AddWithValue("@Reason", ObjModel.Reason);
            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return result;
        }

        public int UpdateByID(TimeSheetModel ObjModel)
        {
            int result = -1;

            SqlCommand cmd = new SqlCommand("dbo.TimeSheetDetailUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TimeSheetDetailID", ObjModel.TimeSheetDetailID);
            cmd.Parameters.AddWithValue("@SystemID", ObjModel.SystemID);
            cmd.Parameters.AddWithValue("@Task", ObjModel.Task);
            cmd.Parameters.AddWithValue("@Time", ObjModel.Time);

            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return result;
        }

        public List<TimeSheetModel> Select(int UserID)
        {
            DataTable dt = new DataTable();
            List<TimeSheetModel> details = new List<TimeSheetModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterSelect");
                cmd.Parameters.AddWithValue("@UserID", UserID);

                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    TimeSheetModel report = new TimeSheetModel();
                    report.TotalTime = Convert.ToInt32(dtrow["TotalTime"].ToString());
                    report.Date = dtrow["Date"].ToString().Trim();
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }

        public List<TimeSheetModel> SelectByID(string Date, int UserID)
        {
            DataTable dt = new DataTable();
            List<TimeSheetModel> details = new List<TimeSheetModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterSelectByID");
                cmd.Parameters.AddWithValue("@Date", Date);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    TimeSheetModel report = new TimeSheetModel();
                    report.TimeSheetMasterID = Convert.ToInt32((dtrow["TimeSheetMasterID"].ToString()));
                    report.TimeSheetDetailID = Convert.ToInt32((dtrow["TimeSheetDetailID"].ToString()));
                    report.Task = dtrow["Task"].ToString();
                    report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                    report.Date = dtrow["Date"].ToString();
                    report.Time = Convert.ToInt32(dtrow["Time"].ToString());

                    report.Date = dtrow["Date"].ToString();
                    report.Reason = dtrow["Reason"].ToString();
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }

        public int Delete(string Date, int UserID)
        {
            int result = -1;

            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterDelete");
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@UserID", UserID);

                cmd.Parameters.AddWithValue("@Date", Date);

                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return result;
        }




        public List<TimeSheetModel> SelectTimeSheetByDate(int UserID, string Date)
        {
            DataTable dt = new DataTable();
            List<TimeSheetModel> details = new List<TimeSheetModel>();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.TimeSheetDetailSelectByDate");
                cmd.Parameters.AddWithValue("@Date", Date);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                dt = dp.getDataTable(cmd);
                foreach (DataRow dtrow in dt.Rows)
                {
                    TimeSheetModel report = new TimeSheetModel();
                    report.TimeSheetDetailID = Convert.ToInt32(dtrow["TimeSheetDetailID"].ToString().Trim());
                    report.Task = dtrow["Task"].ToString();
                    report.SystemName = dtrow["SystemName"].ToString();
                    report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString().Trim());
                    report.Date = dtrow["Date"].ToString();
                    report.Time = Convert.ToInt32(dtrow["Time"].ToString());
                    report.Date = dtrow["Date"].ToString();
                    report.Status =Convert.ToInt32( dtrow["Status"].ToString());
                    details.Add(report);
                }
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return details;
        }

        public int Insert(int UserID, string Date, int SystemID, string Task, int Time, int Status)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.TimeSheetDetailInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@Date", Date);
            cmd.Parameters.AddWithValue("@SystemID", SystemID);
            cmd.Parameters.AddWithValue("@Task", Task);
            cmd.Parameters.AddWithValue("@Time", Time);
            cmd.Parameters.AddWithValue("@Status", Status);

            try
            {
                Result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex) { }
            return Result;
        }

        public int Update(int UserID, int TimeSheetDetailID, int SystemID, string Task, int Time, int Status)
        {

            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.TimeSheetDetailUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@TimeSheetDetailID", TimeSheetDetailID);
            cmd.Parameters.AddWithValue("@SystemID", SystemID);
            cmd.Parameters.AddWithValue("@Task", Task);
            cmd.Parameters.AddWithValue("@Time", Time);
            cmd.Parameters.AddWithValue("@Status", Status);

            try
            {
                Result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex) { }
            return Result;
        }

        public int Delete(int UserID, int TimeSheetDetailID)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.TimeSheetDetailDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@TimeSheetDetailID", TimeSheetDetailID);

            try
            {
                Result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex) { }
            return Result;
        }

        public DateTime getDateByID(int TimeSheetDetailID)
        {
            DateTime TimeSheetDate = DateTime.Today;

            SqlCommand cmd = new SqlCommand("dbo.TimeSheetDetailSelectDateByID");
            cmd.Parameters.AddWithValue("@TimeSheetDetailID", TimeSheetDetailID);
            DataTable dt = new DataTable();
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            DataRow dtRow = dt.Rows[0];
            TimeSheetDate = Convert.ToDateTime(dtRow["DATE"].ToString());
            return TimeSheetDate;
        }

        public int SelectTotalTimeByUserID(int UserID, string Date)
        {
            int TotalTime = 0;
            SqlCommand cmd = new SqlCommand("dbo.TimeSheetDetailSelectTotalTimeByID");
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@Date", Date);
            DataTable dt = new DataTable();
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            DataRow dtRow = dt.Rows[0];
            TotalTime = Convert.ToInt32(dtRow["TotalTime"]);
            return TotalTime;
        }
    }
}