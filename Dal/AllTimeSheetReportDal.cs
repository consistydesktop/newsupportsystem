﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class AllTimeSheetReportDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public List<Model.AllTimeSheetReportModel> SelectEmployeeName()
        {
            List<AllTimeSheetReportModel> details = new List<AllTimeSheetReportModel>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByEmployeeID");
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AllTimeSheetReportModel report = new AllTimeSheetReportModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().ToUpper().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<AllTimeSheetReportModel> Select()
        {
            List<AllTimeSheetReportModel> details = new List<AllTimeSheetReportModel>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterSelectAllReport");
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AllTimeSheetReportModel report = new AllTimeSheetReportModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().ToUpper().Trim();
                report.Date = dtrow["Date"].ToString().Trim();
                report.TotalTime = Convert.ToInt32(dtrow["TotalTime"].ToString().Trim());
                details.Add(report);
            }

            return details;
        }

        public List<AllTimeSheetReportModel> Search(AllTimeSheetReportModel ObjModel)
        {
            List<AllTimeSheetReportModel> details = new List<AllTimeSheetReportModel>();
            DataTable dt = new DataTable();

            string Name = "";

            if (ObjModel.UserID != 0)
                Name = SelectNameByID(ObjModel.UserID);



            SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterSelectAllReportByID");
            cmd.Parameters.AddWithValue("@FromDate", ObjModel.FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ObjModel.ToDate);
            cmd.Parameters.AddWithValue("@DeveloperName", Name);

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AllTimeSheetReportModel report = new AllTimeSheetReportModel();

                report.Name = dtrow["Name"].ToString().ToUpper().Trim();
                report.Date = dtrow["Date"].ToString().Trim();
                report.TotalTime = Convert.ToInt32(dtrow["TotalTime"].ToString().Trim());
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString().Trim());
                details.Add(report);
            }

            return details;
        }

        private string SelectNameByID(int UserID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectNamebyUserID");
            cmd.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }

            DataRow dtrow = dt.Rows[0];

            string Name = dtrow["Name"].ToString();

            return Name;
        }

        //public List<AllTimeSheetReportModel> SelectTimeSheetReportByEmployeeName(string Date, int UserID)
        //{

        //       Date = Convert.ToDateTime(Date).ToString("dddd, dd MMMM yyyy");

        //    List<AllTimeSheetReportModel> details = new List<AllTimeSheetReportModel>();
        //    DataTable dt = new DataTable();

        //    SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterSelectByIDForReport");
        //    cmd.Parameters.AddWithValue("@UserID", UserID);
        //    cmd.Parameters.AddWithValue("@ToDate", Date);

        //    dt = dp.getDataTable(cmd);
        //    foreach (DataRow dtrow in dt.Rows)
        //    {
        //        AllTimeSheetReportModel report = new AllTimeSheetReportModel();

        //        report.TimeSheetDetailID = Convert.ToInt32(dtrow["TimeSheetDetailID"].ToString());
        //        report.Task = dtrow["Task"].ToString();
        //        report.SystemName = (dtrow["SystemName"].ToString());
        //        report.Date = dtrow["Date"].ToString();
        //        report.Time = Convert.ToInt32(dtrow["Time"].ToString());
        //        report.Name = dtrow["Name"].ToString().Trim();
        //        report.Status = Convert.ToInt32(dtrow["Status"].ToString());
        //        details.Add(report);
        //    }
        //    return details;
        //}

        public List<AllTimeSheetReportModel> SelectTimeSheetReportByEmployeeName(string Date, int UserID)
        {
           // Date = Convert.ToDateTime(Date).ToString("dddd, dd MMMM yyyy");
            List<AllTimeSheetReportModel> details = new List<AllTimeSheetReportModel>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.TimeSheetMasterSelectByIDForNewReport");
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@Date", Date);
            dt = dp.getDataTable(cmd);

            foreach (DataRow dtrow in dt.Rows)
            {
                AllTimeSheetReportModel report = new AllTimeSheetReportModel();
                report.TimeSheetDetailID = Convert.ToInt32(dtrow["TimeSheetDetailID"].ToString());
                report.Task = dtrow["Task"].ToString();
                report.SystemName = (dtrow["SystemName"].ToString());
                report.Time = Convert.ToInt32(dtrow["Time"].ToString());
                details.Add(report);
            }
            return details;
        }
    }
}
