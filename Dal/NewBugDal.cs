﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class NewBugDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public int Insert(NewBugModel objModel, int UserID)
        {
            int Result = 0;

            SqlCommand cmd = new SqlCommand("dbo.ProjectBugMasterInsert");
            cmd.Parameters.AddWithValue("@ProjectName", objModel.ProjectName);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter Output = new SqlParameter("@Output", SqlDbType.Int);
            Output.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(Output);
            try
            {
                Result = db.getOutputParam(cmd, Output);
            }
            catch (Exception es)
            {
                new Logger().write(es);
            }

            return Result;
        }

        public List<NewBugModel> Select()
        {
            List<NewBugModel> details = new List<NewBugModel>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.ProjectBugMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                NewBugModel report = new NewBugModel();

                report.ProjectBugMasterID = Convert.ToInt32(dtrow["ProjectBugMasterID"].ToString());
                report.ProjectName = (dtrow["ProjectName"].ToString());
                report.RESOLVED = Convert.ToInt32(dtrow["RESOLVED"]);
                report.NEW = Convert.ToInt32(dtrow["NEW"]);
                report.INPROGRESS = Convert.ToInt32(dtrow["INPROGRESS"]);
                report.CONFIRMED = Convert.ToInt32(dtrow["CONFIRMED"]);
                report.CLOSED = Convert.ToInt32(dtrow["CLOSED"]);
                report.FEEDBACK = Convert.ToInt32(dtrow["FEEDBACK"]);
                report.TotalIssue = (report.RESOLVED + report.NEW + report.INPROGRESS + report.CONFIRMED + report.CLOSED + report.FEEDBACK);
                report.INPROGRESS = (report.TotalIssue - report.RESOLVED);
                details.Add(report);
            }

            return details;
        }

        public int Delete(int ProjectBugMasterID)
        {
            int Result = 0;

            SqlCommand cmd = new SqlCommand("dbo.ProjectBugMasterDelete");
            cmd.Parameters.AddWithValue("@ProjectBugMasterID", ProjectBugMasterID);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception es)
            {
                new Logger().write(es);
            }

            return Result;
        }

        public bool ExistProjectName(string ProjectName)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.ProjectBugMasterSelectForExistance");
            cmd.Parameters.AddWithValue("@ProjectName", ProjectName);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            if (dt.Rows.Count > 0)
                return true;

            return false;
        }
    }
}