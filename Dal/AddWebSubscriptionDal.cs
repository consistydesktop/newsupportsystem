﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class AddWebSubscriptionDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<Model.AddWebSubscriptionModel> SelectSystemName()
        {
            DataTable dt = new DataTable();
            List<AddWebSubscriptionModel> details = new List<AddWebSubscriptionModel>();

            SqlCommand cmd = new SqlCommand("dbo.SystemMasterSelectSystemName");
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddWebSubscriptionModel report = new AddWebSubscriptionModel();
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int Insert(AddWebSubscriptionModel objModel)
        {
            int Result = 0;

            SqlCommand cmd = new SqlCommand("dbo.WebSubscriptionMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@SystemID", objModel.SystemID);
            cmd.Parameters.AddWithValue("@WebsiteURL", objModel.WebsiteURL);
            cmd.Parameters.AddWithValue("@HostingIP", objModel.HostingIP);
            cmd.Parameters.AddWithValue("@EndDate", objModel.EndDate);

            cmd.Parameters.AddWithValue("@PaymentStatus", objModel.PaymentStatus);
            cmd.Parameters.AddWithValue("@Description", objModel.Description);
            cmd.Parameters.AddWithValue("@Token", objModel.Token);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return Result;
        }



        public DataTable getEndDateForFinovativeBySystemID(int systemID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.WebSubscriptionMasterSelectEndDateBySystemID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SystemID", systemID);


            try
            {
                dt = db.getDataTable(cmd);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return dt;
        }

        public List<AddWebSubscriptionModel> Select()
        {

            List<AddWebSubscriptionModel> details = new List<AddWebSubscriptionModel>();

            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.WebSubscriptionMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AddWebSubscriptionModel report = new AddWebSubscriptionModel();
                report.SubscriptionID = Convert.ToInt32(dtrow["WebSubscriptionID"].ToString());
                report.SystemID = Convert.ToInt32(dtrow["SystemID"].ToString());
                report.WebsiteURL = (dtrow["WebsiteURL"].ToString());
                report.DOC = Convert.ToDateTime(dtrow["DOC"].ToString().Trim());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.ProductName = dtrow["ProductName"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.PaymentStatus = dtrow["PaymentStatus"].ToString().Trim();
                report.HostingIP = dtrow["HostingIP"].ToString().Trim();
                report.EndDate = Convert.ToDateTime(dtrow["EndDate"].ToString().Trim());

                details.Add(report);
            }

            return details;
        }

        public AddWebSubscriptionModel SelectByID(int WebSubscriptionID)
        {


            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.WebSubscriptionMasterSelectByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@WebSubscriptionID", WebSubscriptionID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            AddWebSubscriptionModel report = new AddWebSubscriptionModel();
            report.SubscriptionID = Convert.ToInt32(dt.Rows[0]["WebSubscriptionID"].ToString());
            report.SystemID = Convert.ToInt32(dt.Rows[0]["SystemID"].ToString());
            report.WebsiteURL = (dt.Rows[0]["WebsiteURL"].ToString());
            report.DOC = Convert.ToDateTime(dt.Rows[0]["DOC"].ToString().Trim());
            report.SystemName = dt.Rows[0]["SystemName"].ToString().Trim();
            report.Description = dt.Rows[0]["Description"].ToString().Trim();
            report.PaymentStatus = dt.Rows[0]["PaymentStatus"].ToString().Trim();
            report.HostingIP = dt.Rows[0]["HostingIP"].ToString().Trim();
            report.EndDate = Convert.ToDateTime(dt.Rows[0]["EndDate"].ToString().Trim());

            return report;

        }

        public int Delete(int WebSubscriptionID)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.WebSubscriptionMasterDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@WebSubscriptionID", WebSubscriptionID);
            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int Update(AddWebSubscriptionModel objModel)
        {
            int Result = 0;

            SqlCommand cmd = new SqlCommand("dbo.WebSubscriptionMasterUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@WebSubscriptionID", objModel.SubscriptionID);
            cmd.Parameters.AddWithValue("@SystemID", objModel.SystemID);
            cmd.Parameters.AddWithValue("@WebsiteURL", objModel.WebsiteURL);
            cmd.Parameters.AddWithValue("@HostingIP", objModel.HostingIP);
            cmd.Parameters.AddWithValue("@EndDate", objModel.EndDate);
            cmd.Parameters.AddWithValue("@PaymentStatus", objModel.PaymentStatus);
            cmd.Parameters.AddWithValue("@Description", objModel.Description);

            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return Result;
        }

        public int UpdateCallBackURL(string callBackURL, int UserID, int ProductID, string WebURL)
        {
            int Result = 0;
            if (callBackURL == "")
                return Result;


            SqlCommand cmd = new SqlCommand("dbo.SubscriptionCallBackURLMasterUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@callBackURL", callBackURL);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@ProductID", ProductID);
            cmd.Parameters.AddWithValue("@WebURL", WebURL);


            try
            {
                Result = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return Result;
        }


        public DataTable getEndDate(string URL, string HostedIP, string Token)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.WebSubscriptionMasterSelectEndDate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@URL", URL);
            cmd.Parameters.AddWithValue("@Token", Token);
            cmd.Parameters.AddWithValue("@HostedIP", HostedIP);

            try
            {
                dt = db.getDataTable(cmd);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return dt;
        }

        public DataTable getEndDateForFinovative(string URL, string HostedIP, string LoginToken)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.WebSubscriptionMasterSelectEndDateV1");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@URL", URL);
            cmd.Parameters.AddWithValue("@Token", LoginToken);
            cmd.Parameters.AddWithValue("@HostedIP", HostedIP);

            try
            {
                dt = db.getDataTable(cmd);

            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return dt;
        }

    }
}
