﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class ProviderConfigurationDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();
        public DataTable SelectOpertaorSetUp(ProviderConfigurationModel objModel)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.ConfigurationSelect");
                cmd.Parameters.Add("@ProviderID", SqlDbType.Int).Value = objModel.ProviderID;
                cmd.Parameters.Add("@ServiceID", SqlDbType.Int).Value = objModel.ServiceID;
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }

        public DataTable SelectOpertaorSetUp()
        {

            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.RechargeProviderOperatorMasterSelect");

                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }


        public DataTable ServiceSelect()
        {

            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.ServiceSelect");

                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return dt;
        }


        public int UpdateOperatorSetUp(ProviderConfigurationModel objModel)
        {
            int i = -1;
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.RechargeProviderOperatorMasterUpdate");
                cmd.Parameters.Add("@ProviderID", SqlDbType.Int).Value = objModel.ProviderID;
                cmd.Parameters.Add("@OperatorID", SqlDbType.Int).Value = objModel.OperatorID;
                cmd.Parameters.Add("@Opcode", SqlDbType.VarChar).Value = objModel.Opcode;
                cmd.Parameters.Add("@Param1", SqlDbType.VarChar).Value = objModel.Param1;
                i = db.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return i;
        }
    }
}
