﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class FinovativeSampleResponseDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();
        public DataTable selectSampleResponseReport()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.SampleResponseSelectReport");
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }


            return dt;
        }

        public DataTable SelectBYProviderID(int ProviderID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.SampleResponseSelectByProviderID");

            cmd.Parameters.AddWithValue("@ProviderID", ProviderID);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }


            return dt;
        }

        public int Delete(int SampleResponseID)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.SampleResponseDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SampleResponseID", SampleResponseID);
            try
            {
                Result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return Result;
        }

        public int Update(SampleResponseViewModel ObjModel)
        {

            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.SampleResponseUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SampleResponseID", ObjModel.SampleResponseID);
            cmd.Parameters.AddWithValue("@ProviderID", ObjModel.ProviderID);
            cmd.Parameters.AddWithValue("@Type", ObjModel.Type);
            cmd.Parameters.AddWithValue("@TextMustExit", ObjModel.TextMustExit);
            cmd.Parameters.AddWithValue("@Response", ObjModel.Response);
            cmd.Parameters.AddWithValue("@Status", ObjModel.Status);

            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }

        public SampleResponseViewModel SelectByID(int SampleResponseID)
        {
            DataTable dt = new DataTable();
            Model.SampleResponseViewModel report = new Model.SampleResponseViewModel();

            SqlCommand cmd = new SqlCommand("dbo.SampleResponseSelectByID");
            cmd.Parameters.Add("@SampleResponseID", SqlDbType.Int).Value = SampleResponseID;
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {

                report.SampleResponseID = Convert.ToInt32(dtrow["SampleResponseID"].ToString());
                report.ProviderID = Convert.ToInt32(dtrow["ProviderID"].ToString());
                report.DeveloperName = (dtrow["DeveloperName"].ToString());
                report.Type = (dtrow["Type"].ToString());
                report.TextMustExit = (dtrow["TextMustExit"].ToString());
                report.Response = (dtrow["Response"].ToString());
                report.Status = (dtrow["Status"].ToString());
            }
            return report;
        }

        public int Insert(SampleResponseViewModel ObjModel)
        {
            int result = -1;
            SqlCommand cmd = new SqlCommand("dbo.SampleResponseInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ProviderID", ObjModel.ProviderID);
            cmd.Parameters.AddWithValue("@Type", ObjModel.Type);
            cmd.Parameters.AddWithValue("@TextMustExit", ObjModel.TextMustExit);
            cmd.Parameters.AddWithValue("@Response", ObjModel.Response);
            cmd.Parameters.AddWithValue("@Status", ObjModel.Status);


            try
            {
                result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            return result;
        }
    }
}
