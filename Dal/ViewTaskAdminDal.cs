﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Dal
{
    public class ViewTaskAdminDal
    {
        private DatabaseProcessing db = new DatabaseProcessing();

        public List<AssignTaskModel> Select()
        {
            DataTable dt = new DataTable();
            List<AssignTaskModel> details = new List<AssignTaskModel>();
            SqlCommand cmd = new SqlCommand("dbo.TaskMasterSelect");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignTaskModel report = new AssignTaskModel();
                report.TaskID = Convert.ToInt32(dtrow["TaskID"].ToString());
                report.Task = dtrow["Task"].ToString().Trim();
                report.Deadline = dtrow["Deadline"].ToString().Trim();
                report.TaskStatus = (dtrow["Status"].ToString().Trim());
                report.EmployeeNames = getEmployeeNamesByTaskID(report.TaskID);
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        private string getEmployeeNamesByTaskID(int TaskID)
        {
            string EmployeeNames = "";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("dbo.TaskEmployeeMappingSelectNameByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TaskID", TaskID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            foreach (DataRow dtrow in dt.Rows)
            {
                EmployeeNames += " " + dtrow["Name"].ToString();
            }

            return EmployeeNames;
        }

        public List<AssignTaskModel> Search(string FromDate, string ToDate, int UserID, string Status)
        {
            DataTable dt = new DataTable();
            List<AssignTaskModel> details = new List<AssignTaskModel>();
            List<AssignTaskModel> data = new List<AssignTaskModel>();
            SqlCommand cmd = new SqlCommand("dbo.TaskMasterSelectByDate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@Status", Status);
            try
            {
                dt = db.getDataTable(cmd);

                //if (UserID != 0)
                //{
                //    Getdata(details, dt, "UserID", UserID.ToString());
                //}
                //if (!(Status == null || Status == ""))
                //{
                //    Getdata(details, dt, "Status", Status);
                //}

                //if (UserID == 0 && (Status == null || Status == ""))
                //{ 

                //}

                foreach (DataRow dtrow in dt.Rows)
                {
                    AssignTaskModel report = new AssignTaskModel();
                    report.TaskID = Convert.ToInt32(dtrow["TaskID"].ToString());
                    report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                    report.Task = dtrow["Task"].ToString().Trim();
                    report.Deadline = dtrow["Deadline"].ToString().Trim();
                    report.TaskStatus = (dtrow["Status"].ToString().Trim());
                    report.SystemName = dtrow["SystemName"].ToString().Trim();
                    report.EmployeeNames = getEmployeeNamesByTaskID(report.TaskID);
                    details.Add(report);
                }

                data = (from f in details
                        where (UserID.Equals(0) || (f.UserID.Equals(UserID)))
                             //   && (Status.Equals("") || Status.Equals(null) || f.TaskStatus.Equals(Status))
                        select f).ToList();
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }

            return data;
        }

        public List<AssignTaskModel> SearchByUserID(string FromDate, string ToDate, int UserID)
        {
            DataTable dt = new DataTable();
            List<AssignTaskModel> details = new List<AssignTaskModel>();
            SqlCommand cmd = new SqlCommand("dbo.TaskMasterSelectByDateAndEmployee");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignTaskModel report = new AssignTaskModel();
                report.TaskID = Convert.ToInt32(dtrow["TaskID"].ToString());
                report.Task = dtrow["Task"].ToString().Trim();
                report.Deadline = dtrow["Deadline"].ToString().Trim();
                report.TaskStatus = (dtrow["Status"].ToString().Trim());
                report.SystemName = dtrow["SystemName"].ToString().Trim();
                report.EmployeeNames = getEmployeeNamesByTaskID(report.TaskID);
                details.Add(report);
            }

            return details;
        }

        public List<AssignTaskModel> SelectEmployee()
        {
            DataTable dt = new DataTable();
            List<AssignTaskModel> details = new List<AssignTaskModel>();

            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByEmployeeID");
            try
            {
                dt = db.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignTaskModel report = new AssignTaskModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().ToUpper().Trim();
                details.Add(report);
            }

            return details;
        }
    }
}