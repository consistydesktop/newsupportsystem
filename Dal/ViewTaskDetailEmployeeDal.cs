﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Dal
{
    public class ViewTaskDetailEmployeeDal
    {
        private DatabaseProcessing dp = new DatabaseProcessing();

        public List<AssignTaskModel> SelectEmployeeName()
        {
            DataTable dt = new DataTable();
            List<AssignTaskModel> details = new List<AssignTaskModel>();

            SqlCommand cmd = new SqlCommand("dbo.UserInformationSelectByEmployeeID");
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignTaskModel report = new AssignTaskModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                report.Name = dtrow["Name"].ToString().ToUpper().Trim();
                details.Add(report);
            }

            return details;
        }

        public List<List<AssignTaskModel>> SelectByID(int TaskID)
        {
            List<List<AssignTaskModel>> AllTaskDetails = new List<List<AssignTaskModel>>();

            List<AssignTaskModel> TaskDetails = new List<AssignTaskModel>();
            List<AssignTaskModel> SubTaskDetails = new List<AssignTaskModel>();
            List<AssignTaskModel> CommentDetails = new List<AssignTaskModel>();
            List<AssignTaskModel> DeveloperDetails = new List<AssignTaskModel>();

            TaskDetails = TaskMasterSelectByID(TaskID);
            SubTaskDetails = SubTaskSelectByID(TaskID);
            CommentDetails = CommentMasterSelectByID(TaskID);
            DeveloperDetails = TaskEmployeeMappingSelectByID(TaskID);

            AllTaskDetails.Add(TaskDetails);
            AllTaskDetails.Add(SubTaskDetails);
            AllTaskDetails.Add(CommentDetails);
            AllTaskDetails.Add(DeveloperDetails);

            return AllTaskDetails;
        }

        private List<AssignTaskModel> TaskEmployeeMappingSelectByID(int TaskID)
        {
            DataTable dt = new DataTable();
            List<AssignTaskModel> DeveloperDetails = new List<AssignTaskModel>();

            SqlCommand cmd = new SqlCommand("dbo.TaskEmployeeMappingSelectByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TaskID", TaskID);
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignTaskModel report = new AssignTaskModel();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString().Trim());
                DeveloperDetails.Add(report);
            }
            return DeveloperDetails;
        }

        private List<AssignTaskModel> TaskMasterSelectByID(int TaskID)
        {
            DataTable dt = new DataTable();
            List<AssignTaskModel> TaskDetails = new List<AssignTaskModel>();

            SqlCommand cmd = new SqlCommand("dbo.TaskMasterSelectByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TaskID", TaskID);

            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignTaskModel report = new AssignTaskModel();
                report.TaskID = Convert.ToInt32(dtrow["TaskID"].ToString().Trim());
                report.Task = dtrow["Task"].ToString().Trim();
                report.Description = dtrow["Description"].ToString().Trim();
                report.Deadline = dtrow["Deadline"].ToString().Trim();
                report.Point = Convert.ToInt32(dtrow["Points"].ToString().Trim());
                report.TaskStatus =(dtrow["Status"].ToString().Trim());
                TaskDetails.Add(report);
            }

            return TaskDetails;
        }

        private List<AssignTaskModel> SubTaskSelectByID(int TaskID)
        {
            DataTable dt = new DataTable();
            List<AssignTaskModel> SubTaskDetails = new List<AssignTaskModel>();

            SqlCommand cmd = new SqlCommand("dbo.SubTaskMasterSelectByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TaskID", TaskID);
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignTaskModel report = new AssignTaskModel();

                report.SubTaskID = Convert.ToInt32(dtrow["SubTaskMaster"].ToString().Trim());
                report.SubTask = dtrow["SubTask"].ToString().Trim();
                report.SubTaskStatus = Convert.ToInt32(dtrow["Status"].ToString().Trim());
                SubTaskDetails.Add(report);
            }

            return SubTaskDetails;
        }

        private List<AssignTaskModel> CommentMasterSelectByID(int TaskID)
        {
            DataTable dt = new DataTable();
            List<AssignTaskModel> CommentDetails = new List<AssignTaskModel>();

            SqlCommand cmd = new SqlCommand("dbo.CommentMasterSelectByID");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReferenceID", TaskID);
            cmd.Parameters.AddWithValue("@CommentType", "Task");
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignTaskModel report = new AssignTaskModel();
                report.CommentID = Convert.ToInt32(dtrow["CommentmasterID"].ToString());
                report.Comment = dtrow["Comment"].ToString().Trim();
                report.UserID = Convert.ToInt32(dtrow["UserID"].ToString());
                CommentDetails.Add(report);
            }
            return CommentDetails;
        }

        public List<AssignTaskModel> SelectAttachment(int TaskID)
        {
            DataTable dt = new DataTable();
            List<AssignTaskModel> details = new List<AssignTaskModel>();

            SqlCommand cmd = new SqlCommand("dbo.FileMasterSelectForTask");

            cmd.Parameters.AddWithValue("@UploadType", "Task");
            cmd.Parameters.AddWithValue("@TaskID", TaskID);
            try
            {
                dt = dp.getDataTable(cmd);
            }
            catch (Exception ex)
            {
                new Logger().write(ex);
            }
            foreach (DataRow dtrow in dt.Rows)
            {
                AssignTaskModel report = new AssignTaskModel();
                report.FileID = Convert.ToInt32(dtrow["FileID"].ToString());
                report.Attachment = dtrow["Attachment"].ToString().Trim();
                report.OriginalFileName = dtrow["OriginalFileName"].ToString().Trim();
                details.Add(report);
            }

            return details;
        }

        public int InsertComment(AssignTaskModel ObjModel)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.CommentMasterInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReferenceID", ObjModel.TaskID);
            cmd.Parameters.AddWithValue("@UserID", ObjModel.AssignedUserID);
            cmd.Parameters.AddWithValue("@Comment", ObjModel.Comment);
            cmd.Parameters.AddWithValue("@CommentType", ObjModel.CommentType);
            try
            {
                Result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            return Result;
        }

        public int UpdateSubTaskStatus(AssignTaskModel ObjModel)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.SubTaskMasterUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubTaskID", ObjModel.SubTaskID);
            cmd.Parameters.AddWithValue("@SubTaskStatus", ObjModel.SubTaskStatus);
            try
            {
                Result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            return Result;
        }

        public int UpdateTaskStatus(AssignTaskModel ObjModel)
        {
            int Result = -1;
            SqlCommand cmd = new SqlCommand("dbo.TaskMasterUpdateStatus");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TaskID", ObjModel.TaskID);
            cmd.Parameters.AddWithValue("@Status", ObjModel.TaskStatus);
            try
            {
                Result = dp.executeStoredProcedure(cmd);
            }
            catch (Exception ex) { new Logger().write(ex); }
            return Result;
        }
    }
}